
# 文件解释
1.package.json
 项目描述及依赖
2.package-lock.json
 版本管理使用的文件
3.package-lock.json
 指定文件无需提交到git上
4.index.html  
 页面入口文件
5.src 
|- assets
    存放页面对应的css、js以及图片文件
|- components
    存放项目使用的公共组件
6.test
-e2ec
NightWatch是一个专门的端对端测试运行器
7.components
公共组件
8.utils
工具（方法）
9.views
页面

它的配置文件nightwatch.conf.js会设置对应的命令参数，拼接到nightwatch的命令行操作，

