/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
// import http from '@/utils/httpRequest'
// import { isURL } from '@/utils/validate'
// import { clearLoginInfo } from '@/utils'  // 以后要登录再解开

Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
  { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404未找到' } }
]

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
  path: '/',
  component: _import('main'),
  name: 'main',
  redirect: { name: '首页' },
  meta: { title: '主入口整体布局' },
  children: [
    // 通过meta对象设置路由展示方式
    // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
    // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
    // 提示: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
    { path: '/home', component: _import('home'), name: '首页', meta: { title: '首页' } },
    { path: '/greenBuilding', component: _import('greenBuilding'), name: '绿色建筑', meta: { title: '绿色建筑' } },
    { path: '/prefabricatedBuilding', component: _import('prefabricatedBuilding'), name: '装配式建筑', meta: { title: '装配式建筑' } },
    { path: '/energySavingBuilding', component: _import('energySavingBuilding'), name: '建筑节能', meta: { title: '建筑节能' } }
  ],
  beforeEnter (to, from, next) {
    // let token = Vue.cookie.get('token')
    // if (!token || !/\S/.test(token)) {
    //   // 清除登录信息
    //   // clearLoginInfo()
    //   // next({ name: 'login' })
    // }
    next()
  }
}

const router = new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  isAddDynamicMenuRoutes: true, // 是否已经添加动态(菜单)路由,暂时不想要动态菜单功能，先前端写死，如果需要后台去控制菜单则改为false
  routes: globalRoutes.concat(mainRoutes)
})

export default router
