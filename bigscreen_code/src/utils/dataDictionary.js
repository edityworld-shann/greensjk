  const dataDictionary = {
    // 人员列表
    person: [
      {
        id: 2,
        project_name: '公司1',
        name: '李四',
        project_address: '公司',
        project_person: '17829304562',
        contract_quantity: '17829304562@163.com',
        equipment: '外方单位'
      },
      {
        id: 1,
        project_name: '公司2',
        name: '王五',
        project_address: '公司',
        project_person: '16474839201',
        contract_quantity: '16474839201@163.com',
        equipment: '本单位'
      }
    ],
    unitType: [
      {
        label: '本单位',
        value: '0'
      }, {
        label: '外方单位',
        value: '1'
      }
    ],
    // 项目列表
    projectList: [
      {
        id: 1,
        a: '项目一',
        b: '北京市昌平区',
        c: '测量',
        d: '单位1',
        e: '王五',
        f: '进行中',
        g: '2020/04/28',
        h: '暂未',
        i: '10000',
        j: '30%',
        k: '2020/04/28 12:38:56',
        m: '李四',
        l: '该项目为测试项目'
      }
    ],
    // 收付款方向
    paymentDirection: [
      {
        label: '收款',
        value: 0
      }, {
        label: '付款',
        value: 1
      }, {
        label: '全部',
        value: -1
      }
    ],
    // 项目类型
    projectType: [
      {
        label: '市政工程监测',
        value: 0
      }, {
        label: '轨道工程监测',
        value: 1
      }, {
        label: '市政工程测量',
        value: 2
      }, {
        label: '轨道工程测量',
        value: 3
      }
    ],
    // 审定状态
    approvalStatus: [
      {
        label: '通过',
        value: 0
      }, {
        label: '不通过',
        value: 1
      }
    ],
    // 发票类型
    invoiceType: [
      {
        label: '专票',
        value: 0
      }, {
        label: '普票',
        value: 1
      }, {
        label: '收据',
        value: 2
      }
    ],
    // 合同状态
    contractStatus: [
      {
        label: '未签订',
        value: 0
      }, {
        label: '已签未返回',
        value: 1
      }, {
        label: '已签订',
        value: 2
      }
      // , {
      //   label: '已完工',
      //   value: 3
      // }, {
      //   label: '已结清',
      //   value: 4
      // }
    ],
    // 用来手动修改完结状态
    // 合同状态
    contractStatusChange: [
      {
        label: '未签订',
        value: 0
      }, {
        label: '已签未返回',
        value: 1
      }, {
        label: '已签订',
        value: 2
      }, {
        label: '已完工',
        value: 3
      }
    ],
    // 用来在完结列表的表格中回显全部数据
    contractStatusEND: [
      {
        label: '未签订',
        value: 0
      }, {
        label: '已签未返回',
        value: 1
      }, {
        label: '已签订',
        value: 2
      }, {
        label: '已完工',
        value: 3
      }, {
        label: '已结清',
        value: 4
      }
    ],
    // 合同类型
    contractType: [
      {
        label: '劳务分包',
        value: 0
      }, {
        label: '实体或者走账',
        value: 1
      }
    ],
    // 项目状态
    projectState: [
      {
        label: '已签合同',
        value: 0
      }, {
        label: '未签合同',
        value: 1
      }
    ],
    // 收付款列表
    invoice: [{
      a: '发票1',
      b: '5000',
      c: '2020/04/28 08:28:56',
      d: 'N001',
      e: '付款',
      f: '王五',
      m: '50000',
      n: '30000',
      remark: '收款为正，付款为负'
    }],
    // 任务列表
    workDone: [
      {
        a: '任务1',
        b: '2020/04/28 08:28:56',
        c: '2020/05/5 08:28:56',
        d: '王五',
        e: '第一次交付',
        f: '发票1',
        remark: '收款为正，付款为负'
      }
    ],
    // 百分比
    percentage: [{
      id: 1,
      value: '25%'
    }, {
      id: 2,
      value: '50%'
    }, {
      id: 3,
      value: '75%'
    }],
    // 工作量签认
    commitStatus: [
      {
        label: '已签认',
        value: 1
      }, {
        label: '未签认',
        value: 0
      }
    ],
    instrumentStatus: [
      {
        label: '租赁',
        value: 2
      }, {
        label: '自有',
        value: 1
      }
    ],
    instrumentUseStatus: [
      {
        label: '报废',
        value: 1
      }, {
        label: '维修',
        value: 2
      }, {
        label: '使用',
        value: 3
      }, {
        label: '库存',
        value: 4
      }
    ],
    carBuyStatus: [
      {
        label: '租赁',
        value: 2
      }, {
        label: '购买',
        value: 1
      }
    ],
    ExamineApprovalStatus: [
      {
        label: '未开始',
        value: 1
      }, {
        label: '流转中',
        value: 2
      }, {
        label: '驳回',
        value: 3
      }, {
        label: '归档',
        value: 4
      }
    ]
  }
  export default dataDictionary
