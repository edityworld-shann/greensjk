import { Message } from 'element-ui'
import httpRequest from '@/utils/httpRequest'
// function downFile (e) {
//   if (e.target.tagName !== 'P') {
//     return
//   }
//   let element = JSON.parse(e.target.getAttribute('data'))
//   if (!element.msg) {
//     Message.error('找不到地址~')
//   }
//   // 文件的后台地址
//   let fileName = element.msg.replace(/\[|]/g, '')
//   // 文件名
//   let downName = fileName.split('$')[1]
//   let data = {
//     fileName: fileName,
//     type: 2
//   }
//   httpRequest({
//     url: httpRequest.adornUrl(`/sys/file/down`),
//     method: 'post',
//     params: httpRequest.adornParams(data),
//     responseType: 'blob' // 声明返回blob格式
//   }).then(res => {
//     if (res.data) {
//       var blod = new Blob([res.data])
//       var a = document.createElement('a')
//       a.href = URL.createObjectURL(blod)
//       a.style.display = 'none'
//       document.body.appendChild(a)
//       a.download = downName
//       a.click()
//       document.body.removeChild(a)
//     }
//   })
// }
function previewFile (e) {
  if (e.target.tagName !== 'P') {
    return
  }
  let element = JSON.parse(e.target.getAttribute('data'))
  if (!element.msg) {
    Message.error('找不到地址~')
  }
  // 文件的后台地址
  let fileName = element.msg.replace(/\[|]/g, '')
  // 文件名
  let downName = fileName.split('$')[1]
  let filetype = ''
  if (downName && downName.split('.')[downName.split('.').length - 1]) {
    filetype = downName.split('.')[downName.split('.').length - 1]
  }
  let data = {
    fileName: fileName,
    type: 2
  }
  httpRequest({
    url: httpRequest.adornUrl(`/sys/file/down`),
    method: 'post',
    params: httpRequest.adornParams(data),
    responseType: 'blod' // 声明返回blob格式
  }).then(res => {
    console.log(res)
    var blod = ''
    if (filetype === 'pdf') {
      blod = new Blob([res.data], {
        type: 'application/pdf;chartset=UTF-8'
      })
      window.open(URL.createObjectURL(blod))
    }
    if (filetype === 'jpg') {
      blod = new Blob([res.data], {
        type: 'image/jpg'
      })
      window.open(URL.createObjectURL(blod))
      // MessageBox.alert(`<div style="background:url(${URL.createObjectURL(blod)});width:70vw;height:60vh"></div>`, '图片', {
      //   dangerouslyUseHTMLString: true
      // }).catch(() => {})
    }
    if (filetype === 'png') {
      blod = new Blob([res.data], {
        type: 'image/png'
      })
      window.open(URL.createObjectURL(blod))
    }
  })
}
export function viewAttachments (file) {
  if (!file || file === 'string') {
    Message.error('暂无附件~')
  } else {
    let fileList = JSON.parse(file)
    let htmlStr = []
    fileList.forEach(element => {
      htmlStr.push(this.$createElement('p', {
        style: {
          'cursor': 'pointer',
          'text-decoration': 'underline'
        },
        attrs: {
          data: JSON.stringify(element),
          // title: '点击下载'
          title: '点击预览'
        },
        on: {
          click: previewFile
        }
      }, element.name))
    })
    this.$alert(this.$createElement('div', {}, htmlStr), '附件', {
      dangerouslyUseHTMLString: true,
      confirmButtonText: '关闭'
    }).catch(() => {})
  }
}
