import httpRequest from '@/utils/httpRequest'

function getDepartmentList (query) {
  return httpRequest({
    url: httpRequest.adornUrl(`/sys/department/selectList`),
    method: 'get',
    params: httpRequest.adornParams(query)
  })
}
function getConpanyList () {
  return httpRequest({
    url: httpRequest.adornUrl(`/sys/company/listCompany`),
    method: 'get',
    params: httpRequest.adornParams()
  })
}
function getPersonList () {
  return httpRequest({
    url: httpRequest.adornUrl(`/sys/user/selectUserList`),
    method: 'post',
    params: httpRequest.adornParams()
  })
}
function getProjectList (query) {
  let res = httpRequest({
    url: httpRequest.adornUrl(`/sys/project/listProject`),
    method: 'get',
    params: httpRequest.adornParams(query)
  })
  return res
}
function getInvoiceList (query) {
  let res = httpRequest({
    url: httpRequest.adornUrl(`/sys/invoice/listInvoice`),
    method: 'get',
    params: httpRequest.adornParams(query)
  })
  return res
}
function getWorkList (query) {
  let res = httpRequest({
    url: httpRequest.adornUrl(`/sys/workload/list`),
    method: 'get',
    params: httpRequest.adornParams(query)
  })
  return res
}
function getContractId (query) {
  let res = httpRequest({
    url: httpRequest.adornUrl(`/sys/contract/contractList`),
    method: 'get',
    params: httpRequest.adornParams(query)
  })
  return res
}
function getRoleList () {
  return httpRequest({
    url: httpRequest.adornUrl(`/sys/role/select`),
    method: 'get',
    params: httpRequest.adornParams()
  })
}
// 获取证书类型
function getCertificatetype () {
  return httpRequest({
    url: httpRequest.adornUrl(`/sys/certificatetype/selectList`),
    method: 'get',
    params: httpRequest.adornParams()
  })
}
export {getDepartmentList, getConpanyList, getPersonList, getProjectList, getInvoiceList, getWorkList, getContractId, getRoleList, getCertificatetype}
