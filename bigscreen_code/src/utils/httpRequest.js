// import Vue from 'vue'
import axios from 'axios'
// import router from '@/router'
import qs from 'qs'
// import { Message } from 'element-ui'
import merge from 'lodash/merge'
// import { clearLoginInfo } from '@/utils'

const http = axios.create({
  timeout: 1000 * 30,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
  // config.headers['token'] = Vue.cookie.get('token') // 请求头带上token
  let token = JSON.parse(localStorage.getItem('pro__Access-Token')).value
  // let token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NTAyOTIwMDMsInVzZXJuYW1lIjoiZ292In0.KEs3pt8S58PSn1-AlSmc3DUxCKLbnTUeCX_G58zqEZs'
  config.headers['X-Access-Token'] = token // 请求头带上token
  return config
}, error => {
  return Promise.reject(error)
})

/**
 * 响应拦截
 */
http.interceptors.response.use(response => {
  if (response.data && response.data.code === 401) {} // 401, token失效
  return response
}, error => {
  // if (error.response.status) {
  //   switch (error.response.status) {
  //     // 404请求不存在
  //     case 404:
  //       Message.error('服务器被外星人抓走了~')
  //       break
  //     // 其他错误，直接抛出错误提示
  //     default:
  //       Message.error('服务器发生意外错误，错误code：' + error.response.status)
  //   }
  // }
  return Promise.reject(error)
})

/**
 * 请求地址处理
 * @param {*} actionName action方法名称
 */
http.adornUrl = (actionName) => {
  // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
  return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi' : window.SITE_CONFIG.baseUrl) + actionName
}

/**
 * get请求参数处理
 * @param {*} params 参数对象
 * @param {*} openDefultParams 是否开启默认参数?
 */
http.adornParams = (params = {}, openDefultParams = false) => {
  var defaults = {
    't': new Date().getTime()
  }
  return openDefultParams ? merge(defaults, params) : params
}

/**
 * post请求数据处理
 * @param {*} data 数据对象
 * @param {*} openDefultdata 是否开启默认数据?
 * @param {*} contentType 数据格式
 *  json: 'application/json; charset=utf-8'
 *  form: 'application/x-www-form-urlencoded; charset=utf-8'
 */
http.adornData = (data = {}, openDefultdata = false, contentType = 'json') => {
  var defaults = {
    't': new Date().getTime()
  }
  data = openDefultdata ? merge(defaults, data) : data
  return contentType === 'json' ? JSON.stringify(data) : qs.stringify(data)
}

/**
 * 向url后加参数
 */
http.addUrlParam = function (url = '', params = {}) {
  if (url && JSON.stringify(params) !== '{}') {
    var paramArray = []
    Object.keys(params).forEach(function (key) {
      var param = key + '=' + params[key]
      paramArray.push(param)
    })
    var url2 = encodeURI(url + '?' + paramArray.join('&'))
    var enurl = encodeURI(url2)
    return enurl
  } else {
    return url
  }
}

export default http
