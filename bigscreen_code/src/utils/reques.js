
import axios from 'axios'

// import base from '@/api/base.js'

import { Notification } from 'element-ui'

/**
 * 【指定 axios的 baseURL】
 * 则映射后端域名，通过 vue.config.js
 */
// 创建 axios 实例
const service = axios.create({
   baseURL: 'http://localhost:7898/greensjk/',
  //baseURL: 'http://221.216.15.81:7898/greensjk/',
  // baseURL: 'http://221.216.15.81:7898/greensjk/',
  timeout: 9000 * 30, // 请求超时时间
  headers: {
    'Content-Type': 'application/json;charset=utf-8',
    'Access-Control-Allow-Credentials': 'true'// 1
  },
  withCredentials: true// 2
})
// 设置POST请求默认的 Content-Type
// service.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'
service.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8'
service.defaults.crossDomain = true
service.defaults.withCredentials = true
const err = (error) => {
  return Promise.reject(error)
}

// Request Rinterceptor
service.interceptors.request.use(config => {
  // config 配置
  const token = sessionStorage.getItem('token')
  config.headers['X-Access-Token'] = token
  return config
}, (error) => {
  return Promise.reject(error)
})

// Response interceptor
service.interceptors.response.use(response => {
  // const { request, data } = response
  const { config, data } = response
  if (config.isFileDownload && data.code === 200) {
    return data
  }
  const { code, message } = data
  switch (code) {
    case 400:
      Notification.error({ message: '请求参数错误！', description: message, duration: 4 })
      break
    case 401:
      Notification.error({ message: '很抱歉，登录已过期，请重新登录！', description: message, duration: 4 })
      sessionStorage.removeItem('token')
      setTimeout(() => {
        window.location.href = '/login'
      }, 500)
      return
    case 403:
      Notification.error({ message: '暂无权限！', description: message, duration: 4 })
      break
    case 500:
      Notification.error({ message: '服务器系统内部错误！', description: message, duration: 4 })
      break
    default:
      return data
  }
}, err)

export {
  service as axios
}
