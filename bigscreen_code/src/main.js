import Vue from 'vue'
import App from '@/App'
import router from '@/router'                 // api: https://github.com/vuejs/vue-router
import store from '@/store'                   // api: https://github.com/vuejs/vuex
import VueCookie from 'vue-cookie'            // api: https://github.com/alfhen/vue-cookie
import ElementUI from 'element-ui'
import '@/assets/scss/index.scss'
import httpRequest from '@/utils/httpRequest' // api: https://github.com/axios/axios
import { isAuth } from '@/utils'
import dataDictionary from '@/utils/dataDictionary'
import cloneDeep from 'lodash/cloneDeep'
import * as echarts from 'echarts'
import 'element-ui/lib/theme-chalk/index.css'
import 'echarts-gl'
import vueSeamlessScroll from 'vue-seamless-scroll'
Vue.use(VueCookie)
Vue.use(ElementUI)
Vue.config.productionTip = false
Vue.component('vue-seamless-scroll', vueSeamlessScroll)

// 非生产环境, 适配mockjs模拟数据                 // api: https://github.com/nuysoft/Mock
if (process.env.NODE_ENV !== 'production') {
  require('@/mock')
}

// 挂载全局
Vue.prototype.$http = httpRequest // ajax请求方法
Vue.prototype.isAuth = isAuth     // 权限方法
Vue.prototype.$echarts = echarts // echarts
Vue.prototype.dataDictionary = dataDictionary     // 数据字典
// event Bus 用于无关系组件间的通信。
Vue.prototype.$bus = new Vue()
// 适配为rem设置的 remUnit: 3840
Vue.prototype.$getRealPX = (px) => {
  if (!px) { return px }
  const rem = px / 3840
  const htmlWidth = document.documentElement.clientWidth || document.body.clientWidth
  return (htmlWidth * 100 * rem)
}
// 数组去重
Vue.prototype.$unique = (arr) => {
  const res = new Map()
  return arr.filter((arr) => !res.has(arr.id) && res.set(arr.id, 1))
}
// 数组取最大值
Vue.prototype.$maxMum = (arr, parameter) => {
  if (arr && arr.length > 0) {
    return Math.max.apply(Math, arr.map(item => { return (item[parameter]) }))
  } else {
    return 10
  }
}

Vue.prototype.$toFixed = function (number, n) {
  if (n > 20 || n < 0) {
    throw new RangeError('toFixed() digits argument must be between 0 and 20')
  }
  if (isNaN(number) || number >= Math.pow(10, 21)) {
    return number.toString()
  }
  if (typeof (n) === 'undefined' || n === 0) {
    return (Math.round(number)).toString()
  }
  let result = number.toString()
  const arr = result.split('.')
  // 整数的情况
  if (arr.length < 2) {
    result += '.'
    for (let i = 0; i < n; i += 1) {
      result += '0'
    }
    return result
  }
  const integer = arr[0]
  const decimal = arr[1]
  if (decimal.length === n) {
    return result
  }
  if (decimal.length < n) {
    for (let i = 0; i < n - decimal.length; i += 1) {
      result += '0'
    }
    return result
  }
  result = integer + '.' + decimal.substr(0, n)
  const last = decimal.substr(n, 1)
  // 四舍五入，转换为整数再处理，避免浮点数精度的损失
  if (parseInt(last, 10) >= 5) {
    const x = Math.pow(10, n)
    result = (Math.round((parseFloat(result) * x)) + 1) / x
    result = result.toFixed(n)
  }
  return result
}
// 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
