// import Vue from 'vue'
import { axios } from '../utils/reques'

// get
export function getAction (url, params) {
  return axios({
    url: url,
    method: 'GET',
    params
  })
}

// post
export function postAction (url, params) {
  return axios({
    url: url,
    method: 'POST',
    data: params
  })
}

// delete
export function delAction (url, params) {
  return axios({
    url: url,
    method: 'DELETE',
    params
  })
}
