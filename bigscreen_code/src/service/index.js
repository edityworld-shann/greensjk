import { getAction } from './http'

// 绿色建筑竣工面积
const getlargeScreen = (params) => getAction('/largeScreen/greenBuildingStatistics/specificDataInformation', params)
// 装配式
const getPrefabricated = (params) => getAction('/largeScreen/greenBuildingStatistics/assemblyDataStatistics', params)
// 绿色建筑  表格接口
const getTableList = (params) => getAction('/largeScreen/greenBuildingStatistics/accumulatedGreenBuildingGradeInThisYear', params)
// 项目总数
const getTotalItems = (params) => getAction('/largeScreen/greenBuildingStatistics/totalTitems', params)
// 绿色建筑数据
const getGreenBuildingData = (params) => getAction('/largeScreen/greenBuildingStatistics/greenBuildingData', params)
// 装配式建筑 具体数据信息？
const getJtsjList = (params) => getAction('/largeScreen/greenBuildingStatistics/prefabricatedBuilding', params)
// 装配式建筑竣工 具体数据信息
const getJg = (params) => getAction('/largeScreen/greenBuildingStatistics/fabricatedCompletion', params)
// 建筑节能统计 具体数据信息
const getBuildingEnergyEfficiencyStatistics = (params) => getAction('/largeScreen/greenBuildingStatistics/buildingEnergyEfficiencyStatistics', params)
// 可再生能源利用情况
const utilizationOfRenewableEnergy = (params) => getAction('/largeScreen/greenBuildingStatistics/utilizationOfRenewableEnergy', params)
// 大屏建筑节能统计项目总数和绿色项目总数
const buildingEnergySavingTotalTitems = (params) => getAction('/largeScreen/greenBuildingStatistics/buildingEnergySavingTotalTitems', params)
// 建筑节能统计 建筑节能数据
const buildingEnergySaving = (params) => getAction('/largeScreen/greenBuildingStatistics/buildingEnergySaving', params)

export {
  getlargeScreen,
  getPrefabricated,
  getTableList,
  getTotalItems,
  getGreenBuildingData,
  getJtsjList,
  getJg,
  getBuildingEnergyEfficiencyStatistics,
  utilizationOfRenewableEnergy,
  buildingEnergySavingTotalTitems,
  buildingEnergySaving
}
