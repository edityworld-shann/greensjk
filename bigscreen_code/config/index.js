'use strict'
// Template version: 1.2.5
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path')
const devEnv = require('./dev.env')

module.exports = {
  dev: {
    // 编译输出的二级目录
    assetsSubDirectory: 'static',
    // 编译发布的根目录，可配置为资源服务器域名或者cdn域名
    assetsPublicPath: '/',
    // 代理列表, 是否开启代理通过[./dev.env.js]配置
    proxyTable: devEnv.OPEN_PROXY === false ? {} : {
      '/proxyApi': {
        target:"http://121.36.83.156:80/tc", // 服务器
        // target: "http://192.168.31.16/tc", // 本地
        changeOrigin: true,
        pathRewrite: {
          '^/proxyApi': '/'
        }
      }
    },

    host: 'localhost', // 开发时候的访问域名。可以通过环境变量自己设置。
    port: 8001, // 开发时候的端口。可以通过环境变量PORT设定。如果端口被占用了，会随机分配一个未被使用的端口
    autoOpenBrowser: true,//是否自动打开浏览器
    // 下面两个都是浏览器展示错误的方式
    errorOverlay: true,//  在浏览器是否展示错误蒙层
    notifyOnErrors: true,// 是否展示错误的通知

     // 这个是webpack-dev-servr的watchOptions的一个选项，指定webpack检查文件的方式
    // 因为webpack使用文件系统去获取文件改变的通知。在有些情况下，这个可能不起作用。例如，当使用NFC的时候，
    // vagrant也会在这方面存在很多问题，在这些情况下，使用poll选项（以轮询的方式去检查文件是否改变）可以设定为true
    // 或者具体的数值，指定文件查询的具体周期。
    poll: false,

    // 是否使用eslint loader去检查代码
    useEslint: true,
    /// 如果设置为true，在浏览器中，eslint的错误和警告会以蒙层的方式展现。
    showEslintErrorsInOverlay: false,

    /**
     * Source Maps
     */

    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'eval-source-map',

    // 指定是否通过在文件名称后面添加一个查询字符串来创建source map的缓存
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,

    // 关闭css的source map
    // (https://github.com/webpack/css-loader#sourcemaps)
    cssSourceMap: false,
  },

  build: {
    // Template for index.html
    // 入口文件
    index: path.resolve(__dirname, '../dist/index.html'),

    // 编译生成的文件的目录
    assetsRoot: path.resolve(__dirname, '../dist'),
     // 编译生成的静态文件的目录
    assetsSubDirectory: 'static',
    // 编译发布的根目录，可配置为资源服务器域名或者cdn域名，必须加. 否则页面将为白色
    assetsPublicPath: '/',

    /**
     * Source Maps
     */

    productionSourceMap: false,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    /// 是否开启生产环境的gzip压缩
    productionGzip: false,
    // 开启gzip压缩的文件的后缀名称
    productionGzipExtensions: ['js', 'css'],

    // 如果这个选项是true的话，那么则会在build后，会在浏览器中生成一份bundler报告
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
