示例代码

1、控制日期的可选日期：

import dayjs, { Dayjs } from 'dayjs';
<a-form-model-item prop="enddate">
   <j-date :disabled-date="disabledEndDate" :disabled="formDisabled" placeholder="请选择截止日期" v-model="model.enddate"  style="width: 100%" />
</a-form-model-item>


//控制前后日期
disabledEndDate(current) {
  let startTime = this.model.startdate;
  let maxEndDate = moment(startTime).add(2, 'years')
  return  current < dayjs().endOf('day') ||  current > maxEndDate ;
},

//进展当前今天以后的日期 && import dayjs, { Dayjs } from 'dayjs'; && :disabled-date="disabledEndDate"
disabledFillDate(current) {
  return  current > dayjs().endOf('day'); 
},



2、查询参数回调解析函数
/**
* 对查询的参数格式化 / zhangchengguo / 2022年4月23日
* */
queryParamCallback(queryParams){
  if(queryParams.applydate!=undefined){
    queryParams.applydate = moment(queryParams.applydate).format("YYYY-MM-DD");
  }
},



/**
* @description 根据用户登录身份设置数据查询参数
* */
doBeforeCreated(){
    this.queryParam.reporttm = moment(new Date());//.format('YYYY-MM')

      //登录账号类型：0单位，1个人，2专家库，3住建厅（省），4推荐单位，6县区，7市区
      this.loginUserType = localStorage.getItem('loginUserType')
      console.log("page's doBeforeCreated && loginUserType = ",this.loginUserType);
      if(this.loginUserType==3){
        this.queryParam.applyState = "1,2";//省厅查看市州建筑节能申报,已申报、申报通过
      }else{
        this.queryParam.applyState = undefined
      }
},


    /**
     * @description
     * */
    dataSourceAction(dataSource){
      dataSource.forEach(item=>{
        let dt0 = item.dt0;
        Object.keys(dt0).forEach(key=>{
          if(dt0[key]==undefined){
            dt0[key] = 0;
          }
        })
        Object.assign(item,dt0);
      })
      return dataSource;
    },
