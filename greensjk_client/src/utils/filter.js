import Vue from "vue";
import * as dayjs from "dayjs";
import moment from 'moment'

Vue.filter('NumberFormat', function (value) {
  if (!value) {
    return '0'
  }
  let intPartFormat = value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') //将整数部分逢三一断
  return intPartFormat
})

Vue.filter('dayjs', function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return dayjs(dataStr).format(pattern)
})

Vue.filter('moment', function(dataStr, pattern = 'YYYY-MM-DD HH:mm:ss') {
  return dayjs(dataStr).format(pattern)
})

/** 字符串超长截取省略号显示 */
Vue.filter('ellipsis', function (value, vlength = 25) {
  if(!value){
    return "";
  }
  console.log('vlength: '+ vlength);
  if (value.length > vlength) {
    return value.slice(0, vlength) + '...'
  }
  return value
})


/**
 * 根据项目申报日期计算渲染颜色
 * */
Vue.prototype.$recordColorTag = function(terminal_date) {
  //项目完成日期（2022-04-21） - 当前日期（2022年4月25日） = - 4
  let diff = moment(terminal_date).diff(moment(),'days');
  if (diff<0){
    //
    return 'red';
  }else if (diff<31) {
    //2022年4月30日 - 2022年4月25日 = 5 > 0 && <31
    return '#FFA500';
  }
  else {
    return '#1890FF';
  }
}


/**
 * 根据项目申报日期计算渲染颜色
 * @param record 数据列表中的某一列数据
 * */
Vue.prototype.$recordColorTagV2= function(record) {


  let terminal_date = record.completedate||record.enddate||record.projectenddate;//当前申报事项的完成时间
  //项目完成日期（2022-04-21） - 当前日期（2022年4月25日） = - 4
  let diff = moment(terminal_date).diff(moment(),'days');
  if (diff<0){
    //
    return 'red';
  }else if (diff<31) {
    //2022年4月30日 - 2022年4月25日 = 5 > 0 && <31
    return '#FFA500';
  }
  else {
    return '#1890FF';
  }
}

Vue.prototype.$ReportDataValidCheck= function(record) {
  if(record.currentDetail==undefined || record.currentDetail == 0 ) record.currentDetail = {};
  if(record.dt0==undefined || record.dt0 == 0) record.dt0 = {};
  if(record.dt1==undefined || record.dt1 == 0) record.dt1 = {};
  if(record.dt2==undefined || record.dt2 == 0) record.dt2 = {};
  if(record.dt3==undefined || record.dt3 == 0) record.dt3 = {};
}



