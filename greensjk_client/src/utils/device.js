import enquireJs from 'enquire.js'

const enquireScreen = function (call) {
  // tablet
  // const handler = {
  //   match: function () {
  //     call && call(0)
  //   },
  //   unmatch: function () {
  //     call && call(-1)
  //   }
  // }
  // // mobile
  // const handler2 = {
  //   match: () => {
  //     call && call(1)
  //   }
  // }
  // enquireJs.register('screen and (max-width: 1087.99px)', handler)
  // enquireJs.register('screen and (max-width: 767.99px)', handler2)
  var ua = navigator.userAgent,
  isWindowsPhone = /(?:Windows Phone)/.test(ua),
  isSymbian = /(?:SymbianOS)/.test(ua) || isWindowsPhone,
  isAndroid = /(?:Android)/.test(ua),
  isFireFox = /(?:Firefox)/.test(ua),
  isChrome = /(?:Chrome|CriOS)/.test(ua),
  isTablet = /(?:iPad|PlayBook)/.test(ua) || (isAndroid && !/(?:Mobile)/.test(ua)) || (isFireFox && /(?:Tablet)/.test(ua)),
  isPhone = /(?:iPhone)/.test(ua) && !isTablet,
  isPc = !isPhone && !isAndroid && !isSymbian;
  var os =  {
      isTablet: isTablet,
      isPhone: isPhone,
      isAndroid: isAndroid,
      isPc: isPc
  }
  if (os.isAndroid || os.isPhone) {
    console.log("手机--------------------")
    call(0)
  } else if (os.isTablet) {
    console.log("平板-----------------")
    call(1)
  } else if(os.isPc) {
    console.log("电脑-------------------")
    call(-1)
  }
}

export default enquireScreen