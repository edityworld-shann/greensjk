import { getAction, deleteAction, putAction, postAction, httpAction } from '@/api/manage'
import Vue from 'vue'
import {UI_CACHE_DB_DICT_DATA } from "@/store/mutation-types"

//application login
const getUserByToken = (params)=>getAction("/sys/getUserByToken",params);

//角色管理
const addRole = (params)=>postAction("/sys/role/add",params);
const editRole = (params)=>putAction("/sys/role/edit",params);
const checkRoleCode = (params)=>getAction("/sys/role/checkRoleCode",params);
const queryall = (params)=>getAction("/sys/role/queryall",params);

//用户管理
const addUser = (params)=>postAction("/sys/user/add",params);
const editUser = (params)=>putAction("/sys/user/edit",params);
const queryUserRole = (params)=>getAction("/sys/user/queryUserRole",params);
const getUserList = (params)=>getAction("/sys/user/list",params);
const frozenBatch = (params)=>putAction("/sys/user/frozenBatch",params);
//验证用户是否存在
const checkOnlyUser = (params)=>getAction("/sys/user/checkOnlyUser",params);
//改变密码
const changePassword = (params)=>putAction("/sys/user/changePassword",params);

//权限管理
const addPermission= (params)=>postAction("/sys/permission/add",params);
const editPermission= (params)=>putAction("/sys/permission/edit",params);
const getPermissionList = (params)=>getAction("/sys/permission/list",params);
const getSystemMenuList = (params)=>getAction("/sys/permission/getSystemMenuList",params);
const getSystemSubmenu = (params)=>getAction("/sys/permission/getSystemSubmenu",params);
const getSystemSubmenuBatch = (params) => getAction('/sys/permission/getSystemSubmenuBatch', params)
const queryTreeList = (params)=>getAction("/sys/permission/queryTreeList",params);
const queryTreeListForRole = (params)=>getAction("/sys/role/queryTreeList",params);
const queryListAsync = (params)=>getAction("/sys/permission/queryListAsync",params);
const queryRolePermission = (params)=>getAction("/sys/permission/queryRolePermission",params);
const saveRolePermission = (params)=>postAction("/sys/permission/saveRolePermission",params);
const queryPermissionsByUser = ()=>getAction("/sys/permission/getUserPermissionByToken");
const loadAllRoleIds = (params)=>getAction("/sys/permission/loadAllRoleIds",params);
const getPermissionRuleList = (params)=>getAction("/sys/permission/getPermRuleListByPermId",params);
const queryPermissionRule = (params)=>getAction("/sys/permission/queryPermissionRule",params);

// 部门管理
const queryDepartTreeList = (params)=>getAction("/sys/sysDepart/queryTreeList",params);
const queryDepartTreeSync = (params)=>getAction("/sys/sysDepart/queryDepartTreeSync",params);
const queryIdTree = (params)=>getAction("/sys/sysDepart/queryIdTree",params);
const queryParentName   = (params)=>getAction("/sys/sysDepart/queryParentName",params);
const searchByKeywords   = (params)=>getAction("/sys/sysDepart/searchBy",params);
const deleteByDepartId   = (params)=>deleteAction("/sys/sysDepart/delete",params);

//二级部门管理
const queryDepartPermission = (params)=>getAction("/sys/permission/queryDepartPermission",params);
const saveDepartPermission = (params)=>postAction("/sys/permission/saveDepartPermission",params);
const queryTreeListForDeptRole = (params)=>getAction("/sys/sysDepartPermission/queryTreeListForDeptRole",params);
const queryDeptRolePermission = (params)=>getAction("/sys/sysDepartPermission/queryDeptRolePermission",params);
const saveDeptRolePermission = (params)=>postAction("/sys/sysDepartPermission/saveDeptRolePermission",params);
const queryMyDepartTreeList = (params)=>getAction("/sys/sysDepart/queryMyDeptTreeList",params);

//日志管理
const deleteLog = (params)=>deleteAction("/sys/log/delete",params);
const deleteLogList = (params)=>deleteAction("/sys/log/deleteBatch",params);

//数据字典
const addDict = (params)=>postAction("/sys/dict/add",params);
const editDict = (params)=>putAction("/sys/dict/edit",params);
const treeList = (params)=>getAction("/sys/dict/treeList",params);
const addDictItem = (params)=>postAction("/sys/dictItem/add",params);
const editDictItem = (params)=>putAction("/sys/dictItem/edit",params);
const addDictItemGov = (params)=>postAction("/sys/dictItem/addGov",params);
const editDictItemGov = (params)=>putAction("/sys/dictItem/editGov",params);

//字典标签专用（通过code获取字典数组）
export const ajaxGetDictItems = (code, params)=>getAction(`/sys/dict/getDictItems/${code}`,params);
//从缓存中获取字典配置
function getDictItemsFromCache(dictCode) {
  if (Vue.ls.get(UI_CACHE_DB_DICT_DATA) && Vue.ls.get(UI_CACHE_DB_DICT_DATA)[dictCode]) {
    let dictItems = Vue.ls.get(UI_CACHE_DB_DICT_DATA)[dictCode];
    //console.log("-----------getDictItemsFromCache----------dictCode="+dictCode+"---- dictItems=",dictItems)
    return dictItems;
  }
}

//系统通告
const doReleaseData = (params)=>getAction("/sys/annountCement/doReleaseData",params);
const doReovkeData = (params)=>getAction("/sys/annountCement/doReovkeData",params);
//获取系统访问量
const getLoginfo = (params)=>getAction("/sys/loginfo",params);
const getVisitInfo = (params)=>getAction("/sys/visitInfo",params);

// 根据部门主键查询用户信息
const queryUserByDepId = (params)=>getAction("/sys/user/queryUserByDepId",params);

// 重复校验
const duplicateCheck = (params)=>getAction("/sys/duplicate/check",params);
// 加载分类字典
const loadCategoryData = (params)=>getAction("/sys/category/loadAllData",params);
const checkRuleByCode = (params) => getAction('/sys/checkRule/checkByCode', params)
//加载我的通告信息
const getUserNoticeInfo= (params)=>getAction("/sys/sysAnnouncementSend/getMyAnnouncementSend",params);
const getTransitURL = url => `/sys/common/transitRESTful?url=${encodeURIComponent(url)}`
// 中转HTTP请求
export const transitRESTful = {
  get: (url, parameter) => getAction(getTransitURL(url), parameter),
  post: (url, parameter) => postAction(getTransitURL(url), parameter),
  put: (url, parameter) => putAction(getTransitURL(url), parameter),
  http: (url, parameter) => httpAction(getTransitURL(url), parameter),
}

//获取专家列表
const get_experts_list = (params)=>getAction("/zjksb/zgsExpertinfo/list",params);
//获取专家数量类型倒序
const get_theNumber = (params)=>getAction("/zjksb/zgsExpertinfo/getTheNumberOfExpertsFor",params);
const get_applyYear = (params)=>getAction("/common/zgsProjectlibrary/queryApplyYear",params);


//成果公报
const get_cggb = (params)=>getAction("/jhxmcgjj/zgsPlanresultbase/list",params);
//最新公告
const get_zxgg = (params)=>getAction("/sys/annountCement/list",params);

//文件下载
const get_fileDownload =(params)=>getAction("/common/zgsIndexFile/list",params);
//我的工作台 - 个人
const get_personalWorkTableMap =(params)=>getAction("/worktable/index/personalWorkTableMap");
//我的工作台 - 推荐单位
const get_unitWorkTableMap = ()=>getAction("/worktable/index/unitWorkTableMap");
//我的工作台 - 省厅账户我
const get_govWorkTableMap = (params) =>getAction('/worktable/index/govWorkTableMap',params);
//获取项目任务书
const get_projectReportBookWithPdfFormateById = (params)=>getAction('/xmyssq/zgsScientificbase/queryById',params)

// logintype6工作台
const getEnergySavingGreenBuildingProjectData = (params)=>getAction('/worktable/index/energySavingGreenBuildingProjectData',params)
// 区县清单

const getListOfDistrictsAndCounties = (params)=>getAction('/worktable/index/listOfDistrictsAndCounties',params)

const getlistyq = (params)=>getAction('/common/zgsSciencetechtask/YqLqlist',params)

// const zjlist = (params)=>getAction('/zjksb/zgsExpertinfo/list',params)

const zjlist = (params)=>getAction('/zjksb/zgsExpertinfo/queryExpertListByType',params)

const yanzheng = (params)=>getAction('/zjksb/zgsExpertinfo/expertInfoValidation',params)

const savezhuanjialist = (params)=>postAction('/common/zgsProjectlibrary/add',params)

const getlist = (params)=>getAction('/common/zgsProjectlibrary/queryById',params)

const shifansearch = (params)=>getAction('/common/zgsProjectlibrary/queryDemoProjectList',params)

const keyansearch = (params)=>getAction('/common/zgsSciencetechfeasible/queryScientificProjectList',params)

const report = (params)=>putAction('/common/zgsProjecttask/generateDemoReport',params)

const getkeyan = (params)=>getAction('/common/zgsSciencetechfeasible/queryById',params)



const cgsave = (params)=>postAction('/jhxmcgjj/zgsPlanresultbase/add',params)

const kysave = (params)=>postAction('/xmyszssq/zgsSacceptcertificatebase/add',params)

const shsave = (params)=>postAction('/xmyszssq/zgsExamplecertificatebase/add',params)

const getcg = (params)=>getAction('/xmyszssq/zgsSacceptcertificatebase/queryById',params)

const getcgsh = (params)=>getAction('/xmyszssq/zgsExamplecertificatebase/queryById',params)

// 新增专家保存

const addinfo = (params)=>postAction('/xmyszssq/zgsSacceptcertexpert/addExperts',params)

const getinfo = (params)=>getAction('/xmyszssq/zgsSacceptcertexpert/queryExpertById',params)

// 上传文件后保存
const addfujian = (params)=>postAction('/common/zgsProjectlibrary/addExpertMatterial',params)

// 文件删除
const delfujian = (params)=>postAction('/common/zgsProjectlibrary/delExpertMatterial',params)


//工程项目
const sflist = (params)=>getAction('/common/zgsProjectlibrary/list',params)
// 科技项目
const kylist = (params)=>getAction('/common/zgsSciencetechfeasible/list',params)
// 任务书
const sftasklist = (params)=>getAction('/common/zgsProjecttask/list',params)
const kytasklist = (params)=>getAction('/common/zgsSciencetechtask/list',params)
// 中期查验
const zqlist = (params)=>getAction('/zqcysb/zgsMidterminspection/list',params)
// 项目变更
const bglist = (params)=>getAction('/jhxmbgsq/zgsPlannedprojectchange/list',params)
// 验收管理阶段
const sfyslist = (params)=>getAction('/xmyssq/zgsDemoprojectacceptance/list',params)
const kyyslist = (params)=>getAction('/xmyssq/zgsScientificbase/list',params)
const kyjtlist = (params)=>getAction('/kyxmjtsq/zgsScientificpostbase/list',params)
// 项目验收结题证书
const kyyscslist = (params)=>getAction('/xmyszssq/zgsSacceptcertificatebase/list',params)
const kyjtcslist = (params)=>getAction('/kyxmjtzs/zgsSpostcertificatebase/list',params)
const sfyscslist = (params)=>getAction('/xmyszssq/zgsExamplecertificatebase/list',params)
// 成果简介
const cglist = (params)=>getAction('/jhxmcgjj/zgsPlanresultbase/listSys',params)
// 绩效自评
const jxlist = (params)=>getAction('/jxzpsq/zgsPerformancebase/list',params)

// 工程编辑授权接口
const isEditApi = (params)=>postAction('/common/zgsProjectlibrary/accreditUpdate',params)

// 科研编辑授权接口
const isEditApi_ky = (params)=>postAction('/common/zgsSciencetechfeasible/accreditUpdate',params)

// 验收 工程

// 工程编辑授权接口
const isEditApi_ys = (params)=>postAction('/xmyssq/zgsDemoprojectacceptance/accreditUpdate',params)

// // 科研编辑授权接口
const isEditApi_kyys = (params)=>postAction('/xmyssq/zgsScientificbase/accreditUpdate ',params)

// // 科研结题编辑授权接口
const isEditApi_kyjt = (params)=>postAction('/kyxmjtsq/zgsScientificpostbase/accreditUpdate ',params)




// 绿色发展
const grbdAdd = (params)=>postAction('/lsfz/zgsGreenDevelop/add',params)
const grlist = (params)=>getAction('/lsfz/zgsGreenDevelop/list',params)
const getForm = (params)=>getAction('/lsfz/zgsGreenDevelop/queryById',params) 
const grbdEdit = (params)=>putAction('/lsfz/zgsGreenDevelop/edit',params)   
const grbdDel = (params)=>deleteAction('/lsfz/zgsGreenDevelop/delete',params) 
const grbdBatchDel = (params)=>deleteAction('/lsfz/zgsGreenDevelop/deleteBatch',params)
const grCs = (params)=>postAction('/lsfz/zgsGreenDevelop/spProject',params)

// swtich 开关
const sf_swtich = (params)=>postAction('/xmyszssq/zgsExamplecertificatebase/induce',params)
const ky_swtich = (params)=>postAction('/xmyszssq/zgsSacceptcertificatebase/induce',params)
const kyjt_swtich = (params)=>postAction('/kyxmjtzs/zgsSpostcertificatebase/induce',params)


// 总览导出
const exportExcel = (params)=>getAction('/common/zgsProjectlibrary/dataSummaryExport',params) 
const exportExcel_ky = (params)=>getAction('/common/zgsSciencetechfeasible/dataSummaryExport',params) 

const command_run = (params)=>postAction('/sys/command/run-command',params)

export {
  grbdAdd,grbdDel,grbdBatchDel,grCs,sf_swtich,ky_swtich,kyjt_swtich,exportExcel,exportExcel_ky,
  grlist,
  getForm,
  grbdEdit,
  sflist,
  kylist,
  sftasklist,
  kytasklist,
  zqlist,bglist,sfyslist,kyyslist,kyjtlist,kyyscslist,kyjtcslist,sfyscslist,cglist,jxlist,isEditApi,isEditApi_ky,isEditApi_ys,isEditApi_kyys,delfujian,
  isEditApi_kyjt,
  // imgView,
  // doMian,

  getListOfDistrictsAndCounties,
  getEnergySavingGreenBuildingProjectData,
  addRole,
  editRole,
  checkRoleCode,
  addUser,
  editUser,
  queryUserRole,
  getUserList,
  queryall,
  frozenBatch,
  checkOnlyUser,
  changePassword,
  getPermissionList,
  addPermission,
  editPermission,
  queryTreeList,
  queryListAsync,
  queryRolePermission,
  saveRolePermission,
  queryPermissionsByUser,
  loadAllRoleIds,
  getPermissionRuleList,
  queryPermissionRule,
  queryDepartTreeList,
  queryDepartTreeSync,
  queryIdTree,
  queryParentName,
  searchByKeywords,
  deleteByDepartId,
  deleteLog,
  deleteLogList,
  addDict,
  editDict,
  treeList,
  addDictItem,
  editDictItem,
  addDictItemGov,
  editDictItemGov,
  doReleaseData,
  doReovkeData,
  getLoginfo,
  getVisitInfo,
  queryUserByDepId,
  duplicateCheck,
  queryTreeListForRole,
  getSystemMenuList,
  getSystemSubmenu,
  getSystemSubmenuBatch,
  loadCategoryData,
  checkRuleByCode,
  queryDepartPermission,
  saveDepartPermission,
  queryTreeListForDeptRole,
  queryDeptRolePermission,
  saveDeptRolePermission,
  queryMyDepartTreeList,
  getUserNoticeInfo,
  getDictItemsFromCache,
  get_experts_list,
  getUserByToken,
  get_cggb,get_zxgg,get_fileDownload,
  get_personalWorkTableMap,
  get_unitWorkTableMap,
  get_projectReportBookWithPdfFormateById,
  get_govWorkTableMap,
  get_theNumber,
  get_applyYear,
  getlistyq,
  zjlist,
  yanzheng,
  savezhuanjialist,
  getlist,
  shifansearch,keyansearch,report,getkeyan,kysave,cgsave,shsave,getcg,getcgsh,addinfo,getinfo,addfujian,
  command_run
}


//import example code
//  import { duplicateCheck } from '@/api/api'


