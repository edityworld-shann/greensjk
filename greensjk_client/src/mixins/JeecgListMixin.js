/**
 * 新增修改完成调用 modalFormOk方法 编辑弹框组件ref定义为modalForm
 * 高级查询按钮调用 superQuery方法  高级查询组件ref定义为superQueryModal
 * data中url定义 list为查询列表  delete为删除单条记录  deleteBatch为批量删除
 */
import { filterObj } from '@/utils/util';
import { deleteAction, getAction, downFile, getFileAccessHttpUrl } from '@/api/manage'
import Vue from 'vue'
import { ACCESS_TOKEN, TENANT_ID } from "@/store/mutation-types"
import store from '@/store'

import appconf from '../config/appconf'
import moment from 'moment'
let docx = require('docx-preview');
export const JeecgListMixin = {
  data() {
    return {
      docShowPreview: false,
      projectYears: appconf.projectYears,
      /* 查询条件-请不要在queryParam中声明非字符串值的属性 */
      queryParam: {
      },
      /* 数据源 */
      dataSource: [],
      /* 分页参数 */
      ipagination: {
        current: 1,
        pageSize: 10,
        pageSizeOptions: ['10', '20', '30', '50', '100'],
        showTotal: (total, range) => {
          return range[0] + "-" + range[1] + " 共" + total + "条"
        },
        showQuickJumper: true,
        showSizeChanger: true,
        total: 0
      },
      /* 排序参数 */
      isorter: {
        // column: 'createTime',
        // order: 'desc',
      },
      /* 筛选参数 */
      filters: {},
      /* table加载状态 */
      loading: false,
      /* table选中keys*/
      selectedRowKeys: [],
      /* table选中records*/
      selectionRows: [],
      /* 查询折叠 */
      toggleSearchStatus: false,
      /* 高级查询条件生效状态 */
      superQueryFlag: false,
      /* 高级查询条件 */
      superQueryParams: '',
      /** 高级查询拼接方式 */
      superQueryMatchType: 'and',


      /** code bellow  add by zhangchengguo **/
      disableReportDataAddButtonAction: undefined,  //数据上报模块&&当月数据是否已上报
      url: {
        queryLatestInfo: "/report/zgsReportMonthgreenbuildArea/queryLatestInfo",
      }

    }
  },
  created() {
    if (this.doBeforeCreated != undefined) {
      this.doBeforeCreated();
    }

    if (!this.disableMixinCreated) {
      this.loadData();
      //初始化字典配置 在自己页面定义
      this.initDictConfig();
    }
  },
  // mounted: function () {
  //   this.$nextTick(function () {
  //     // 仅在整个视图都被渲染之后才会运行的代码
  //     if(!this.disableMixinCreated){
  //       this.loadData();
  //       //初始化字典配置 在自己页面定义
  //       this.initDictConfig();
  //     }
  //   })
  // },
  deactivated() {
    if (sessionStorage.getItem('projectStatus')) {
      // sessionStorage.removeItem('projectStatus')
    }
    if (sessionStorage.getItem('dealtype')) {
      // sessionStorage.removeItem('dealtype')
    }
  },
  computed: {
    //token header
    tokenHeader() {
      let head = { 'X-Access-Token': Vue.ls.get(ACCESS_TOKEN) }
      let tenantid = Vue.ls.get(TENANT_ID)
      if (tenantid) {
        head['tenant-id'] = tenantid
      }
      return head;
    }
  },
  methods: {
    queryList() {
      sessionStorage.removeItem('projectStatus')
      sessionStorage.removeItem('dealtype')
      this.loadData()
    },

    /**
     * @author zhangchengguo / 2022年4月16日
     * @description 进入下一步操作之前根据ID查询数据详情
     * */
    queryDetialByIdBeforeNextAction(record, action) {
      if (this.url.queryById == undefined) {
        this.$message.warning("详情服务访问地址不正确!");
        return;
      }
      getAction(this.url.queryById, { id: record.id }).then(res => {
        console.log("res = ", res);
        if (action == 'handleDetail') {
          console.log("queryDetialByIdBeforeNextAction && nextaction  = handleDetail");
          this.handleDetail(res.result);
        }
      })
    },




    /**
     * @author zhangchengguo / 2022年3月31日10:21:35
     * @description 用来测试框架代码执行逻辑的函数
     * */
    jm_sayHi() {
      // alert("hell");
    },

    /**
     * @param arg 表示当前list查询参数中的 pageIndex （页码)
     * */
    loadData(arg) {
      console.log("JeecgListMixin.js && loadData(arg) = ", arg);

      if (!this.url.list) {
        this.$message.error("请设置url.list属性!(3)")
        return
      }
      //加载数据 若传入参数1则加载第一页的内容
      if (arg === 1) {
        this.ipagination.current = 1;
      }
      let params = this.getQueryParams();//
      this.loading = true;
      if (sessionStorage.getItem('projectStatus') == '待初审') {
        params.status = 1
        params.dspForTjdw = 3
      }
      if (sessionStorage.getItem('projectStatus') == '项目统计') {
        params.flagByWorkTable = 1
        // params.status = 1
      }
      if (sessionStorage.getItem('projectStatus') == '待补充') {
        params.status = -1
      }
      if (sessionStorage.getItem('projectStatus') == '待审批') {
        params.status = 4
      }
      if (sessionStorage.getItem('dealtype') == '逾期') {
        params.dealtype = 0
      }
      if (sessionStorage.getItem('dealtype') == '临期') {
        params.dealtype = 2
      }
      // sessionStorage.removeItem('projectStatus')
      // sessionStorage.removeItem('dealtype')
      /**
       * @author zhangchengguo 2022年4月16日
       * @description 提交请求之前，主要对日期参数格式化，明确点说就是格式化 reporttm(报表时间）这个时间，格式为 YYYY-MM
       * */
      if (this.queryParamCallback != undefined) {
        console.log("this.queryParamAction != undefined");
        this.queryParamCallback(params);
      }
      console.log("getAction && url.list = ", this.url, " && params = ", params);
     
      getAction(this.url.list, params).then((res) => {
        // console.log('列表数据',res);
        if (res.success) {
          //update-begin---author:zhangyafei    Date:20201118  for：适配不分页的数据列表------------
          let _dataSource = res.result.records || res.result;

          if (sessionStorage.getItem('projectStatus') == '待初审') {
            sessionStorage.setItem('projectStatus','待初审1')
            params.status = 1
            params.dspForTjdw =''
          }
          if (sessionStorage.getItem('projectStatus') == '项目统计') {
            sessionStorage.setItem('projectStatus','项目统计1')
            params.flagByWorkTable = ''
            // params.status = 1
          }
          /**
           * @description 列表请求结果的自定义解析函数 && zhangchengguo@2022年4月7日
           * */
          if (this.dataSourceAction != undefined) {
            console.log("this.dataSourceAction != undefined && start to excute dataSourceAction Function && _dataSource = ", _dataSource);
            console.log("typeof dataSource = ", (typeof _dataSource));
            _dataSource = this.dataSourceAction(_dataSource);
            
          } else {
            console.log("this.dataSourceAction == undefined ");
          }

          this.dataSource = _dataSource;
          console.log( this.dataSource , '这是页面进来请求的数据');
          //重置分页分数
          if (res.result.total) {
            this.ipagination.total = res.result.total;
          } else {
            this.ipagination.total = 0;
          }
          //update-end---author:zhangyafei    Date:20201118  for：适配不分页的数据列表------------

          //
          if (this.queryLatestInfo != undefined && _dataSource != undefined && (typeof _dataSource == 'object') && _dataSource.length == 0) {
            console.log("申报数据 && 需要查询最新有数据的日期")
            this.queryLatestInfo();
          }
        } else {
          this.$message.warning(res.message);
        }
      }).finally(() => {
        this.loading = false;
        // let recordNum = this.dataSource.length;
        // console.log("finally && this.dataSource = ",this.dataSource);
        // if(recordNum==0 && this.queryLatestInfo!=undefined){
        //   console.log("no data && get latest reporttm");
        //   this.queryLatestInfo();
        // }
      })
    },
    initDictConfig() {
      console.log("--这是一个假的方法!")
    },
    handleSuperQuery(params, matchType) {
      //高级查询方法
      if (!params) {
        this.superQueryParams = ''
        this.superQueryFlag = false
      } else {
        this.superQueryFlag = true
        this.superQueryParams = JSON.stringify(params)
        this.superQueryMatchType = matchType
      }
      this.loadData(1)
    },
    getQueryParams() {
      //获取查询条件
      let sqp = {}
      if (this.superQueryParams) {
        sqp['superQueryParams'] = encodeURI(this.superQueryParams)
        sqp['superQueryMatchType'] = this.superQueryMatchType
      }
      var param = Object.assign(sqp, this.queryParam, this.isorter, this.filters);
      param.field = this.getQueryField();
      param.pageNo = this.ipagination.current;
      param.pageSize = this.ipagination.pageSize;
      // param.projectstage =1 ;
      return filterObj(param);
    },
    getQueryField() {
      //TODO 字段权限控制
      var str = "id,";
      this.columns.forEach(function (value) {
        str += "," + value.dataIndex;
      });
      return str;
    },

    onSelectChange(selectedRowKeys, selectionRows) {
      this.selectedRowKeys = selectedRowKeys;
      this.selectionRows = selectionRows;
    },
    onClearSelected() {
      this.selectedRowKeys = [];
      this.selectionRows = [];
    },
    searchQuery() {
      console.log("JeecpListMixin.js && searchQuery");
      sessionStorage.removeItem('projectStatus')
      sessionStorage.removeItem('dealtype')
      // console.log(11111);
      this.loadData(1);
      // 点击查询清空列表选中行
      // https://gitee.com/jeecg/jeecg-boot/issues/I4KTU1
      this.selectedRowKeys = []
      this.selectionRows = []
    },
    superQuery() {
      this.$refs.superQueryModal.show();
    },
    searchReset() {
      this.queryParam = {}
      this.loadData(1);
    },
    batchDel: function () {
      if (!this.url.deleteBatch) {
        this.$message.error("请设置url.deleteBatch属性!")
        return
      }
      if (this.selectedRowKeys.length <= 0) {
        this.$message.warning('请选择一条记录！');
        return;
      } else {
        var ids = "";
        for (var a = 0; a < this.selectedRowKeys.length; a++) {
          ids += this.selectedRowKeys[a] + ",";
        }
        var that = this;
        this.$confirm({
          title: "确认删除",
          content: "是否删除选中数据?",
          onOk: function () {
            that.loading = true;
            deleteAction(that.url.deleteBatch, { ids: ids }).then((res) => {
              if (res.success) {
                //重新计算分页问题
                that.reCalculatePage(that.selectedRowKeys.length)
                that.$message.success(res.message);
                that.loadData();
                that.onClearSelected();
              } else {
                that.$message.warning(res.message);
              }
            }).finally(() => {
              that.loading = false;
            });
          }
        });
      }
    },
    handleDelete: function (id) {
      if (!this.url.delete) {
        this.$message.error("请设置url.delete属性!")
        return
      }
      var that = this;
      deleteAction(that.url.delete, { id: id }).then((res) => {
        if (res.success) {
          //重新计算分页问题
          that.reCalculatePage(1)
          that.$message.success(res.message);
          that.loadData();
        } else {
          that.$message.warning(res.message);
        }
      });
    },
    reCalculatePage(count) {
      //总数量-count
      let total = this.ipagination.total - count;
      //获取删除后的分页数
      let currentIndex = Math.ceil(total / this.ipagination.pageSize);
      //删除后的分页数<所在当前页
      if (currentIndex < this.ipagination.current) {
        this.ipagination.current = currentIndex;
      }
      console.log('currentIndex', currentIndex)
    },
    handleEdit: function (record) {
      
      record.type = 2
      console.log(record,'6666');
      this.$refs.modalForm.edit(record);
      this.$refs.modalForm.title = "编辑";
      this.$refs.modalForm.disableSubmit = false;
    },

    /**
     * @description 列表页面的添加函数
     * */
    handleAdd: function () {
      this.$refs.modalForm.add();
      this.$refs.modalForm.title = "新增";
      this.$refs.modalForm.disableSubmit = false;
    },
    handleTableChange(pagination, filters, sorter) {
      // alert('1');
      //分页、排序、筛选变化时触发
      //TODO 筛选
      console.log(pagination)
      if (Object.keys(sorter).length > 0) {
        this.isorter.column = sorter.field;
        this.isorter.order = "ascend" == sorter.order ? "asc" : "desc"
      }
      this.ipagination = pagination;
      this.loadData();
    },
    handleToggleSearch() {
      this.toggleSearchStatus = !this.toggleSearchStatus;
    },
    // 给popup查询使用(查询区域不支持回填多个字段，限制只返回一个字段)
    getPopupField(fields) {
      return fields.split(',')[0]
    },
    modalFormOk() {
      console.log('刷新');
      // 新增/修改 成功时，重载列表
      this.loadData()
      //清空列表选中
      this.onClearSelected()
      this.$refs.modalForm.visible = false

    },
    handleDetail: function (record) {
      record.type = 3
      console.log(record,'6666');
      this.$refs.modalForm.edit(record);
    
      this.$refs.modalForm.title = "详情";
      this.$refs.modalForm.disableSubmit = true;
    },

    handleDetailWithPdf: function (record, pdf) {
      // const docxUrl = pdf.replace('.pdf', '.doc');
      // console.log(docxUrl,'pdf',record);
      this.$refs.modalForm.editWithPdf(record,pdf);
      this.$refs.modalForm.title = "详情";
      this.$refs.modalForm.disableSubmit = true;
      this.$refs.modalForm.pdf = pdf;
    },


    /* 导出 */
    handleExportXls2() {
      let paramsStr = encodeURI(JSON.stringify(this.getQueryParams()));
      let url = `${window._CONFIG['domianURL']}/${this.url.exportXlsUrl}?paramsStr=${paramsStr}`;
      window.location.href = url;
    },
    handleExportXls(fileName) {
      if (!fileName || typeof fileName != "string") {
        fileName = "导出文件"
      }
      let param = this.getQueryParams();
      if (sessionStorage.getItem('projectStatus') == '待初审') {
        param.status = 0
      }
      if (sessionStorage.getItem('projectStatus') == '待补充') {
        param.status = -1
      }
      if (sessionStorage.getItem('projectStatus') == '待审批') {
        param.status = 0
      }
      if (sessionStorage.getItem('dealtype') == '逾期') {
        param.dealtype = 0
      }
      if (sessionStorage.getItem('dealtype') == '临期') {
        param.dealtype = 2
      }
      if (this.selectedRowKeys && this.selectedRowKeys.length > 0) {
        param['selections'] = this.selectedRowKeys.join(",")
      }
      console.log("导出参数", param)
      downFile(this.url.exportXlsUrl, param).then((data) => {
        if (!data) {
          this.$message.warning("文件下载失败")
          return
        }
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(new Blob([data], { type: 'application/vnd.ms-excel' }), fileName + '.xls')
        } else {
          let url = window.URL.createObjectURL(new Blob([data], { type: 'application/vnd.ms-excel' }))
          let link = document.createElement('a')
          link.style.display = 'none'
          link.href = url
          link.setAttribute('download', fileName + '.xls')
          document.body.appendChild(link)
          link.click()
          document.body.removeChild(link); //下载完成移除元素
          window.URL.revokeObjectURL(url); //释放掉blob对象
        }
      })
    },
    handleExportXlsInner(fileName, url) {
      if (!fileName || typeof fileName != "string") {
        fileName = "导出文件"
      }
      let param = this.getQueryParams();
      if (this.selectedRowKeys && this.selectedRowKeys.length > 0) {
        param['selections'] = this.selectedRowKeys.join(",")
      }
      console.log("导出参数", param)
      downFile(url, param).then((data) => {
        if (!data) {
          this.$message.warning("文件下载失败")
          return
        }
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(new Blob([data], { type: 'application/vnd.ms-excel' }), fileName + '.xls')
        } else {
          let url = window.URL.createObjectURL(new Blob([data], { type: 'application/vnd.ms-excel' }))
          let link = document.createElement('a')
          link.style.display = 'none'
          link.href = url
          link.setAttribute('download', fileName + '.xls')
          document.body.appendChild(link)
          link.click()
          document.body.removeChild(link); //下载完成移除元素
          window.URL.revokeObjectURL(url); //释放掉blob对象
        }
      })
    },
    handleExportDoc(fileName) {
      this.docShowPreview = false
      if (!fileName || typeof fileName != "string") {
        fileName = "导出文件"
      }
      let param = this.getQueryParams();
      console.log("导出参数", param)
      downFile(this.url.exportDocUrl, param).then((data) => {
        if (!data) {
          this.$message.warning("文件下载失败")
          return
        }
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(new Blob([data], { type: 'application/msword' }), fileName + '.doc')
        } else {
          let url = window.URL.createObjectURL(new Blob([data], { type: 'application/msword' }))
          let link = document.createElement('a')
          link.style.display = 'none'
          link.href = url
          link.setAttribute('download', fileName + '.doc')
          document.body.appendChild(link)
          link.click()
          document.body.removeChild(link); //下载完成移除元素
          window.URL.revokeObjectURL(url); //释放掉blob对象

          // this.docShowPreview = true
          // docx.renderAsync(data,document.getElementById('DocShowPreview'))
        }
      })
    },
    printerExportXls() {
      // var head_str = "<html><head><title></title></head><body>"; //先生成头部
      // var foot_str = "</body></html>"; //生成尾部
      // var older = document.body.innerHTML;
      // var new_str = document.getElementById('DocShowPreview').innerHTML;//获取指定打印区域
      // var old_str = document.body.innerHTML; //获得原本页面的代码
      // document.body.innerHTML = head_str + new_str + foot_str; //构建新网页
      // window.print(); //打印刚才新建的网页
      // // document.body.innerHTML = older; //将网页还原
      // return false;
      var newWindow = window.open("打印窗口", "_blank");
      var head_str = "<html><head><title>打印窗口</title></head><body>"; //先生成头部
      var foot_str = "</body></html>"; //生成尾部
      var docStr = document.getElementById('DocShowPreview').innerHTML;//获取指定打印区域

      newWindow.document.write(head_str + docStr + foot_str);

      newWindow.document.close();

      newWindow.print();

      newWindow.close();
    },
    closeDocShowPreview() {
      this.docShowPreview = false
      document.getElementById('DocShowPreview').innerHTML = ''
    },
    /* 导入 */
    handleImportExcel(info) {
      this.loading = true;
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === 'done') {
        this.loading = false;
        if (info.file.response.success) {
          // this.$message.success(`${info.file.name} 文件上传成功`);
          if (info.file.response.code === 201) {
            let { message, result: { msg, fileUrl, fileName } } = info.file.response
            let href = window._CONFIG['domianURL'] + fileUrl
            this.$warning({
              title: message,
              content: (<div>
                <span>{msg}</span><br />
                <span>具体详情请 <a href={href} target="_blank" download={fileName}>点击下载</a> </span>
              </div>
              )
            })
          } else {
            this.$message.success(info.file.response.message || `${info.file.name} 文件上传成功`)
          }
          this.loadData()
        } else {
          this.$message.error(`${info.file.name} ${info.file.response.message}.`);
        }
      } else if (info.file.status === 'error') {
        this.loading = false;
        if (info.file.response.status === 500) {
          let data = info.file.response
          const token = Vue.ls.get(ACCESS_TOKEN)
          if (token && data.message.includes("Token失效")) {
            this.$error({
              title: '登录已过期',
              content: '很抱歉，登录已过期，请重新登录',
              okText: '重新登录',
              mask: false,
              onOk: () => {
                store.dispatch('Logout').then(() => {
                  Vue.ls.remove(ACCESS_TOKEN)
                  window.location.reload();
                })
              }
            })
          }
        } else {
          this.$message.error(`文件上传失败: ${info.file.msg} `);
        }
      }
    },
    /* 图片预览 */
    getImgView(text) {
      if (text && text.indexOf(",") > 0) {
        text = text.substring(0, text.indexOf(","))
      }
      return getFileAccessHttpUrl(text)
    },
    /* 文件下载 */
    // update--autor:lvdandan-----date:20200630------for：修改下载文件方法名uploadFile改为downloadFile------
    downloadFile(text) {
      if (!text) {
        this.$message.warning("未知的文件")
        return;
      }
      if (text.indexOf(",") > 0) {
        text = text.substring(0, text.indexOf(","))
      }
      let url = getFileAccessHttpUrl(text)
      window.open(url);
    },



    //********************************************************************************************************************
    //code bellow write by zhangchengguo && start at 2022年4月29日
    //对表求和的结果进行重构，主要是防止小数点超过5位
    refactorTableSumValue(object, filterKeys = []) {

      Object.keys(object).forEach(key => {
        if (!filterKeys.includes(key)) {
          let value = object[key] + "";
          //小数点超过5位后才对小数点进行处理 并精确到5位小数
          if (value != undefined && value.indexOf(".") > -1 && value.substring(value.indexOf(".")).length > 6) {
            object[key] = parseFloat(value).toFixed(6);
          }
        }
      })
    },

    //自定义table排序
    handleTableSorter(pagination, filters, sorter) {
      let sorter_field = sorter.field;
      console.log(sorter)
      this.queryParam.wcArrow = undefined;
      this.queryParam.sbArrow = undefined;
      this.queryParam.csArrow=undefined;
      //完成时间
      if(sorter_field=="completedate"||sorter_field=="enddate" ){

        let order = sorter.order;
        console.log("order = ", order);
        if (order == undefined) {
          this.queryParam.wcArrow = undefined;
        } else if (order == 'ascend') {
          this.queryParam.wcArrow = 0;//升序
        } else if (order == 'descend') {
          this.queryParam.wcArrow = 1;//降序
        }
      }
      //初审日期
      if(sorter_field=="firstdate"){
        let order = sorter.order;
        console.log("order = ", order);
        if (order == undefined) {
          this.queryParam.sbArrow = undefined;
        } else if (order == 'ascend') {
          this.queryParam.csArrow = 0;//升序
        } else if (order == 'descend') {
          this.queryParam.csArrow = 1;//降序
        }
      }
      //申报日期
      if (sorter_field == 'applydate') {
        let order = sorter.order;
        console.log("order = ", order);
        if (order == undefined) {
          this.queryParam.sbArrow = undefined;
        } else if (order == 'ascend') {
          this.queryParam.sbArrow = 0;//升序
        } else if (order == 'descend') {
          this.queryParam.sbArrow = 1;//降序
        }
      }

      //项目编号
      if (sorter_field == 'projectnum' || sorter_field == 'projectnumber') {
        let order = sorter.order;
        console.log("order = ", order);
        if (order == undefined) {
          this.queryParam.numArrow = undefined;
        } else if (order == 'ascend') {
          this.queryParam.numArrow = 0;//升序
        } else if (order == 'descend') {
          this.queryParam.numArrow = 1;//降序
        }
      }
      //执行重新检索的函数
      this.ipagination = pagination;
      this.loadData();
    },



    //设置审批菜单选项
    getSPOptions(loginUserType) {
      let hash = window.location.hash;
      let lastPath = hash.substring(hash.lastIndexOf('/') + 1);

      let selectOptions1 = [
        { label: '通过', value: 1 },
        { label: '驳回', value: 0 },
      ];
      let selectOptions2 = [
        { label: '通过', value: 1 },
        { label: '补正修改', value: 2 },
        { label: '驳回', value: 0 },
      ];

      let projectSBRouters = [
        'ZgsProjectlibraryList1',
        'ZgsProjectlibraryList2',
        'ZgsProjectlibraryList3',
        'ZgsProjectlibraryList4',
        'ZgsProjectlibraryList5',
        'ZgsSciencetechfeasibleList1',
        'ZgsSciencetechfeasibleList2',];

      if (loginUserType == 3 && projectSBRouters.includes(lastPath)) {
        //省厅用户 && 申报阶段
        return selectOptions1;
      }
      //其他
      return selectOptions2;
    },

    getTipVisible() {
      let hash = window.location.hash;
      let lastPath = hash.substring(hash.lastIndexOf('/') + 1);
      let lsKey = lastPath + "_" + moment(new Date()).format("YYYYMMDD");
      if (localStorage.getItem(lsKey) == undefined) {
        localStorage.setItem(lsKey, '1');
        return true;
      } else {
        return false;
      }
    }









  },
  // watch: {
  //   url: {handler: function (val, oldVal) {
  //     if (val&&val.list) {
  //       console.log(1111111111111111)
  //       console.log(val)
  //       if(!this.disableMixinCreated){
  //         this.loadData();
  //       }
  //     }
  //   },
  //   immediate: true}

  // }


}