import moment from 'moment'

/**
 * @author zhangchengguo / 2022年4月11日21:29:17
 * @description 主要是为了增强Form表单的功能
 */

export const JeecgFormMixin={

  data(){
    return {
      linkageDisabled:true,
    }
  },

  methods:{
    /**
     * 嵌套对象校验器
     * */
    nestedObjectValidator(rule,value,callback){
      console.log(rule);
      let field = rule.field;
      let nestedObjectName = rule.nestedObjectName;
      let nestedObject = this.model[nestedObjectName];
      let field_value = nestedObject[field];
      if(nestedObject==undefined){
        console.log("nestedObject = ",nestedObject," && field_value = ",field_value);
        callback('nestedObject==undefined');
      }else if(nestedObject[rule.field]==undefined || nestedObject[rule.field]=='' ||nestedObject[rule.field].length==0){
        console.log("nestedObject = ",nestedObject," && field_value = ",field_value);
        callback('nestedObject[rule.field]==undefined || nestedObject[rule.field]==||nestedObject[rule.field].length==0');
      }else {
        callback();
      }
    },

    /**
     * 经费预算中的年费
     * */
    initSelectedYears(){
      let _years = new Array();
      let _year = moment(new Date()).format('yyyy')
      let int_year = parseInt(_year);
      for(let i=0;i<10;i++){
        let item = int_year+i;
        _years.push({
          label:item+'年',
          value:item
        });
      }
      return  _years.reverse()
    }

  }



}