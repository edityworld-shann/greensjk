import Vue from 'vue'

/**
 * 存储App常用变量
 * */

const getTaskBookButtonVisible = function() {
  let loginUserType =localStorage.getItem("loginUserType");
  //<!-- loginUserType登录账号类型：0单位，1个人，2专家库，3住建厅-->
  if(loginUserType!=undefined && loginUserType==3){
    return  true;
  }
  return false;
}

export default {
  AC_USER_LEVEL:"AC_USER_LEVEL",
  AC_USER_LEVEL_ViewTaskBook:1,//能够查看任务报告书的的角色
  getTaskBookButtonVisible
}