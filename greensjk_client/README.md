Ant Design Jeecg Vue
====

当前最新版本： 3.0.0（发布日期：2021-11-01）

Overview
----

基于 [Ant Design of Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/) 实现的 Ant Design Pro  Vue 版
Jeecg-boot 的前端UI框架，采用前后端分离方案，提供强大代码生成器的低代码平台。
前端页面代码和后端功能代码一键生成，不需要写任何代码，保持jeecg一贯的强大！！



#### 前端技术
 
- 基础框架：[ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现
- JavaScript框架：Vue
- Webpack
- node
- yarn
- eslint
- @vue/cli 3.2.1
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现



项目下载和运行
----

- 拉取项目代码
```bash
git clone https://github.com/zhangdaiscott/jeecg-boot.git
cd  jeecg-boot/ant-design-vue-jeecg
```

- 安装依赖
```
yarn install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```



其他说明
----

- 项目使用的 [vue-cli3](https://cli.vuejs.org/guide/), 请更新您的 cli

- 关闭 Eslint (不推荐) 移除 `package.json` 中 `eslintConfig` 整个节点代码

- 修改 Ant Design 配色，在文件 `vue.config.js` 中，其他 less 变量覆盖参考 [ant design](https://ant.design/docs/react/customize-theme-cn) 官方说明
```ecmascript 6
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          /* less 变量覆盖，用于自定义 ant design 主题 */

          'primary-color': '#F5222D',
          'link-color': '#F5222D',
          'border-radius-base': '4px',
        },
        javascriptEnabled: true,
      }
    }
  }
```



附属文档
----
- [Ant Design Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn)

- [报表 viser-vue](https://viserjs.github.io/demo.html#/viser/bar/basic-bar)

- [Vue](https://cn.vuejs.org/v2/guide)

- [路由/菜单说明](https://github.com/zhangdaiscott/jeecg-boot/tree/master/ant-design-vue-jeecg/src/router/README.md)

- [ANTD 默认配置项](https://github.com/zhangdaiscott/jeecg-boot/tree/master/ant-design-vue-jeecg/src/defaultSettings.js)

- 其他待补充...


备注
----

> @vue/cli 升级后，eslint 规则更新了。由于影响到全部 .vue 文件，需要逐个验证。既暂时关闭部分原本不验证的规则，后期维护时，在逐步修正这些 rules


Docker 镜像使用
----

 ``` 
# 1.修改前端项目的后台域名
    .env.development
    域名改成： http://jeecg-boot-system:8080/jeecg-boot
   
# 2.先进入打包前端项目
  yarn run build

# 3.构建镜像
  docker build -t nginx:jeecgboot .

# 4.启动镜像
  docker run --name jeecg-boot-nginx -p 80:80 -d nginx:jeecgboot

# 5.配置host

    # jeecgboot
    127.0.0.1   jeecg-boot-redis
    127.0.0.1   jeecg-boot-mysql
    127.0.0.1   jeecg-boot-system
  
# 6.访问前台项目
  http://localhost:80
``` 


# svn test @ zcg

# 测试账号
```angular2html
http://61.178.32.163:84/GSJZJGTechProject/jskjxm.aspx

登录账号类型：0单位，1个人，2专家库，3住建厅（省），4推荐单位，6县区，7市区

省厅账号 gov 密码 Greensjk@835290
个人账号 lilingjun654	密码 Nwhxn!978253    上级推荐单位=天水师范学院
企业账号 天水师范学院	密码	Ndnlm!655994
上级推荐单位   账号：天水师范学院建设科技项目初审   密码：Z4d#TI1H%EEc
专家账号 陈斌	密码 Gbkxa!339423
admin管理员权限,密码Greensjk!386490
green:taskScreen
甘肃六建集团 332211

```
#账号类型
```
登录账号类型：0单位，1个人，2专家库，3住建厅
```
#每个页面按钮显示隐藏写法
```
编辑 	green:edit  
删除 	green:delete 
初审 	green:startsp
终审 	green:endsp
专家分配green:chouqu
专家审批green:expertsp
新增 	green:add
导出 	green:export
导入 	green:import
示例如下
<a-button v-has="'green:add'" @click="handleAdd(1)" icon="plus" class="cus-bt-success">新增</a-button>
v-has="'green:add'"控制新增按钮
v-has="'green:edit'"控制编辑按钮
以此类推，若有新增的类型按钮会及时更新
```

# 专家评审判断逻辑
```markdown
评审这块
初审通过 才进行下一步的终审
终审通过后 才进行专家评审是不？

判断逻辑：
如果
status等于2，初审结果和终审结果都显示-通过；(初审=通过、终审=通过）
status等于4，只有初审结果显示-通过；(初审=通过、终审=未知结果）
status等于3，只有初审显示-初审退回；(初审=退回、终审=未执行）
status等于5，初审显示-通过，终审显示-终审退回(初审=通过、终审=退回）

```
#审批状态status
```
审核状态：0未上报，1待审核，4初审通过，3初审退回，2终审通过，5终审退回给初审，6专家驳回

```

# 示例代码
```markdown
@import '~@assets/less/app.less';

时间控制
1、引入类库
import moment from 'moment'
2、时间控制
<a-form-model-item label="截止日期" :labelCol="labelCol" :wrapperCol="wrapperCol" prop="enddate">
  <j-date  :disabled-date="disabledEndDate" :disabled="formDisabled"  placeholder="请选择截止日期" v-model="model.enddate"  style="width: 100%" />
</a-form-model-item>
3、时间控制
disabledEndDate(current) {
      let startTime = this.model.startdate;
      let maxEndDate = moment(startTime).add(2, 'years')
      return current > maxEndDate;
},


:footer="disableSubmit?null:undefined"
```

```表格自动控制
  class="j-table-force-nowrap-noColAuto"
```

```非编辑状态下的输入框
deep/ .ant-input-number-input:disabled{
  color: gray !important;
}

/deep/ .ant-input:disabled{
  color: gray !important;
}

```

# 数据上报说明
1.	县级账号
    逻辑关系：各县填报数据，上报到市级，当新增数据上报之后，隐藏新增按钮，也就是说县级每个月只有一次新增机会，如果想改这个月的数据，需要在操作栏内的编辑里修改。
    状态：保存时，状态为“未上报”，按钮为蓝色。点击了上报按钮之后，状态为“已上报”按钮为灰色。被退回时，状态为“未上报”，按钮变为“再次上报”，为蓝色。编辑按钮只在未上报是出现，上报之后不可编辑，编辑按钮置灰或取消均可。
    按钮：操作栏：“上报”“编辑”“再次上报”。左上方：“新增”“查询”（查询的是月份）

2.	市级账号
    逻辑关系：各县填报数据汇总，如果没有问题，上报到省级，如有问题，退回到当前县级。
    如果县级未上报，显示数据全部为0。市级账号没有新增按钮。
    状态：汇总之后未上报时，状态为“未上报”，上报按钮为蓝色。点击上报按钮之后，状态为“已上报”，上报按钮为灰色。被退回时，状态为“未上报”，按钮变为“再次上报”，为蓝色，市级汇总数据后面才有上报按钮，显示操作栏是退回按钮。县级栏的操作栏：“退回”。已上报时，退回按钮蓝色，可点击，状态为“已上报”。未上报内容时状态是“未上报”，操作栏无按钮。已退回时状态是“已退回”，按钮为“退回”，置灰。
    按钮：操作栏：“上报”“再次上报”“退回”。左上方：“查询”（查询的是月份）

3.	省级账号
    逻辑关系：只是查看数据汇总，如果市级的数据有问题，退回之后，该市级数据全部清0，0的状态代表该市级未上报数据，状态为“已退回”。点击市级操作栏的“退回”按钮时，状态是已退回，按钮置灰。未上报上来数据时，市级操作栏无按钮，状态为“未上报”。上报上来之后，按钮为“退回”，蓝色，可点击，状态为“已上报”
    按钮：操作栏：“退回”。左上方：“查询”（查询的是月份）


> 整体逻辑关系：县级上报，市级汇总再次上报给省级，省级查看汇总数据。市级需要计算汇总数据，建议每一个县上报上来一次，计算一下和，生成市级汇总数据。省级同理。


# 添加查看任务书按钮

范围：项目验收管理 ~ 绩效自评


0.
       <!-- 查看任务书 @ 2022年3月18日 -->
       <a v-if="loginUserType!=undefined && parseInt(loginUserType)==3" class="cus-table-edit-bt-primary"
          @click="handleDetailWithPdf(record,'http://39.106.197.23:9090/doc/%E4%B8%93%E5%AE%B6%E5%BA%93%E9%A1%B5%E9%9D%A2%E4%BF%AE%E6%94%B9.pdf')"
          style="margin-right: 5px;">查看任务书</a>

1.

    <!-- 添加查看任务书 Start -->
    <zgs-midterminspection-form v-if="pdf==undefined" ref="realForm" @ok="submitCallback" :disabled="disableSubmit"></zgs-midterminspection-form>
    <div v-else style="background-color: transparent; width: 100%;height: 100%; border: solid 1px lightgray">
      <split-pane :min-percent='0' :default-percent='40' split="vertical">
        <template slot="paneL">
          <iframe border="0" style="width: 100%; height: 100%;" src="http://39.106.197.23:9090/doc/专家库页面修改.pdf#view=FitH,top"></iframe>
        </template>
        <template slot="paneR">
          <div style="height: 100%;overflow-y: scroll">
            <zgs-midterminspection-form ref="realForm" @ok="submitCallback" :disabled="disableSubmit"></zgs-midterminspection-form>
          </div>
        </template>
      </split-pane>
    </div>
    <!-- 添加查看任务书 End -->
2.
data(){
  pdf:undefined,
}

3.
watch:{
  visible(val){
    if(val==false){
      this.pdf=undefined;
    }
  }
},

4.methods

    editWithPdf(record,pdf) {
      this.pdf = pdf;
      this.edit(record);
    },


# 控制模态框高度
```markdown
 :dialog-style="{ top: '0px' }"

//modal x 轴固定
/deep/ .ant-modal-wrap{
  overflow-y: hidden !important;
}

//modal  boday 控制高度
/deep/ .ant-modal-body{
  height: calc(100vh - 56px - 53px);
  overflow-y: scroll;
}
```



# uesrInfo
```markdown
  userInfo:
  accounttype: "7"
  activitiSync: null
  areacode: "6211"
  areaname: "定西市"
  avatar: null
  birthday: null
  clientId: null
  createBy: "admin"
  createTime: "2022-03-26 00:48:00"
  delFlag: 0
  departIds: null
  email: null
  enterpriseguid: "054336b5-fe1f-4po5-atw5-afyfc2s1001"
  id: "1507398907109892097"
  loginUserType: 7
  orgCode: "A01"
  orgCodeTxt: null
  phone: null
  post: null
  realname: "陈建军"
  relTenantIds: null
  sex: null
  status: 1
  telephone: null
  token: null
  unitmemberid: "012545b5-ft1f-4rt5-bqr5-avcwq1a1001"
  updateBy: null
  updateTime: null
  userIdentity: null
  username: "AT000108"
  usertype: "1"
  workNo: null
```

zhao:
http://221.216.15.81:7898/greensjk/#/home
zhao:
jeecg/jeecg1314


//嵌套表单校验
import { JeecgFormMixin } from '@/mixins/JeecgFormMixin'
mixins:[JeecgFormMixin],
projectsummary:[{ required: true, message: '请输入xxxx!' ,validator: this.nestedObjectValidator,nestedObjectName:'zgsSamplebuildproject'}],
使用
<a-col :span="24">
<a-form-model-item prop="projectsummary">
<a-textarea :disabled="formDisabled" v-model="model.zgsSamplebuildproject.projectsummary" rows="15" placeholder="请输入项目简介及工程项目规划清空、建设规模、建设手续、施工情况、新技术应用、增量成本等" />
</a-form-model-item>
</a-col>




///deep/.ant-input-number-input-wrap{
//  border: none !important;
//}
//
///deep/.ant-input-number-input{
//  border: none !important;
//  font-size: 0.8rem;
//}

/deep/.ant-input-number{
border: none !important;
font-size: 0.8rem;
}


http://221.216.15.81:7898/greensjk/doc.html#
username: jeecg
password: jeecg1314



# 查询最新的的报表数据
注意：！！！！！！！！！！
查询主表信息，直接在连接中拼接参数：  ?dataSourceName = greenBuild_Qx；
如果是字表数据，传递两个参数如： ?dataSourceName=greenBuild_Qx&processType=0, 查询的就是绿色建筑未开工的

greenBuild_Qx             //  绿色建筑 - 区县
greenBuild_Sz              //  绿色建筑 - 市州
绿色建筑 - 市州 - 明细表   （0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3：竣工后运行）

fabricate_area_Qx       //  装配式建筑 - 区县
fabricate_Sz                 //  装配式建筑 - 市州
（1:新开工  2:已竣工验收 3：生产产能）

buildingenergy_Qx               //  建筑节能 - 区县
buildingenergy_Sz               //  建筑节能 - 市州
不需要查询子类
