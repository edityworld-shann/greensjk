package org.jeecg.modules.green.jxzpsq.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 绩效自评申请
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsPerformancebaseService extends IService<ZgsPerformancebase> {
    int spProject(ZgsPerformancebase zgsPerformancebase);

    String zgsInfoOne(String baseguid);

    Page<ZgsPerformancebase> listZgsPerformancebase(Wrapper<ZgsPerformancebase> queryWrapper, Integer pageNo, Integer pageSize);

    List<ZgsPerformancebase> listZgsPerformancebaseForJxzp(Wrapper<ZgsPerformancebase> queryWrapper);
}
