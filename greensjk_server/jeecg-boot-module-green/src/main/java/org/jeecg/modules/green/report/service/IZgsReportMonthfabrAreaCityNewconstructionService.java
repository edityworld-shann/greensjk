package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityNewconstruction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 现用来同步数据使用
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaCityNewconstructionService extends IService<ZgsReportMonthfabrAreaCityNewconstruction> {

    void dataInitByAreaCode(String areaCode, Integer type);

}
