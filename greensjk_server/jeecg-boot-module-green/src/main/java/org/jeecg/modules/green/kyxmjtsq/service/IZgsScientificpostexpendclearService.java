package org.jeecg.modules.green.kyxmjtsq.service;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificpostexpendclearService extends IService<ZgsScientificpostexpendclear> {

}
