package org.jeecg.modules.green.sfxmsb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_buildrojectmainparticipant")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_buildrojectmainparticipant对象", description = "建筑工程项目主要参加人员")
public class ZgsBuildrojectmainparticipant implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 对应业务表T_BUILDPROJECT主键
     */
    @Excel(name = "对应业务表T_BUILDPROJECT主键", width = 15)
    @ApiModelProperty(value = "对应业务表T_BUILDPROJECT主键")
    private java.lang.String buildguid;
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personname;
    /**
     * 职务
     */
    @Excel(name = "职务", width = 15)
    @ApiModelProperty(value = "职务")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personduty;
    /**
     * 职称
     */
    @Excel(name = "职称", width = 15)
    @ApiModelProperty(value = "职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personpost;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话  ", width = 15)
    @ApiModelProperty(value = "联系电话  ")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String tel;
    /**
     * 承担主要工作
     */
    @Excel(name = "承担主要工作", width = 15)
    @ApiModelProperty(value = "承担主要工作")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String workcontent;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 单位名称
     */
    @Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunit;
    /**
     * 排序号
     */
    @Excel(name = "排序号", width = 15)
    @ApiModelProperty(value = "排序号")
    private java.math.BigDecimal ordernum;
    /**
     * 年龄
     */
    @Excel(name = "年龄", width = 15)
    @ApiModelProperty(value = "年龄")
    private java.math.BigDecimal age;
    /**
     * 身份证号码
     */
    @Excel(name = "身份证号码", width = 15)
    @ApiModelProperty(value = "身份证号码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String idcard;
    /**
     * 性别
     */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String sex;
    /**
     * 专业
     */
    @Excel(name = "专业", width = 15)
    @ApiModelProperty(value = "专业")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String specialty;
    /**
     * 文化程度
     */
    @Excel(name = "文化程度", width = 15)
    @ApiModelProperty(value = "文化程度")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String standardOfCulture;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
