package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_monthfabricate
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabricate")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_monthfabricate对象", description = "zgs_report_monthfabricate")
public class ZgsReportMonthfabricate implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * 填报人
     */
    @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String fillpersontel;
    /**
     * 填报时间
     */
    @Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 填报单位
     */
    @Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    private java.lang.String fillunit;
    /**
     * 单位负责人
     */
    @Excel(name = "单位负责人", width = 15)
    @ApiModelProperty(value = "单位负责人")
    private java.lang.String unitheader;
    /**
     * 报表时间
     */
    @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 行政区域
     */
    @Excel(name = "行政区域", width = 15)
    @ApiModelProperty(value = "行政区域")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    @Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 审核人
     */
    @Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 申报状态
     */
    @Excel(name = "申报状态", width = 15)
    @ApiModelProperty(value = "申报状态")
    private java.math.BigDecimal applystate;
    /**
     * 删除标志
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete;
    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "createpersonname")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    private java.lang.String modifypersonaccount;
    /**
     * 修改人名称
     */
    @Excel(name = "修改人名称", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 本月检查情况次数
     */
    @Excel(name = "本月检查情况次数", width = 15)
    @ApiModelProperty(value = "本月检查情况次数")
    private java.math.BigDecimal byjccs;
    /**
     * 本月检查情况公共建筑项数
     */
    @Excel(name = "本月检查情况公共建筑项数", width = 15)
    @ApiModelProperty(value = "本月检查情况公共建筑项数")
    private java.math.BigDecimal byjcggxs;
    /**
     * 本月检查情况保障性住房项数
     */
    @Excel(name = "本月检查情况保障性住房项数", width = 15)
    @ApiModelProperty(value = "本月检查情况保障性住房项数")
    private java.math.BigDecimal byjcbzxxs;
    /**
     * 本月检查情况商业开发住宅项数
     */
    @Excel(name = "本月检查情况商业开发住宅项数", width = 15)
    @ApiModelProperty(value = "本月检查情况商业开发住宅项数")
    private java.math.BigDecimal byjckfzzxs;
    /**
     * 本月检查情况工业建筑项数
     */
    @Excel(name = "本月检查情况工业建筑项数", width = 15)
    @ApiModelProperty(value = "本月检查情况工业建筑项数")
    private java.math.BigDecimal byjcgyxs;
    /**
     * 通报批评单位（家）
     */
    @Excel(name = "通报批评单位（家）", width = 15)
    @ApiModelProperty(value = "通报批评单位（家）")
    private java.math.BigDecimal bytbppdw;
    /**
     * 通报批评个人（人）
     */
    @Excel(name = "通报批评个人（人）", width = 15)
    @ApiModelProperty(value = "通报批评个人（人）")
    private java.math.BigDecimal bytbppgr;
    /**
     * 限期整改通知（份）
     */
    @Excel(name = "限期整改通知（份）", width = 15)
    @ApiModelProperty(value = "限期整改通知（份）")
    private java.math.BigDecimal byxqzgtz;
    /**
     * 曝光案例（个）
     */
    @Excel(name = "曝光案例（个）", width = 15)
    @ApiModelProperty(value = "曝光案例（个）")
    private java.math.BigDecimal bypgal;


    @TableField(exist = false)
    @ApiModelProperty(value = "1:开工建设")
    private ZgsReportMonthfabricateDetail dt1;

    @TableField(exist = false)
    @ApiModelProperty(value = "2:已竣工验收")
    private ZgsReportMonthfabricateDetail dt2;

    @TableField(exist = false)
    @ApiModelProperty(value = "1:项目库-开工建设-对象")
    private ZgsReportMonthfabrAreaproject dtproject1;
    @TableField(exist = false)
    @ApiModelProperty(value = "2:项目库-已竣工验收-对象")
    private ZgsReportMonthfabrAreaproject dtproject2;
    @TableField(exist = false)
    @ApiModelProperty(value = "单位库-生产产能-对象")
    private ZgsReportMonthfabrAreacompany dtcompany;


    @TableField(exist = false)
    @ApiModelProperty(value = "生产产能情况统计")
    private ZgsReportMonthfabricateScdetail scdetail;

}
