package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertpaper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库专家论文信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertpaperService extends IService<ZgsExpertpaper> {

}
