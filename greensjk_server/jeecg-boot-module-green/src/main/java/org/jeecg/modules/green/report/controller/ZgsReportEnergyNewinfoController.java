package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoGreentotalService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoTotalService;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTableTjPro;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 新建绿色建筑-节能项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Api(tags = "新建绿色建筑-节能项目清单")
@RestController
@RequestMapping("/report/zgsReportEnergyNewinfo")
@Slf4j
public class ZgsReportEnergyNewinfoController extends JeecgController<ZgsReportEnergyNewinfo, IZgsReportEnergyNewinfoService> {
    @Autowired
    private IZgsReportEnergyNewinfoService zgsReportEnergyNewinfoService;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService zgsReportEnergyNewinfoTotalService;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService zgsReportEnergyNewinfoGreentotalService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页列表查询
     *
     * @param zgsReportEnergyNewinfo
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-分页列表查询")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-分页列表查询", notes = "新建绿色建筑-节能项目清单-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportEnergyNewinfo zgsReportEnergyNewinfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "accessType") Integer accessType,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (accessType != null && accessType == 2) {
            queryWrapper.eq("is_no", "1");
        }
        if (zgsReportEnergyNewinfo != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getProjectname())) {
                queryWrapper.like("projectname", zgsReportEnergyNewinfo.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyNewinfo.getFilltm());
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            queryWrapper.eq("applystate", GlobalConstants.apply_state2);
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getAreacode())) {
                if (zgsReportEnergyNewinfo.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportEnergyNewinfo.getAreacode());
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportEnergyNewinfo.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                } else {
//                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
//                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getAreacode())) {
                if (zgsReportEnergyNewinfo.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                } else {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                }
            } else {
                queryWrapper.eq("areacode", sysUser.getAreacode());
                //再加上县区已审核通过的
                queryWrapper.or(w1 -> {
                    if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getFilltm())) {
                        w1.eq("filltm", zgsReportEnergyNewinfo.getFilltm());
                    }
                    if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getProjectname())) {
                        w1.like("projectname", zgsReportEnergyNewinfo.getProjectname());
                    }
                    if (accessType != null && accessType == 2) {
                        w1.eq("is_no", "1");
                    }
                    w1.likeRight("areacode", sysUser.getAreacode());
                    w1.and(w2 -> {
                        w2.eq("applystate", GlobalConstants.apply_state2).or().eq("applystate", GlobalConstants.apply_state1);
                        w2.last(" and length(areacode)=6");
                    });
                });
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByAsc("areacode");
        Page<ZgsReportEnergyNewinfo> page = new Page<ZgsReportEnergyNewinfo>(pageNo, pageSize);
        IPage<ZgsReportEnergyNewinfo> pageList = zgsReportEnergyNewinfoService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsReportEnergyNewinfo
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-添加")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-添加", notes = "新建绿色建筑-节能项目清单-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportEnergyNewinfo zgsReportEnergyNewinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportEnergyNewinfo.setId(UUID.randomUUID().toString());
        zgsReportEnergyNewinfo.setCreatepersonaccount(sysUser.getUsername());
        zgsReportEnergyNewinfo.setCreatepersonname(sysUser.getRealname());
        zgsReportEnergyNewinfo.setCreatetime(new Date());
        zgsReportEnergyNewinfo.setAreacode(sysUser.getAreacode());
        zgsReportEnergyNewinfo.setAreaname(sysUser.getAreaname());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (zgsReportEnergyNewinfo.getEndDate() != null) {
            yearMonth = simpleDateFormat.format(zgsReportEnergyNewinfo.getEndDate());
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportEnergyNewinfo.setFilltm(yearMonth);
        zgsReportEnergyNewinfo.setQuarter(new BigDecimal(quarter));
        zgsReportEnergyNewinfo.setYear(new BigDecimal(year));
        //添加前先查询是否本月已上报
        QueryWrapper<ZgsReportEnergyNewinfo> queryWrapper = new QueryWrapper<ZgsReportEnergyNewinfo>();
        queryWrapper.eq("filltm", yearMonth);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportEnergyNewinfo> list = zgsReportEnergyNewinfoService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("本月已审批，退回后方可新增！");
        }
        zgsReportEnergyNewinfoService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfo, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }


    /**
     * 零上报
     * <p>
     * 新建建筑项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "新建建筑项目清单-本月一键上报（县区或市州）", notes = "新建建筑项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/initMonthNewInfoZero")
    public Result<?> initMonthNewInfoZero(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "EnergyZeroItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        //再判断本月是否已上报
//        QueryWrapper<ZgsReportEnergyNewinfoTotal> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", filltm);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        wrapper.ne("applystate", GlobalConstants.apply_state3);
//        List<ZgsReportEnergyNewinfoTotal> listTotal = zgsReportEnergyNewinfoTotalService.list(wrapper);
//        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> wrapperGreen = new QueryWrapper();
//        wrapperGreen.eq("filltm", filltm);
//        wrapperGreen.eq("areacode", sysUser.getAreacode());
//        wrapperGreen.ne("applystate", GlobalConstants.apply_state3);
//        List<ZgsReportEnergyNewinfoGreentotal> listTotalGreen = zgsReportEnergyNewinfoGreentotalService.list(wrapperGreen);
        //判断节能清单是否有数据
        QueryWrapper<ZgsReportEnergyNewinfo> wrapperItem = new QueryWrapper();
        wrapperItem.eq("filltm", filltm);
        wrapperItem.eq("areacode", sysUser.getAreacode());
        wrapperItem.ne("isdelete", 1);
        List<ZgsReportEnergyNewinfo> listTotalItem = zgsReportEnergyNewinfoService.list(wrapperItem);
//        if (listTotal.size() > 0 && listTotalGreen.size() > 0 && listTotalItem.size() > 0) {
        if (listTotalItem.size() > 0) {
            return Result.error("本月项目清单删除后，方可进行无数据上报操作！");
        }
        zgsReportEnergyNewinfoService.addIterm(filltm);
        zgsReportEnergyNewinfoTotalService.initEnergyNewinfoTotalZero(year, quarter, filltm);
        zgsReportEnergyNewinfoGreentotalService.initEnergyNewinfoGreenTotalZero(year, quarter, filltm);
        return Result.OK("本月一键上报成功！");
    }


    /**
     * 新建建筑项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "新建建筑项目清单-本月一键上报（县区或市州）", notes = "新建建筑项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/initMonthNewInfo")
    public Result<?> initMonthNewInfo(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "EnergyItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        QueryWrapper<ZgsReportEnergyNewinfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("filltm", filltm);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.isNotNull("end_date");
//        List<ZgsReportEnergyNewinfo> list = zgsReportEnergyNewinfoService.list(queryWrapper);
//        boolean flag = false;
//        if (list.size() == 0) {
//            return Result.error("请完整填写项目清单！");
//        } else {
//            //判断项目清单中是否存在绿色建筑，如果不存在直接忽略不上报绿色建筑汇总
//            for (ZgsReportEnergyNewinfo newinfo : list) {
//                if (StringUtils.isNotEmpty(newinfo.getIsNo()) && "1".equals(newinfo.getIsNo())) {
//                    flag = true;
//                }
//            }
//        }
        //再判断本月是否已上报
        QueryWrapper<ZgsReportEnergyNewinfoTotal> wrapper = new QueryWrapper();
        wrapper.eq("filltm", filltm);
        wrapper.eq("areacode", sysUser.getAreacode());
        wrapper.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportEnergyNewinfoTotal> listTotal = zgsReportEnergyNewinfoTotalService.list(wrapper);
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> wrapperGreen = new QueryWrapper();
        wrapperGreen.eq("filltm", filltm);
        wrapperGreen.eq("areacode", sysUser.getAreacode());
        wrapperGreen.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportEnergyNewinfoGreentotal> listTotalGreen = zgsReportEnergyNewinfoGreentotalService.list(wrapperGreen);
        if (listTotal.size() > 0 || listTotalGreen.size() > 0) {
            return Result.error("本月数据已上报！");
        }
//        zgsReportEnergyNewinfoTotalService.initEnergyNewinfoTotal(year, quarter, filltm);
        zgsReportEnergyNewinfoTotalService.energyNewinfoTotalAll(0, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
//        if (flag) {
        if (true) {
            zgsReportEnergyNewinfoGreentotalService.energyNewinfoGreenTotalAll(0, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
        }
        return Result.OK("本月一键上报成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportEnergyNewinfo
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-编辑")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-编辑", notes = "新建绿色建筑-节能项目清单-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportEnergyNewinfo zgsReportEnergyNewinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportEnergyNewinfo.setModifypersonaccount(sysUser.getUsername());
        zgsReportEnergyNewinfo.setModifypersonname(sysUser.getRealname());
        zgsReportEnergyNewinfo.setModifytime(new Date());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (zgsReportEnergyNewinfo.getEndDate() != null) {
            yearMonth = simpleDateFormat.format(zgsReportEnergyNewinfo.getEndDate());
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportEnergyNewinfo.setFilltm(yearMonth);
        zgsReportEnergyNewinfo.setQuarter(new BigDecimal(quarter));
        zgsReportEnergyNewinfo.setYear(new BigDecimal(year));
        zgsReportEnergyNewinfoService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfo, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-通过id删除")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-通过id删除", notes = "新建绿色建筑-节能项目清单-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
//        zgsReportEnergyNewinfoService.removeById(id);
        ZgsReportEnergyNewinfo project = zgsReportEnergyNewinfoService.getById(id);
        if (project != null) {
            if ("0".equals(project.getApplystate().toString()) || "3".equals(project.getApplystate().toString())) {
                zgsReportEnergyNewinfoService.removeById(id);
            } else {
                return Result.error("清单已上报，禁止删除！");
            }
        }
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-批量删除")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-批量删除", notes = "新建绿色建筑-节能项目清单-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportEnergyNewinfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "新建绿色建筑-节能项目清单-通过id查询")
    @ApiOperation(value = "新建绿色建筑-节能项目清单-通过id查询", notes = "新建绿色建筑-节能项目清单-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportEnergyNewinfo zgsReportEnergyNewinfo = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportEnergyNewinfoService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportEnergyNewinfo == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportEnergyNewinfo);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportEnergyNewinfo
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportEnergyNewinfo zgsReportEnergyNewinfo,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "accessType") Integer accessType,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String title = "绿色建筑清单表";
        Result<?> result = queryPageList(zgsReportEnergyNewinfo, 1, 9999, accessType, req);
        IPage<ZgsReportEnergyNewinfo> pageList = (IPage<ZgsReportEnergyNewinfo>) result.getResult();
        List<ZgsReportEnergyNewinfo> list = pageList.getRecords();
        //1、住宅建筑面积2、公共建筑面积3、工业建筑面积
        for (ZgsReportEnergyNewinfo zgs : list) {
            switch (zgs.getBuildType()) {
                case "1":
                    zgs.setBuildType("居住建筑");
                    break;
                case "2":
                    zgs.setBuildType("公共建筑");
                    break;
                case "3":
                    zgs.setBuildType("工业建筑");
                    break;
            }
            if (StringUtils.isEmpty(zgs.getGreenRank())) {
                zgs.setGreenRank("非绿色建筑项目");
            }
        }
        List<ZgsReportEnergyNewinfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportEnergyNewinfo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportEnergyNewinfo, ZgsReportEnergyNewinfo.class, "新建绿色建筑-节能项目清单");
        return mv;
    }

    private String getId(ZgsReportEnergyNewinfo item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportEnergyNewinfo
     */
    @RequestMapping(value = "/exportXlsNew")
    public ModelAndView exportXlsNew(ZgsReportEnergyNewinfo zgsReportEnergyNewinfo,
                                     @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                     @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                     @RequestParam(name = "accessType") Integer accessType,
                                     HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String title = "绿色建筑-节能项目清单";
        Result<?> result = queryPageList(zgsReportEnergyNewinfo, pageNo, 9999, accessType, req);
        IPage<ZgsReportEnergyNewinfo> pageList = (IPage<ZgsReportEnergyNewinfo>) result.getResult();
        List<ZgsReportEnergyNewinfo> list = pageList.getRecords();
        List<ZgsReportEnergyNewinfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportEnergyNewinfo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportEnergyNewinfo, ZgsReportEnergyNewinfo.class, "新建绿色建筑-节能项目清单");
        return mv;
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportEnergyNewinfo.class);
    }

}
