package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsBuildfundbudget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsBuildfundbudgetService extends IService<ZgsBuildfundbudget> {

}
