package org.jeecg.modules.green.kyxmjtzs.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 科研项目结题证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsSpostcertificatebaseService extends IService<ZgsSpostcertificatebase> {
    int spProject(ZgsSpostcertificatebase zgsSpostcertificatebase);

    public String number();

    List<ZgsSpostcertificatebase> listWarnByFundType(Wrapper<ZgsSpostcertificatebase> queryWrapper);
}
