package org.jeecg.modules.green.report.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: zgs_report_monthgreenbuild_area
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface ZgsReportMonthgreenbuildAreaMapper extends BaseMapper<ZgsReportMonthgreenbuildArea> {

  ZgsReportMonthgreenbuildArea queryLatestInfo (@Param("dataSourceName") String dataSourceName, @Param("processType") String processType);
}
