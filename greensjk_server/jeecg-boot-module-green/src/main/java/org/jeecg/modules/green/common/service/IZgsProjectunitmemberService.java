package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目单位注册信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface IZgsProjectunitmemberService extends IService<ZgsProjectunitmember> {
    public int updateUserRegistStatus(ZgsProjectunitmember zgsProjectunitmember);

    public int updateUserRegistStatusByAdmin(ZgsProjectunitmember zgsProjectunitmember);
}
