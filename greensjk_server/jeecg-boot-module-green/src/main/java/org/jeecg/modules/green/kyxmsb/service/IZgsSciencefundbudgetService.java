package org.jeecg.modules.green.kyxmsb.service;

import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface IZgsSciencefundbudgetService extends IService<ZgsSciencefundbudget> {

}
