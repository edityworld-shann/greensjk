package org.jeecg.modules.green.zjksb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 专家库专家论文信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Data
@TableName("zgs_expertpaper")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_expertpaper对象", description = "专家库专家论文信息表")
public class ZgsExpertpaper implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 外键 T_PROJECTUNITMEMBER.guid
     */
    @Excel(name = "外键 T_PROJECTUNITMEMBER.guid", width = 15)
    @ApiModelProperty(value = "外键 T_PROJECTUNITMEMBER.guid")
    private java.lang.String userid;
    /**
     * 对应业务表T_EXPERTINFO主键
     */
    @Excel(name = "对应业务表T_EXPERTINFO主键", width = 15)
    @ApiModelProperty(value = "对应业务表T_EXPERTINFO主键")
    private java.lang.String expertguid;
    /**
     * 发表日期
     */
    @Excel(name = "发表日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "发表日期")
    private java.util.Date publishdate;
    /**
     * 期刊名称
     */
    @Excel(name = "期刊名称", width = 15)
    @ApiModelProperty(value = "期刊名称")
    private java.lang.String periodicalname;
    /**
     * 期号
     */
    @Excel(name = "期号", width = 15)
    @ApiModelProperty(value = "期号")
    private java.lang.String issue;
    /**
     * 论文题目
     */
    @Excel(name = "论文题目", width = 15)
    @ApiModelProperty(value = "论文题目")
    private java.lang.String papertitle;
    /**
     * 署名次序
     */
    @Excel(name = "署名次序", width = 15)
    @ApiModelProperty(value = "署名次序")
    private java.lang.String signatureorder;
    /**
     * 是否删除
     */
    @Excel(name = "是否删除", width = 15)
    @ApiModelProperty(value = "是否删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
