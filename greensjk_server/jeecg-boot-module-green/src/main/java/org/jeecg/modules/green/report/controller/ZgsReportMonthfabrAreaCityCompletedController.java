package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompanyService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompletedService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProductioncapacityService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 装配式建筑-已竣工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Api(tags = "装配式建筑-已竣工-汇总表")
@RestController
@RequestMapping("/report/zgsReportMonthfabrAreaCityCompleted")
@Slf4j
public class ZgsReportMonthfabrAreaCityCompletedController extends JeecgController<ZgsReportMonthfabrAreaCityCompleted, IZgsReportMonthfabrAreaCityCompletedService> {
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;

    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService zgsReportMonthfabrAreaCityCompanyService;

    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService zgsReportMonthfabrAreaCityProductioncapacityService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabrAreaCityCompleted
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-分页列表查询")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-分页列表查询", notes = "装配式建筑-已竣工-汇总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "99") Integer pageSize,
                                   HttpServletRequest req) {

        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (zgsReportMonthfabrAreaCityCompleted != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityCompleted.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityCompleted.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityCompleted.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityCompleted.getApplystate());
                }
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    //                    queryWrapper.isNull("area_type");
                    //                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                    //                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
                //                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.orderByAsc("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        if (StringUtils.isNotBlank(zgsReportMonthfabrAreaCityCompleted.getProjecttype())) {
            queryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityCompleted.getProjecttype());
        }
        queryWrapper.orderByDesc("createtime");
        Page<ZgsReportMonthfabrAreaCityCompleted> page = new Page<>(pageNo, pageSize);
        IPage<ZgsReportMonthfabrAreaCityCompleted> pageList = zgsReportMonthfabrAreaCityCompletedService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 列表查询
     *
     * @param zgsReportMonthfabrAreaCityCompleted
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-列表查询")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-列表查询", notes = "装配式建筑-已竣工-汇总表-列表查询")
    @GetMapping(value = "/queryList")
    public Result<?> queryList(ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted, HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        queryWrapper.orderByAsc("areacode");
        Integer applystate = null;
        if (zgsReportMonthfabrAreaCityCompleted != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityCompleted.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityCompleted.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityCompleted.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityCompleted.getApplystate());
                    applystate = zgsReportMonthfabrAreaCityCompleted.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
                    //                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                    //                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
                //                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.orderByAsc("area_type");
            }
            queryWrapper.isNull("area_type");
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.likeRight("areacode", sysUser.getAreacode());
            queryWrapper.isNull("area_type");
        }
        queryWrapper.orderByDesc("createtime");
        if (StringUtils.isNotBlank(zgsReportMonthfabrAreaCityCompleted.getProjecttype())) {
            queryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityCompleted.getProjecttype());
        }

        List<ZgsReportMonthfabrAreaCityCompleted> areaCityCompletedList = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapper);
        ZgsReportMonthfabrAreaCityCompleted total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 4 || zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataCity(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getAreacode(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
                } else {
                    total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataProvince(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
                }
            } else {
                total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataProvince(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityCompleted.getReporttm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            totalQueryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityCompleted.getProjecttype());
            totalQueryWrapper.ne("isdelete", 1);
            totalQueryWrapper.orderByAsc(" createtime");
            ZgsReportMonthfabrAreaCityCompleted totalCity = zgsReportMonthfabrAreaCityCompletedService.getOne(totalQueryWrapper);
            total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataCity(zgsReportMonthfabrAreaCityCompleted.getReporttm(), sysUser.getAreacode(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
            if (total != null) {
                //计算月份、年、季度
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                String yearMonth = simpleDateFormat.format(new Date());
                if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getReporttm())) {
                    yearMonth = zgsReportMonthfabrAreaCityCompleted.getReporttm().substring(0, 7);
                }
                String year = yearMonth.substring(0, 4);
                String month = yearMonth.substring(5, 7);
                int intMonth = Integer.parseInt(month);
                int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//                total.setCreatepersonaccount(sysUser.getUsername());
//                total.setCreatepersonname(sysUser.getRealname());
                total.setCreatetime(new Date());
                total.setAreacode(sysUser.getAreacode());
                total.setAreaname(sysUser.getAreaname());
                total.setFilltm(zgsReportMonthfabrAreaCityCompleted.getReporttm());
                total.setReporttm(zgsReportMonthfabrAreaCityCompleted.getReporttm());
                total.setIsdelete(new BigDecimal(0));
                total.setQuarter(new BigDecimal(quarter));
                total.setYear(new BigDecimal(year));
                total.setProjecttype(zgsReportMonthfabrAreaCityCompleted.getProjecttype());
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (areaCityCompletedList.size() > 0) {
            if (total != null) {
                areaCityCompletedList.add(total);
            }
        }
        if (areaCityCompletedList.size() > 1) {
            areaCityCompletedList.get(areaCityCompletedList.size() - 1).setAreaname("合计");
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                areaCityCompletedList.get(areaCityCompletedList.size() - 1).setApplystate(null);
            }
        }
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptList(areaCityCompletedList, ValidateEncryptEntityUtil.isDecrypt));
    }


    /**
     * 装配式建筑-已竣工-汇总表-审批
     *
     * @param id
     * @param spStatus
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-审批")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-审批", notes = "装配式建筑-已竣工-汇总表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestParam(name = "id") String id,
                               @RequestParam(name = "spStatus") Integer spStatus,
                               @RequestParam(name = "backreason", required = false) String backreason) {
        zgsReportMonthfabrAreaCityCompletedService.spProject(id, spStatus, backreason);
        return Result.OK("操作成功!");
    }

    /**
     * 新建建筑节能情况汇总表-本月一键上报（市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑节能情况汇总表-本月一键上报（市州）")
    @ApiOperation(value = "新建建筑节能情况汇总表-本月一键上报（市州）", notes = "新建建筑节能情况汇总表-本月一键上报（市州）")
    @GetMapping(value = "/summary")
    public Result<?> summary(@RequestParam(name = "reporttm") String filltm,
                             @RequestParam(name = "projecttype") String projecttype,
                             @RequestParam(name = "newConstructionArea") double newConstructionArea,
                             @RequestParam(name = "completedArea") double completedArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "Complete" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
        String yearMonth = filltm.substring(0, 7);
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        if (!filltm.equals(yearMonth)) {
//            return Result.error("上报日期错误！");
        }
        // ====--====--====--====--====-- 新开工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper();
        queryWrapper.eq("reporttm", filltm);
        queryWrapper.likeRight("areacode", sysUser.getAreacode());
        queryWrapper.isNull("area_type");
        queryWrapper.eq("projecttype", 1);
        queryWrapper.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityCompleted> list = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("请先处理新开工汇总未审批记录！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapper = new QueryWrapper();
//     wrapper.eq("reporttm", filltm);
//     wrapper.eq("areacode", sysUser.getAreacode());
//     wrapper.eq("projecttype", 1);
//     wrapper.ne("applystate", GlobalConstants.apply_state3);
//     wrapper.isNotNull("area_type");
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotal = zgsReportMonthfabrAreaCityCompletedService.list(wrapper);
//     if (listTotal.size() > 0) {
//       return Result.error("本月汇总数据已上报！");
//     }
        // ====--====--====--====--====-- 新开工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        // ====--====--====--====--====-- 已竣工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapperYjg = new QueryWrapper();
        queryWrapperYjg.eq("reporttm", filltm);
        queryWrapperYjg.likeRight("areacode", sysUser.getAreacode());
        queryWrapperYjg.isNull("area_type");
        queryWrapperYjg.eq("projecttype", 2);
        queryWrapperYjg.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityCompleted> listYjg = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapperYjg);
        if (listYjg.size() > 0) {
            return Result.error("请先处理已竣工汇总未审批记录！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperYjg = new QueryWrapper();
//     wrapperYjg.eq("reporttm", filltm);
//     wrapperYjg.eq("areacode", sysUser.getAreacode());
//     wrapperYjg.eq("projecttype", 2);
//     wrapperYjg.ne("applystate", GlobalConstants.apply_state3);
//     wrapperYjg.isNotNull("area_type");
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotalYjg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperYjg);
//     if (listTotalYjg.size() > 0) {
//       return Result.error("本月汇总数据已上报！");
//     }
        // ====--====--====--====--====-- 已竣工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
//     // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompany> queryWrapperSccn = new QueryWrapper();
//        queryWrapperSccn.eq("reporttm", filltm);
//        queryWrapperSccn.likeRight("areacode", sysUser.getAreacode());
//        //     queryWrapper.ne("projecttype", projecttype);
//        List<ZgsReportMonthfabrAreaCityCompany> listSccn = zgsReportMonthfabrAreaCityCompanyService.list(queryWrapperSccn);
//        if (listSccn.size() == 0) {
//            return Result.error("请先填写生产产能清单！");
//        }
        /*QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapperCn = new QueryWrapper();
        queryWrapperCn.eq("reporttm", filltm);
        queryWrapperCn.likeRight("areacode", sysUser.getAreacode());
        queryWrapperCn.isNull("area_type");
        queryWrapperCn.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityProductioncapacity> listCn = zgsReportMonthfabrAreaCityProductioncapacityService.list(queryWrapperCn);
        if (listCn.size() > 0) {
            return Result.error("请先处理生产产能汇总未审批记录！");
        }*/
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperSccn = new QueryWrapper();
//     wrapperSccn.eq("reporttm", filltm);
//     wrapperSccn.eq("areacode", sysUser.getAreacode());
//     wrapperSccn.ne("applystate", GlobalConstants.apply_state3);
//     wrapperSccn.isNotNull("area_type");
//     //     wrapper.ne("projecttype", projecttype);
//     List<ZgsReportMonthfabrAreaCityProductioncapacity> listTotalSccn = zgsReportMonthfabrAreaCityProductioncapacityService.list(wrapperSccn);
//     if (listTotalSccn.size() > 0) {
//       return Result.error("本月生产产能数据已上报！");
//     }
//     // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotal(year, quarter, filltm, "1", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotal(year, quarter, filltm, "2", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrCityTotal(year, quarter, filltm, null);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(0, GlobalConstants.apply_state1, year, quarter, filltm, "1", sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(0, GlobalConstants.apply_state1, year, quarter, filltm, "2", sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrCityTotalALL(0, GlobalConstants.apply_state1, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        return Result.OK("本月汇总一键上报成功！");
    }


    /**
     * 添加
     *
     * @param zgsReportMonthfabrAreaCityCompleted
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-添加")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-添加", notes = "装配式建筑-已竣工-汇总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsReportMonthfabrAreaCityCompleted != null) {
            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
//      zgsReportMonthfabrAreaCityCompleted.setAreaType(sysUser.getUsertype());   //  默认为空：市州内上报，1市州向省厅上报
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsertype());
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getUsertype());
            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
//            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(2022));
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(0));   //  申报状态 0未上报 1 已上报 3 退回
        }
        zgsReportMonthfabrAreaCityCompletedService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportMonthfabrAreaCityCompleted
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-编辑")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-编辑", notes = "装配式建筑-已竣工-汇总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted) {
        zgsReportMonthfabrAreaCityCompletedService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-通过id删除")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-通过id删除", notes = "装配式建筑-已竣工-汇总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportMonthfabrAreaCityCompletedService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-批量删除")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-批量删除", notes = "装配式建筑-已竣工-汇总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabrAreaCityCompletedService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-通过id查询")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-通过id查询", notes = "装配式建筑-已竣工-汇总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportMonthfabrAreaCityCompletedService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportMonthfabrAreaCityCompleted == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportMonthfabrAreaCityCompleted);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportMonthfabrAreaCityCompleted
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted, HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryList(zgsReportMonthfabrAreaCityCompleted, req);
        List<ZgsReportMonthfabrAreaCityCompleted> list = (List<ZgsReportMonthfabrAreaCityCompleted>) result.getResult();
        List<ZgsReportMonthfabrAreaCityCompleted> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "装配式建筑-已竣工-汇总表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportMonthfabrAreaCityCompleted.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportMonthfabrAreaCityCompleted, ZgsReportMonthfabrAreaCityCompleted.class, "装配式建筑-已竣工-汇总表");
        return mv;
    }

    private String getId(ZgsReportMonthfabrAreaCityCompleted item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabrAreaCityCompleted.class);
    }

}
