package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 装配式建筑-单位库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_area_city_company")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_monthfabr_area_city_company对象", description = "装配式建筑-单位库-申报")
public class ZgsReportMonthfabrAreaCityCompany implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    //@Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 报表时间
     */
    //@Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 单位名称
     */
    @Excel(name = "企业名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    private java.lang.String companyName;
    /**
     * 行政区域名称
     */
    @Excel(name = "地区", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 创建人帐号
     */
    //@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;

    /**
     * 退回原因
     */
    //@Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;

    /**
     * 工改系统代码
     */
    //@Excel(name = "工改系统代码", width = 15)
    @ApiModelProperty(value = "工改系统代码")
    private java.lang.String sysCode;
    /**
     * 主要技术体系-住宅（建筑面积 / 数量）
     */
    //@Excel(name = "主要技术体系-住宅（建筑面积 / 数量）", width = 15)
    @ApiModelProperty(value = "主要技术体系-住宅（建筑面积 / 数量）")
    private java.math.BigDecimal buildingArea;
    /**
     * 主要技术体系-公共建筑（建筑面积 / 数量）
     */
    //@Excel(name = "主要技术体系-公共建筑（建筑面积 / 数量）", width = 15)
    @ApiModelProperty(value = "主要技术体系-公共建筑（建筑面积 / 数量）")
    private java.math.BigDecimal buildingArea2;

    /**
     * 主要技术体系（清单列表显示字段）
     */
    @Excel(name = "主要技术体系（清单列表显示字段）", width = 200)
    @ApiModelProperty(value = "主要技术体系（清单列表显示字段）")
    private java.lang.String buildTypeAll;
    /**
     * 选项1-父项：1、住宅 2、公共建筑
     */
    //@Excel(name = "选项1-父项：1、住宅 2、公共建筑", width = 50)
    @ApiModelProperty(value = "选项1-父项：1、住宅 2、公共建筑")
    private java.lang.String buildType;
    /**
     * 选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构
     */
    //@Excel(name = "选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构", width = 50)
    @ApiModelProperty(value = "选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构")
    private java.lang.String buildChildType;
    /**
     * 选项2-父项：1、住宅 2、公共建筑
     */
    //@Excel(name = "选项2-父项：1、住宅 2、公共建筑", width = 50)
    @ApiModelProperty(value = "选项2-父项：1、住宅 2、公共建筑")
    private java.lang.String buildType2;
    /**
     * 选项2-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构
     */
    //@Excel(name = "选项2-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构", width = 50)
    @ApiModelProperty(value = "选项2-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构")
    private java.lang.String buildChildType2;

    /**
     * 开工时间
     */
    //@Excel(name = "开工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开工时间")
    private java.util.Date startDate;
    /**
     * 完成时间
     */
    //@Excel(name = "完成时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "完成时间")
    private java.util.Date endDate;
    /**
     * 装配式混凝土预制构配件生产线（条）
     */
    @Excel(name = "混凝土预制构配件生产情况（生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产线（条）")
    private java.math.BigDecimal zpshntscx;
    /**
     * 装配式混凝土预制构配件设计产能（万立方米）
     */
    @Excel(name = "混凝土预制构配件生产情况（设计产能（万立方米）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件设计产能（万立方米）")
    private java.math.BigDecimal zpshntshejcn;
    /**
     * 装配式混凝土预制构配件实际产能（万立方米）
     */
    @Excel(name = "混凝土预制构配件生产情况（实际产能（万立方米））", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件实际产能（万立方米）")
    private java.math.BigDecimal zpshntsjcn;
    /**
     * 装配式钢结构构件生产线（条）
     */
    @Excel(name = "钢结构构件生产情况（生产线（条））", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产线（条）")
    private java.math.BigDecimal zpsgjgscx;
    /**
     * 装配式钢结构构件生产能力（万吨）
     */
    @Excel(name = "钢结构构件生产情况（生产能力（万吨））", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产能力（万吨）")
    private java.math.BigDecimal zpsgjgscnl;
    /**
     * 装配式钢结构构件应用（万平方米）
     */
    @Excel(name = "钢结构构件生产情况（应用（万平方米））", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件应用（万平方米）")
    private java.math.BigDecimal zpsgjgyy;
    /**
     * 装配式木结构构件生产线（条）
     */
    @Excel(name = "木结构构件生产情况（生产线（条））", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产线（条）")
    private java.math.BigDecimal zpsmjgscx;
    /**
     * 装配式木结构构件生产能力（万吨）
     */
    @Excel(name = "木结构构件生产情况（生产能力（万平方米））", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产能力（万吨）")
    private java.math.BigDecimal zpsmjgscnl;
    /**
     * 装配式木结构构件应用（万平方米）
     */
    @Excel(name = "装配式木结构构件应用（万平方米）", width = 15)
    @ApiModelProperty(value = "装配式木结构构件应用（万平方米）")
    private java.math.BigDecimal zpsmjgyy;

    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String contacts;

    /**
     * 联系人电话
     */
    @Excel(name = "联系人电话", width = 15)
    @ApiModelProperty(value = "联系人电话")
    private java.lang.String contactsPhone;

    /**
     * 厂址
     */
    @Excel(name = "厂址", width = 15)
    @ApiModelProperty(value = "厂址")
    private java.lang.String companyaddress;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
