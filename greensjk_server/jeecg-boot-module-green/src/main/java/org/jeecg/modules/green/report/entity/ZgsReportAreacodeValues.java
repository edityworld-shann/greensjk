package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 装配式新开工和已竣工建筑总面积表
 * @Author: jeecg-boot
 * @Date: 2022-07-29
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_areacode_values")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_areacode_values对象", description = "装配式新开工和已竣工建筑总面积表")
public class ZgsReportAreacodeValues implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
    /**
     * 行政区划编码
     */
    @Excel(name = "行政区划编码", width = 15)
    @ApiModelProperty(value = "行政区划编码")
    private java.lang.String areacode;
    /**
     * 行政区划
     */
    @Excel(name = "行政区划", width = 15)
    @ApiModelProperty(value = "行政区划")
    private java.lang.String areaname;
    /**
     * 月份（如：2022-07）
     */
    @Excel(name = "月份（如：2022-07）", width = 15)
    @ApiModelProperty(value = "月份（如：2022-07）")
    private java.lang.String month;
    /**
     * 本月所有新开工建筑的总面积（㎡）
     */
    @Excel(name = "本月所有新开工建筑的总面积（㎡）", width = 15)
    @ApiModelProperty(value = "本月所有新开工建筑的总面积（㎡）")
    private java.math.BigDecimal monthNewconstructArea;
    /**
     * 本月所有已竣工验收建筑总面积（㎡）
     */
    @Excel(name = "本月所有已竣工验收建筑总面积（㎡）", width = 15)
    @ApiModelProperty(value = "本月所有已竣工验收建筑总面积（㎡）")
    private java.math.BigDecimal monthCompletedArea;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
