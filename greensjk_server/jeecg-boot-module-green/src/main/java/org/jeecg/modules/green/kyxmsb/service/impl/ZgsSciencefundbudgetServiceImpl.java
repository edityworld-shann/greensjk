package org.jeecg.modules.green.kyxmsb.service.impl;

import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.mapper.ZgsSciencefundbudgetMapper;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencefundbudgetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Service
public class ZgsSciencefundbudgetServiceImpl extends ServiceImpl<ZgsSciencefundbudgetMapper, ZgsSciencefundbudget> implements IZgsSciencefundbudgetService {

}
