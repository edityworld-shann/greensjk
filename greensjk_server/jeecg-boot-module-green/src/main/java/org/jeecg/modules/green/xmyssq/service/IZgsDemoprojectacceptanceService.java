package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建设科技示范项目验收申请业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsDemoprojectacceptanceService extends IService<ZgsDemoprojectacceptance> {

}
