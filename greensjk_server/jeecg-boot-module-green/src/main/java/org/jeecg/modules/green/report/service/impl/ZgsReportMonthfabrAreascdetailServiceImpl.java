package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreascdetail;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreascdetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreascdetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabr_areascdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreascdetailServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreascdetailMapper, ZgsReportMonthfabrAreascdetail> implements IZgsReportMonthfabrAreascdetailService {

}
