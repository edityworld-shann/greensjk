package org.jeecg.modules.green.jhxmbgsq.service.impl;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.mapper.ZgsPlannedchangeparticipantMapper;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 计划变更项目组成员表
 * @Author: jeecg-boot
 * @Date:   2022-07-14
 * @Version: V1.0
 */
@Service
public class ZgsPlannedchangeparticipantServiceImpl extends ServiceImpl<ZgsPlannedchangeparticipantMapper, ZgsPlannedchangeparticipant> implements IZgsPlannedchangeparticipantService {

}
