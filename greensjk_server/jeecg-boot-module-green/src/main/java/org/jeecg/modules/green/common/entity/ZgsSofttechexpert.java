package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科技类审批记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Data
@TableName("zgs_softtechexpert")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_softtechexpert对象", description = "科技类审批记录")
public class ZgsSofttechexpert implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * expertguid
     */
    @Excel(name = "expertguid", width = 15)
    @ApiModelProperty(value = "expertguid")
    private java.lang.String expertguid;
    /**
     * businessguid
     */
    @Excel(name = "businessguid", width = 15)
    @ApiModelProperty(value = "businessguid")
    private java.lang.String businessguid;
    /**
     * businesstype
     */
    @Excel(name = "businesstype", width = 15)
    @ApiModelProperty(value = "businesstype")
    private java.lang.String businesstype;
    /**
     * createpersonaccount
     */
    @Excel(name = "createpersonaccount", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * createpersonname
     */
    @Excel(name = "createpersonname", width = 15)
    @ApiModelProperty(value = "createpersonname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * createtime
     */
    @Excel(name = "createtime", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * modifypersonaccount
     */
    @Excel(name = "modifypersonaccount", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * modifypersonname
     */
    @Excel(name = "modifypersonname", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * modifytime
     */
    @Excel(name = "modifytime", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * isdelete
     */
    @Excel(name = "isdelete", width = 15)
    @ApiModelProperty(value = "isdelete")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * auditconclusion
     */
    @Excel(name = "auditconclusion", width = 15)
    @ApiModelProperty(value = "auditconclusion")
    private java.lang.String auditconclusion;
    /**
     * auditidea
     */
    @Excel(name = "auditidea", width = 15)
    @ApiModelProperty(value = "auditidea")
    private java.lang.String auditidea;
    /**
     * auditdate
     */
    @Excel(name = "auditdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "auditdate")
    private java.util.Date auditdate;
    /**
     * validdate
     */
    @Excel(name = "validdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "validdate")
    private java.util.Date validdate;
    /**
     * auditconclusionnum
     */
    @Excel(name = "auditconclusionnum", width = 15)
    @ApiModelProperty(value = "auditconclusionnum")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditconclusionnum;
    /**
     * ispingshen
     */
    @Excel(name = "ispingshen", width = 15)
    @ApiModelProperty(value = "ispingshen")
    private java.math.BigDecimal ispingshen;
    @TableField(exist = false)
    @ValidateEncryptEntity(isEncrypt = true)
    private String username;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
