package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class TotalTitemsPo implements Serializable {
    /**
     * 项目总数
     */
    private String allYearNumber;
    /**
     * 绿色建筑
     */
    private String yearNumber;
    /**
     * 新建占比
     */
    private java.math.BigDecimal newlyBuildZb;

}
