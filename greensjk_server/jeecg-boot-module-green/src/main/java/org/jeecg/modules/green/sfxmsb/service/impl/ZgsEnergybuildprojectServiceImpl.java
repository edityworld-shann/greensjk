package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergybuildproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsEnergybuildprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsEnergybuildprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 节能建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsEnergybuildprojectServiceImpl extends ServiceImpl<ZgsEnergybuildprojectMapper, ZgsEnergybuildproject> implements IZgsEnergybuildprojectService {

}
