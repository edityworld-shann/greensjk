package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 多附件信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsAttachappendixService extends IService<ZgsAttachappendix> {

}
