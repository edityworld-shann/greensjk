package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificexpenditureclear;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificexpenditureclearMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificexpenditureclearService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研项目申报验收经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificexpenditureclearServiceImpl extends ServiceImpl<ZgsScientificexpenditureclearMapper, ZgsScientificexpenditureclear> implements IZgsScientificexpenditureclearService {

}
