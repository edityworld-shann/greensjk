package org.jeecg.modules.green.jxzpsq.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 绩效自评申请
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsPerformancebaseMapper extends BaseMapper<ZgsPerformancebase> {
    @Select("select applyunitprojectleader from zgs_projectlibrary where id=#{baseguid}")
    String zgsInfoOne(@Param("baseguid") String baseguid);

    Page<ZgsPerformancebase> listZgsPerformancebase(Page<ZgsPerformancebase> page, @Param(Constants.WRAPPER) Wrapper<ZgsPerformancebase> queryWrapper);

    List<ZgsPerformancebase> listZgsPerformancebaseForJxzp(@Param(Constants.WRAPPER) Wrapper<ZgsPerformancebase> queryWrapper);

}
