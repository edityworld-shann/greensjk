package org.jeecg.modules.green.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.green.common.entity.ZgsProjectParentInfo;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface IZgsProjectlibraryService extends IService<ZgsProjectlibrary> {
    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo1(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo2(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo3(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo4(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo5(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo6(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize);

    int verificationUnitAndProjectLeader(String projectlibraryId, String unitName, String projectLeader , Date startdate, int addEdit);

    List<ZgsProjectParentInfo> listZgsProjectParentInfo(Wrapper<ZgsProjectParentInfo> queryWrapper, Integer accessType);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoByAccessType(Integer accessType);

    List<ZgsProjectlibraryListInfo> listZgsProjectCommonLibrary(Wrapper<ZgsProjectlibrary> queryWrapper);

    void updateAgreeProject(String pid);

    int updateAddMoney(ZgsProjectlibraryListInfo zgsProjectlibrary);

    int rollBackSp(String id);

    void updateFeeSb(ZgsProjecttask zgsProjecttask);

    String getProjectNumCurrent();

    void deleteByItemId(String ids);

    void initStageAndStatus(String projectlibraryguid);

    void initStageAndStatusForYszs(String projectlibraryguid);

    Map<String, String> getExportXlsData(String id, String bid, Map<String, String> map);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOne(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOneForExport(@Param("fieldStr") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwo(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwoForExport(@Param("fieldStr") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThree(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThreeForExport(@Param("fieldStr") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFour(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFourForExport(@Param("fieldStr") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFive(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFiveForExport(@Param("fieldStr") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> selectAllData(Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectlibraryListInfo> selectAllDataForExport(@Param("fieldStrOfAllData") String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper);



}
