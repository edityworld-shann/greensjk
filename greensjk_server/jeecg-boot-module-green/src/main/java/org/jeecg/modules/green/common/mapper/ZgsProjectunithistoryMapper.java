package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 黑名单历史记录
 * @Author: jeecg-boot
 * @Date: 2022-03-06
 * @Version: V1.0
 */
public interface ZgsProjectunithistoryMapper extends BaseMapper<ZgsProjectunithistory> {
    public int updateUserStatus(@Param("userId") String userId);
}
