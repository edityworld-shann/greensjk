package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsBuildproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsBuildprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsBuildprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsBuildprojectServiceImpl extends ServiceImpl<ZgsBuildprojectMapper, ZgsBuildproject> implements IZgsBuildprojectService {

}
