package org.jeecg.modules.green.common.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科技类审批记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
public interface ZgsSofttechexpertMapper extends BaseMapper<ZgsSofttechexpert> {
    List<ZgsSofttechexpert> getZgsSofttechexpertList(@Param("businessguid") String businessguid);
}
