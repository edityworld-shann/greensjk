package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description: 装配式建筑-已竣工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_area_city_completed")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "装配式建筑-已竣工-汇总表", description = "装配式建筑-已竣工-汇总表")
public class ZgsReportMonthfabrAreaCityCompleted implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    //@Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 报表时间
     */
    // @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 创建人帐号
     */
    //@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    //@Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;
    /**
     * 默认为空：市州内上报，1市州向省厅上报
     */
    //@Excel(name = "默认为空：市州内上报，1市州向省厅上报", width = 15)
    @ApiModelProperty(value = "默认为空：市州内上报，1市州向省厅上报")
    private java.lang.String areaType;

    /**
     * 本月-已竣工验收建筑-总面积
     */
    @Excel(name = "本月-已竣工验收建筑-总面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收建筑-总面积")
    private java.math.BigDecimal monthArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-已竣工验收装配式建筑-建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收装配式建筑-建筑面积")
    private java.math.BigDecimal monthFarArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-已竣工验收装配式建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收装配式建筑-项数")
    private java.math.BigDecimal monthFarNumber = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    //@Excel(name = "本月-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它")
    private java.lang.String monthBaildType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    // @Excel(name = "本月-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它")
    private java.lang.String monthStructureType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-已竣工验收全装修住宅建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收全装修住宅建筑面积")
    private java.math.BigDecimal monthFarZpszxzzjzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-已竣工验收装配化装修住宅建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收装配化装修住宅建筑面积")
    private java.math.BigDecimal monthFarQzxzzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-已竣工验收装配式建筑面积在已竣工验收建筑总面积的占比（本月-装配率（%））", width = 15)
    @ApiModelProperty(value = "本月-已竣工验收装配式建筑面积在已竣工验收建筑总面积的占比（本月-装配率（%））")
    private java.math.BigDecimal monthFarAssemblyrate = new BigDecimal(0);

    /**
     * 本年-已竣工验收建筑-总面积
     */
    @Excel(name = "本年-已竣工验收建筑-总面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收建筑-总面积")
    private java.math.BigDecimal yearArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-已竣工验收装配式建筑-建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收装配式建筑-建筑面积")
    private java.math.BigDecimal yearFarArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-已竣工验收装配式建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收装配式建筑-项数")
    private java.math.BigDecimal yearFarNumber = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    //@Excel(name = "本年-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它")
    private java.lang.String yearBaildType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    //@Excel(name = "本年-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它")
    private java.lang.String yearStructureType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-已竣工验收全装修住宅建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收全装修住宅建筑面积")
    private java.math.BigDecimal yearFarZpszxzzjzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-已竣工验收装配化装修住宅建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收装配化装修住宅建筑面积")
    private java.math.BigDecimal yearFarQzxzzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-已竣工验收装配式建筑面积在已竣工验收建筑总面积的占比（本月-装配率（%））", width = 15)
    @ApiModelProperty(value = "本年-已竣工验收装配式建筑面积在已竣工验收建筑总面积的占比（本月-装配率（%））")
    private java.math.BigDecimal yearFarAssemblyrate = new BigDecimal(0);


    /**
     * 本月-按建筑类型分类-保障性住房-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-保障性住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-保障性住房-建筑面积")
    private java.math.BigDecimal monthFarArea1 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-保障性住房-项数
     */
    @Excel(name = "本月-按建筑类型分类-保障性住房-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-保障性住房-项数")
    private java.math.BigDecimal monthFarNumber1 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-商品住房-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-商品住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-商品住房-建筑面积")
    private java.math.BigDecimal monthFarArea2 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-商品住房-项数
     */
    @Excel(name = "本月-按建筑类型分类-商品住房-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-商品住房-项数")
    private java.math.BigDecimal monthFarNumber2 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-公共建筑-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-公共建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-公共建筑-建筑面积")
    private java.math.BigDecimal monthFarArea3 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-公共建筑-项数
     */
    @Excel(name = "本月-按建筑类型分类-公共建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-公共建筑-项数")
    private java.math.BigDecimal monthFarNumber3 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-农村及旅游景观项目-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-农村及旅游景观项目-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-农村及旅游景观项目-建筑面积")
    private java.math.BigDecimal monthFarArea4 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-农村及旅游景观项目-项数
     */
    @Excel(name = "本月-按建筑类型分类-农村及旅游景观项目-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-农村及旅游景观项目-项数")
    private java.math.BigDecimal monthFarNumber4 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-其他-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-其他-建筑面积")
    private java.math.BigDecimal monthFarArea5 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-其他-项数
     */
    @Excel(name = "本月-按建筑类型分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-其他-项数")
    private java.math.BigDecimal monthFarNumber5 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式混凝土结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式混凝土结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式混凝土结构建筑-建筑面积")
    private java.math.BigDecimal monthFarArea6 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式混凝土结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式混凝土结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式混凝土结构建筑-项数")
    private java.math.BigDecimal monthFarNumber6 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式钢结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式钢结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式钢结构建筑-建筑面积")
    private java.math.BigDecimal monthFarArea7 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式钢结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式钢结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式钢结构建筑-项数")
    private java.math.BigDecimal monthFarNumber7 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积")
    private java.math.BigDecimal monthFarArea8 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其中，钢结构装配式住宅-项数
     */
    @Excel(name = "本月-按结构形式分类-其中，钢结构装配式住宅-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其中，钢结构装配式住宅-项数")
    private java.math.BigDecimal monthFarNumber8 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式木结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式木结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式木结构建筑-建筑面积")
    private java.math.BigDecimal monthFarArea9 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式木结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式木结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式木结构建筑-项数")
    private java.math.BigDecimal monthFarNumber9 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其他-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其他-建筑面积")
    private java.math.BigDecimal monthFarArea10 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其他-项数
     */
    @Excel(name = "本月-按结构形式分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其他-项数")
    private java.math.BigDecimal monthFarNumber10 = new BigDecimal(0);


    /**
     * 本年-按建筑类型分类-保障性住房-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-保障性住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-保障性住房-建筑面积")
    private java.math.BigDecimal yearFarArea1 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-保障性住房-项数
     */
    @Excel(name = "本年-按建筑类型分类-保障性住房-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-保障性住房-项数")
    private java.math.BigDecimal yearFarNumber1 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-商品住房-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-商品住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-商品住房-建筑面积")
    private java.math.BigDecimal yearFarArea2 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-商品住房-项数
     */
    @Excel(name = "本年-按建筑类型分类-商品住房-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-商品住房-项数")
    private java.math.BigDecimal yearFarNumber2 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-公共建筑-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-公共建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-公共建筑-建筑面积")
    private java.math.BigDecimal yearFarArea3 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-公共建筑-项数
     */
    @Excel(name = "本年-按建筑类型分类-公共建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-公共建筑-项数")
    private java.math.BigDecimal yearFarNumber3 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-农村及旅游景观项目-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-农村及旅游景观项目-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-农村及旅游景观项目-建筑面积")
    private java.math.BigDecimal yearFarArea4 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-农村及旅游景观项目-项数
     */
    @Excel(name = "本年-按建筑类型分类-农村及旅游景观项目-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-农村及旅游景观项目-项数")
    private java.math.BigDecimal yearFarNumber4 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-其他-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-其他-建筑面积")
    private java.math.BigDecimal yearFarArea5 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-其他-项数
     */
    @Excel(name = "本年-按建筑类型分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-其他-项数")
    private java.math.BigDecimal yearFarNumber5 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式混凝土结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式混凝土结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式混凝土结构建筑-建筑面积")
    private java.math.BigDecimal yearFarArea6 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式混凝土结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式混凝土结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式混凝土结构建筑-项数")
    private java.math.BigDecimal yearFarNumber6 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式钢结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式钢结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式钢结构建筑-建筑面积")
    private java.math.BigDecimal yearFarArea7 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式钢结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式钢结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式钢结构建筑-项数")
    private java.math.BigDecimal yearFarNumber7 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积")
    private java.math.BigDecimal yearFarArea8 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其中，钢结构装配式住宅-项数
     */
    @Excel(name = "本年-按结构形式分类-其中，钢结构装配式住宅-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其中，钢结构装配式住宅-项数")
    private java.math.BigDecimal yearFarNumber8 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式木结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式木结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式木结构建筑-建筑面积")
    private java.math.BigDecimal yearFarArea9 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式木结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式木结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式木结构建筑-项数")
    private java.math.BigDecimal yearFarNumber9 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其他-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其他-建筑面积")
    private java.math.BigDecimal yearFarArea10 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其他-项数
     */
    @Excel(name = "本年-按结构形式分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其他-项数")
    private java.math.BigDecimal yearFarNumber10 = new BigDecimal(0);

    /**
     * 1:新开工2:已竣工验收
     */
    //@Excel(name = "1:新开工2:已竣工验收", width = 3)
    @ApiModelProperty(value = "1:新开工2:已竣工验收")
    private java.lang.String projecttype;

    @TableField(exist = false)
    @ApiModelProperty(value = "本月新开工建筑总面积")
    private double newConstructionArea = 0.0;
    @TableField(exist = false)
    @ApiModelProperty(value = "本月已竣工验收建筑总面积")
    private double completedArea = 0.0;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;


}
