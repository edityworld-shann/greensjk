package org.jeecg.modules.green.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Description: 科技项目任务书申报
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
public interface ZgsSciencetechtaskMapper extends BaseMapper<ZgsSciencetechtask> {
    int updateDealType1IsNull();

    int updateDealType2IsNull();

    Page<ZgsSciencetechtask> YqLqlist(Page<ZgsSciencetechtask> page, @Param(Constants.WRAPPER) Wrapper<ZgsSciencetechtask> queryWrapper,@Param("enterpriseguid") String enterpriseguid);

    List<ZgsSciencetechtask> listZgsScienceTechTaskListInfoTask(@Param(Constants.WRAPPER) Wrapper<ZgsSciencetechtask> queryWrapper);

    int updateStageByBid1(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid2(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid3(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid4(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid5(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid6(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid7(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid8(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid9(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid10(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid11(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid12(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    int updateStageByBid13(@Param("projectstage") String projectstage, @Param("bid") String bid, @Param("status") String status);

    Page<ZgsSciencetechtask> getZgsSciencetechtask(Page<ZgsSciencetechtask> page, @Param(Constants.WRAPPER) Wrapper<ZgsSciencetechtask> queryWrapper);

    List<ZgsSciencetechtask> zgsScienceTechTaskListForYq(@Param(Constants.WRAPPER) Wrapper<ZgsSciencetechtask> queryWrapper);
}
