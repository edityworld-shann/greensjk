package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目单位注册信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_projectunitmember")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectunitmember对象", description = "项目单位注册信息表")
public class ZgsProjectunitmember implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 登录名
     */
    @Excel(name = "登录名", width = 15)
    @ApiModelProperty(value = "登录名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String loginname;
    /**
     * 登录密码
     */
    @Excel(name = "登录密码", width = 15)
    @ApiModelProperty(value = "登录密码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String loginpassword;
    /**
     * 项目单位ID
     */
    @Excel(name = "项目单位ID", width = 15)
    @ApiModelProperty(value = "项目单位ID")
    private java.lang.String enterpriseguid;
    /**
     * 项目单位名称/人员姓名（帐号类型为个人时填写）
     */
    @Excel(name = "项目单位名称/人员姓名（帐号类型为个人时填写）", width = 15)
    @ApiModelProperty(value = "项目单位名称/人员姓名（帐号类型为个人时填写）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String enterprisename;
    /**
     * 统一社会信用编码18位或组织机构代码无横线9位
     */
    @Excel(name = "统一社会信用编码18位或组织机构代码无横线9位", width = 15)
    @ApiModelProperty(value = "统一社会信用编码18位或组织机构代码无横线9位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unifiedcreditencode;
    /**
     * 单位地址
     */
    @Excel(name = "单位地址", width = 15)
    @ApiModelProperty(value = "单位地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String address;
    /**
     * 单位联系人
     */
    @Excel(name = "单位联系人", width = 15)
    @ApiModelProperty(value = "单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkuser;
    /**
     * 单位联系电话
     */
    @Excel(name = "单位联系电话", width = 15)
    @ApiModelProperty(value = "单位联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String mobilephone;
    /**
     * 传真
     */
    @Excel(name = "传真", width = 15)
    @ApiModelProperty(value = "传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fax;
    /**
     * 电子邮箱
     */
    @Excel(name = "电子邮箱", width = 15)
    @ApiModelProperty(value = "电子邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String email;
    /**
     * 单位法定代表人
     */
    @Excel(name = "单位法定代表人", width = 15)
    @ApiModelProperty(value = "单位法定代表人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalperson;
    /**
     * 单位法定代表人身份证号，帐号类型个人时的个人身份证号
     */
    @Excel(name = "单位法定代表人身份证号，帐号类型个人时的个人身份证号", width = 15)
    @ApiModelProperty(value = "单位法定代表人身份证号，帐号类型个人时的个人身份证号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalpersonidcard;
    /**
     * 单位所属区域编码
     */
    @Excel(name = "单位所属区域编码", width = 15)
    @ApiModelProperty(value = "单位所属区域编码")
    private java.lang.String citynum;
    /**
     * 单位所属区域
     */
    @Excel(name = "单位所属区域", width = 15)
    @ApiModelProperty(value = "单位所属区域")
    private java.lang.String cityname;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 审核状态：1待审核，2审核通过
     */
    @Excel(name = "审核状态：1待审核，2审核通过", width = 15)
    @ApiModelProperty(value = "审核状态：1待审核，2审核通过")
    private java.lang.String status;
    /**
     * 审核意见
     */
    @Excel(name = "审核意见", width = 15)
    @ApiModelProperty(value = "审核意见")
    private java.lang.String auditopinion;
    /**
     * 审核时间
     */
    @Excel(name = "审核时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "审核时间")
    private java.util.Date auditdate;
    /**
     * 审核人
     */
    @Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String auditer;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 帐号类型：0单位，1个人
     */
    @Excel(name = "帐号类型：0单位，1个人", width = 15)
    @ApiModelProperty(value = "帐号类型：0单位，1个人")
    private java.lang.String accounttype;
    /**
     * 聘用单位名称，帐号类型为个人时填写
     */
    @Excel(name = "聘用单位名称，帐号类型为个人时填写", width = 15)
    @ApiModelProperty(value = "聘用单位名称，帐号类型为个人时填写")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String employunit;
    /**
     * 就职部门*，帐号类型为个人时填写
     */
    @Excel(name = " 就职部门*，帐号类型为个人时填写", width = 15)
    @ApiModelProperty(value = " 就职部门*，帐号类型为个人时填写")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String department;
    /**
     * 个人帐号类型：1科技攻关，2专家库
     */
    @Excel(name = "个人帐号类型：1科技攻关，2专家库", width = 15)
    @ApiModelProperty(value = "个人帐号类型：1科技攻关，2专家库")
    private java.lang.String usertype;
    private Integer isFlag;
    @TableField(exist = false)
    private String appendixpath;
    @TableField(exist = false)
    private String userid;
    /**
     * 行政区域代码
     */
    @Excel(name = "行政区域代码", width = 15)
    @ApiModelProperty(value = "行政区域代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
