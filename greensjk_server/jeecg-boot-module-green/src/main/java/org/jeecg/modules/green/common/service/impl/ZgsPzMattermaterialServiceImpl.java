package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsPzMattermaterial;
import org.jeecg.modules.green.common.mapper.ZgsPzMattermaterialMapper;
import org.jeecg.modules.green.common.service.IZgsPzMattermaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 配置库-各类项目工程申报材料配置表
 * @Author: jeecg-boot
 * @Date:   2022-02-09
 * @Version: V1.0
 */
@Service
public class ZgsPzMattermaterialServiceImpl extends ServiceImpl<ZgsPzMattermaterialMapper, ZgsPzMattermaterial> implements IZgsPzMattermaterialService {

}
