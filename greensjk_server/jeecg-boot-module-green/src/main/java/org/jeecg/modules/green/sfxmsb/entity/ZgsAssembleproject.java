package org.jeecg.modules.green.sfxmsb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 装配式建筑示范工程业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_assembleproject")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_assembleproject对象", description = "装配式建筑示范工程业务主表")
public class ZgsAssembleproject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 建筑工程项目库ID
     */
    @Excel(name = "建筑工程项目库ID", width = 15)
    @ApiModelProperty(value = "建筑工程项目库ID")
    private java.lang.String projectlibraryguid;
    /**
     * 工程概况
     */
    @Excel(name = "工程概况", width = 15)
    @ApiModelProperty(value = "工程概况")
    private java.lang.String projectsurvey;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审
     */
    @Excel(name = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
    @Excel(name = "审核退回意见", width = 15)
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
    @Excel(name = "初审人", width = 15)
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
    @Excel(name = "终审人", width = 15)
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 初审部门
     */
    @Excel(name = "初审部门", width = 15)
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
    @Excel(name = "终审部门", width = 15)
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 购置设备费
     */
    @Excel(name = "购置设备费", width = 15)
    @ApiModelProperty(value = "购置设备费")
    private java.math.BigDecimal equipmentcost;
    /**
     * 试制设备费
     */
    @Excel(name = "试制设备费", width = 15)
    @ApiModelProperty(value = "试制设备费")
    private java.math.BigDecimal equipmenttrial;
    /**
     * 设备改造与租赁费
     */
    @Excel(name = "设备改造与租赁费", width = 15)
    @ApiModelProperty(value = "设备改造与租赁费")
    private java.math.BigDecimal equipmentchange;
    /**
     * 材料费
     */
    @Excel(name = "材料费", width = 15)
    @ApiModelProperty(value = "材料费")
    private java.math.BigDecimal materialfee;
    /**
     * 测试化验加工费
     */
    @Excel(name = "测试化验加工费", width = 15)
    @ApiModelProperty(value = "测试化验加工费")
    private java.math.BigDecimal processfee;
    /**
     * 燃料动力费
     */
    @Excel(name = "燃料动力费", width = 15)
    @ApiModelProperty(value = "燃料动力费")
    private java.math.BigDecimal fuelpowerfee;
    /**
     * 差旅费
     */
    @Excel(name = "差旅费", width = 15)
    @ApiModelProperty(value = "差旅费")
    private java.math.BigDecimal travelfee;
    /**
     * 会议费
     */
    @Excel(name = "会议费", width = 15)
    @ApiModelProperty(value = "会议费")
    private java.math.BigDecimal meetingfee;
    /**
     * 合作与交流费
     */
    @Excel(name = "合作与交流费", width = 15)
    @ApiModelProperty(value = "合作与交流费")
    private java.math.BigDecimal exchangefee;
    /**
     * 出版/文献/信息传播/知识产权事务费
     */
    @Excel(name = "出版/文献/信息传播/知识产权事务费", width = 15)
    @ApiModelProperty(value = "出版/文献/信息传播/知识产权事务费")
    private java.math.BigDecimal publicationfee;
    /**
     * 劳务费
     */
    @Excel(name = "劳务费", width = 15)
    @ApiModelProperty(value = "劳务费")
    private java.math.BigDecimal labourfee;
    /**
     * 专家咨询评审费
     */
    @Excel(name = "专家咨询评审费", width = 15)
    @ApiModelProperty(value = "专家咨询评审费")
    private java.math.BigDecimal assessmentfee;
    /**
     * 管理费
     */
    @Excel(name = "管理费", width = 15)
    @ApiModelProperty(value = "管理费")
    private java.math.BigDecimal managefee;
    /**
     * 其他（含培训费）
     */
    @Excel(name = "其他（含培训费）", width = 15)
    @ApiModelProperty(value = "其他（含培训费）")
    private java.math.BigDecimal trainingfee;
    /**
     * 政府投资
     */
    @Excel(name = "政府投资", width = 15)
    @ApiModelProperty(value = "政府投资")
    private java.math.BigDecimal govinvestment;
    /**
     * 自筹资金
     */
    @Excel(name = "自筹资金", width = 15)
    @ApiModelProperty(value = "自筹资金")
    private java.math.BigDecimal selfcapital;
    /**
     * 单体建筑数量
     */
    @Excel(name = "单体建筑数量", width = 15)
    @ApiModelProperty(value = "单体建筑数量")
    private java.math.BigDecimal singleprojectnum;
    /**
     * 楼号
     */
    @Excel(name = "楼号", width = 15)
    @ApiModelProperty(value = "楼号")
    private java.lang.String serialnum;
    /**
     * 混凝土结构项目加权平均预制率（%）
     */
    @Excel(name = "混凝土结构项目加权平均预制率（%）", width = 15)
    @ApiModelProperty(value = "混凝土结构项目加权平均预制率（%）")
    private java.math.BigDecimal weightedrate;
    /**
     * 建设单位联系人邮箱
     */
    @Excel(name = "建设单位联系人邮箱", width = 15)
    @ApiModelProperty(value = "建设单位联系人邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerchargeremail;
    /**
     * 建设单位联系人邮箱
     */
    @Excel(name = "建设单位联系人邮箱", width = 15)
    @ApiModelProperty(value = "建设单位联系人邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerlinkmanemail;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 专家评审结果 1 ：不同意立项 ，2：同意立项
     */
    @Excel(name = "专家评审结果 1 ：不同意立项 ，2：同意立项", width = 15)
    @ApiModelProperty(value = "专家评审结果 1 ：不同意立项 ，2：同意立项")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal agreeproject;
    /**
     * 立项补证状态（默认为空，1表示被补证修改过）
     */
    private String lxbzStatus;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15)
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 不同意立项原因
     */
    @Excel(name = "不同意立项原因", width = 15)
    @ApiModelProperty(value = "不同意立项原因")
    private java.lang.String notagreereason;
    /**
     * 0不是历史数据，1历史数据
     */
    @Excel(name = "0不是历史数据，1历史数据", width = 15)
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    /**
     * 申报单位意见
     */
    @Excel(name = "申报单位意见", width = 15)
    @ApiModelProperty(value = "申报单位意见")
    private java.lang.String declaration;
    /**
     * 参建单位意见
     */
    @Excel(name = "参建单位意见", width = 15)
    @ApiModelProperty(value = "参建单位意见")
    private java.lang.String ownerunitdeclaration;
    /**
     * 市级建设或建设单位主管部门意见
     */
    @Excel(name = "市级建设或建设单位主管部门意见", width = 15)
    @ApiModelProperty(value = "市级建设或建设单位主管部门意见")
    private java.lang.String departmentdeclaration;
    /**
     * 省级建设主管部门意见
     */
    @Excel(name = "省级建设主管部门意见", width = 15)
    @ApiModelProperty(value = "省级建设主管部门意见")
    private java.lang.String provincedeclaration;
    /**
     * 法人代表邮箱
     */
    @Excel(name = "法人代表邮箱", width = 15)
    @ApiModelProperty(value = "法人代表邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitemail;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkman;
    /**
     * 联系人联系电话
     */
    @Excel(name = "联系人联系电话", width = 15)
    @ApiModelProperty(value = "联系人联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkmanphone;
    /**
     * 联系人邮箱
     */
    @Excel(name = "联系人邮箱", width = 15)
    @ApiModelProperty(value = "联系人邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkmanemail;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
