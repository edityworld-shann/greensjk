package org.jeecg.modules.green.report.service;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaprojectService extends IService<ZgsReportMonthfabrAreaproject> {
  public ZgsReportMonthfabrAreaproject queryProjectByReportTime(String reporttm);
}
