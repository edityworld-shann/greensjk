package org.jeecg.modules.green.largeScreen.service;

import org.jeecg.modules.green.largeScreen.po.*;
import org.jeecg.modules.green.largeScreen.vo.PrefabricaeZbVo;

import java.util.List;
import java.util.Map;

public interface LargeScreenZgsReportEnergyNewinfoGreentotalService {
    SpecificDataInformationPo specificDataInformation(String year, String areacodel);

    AssemblyDataStatisticsPo assemblyDataStatistics(String year);

    TotalTitemsPo totalTitems(String year, String areacodel);

    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYear(String year);

    List<MoonGreenBuildingPo> greenBuildingData(String year, String month, String areacodel);

    PrefabricatedBuildingPo prefabricatedBuilding(String year, String month, String areacodel);

    String newConstructiontoTalArea(String year, String month, String areacodel);

    PrefabricatedBuildingPo fabricatedCompletion(String year, String month, String areacodel);

    String totalBuildingArea(String year, String month, String areacodel);

    List<BuildingEnergySavingPo> buildingEnergySavingData(String year, String month, String areacodel);

    BuildingEnergySavingTotalTitemsPo buildingEnergySavingTotalTitems(String year, String month,String areacode);

    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergy(String year);

    BuildingEnergyEfficiencyStatisticsPo buildingEnergyEfficiencyStatistics(String year, String month, String areacodel);

    List<BuildingEnergySavingPo> buildingEnergySavingExisting(String year, String month, String areacodel);

    List<City> getAllCities();

    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYearNew(String year, String code);

    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyNew(String year, String code);

    List<MoonGreenBuildingPo> moonGreenBuildingPo(String year, String month, String code);

    SpecificDataInformationPo specificDataInformationNew(String year, String areacodel);

    BuildingEnergySavingPo buildingEnergySavingPo(String year, String month, String code);

    BuildingEnergySavingPo buildingEnergySavingExistingPo(String year, String month, String code);

    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInPoList(String year, String areacode);

    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyList(String year, String areacode);

    String getCityOne(String areacodel);

    MoonGreenBuildingPo monthMoonGreenBuildingPo(String year, String month, String code);

    List<MoonGreenBuildingPo> monthGreenBuildingData(String year, String month, String areacodel);

    List<BuildingEnergySavingPo> monthBuildingEnergySavingPo(String year, String month, String code);

    List<BuildingEnergySavingPo> monthBuildingEnergySavingExistingPo(String year, String month, String code);

    PrefabricaeZbVo prefabricateBulidngZb(String month, String areacode,String prdType);

    List<MoonGreenBuildingPo> moonGreenBuildingPoTow(String year, String month, String areacodel);
}
