package org.jeecg.modules.green.common.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectlibrary对象", description = "建筑工程项目库")
public class ZgsProjectlibraryListInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private String id;
    /**
     * 关联项目主键ID
     */
    @ApiModelProperty(value = "关联项目主键ID")
    private String bid;
    /**
     * 企业ID
     */
    @ApiModelProperty(value = "企业ID")
    private String enterpriseguid;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 25, orderNum = "1")
    @ApiModelProperty(value = "工程项目名称")
    private String projectname;
    /**
     * 申报单位
     */
    @Excel(name = "申报单位", width = 25, orderNum = "4")
    @ApiModelProperty(value = "申报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String applyunit;
    /**
     * 申报单位项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "6")
    @ApiModelProperty(value = "申报单位项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private String applyunitprojectleader;
    /**
     * 申报单位联系电话
     */
    @Excel(name = "联系电话", width = 15, orderNum = "1")
    @ApiModelProperty(value = "申报单位联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String applyunitphone;

    /**
     * 完成日期(示范/)
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "完成日期(示范/)")
    @Excel(name = "完成日期", width = 15, format = "yyyy-MM-dd", orderNum = "5")
    private Date completedate;

    //@Excel(name = "完成日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始日期(示范/)")
    private Date startdate;
    /**
     * 项目类型:字典DemonstrateType
     */
    @ApiModelProperty(value = "项目类型:字典DemonstrateType")
    private java.lang.String projecttypenum;
    //@Excel(name = "项目类型", width = 15, dicCode = "DemonstrateType", orderNum = "1")
    @ApiModelProperty(value = "项目类型")
    private String projecttype;
    /**
     * 示范类别
     */
    @Excel(name = "示范类别", width = 15, orderNum = "3")
    @ApiModelProperty(value = "示范类别")
    private String demonstratetype;

    @Excel(name = "退回原因", width = 15, orderNum = "10")
    @ApiModelProperty(value = "审核退回意见")
    private String auditopinion;

    // @Excel(name = "审核状态", width = 15, dicCode = "sfproject_status", orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，3退回企业，4初审通过，5退回初审，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private String status;
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private Date applydate;
    //@Excel(name = "初审日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审日期")
    private Date firstdate;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private Date finishdate;
    /**
     * 项目竣工时间
     */
    @Excel(name = "竣工时间", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目竣工时间")
    private java.util.Date projectenddate;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;

    @TableField(exist = false)
    private Integer spStatus;
    //专家审批列表新增返回字段
    /**
     * 截止审核日期
     */
//    @Excel(name = "截止审核日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "截止审核日期")
    private java.util.Date validdate;
    @ApiModelProperty(value = "是否评审，1：已评审（反馈给省厅）；否则未评审；")
    private java.math.BigDecimal ispingshen;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    private java.math.BigDecimal returntype;
    /**
     * 专家评审结果 1 ：同意立项 ，0：不同意立项
     */
    @Excel(name = "是否立项", width = 15, orderNum = "10")
    @ApiModelProperty(value = "专家评审结果 1 ：同意立项 ，0：不同意立项")
    @Dict(dicCode = "agreeproject")
    private String agreeproject;

    @TableField(exist = false)
    private Object cstatus;
    @ApiModelProperty(value = "是否可强制编辑:默认为空，1为可编辑")
    private String editStatus;
    @ApiModelProperty(value = "pdf导出文件路径")
    private String pdfUrl;
    @TableField(exist = false)
    private String applyStatus = "1";
    /**
     * 初审部门
     */
    //@Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private String firstdepartment;
    /**
     * 1项目库2示范项目
     */
    private String sfxmtype;
    /**
     * 项目库状态：1未入库2已入库3已推荐
     */
    @ApiModelProperty(value = "项目库状态：1未入库2已入库3已推荐")
    @Dict(dicCode = "SfProjectKuStatus")
    private String sfxmstatus;

    @Excel(name = "项目阶段", width = 15, orderNum = "7")
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;


    //@ApiModelProperty(value = "项目阶段状态")
    // @Dict(dicCode = "sfproject_status")
    @Excel(name = "项目状态", width = 15, dicCode = "sfproject_status", orderNum = "8")
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;

    @ApiModelProperty(value = "1：终止日期延期2:闭合-终止")
    private java.math.BigDecimal isdealoverdue;

    @Dict(dicCode = "yn")
    @Excel(name = "是否会审", width = 15, orderNum = "9")
    private String sfhs;

    private java.math.BigDecimal provincialfundtotal;
    private String yearNum;
    private Integer moneyStatus;

    @Excel(name = "项目状态", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目阶段状态-字典值")
    private java.lang.String projectstagestatusDict;

    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;

    // 往期逾期项目
    @TableField(exist = false)
    private List<ZgsProjecttask> yqProjectList;
    // 判断是否存在逾期项目的标识  1存在   0不存在
    @TableField(exist = false)
    private String yqFlag;

    @ApiModelProperty(value = "授权编辑 1同意编辑  2取消编辑")
    private String sqbj;

    @ApiModelProperty(value = "编辑保存 1修改未提交  2修改已提交")
    private String updateSave;

    // 1 表示申报单位可编辑    2 表示取消编辑，编辑按钮不存在
    @TableField(exist = false)
    private String updateFlag;

    // 编辑状态  1 修改未提交   2修改已提交
    @TableField(exist = false)
    private String bjzt;

    @TableField(exist = false)
    private boolean sqbjBol = true;

    @ApiModelProperty(value = "经费备注")
    private java.lang.String fundRemark;
}
