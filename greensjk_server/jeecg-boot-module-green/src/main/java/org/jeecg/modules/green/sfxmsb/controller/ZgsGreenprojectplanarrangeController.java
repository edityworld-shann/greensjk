package org.jeecg.modules.green.sfxmsb.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenprojectplanarrange;
import org.jeecg.modules.green.sfxmsb.service.IZgsGreenprojectplanarrangeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 绿色建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Api(tags="绿色建筑工程计划进度与安排")
@RestController
@RequestMapping("/sfxmsb/zgsGreenprojectplanarrange")
@Slf4j
public class ZgsGreenprojectplanarrangeController extends JeecgController<ZgsGreenprojectplanarrange, IZgsGreenprojectplanarrangeService> {
	@Autowired
	private IZgsGreenprojectplanarrangeService zgsGreenprojectplanarrangeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsGreenprojectplanarrange
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-分页列表查询")
	@ApiOperation(value="绿色建筑工程计划进度与安排-分页列表查询", notes="绿色建筑工程计划进度与安排-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsGreenprojectplanarrange zgsGreenprojectplanarrange,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsGreenprojectplanarrange> queryWrapper = QueryGenerator.initQueryWrapper(zgsGreenprojectplanarrange, req.getParameterMap());
		Page<ZgsGreenprojectplanarrange> page = new Page<ZgsGreenprojectplanarrange>(pageNo, pageSize);
		IPage<ZgsGreenprojectplanarrange> pageList = zgsGreenprojectplanarrangeService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsGreenprojectplanarrange
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-添加")
	@ApiOperation(value="绿色建筑工程计划进度与安排-添加", notes="绿色建筑工程计划进度与安排-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsGreenprojectplanarrange zgsGreenprojectplanarrange) {
		zgsGreenprojectplanarrangeService.save(zgsGreenprojectplanarrange);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsGreenprojectplanarrange
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-编辑")
	@ApiOperation(value="绿色建筑工程计划进度与安排-编辑", notes="绿色建筑工程计划进度与安排-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsGreenprojectplanarrange zgsGreenprojectplanarrange) {
		zgsGreenprojectplanarrangeService.updateById(zgsGreenprojectplanarrange);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-通过id删除")
	@ApiOperation(value="绿色建筑工程计划进度与安排-通过id删除", notes="绿色建筑工程计划进度与安排-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsGreenprojectplanarrangeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-批量删除")
	@ApiOperation(value="绿色建筑工程计划进度与安排-批量删除", notes="绿色建筑工程计划进度与安排-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsGreenprojectplanarrangeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绿色建筑工程计划进度与安排-通过id查询")
	@ApiOperation(value="绿色建筑工程计划进度与安排-通过id查询", notes="绿色建筑工程计划进度与安排-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsGreenprojectplanarrange zgsGreenprojectplanarrange = zgsGreenprojectplanarrangeService.getById(id);
		if(zgsGreenprojectplanarrange==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsGreenprojectplanarrange);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsGreenprojectplanarrange
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsGreenprojectplanarrange zgsGreenprojectplanarrange) {
        return super.exportXls(request, zgsGreenprojectplanarrange, ZgsGreenprojectplanarrange.class, "绿色建筑工程计划进度与安排");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsGreenprojectplanarrange.class);
    }

}
