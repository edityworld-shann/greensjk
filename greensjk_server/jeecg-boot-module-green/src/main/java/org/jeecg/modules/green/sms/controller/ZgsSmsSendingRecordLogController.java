package org.jeecg.modules.green.sms.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 短信记录表
 * @Author: jeecg-boot
 * @Date: 2022-06-07
 * @Version: V1.0
 */
@Api(tags = "短信记录表")
@RestController
@RequestMapping("/sms/zgsSmsSendingRecordLog")
@Slf4j
public class ZgsSmsSendingRecordLogController extends JeecgController<ZgsSmsSendingRecordLog, IZgsSmsSendingRecordLogService> {
    @Autowired
    private IZgsSmsSendingRecordLogService zgsSmsSendingRecordLogService;

    /**
     * 分页列表查询
     *
     * @param zgsSmsSendingRecordLog
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "短信记录表-分页列表查询")
    @ApiOperation(value = "短信记录表-分页列表查询", notes = "短信记录表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSmsSendingRecordLog zgsSmsSendingRecordLog,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
//		QueryWrapper<ZgsSmsSendingRecordLog> queryWrapper = QueryGenerator.initQueryWrapper(zgsSmsSendingRecordLog, req.getParameterMap());
        QueryWrapper<ZgsSmsSendingRecordLog> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("createtime");
        if (StringUtils.isNotBlank(zgsSmsSendingRecordLog.getSendingrecord())) {
            queryWrapper.like("sendingrecord", zgsSmsSendingRecordLog.getSendingrecord());
        }
        Page<ZgsSmsSendingRecordLog> page = new Page<ZgsSmsSendingRecordLog>(pageNo, pageSize);
        IPage<ZgsSmsSendingRecordLog> pageList = zgsSmsSendingRecordLogService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsSmsSendingRecordLog
     * @return
     */
    @AutoLog(value = "短信记录表-添加")
    @ApiOperation(value = "短信记录表-添加", notes = "短信记录表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSmsSendingRecordLog zgsSmsSendingRecordLog) {
        zgsSmsSendingRecordLogService.save(zgsSmsSendingRecordLog);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsSmsSendingRecordLog
     * @return
     */
    @AutoLog(value = "短信记录表-编辑")
    @ApiOperation(value = "短信记录表-编辑", notes = "短信记录表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSmsSendingRecordLog zgsSmsSendingRecordLog) {
        zgsSmsSendingRecordLogService.updateById(zgsSmsSendingRecordLog);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "短信记录表-通过id删除")
    @ApiOperation(value = "短信记录表-通过id删除", notes = "短信记录表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSmsSendingRecordLogService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "短信记录表-批量删除")
    @ApiOperation(value = "短信记录表-批量删除", notes = "短信记录表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSmsSendingRecordLogService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "短信记录表-通过id查询")
    @ApiOperation(value = "短信记录表-通过id查询", notes = "短信记录表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsSmsSendingRecordLog zgsSmsSendingRecordLog = zgsSmsSendingRecordLogService.getById(id);
        if (zgsSmsSendingRecordLog == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsSmsSendingRecordLog);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsSmsSendingRecordLog
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsSmsSendingRecordLog zgsSmsSendingRecordLog) {
        return super.exportXls(request, zgsSmsSendingRecordLog, ZgsSmsSendingRecordLog.class, "短信记录表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSmsSendingRecordLog.class);
    }

}
