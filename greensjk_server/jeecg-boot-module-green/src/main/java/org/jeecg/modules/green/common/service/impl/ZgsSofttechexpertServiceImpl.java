package org.jeecg.modules.green.common.service.impl;

import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.common.mapper.ZgsSofttechexpertMapper;
import org.jeecg.modules.green.common.service.IZgsSofttechexpertService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 科技类审批记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Service
public class ZgsSofttechexpertServiceImpl extends ServiceImpl<ZgsSofttechexpertMapper, ZgsSofttechexpert> implements IZgsSofttechexpertService {

    @Override
    public List<ZgsSofttechexpert> getZgsSofttechexpertList(String businessguid) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.getZgsSofttechexpertList(businessguid), ValidateEncryptEntityUtil.isDecrypt);
    }
}
