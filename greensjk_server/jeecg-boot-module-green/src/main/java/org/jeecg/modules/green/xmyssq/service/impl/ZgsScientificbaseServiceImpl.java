package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificbaseMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研项目申报验收基本信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsScientificbaseServiceImpl extends ServiceImpl<ZgsScientificbaseMapper, ZgsScientificbase> implements IZgsScientificbaseService {

}
