package org.jeecg.modules.green.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;

import java.util.List;

/**
 * @Description: 科技项目任务书申报
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
public interface IZgsSciencetechtaskService extends IService<ZgsSciencetechtask> {
    ZgsSciencetechtask getBySciencetechguid(String sciencetechguid, Integer accessType);

    Page<ZgsSciencetechtask> YqLqlist(Page<ZgsSciencetechtask> page, Wrapper<ZgsSciencetechtask> wrapper,String enterpriseguid);

    List<ZgsSciencetechtask> listZgsScienceTechTaskListInfoTask(Wrapper<ZgsSciencetechtask> queryWrapper);

    int spProject(ZgsSciencetechtask zgsSciencetechtask);

    int changeBg(ZgsSciencetechtask zgsSciencetechtask);

    void initProjectSelectById(ZgsSciencetechtask zgsSciencetechtask);

    boolean selectSciencetechExist(String sciencetechguid);

    int updateDealTypeIsNull();

    int updateStageByBid1(String projectstage, String bid, Object status);

    int updateStageByBid2(String projectstage, String bid, Object status);

    int updateStageByBid3(String projectstage, String bid, Object status);

    int updateStageByBid4(String projectstage, String bid, Object status);

    int updateStageByBid5(String projectstage, String bid, Object status);

    int updateStageByBid6(String projectstage, String bid, Object status);

    int updateStageByBid7(String projectstage, String bid, Object status);

    int updateStageByBid8(String projectstage, String bid, Object status);

    int updateStageByBid9(String projectstage, String bid, Object status);

    int updateStageByBid10(String projectstage, String bid, Object status);

    int updateStageByBid11(String projectstage, String bid, Object status);

    int updateStageByBid12(String projectstage, String bid, Object status);

    int updateStageByBid13(String projectstage, String bid, Object status);

    Page<ZgsSciencetechtask> listZgsSciencetechtask(Wrapper<ZgsSciencetechtask> queryWrapper, Integer pageNo, Integer pageSize);
    // 往期 逾期项目查询
    List<ZgsSciencetechtask> zgsScienceTechTaskListForYq(Wrapper<ZgsSciencetechtask> queryWrapper);
}
