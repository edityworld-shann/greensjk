package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergyprojectplanarrange;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsEnergyprojectplanarrangeMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsEnergyprojectplanarrangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 节能建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsEnergyprojectplanarrangeServiceImpl extends ServiceImpl<ZgsEnergyprojectplanarrangeMapper, ZgsEnergyprojectplanarrange> implements IZgsEnergyprojectplanarrangeService {

}
