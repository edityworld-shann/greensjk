package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AssemblyDataStatisticsPo implements Serializable {
    /**
     * 装配式开工总面积
     */
    private String total;
    /**
     * 保障性住房
     */
    private String yearFarArea1;
    /**
     * 商品住房
     */
    private String yearFarArea2;
    /**
     * 公共建筑
     */
    private String yearFarArea3;
    /**
     * 建筑总面积
     */
    private String totalAll;

    private List<Prefabricated> listData;
    private List<Prefabricated> listDataNew;
}

