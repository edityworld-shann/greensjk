package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemblesingleproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 装配式示范工程单体建筑基本信息
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsAssemblesingleprojectService extends IService<ZgsAssemblesingleproject> {

}
