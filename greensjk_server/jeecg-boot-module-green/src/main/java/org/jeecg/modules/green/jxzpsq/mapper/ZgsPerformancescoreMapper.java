package org.jeecg.modules.green.jxzpsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 计划项目执行情况绩效自评表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsPerformancescoreMapper extends BaseMapper<ZgsPerformancescore> {

}
