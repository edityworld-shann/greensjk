package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 三类人员实时信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
@Data
@TableName("zgs_threepersonfinal")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="zgs_threepersonfinal对象", description="三类人员实时信息表")
public class ZgsThreepersonfinal implements Serializable {
    private static final long serialVersionUID = 1L;

	/**唯一编号*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "唯一编号")
    private java.lang.String id;
	/**申请表编号，与业务表.GUID关联*/
	@Excel(name = "申请表编号，与业务表.GUID关联", width = 15)
    @ApiModelProperty(value = "申请表编号，与业务表.GUID关联")
    private java.lang.String applyguid;
	/**企业编码，与EnterpriseInfo.GUID关联*/
	@Excel(name = "企业编码，与EnterpriseInfo.GUID关联", width = 15)
    @ApiModelProperty(value = "企业编码，与EnterpriseInfo.GUID关联")
    private java.lang.String enterpriseguid;
	/**企业行政管辖地*/
	@Excel(name = "企业行政管辖地", width = 15)
    @ApiModelProperty(value = "企业行政管辖地")
    private java.lang.String regioncode;
	/**企业名称*/
	@Excel(name = "企业名称", width = 15)
    @ApiModelProperty(value = "企业名称")
    private java.lang.String enterprisename;
	/**证件类型，0-身份证，1-军官证，2-警官证，3-护照，*/
	@Excel(name = "证件类型，0-身份证，1-军官证，2-警官证，3-护照，", width = 15)
    @ApiModelProperty(value = "证件类型，0-身份证，1-军官证，2-警官证，3-护照，")
    private java.lang.String identitytype;
	/**15身份证号*/
	@Excel(name = "15身份证号", width = 15)
    @ApiModelProperty(value = "15身份证号")
    private java.lang.String identitycard;
	/**18位身份证号*/
	@Excel(name = "18位身份证号", width = 15)
    @ApiModelProperty(value = "18位身份证号")
    private java.lang.String identitycard18;
	/**姓名*/
	@Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private java.lang.String name;
	/**性别，1男/0女*/
	@Excel(name = "性别，1男/0女", width = 15)
    @ApiModelProperty(value = "性别，1男/0女")
    private java.math.BigDecimal sex;
	/**出生年月*/
	@Excel(name = "出生年月", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "出生年月")
    private java.util.Date birthday;
	/**参加考试时间*/
	@Excel(name = "参加考试时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "参加考试时间")
    private java.util.Date examinationdate;
	/**考试成绩*/
	@Excel(name = "考试成绩", width = 15)
    @ApiModelProperty(value = "考试成绩")
    private java.lang.String examinationscore;
	/**毕业院校*/
	@Excel(name = "毕业院校", width = 15)
    @ApiModelProperty(value = "毕业院校")
    private java.lang.String graduateduniversity;
	/**所学专业*/
	@Excel(name = "所学专业", width = 15)
    @ApiModelProperty(value = "所学专业")
    private java.lang.String speciality;
	/**毕业时间*/
	@Excel(name = "毕业时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "毕业时间")
    private java.util.Date graduationtime;
	/**学历，0-高中，1-中专，2-大专，3-本科，4-研究生*/
	@Excel(name = "学历，0-高中，1-中专，2-大专，3-本科，4-研究生", width = 15)
    @ApiModelProperty(value = "学历，0-高中，1-中专，2-大专，3-本科，4-研究生")
    private java.lang.String education;
	/**学位，0-无，1-学士，2-硕士，3-博士*/
	@Excel(name = "学位，0-无，1-学士，2-硕士，3-博士", width = 15)
    @ApiModelProperty(value = "学位，0-无，1-学士，2-硕士，3-博士")
    private java.lang.String degree;
	/**执业资格，空和建造师*/
	@Excel(name = "执业资格，空和建造师", width = 15)
    @ApiModelProperty(value = "执业资格，空和建造师")
    private java.lang.String competence;
	/**照片扫描件*/
	@Excel(name = "照片扫描件", width = 15)
    @ApiModelProperty(value = "照片扫描件")
    private java.lang.String photo;
	/**通讯地址*/
	@Excel(name = "通讯地址", width = 15)
    @ApiModelProperty(value = "通讯地址")
    private java.lang.String address;
	/**企业联系电话*/
	@Excel(name = "企业联系电话", width = 15)
    @ApiModelProperty(value = "企业联系电话")
    private java.lang.String enterpriselinkphone;
	/**企业资质等级，特级、一级、二级、三级、无等级*/
	@Excel(name = "企业资质等级，特级、一级、二级、三级、无等级", width = 15)
    @ApiModelProperty(value = "企业资质等级，特级、一级、二级、三级、无等级")
    private java.lang.String enterpriseaptitude;
	/**邮政编码*/
	@Excel(name = "邮政编码", width = 15)
    @ApiModelProperty(value = "邮政编码")
    private java.lang.String postcode;
	/**电子邮箱*/
	@Excel(name = "电子邮箱", width = 15)
    @ApiModelProperty(value = "电子邮箱")
    private java.lang.String email;
	/**参加工作时间*/
	@Excel(name = "参加工作时间", width = 15)
    @ApiModelProperty(value = "参加工作时间")
    private java.lang.String joinworkdate;
	/**累计从事安全生产管理工作年限*/
	@Excel(name = "累计从事安全生产管理工作年限", width = 15)
    @ApiModelProperty(value = "累计从事安全生产管理工作年限")
    private java.lang.String workyear;
	/**接受安全教育、培训及考核情况*/
	@Excel(name = "接受安全教育、培训及考核情况", width = 15)
    @ApiModelProperty(value = "接受安全教育、培训及考核情况")
    private java.lang.String assessment;
	/**在职状态，0:在职; 1:调动中,默认为0*/
	@Excel(name = "在职状态，0:在职; 1:调动中,默认为0", width = 15)
    @ApiModelProperty(value = "在职状态，0:在职; 1:调动中,默认为0")
    private java.math.BigDecimal workstate;
	/**创建人帐号*/
	@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    private java.lang.String createpersonaccount;
	/**创建人姓名*/
	@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    private java.lang.String createpersonname;
	/**创建时间*/
	@Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createtime;
	/**修改人帐号*/
	@Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    private java.lang.String modifypersonaccount;
	/**修改人姓名*/
	@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    private java.lang.String modifypersonname;
	/**修改时间*/
	@Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifytime;
	/**是否删除，0或null:未删除；1:已删除*/
	@Excel(name = "是否删除，0或null:未删除；1:已删除", width = 15)
    @ApiModelProperty(value = "是否删除，0或null:未删除；1:已删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
	/**有效、注销*/
	@Excel(name = "有效、注销", width = 15)
    @ApiModelProperty(value = "有效、注销")
    private java.lang.String registerstate;
	/**移动电话*/
	@Excel(name = "移动电话", width = 15)
    @ApiModelProperty(value = "移动电话")
    private java.lang.String phone;
	/**固定电话*/
	@Excel(name = "固定电话", width = 15)
    @ApiModelProperty(value = "固定电话")
    private java.lang.String mobilephone;
	/**删除说明*/
	@Excel(name = "删除说明", width = 15)
    @ApiModelProperty(value = "删除说明")
    private java.lang.String delreason;
	/**1:导入;0:业务修改过；2:实时库管理修改;*/
	@Excel(name = "1:导入;0:业务修改过；2:实时库管理修改;", width = 15)
    @ApiModelProperty(value = "1:导入;0:业务修改过；2:实时库管理修改;")
    private java.math.BigDecimal importstate;
	/**打印次数*/
	@Excel(name = "打印次数", width = 15)
    @ApiModelProperty(value = "打印次数")
    private java.math.BigDecimal printcertcount;
	/**最后一次打印日期*/
	@Excel(name = "最后一次打印日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "最后一次打印日期")
    private java.util.Date printcerdate;
	/**自动注销的提示信息;2017.12.18 ljx-add*/
	@Excel(name = "自动注销的提示信息;2017.12.18 ljx-add", width = 15)
    @ApiModelProperty(value = "自动注销的提示信息;2017.12.18 ljx-add")
    private java.lang.String autoprompt;
}
