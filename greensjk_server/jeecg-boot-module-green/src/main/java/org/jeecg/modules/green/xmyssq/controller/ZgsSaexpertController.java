package org.jeecg.modules.green.xmyssq.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsExpertset;
import org.jeecg.modules.green.common.entity.ZgsProjectSoftExpertinfo;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.common.service.IZgsExpertsetService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostelistService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsSaexpertService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 项目验收专家审批记录
 * @Author: jeecg-boot
 * @Date: 2022-03-15
 * @Version: V1.0
 */
@Api(tags = "项目验收专家审批记录")
@RestController
@RequestMapping("/xmyssq/zgsSaexpert")
@Slf4j
public class ZgsSaexpertController extends JeecgController<ZgsSaexpert, IZgsSaexpertService> {
    @Autowired
    private IZgsSaexpertService zgsSaexpertService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;

    /**
     * 分页列表查询
     *
     * @param zgsSaexpert
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-分页列表查询")
    @ApiOperation(value = "项目验收专家审批记录-分页列表查询", notes = "项目验收专家审批记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSaexpert zgsSaexpert,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsSaexpert> queryWrapper = QueryGenerator.initQueryWrapper(zgsSaexpert, req.getParameterMap());
        Page<ZgsSaexpert> page = new Page<ZgsSaexpert>(pageNo, pageSize);
        IPage<ZgsSaexpert> pageList = zgsSaexpertService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsSaexpert
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-添加")
    @ApiOperation(value = "项目验收专家审批记录-添加", notes = "项目验收专家审批记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSaexpert zgsSaexpert) {
        zgsSaexpertService.save(zgsSaexpert);
        return Result.OK("添加成功！");
    }

    /**
     * 专家分配
     *
     * @param zgsProjectSoftExpertinfo
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-专家分配")
    @ApiOperation(value = "项目验收专家审批记录-专家分配", notes = "项目验收专家审批记录-专家分配")
    @PostMapping(value = "/addFp")
    public Result<?> add(@RequestBody ZgsProjectSoftExpertinfo zgsProjectSoftExpertinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectSoftExpertinfo != null && zgsProjectSoftExpertinfo.getZgsExpertinfoList() != null && zgsProjectSoftExpertinfo.getZgsExpertinfoList().size() > 0) {
            String projectId = zgsProjectSoftExpertinfo.getId();
            String sciencetype = zgsProjectSoftExpertinfo.getProjecttypenum();
            if (GlobalConstants.ScientificAcceptance.equals(sciencetype)) {
                sciencetype = GlobalConstants.ScientificAcceptance_VALUE;
            } else if (GlobalConstants.TechAcceptance.equals(sciencetype)) {
                sciencetype = GlobalConstants.TechAcceptance_VALUE;
            } else if (GlobalConstants.MidtermInspection.equals(sciencetype)) {
                sciencetype = GlobalConstants.MidtermInspection_VALUE;
            } else if (GlobalConstants.ScientificPost.equals(sciencetype)) {
                sciencetype = GlobalConstants.ScientificPost_VALUE;
            }
            for (int i = 0; i < zgsProjectSoftExpertinfo.getZgsExpertinfoList().size(); i++) {
                ZgsSaexpert zgsSaexpert = new ZgsSaexpert();
                ZgsExpertinfo zgsExpertinfo = zgsProjectSoftExpertinfo.getZgsExpertinfoList().get(i);
                zgsSaexpert.setId(UUID.randomUUID().toString());
                zgsSaexpert.setExpertguid(zgsExpertinfo.getId());
                zgsSaexpert.setBusinessguid(projectId);
                zgsSaexpert.setBusinesstype(sciencetype);
                zgsSaexpert.setCreatepersonaccount(sysUser.getUsername());
                zgsSaexpert.setCreatepersonname(sysUser.getRealname());
                zgsSaexpert.setCreatetime(new Date());
                zgsSaexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSaexpert, ValidateEncryptEntityUtil.isEncrypt));
                //添加屏蔽专家数据
                ZgsExpertset zgsExpertset = new ZgsExpertset();
                zgsExpertset.setId(UUID.randomUUID().toString());
                zgsExpertset.setExpertguid(zgsExpertinfo.getId());
                zgsExpertset.setBusinessguid(projectId);
                zgsExpertset.setCreatepersonaccount(sysUser.getUsername());
                zgsExpertset.setCreatepersonname(sysUser.getRealname());
                zgsExpertset.setCreatetime(new Date());
                zgsExpertset.setSetvalue(zgsProjectSoftExpertinfo.getSet());
                zgsExpertsetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertset, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 专家-审批
     *
     * @param zgsSaexpert
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-专家-审批")
    @ApiOperation(value = "项目验收专家审批记录-专家-审批", notes = "项目验收专家审批记录-专家-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSaexpert zgsSaexpert) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String businessguid = zgsSaexpert.getId();
        zgsSaexpert.setModifypersonaccount(sysUser.getUsername());
        zgsSaexpert.setModifypersonname(sysUser.getRealname());
        zgsSaexpert.setModifytime(new Date());
        zgsSaexpert.setAuditdate(new Date());
        if (zgsSaexpert != null && StringUtils.isNotEmpty(zgsSaexpert.getAuditconclusionnum())) {
            if (GlobalConstants.AGREE_1.equals(zgsSaexpert.getAuditconclusionnum())) {
                zgsSaexpert.setAuditconclusion("同意");
            } else {
                zgsSaexpert.setAuditconclusion("不同意");
            }
        }
        QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
        zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
        ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
        QueryWrapper<ZgsSaexpert> zgsSaexpertQueryWrapper = new QueryWrapper();
        zgsSaexpertQueryWrapper.eq("businessguid", businessguid);
        zgsSaexpertQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
        zgsSaexpertQueryWrapper.isNull("auditconclusionnum");
        ZgsSaexpert saexpert = zgsSaexpertService.getOne(zgsSaexpertQueryWrapper);
        zgsSaexpert.setId(saexpert.getId());
        zgsSaexpertService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSaexpert, ValidateEncryptEntityUtil.isEncrypt));
        //判断是否为驳回
        QueryWrapper<ZgsSaexpert> saexpertQueryWrapper = new QueryWrapper();
        saexpertQueryWrapper.eq("businessguid", businessguid);
        saexpertQueryWrapper.ne("isdelete", 1);
        List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(saexpertQueryWrapper);
        if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
            int size = zgsSaexpertList.size();
            int count1 = 0;
            int count0 = 0;
            for (ZgsSaexpert saexpert1 : zgsSaexpertList) {
                //1同意0不同意
                String auditconclusionnum = saexpert1.getAuditconclusionnum();
                if (StringUtils.isNotEmpty(auditconclusionnum)) {
                    if (GlobalConstants.AGREE_1.equals(auditconclusionnum)) {
                        count1++;//同意
                    } else {
                        count0++;//不同意
                    }
                }
            }
            String businesstype = zgsSaexpert.getBusinesstype();
            if (GlobalConstants.ScientificAcceptance.equals(businesstype)) {
                if ((count1 + count0) == size) {
                    ZgsScientificbase zgsScientificbase = new ZgsScientificbase();
                    zgsScientificbase.setId(businessguid);
                    if ((double) count1 / size < 0.6) {
                        //同意率小于百分之60直接驳回
                        zgsScientificbase.setAuditopinion("专家审批通过率低于60%直接驳回");
                        zgsScientificbase.setExpertauditresult("不通过");
                        zgsScientificbase.setStatus(GlobalConstants.SHENHE_STATUS6);
                    } else {
                        zgsScientificbase.setAuditopinion(null);
                        zgsScientificbase.setExpertauditresult("通过");
                        zgsScientificbase.setStatus(GlobalConstants.SHENHE_STATUS10);
                    }
                    zgsScientificbaseService.updateById(zgsScientificbase);
                }
            } else if (GlobalConstants.TechAcceptance.equals(businesstype)) {
                if ((count1 + count0) == size) {
                    ZgsDemoprojectacceptance zgsDemoprojectacceptance = new ZgsDemoprojectacceptance();
                    zgsDemoprojectacceptance.setId(businessguid);
                    if ((double) count1 / size < 0.6) {
                        //同意率小于百分之60直接驳回
                        zgsDemoprojectacceptance.setAuditopinion("专家审批通过率低于60%直接驳回");
                        zgsDemoprojectacceptance.setExpertauditresult("不通过");
                        zgsDemoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS6);
                    } else {
                        zgsDemoprojectacceptance.setAuditopinion(null);
                        zgsDemoprojectacceptance.setExpertauditresult("通过");
                        zgsDemoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS10);
                    }
                    zgsDemoprojectacceptanceService.updateById(zgsDemoprojectacceptance);
                }
            } else if (GlobalConstants.MidtermInspection.equals(businesstype)) {
                if ((count1 + count0) == size) {
                    ZgsMidterminspection zgsMidterminspection = new ZgsMidterminspection();
                    zgsMidterminspection.setId(businessguid);
                    if ((double) count1 / size < 0.6) {
                        //同意率小于百分之60直接驳回
                        zgsMidterminspection.setAuditopinion("专家审批通过率低于60%直接驳回");
                        zgsMidterminspection.setExpertauditresult("不通过");
                        zgsMidterminspection.setStatus(GlobalConstants.SHENHE_STATUS6);
                    } else {
                        zgsMidterminspection.setAuditopinion(null);
                        zgsMidterminspection.setExpertauditresult("通过");
                        zgsMidterminspection.setStatus(GlobalConstants.SHENHE_STATUS10);
                    }
                    zgsMidterminspectionService.updateById(zgsMidterminspection);
                }
            } else if (GlobalConstants.ScientificPost.equals(businesstype)) {
                if ((count1 + count0) == size) {
                    ZgsScientificpostbase zgsScientificpostbase = new ZgsScientificpostbase();
                    zgsScientificpostbase.setId(businessguid);
                    if ((double) count1 / size < 0.6) {
                        //同意率小于百分之60直接驳回
                        zgsScientificpostbase.setAuditopinion("专家审批通过率低于60%直接驳回");
                        zgsScientificpostbase.setExpertauditresult("不通过");
                        zgsScientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS6);
                    } else {
                        zgsScientificpostbase.setAuditopinion(null);
                        zgsScientificpostbase.setExpertauditresult("通过");
                        zgsScientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS10);
                    }
                    zgsScientificpostbaseService.updateById(zgsScientificpostbase);
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 编辑
     *
     * @param zgsSaexpert
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-编辑")
    @ApiOperation(value = "项目验收专家审批记录-编辑", notes = "项目验收专家审批记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSaexpert zgsSaexpert) {
        zgsSaexpertService.updateById(zgsSaexpert);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-通过id删除")
    @ApiOperation(value = "项目验收专家审批记录-通过id删除", notes = "项目验收专家审批记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSaexpertService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-批量删除")
    @ApiOperation(value = "项目验收专家审批记录-批量删除", notes = "项目验收专家审批记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSaexpertService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目验收专家审批记录-通过id查询")
    @ApiOperation(value = "项目验收专家审批记录-通过id查询", notes = "项目验收专家审批记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsSaexpert zgsSaexpert = zgsSaexpertService.getById(id);
        if (zgsSaexpert == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsSaexpert);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsSaexpert
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsSaexpert zgsSaexpert) {
        return super.exportXls(request, zgsSaexpert, ZgsSaexpert.class, "项目验收专家审批记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSaexpert.class);
    }

}
