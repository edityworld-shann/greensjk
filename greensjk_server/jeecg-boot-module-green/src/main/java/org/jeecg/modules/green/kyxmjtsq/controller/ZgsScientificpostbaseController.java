package org.jeecg.modules.green.kyxmjtsq.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.kyxmjtsq.entity.*;
import org.jeecg.modules.green.kyxmjtsq.service.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.xmyssq.entity.*;
import org.jeecg.modules.green.xmyssq.service.IZgsSaexpertService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科研项目申报结项基本信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "科研项目申报结项基本信息表")
@RestController
@RequestMapping("/kyxmjtsq/zgsScientificpostbase")
@Slf4j
public class ZgsScientificpostbaseController extends JeecgController<ZgsScientificpostbase, IZgsScientificpostbaseService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsScientificpostelistService zgsScientificpostelistService;
    @Autowired
    private IZgsScientificpostexpendclearService zgsScientificpostexpendclearService;
    @Autowired
    private IZgsScientificpostprojectexecutService zgsScientificpostprojectexecutService;
    @Autowired
    private IZgsScientificpostprojectfinishService zgsScientificpostprojectfinishService;
    @Autowired
    private IZgsScientificpostresearcherService zgsScientificpostresearcherService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsSaexpertService zgsSaexpertService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsBuildjointunitService zgsBuildjointunitService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;

    /**
     * 分页列表查询
     *
     * @param zgsScientificpostbase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-分页列表查询")
    @ApiOperation(value = "科研项目申报结项基本信息表-分页列表查询", notes = "科研项目申报结项基本信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsScientificpostbase zgsScientificpostbase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsScientificpostbase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsScientificpostbase.getProjectname())) {
            queryWrapper.like("projectname", zgsScientificpostbase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsScientificpostbase.getProjectnumber())) {
            queryWrapper.like("projectnumber", zgsScientificpostbase.getProjectnumber());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnumber,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsScientificpostbase.getProjectstage())) {
            if (!"-1".equals(zgsScientificpostbase.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsScientificpostbase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                if (StringUtils.isNotEmpty(zgsScientificpostbase.getStatus())) {
                    if (!"0".equals(zgsScientificpostbase.getStatus())) {
                        queryWrapper.and(qrt -> {
                            qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43)).or().isNull("projectstage");
                        });
                    }
                } else {
                    queryWrapper.and(qrt -> {
                        qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43)).or().isNull("projectstage");
                    });
                }
            }
        }
//        QueryWrapperUtil.initScientificpostbaseSelect(queryWrapper, zgsScientificpostbase.getStatus(), sysUser);
       // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsScientificpostbase.getStatus(), sysUser, 1);
        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    QueryWrapper<ZgsSaexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
                    zgsSofttechexpertQueryWrapper.eq("expertguid", expertId);
                    zgsSofttechexpertQueryWrapper.ne("isdelete", 1);
//                    zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSofttechexpertQueryWrapper);
                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
                        queryWrapper.and(qw -> {
                            qw.and(w1 -> {
                                for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
                                    w1.or(w2 -> {
                                        w2.eq("id", zgsSaexpert.getBusinessguid());
                                    });
                                }
                            });
                        });
                    } else {
                        queryWrapper.eq("id", "null");
                    }
                }
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsScientificpostbase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("isdelete", 1);
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("enddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
           // queryWrapper.last("ORDER BY IF(isnull(applydate),0,1), applydate DESC");

        }
        Page<ZgsScientificpostbase> page = new Page<ZgsScientificpostbase>(pageNo, pageSize);
        // IPage<ZgsScientificpostbase> pageList = zgsScientificpostbaseService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsScientificpostbase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsScientificpostbase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsScientificpostbase> pageList = null;

        // 科技项目结题
        redisUtil.expire("kyxmjt",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmjtOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmjtOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsScientificpostbase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsScientificpostbase.getDspForTjdw())) {
            if (zgsScientificpostbase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("kyxmjt")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsScientificpostbaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmjt",pageListForProjectCount.getTotal());
                }
            }
            if (zgsScientificpostbase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("kyxmjtOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsScientificpostbaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmjtOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsScientificpostbase.getDspForTjdw()) && StringUtils.isBlank(zgsScientificpostbase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsScientificpostbaseService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsScientificpostbase.getDspForTjdw()) && StringUtils.isNotBlank(zgsScientificpostbase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsScientificpostbaseService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsScientificpostbase.getDspForSt()) && zgsScientificpostbase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsScientificpostbaseService.page(page, queryWrapper);
            redisUtil.set("kyxmjtOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsScientificpostbase.getStatus(), sysUser, 1);
            pageList = zgsScientificpostbaseService.page(page, queryWrapper);
        }



        /*// 科技项目结题
        redisUtil.expire("kyxmjt",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjt")) {
            if (StringUtils.isNotBlank(zgsScientificpostbase.getFlagByWorkTable()) && zgsScientificpostbase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("kyxmjt",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmjtOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjtOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsScientificpostbase.getDspForSt()) && zgsScientificpostbase.getDspForSt().equals("2")) {
                redisUtil.set("kyxmjtOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmjtOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjtOfTjdw")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsScientificpostbase.getDspForTjdw()) && zgsScientificpostbase.getDspForTjdw().equals("3")) {
                redisUtil.set("kyxmjtOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsScientificpostbase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //专家审批
                        QueryWrapper<ZgsSaexpert> wrapper = new QueryWrapper();
                        wrapper.eq("expertguid", expertId);
                        wrapper.eq("businessguid", zgsInfo.getId());
                        ZgsSaexpert saexpert = zgsSaexpertService.getOne(wrapper);
                        if (!(saexpert != null && StringUtils.isNotEmpty(saexpert.getAuditconclusionnum()))) {
                            pageList.getRecords().get(m).setSpStatus(2);
                        }
                    }

                    // 如果当前登录角色为个人或单位时 根据授权标识判断是否可进行编辑操作
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                        if (StrUtil.isNotBlank(zgsInfo.getSqbj()) && zgsInfo.getSqbj().equals("1")) {
                            zgsInfo.setUpdateFlag("1");   // 可编辑
                            zgsInfo.setSqbjBol(true);
                        } else if (StrUtil.isNotBlank(zgsInfo.getSqbj()) && zgsInfo.getSqbj().equals("2")) {
                            zgsInfo.setUpdateFlag("2");  // 取消编辑按钮
                            zgsInfo.setSqbjBol(false);
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(zgsInfo.getUpdateSave()) && zgsInfo.getUpdateSave().equals("1")) {  // 编辑未提交
                        zgsInfo.setBjzt("1");
                    } else if (StrUtil.isNotBlank(zgsInfo.getUpdateSave()) && zgsInfo.getUpdateSave().equals("2")) {  // 编辑已提交
                        zgsInfo.setBjzt("2");
                    }


                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsScientificpostbase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    } else if (GlobalConstants.SHENHE_STATUS8.equals(zgsInfo.getStatus())) {
                        //分配专家
//                        QueryWrapper<ZgsSaexpert> zgsSaexpertQueryWrapper = new QueryWrapper();
//                        zgsSaexpertQueryWrapper.eq("businessguid", zgsInfo.getId());
//                        zgsSaexpertQueryWrapper.ne("isdelete", 1);
//                        List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSaexpertQueryWrapper);
//                        if (!(zgsSaexpertList != null && zgsSaexpertList.size() > 0)) {
//                            //专家分配
//                            Calendar calendar = new GregorianCalendar();
//                            calendar.setTime(new Date());
//                            calendar.add(calendar.DATE, -15);//15天前未分配的还可以继续分配，超了15天分配按钮消失
//                            if (calendar.getTime().compareTo(zgsInfo.getApplydate()) < 0) {
//                                pageList.getRecords().get(m).setSpStatus(3);
//                            }
//                        }
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                    queryWrapper0.eq("sciencetechguid", zgsInfo.getScientificbaseguid());
                    queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                    queryWrapper0.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                    if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }

                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(zgsInfo.getUpdateSave()) && zgsInfo.getUpdateSave().equals("1")) {  // 编辑未提交
                        zgsInfo.setBjzt("1");
                    } else if (StrUtil.isNotBlank(zgsInfo.getUpdateSave()) && zgsInfo.getUpdateSave().equals("2")) {  // 编辑已提交
                        zgsInfo.setBjzt("2");
                    }

                    if (StrUtil.isNotBlank(zgsInfo.getSqbj()) && zgsInfo.getSqbj().equals("1")) {  // 授权编辑
                        zgsInfo.setSqbjBol(false);
                        zgsInfo.setUpdateFlag("1");  // 可编辑
                    } else if (StrUtil.isNotBlank(zgsInfo.getSqbj()) && zgsInfo.getSqbj().equals("2")) {  // 取消编辑
                        zgsInfo.setSqbjBol(true);
                        zgsInfo.setUpdateFlag("2");  // 取消编辑
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsScientificpostbase
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-撤回")
    @ApiOperation(value = "科研项目申报结项基本信息表-撤回", notes = "科研项目申报结项基本信息表-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsScientificpostbase zgsScientificpostbase) {
        if (zgsScientificpostbase != null) {
            String id = zgsScientificpostbase.getId();
            String bid = zgsScientificpostbase.getScientificbaseguid();
            String status = zgsScientificpostbase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43).equals(zgsScientificpostbase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsScientificpostbase info = new ZgsScientificpostbase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsScientificpostbaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsScientificpostbase
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-审批")
    @ApiOperation(value = "科研项目申报结项基本信息表-审批", notes = "科研项目申报结项基本信息表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsScientificpostbase zgsScientificpostbase) {
        zgsScientificpostbaseService.spProject(zgsScientificpostbase);
        String projectlibraryguid = zgsScientificpostbaseService.getById(zgsScientificpostbase.getId()).getScientificbaseguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsScientificpostbase
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-添加")
    @ApiOperation(value = "科研项目申报结项基本信息表-添加", notes = "科研项目申报结项基本信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsScientificpostbase zgsScientificpostbase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsScientificpostbase.setId(id);
        zgsScientificpostbase.setEnterpriseguid(sysUser.getEnterpriseguid());
        if (GlobalConstants.handleSubmit.equals(zgsScientificpostbase.getStatus())) {
            //保存并上报
            zgsScientificpostbase.setApplydate(new Date());
        }
        zgsScientificpostbase.setCreateaccountname(sysUser.getUsername());
        zgsScientificpostbase.setCreateusername(sysUser.getRealname());
        zgsScientificpostbase.setCreatedate(new Date());
        //延期提醒
        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -6);
        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper = new QueryWrapper();
        queryWrapper.and(qwp1 -> {
            qwp1.or(qwp2 -> {
                qwp2.eq("projectname", zgsScientificpostbase.getProjectname());
            });
            qwp1.or(qwp2 -> {
                qwp2.eq("id", zgsScientificpostbase.getScientificbaseguid());
            });
        });
        queryWrapper.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
//        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper);
//        if (sciencetechfeasibleList != null && sciencetechfeasibleList.size() > 0) {
//            return Result.error("【按照甘建科〔2018〕234号文件，计划项目应按约定时限完成全部科研产出，并在约定的研究期限结束后3个月内，由第一承担单位通过管理系统提交验收（结题）申请等材料。】");
//        }
        //判断项目终止
//        QueryWrapper<ZgsSciencetechtask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("sciencetechguid", zgsScientificpostbase.getScientificbaseguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsSciencetechtask> listHist = zgsSciencetechtaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsScientificpostbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
        zgsScientificpostbase.setProjectstagestatus(zgsScientificpostbase.getStatus());
        zgsScientificpostbaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostbase, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsScientificpostbase.getStatus()) && StringUtils.isNotEmpty(zgsScientificpostbase.getScientificbaseguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsScientificpostbase.getScientificbaseguid());
        }
//        redisUtil.set(zgsScientificpostbase.getScientificbaseguid(), "验收管理阶段");

        ZgsScientificpostprojectexecut zgsScientificpostprojectexecut = zgsScientificpostbase.getZgsScientificpostprojectexecut();
        zgsScientificpostprojectexecut.setId(UUID.randomUUID().toString());
        zgsScientificpostprojectexecut.setBaseguid(id);
        zgsScientificpostprojectexecut.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsScientificpostprojectexecut.setCreateaccountname(sysUser.getUsername());
        zgsScientificpostprojectexecut.setCreateusername(sysUser.getRealname());
        zgsScientificpostprojectexecut.setCreatedate(new Date());
        zgsScientificpostprojectexecutService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectexecut, ValidateEncryptEntityUtil.isEncrypt));

        ZgsScientificpostexpendclear zgsScientificpostexpendclear = zgsScientificpostbase.getZgsScientificpostexpendclear();
        zgsScientificpostexpendclear.setId(UUID.randomUUID().toString());
        zgsScientificpostexpendclear.setBaseguid(id);
        zgsScientificpostexpendclear.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsScientificpostexpendclear.setCreateaccountname(sysUser.getUsername());
        zgsScientificpostexpendclear.setCreateusername(sysUser.getRealname());
        zgsScientificpostexpendclear.setCreatedate(new Date());
        zgsScientificpostexpendclearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostexpendclear, ValidateEncryptEntityUtil.isEncrypt));

        List<ZgsMattermaterial> zgsMattermaterialList = zgsScientificpostbase.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.ScientificPost);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }
        //科研项目验收申请项目执行情况评价奖励列表
        List<ZgsScientificpostelist> zgsScientificpostelistList = zgsScientificpostbase.getZgsScientificpostelistList();
        if (zgsScientificpostelistList != null && zgsScientificpostelistList.size() > 0) {
            for (int a0 = 0; a0 < zgsScientificpostelistList.size(); a0++) {
                ZgsScientificpostelist zgsScientificpostelist = zgsScientificpostelistList.get(a0);
                zgsScientificpostelist.setId(UUID.randomUUID().toString());
                zgsScientificpostelist.setBaseguid(id);
                zgsScientificpostelist.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificpostelist.setCreateaccountname(sysUser.getUsername());
                zgsScientificpostelist.setCreateusername(sysUser.getRealname());
                zgsScientificpostelist.setCreatedate(new Date());
                zgsScientificpostelistService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostelist, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        List<ZgsScientificpostresearcher> zgsScientificpostresearcherList = zgsScientificpostbase.getZgsScientificpostresearcherList();
        if (zgsScientificpostresearcherList != null && zgsScientificpostresearcherList.size() > 0) {
            if (zgsScientificpostresearcherList.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a1 = 0; a1 < zgsScientificpostresearcherList.size(); a1++) {
                ZgsScientificpostresearcher zgsScientificpostresearcher = zgsScientificpostresearcherList.get(a1);
                zgsScientificpostresearcher.setId(UUID.randomUUID().toString());
                zgsScientificpostresearcher.setBaseguid(id);
                zgsScientificpostresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificpostresearcher.setCreateaccountname(sysUser.getUsername());
                zgsScientificpostresearcher.setCreateusername(sysUser.getRealname());
                zgsScientificpostresearcher.setCreatedate(new Date());
                zgsScientificpostresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        // 因申报阶段已添加 项目完成单位信息    故验收阶段 不再进行单位信息新增和编辑操作
        List<ZgsScientificpostprojectfinish> zgsScientificpostprojectfinishList = zgsScientificpostbase.getZgsScientificpostprojectfinishList();
        if (zgsScientificpostprojectfinishList != null && zgsScientificpostprojectfinishList.size() > 0) {
            if (zgsScientificpostprojectfinishList.size() > 5) {
                return Result.error("项目完成单位情况不得超过5个！");
            }
            for (int a2 = 0; a2 < zgsScientificpostprojectfinishList.size(); a2++) {
                ZgsScientificpostprojectfinish zgsScientificpostprojectfinish = zgsScientificpostprojectfinishList.get(a2);
                zgsScientificpostprojectfinish.setId(UUID.randomUUID().toString());
                zgsScientificpostprojectfinish.setBaseguid(id);
                zgsScientificpostprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificpostprojectfinish.setCreateaccountname(sysUser.getUsername());
                zgsScientificpostprojectfinish.setCreateusername(sysUser.getRealname());
                zgsScientificpostprojectfinish.setCreatedate(new Date());
                zgsScientificpostprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsScientificpostbase
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-编辑")
    @ApiOperation(value = "科研项目申报结项基本信息表-编辑", notes = "科研项目申报结项基本信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsScientificpostbase zgsScientificpostbase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsScientificpostbase != null && StringUtils.isNotEmpty(zgsScientificpostbase.getId())) {
            ZgsScientificpostbase entity = zgsScientificpostbaseService.getById(zgsScientificpostbase.getId());
            if (ObjectUtil.isNotNull(entity)) {
                if (StringUtils.isNotBlank(entity.getSqbj()) && entity.getSqbj().equals("2")) {

                    Result result = new Result();

                    result.setMessage("授权编辑已被取消，已修改信息不能保存");
                    result.setCode(0);
                    result.setSuccess(false);
                    return result;
                } else {
                    String id = zgsScientificpostbase.getId();
                    if (GlobalConstants.handleSubmit.equals(zgsScientificpostbase.getStatus())) {
                        //保存并上报
                        zgsScientificpostbase.setApplydate(new Date());
                        zgsScientificpostbase.setAuditopinion(null);
                     // zgsScientificpostbase.setReturntype(null);
                    } else {
                        zgsScientificpostbase.setApplydate(null);
                        //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                        ZgsScientificpostbase projectTask = zgsScientificpostbaseService.getById(id);
                        if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                            zgsScientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                        }
                    }
                    zgsScientificpostbase.setModifyaccountname(sysUser.getUsername());
                    zgsScientificpostbase.setModifyusername(sysUser.getRealname());
                    zgsScientificpostbase.setModifydate(new Date());
                    zgsScientificpostbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
                    zgsScientificpostbase.setProjectstagestatus(zgsScientificpostbase.getStatus());

                    // 将授权编辑变为 取消授权编辑（说明：终审单位的此次授权编辑已使用完，想要修改，必须重新授权）
                    if (StringUtils.isNotBlank(zgsScientificpostbase.getSqbj()) && StringUtils.isNotBlank(zgsScientificpostbase.getUpdateSave())) {
                        zgsScientificpostbase.setSqbj("2");  // 取消编辑
                        zgsScientificpostbase.setUpdateSave("2");  // 修稿已提交
                    }
                    zgsScientificpostbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostbase, ValidateEncryptEntityUtil.isEncrypt));
                    //实时更新阶段和状态
                    if (GlobalConstants.handleSubmit.equals(zgsScientificpostbase.getStatus()) && StringUtils.isNotEmpty(zgsScientificpostbase.getScientificbaseguid())) {
                        zgsProjectlibraryService.initStageAndStatus(zgsScientificpostbase.getScientificbaseguid());
                    }
                    String enterpriseguid = zgsScientificpostbase.getEnterpriseguid();
                    //科研项目结题申请项目执行情况评价奖励列表
                    List<ZgsScientificpostelist> zgsScientificpostelistList0 = zgsScientificpostbase.getZgsScientificpostelistList();
                    QueryWrapper<ZgsScientificpostelist> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("enterpriseguid", enterpriseguid);
                    queryWrapper1.eq("baseguid", id);
                    queryWrapper1.ne("isdelete", 1);
                    List<ZgsScientificpostelist> zgsScientificpostelistList0_1 = zgsScientificpostelistService.list(queryWrapper1);
                    if (zgsScientificpostelistList0 != null && zgsScientificpostelistList0.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificpostelistList0.size(); a0++) {
                            ZgsScientificpostelist zgsScientificpostelist = zgsScientificpostelistList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificpostelist.getId())) {
                                //编辑
                                zgsScientificpostelist.setModifyaccountname(sysUser.getUsername());
                                zgsScientificpostelist.setModifyusername(sysUser.getRealname());
                                zgsScientificpostelist.setModifydate(new Date());
                                zgsScientificpostelistService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostelist, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificpostelist.setId(UUID.randomUUID().toString());
                                zgsScientificpostelist.setBaseguid(id);
                                zgsScientificpostelist.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificpostelist.setCreateaccountname(sysUser.getUsername());
                                zgsScientificpostelist.setCreateusername(sysUser.getRealname());
                                zgsScientificpostelist.setCreatedate(new Date());
                                zgsScientificpostelistService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostelist, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificpostelist zgsScientificpostelist1 : zgsScientificpostelistList0_1) {
                                if (!zgsScientificpostelist1.getId().equals(zgsScientificpostelist.getId())) {
                                    map0.put(zgsScientificpostelist1.getId(), zgsScientificpostelist1.getId());
                                } else {
                                    list0.add(zgsScientificpostelist1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificpostelistService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificpostelistList0_1 != null && zgsScientificpostelistList0_1.size() > 0) {
                            for (ZgsScientificpostelist zgsScientificpostelist : zgsScientificpostelistList0_1) {
                                zgsScientificpostelistService.removeById(zgsScientificpostelist.getId());
                            }
                        }
                    }

                    //相关附件
                    List<ZgsMattermaterial> zgsMattermaterialList = zgsScientificpostbase.getZgsMattermaterialList();
                    if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                        for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                            ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                            String mattermaterialId = zgsMattermaterial.getId();
                            UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                            wrapper.eq("relationid", mattermaterialId);
                            //先删除原来的再重新添加
                            zgsAttachappendixService.remove(wrapper);

                            zgsMattermaterialService.saveOrUpdate(zgsMattermaterial);

                            if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                                for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                                    ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                                    if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                        zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                        zgsAttachappendix.setModifytime(new Date());
                                    } else {
                                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                                        zgsAttachappendix.setRelationid(mattermaterialId);
                                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                        zgsAttachappendix.setCreatetime(new Date());
                                        zgsAttachappendix.setAppendixtype(GlobalConstants.ScientificPost);
                                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                                    }
                                    zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }

                    //科研项目结题申请项目执行情况评价实体类
                    ZgsScientificpostprojectexecut zgsScientificpostprojectexecut = zgsScientificpostbase.getZgsScientificpostprojectexecut();
                    if (StringUtils.isNotEmpty(zgsScientificpostprojectexecut.getId())) {
                        //编辑
                        zgsScientificpostprojectexecut.setModifyaccountname(sysUser.getUsername());
                        zgsScientificpostprojectexecut.setModifyusername(sysUser.getRealname());
                        zgsScientificpostprojectexecut.setModifydate(new Date());
                        zgsScientificpostprojectexecutService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectexecut, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsScientificpostprojectexecut.setId(UUID.randomUUID().toString());
                        zgsScientificpostprojectexecut.setBaseguid(id);
                        zgsScientificpostprojectexecut.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsScientificpostprojectexecut.setCreateaccountname(sysUser.getUsername());
                        zgsScientificpostprojectexecut.setCreateusername(sysUser.getRealname());
                        zgsScientificpostprojectexecut.setCreatedate(new Date());
                        zgsScientificpostprojectexecutService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectexecut, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    //科研项目结题申请经费决算
                    ZgsScientificpostexpendclear zgsScientificpostexpendclear = zgsScientificpostbase.getZgsScientificpostexpendclear();
                    if (StringUtils.isNotEmpty(zgsScientificpostexpendclear.getId())) {
                        //编辑
                        zgsScientificpostexpendclear.setModifyaccountname(sysUser.getUsername());
                        zgsScientificpostexpendclear.setModifyusername(sysUser.getRealname());
                        zgsScientificpostexpendclear.setModifydate(new Date());
                        zgsScientificpostexpendclearService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostexpendclear, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsScientificpostexpendclear.setId(UUID.randomUUID().toString());
                        zgsScientificpostexpendclear.setBaseguid(id);
                        zgsScientificpostexpendclear.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsScientificpostexpendclear.setCreateaccountname(sysUser.getUsername());
                        zgsScientificpostexpendclear.setCreateusername(sysUser.getRealname());
                        zgsScientificpostexpendclear.setCreatedate(new Date());
                        zgsScientificpostexpendclearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostexpendclear, ValidateEncryptEntityUtil.isEncrypt));
                    }

                    //主要研究人员名单
                    List<ZgsScientificpostresearcher> zgsScientificpostresearcherList0 = zgsScientificpostbase.getZgsScientificpostresearcherList();
                    QueryWrapper<ZgsScientificpostresearcher> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("enterpriseguid", enterpriseguid);
                    queryWrapper3.eq("baseguid", id);
                    queryWrapper3.ne("isdelete", 1);
                    queryWrapper3.orderByAsc("ordernum");
                    List<ZgsScientificpostresearcher> zgsScientificpostresearcherList0_1 = zgsScientificpostresearcherService.list(queryWrapper3);
                    if (zgsScientificpostresearcherList0 != null && zgsScientificpostresearcherList0.size() > 0) {
                        if (zgsScientificpostresearcherList0.size() > 15) {
                            return Result.error("人员名单数量不得超过15人！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificpostresearcherList0.size(); a0++) {
                            ZgsScientificpostresearcher zgsScientificpostresearcher = zgsScientificpostresearcherList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificpostresearcher.getId())) {
                                //编辑
                                zgsScientificpostresearcher.setModifyaccountname(sysUser.getUsername());
                                zgsScientificpostresearcher.setModifyusername(sysUser.getRealname());
                                zgsScientificpostresearcher.setModifydate(new Date());
                                zgsScientificpostresearcherService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificpostresearcher.setId(UUID.randomUUID().toString());
                                zgsScientificpostresearcher.setBaseguid(id);
                                zgsScientificpostresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificpostresearcher.setCreateaccountname(sysUser.getUsername());
                                zgsScientificpostresearcher.setCreateusername(sysUser.getRealname());
                                zgsScientificpostresearcher.setCreatedate(new Date());
                                zgsScientificpostresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificpostresearcher zgsScientificpostresearcher1 : zgsScientificpostresearcherList0_1) {
                                if (!zgsScientificpostresearcher1.getId().equals(zgsScientificpostresearcher.getId())) {
                                    map0.put(zgsScientificpostresearcher1.getId(), zgsScientificpostresearcher1.getId());
                                } else {
                                    list0.add(zgsScientificpostresearcher1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificpostresearcherService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificpostresearcherList0_1 != null && zgsScientificpostresearcherList0_1.size() > 0) {
                            for (ZgsScientificpostresearcher zgsScientificpostresearcher : zgsScientificpostresearcherList0_1) {
                                zgsScientificpostresearcherService.removeById(zgsScientificpostresearcher.getId());
                            }
                        }
                    }
                    //项目完成单位情况    因申报单位已填写单位信息，故此处不再进行编辑操作  rxl 20230810
                    List<ZgsScientificpostprojectfinish> zgsScientificpostprojectfinishList0 = zgsScientificpostbase.getZgsScientificpostprojectfinishList();
                    QueryWrapper<ZgsScientificpostprojectfinish> queryWrapper4 = new QueryWrapper<>();
                    queryWrapper4.eq("enterpriseguid", enterpriseguid);
                    queryWrapper4.eq("baseguid", id);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.orderByAsc("ordernum");
                    List<ZgsScientificpostprojectfinish> zgsScientificpostprojectfinishList0_1 = zgsScientificpostprojectfinishService.list(queryWrapper4);
                    if (zgsScientificpostprojectfinishList0 != null && zgsScientificpostprojectfinishList0.size() > 0) {
                        if (zgsScientificpostprojectfinishList0.size() > 5) {
                            return Result.error("项目完成单位情况不得超过5个！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificpostprojectfinishList0.size(); a0++) {
                            ZgsScientificpostprojectfinish zgsScientificpostprojectfinish = zgsScientificpostprojectfinishList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificpostprojectfinish.getId())) {
                                //编辑
                                zgsScientificpostprojectfinish.setModifyaccountname(sysUser.getUsername());
                                zgsScientificpostprojectfinish.setModifyusername(sysUser.getRealname());
                                zgsScientificpostprojectfinish.setModifydate(new Date());
                                zgsScientificpostprojectfinishService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificpostprojectfinish.setId(UUID.randomUUID().toString());
                                zgsScientificpostprojectfinish.setBaseguid(id);
                                zgsScientificpostprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificpostprojectfinish.setCreateaccountname(sysUser.getUsername());
                                zgsScientificpostprojectfinish.setCreateusername(sysUser.getRealname());
                                zgsScientificpostprojectfinish.setCreatedate(new Date());
                                zgsScientificpostprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificpostprojectfinish zgsScientificpostprojectfinish1 : zgsScientificpostprojectfinishList0_1) {
                                if (!zgsScientificpostprojectfinish1.getId().equals(zgsScientificpostprojectfinish.getId())) {
                                    map0.put(zgsScientificpostprojectfinish1.getId(), zgsScientificpostprojectfinish1.getId());
                                } else {
                                    list0.add(zgsScientificpostprojectfinish1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificpostprojectfinishService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificpostprojectfinishList0_1 != null && zgsScientificpostprojectfinishList0_1.size() > 0) {
                            for (ZgsScientificpostprojectfinish zgsScientificpostprojectfinish : zgsScientificpostprojectfinishList0_1) {
                                zgsScientificpostprojectfinishService.removeById(zgsScientificpostprojectfinish.getId());
                            }
                        }
                    }
                    // 科研项目结题 原有项目涉及的单位信息；   因申报阶段已填写完成单位，故不再进行单位信息编辑操作   rxl 20230810
            /*List<ZgsBuildjointunit> zgsBuildjointunitList = zgsScientificpostbase.getb();
            QueryWrapper<ZgsBuildjointunit> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("buildguid", bid);
            queryWrapper2.ne("isdelete", 1);
            List<ZgsBuildjointunit> zgsBuildjointunitList_0 = zgsBuildjointunitService.list(queryWrapper2);
            if (zgsBuildjointunitList != null && zgsBuildjointunitList.size() > 0) {
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsBuildjointunitList.size(); a0++) {
                    ZgsBuildjointunit zgsBuildjointunit = zgsBuildjointunitList.get(a0);
                    if (StringUtils.isNotEmpty(zgsBuildjointunit.getId())) {
                        //编辑
                        zgsBuildjointunit.setModifyaccountname(sysUser.getUsername());
                        zgsBuildjointunit.setModifyusername(sysUser.getRealname());
                        zgsBuildjointunit.setModifydate(new Date());
                        zgsBuildjointunit = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt);
                        zgsBuildjointunitService.updateById(zgsBuildjointunit);
                    } else {
                        zgsBuildjointunit.setId(UUID.randomUUID().toString());
                        zgsBuildjointunit.setBuildguid(bid);
                        zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                        zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                        zgsBuildjointunit.setCreatedate(new Date());
                        zgsBuildjointunit = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt);
                        zgsBuildjointunitService.save(zgsBuildjointunit);
                    }
                    for (ZgsBuildjointunit plb1 : zgsBuildjointunitList_0) {
                        if (!plb1.getId().equals(zgsBuildjointunit.getId())) {
                            map0.put(plb1.getId(), plb1.getId());
                        } else {
                            list0.add(plb1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsBuildjointunitService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsBuildjointunitList_0 != null && zgsBuildjointunitList_0.size() > 0) {
                    for (ZgsBuildjointunit plb : zgsBuildjointunitList_0) {
                        zgsBuildjointunitService.removeById(plb.getId());
                    }
                }
            }*/

                }

            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-通过id删除")
    @ApiOperation(value = "科研项目申报结项基本信息表-通过id删除", notes = "科研项目申报结项基本信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsScientificpostbaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-批量删除")
    @ApiOperation(value = "科研项目申报结项基本信息表-批量删除", notes = "科研项目申报结项基本信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsScientificpostbaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-通过id查询")
    @ApiOperation(value = "科研项目申报结项基本信息表-通过id查询", notes = "科研项目申报结项基本信息表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsScientificpostbase zgsScientificpostbase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsScientificpostbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificpostbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsScientificpostbase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsScientificpostbase);
            }
            if ((zgsScientificpostbase.getFirstdate() != null || zgsScientificpostbase.getFinishdate() != null) && zgsScientificpostbase.getApplydate() != null) {
                zgsScientificpostbase.setSpLogStatus(1);
            } else {
                zgsScientificpostbase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsScientificpostbase = new ZgsScientificpostbase();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.ScientificPost);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsScientificpostbase.setZgsMattermaterialList(zgsMattermaterialList);
            zgsScientificpostbase.setSpLogStatus(0);
        }
        return Result.OK(zgsScientificpostbase);
    }

    private void initProjectSelectById(ZgsScientificpostbase zgsScientificpostbase) {
        String enterpriseguid = zgsScientificpostbase.getEnterpriseguid();
        String buildguid = zgsScientificpostbase.getId();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //科研项目结题申请项目执行情况评价奖励列表
            QueryWrapper<ZgsScientificpostelist> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            List<ZgsScientificpostelist> zgsScientificpostelistList = zgsScientificpostelistService.list(queryWrapper1);
            zgsScientificpostbase.setZgsScientificpostelistList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificpostelistList, ValidateEncryptEntityUtil.isDecrypt));
            //科研项目结题申请项目执行情况评价实体类
            QueryWrapper<ZgsScientificpostprojectexecut> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("baseguid", buildguid);
            queryWrapper2.ne("isdelete", 1);
            ZgsScientificpostprojectexecut zgsScientificpostprojectexecut = zgsScientificpostprojectexecutService.getOne(queryWrapper2);
            zgsScientificpostbase.setZgsScientificpostprojectexecut(ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificpostprojectexecut, ValidateEncryptEntityUtil.isDecrypt));
            //主要研究人员名单
            QueryWrapper<ZgsScientificpostresearcher> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", buildguid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            queryWrapper3.last("limit 15");
            List<ZgsScientificpostresearcher> zgsScientificpostresearcherList = zgsScientificpostresearcherService.list(queryWrapper3);
            zgsScientificpostbase.setZgsScientificpostresearcherList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificpostresearcherList, ValidateEncryptEntityUtil.isDecrypt));
            //项目完成单位情况  暂不使用
            QueryWrapper<ZgsScientificpostprojectfinish> queryWrapper4 = new QueryWrapper<>();
            // queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", buildguid);
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            queryWrapper4.last("limit 5");
            List<ZgsScientificpostprojectfinish> zgsScientificpostprojectfinishList = zgsScientificpostprojectfinishService.list(queryWrapper4);
            // zgsScientificpostbase.setZgsScientificpostprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificpostprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));


            // 申报阶段_科技攻关项目_申报单位信息   暂不使用
            QueryWrapper<ZgsSciencejointunit> queryWrapper8 = new QueryWrapper<>();
            queryWrapper8.eq("enterpriseguid", enterpriseguid);
            queryWrapper8.eq("scienceguid", zgsScientificpostbase.getScientificbaseguid());
            queryWrapper8.isNull("tag");
            queryWrapper8.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForKjgg = zgsSciencejointunitService.list(queryWrapper8);

            // 申报阶段_软科学_申报单位信息   暂不使用
            QueryWrapper<ZgsSciencejointunit> queryWrapper9 = new QueryWrapper<>();
            queryWrapper9.eq("enterpriseguid", enterpriseguid);
            queryWrapper9.eq("scienceguid", zgsScientificpostbase.getScientificbaseguid());
            queryWrapper9.isNull("tag");
            queryWrapper9.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForRkx = zgsSciencejointunitService.list(queryWrapper9);

            if (zgsSciencejointunitListForKjgg.size() > 0) { // 申报阶段_科技攻关的完成单位信息
                // zgsScientificpostbase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForKjgg, ValidateEncryptEntityUtil.isDecrypt));
            } else if (zgsSciencejointunitListForRkx.size() > 0) { // 申报阶段_软科学的完成单位信息
                // zgsScientificpostbase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForRkx, ValidateEncryptEntityUtil.isDecrypt));
            }

            //项目完成单位情况
            QueryWrapper<ZgsScientificprojectfinish> queryWrapper = new QueryWrapper<>();
            // queryWrapper.eq("enterpriseguid", enterpriseguid);
            queryWrapper.eq("baseguid", zgsScientificpostbase.getScientificbaseguid());
            queryWrapper.ne("isdelete", 1);
            queryWrapper.orderByAsc("ordernum");
            queryWrapper.last("limit 5");
            List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificprojectfinishService.list(queryWrapper);
            zgsScientificpostbase.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));



            //科研项目结题申请经费决算
            QueryWrapper<ZgsScientificpostexpendclear> queryWrapper6 = new QueryWrapper<>();
            queryWrapper6.eq("enterpriseguid", enterpriseguid);
            queryWrapper6.eq("baseguid", buildguid);
            queryWrapper6.ne("isdelete", 1);
            ZgsScientificpostexpendclear zgsScientificpostexpendclear = zgsScientificpostexpendclearService.getOne(queryWrapper6);
            zgsScientificpostbase.setZgsScientificpostexpendclear(ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificpostexpendclear, ValidateEncryptEntityUtil.isDecrypt));
            //相关附件
            map.put("buildguid", buildguid);
            map.put("projecttypenum", GlobalConstants.ScientificPost);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    if (StrUtil.isBlank(zgsMattermaterialList.get(i).getBuildguid())) {
                        zgsMattermaterialList.get(i).setId(UUID.randomUUID().toString());
                        zgsMattermaterialList.get(i).setBuildguid(buildguid); // 针对后期新增的查重报告缺少业务ID问题，进行补充
                    }
                    QueryWrapper<ZgsAttachappendix> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper5.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper5), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsScientificpostbase.setZgsMattermaterialList(zgsMattermaterialList);
            }
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsScientificpostbase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsScientificpostbase zgsScientificpostbase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsScientificpostbase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsScientificpostbase> pageList = (IPage<ZgsScientificpostbase>) result.getResult();
        List<ZgsScientificpostbase> list = pageList.getRecords();
        List<ZgsScientificpostbase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "科研项目申报结项基本信息表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsScientificpostbase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsScientificpostbase, ZgsScientificpostbase.class, "科研项目申报结项基本信息表");
        return mv;
    }

    private String getId(ZgsScientificpostbase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsScientificpostbase.class);
    }


    /**
     * 通过id查询导出word表
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报结项基本信息表-通过id查询导出word表")
    @ApiOperation(value = "科研项目申报结项基本信息表-通过id查询导出word表", notes = "科研项目申报结项基本信息表-通过id查询导出word表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsScientificpostbase zgsScientificpostbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificpostbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsScientificpostbase != null) {
            initProjectSelectById(zgsScientificpostbase);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "12甘肃省建设科技科研项目结项申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsScientificpostbase);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }


    @AutoLog(value = "授权编辑—状态修改")
    @ApiOperation(value = "授权编辑—状态修改", notes = "授权编辑—状态修改")
    @PostMapping(value = "/accreditUpdate")
    public Result<?> accreditUpdate(@RequestBody Map<String,Object> params) {
        Result result = new Result();

        String id = params.get("id").toString();
        String sqbj = params.get("sqbj").toString();  // 授权编辑  1 同意编辑  2 取消编辑
        String updateSave = params.get("updateSave").toString();  // 编辑保存  1 修改未提交  2 修改已提交

        if (StrUtil.isNotBlank(id) && StrUtil.isNotBlank(sqbj) && StrUtil.isNotBlank(updateSave)) {
            ZgsScientificpostbase zgsScientificpostbase = new ZgsScientificpostbase();
            zgsScientificpostbase.setId(id);
            zgsScientificpostbase.setSqbj(sqbj);
            zgsScientificpostbase.setUpdateSave(updateSave);
            if (zgsScientificpostbaseService.updateById(zgsScientificpostbase)){
                result.setMessage("授权编辑成功");
                result.setCode(1);
                result.setSuccess(true);
            } else {
                result.setMessage("授权编辑失败");
                result.setCode(0);
                result.setSuccess(false);
            }
        } else {
            result.setMessage("授权编辑失败");
            result.setCode(0);
            result.setSuccess(false);
        }
        return result;
    }











}
