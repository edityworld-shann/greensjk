package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 项目验收专家审批记录
 * @Author: jeecg-boot
 * @Date: 2022-03-15
 * @Version: V1.0
 */
public interface IZgsSaexpertService extends IService<ZgsSaexpert> {
    List<ZgsSaexpert> getZgsSaexpertList(String businessguid);
}
