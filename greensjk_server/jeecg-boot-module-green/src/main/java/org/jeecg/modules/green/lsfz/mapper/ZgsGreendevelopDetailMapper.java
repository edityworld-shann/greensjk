package org.jeecg.modules.green.lsfz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelopDetail;

/**
 * @describe: 绿色发展项目详情信息
 * @author: renxiaoliang
 * @date: 2023/12/13 14:30
 */
public interface ZgsGreendevelopDetailMapper extends BaseMapper<ZgsGreendevelopDetail> {


}
