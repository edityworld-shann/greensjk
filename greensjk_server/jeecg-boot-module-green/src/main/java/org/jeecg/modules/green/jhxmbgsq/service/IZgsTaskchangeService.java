package org.jeecg.modules.green.jhxmbgsq.service;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsTaskchange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目变更记录
 * @Author: jeecg-boot
 * @Date:   2022-03-15
 * @Version: V1.0
 */
public interface IZgsTaskchangeService extends IService<ZgsTaskchange> {

}
