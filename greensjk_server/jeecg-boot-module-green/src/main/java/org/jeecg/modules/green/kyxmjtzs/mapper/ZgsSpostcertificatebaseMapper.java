package org.jeecg.modules.green.kyxmjtzs.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目结题证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface ZgsSpostcertificatebaseMapper extends BaseMapper<ZgsSpostcertificatebase> {
    List<ZgsSpostcertificatebase> listWarnByFundType(@Param(Constants.WRAPPER) Wrapper<ZgsSpostcertificatebase> queryWrapper);
}
