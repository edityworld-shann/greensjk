package org.jeecg.modules.green.xmyszssq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科研项目验收证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_sacceptcertificatebase")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_sacceptcertificatebase对象", description = "科研项目验收证书")
public class ZgsSacceptcertificatebase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 项目库ID
     */
//    @Excel(name = "项目库ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目库ID")
    private java.lang.String projectlibraryguid;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnumber;
    /**
     * 第一完成单位
     */
    @Excel(name = "第一完成单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "第一完成单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishenterprisename;
    /**
     * 项目负责人姓名
     */
    @Excel(name = "项目负责人姓名", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String principalname;
    /**
     * 学历
     */
    // @Excel(name = "学历", width = 15, orderNum = "2")
    @ApiModelProperty(value = "学历")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String education;
    /**
     * 职称
     */
    //@Excel(name = "职称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String jobtitle;
    /**
     * 手机号码
     */
    @Excel(name = "手机号码", width = 15, orderNum = "2")
    @ApiModelProperty(value = "手机号码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String principalphone;
    /**
     * 工作单位
     */
    @Excel(name = "工作单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "工作单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String workunit;
    /**
     * 邮政编码
     */
    @Excel(name = "邮政编码", width = 15, orderNum = "2")
    @ApiModelProperty(value = "邮政编码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applycode;
    /**
     * 通信地址
     */
    //@Excel(name = "通信地址", width = 15, orderNum = "2")
    @ApiModelProperty(value = "通信地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyaddress;
    /**
     * 电子邮箱
     */
    //@Excel(name = "电子邮箱", width = 15, orderNum = "2")
    @ApiModelProperty(value = "电子邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyemail;
    /**
     * 新产品数
     */
    @Excel(name = "新产品数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "新产品数")
    private java.math.BigDecimal newproductcount;
    /**
     * 新技术、新工艺数
     */
    // @Excel(name = "新技术、新工艺数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "新技术、新工艺数")
    private java.math.BigDecimal newtechnologycount;
    /**
     * 新材料数
     */
    //@Excel(name = "新材料数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "新材料数")
    private java.math.BigDecimal newmaterialcount;
    /**
     * 获专利数
     */
    //@Excel(name = "获专利数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "获专利数")
    private java.math.BigDecimal newpatentedcount;
    /**
     * 国外发明专利数
     */
    //@Excel(name = "国外发明专利数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国外发明专利数")
    private java.math.BigDecimal newabroadcount;
    /**
     * 国内发明专利数
     */
    //@Excel(name = "国内发明专利数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国内发明专利数")
    private java.math.BigDecimal newinlandcount;
    /**
     * 论文篇数
     */
    //@Excel(name = "论文篇数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "论文篇数")
    private java.math.BigDecimal thesiscount;
    /**
     * 国外论文篇数
     */
    @Excel(name = "国外论文篇数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国外论文篇数")
    private java.math.BigDecimal thesisabroadcount;
    /**
     * 国内论文篇数
     */
    //@Excel(name = "国内论文篇数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国内论文篇数")
    private java.math.BigDecimal thesisinlandcount;
    /**
     * 制定标准项数
     */
    //@Excel(name = "制定标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "制定标准项数")
    private java.math.BigDecimal standardcount;
    /**
     * 国际标准项数
     */
    //@Excel(name = "国际标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国际标准项数")
    private java.math.BigDecimal internationstandardcount;
    /**
     * 国家标准项数
     */
    @Excel(name = "国家标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国家标准项数")
    private java.math.BigDecimal nationstandardcount;
    /**
     * 行业标准项数
     */
    //@Excel(name = "行业标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "行业标准项数")
    private java.math.BigDecimal industrialstandardcount;
    /**
     * 地方标准项数
     */
    //@Excel(name = "地方标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "地方标准项数")
    private java.math.BigDecimal provincialstandardcount;
    /**
     * 技术规范项数
     */
    //@Excel(name = "技术规范项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "技术规范项数")
    private java.math.BigDecimal technicalstandardcount;
    /**
     * 企业标准项数
     */
    @Excel(name = "企业标准项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业标准项数")
    private java.math.BigDecimal companystandardcount;
    /**
     * 工法项数
     */
    //@Excel(name = "工法项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "工法项数")
    private java.math.BigDecimal construstandardcount;
    /**
     * 示范点个数
     */
    @Excel(name = "示范点个数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "示范点个数")
    private java.math.BigDecimal demonstrationcount;
    /**
     * 中试线条数
     */
    //@Excel(name = "中试线条数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "中试线条数")
    private java.math.BigDecimal mediumcount;
    /**
     * 生产线条数
     */
    //@Excel(name = "生产线条数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "生产线条数")
    private java.math.BigDecimal productcount;
    /**
     * 培养硕士人数
     */
    @Excel(name = "培养硕士人数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "培养硕士人数")
    private java.math.BigDecimal mastercount;
    /**
     * 培养博士人数
     */
    //@Excel(name = "培养博士人数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "培养博士人数")
    private java.math.BigDecimal doctorcount;
    /**
     * 培养博士后人数
     */
    //@Excel(name = "培养博士后人数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "培养博士后人数")
    private java.math.BigDecimal postdoctorcount;
    /**
     * 获奖项数
     */
    //@Excel(name = "获奖项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "获奖项数")
    private java.math.BigDecimal palmbearcount;
    /**
     * 市级获奖项数
     */
    //@Excel(name = "市级获奖项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "市级获奖项数")
    private java.math.BigDecimal citypalmbearcount;
    /**
     * 省部级获奖项数
     */
    @Excel(name = "省部级获奖项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "省部级获奖项数")
    private java.math.BigDecimal provincepalmbearcount;
    /**
     * 国家级获奖项数
     */
    @Excel(name = "国家级获奖项数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "国家级获奖项数")
    private java.math.BigDecimal nationpalmbearcount;
    /**
     * 其他成果
     */
    //@Excel(name = "其他成果", width = 15, orderNum = "2")
    @ApiModelProperty(value = "其他成果")
    private java.lang.String otherresult;
    /**
     * 成果转让合同数
     */
    //@Excel(name = "成果转让合同数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "成果转让合同数")
    private java.math.BigDecimal resulttransfercount;
    /**
     * 成果转让合同额 万元
     */
    // @Excel(name = "成果转让合同额 万元", width = 15, orderNum = "2")
    @ApiModelProperty(value = "成果转让合同额 万元")
    private java.math.BigDecimal resulttransferlimit;
    /**
     * 已商品化成果数
     */
    //@Excel(name = "已商品化成果数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "已商品化成果数")
    private java.math.BigDecimal commoditizecount;
    /**
     * 实际应用成果数
     */
    // @Excel(name = "实际应用成果数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "实际应用成果数")
    private java.math.BigDecimal practicalusecount;
    /**
     * 已获综合经济效益 万元
     */
    // @Excel(name = "已获综合经济效益 万元", width = 15, orderNum = "2")
    @ApiModelProperty(value = "已获综合经济效益 万元")
    private java.math.BigDecimal gainprofit;
    /**
     * 新增产值 万元
     */
    //@Excel(name = "新增产值 万元", width = 15, orderNum = "2")
    @ApiModelProperty(value = "新增产值 万元")
    private java.math.BigDecimal addoutput;
    /**
     * 新增利税 万元
     */
    //@Excel(name = "新增利税 万元", width = 15, orderNum = "2")
    @ApiModelProperty(value = "新增利税 万元")
    private java.math.BigDecimal addprofittax;
    /**
     * 出口创汇 万元
     */
    //@Excel(name = "出口创汇 万元", width = 15, orderNum = "2")
    @ApiModelProperty(value = "出口创汇 万元")
    private java.math.BigDecimal exportcurrency;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
//    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * createaccountname
     */
//    @Excel(name = "createaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
//    @Excel(name = "createusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
//    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
//    @Excel(name = "modifyaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
//    @Excel(name = "modifyusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
//    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 任务书约定的研究任务
     */
    @Excel(name = "任务书约定的研究任务", width = 15, orderNum = "2")
    @ApiModelProperty(value = "任务书约定的研究任务")
    private java.lang.String researchtask;
    /**
     * 考核目标
     */
    @Excel(name = "考核目标", width = 15, orderNum = "2")
    @ApiModelProperty(value = "考核目标")
    private java.lang.String appraisaltarget;
    /**
     * 技术经济指标
     */
    @Excel(name = "技术经济指标", width = 15, orderNum = "2")
    @ApiModelProperty(value = "技术经济指标")
    private java.lang.String technicaleconomical;
    /**
     * 效益及评价
     */
    @Excel(name = "效益及评价", width = 15, orderNum = "2")
    @ApiModelProperty(value = "效益及评价")
    private java.lang.String benefitevaluation;
    /**
     * 组织管理经验评价
     */
    @Excel(name = "组织管理经验评价", width = 15, orderNum = "2")
    @ApiModelProperty(value = "组织管理经验评价")
    private java.lang.String empiricalevaluation;
    /**
     * 存在的问题
     */
    @Excel(name = "存在的问题", width = 15, orderNum = "2")
    @ApiModelProperty(value = "存在的问题")
    private java.lang.String openquestion;
    /**
     * 申请单位意见
     */
//    @Excel(name = "申请单位意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "申请单位意见")
    private java.lang.String applicantunitopinion;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    private java.lang.String higherupunitnum;
    /**
     * 实际参加研究人员总数
     */
    //@Excel(name = "实际参加研究人员总数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "实际参加研究人员总数")
    private java.math.BigDecimal studyperson;
    /**
     * 高级职称
     */
    // @Excel(name = "高级职称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "高级职称")
    private java.math.BigDecimal studyhigh;
    /**
     * 中级职称
     */
    //@Excel(name = "中级职称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "中级职称")
    private java.math.BigDecimal studmiddle;
    /**
     * 初级职称
     */
    //@Excel(name = "初级职称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初级职称")
    private java.math.BigDecimal studlow;
    /**
     * 博士
     */
    @Excel(name = "博士", width = 15, orderNum = "2")
    @ApiModelProperty(value = "博士")
    private java.math.BigDecimal studdoctor;
    /**
     * 硕士
     */
    //@Excel(name = "硕士", width = 15, orderNum = "2")
    @ApiModelProperty(value = "硕士")
    private java.math.BigDecimal studmaster;
    /**
     * 其他
     */
    //@Excel(name = "其他", width = 15, orderNum = "2")
    @ApiModelProperty(value = "其他")
    private java.math.BigDecimal studother;
    /**
     * 总经费
     */
    // @Excel(name = "总经费", width = 15, orderNum = "2")
    @ApiModelProperty(value = "总经费")
    private java.math.BigDecimal totalexpenditure;
    /**
     * 省级专项资金拨款
     */
    //@Excel(name = "省级专项资金拨款", width = 15, orderNum = "2")
    @ApiModelProperty(value = "省级专项资金拨款")
    private java.math.BigDecimal provinceexpenditure;
    /**
     * 简要技术说明及主要技术性能指标
     */
    //@Excel(name = "简要技术说明及主要技术性能指标", width = 15, orderNum = "2")
    @ApiModelProperty(value = "简要技术说明及主要技术性能指标")
    private java.lang.String technologyexplain;
    /**
     * 推广应用前景与措施
     */
    //@Excel(name = "推广应用前景与措施", width = 15, orderNum = "2")
    @ApiModelProperty(value = "推广应用前景与措施")
    private java.lang.String applyprospect;
    /**
     * 存在的技术问题或不足
     */
    //@Excel(name = "存在的技术问题或不足", width = 15, orderNum = "2")
    @ApiModelProperty(value = "存在的技术问题或不足")
    private java.lang.String technologyquestion;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;

    @ApiModelProperty(value = "成果展示：1 展示； 2 不展示")
    private java.lang.String resultShow;
    // 资料归档
    @ApiModelProperty(value = "文件归档：1 已归档； 2 未归档")
    private java.lang.String fileInduce;

    /**
     * 是否闭合，1：闭合；2019.11.8 ljx-add
     */
//    @Excel(name = "是否闭合，1：闭合；2019.11.8 ljx-add", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否闭合，1：闭合；2019.11.8 ljx-add")
    private java.math.BigDecimal isclose;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(exist = false)
    private List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList;
    @TableField(exist = false)
    private List<ZgsSacceptcertprojectfinish> zgsSacceptcertprojectfinishList;
    @TableField(exist = false)
    private List<ZgsSacceptcertexpert> zgsSacceptcertexpertList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    @TableField(exist = false)
    private Object cstatus;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    // 成果简介信息
    @TableField(exist = false)
    private ZgsPlanresultbase zgsPlanresultbase;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    @TableField(exist = false)
    private List<ZgsSciencejointunit> zgsSciencejointunitList;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
    // 项目完成单位
    @TableField(exist = false)
    private List<ZgsScientificprojectfinish> zgsScientificprojectfinishList;
}
