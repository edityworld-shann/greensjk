package org.jeecg.modules.green.sfxmsb.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.sfxmsb.entity.ZgsBuildproject;
import org.jeecg.modules.green.sfxmsb.service.IZgsBuildprojectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Api(tags="建筑工程业务主表")
@RestController
@RequestMapping("/sfxmsb/zgsBuildproject")
@Slf4j
public class ZgsBuildprojectController extends JeecgController<ZgsBuildproject, IZgsBuildprojectService> {
	@Autowired
	private IZgsBuildprojectService zgsBuildprojectService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsBuildproject
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-分页列表查询")
	@ApiOperation(value="建筑工程业务主表-分页列表查询", notes="建筑工程业务主表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsBuildproject zgsBuildproject,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsBuildproject> queryWrapper = QueryGenerator.initQueryWrapper(zgsBuildproject, req.getParameterMap());
		Page<ZgsBuildproject> page = new Page<ZgsBuildproject>(pageNo, pageSize);
		IPage<ZgsBuildproject> pageList = zgsBuildprojectService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsBuildproject
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-添加")
	@ApiOperation(value="建筑工程业务主表-添加", notes="建筑工程业务主表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsBuildproject zgsBuildproject) {
		zgsBuildprojectService.save(zgsBuildproject);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsBuildproject
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-编辑")
	@ApiOperation(value="建筑工程业务主表-编辑", notes="建筑工程业务主表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsBuildproject zgsBuildproject) {
		zgsBuildprojectService.updateById(zgsBuildproject);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-通过id删除")
	@ApiOperation(value="建筑工程业务主表-通过id删除", notes="建筑工程业务主表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsBuildprojectService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-批量删除")
	@ApiOperation(value="建筑工程业务主表-批量删除", notes="建筑工程业务主表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsBuildprojectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "建筑工程业务主表-通过id查询")
	@ApiOperation(value="建筑工程业务主表-通过id查询", notes="建筑工程业务主表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsBuildproject zgsBuildproject = zgsBuildprojectService.getById(id);
		if(zgsBuildproject==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsBuildproject);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsBuildproject
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsBuildproject zgsBuildproject) {
        return super.exportXls(request, zgsBuildproject, ZgsBuildproject.class, "建筑工程业务主表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsBuildproject.class);
    }

}
