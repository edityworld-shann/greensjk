package org.jeecg.modules.green.report.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 新建建筑节能情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface IZgsReportEnergyNewinfoTotalService extends IService<ZgsReportEnergyNewinfoTotal> {
//    void initEnergyNewinfoTotal(String year, int quarter, String yearMonth);

//    void initEnergyNewinfoTotalCity(String year, int quarter, String yearMonth);

    //type=0(正常执行)、type=1(审批结束过滤执行)
    void initEnergyNewinfoTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState);

    void spProject(String id, Integer spStatus, String backreason);

    ZgsReportEnergyNewinfoTotal lastTotalDataProvince(String filltm, String areacode, Integer applystate);

    ZgsReportEnergyNewinfoTotal lastTotalDataCity(String filltm, String areacode, Integer applystate);

    ZgsReportEnergyNewinfoTotal getYearNewBuildArea(String filltm, String areacode);

    void initEnergyNewinfoTotalZero(String year, int quarter, String yearMonth);

    void energyNewinfoTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser);

    ZgsReportEnergyNewinfoTotal queryEnergNewInfoList(String areacode, String filltm);



    ZgsReportEnergyNewinfoTotal getAreadByCityCode(String filltm, String areacode, Integer applystate);

    ZgsReportEnergyNewinfoTotal lastTotalDataProvinceByCityCode(String filltm, String areacode, Integer applystate);

}
