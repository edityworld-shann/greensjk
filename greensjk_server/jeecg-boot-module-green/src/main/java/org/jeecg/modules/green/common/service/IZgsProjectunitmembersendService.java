package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 账号短信下发表
 * @Author: jeecg-boot
 * @Date:   2022-05-06
 * @Version: V1.0
 */
public interface IZgsProjectunitmembersendService extends IService<ZgsProjectunitmembersend> {

}
