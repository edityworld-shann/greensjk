package org.jeecg.modules.green.task.service;

import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 任务书项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface IZgsTaskprojectmainparticipantService extends IService<ZgsTaskprojectmainparticipant> {

}
