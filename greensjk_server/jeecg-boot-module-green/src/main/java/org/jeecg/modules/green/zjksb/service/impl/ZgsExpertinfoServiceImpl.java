package org.jeecg.modules.green.zjksb.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertinfoMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 专家库表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertinfoServiceImpl extends ServiceImpl<ZgsExpertinfoMapper, ZgsExpertinfo> implements IZgsExpertinfoService {
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    @Override
    public int spProject(ZgsExpertinfo zgsExpertinfo) {
        //审核状态：0未上报,1待审核 8形审通过,9形审退回
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
        zgsReturnrecord.setId(UUID.randomUUID().toString());
        zgsReturnrecord.setBusinessguid(zgsExpertinfo.getId());
        zgsReturnrecord.setReturnreason(zgsExpertinfo.getAuditopinion());
        zgsReturnrecord.setReturndate(new Date());
        zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
        zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
        zgsReturnrecord.setCreatetime(new Date());
        zgsReturnrecord.setReturnperson(sysUser.getRealname());
        zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE22);
        zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
        zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
        zgsReturnrecord.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsReturnrecord.setEnterprisename(sysUser.getEnterprisename());
        zgsReturnrecord.setProjectlibraryguid(zgsExpertinfo.getId());
        //
        zgsExpertinfo.setFinishperson(sysUser.getRealname());
        zgsExpertinfo.setFinishdate(new Date());
        zgsExpertinfo.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
        if (zgsExpertinfo.getSpStatus() == 1) {
            //通过记录状态==0
            zgsReturnrecord.setReturntype(new BigDecimal(0));
            zgsExpertinfo.setStatus(GlobalConstants.SHENHE_STATUS8);
        } else {
            //驳回-退回（补正修改） 不通过记录状态==1
            zgsReturnrecord.setReturnreason(zgsExpertinfo.getAuditopinion());
            zgsReturnrecord.setReturntype(new BigDecimal(1));
            if (zgsExpertinfo.getSpStatus() == 2) {
                zgsExpertinfo.setStatus(GlobalConstants.SHENHE_STATUS9);
            } else {
                zgsExpertinfo.setStatus(GlobalConstants.SHENHE_STATUS14);
            }
        }
        zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
        return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertinfo, ValidateEncryptEntityUtil.isEncrypt));
    }
}
