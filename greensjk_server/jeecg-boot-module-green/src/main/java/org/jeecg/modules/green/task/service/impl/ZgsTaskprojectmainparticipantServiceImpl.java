package org.jeecg.modules.green.task.service.impl;

import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.mapper.ZgsTaskprojectmainparticipantMapper;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 任务书项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
@Service
public class ZgsTaskprojectmainparticipantServiceImpl extends ServiceImpl<ZgsTaskprojectmainparticipantMapper, ZgsTaskprojectmainparticipant> implements IZgsTaskprojectmainparticipantService {

}
