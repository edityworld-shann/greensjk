package org.jeecg.modules.green.sfxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sfxmsb.entity.ZgsSamplebuildproject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 市政公用工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsSamplebuildprojectMapper extends BaseMapper<ZgsSamplebuildproject> {

}
