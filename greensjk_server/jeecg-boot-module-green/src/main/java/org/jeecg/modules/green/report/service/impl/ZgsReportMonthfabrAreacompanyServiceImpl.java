package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreacompany;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreacompanyMapper;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaprojectMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreacompanyService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaprojectService;
import org.springframework.stereotype.Service;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreacompanyServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreacompanyMapper, ZgsReportMonthfabrAreacompany> implements IZgsReportMonthfabrAreacompanyService {

}
