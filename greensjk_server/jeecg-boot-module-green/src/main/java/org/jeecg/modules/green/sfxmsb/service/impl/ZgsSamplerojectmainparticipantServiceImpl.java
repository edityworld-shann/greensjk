package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsSamplerojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsSamplerojectmainparticipantMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsSamplerojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 市政公用建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsSamplerojectmainparticipantServiceImpl extends ServiceImpl<ZgsSamplerojectmainparticipantMapper, ZgsSamplerojectmainparticipant> implements IZgsSamplerojectmainparticipantService {

}
