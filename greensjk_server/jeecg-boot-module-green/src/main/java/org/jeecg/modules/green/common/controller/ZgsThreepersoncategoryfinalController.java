package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsThreepersoncategoryfinal;
import org.jeecg.modules.green.common.service.IZgsThreepersoncategoryfinalService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 三类人员类别实时表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
@Api(tags="三类人员类别实时表")
@RestController
@RequestMapping("/common/zgsThreepersoncategoryfinal")
@Slf4j
public class ZgsThreepersoncategoryfinalController extends JeecgController<ZgsThreepersoncategoryfinal, IZgsThreepersoncategoryfinalService> {
	@Autowired
	private IZgsThreepersoncategoryfinalService zgsThreepersoncategoryfinalService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsThreepersoncategoryfinal
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-分页列表查询")
	@ApiOperation(value="三类人员类别实时表-分页列表查询", notes="三类人员类别实时表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsThreepersoncategoryfinal zgsThreepersoncategoryfinal,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsThreepersoncategoryfinal> queryWrapper = QueryGenerator.initQueryWrapper(zgsThreepersoncategoryfinal, req.getParameterMap());
		Page<ZgsThreepersoncategoryfinal> page = new Page<ZgsThreepersoncategoryfinal>(pageNo, pageSize);
		IPage<ZgsThreepersoncategoryfinal> pageList = zgsThreepersoncategoryfinalService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsThreepersoncategoryfinal
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-添加")
	@ApiOperation(value="三类人员类别实时表-添加", notes="三类人员类别实时表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsThreepersoncategoryfinal zgsThreepersoncategoryfinal) {
		zgsThreepersoncategoryfinalService.save(zgsThreepersoncategoryfinal);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsThreepersoncategoryfinal
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-编辑")
	@ApiOperation(value="三类人员类别实时表-编辑", notes="三类人员类别实时表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsThreepersoncategoryfinal zgsThreepersoncategoryfinal) {
		zgsThreepersoncategoryfinalService.updateById(zgsThreepersoncategoryfinal);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-通过id删除")
	@ApiOperation(value="三类人员类别实时表-通过id删除", notes="三类人员类别实时表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsThreepersoncategoryfinalService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-批量删除")
	@ApiOperation(value="三类人员类别实时表-批量删除", notes="三类人员类别实时表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsThreepersoncategoryfinalService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "三类人员类别实时表-通过id查询")
	@ApiOperation(value="三类人员类别实时表-通过id查询", notes="三类人员类别实时表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsThreepersoncategoryfinal zgsThreepersoncategoryfinal = zgsThreepersoncategoryfinalService.getById(id);
		if(zgsThreepersoncategoryfinal==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsThreepersoncategoryfinal);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsThreepersoncategoryfinal
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsThreepersoncategoryfinal zgsThreepersoncategoryfinal) {
        return super.exportXls(request, zgsThreepersoncategoryfinal, ZgsThreepersoncategoryfinal.class, "三类人员类别实时表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsThreepersoncategoryfinal.class);
    }

}
