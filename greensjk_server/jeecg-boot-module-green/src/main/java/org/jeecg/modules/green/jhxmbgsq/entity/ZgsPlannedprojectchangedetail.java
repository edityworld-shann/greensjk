package org.jeecg.modules.green.jhxmbgsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建设科技计划项目变更内容记录表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_plannedprojectchangedetail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_plannedprojectchangedetail对象", description = "建设科技计划项目变更内容记录表")
public class ZgsPlannedprojectchangedetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 业务主表ID
     */
    @Excel(name = "业务主表ID", width = 15)
    @ApiModelProperty(value = "业务主表ID")
    private java.lang.String baseguid;
    /**
     * 变更内容
     */
    @Excel(name = "变更内容", width = 15)
    @ApiModelProperty(value = "变更内容")
    private java.lang.String changecontent;
    /**
     * 变更理由
     */
    @Excel(name = "变更理由", width = 15)
    @ApiModelProperty(value = "变更理由")
    private java.lang.String changereason;
    /**
     * 变更日志
     */
    @Excel(name = "变更日志", width = 15)
    @ApiModelProperty(value = "变更日志")
    private java.lang.String changelog;
    /**
     * 字段类型
     */
    @Excel(name = "字段类型", width = 15)
    @ApiModelProperty(value = "字段类型")
    private java.lang.String itemKey;
    /**
     * 排序号
     */
    @Excel(name = "排序号", width = 15)
    @ApiModelProperty(value = "排序号")
    private java.math.BigDecimal ordernum;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;

    /*@ApiModelProperty(value = "变更字段")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String changeColumn;*/
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

}
