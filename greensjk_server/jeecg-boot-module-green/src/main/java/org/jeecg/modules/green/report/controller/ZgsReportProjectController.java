package org.jeecg.modules.green.report.controller;

import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.service.IZgsAttachappendixService;
import org.jeecg.modules.green.common.service.IZgsMattermaterialService;
import org.jeecg.modules.green.report.entity.ZgsReportProject;
import org.jeecg.modules.green.report.service.IZgsReportProjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 绿色建筑和建筑节能示范项目库
 * @Author: jeecg-boot
 * @Date: 2022-08-02
 * @Version: V1.0
 */
@Api(tags = "绿色建筑和建筑节能示范项目库")
@RestController
@RequestMapping("/report/zgsReportProject")
@Slf4j
public class ZgsReportProjectController extends JeecgController<ZgsReportProject, IZgsReportProjectService> {
    @Autowired
    private IZgsReportProjectService zgsReportProjectService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;

    /**
     * 分页列表查询
     *
     * @param zgsReportProject
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-分页列表查询")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-分页列表查询", notes = "绿色建筑和建筑节能示范项目库-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportProject zgsReportProject,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (StringUtils.isNotEmpty(zgsReportProject.getProjectname())) {
            zgsReportProject.setProjectname("*" + zgsReportProject.getProjectname() + "*");
        }
        if (StringUtils.isEmpty(zgsReportProject.getAreacode())) {
            zgsReportProject.setAreacode(sysUser.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                zgsReportProject.setAreacode("*" + sysUser.getAreacode() + "*");
            }
        }
        QueryWrapper<ZgsReportProject> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportProject, req.getParameterMap());
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            queryWrapper.and(w2 -> {
                w2.eq("applystate", GlobalConstants.apply_state1).or().eq("applystate", GlobalConstants.apply_state2);
            });
        }
        Page<ZgsReportProject> page = new Page<ZgsReportProject>(pageNo, pageSize);
        IPage<ZgsReportProject> pageList = zgsReportProjectService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 审批
     *
     * @param zgsReportProject
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-审批")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-审批", notes = "绿色建筑和建筑节能示范项目库-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsReportProject zgsReportProject) {
        zgsReportProjectService.spProject(zgsReportProject);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsReportProject
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-添加")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-添加", notes = "绿色建筑和建筑节能示范项目库-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportProject zgsReportProject) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        if (GlobalConstants.handleSubmit.equals(zgsReportProject.getApplystate())) {
            //保存并上报
            zgsReportProject.setApplydate(new Date());
        } else {
            zgsReportProject.setApplydate(null);
        }
        zgsReportProject.setId(id);
        zgsReportProject.setCreateaccountname(sysUser.getUsername());
        zgsReportProject.setCreateusername(sysUser.getRealname());
        zgsReportProject.setCreatedate(new Date());
        zgsReportProject.setAreacode(sysUser.getAreacode());
        zgsReportProject.setAreaname(sysUser.getAreaname());
        zgsReportProject.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsReportProjectService.save(zgsReportProject);
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsReportProject.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a0 = 0; a0 < zgsMattermaterialList.size(); a0++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a0);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a1 = 0; a1 < zgsMattermaterial.getZgsAttachappendixList().size(); a1++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a1);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.ZgsReportProject);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(zgsAttachappendix);
                    }
                }
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportProject
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-编辑")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-编辑", notes = "绿色建筑和建筑节能示范项目库-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportProject zgsReportProject) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (GlobalConstants.handleSubmit.equals(zgsReportProject.getApplystate())) {
            //保存并上报
            zgsReportProject.setApplydate(new Date());
        } else {
            zgsReportProject.setApplydate(null);
        }
        zgsReportProject.setModifyaccountname(sysUser.getUsername());
        zgsReportProject.setModifyusername(sysUser.getRealname());
        zgsReportProject.setModifydate(new Date());
        zgsReportProjectService.updateById(zgsReportProject);
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsReportProject.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                String mattermaterialId = zgsMattermaterial.getId();
                UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                wrapper.eq("relationid", mattermaterialId);
                //先删除原来的再重新添加
                zgsAttachappendixService.remove(wrapper);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                            zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                            zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                            zgsAttachappendix.setModifytime(new Date());
                        } else {
                            zgsAttachappendix.setId(UUID.randomUUID().toString());
                            zgsAttachappendix.setRelationid(mattermaterialId);
                            zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                            zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                            zgsAttachappendix.setCreatetime(new Date());
                            zgsAttachappendix.setAppendixtype(GlobalConstants.ZgsReportProject);
                            zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        }
                        zgsAttachappendixService.save(zgsAttachappendix);
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-通过id删除")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-通过id删除", notes = "绿色建筑和建筑节能示范项目库-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportProjectService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-批量删除")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-批量删除", notes = "绿色建筑和建筑节能示范项目库-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportProjectService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绿色建筑和建筑节能示范项目库-通过id查询")
    @ApiOperation(value = "绿色建筑和建筑节能示范项目库-通过id查询", notes = "绿色建筑和建筑节能示范项目库-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsReportProject zgsReportProject = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsReportProject = zgsReportProjectService.getById(id);
            if (zgsReportProject == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsReportProject);
            }
        } else {
            //新增时查出附件
            zgsReportProject = new ZgsReportProject();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.ZgsReportProject);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsReportProject.setZgsMattermaterialList(zgsMattermaterialList);
        }

        return Result.OK(zgsReportProject);
    }

    private void initProjectSelectById(ZgsReportProject zgsReportProject) {
        Map<String, Object> map = new HashMap<>();
        //相关附件
        map.put("buildguid", zgsReportProject.getId());
        //后期查下TechAcceptance出现在哪张表里
        map.put("projecttypenum", GlobalConstants.ZgsReportProject);
        List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                queryWrapper2.ne("isdelete", 1);
                List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                    for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                        zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                    }
                }
                zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
            }
            zgsReportProject.setZgsMattermaterialList(zgsMattermaterialList);
        }
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsReportProject
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportProject zgsReportProject) {
//        return super.exportXls(request, zgsReportProject, ZgsReportProject.class, "绿色建筑和建筑节能示范项目库");
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        IPage<ZgsReportProject> pageList = (IPage<ZgsReportProject>) queryPageList(zgsReportProject, 1, 99999, request).getResult();
        List<ZgsReportProject> list = pageList.getRecords();
        List<ZgsReportProject> exportList = null;
        String selections = request.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        String title = "绿色建筑和建筑节能示范项目库";
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportProject.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        return mv;
    }

    private String getId(ZgsReportProject item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportProject.class);
    }

}
