package org.jeecg.modules.green.kyxmjtzs.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostprojectfinishService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostresearcherService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.jeecg.modules.green.xmyszssq.entity.*;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科研项目结题证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "科研项目结题证书")
@RestController
@RequestMapping("/kyxmjtzs/zgsSpostcertificatebase")
@Slf4j
public class ZgsSpostcertificatebaseController extends JeecgController<ZgsSpostcertificatebase, IZgsSpostcertificatebaseService> {
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsSpostcertificatebaseService zgsSpostcertificatebaseService;
    @Autowired
    private IZgsSpostprojectfinishService zgsSpostprojectfinishService;
    @Autowired
    private IZgsSpostresearcherService zgsSpostresearcherService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;

    /**
     * 分页列表查询
     *
     * @param zgsSpostcertificatebase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科研项目结题证书-分页列表查询")
    @ApiOperation(value = "科研项目结题证书-分页列表查询", notes = "科研项目结题证书-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSpostcertificatebase zgsSpostcertificatebase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsSpostcertificatebase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectname())) {
            queryWrapper.like("projectname", zgsSpostcertificatebase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectnumber())) {
            queryWrapper.like("projectnumber", zgsSpostcertificatebase.getProjectnumber());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnumber,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectstage())) {
            if (!"-1".equals(zgsSpostcertificatebase.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsSpostcertificatebase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initSpostcertificateSelect(queryWrapper, zgsSpostcertificatebase.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSpostcertificatebase.getStatus(), sysUser, 1);
        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsSpostcertificatebase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("isdelete", 1);
        //TODO 修改根据初审日期进行降序 2022-09-21
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
//            queryWrapper.orderByDesc("createdate");
//            queryWrapper.orderByDesc("applydate");
            queryWrapper.orderByDesc("firstdate");
            queryWrapper.orderByDesc("applydate");
        }
        Page<ZgsSpostcertificatebase> page = new Page<ZgsSpostcertificatebase>(pageNo, pageSize);
        // IPage<ZgsSpostcertificatebase> pageList = zgsSpostcertificatebaseService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsSpostcertificatebase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsSpostcertificatebase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsSpostcertificatebase> pageList = null;

        // 科研项目结题证书
        redisUtil.expire("kyxmjtzs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmjtzsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmjtzsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsSpostcertificatebase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsSpostcertificatebase.getDspForTjdw())) {
            if (zgsSpostcertificatebase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("kyxmjtzs")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsSpostcertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmjtzs",pageListForProjectCount.getTotal());
                }
            }
            if (zgsSpostcertificatebase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("kyxmjtzsOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsSpostcertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmjtzsOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsSpostcertificatebase.getDspForTjdw()) && StringUtils.isBlank(zgsSpostcertificatebase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsSpostcertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsSpostcertificatebase.getDspForTjdw()) && StringUtils.isNotBlank(zgsSpostcertificatebase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsSpostcertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsSpostcertificatebase.getDspForSt()) && zgsSpostcertificatebase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsSpostcertificatebaseService.page(page, queryWrapper);
            redisUtil.set("kyxmjtzsOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSpostcertificatebase.getStatus(), sysUser, 1);
            pageList = zgsSpostcertificatebaseService.page(page, queryWrapper);
        }



        /*// 科研项目结题证书
        redisUtil.expire("kyxmjtzs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjtzs")) {
            if (StringUtils.isNotBlank(zgsSpostcertificatebase.getFlagByWorkTable()) && zgsSpostcertificatebase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("kyxmjtzs",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmjtzsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjtzsOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsSpostcertificatebase.getDspForSt()) && zgsSpostcertificatebase.getDspForSt().equals("2")) {
                redisUtil.set("kyxmjtzsOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmjtzsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmjtzsOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsSpostcertificatebase.getDspForTjdw()) && zgsSpostcertificatebase.getDspForTjdw().equals("3")) {
                redisUtil.set("kyxmjtzsOfTjdw",pageList.getTotal());
            }
        }*/

        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSpostcertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSpostcertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //终审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                    queryWrapper0.eq("sciencetechguid", zgsInfo.getProjectlibraryguid());
                    queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                    queryWrapper0.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                    if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsSpostcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目结题证书-撤回")
    @ApiOperation(value = "科研项目结题证书-撤回", notes = "科研项目结题证书-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsSpostcertificatebase zgsSpostcertificatebase) {
        if (zgsSpostcertificatebase != null) {
            String id = zgsSpostcertificatebase.getId();
            String bid = zgsSpostcertificatebase.getProjectlibraryguid();
            String status = zgsSpostcertificatebase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53).equals(zgsSpostcertificatebase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsSpostcertificatebase info = new ZgsSpostcertificatebase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsSpostcertificatebaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsSpostcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目结题证书-审批")
    @ApiOperation(value = "科研项目结题证书-审批", notes = "科研项目结题证书-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSpostcertificatebase zgsSpostcertificatebase) {
        zgsSpostcertificatebaseService.spProject(zgsSpostcertificatebase);
        String projectlibraryguid = zgsSpostcertificatebaseService.getById(zgsSpostcertificatebase.getId()).getProjectlibraryguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsSpostcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目结题证书-添加")
    @ApiOperation(value = "科研项目结题证书-添加", notes = "科研项目结题证书-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSpostcertificatebase zgsSpostcertificatebase) {
        // 1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsSpostcertificatebase.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsSpostcertificatebase.getStatus())) {
            //保存并上报
            zgsSpostcertificatebase.setApplydate(new Date());
        } else {
            zgsSpostcertificatebase.setApplydate(null);
        }
        zgsSpostcertificatebase.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsSpostcertificatebase.setCreateaccountname(sysUser.getUsername());
        zgsSpostcertificatebase.setCreateusername(sysUser.getRealname());
        zgsSpostcertificatebase.setCreatedate(new Date());
        //判断是否添加验收结题项目，否则给予提醒，需先添加验收结题项目
        QueryWrapper<ZgsScientificpostbase> queryWrapper = new QueryWrapper();
        queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS2).or().eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectname())) {
                qwp1.like("projectname", zgsSpostcertificatebase.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectlibraryguid())) {
                qwp1.or().eq("scientificbaseguid", zgsSpostcertificatebase.getProjectlibraryguid());
            }
        });
//        List<ZgsScientificpostbase> list = zgsScientificpostbaseService.list(queryWrapper);
//        if (list.size() == 0) {
//            return Result.error("请您先添加科研项目结题申请，形审通过后方可添加此项！");
//        }
        //判断项目终止
//        QueryWrapper<ZgsSciencetechtask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("sciencetechguid", zgsSpostcertificatebase.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsSciencetechtask> listHist = zgsSciencetechtaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsSpostcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
        zgsSpostcertificatebase.setProjectstagestatus(zgsSpostcertificatebase.getStatus());
        zgsSpostcertificatebaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostcertificatebase, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsSpostcertificatebase.getStatus()) && StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectlibraryguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsSpostcertificatebase.getProjectlibraryguid());
        }
//        redisUtil.set(zgsSpostcertificatebase.getProjectlibraryguid(), "验收结题证书阶段");
        //主要研究人员名单
        List<ZgsSpostresearcher> zgsSpostresearcherList = zgsSpostcertificatebase.getZgsSpostresearcherList();
        if (zgsSpostresearcherList != null && zgsSpostresearcherList.size() > 0) {
            if (zgsSpostresearcherList.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a1 = 0; a1 < zgsSpostresearcherList.size(); a1++) {
                ZgsSpostresearcher zgsSpostresearcher = zgsSpostresearcherList.get(a1);
                zgsSpostresearcher.setId(UUID.randomUUID().toString());
                zgsSpostresearcher.setBaseguid(id);
                zgsSpostresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSpostresearcher.setCreateaccountname(sysUser.getUsername());
                zgsSpostresearcher.setCreateusername(sysUser.getRealname());
                zgsSpostresearcher.setCreatedate(new Date());
                zgsSpostresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        //完成单位情况     因申报阶段已填写项目完成单位信息，故验收结题证书阶段不再进行新增或编辑操作    rxl 20230809
        /*List<ZgsSpostprojectfinish> zgsSpostprojectfinishList = zgsSpostcertificatebase.getZgsSpostprojectfinishList();
        if (zgsSpostprojectfinishList != null && zgsSpostprojectfinishList.size() > 0) {
            if (zgsSpostprojectfinishList.size() > 5) {
                return Result.error("项目完成单位情况不得超过5个！");
            }
            for (int a1 = 0; a1 < zgsSpostprojectfinishList.size(); a1++) {
                ZgsSpostprojectfinish zgsSpostprojectfinish = zgsSpostprojectfinishList.get(a1);
                zgsSpostprojectfinish.setId(UUID.randomUUID().toString());
                zgsSpostprojectfinish.setBaseguid(id);
                zgsSpostprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSpostprojectfinish.setCreateaccountname(sysUser.getUsername());
                zgsSpostprojectfinish.setCreateusername(sysUser.getRealname());
                zgsSpostprojectfinish.setCreatedate(new Date());
                zgsSpostprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
            }
        }*/

        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsSpostcertificatebase.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.SPostCertificate);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }

        //验收专家名单
        List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSpostcertificatebase.getZgsSacceptcertexpertList();
        String baseguid = zgsSpostcertificatebase.getProjectlibraryguid();
        if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
            for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList.get(a1);
                zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                zgsSacceptcertexpert.setBaseguid(baseguid);
                zgsSacceptcertexpert.setProjectStage("ysjd");  // 验收阶段
                zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                zgsSacceptcertexpert.setCreatedate(new Date());
                zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsSpostcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目结题证书-编辑")
    @ApiOperation(value = "科研项目结题证书-编辑", notes = "科研项目结题证书-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSpostcertificatebase zgsSpostcertificatebase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSpostcertificatebase != null && StringUtils.isNotEmpty(zgsSpostcertificatebase.getId())) {
            String id = zgsSpostcertificatebase.getId();
            if (GlobalConstants.handleSubmit.equals(zgsSpostcertificatebase.getStatus())) {
                //保存并上报
                zgsSpostcertificatebase.setApplydate(new Date());
                zgsSpostcertificatebase.setAuditopinion(null);
//                zgsSpostcertificatebase.setReturntype(null);
            } else {
                zgsSpostcertificatebase.setApplydate(null);
                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                ZgsSpostcertificatebase projectTask = zgsSpostcertificatebaseService.getById(id);
                if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                    zgsSpostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                }
            }
            zgsSpostcertificatebase.setModifyaccountname(sysUser.getUsername());
            zgsSpostcertificatebase.setModifyusername(sysUser.getRealname());
            zgsSpostcertificatebase.setModifydate(new Date());
            zgsSpostcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
            zgsSpostcertificatebase.setProjectstagestatus(zgsSpostcertificatebase.getStatus());
            zgsSpostcertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostcertificatebase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsSpostcertificatebase.getStatus()) && StringUtils.isNotEmpty(zgsSpostcertificatebase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsSpostcertificatebase.getProjectlibraryguid());
            }
            String enterpriseguid = zgsSpostcertificatebase.getEnterpriseguid();
            //主要研究人员名单
            List<ZgsSpostresearcher> zgsSpostresearcherList0 = zgsSpostcertificatebase.getZgsSpostresearcherList();
            QueryWrapper<ZgsSpostresearcher> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", id);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsSpostresearcher> zgsSpostresearcherList0_1 = zgsSpostresearcherService.list(queryWrapper1);
            if (zgsSpostresearcherList0 != null && zgsSpostresearcherList0.size() > 0) {
                if (zgsSpostresearcherList0.size() > 15) {
                    return Result.error("人员名单数量不得超过15人！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsSpostresearcherList0.size(); a0++) {
                    ZgsSpostresearcher zgsSpostresearcher = zgsSpostresearcherList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsSpostresearcher.getId())) {
                        //编辑
                        zgsSpostresearcher.setModifyaccountname(sysUser.getUsername());
                        zgsSpostresearcher.setModifyusername(sysUser.getRealname());
                        zgsSpostresearcher.setModifydate(new Date());
                        zgsSpostresearcherService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsSpostresearcher.setId(UUID.randomUUID().toString());
                        zgsSpostresearcher.setBaseguid(id);
                        zgsSpostresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsSpostresearcher.setCreateaccountname(sysUser.getUsername());
                        zgsSpostresearcher.setCreateusername(sysUser.getRealname());
                        zgsSpostresearcher.setCreatedate(new Date());
                        zgsSpostresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostresearcher, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsSpostresearcher zgsSpostresearcher1 : zgsSpostresearcherList0_1) {
                        if (!zgsSpostresearcher1.getId().equals(zgsSpostresearcher.getId())) {
                            map0.put(zgsSpostresearcher1.getId(), zgsSpostresearcher1.getId());
                        } else {
                            list0.add(zgsSpostresearcher1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsSpostresearcherService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsSpostresearcherList0_1 != null && zgsSpostresearcherList0_1.size() > 0) {
                    for (ZgsSpostresearcher zgsSpostresearcher : zgsSpostresearcherList0_1) {
                        zgsSpostresearcherService.removeById(zgsSpostresearcher.getId());
                    }
                }
            }

            //完成单位情况     因申报阶段已填写项目完成单位信息，故项目验收结题证书阶段不再进行新增或编辑操作   rxl  20230809
            /*List<ZgsSpostprojectfinish> zgsSpostprojectfinishList0 = zgsSpostcertificatebase.getZgsSpostprojectfinishList();
            QueryWrapper<ZgsSpostprojectfinish> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", id);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            List<ZgsSpostprojectfinish> zgsSpostprojectfinishList0_1 = zgsSpostprojectfinishService.list(queryWrapper3);
            if (zgsSpostprojectfinishList0 != null && zgsSpostprojectfinishList0.size() > 0) {
                if (zgsSpostprojectfinishList0.size() > 5) {
                    return Result.error("项目完成单位情况不得超过5个！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsSpostprojectfinishList0.size(); a0++) {
                    ZgsSpostprojectfinish zgsSpostprojectfinish = zgsSpostprojectfinishList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsSpostprojectfinish.getId())) {
                        //编辑
                        zgsSpostprojectfinish.setModifyaccountname(sysUser.getUsername());
                        zgsSpostprojectfinish.setModifyusername(sysUser.getRealname());
                        zgsSpostprojectfinish.setModifydate(new Date());
                        zgsSpostprojectfinishService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsSpostprojectfinish.setId(UUID.randomUUID().toString());
                        zgsSpostprojectfinish.setBaseguid(id);
                        zgsSpostprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsSpostprojectfinish.setCreateaccountname(sysUser.getUsername());
                        zgsSpostprojectfinish.setCreateusername(sysUser.getRealname());
                        zgsSpostprojectfinish.setCreatedate(new Date());
                        zgsSpostprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSpostprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsSpostprojectfinish zgsSpostprojectfinish1 : zgsSpostprojectfinishList0_1) {
                        if (!zgsSpostprojectfinish1.getId().equals(zgsSpostprojectfinish.getId())) {
                            map0.put(zgsSpostprojectfinish1.getId(), zgsSpostprojectfinish1.getId());
                        } else {
                            list0.add(zgsSpostprojectfinish1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsSpostprojectfinishService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsSpostprojectfinishList0_1 != null && zgsSpostprojectfinishList0_1.size() > 0) {
                    for (ZgsSpostprojectfinish zgsSpostprojectfinish : zgsSpostprojectfinishList0_1) {
                        zgsSpostprojectfinishService.removeById(zgsSpostprojectfinish.getId());
                    }
                }
            }*/

            // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
            QueryWrapper<ZgsSacceptcertexpert> expertQueryWrapper = new QueryWrapper<>();
            expertQueryWrapper.eq("baseguid",zgsSpostcertificatebase.getProjectlibraryguid());
            expertQueryWrapper.eq("project_stage","ysjd");
            expertQueryWrapper.ne("isdelete",1);
            List<ZgsSacceptcertexpert> ZgsSacceptcertexpertList = zgsSacceptcertexpertService.list(expertQueryWrapper);
            if (ZgsSacceptcertexpertList.size() > 0) {
                // 删除历史专家信息
                QueryWrapper<ZgsSacceptcertexpert> expertDeleteWrapper = new QueryWrapper<>();
                expertDeleteWrapper.eq("baseguid",zgsSpostcertificatebase.getProjectlibraryguid());
                expertDeleteWrapper.eq("project_stage","ysjd");
                expertDeleteWrapper.ne("isdelete",1);
                boolean bolResult = zgsSacceptcertexpertService.remove(expertDeleteWrapper);
                // 删除成功后，保存修改后的专家信息  rxl  20230620
                if (bolResult) {
                    List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSpostcertificatebase.getZgsSacceptcertexpertList();
                    if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
                        for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                            ZgsSacceptcertexpert zgsSacceptcertexpertForUpdate = zgsSacceptcertexpertList.get(a1);
                            zgsSacceptcertexpertForUpdate.setId(UUID.randomUUID().toString());
                            zgsSacceptcertexpertForUpdate.setBaseguid(zgsSpostcertificatebase.getProjectlibraryguid());
                            zgsSacceptcertexpertForUpdate.setProjectStage("ysjd");   // 申报阶段
                            zgsSacceptcertexpertForUpdate.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsSacceptcertexpertForUpdate.setCreateaccountname(sysUser.getUsername());
                            zgsSacceptcertexpertForUpdate.setCreateusername(sysUser.getRealname());
                            zgsSacceptcertexpertForUpdate.setCreatedate(new Date());
                            zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpertForUpdate, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }


            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsSpostcertificatebase.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.SPostCertificate);
                                zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目结题证书-通过id删除")
    @ApiOperation(value = "科研项目结题证书-通过id删除", notes = "科研项目结题证书-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSpostcertificatebaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科研项目结题证书-批量删除")
    @ApiOperation(value = "科研项目结题证书-批量删除", notes = "科研项目结题证书-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSpostcertificatebaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目结题证书-通过id查询")
    @ApiOperation(value = "科研项目结题证书-通过id查询", notes = "科研项目结题证书-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsSpostcertificatebase zgsSpostcertificatebase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsSpostcertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsSpostcertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsSpostcertificatebase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsSpostcertificatebase);
            }
            if ((zgsSpostcertificatebase.getFirstdate() != null || zgsSpostcertificatebase.getFinishdate() != null) && zgsSpostcertificatebase.getApplydate() != null) {
                zgsSpostcertificatebase.setSpLogStatus(1);
            } else {
                zgsSpostcertificatebase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsSpostcertificatebase = new ZgsSpostcertificatebase();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.SPostCertificate);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsSpostcertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            zgsSpostcertificatebase.setSpLogStatus(0);
        }
        return Result.OK(zgsSpostcertificatebase);
    }

    private void initProjectSelectById(ZgsSpostcertificatebase zgsSpostcertificatebase) {
        String enterpriseguid = zgsSpostcertificatebase.getEnterpriseguid();
        String buildguid = zgsSpostcertificatebase.getId();
        String baseguid = zgsSpostcertificatebase.getProjectlibraryguid();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //主要研究人员名单
            QueryWrapper<ZgsSpostresearcher> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            queryWrapper1.last("limit 15");
            List<ZgsSpostresearcher> zgsSpostresearcherList = zgsSpostresearcherService.list(queryWrapper1);
            zgsSpostcertificatebase.setZgsSpostresearcherList(ValidateEncryptEntityUtil.validateDecryptList(zgsSpostresearcherList, ValidateEncryptEntityUtil.isDecrypt));
            //相关附件
            map.put("buildguid", buildguid);
            //后期查下SPostCertificate出现在哪张表里
            map.put("projecttypenum", GlobalConstants.SPostCertificate);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsSpostcertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //完成单位情况
            QueryWrapper<ZgsSpostprojectfinish> queryWrapper3 = new QueryWrapper<>();
            // queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", buildguid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            queryWrapper3.last("limit 5");
            List<ZgsSpostprojectfinish> zgsSpostprojectfinishList = zgsSpostprojectfinishService.list(queryWrapper3);
            zgsSpostcertificatebase.setZgsSpostprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsSpostprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));

            // 申报阶段_科技攻关项目_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper8 = new QueryWrapper<>();
            // queryWrapper8.eq("enterpriseguid", enterpriseguid);
            queryWrapper8.eq("scienceguid", zgsSpostcertificatebase.getProjectlibraryguid());
            queryWrapper8.isNull("tag");
            queryWrapper8.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForKjgg = zgsSciencejointunitService.list(queryWrapper8);

            // 申报阶段_软科学_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper9 = new QueryWrapper<>();
            // queryWrapper9.eq("enterpriseguid", enterpriseguid);
            queryWrapper9.eq("scienceguid", zgsSpostcertificatebase.getProjectlibraryguid());
            queryWrapper9.isNull("tag");
            queryWrapper9.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForRkx = zgsSciencejointunitService.list(queryWrapper9);

            if (zgsSciencejointunitListForKjgg.size() > 0) { // 申报阶段_科技攻关的完成单位信息
                // zgsSpostcertificatebase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForKjgg, ValidateEncryptEntityUtil.isDecrypt));
            } else if (zgsSciencejointunitListForRkx.size() > 0) { // 申报阶段_软科学的完成单位信息
                // zgsSpostcertificatebase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForRkx, ValidateEncryptEntityUtil.isDecrypt));
            }

            //项目完成单位情况
            QueryWrapper<ZgsScientificprojectfinish> queryWrapper7 = new QueryWrapper<>();
            // queryWrapper7.eq("enterpriseguid", enterpriseguid);
            queryWrapper7.eq("baseguid", zgsSpostcertificatebase.getProjectlibraryguid());
            queryWrapper7.ne("isdelete", 1);
            queryWrapper7.orderByAsc("ordernum");
            queryWrapper7.last("limit 5");
            List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificprojectfinishService.list(queryWrapper7);
            zgsSpostcertificatebase.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));


            //验收专家名单
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", baseguid);
            queryWrapper4.eq("project_stage", "ysjd");
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertexpertService.list(queryWrapper4);
            zgsSpostcertificatebase.setZgsSacceptcertexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertexpertList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param zgsSpostcertificatebase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsSpostcertificatebase zgsSpostcertificatebase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsSpostcertificatebase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsSpostcertificatebase> pageList = (IPage<ZgsSpostcertificatebase>) result.getResult();
        List<ZgsSpostcertificatebase> list = pageList.getRecords();
        List<ZgsSpostcertificatebase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "科研项目结题证书";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsSpostcertificatebase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsSpostcertificatebase, ZgsSpostcertificatebase.class, "科研项目结题证书");
        return mv;

    }

    private String getId(ZgsSpostcertificatebase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSpostcertificatebase.class);
    }

    /**
     * 通过id查询导出word
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目结题证书-通过id查询导出word")
    @ApiOperation(value = "科研项目结题证书-通过id查询导出word", notes = "科研项目结题证书-通过id查询导出word")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsSpostcertificatebase zgsSpostcertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsSpostcertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsSpostcertificatebase != null) {
            // 科研项目结题证书_个人单位导出的“结题意见”修改为空  rxl 20230411
            zgsSpostcertificatebase.setOpinion(null); // 结题意见为申报单位提出，省厅导出须去除结题意见

            initProjectSelectById(zgsSpostcertificatebase);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "16甘肃省建设科技科研项目结题证书.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName); 
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsSpostcertificatebase);
        //完成单位情况-start
        StringBuffer sb = new StringBuffer();
        // if (templateMap != null && zgsSpostcertificatebase.getZgsSpostprojectfinishList() != null) {
        //     if (zgsSpostcertificatebase.getZgsSpostprojectfinishList().size() > 0) {
        //         for (int i = 0; i < zgsSpostcertificatebase.getZgsSpostprojectfinishList().size(); i++) {
        //             ZgsSpostprojectfinish zgsSpostprojectfinish = zgsSpostcertificatebase.getZgsSpostprojectfinishList().get(i);
        //             sb.append(zgsSpostprojectfinish.getEnterprisename());
        //             if (i < zgsSpostcertificatebase.getZgsSpostprojectfinishList().size() - 1) {
        //                 sb.append("、");
        //             }
        //         }
        //     }
        //     templateMap.put("enterprisenameList", sb.toString());
        // }

        if (templateMap != null && null != zgsSpostcertificatebase && zgsSpostcertificatebase.getZgsScientificprojectfinishList() != null) {
            if (zgsSpostcertificatebase.getZgsScientificprojectfinishList().size() > 0) {
                for (int i = 0; i < zgsSpostcertificatebase.getZgsScientificprojectfinishList().size(); i++) {
                    ZgsScientificprojectfinish zgsSpostprojectfinish = zgsSpostcertificatebase.getZgsScientificprojectfinishList().get(i);
                    sb.append(zgsSpostprojectfinish.getEnterprisename());
                    if (i < zgsSpostcertificatebase.getZgsScientificprojectfinishList().size() - 1) {
                        sb.append("、");
                    }
                }
            }
            templateMap.put("enterprisenameList", sb.toString());
        }

        //完成单位情况-end
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

    /**
     * @describe: 文件归档
     * @author: renxiaoliang
     * @date: 2024/1/12 17:03
     */
    @AutoLog(value = "项目文件归档")
    @ApiOperation(value = "项目文件归档", notes = "项目文件归档")
    @PostMapping(value = "/induce")
    public Result<?> fileInduce(@RequestBody Map<String,Object> params) {
        Result result = new Result();
        UpdateWrapper<ZgsSpostcertificatebase> updateWrapper = new UpdateWrapper<>();
        String id = params.get("id").toString();
        String fileState = params.get("fileState").toString();
        if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(fileState)) {
            updateWrapper.set("file_induce", fileState);
            updateWrapper.eq("id", id);
            boolean bol = zgsSpostcertificatebaseService.update(updateWrapper);
            if (fileState.equals("1") && bol) {
                result.setSuccess(true);
                result.setMessage("已归档");
            } else if (fileState.equals("2") && bol) {
                result.setSuccess(true);
                result.setMessage("取消归档");
            } else {
                result.setSuccess(false);
                result.setMessage("归档失败");
            }

        }

        return result;

    }

}
