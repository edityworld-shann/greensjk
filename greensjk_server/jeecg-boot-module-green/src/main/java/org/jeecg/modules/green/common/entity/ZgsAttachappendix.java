package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 多附件信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_attachappendix")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_attachappendix对象", description = "多附件信息表")
public class ZgsAttachappendix implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 外键，关联的业务表主键ID
     */
    @Excel(name = "外键，关联的业务表主键ID", width = 15)
    @ApiModelProperty(value = "外键，关联的业务表主键ID")
    private java.lang.String relationid;
    /**
     * 附件类型，EnterprisePerson
     */
    @Excel(name = "附件类型，EnterprisePerson", width = 15)
    @ApiModelProperty(value = "附件类型，EnterprisePerson")
    private java.lang.String appendixtype;
    /**
     * 附件子类型,EnterpriseBase等
     */
    @Excel(name = "附件子类型,EnterpriseBase等", width = 15)
    @ApiModelProperty(value = "附件子类型,EnterpriseBase等")
    private java.lang.String appendixsubtype;
    /**
     * 附件名称
     */
    @Excel(name = "附件名称", width = 15)
    @ApiModelProperty(value = "附件名称")
    private java.lang.String appendixname;
    /**
     * 上传文件的实际名称
     */
    @Excel(name = "上传文件的实际名称", width = 15)
    @ApiModelProperty(value = "上传文件的实际名称")
    private java.lang.String filename;
    /**
     * 附件扩展名,保存.jpg .gif .png扩展名为小写
     */
    @Excel(name = "附件扩展名,保存.jpg .gif .png扩展名为小写", width = 15)
    @ApiModelProperty(value = "附件扩展名,保存.jpg .gif .png扩展名为小写")
    private java.lang.String appendixextension;
    /**
     * 附件路径/Appendix/EnterpriseID/Enterprise/
     */
    @Excel(name = "附件路径/Appendix/EnterpriseID/Enterprise/", width = 15)
    @ApiModelProperty(value = "附件路径/Appendix/EnterpriseID/Enterprise/")
    private java.lang.String appendixpath;
    /**
     * 上传时间
     */
    @Excel(name = "上传时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上传时间")
    private java.util.Date uploaddate;
    /**
     * 是否删除，1删除
     */
    @Excel(name = "是否删除，1删除", width = 15)
    @ApiModelProperty(value = "是否删除，1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createtime;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifytime;
    @TableField(exist = false)
    private java.lang.String fileOnlineUrl;
    private Integer isflag;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
