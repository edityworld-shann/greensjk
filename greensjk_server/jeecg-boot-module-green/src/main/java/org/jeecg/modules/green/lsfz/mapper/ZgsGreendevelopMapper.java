package org.jeecg.modules.green.lsfz.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelop;

/**
 * @describe: 绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/13 14:30
 */
public interface ZgsGreendevelopMapper extends BaseMapper<ZgsGreendevelop> {

    Page<ZgsGreendevelop> greenDevelopList(Page<ZgsGreendevelop> page, @Param(Constants.WRAPPER) Wrapper<ZgsGreendevelop> queryWrapper);



}
