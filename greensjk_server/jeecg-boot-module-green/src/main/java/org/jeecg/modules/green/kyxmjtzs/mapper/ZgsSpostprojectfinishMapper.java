package org.jeecg.modules.green.kyxmjtzs.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研结题证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsSpostprojectfinishMapper extends BaseMapper<ZgsSpostprojectfinish> {

}
