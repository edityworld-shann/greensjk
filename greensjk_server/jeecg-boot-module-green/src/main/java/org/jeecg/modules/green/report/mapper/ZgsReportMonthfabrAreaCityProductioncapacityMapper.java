package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProductioncapacity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式建筑-生产产能-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaCityProductioncapacityMapper extends BaseMapper<ZgsReportMonthfabrAreaCityProductioncapacity> {
    ZgsReportMonthfabrAreaCityProductioncapacity totalMonthfabrAreaDataMonth(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

//    ZgsReportMonthfabrAreaCityProductioncapacity totalMonthfabrAreaDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityProductioncapacity totalMonthfabrCityDataMonth(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

//    ZgsReportMonthfabrAreaCityProductioncapacity totalMonthfabrCityDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    //省厅账号计算当前月份、当前行政区划合计值
    ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataProvince(@Param("filltm") String filltm, @Param("applystate") Integer applystate);

    //市州账号计算当前月份、当前行政区划合计值
    ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    /**
     * 滚屏 - 新开工 装配式建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionFabricatAreaData(@Param("filltm") String filltm);

    /**
     * 滚屏 - 新开工 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionAreaData(@Param("filltm") String filltm);


    /**
     * 滚屏 -  新建 - 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewBuildingAreaData(@Param("filltm") String filltm);

    /**
     * 滚屏 - 绿色 - 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingAreaData(@Param("filltm") String filltm);

    /**
     * 滚屏 - 绿色 - 建筑等级
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingLevelData(@Param("filltm") String filltm);
}
