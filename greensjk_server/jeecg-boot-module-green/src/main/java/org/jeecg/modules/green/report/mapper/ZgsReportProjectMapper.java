package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 绿色建筑和建筑节能示范项目库
 * @Author: jeecg-boot
 * @Date:   2022-08-02
 * @Version: V1.0
 */
public interface ZgsReportProjectMapper extends BaseMapper<ZgsReportProject> {

}
