package org.jeecg.modules.green.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;

import java.util.List;
import java.util.Map;

/**
 * @Description: 示范项目任务书
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
public interface IZgsProjecttaskService extends IService<ZgsProjecttask> {
    Page<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfo(Wrapper<ZgsProjecttask> queryWrapper, Integer pageNo, Integer pageSize);

    List<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfoTask(Wrapper<ZgsProjecttask> queryWrapper);

    int spProject(ZgsProjecttask zgsProjecttask);

    int changeBg(ZgsProjecttask zgsProjecttask);

    Object queryByProjectGuid(String projectguid, Integer accessType);

    String queryByProjectGuid(String projectguid);

    List<ZgsPlannedchangeparticipant> queryTaskparticipantList(String taskguid);

    ZgsProjecttask getByProjectGuid(String projectguid, Integer accessType);

    void initProjectSelectById(ZgsProjecttask zgsProjecttask);

    boolean selectProjectExist(String projectguid);

    String initFundType(String baseguid);

    List<ZgsProjecttask> zgsProjectTaskListForYq(Wrapper<ZgsProjecttask> queryWrapper);

}
