package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityCompletedMapper;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityProductioncapacityMapper;
import org.jeecg.modules.green.report.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 装配式建筑-已竣工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreaCityCompletedServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaCityCompletedMapper, ZgsReportMonthfabrAreaCityCompleted> implements IZgsReportMonthfabrAreaCityCompletedService {
    @Autowired
    private ZgsReportMonthfabrAreaCityCompletedMapper zgsReportMonthfabrAreaCityCompletedMapper;
    @Autowired
    private IZgsReportMonthfabrAreaCityProjectService zgsReportMonthfabrAreaCityProjectService;


    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService zgsReportMonthfabrAreaCityCompanyService;

    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService zgsReportMonthfabrAreaCityProductioncapacityService;
    @Autowired
    private ZgsReportMonthfabrAreaCityProductioncapacityMapper zgsReportMonthfabrAreaCityProductioncapacityMapper;

    @Override
    public ZgsReportMonthfabrAreaproject queryProjectByReportTime(@Param("reporttm") String reporttm) {
        return zgsReportMonthfabrAreaCityCompletedMapper.queryProjectByReportTime(reporttm);
    }


    /**
     * 清零 0
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrAreaTotalZero(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        double newConstructionArea_Y = 0;
        double completedArea_Y = 0;
        //计算累计值newConstructionArea、completedArea
        ZgsReportMonthfabrAreaCityCompleted cityDataYear = this.baseMapper.totalMonthfabrCityToProDataYear(yearMonth, sysUser.getAreacode(), year, projecttype);
        newConstructionArea_Y = cityDataYear.getYearArea().doubleValue();
        completedArea_Y = cityDataYear.getYearArea().doubleValue();
        //
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        zgsReportMonthfabrAreaCityCompleted.setBackreason("");
//        BeanUtils.copyProperties(zgsReportEnergyNewinfo, zgsReportMonthfabrAreaCityCompleted);
        //计算当月上报合计
        //1、查汇总当年最新一条上报记录
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper();
        queryWrapper.ne("isdelete", 1);
        queryWrapper.eq("year", year);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.orderByDesc("applydate");
        queryWrapper.last("LIMIT 1");
        ZgsReportMonthfabrAreaCityCompleted energyNewinfoTotal = null;
        //新方案：每次上报重新计算年累计值
        energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, sysUser.getAreacode(), year, projecttype);

        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityCompleted reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
        //3、本月记录+当年最新一条上报记录
        zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthBaildType("5");
        zgsReportMonthfabrAreaCityCompleted.setMonthStructureType("5");
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(new BigDecimal(0));
        if (projecttype == "1") {
            zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea));  //  手填数据
            zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(new BigDecimal(0));
            if (energyNewinfoTotal != null && energyNewinfoTotal.getYearFarArea() != null && newConstructionArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(newConstructionArea_Y));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (newConstructionArea_Y) * 100)));
            }
        } else if (projecttype == "2") {
            zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea));  //  手填数据
            zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(new BigDecimal(0));
            if (energyNewinfoTotal != null && energyNewinfoTotal.getYearFarArea() != null && completedArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(completedArea_Y));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (completedArea_Y) * 100)));
            }
        }
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(new BigDecimal(0));
        //  年
        zgsReportMonthfabrAreaCityCompleted.setYearBaildType("5");
        zgsReportMonthfabrAreaCityCompleted.setYearStructureType("5");

        energyNewinfoTotal = energyNewinfoTotal == null ? new ZgsReportMonthfabrAreaCityCompleted() : energyNewinfoTotal;

        zgsReportMonthfabrAreaCityCompleted.setYearFarArea(energyNewinfoTotal.getYearFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(energyNewinfoTotal.getYearFarAssemblyrate());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(energyNewinfoTotal.getYearFarNumber());
        zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(energyNewinfoTotal.getYearFarQzxzzmj());
        zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(energyNewinfoTotal.getYearFarZpszxzzjzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearArea(energyNewinfoTotal.getYearArea());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(energyNewinfoTotal.getYearFarArea1());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(energyNewinfoTotal.getYearFarNumber1());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(energyNewinfoTotal.getYearFarArea2());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(energyNewinfoTotal.getYearFarNumber2());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(energyNewinfoTotal.getYearFarArea3());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(energyNewinfoTotal.getYearFarNumber3());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(energyNewinfoTotal.getYearFarArea4());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(energyNewinfoTotal.getYearFarNumber4());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(energyNewinfoTotal.getYearFarArea5());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(energyNewinfoTotal.getYearFarNumber5());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(energyNewinfoTotal.getYearFarArea6());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(energyNewinfoTotal.getYearFarNumber6());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(energyNewinfoTotal.getYearFarArea7());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(energyNewinfoTotal.getYearFarNumber7());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(energyNewinfoTotal.getYearFarArea8());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(energyNewinfoTotal.getYearFarNumber8());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(energyNewinfoTotal.getYearFarArea9());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(energyNewinfoTotal.getYearFarNumber9());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(energyNewinfoTotal.getYearFarArea10());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(energyNewinfoTotal.getYearFarNumber10());

        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", sysUser.getAreacode());
        queryW.isNull("area_type");
        queryW.eq("projecttype", projecttype);
        ZgsReportMonthfabrAreaCityCompleted energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        }
        //上报完修改上报状态
        QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapper = new QueryWrapper();
        wrapper.eq("filltm", yearMonth);
        wrapper.eq("areacode", sysUser.getAreacode());
        wrapper.eq("projecttype", projecttype);
        ZgsReportMonthfabrAreaCityProject energyNewinfo = new ZgsReportMonthfabrAreaCityProject();
        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        energyNewinfo.setBackreason("");
        energyNewinfo.setApplydate(new Date());
        energyNewinfo.setBuildingArea(null);
        zgsReportMonthfabrAreaCityProjectService.update(energyNewinfo, wrapper);
    }


    /**
     * 区县 一键上报
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrAreaTotalALL(int type, String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        //type=0代表手动上报，type=1代表任务定时上报
        double newConstructionArea_M = 0;
        double completedArea_M = 0;
        double newConstructionArea_Y = 0;
        double completedArea_Y = 0;
        //计算累计值newConstructionArea、completedArea
        ZgsReportMonthfabrAreaCityCompleted cityDataYear = this.baseMapper.totalMonthfabrCityToProDataYear(yearMonth, areacode, year, projecttype);
        ZgsReportMonthfabrAreaCityCompleted cityDataMonth = this.baseMapper.totalMonthfabrCityToProDataMonth(yearMonth, areacode, projecttype);
        newConstructionArea_M = cityDataMonth.getMonthArea().doubleValue();
        completedArea_M = cityDataMonth.getMonthArea().doubleValue();
        newConstructionArea_Y = cityDataYear.getYearArea().doubleValue();
        completedArea_Y = cityDataYear.getYearArea().doubleValue();
        //
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
        if (type == 0) {
            zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
        }
        if (applyState != null) {
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(applyState));
        }
        //新方案：每次上报重新计算年累计值
        ZgsReportMonthfabrAreaCityCompleted energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, areacode, year, projecttype);
        energyNewinfoTotal= energyNewinfoTotal!=null?energyNewinfoTotal:new ZgsReportMonthfabrAreaCityCompleted();
        //2、计算本月所有汇总记录
        ZgsReportMonthfabrAreaCityCompleted reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, areacode, projecttype);
        reportEnergyNewinfoTotal = reportEnergyNewinfoTotal!=null?reportEnergyNewinfoTotal:new ZgsReportMonthfabrAreaCityCompleted();
        //3、本月记录+当年最新一条上报记录
        zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthBaildType("1");
        zgsReportMonthfabrAreaCityCompleted.setMonthStructureType("2");
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(reportEnergyNewinfoTotal.getMonthFarArea());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(reportEnergyNewinfoTotal.getMonthFarNumber());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(reportEnergyNewinfoTotal.getMonthFarQzxzzmj());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(reportEnergyNewinfoTotal.getMonthFarZpszxzzjzmj());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(reportEnergyNewinfoTotal.getMonthFarArea1());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(reportEnergyNewinfoTotal.getMonthFarNumber1());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(reportEnergyNewinfoTotal.getMonthFarArea2());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(reportEnergyNewinfoTotal.getMonthFarNumber2());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(reportEnergyNewinfoTotal.getMonthFarArea3());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(reportEnergyNewinfoTotal.getMonthFarNumber3());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(reportEnergyNewinfoTotal.getMonthFarArea4());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(reportEnergyNewinfoTotal.getMonthFarNumber4());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(reportEnergyNewinfoTotal.getMonthFarArea5());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(reportEnergyNewinfoTotal.getMonthFarNumber5());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(reportEnergyNewinfoTotal.getMonthFarArea6());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(reportEnergyNewinfoTotal.getMonthFarNumber6());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(reportEnergyNewinfoTotal.getMonthFarArea7());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(reportEnergyNewinfoTotal.getMonthFarNumber7());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(reportEnergyNewinfoTotal.getMonthFarArea8());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(reportEnergyNewinfoTotal.getMonthFarNumber8());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(reportEnergyNewinfoTotal.getMonthFarArea9());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(reportEnergyNewinfoTotal.getMonthFarNumber9());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(reportEnergyNewinfoTotal.getMonthFarArea10());
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(reportEnergyNewinfoTotal.getMonthFarNumber10());
        //  年
        zgsReportMonthfabrAreaCityCompleted.setYearBaildType("1");
        zgsReportMonthfabrAreaCityCompleted.setYearStructureType("2");
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea(energyNewinfoTotal.getYearFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(energyNewinfoTotal.getYearFarAssemblyrate());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(energyNewinfoTotal.getYearFarNumber());
        zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(energyNewinfoTotal.getYearFarQzxzzmj());
        zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(energyNewinfoTotal.getYearFarZpszxzzjzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearArea(energyNewinfoTotal.getYearArea());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(energyNewinfoTotal.getYearFarArea1());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(energyNewinfoTotal.getYearFarNumber1());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(energyNewinfoTotal.getYearFarArea2());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(energyNewinfoTotal.getYearFarNumber2());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(energyNewinfoTotal.getYearFarArea3());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(energyNewinfoTotal.getYearFarNumber3());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(energyNewinfoTotal.getYearFarArea4());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(energyNewinfoTotal.getYearFarNumber4());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(energyNewinfoTotal.getYearFarArea5());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(energyNewinfoTotal.getYearFarNumber5());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(energyNewinfoTotal.getYearFarArea6());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(energyNewinfoTotal.getYearFarNumber6());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(energyNewinfoTotal.getYearFarArea7());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(energyNewinfoTotal.getYearFarNumber7());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(energyNewinfoTotal.getYearFarArea8());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(energyNewinfoTotal.getYearFarNumber8());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(energyNewinfoTotal.getYearFarArea9());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(energyNewinfoTotal.getYearFarNumber9());
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(energyNewinfoTotal.getYearFarArea10());
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(energyNewinfoTotal.getYearFarNumber10());
        //newConstructionArea_M、completedArea_M、newConstructionArea_Y、completedArea_Y
        if (projecttype == "1") {
            if (newConstructionArea == 0) {
                if (type == 1) {
                    newConstructionArea = newConstructionArea_M;
                }
            } else {
                if (newConstructionArea_M == 0) {
                    newConstructionArea_Y += newConstructionArea_M;
                }
            }
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && newConstructionArea != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (newConstructionArea) * 100)));
            }
            if (energyNewinfoTotal.getYearFarArea() != null && newConstructionArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(newConstructionArea_Y));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (newConstructionArea_Y) * 100)));
            }
        } else if (projecttype == "2") {
            if (completedArea == 0) {
                if (type == 1) {
                    completedArea = completedArea_M;
                }
            } else {
                if (completedArea_M == 0) {
                    completedArea_Y += completedArea_M;
                }
            }
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && completedArea != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (completedArea) * 100)));
            }
            if (energyNewinfoTotal.getYearFarArea() != null && completedArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(completedArea_Y));  //  手填数据
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (completedArea_Y) * 100)));
            }
        }
        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNull("area_type");
        queryW.eq("projecttype", projecttype);
        ZgsReportMonthfabrAreaCityCompleted energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
                zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            }
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
                }
                zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
                zgsReportMonthfabrAreaCityCompleted.setAreacode(areacode);
                zgsReportMonthfabrAreaCityCompleted.setAreaname(areaname);
                zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
                zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
                zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
                zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        if (type == 0) {
            //上报完修改上报状态
            QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapper = new QueryWrapper();
            wrapper.eq("filltm", yearMonth);
            wrapper.eq("areacode", areacode);
            wrapper.eq("projecttype", projecttype);
            ZgsReportMonthfabrAreaCityProject energyNewinfo = new ZgsReportMonthfabrAreaCityProject();
            energyNewinfo.setApplystate(new BigDecimal(applyState));
            energyNewinfo.setBackreason("");
            energyNewinfo.setApplydate(new Date());
            energyNewinfo.setBuildingArea(null);
            zgsReportMonthfabrAreaCityProjectService.update(energyNewinfo, wrapper);
        }
    }


    /**
     * 区县 一键上报
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
//    @Override
//    public void initMonthfabrAreaTotal(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
//        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
//        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportMonthfabrAreaCityCompleted.setBackreason("");
////        BeanUtils.copyProperties(zgsReportEnergyNewinfo, zgsReportMonthfabrAreaCityCompleted);
//        //计算当月上报合计
//        //1、查汇总当年最新一条上报记录
////    QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper();
////    queryWrapper.ne("isdelete", 1);
////    queryWrapper.eq("year", year);
////    queryWrapper.eq("areacode", sysUser.getAreacode());
////    queryWrapper.orderByDesc("applydate");
////    queryWrapper.last("LIMIT 1");
////        List<ZgsReportMonthfabrAreaCityCompleted> newinfoTotalList = this.baseMapper.selectList(queryWrapper);
//
////        if (newinfoTotalList.size() > 0) {
////            energyNewinfoTotal = newinfoTotalList.get(0);
////        }
////    if (energyNewinfoTotal == null) {
////            energyNewinfoTotal = new ZgsReportMonthfabrAreaCityCompleted();
////    }
//
//        ZgsReportMonthfabrAreaCityCompleted energyNewinfoTotal = null;
//        //新方案：每次上报重新计算年累计值
//        energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, sysUser.getAreacode(), year, projecttype);
//
//        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityCompleted reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
//
//        //3、本月记录+当年最新一条上报记录
//        zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
//        //  月
//        zgsReportMonthfabrAreaCityCompleted.setMonthBaildType("1");
//        zgsReportMonthfabrAreaCityCompleted.setMonthStructureType("2");
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(reportEnergyNewinfoTotal.getMonthFarArea());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(reportEnergyNewinfoTotal.getMonthFarNumber());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(reportEnergyNewinfoTotal.getMonthFarQzxzzmj());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(reportEnergyNewinfoTotal.getMonthFarZpszxzzjzmj());
//
//        if (projecttype == "1") {
//            zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea));  //  手填数据
//            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && newConstructionArea != 0) {
//                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
//                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (newConstructionArea) * 100)));
//            }
//        } else if (projecttype == "2") {
//            zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea));  //  手填数据
//            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && completedArea != 0) {
//                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
//                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (completedArea) * 100)));
//            }
//        }
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(reportEnergyNewinfoTotal.getMonthFarArea1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(reportEnergyNewinfoTotal.getMonthFarNumber1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(reportEnergyNewinfoTotal.getMonthFarArea2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(reportEnergyNewinfoTotal.getMonthFarNumber2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(reportEnergyNewinfoTotal.getMonthFarArea3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(reportEnergyNewinfoTotal.getMonthFarNumber3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(reportEnergyNewinfoTotal.getMonthFarArea4());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(reportEnergyNewinfoTotal.getMonthFarNumber4());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(reportEnergyNewinfoTotal.getMonthFarArea5());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(reportEnergyNewinfoTotal.getMonthFarNumber5());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(reportEnergyNewinfoTotal.getMonthFarArea6());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(reportEnergyNewinfoTotal.getMonthFarNumber6());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(reportEnergyNewinfoTotal.getMonthFarArea7());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(reportEnergyNewinfoTotal.getMonthFarNumber7());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(reportEnergyNewinfoTotal.getMonthFarArea8());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(reportEnergyNewinfoTotal.getMonthFarNumber8());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(reportEnergyNewinfoTotal.getMonthFarArea9());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(reportEnergyNewinfoTotal.getMonthFarNumber9());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(reportEnergyNewinfoTotal.getMonthFarArea10());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(reportEnergyNewinfoTotal.getMonthFarNumber10());
//
//        //  年
//        zgsReportMonthfabrAreaCityCompleted.setYearBaildType("1");
//        zgsReportMonthfabrAreaCityCompleted.setYearStructureType("2");
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea(energyNewinfoTotal.getYearFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(energyNewinfoTotal.getYearFarAssemblyrate());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(energyNewinfoTotal.getYearFarNumber());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(energyNewinfoTotal.getYearFarQzxzzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(energyNewinfoTotal.getYearFarZpszxzzjzmj());
//
//        zgsReportMonthfabrAreaCityCompleted.setYearArea(zgsReportMonthfabrAreaCityCompleted.getMonthArea());  //  手填数据
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(energyNewinfoTotal.getYearFarArea1());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(energyNewinfoTotal.getYearFarNumber1());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(energyNewinfoTotal.getYearFarArea2());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(energyNewinfoTotal.getYearFarNumber2());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(energyNewinfoTotal.getYearFarArea3());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(energyNewinfoTotal.getYearFarNumber3());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(energyNewinfoTotal.getYearFarArea4());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(energyNewinfoTotal.getYearFarNumber4());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(energyNewinfoTotal.getYearFarArea5());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(energyNewinfoTotal.getYearFarNumber5());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(energyNewinfoTotal.getYearFarArea6());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(energyNewinfoTotal.getYearFarNumber6());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(energyNewinfoTotal.getYearFarArea7());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(energyNewinfoTotal.getYearFarNumber7());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(energyNewinfoTotal.getYearFarArea8());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(energyNewinfoTotal.getYearFarNumber8());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(energyNewinfoTotal.getYearFarArea9());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(energyNewinfoTotal.getYearFarNumber9());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(energyNewinfoTotal.getYearFarArea10());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(energyNewinfoTotal.getYearFarNumber10());
//
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNull("area_type");
//        queryW.eq("projecttype", projecttype);
//        ZgsReportMonthfabrAreaCityCompleted energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
//            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
//            this.baseMapper.updateById(zgsReportMonthfabrAreaCityCompleted);
//        } else {
//            //新增
//            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
//            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
//            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
//            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
//            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
//            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
//            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
//            this.baseMapper.insert(zgsReportMonthfabrAreaCityCompleted);
//        }
//        //上报完修改上报状态
//        QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", yearMonth);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        wrapper.eq("projecttype", projecttype);
//        ZgsReportMonthfabrAreaCityProject energyNewinfo = new ZgsReportMonthfabrAreaCityProject();
//        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        energyNewinfo.setBackreason("");
//        energyNewinfo.setApplydate(new Date());
//        energyNewinfo.setBuildingArea(null);
//        zgsReportMonthfabrAreaCityProjectService.update(energyNewinfo, wrapper);
//    }


    /**
     * 市州 一键上报 到省厅(先注释-已重复)
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
//    @Override
//    public void initMonthfabrCityTotal(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
//        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
//        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportMonthfabrAreaCityCompleted.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportMonthfabrAreaCityCompleted energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, sysUser.getAreacode(), year, projecttype);
//        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityCompleted reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
//        //3、本月记录+当年最新一条上报记录
//
//        //  月
//        zgsReportMonthfabrAreaCityCompleted.setMonthBaildType("1");
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(reportEnergyNewinfoTotal.getMonthFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(reportEnergyNewinfoTotal.getMonthFarAssemblyrate());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(reportEnergyNewinfoTotal.getMonthFarNumber());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(reportEnergyNewinfoTotal.getMonthFarQzxzzmj());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(reportEnergyNewinfoTotal.getMonthFarZpszxzzjzmj());
//        zgsReportMonthfabrAreaCityCompleted.setMonthStructureType("2");
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthArea(reportEnergyNewinfoTotal.getMonthArea());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(reportEnergyNewinfoTotal.getMonthFarArea1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(reportEnergyNewinfoTotal.getMonthFarNumber1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(reportEnergyNewinfoTotal.getMonthFarArea2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(reportEnergyNewinfoTotal.getMonthFarNumber2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(reportEnergyNewinfoTotal.getMonthFarArea3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(reportEnergyNewinfoTotal.getMonthFarNumber3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(reportEnergyNewinfoTotal.getMonthFarArea4());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(reportEnergyNewinfoTotal.getMonthFarNumber4());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(reportEnergyNewinfoTotal.getMonthFarArea5());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(reportEnergyNewinfoTotal.getMonthFarNumber5());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(reportEnergyNewinfoTotal.getMonthFarArea6());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(reportEnergyNewinfoTotal.getMonthFarNumber6());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(reportEnergyNewinfoTotal.getMonthFarArea7());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(reportEnergyNewinfoTotal.getMonthFarNumber7());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(reportEnergyNewinfoTotal.getMonthFarArea8());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(reportEnergyNewinfoTotal.getMonthFarNumber8());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(reportEnergyNewinfoTotal.getMonthFarArea9());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(reportEnergyNewinfoTotal.getMonthFarNumber9());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(reportEnergyNewinfoTotal.getMonthFarArea10());
//        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(reportEnergyNewinfoTotal.getMonthFarNumber10());
//
//        //  年
//        zgsReportMonthfabrAreaCityCompleted.setYearBaildType("1");
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea(energyNewinfoTotal.getYearFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(energyNewinfoTotal.getYearFarAssemblyrate());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(energyNewinfoTotal.getYearFarNumber());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(energyNewinfoTotal.getYearFarQzxzzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(energyNewinfoTotal.getYearFarZpszxzzjzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearStructureType("2");
//
//        zgsReportMonthfabrAreaCityCompleted.setYearArea(energyNewinfoTotal.getYearArea());  //  手填数据
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(energyNewinfoTotal.getYearFarArea1());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(energyNewinfoTotal.getYearFarNumber1());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(energyNewinfoTotal.getYearFarArea2());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(energyNewinfoTotal.getYearFarNumber2());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(energyNewinfoTotal.getYearFarArea3());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(energyNewinfoTotal.getYearFarNumber3());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(energyNewinfoTotal.getYearFarArea4());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(energyNewinfoTotal.getYearFarNumber4());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(energyNewinfoTotal.getYearFarArea5());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(energyNewinfoTotal.getYearFarNumber5());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(energyNewinfoTotal.getYearFarArea6());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(energyNewinfoTotal.getYearFarNumber6());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(energyNewinfoTotal.getYearFarArea7());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(energyNewinfoTotal.getYearFarNumber7());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(energyNewinfoTotal.getYearFarArea8());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(energyNewinfoTotal.getYearFarNumber8());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(energyNewinfoTotal.getYearFarArea9());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(energyNewinfoTotal.getYearFarNumber9());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(energyNewinfoTotal.getYearFarArea10());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(energyNewinfoTotal.getYearFarNumber10());
//
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.eq("projecttype", projecttype);
//        queryW.isNotNull("area_type");
//        ZgsReportMonthfabrAreaCityCompleted energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
//            this.baseMapper.updateById(zgsReportMonthfabrAreaCityCompleted);
//        } else {
//            //新增
//            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
//            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
//            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
//            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setFillpersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setFillpersontel(sysUser.getPhone());
//            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
//            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
//            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
//            zgsReportMonthfabrAreaCityCompleted.setAreaType("1");
//            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
//            this.baseMapper.insert(zgsReportMonthfabrAreaCityCompleted);
//        }
//    }

    /**
     * 市州 一键上报 到省厅(定时任务执行)
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrCityTotalAll(int type, Integer applyState, String year, int quarter, String yearMonth, String projecttype, String areacode, String areaname, LoginUser sysUser) {
        double newConstructionArea_M = 0;
        double completedArea_M = 0;
        double newConstructionArea_Y = 0;
        double completedArea_Y = 0;
        //计算累计值newConstructionArea、completedArea
        ZgsReportMonthfabrAreaCityCompleted cityDataYear = this.baseMapper.totalMonthfabrCityDataYear(yearMonth, areacode, year, projecttype);
        ZgsReportMonthfabrAreaCityCompleted cityDataMonth = this.baseMapper.totalMonthfabrCityDataMonth(yearMonth, areacode, projecttype);
        newConstructionArea_M = cityDataMonth.getMonthArea().doubleValue();
        completedArea_M = cityDataMonth.getMonthArea().doubleValue();
        newConstructionArea_Y = cityDataYear.getYearArea().doubleValue();
        completedArea_Y = cityDataYear.getYearArea().doubleValue();
        //
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
        if (type == 0) {
            zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
        }
        if (applyState != null) {
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(applyState));
        }
        //新方案：每次上报重新计算年累计值
        ZgsReportMonthfabrAreaCityCompleted energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYearTask(yearMonth, areacode, year, projecttype);
        if (energyNewinfoTotal != null) {
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea(energyNewinfoTotal.getYearFarArea());
//        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(energyNewinfoTotal.getYearFarAssemblyrate());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(energyNewinfoTotal.getYearFarNumber());
            zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(energyNewinfoTotal.getYearFarQzxzzmj());
            zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(energyNewinfoTotal.getYearFarZpszxzzjzmj());
//        zgsReportMonthfabrAreaCityCompleted.setYearArea(zgsReportMonthfabrAreaCityCompleted.getMonthArea());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(energyNewinfoTotal.getYearFarArea1());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(energyNewinfoTotal.getYearFarNumber1());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(energyNewinfoTotal.getYearFarArea2());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(energyNewinfoTotal.getYearFarNumber2());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(energyNewinfoTotal.getYearFarArea3());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(energyNewinfoTotal.getYearFarNumber3());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(energyNewinfoTotal.getYearFarArea4());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(energyNewinfoTotal.getYearFarNumber4());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(energyNewinfoTotal.getYearFarArea5());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(energyNewinfoTotal.getYearFarNumber5());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(energyNewinfoTotal.getYearFarArea6());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(energyNewinfoTotal.getYearFarNumber6());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(energyNewinfoTotal.getYearFarArea7());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(energyNewinfoTotal.getYearFarNumber7());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(energyNewinfoTotal.getYearFarArea8());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(energyNewinfoTotal.getYearFarNumber8());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(energyNewinfoTotal.getYearFarArea9());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(energyNewinfoTotal.getYearFarNumber9());
            zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(energyNewinfoTotal.getYearFarArea10());
            zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(energyNewinfoTotal.getYearFarNumber10());

        }

        //2、计算本月所有汇总记录
        ZgsReportMonthfabrAreaCityCompleted reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonthTask(yearMonth, areacode, projecttype);
        if (reportEnergyNewinfoTotal != null) {

            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(reportEnergyNewinfoTotal.getMonthFarArea());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(reportEnergyNewinfoTotal.getMonthFarNumber());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(reportEnergyNewinfoTotal.getMonthFarQzxzzmj());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(reportEnergyNewinfoTotal.getMonthFarZpszxzzjzmj());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(reportEnergyNewinfoTotal.getMonthFarArea1());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(reportEnergyNewinfoTotal.getMonthFarNumber1());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(reportEnergyNewinfoTotal.getMonthFarArea2());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(reportEnergyNewinfoTotal.getMonthFarNumber2());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(reportEnergyNewinfoTotal.getMonthFarArea3());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(reportEnergyNewinfoTotal.getMonthFarNumber3());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(reportEnergyNewinfoTotal.getMonthFarArea4());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(reportEnergyNewinfoTotal.getMonthFarNumber4());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(reportEnergyNewinfoTotal.getMonthFarArea5());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(reportEnergyNewinfoTotal.getMonthFarNumber5());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(reportEnergyNewinfoTotal.getMonthFarArea6());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(reportEnergyNewinfoTotal.getMonthFarNumber6());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(reportEnergyNewinfoTotal.getMonthFarArea7());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(reportEnergyNewinfoTotal.getMonthFarNumber7());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(reportEnergyNewinfoTotal.getMonthFarArea8());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(reportEnergyNewinfoTotal.getMonthFarNumber8());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(reportEnergyNewinfoTotal.getMonthFarArea9());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(reportEnergyNewinfoTotal.getMonthFarNumber9());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(reportEnergyNewinfoTotal.getMonthFarArea10());
            zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(reportEnergyNewinfoTotal.getMonthFarNumber10());


        }
        if (projecttype == "1") {
            // reportEnergyNewinfoTotal.getMonthFarArea() != null &&
            if (newConstructionArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea_M));
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        // reportEnergyNewinfoTotal.getMonthFarArea().toString()
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal != null ? reportEnergyNewinfoTotal.getMonthFarArea().toString() : String.valueOf(0) )) / (newConstructionArea_M) * 100)));
            }
            // energyNewinfoTotal.getYearFarArea() != null &&
            if (newConstructionArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(newConstructionArea_Y));
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        //energyNewinfoTotal.getYearFarArea().toString()
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal != null ? energyNewinfoTotal.getYearFarArea().toString() : String.valueOf(0))) / (newConstructionArea_Y) * 100)));
            }
        } else if (projecttype == "2") {
            // reportEnergyNewinfoTotal.getMonthFarArea() != null &&
            if (completedArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea_M));
                // reportEnergyNewinfoTotal.getMonthFarArea().toString()

                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal != null ? reportEnergyNewinfoTotal.getMonthFarArea().toString() : String.valueOf(0))) / (completedArea_M) * 100)));
            }
            // reportEnergyNewinfoTotal.getYearFarArea() != null &&
            if (completedArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(completedArea_Y));
            }
        }


        /*if (projecttype == "1") {
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && newConstructionArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea_M));
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (newConstructionArea_M) * 100)));
            }
            if (energyNewinfoTotal.getYearFarArea() != null && newConstructionArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(newConstructionArea_Y));
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (newConstructionArea_Y) * 100)));
            }
        } else if (projecttype == "2") {
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && completedArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea_M));
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (completedArea_M) * 100)));
            }
            if (reportEnergyNewinfoTotal.getYearFarArea() != null && completedArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(completedArea_Y));
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (completedArea_Y) * 100)));
            }
        }*/

        //3、本月记录+当年最新一条上报记录
        zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthBaildType("1");
        zgsReportMonthfabrAreaCityCompleted.setMonthStructureType("2");

        //  年
        zgsReportMonthfabrAreaCityCompleted.setYearBaildType("1");
        zgsReportMonthfabrAreaCityCompleted.setYearStructureType("2");

        /*if (projecttype == "1") {
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && newConstructionArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(newConstructionArea_M));
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (newConstructionArea_M) * 100)));
            }
            if (energyNewinfoTotal.getYearFarArea() != null && newConstructionArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(newConstructionArea_Y));
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (newConstructionArea_Y) * 100)));
            }
        } else if (projecttype == "2") {
            if (reportEnergyNewinfoTotal.getMonthFarArea() != null && completedArea_M != 0) {
                zgsReportMonthfabrAreaCityCompleted.setMonthArea(new BigDecimal(completedArea_M));
                zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(reportEnergyNewinfoTotal.getMonthFarArea().toString())) / (completedArea_M) * 100)));
            }
            if (reportEnergyNewinfoTotal.getYearFarArea() != null && completedArea_Y != 0) {
                zgsReportMonthfabrAreaCityCompleted.setYearArea(new BigDecimal(completedArea_Y));
                zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(
                        new BigDecimal(((Double.parseDouble(energyNewinfoTotal.getYearFarArea().toString())) / (completedArea_Y) * 100)));
            }
        }*/
        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNotNull("area_type");
        queryW.eq("projecttype", projecttype);
        ZgsReportMonthfabrAreaCityCompleted energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
                zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            }
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
                }
                zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
                zgsReportMonthfabrAreaCityCompleted.setAreacode(areacode);
                zgsReportMonthfabrAreaCityCompleted.setAreaname(areaname);
                zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
                zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
                zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
                zgsReportMonthfabrAreaCityCompleted.setProjecttype(projecttype);
                zgsReportMonthfabrAreaCityCompleted.setAreaType("1");
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }

    @Override
    public void spProject(String id, Integer spStatus, String backreason) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
        zgsReportMonthfabrAreaCityCompleted.setId(id);
        zgsReportMonthfabrAreaCityCompleted.setMonthArea(null);
        zgsReportMonthfabrAreaCityCompleted.setYearArea(null);
        zgsReportMonthfabrAreaCityCompleted.setIsdelete(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarZpszxzzjzmj(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarQzxzzmj(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarAssemblyrate(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarZpszxzzjzmj(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarQzxzzmj(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarAssemblyrate(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber3(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea4(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber4(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea5(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber5(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea6(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber6(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea7(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber7(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea8(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber8(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea9(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber9(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarArea10(null);
        zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber10(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea1(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber1(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea2(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber2(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea3(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber3(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea4(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber4(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea5(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber5(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea6(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber6(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea7(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber7(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea8(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber8(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea9(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber9(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarArea10(null);
        zgsReportMonthfabrAreaCityCompleted.setYearFarNumber10(null);
        //
        //修改项目清单的状态
        ZgsReportMonthfabrAreaCityCompleted newinfoTotal = this.baseMapper.selectById(id);
        ZgsReportMonthfabrAreaCityProject energyNewinfo = new ZgsReportMonthfabrAreaCityProject();
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity = new ZgsReportMonthfabrAreaCityProductioncapacity();
        ZgsReportMonthfabrAreaCityCompany zgsReportMonthfabrAreaCityCompany = new ZgsReportMonthfabrAreaCityCompany();
        if (spStatus == 0) {
            //驳回
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            energyNewinfo.setBackreason(backreason);
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityCompleted.setBackreason(backreason);
            zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityProductioncapacity.setBackreason(backreason);
            zgsReportMonthfabrAreaCityCompany.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityCompany.setBackreason(backreason);
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                //  省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin --
                //  省厅账号退回后，市州汇总表子项状态改为待审批
                //  省厅驳回区县：省厅只修改 汇总表中 合计的那一条，清单不修改
                //  省厅驳回市州：修改清单，汇总表都需要修改
                // ====--====--====--====--====-- 新开工、已竣工 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                ZgsReportMonthfabrAreaCityCompleted info = new ZgsReportMonthfabrAreaCityCompleted();
                BeanUtils.copyProperties(zgsReportMonthfabrAreaCityCompleted, info);
                info.setId(null);
                info.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                info.setBackreason("");
                QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.likeRight("areacode", newinfoTotal.getAreacode());
                wrapper.isNull("area_type");
                this.baseMapper.update(info, wrapper);
                //
                info.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapper1 = new QueryWrapper();
                wrapper1.eq("filltm", newinfoTotal.getFilltm());
                wrapper1.eq("areacode", newinfoTotal.getAreacode());
                wrapper1.isNotNull("area_type");
                this.baseMapper.update(info, wrapper1);
                //省退市，删掉该条汇总数据-不留痕
//                this.baseMapper.delete(wrapper1);
                // ====--====--====--====--====-- 新开工、已竣工 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                // ====--====--====--====--====-- 生产产能 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                ZgsReportMonthfabrAreaCityProductioncapacity infoSccn = new ZgsReportMonthfabrAreaCityProductioncapacity();
                infoSccn.setId(null);
                infoSccn.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                infoSccn.setBackreason("");
                QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperSccn = new QueryWrapper();
                wrapperSccn.eq("filltm", newinfoTotal.getFilltm());
                wrapperSccn.likeRight("areacode", newinfoTotal.getAreacode());
                wrapperSccn.isNull("area_type");
                zgsReportMonthfabrAreaCityProductioncapacityMapper.update(infoSccn, wrapperSccn);
                infoSccn.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperSccn1 = new QueryWrapper();
                wrapperSccn1.eq("filltm", newinfoTotal.getFilltm());
                wrapperSccn1.eq("areacode", newinfoTotal.getAreacode());
                wrapperSccn1.isNotNull("area_type");
                zgsReportMonthfabrAreaCityProductioncapacityMapper.update(infoSccn, wrapperSccn1);
                //省退市，删掉该条汇总数据-不留痕
//                zgsReportMonthfabrAreaCityProductioncapacityMapper.delete(wrapperSccn1);
                // ====--====--====--====--====-- 生产产能 退回    End ====--====--====--====--====--====--====--====--====--====--
                //  省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- End -- End -- End -- End -- End -- End --
            }
        } else {
            //通过
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            energyNewinfo.setBackreason("");
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
            zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityProductioncapacity.setBackreason("");
            zgsReportMonthfabrAreaCityCompany.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityCompany.setBackreason("");
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市州账号审批
            //  市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin --
            //  省厅账号退回后，市州汇总表子项状态改为待审批
            //  省厅驳回区县：省厅只修改 汇总表中 合计的那一条，清单不修改
            //  省厅驳回市州：修改清单，汇总表都需要修改
            if (spStatus == 0) {
                QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.eq("areacode", newinfoTotal.getAreacode());
                zgsReportMonthfabrAreaCityProjectService.update(energyNewinfo, wrapper);
                // ====--====--====--====--====-- 生产产能 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                QueryWrapper<ZgsReportMonthfabrAreaCityCompany> queryWrapperCompany = new QueryWrapper();
                queryWrapperCompany.eq("reporttm", newinfoTotal.getFilltm());
                queryWrapperCompany.eq("areacode", newinfoTotal.getAreacode());
                queryWrapperCompany.ne("isdelete", 1);
//                zgsReportMonthfabrAreaCityCompany.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                zgsReportMonthfabrAreaCityCompanyService.update(zgsReportMonthfabrAreaCityCompany, queryWrapperCompany);
                QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperProductioncapacity = new QueryWrapper();
                wrapperProductioncapacity.eq("reporttm", newinfoTotal.getFilltm());
                wrapperProductioncapacity.eq("areacode", newinfoTotal.getAreacode());
                wrapperProductioncapacity.ne("isdelete", 1);
//                zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                zgsReportMonthfabrAreaCityProductioncapacityService.update(zgsReportMonthfabrAreaCityProductioncapacity, wrapperProductioncapacity);
                // ====--====--====--====--====-- 生产产能 退回    End ====--====--====--====--====--====--====--====--====--====--
                //  市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- End -- End -- End -- End -- End -- End --
            } else {
                QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.eq("areacode", newinfoTotal.getAreacode());
                wrapper.eq("projecttype", newinfoTotal.getProjecttype());
                zgsReportMonthfabrAreaCityProjectService.update(energyNewinfo, wrapper);
            }
        }
        if (spStatus == 0) {
            QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapper2 = new QueryWrapper();
            wrapper2.eq("filltm", newinfoTotal.getFilltm());
            wrapper2.eq("areacode", newinfoTotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                wrapper2.isNotNull("area_type");
            }
            this.baseMapper.update(zgsReportMonthfabrAreaCityCompleted, wrapper2);
        } else {
            this.baseMapper.updateById(zgsReportMonthfabrAreaCityCompleted);
        }
    }

    @Override
    public ZgsReportMonthfabrAreaCityCompleted lastTotalDataProvince(String filltm, String projecttype, Integer applystate) {
        return this.baseMapper.lastTotalDataProvince(filltm, projecttype, applystate);
    }

    @Override
    public ZgsReportMonthfabrAreaCityCompleted lastTotalDataCity(String filltm, String areacode, String projecttype, Integer applystate) {
        return this.baseMapper.lastTotalDataCity(filltm, areacode, projecttype, applystate);
    }
}
