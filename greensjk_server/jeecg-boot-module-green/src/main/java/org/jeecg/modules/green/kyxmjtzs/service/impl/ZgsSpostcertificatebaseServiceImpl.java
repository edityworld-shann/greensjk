package org.jeecg.modules.green.kyxmjtzs.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.injector.methods.SelectByMap;
import com.baomidou.mybatisplus.core.injector.methods.SelectList;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.mapper.ZgsSpostcertificatebaseMapper;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 科研项目结题证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSpostcertificatebaseServiceImpl extends ServiceImpl<ZgsSpostcertificatebaseMapper, ZgsSpostcertificatebase> implements IZgsSpostcertificatebaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;


    @Override
    public int spProject(ZgsSpostcertificatebase zgsSpostcertificatebase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSpostcertificatebase != null && StringUtils.isNotEmpty(zgsSpostcertificatebase.getId())) {
            ZgsSpostcertificatebase spostcertificatebase = new ZgsSpostcertificatebase();
            spostcertificatebase.setId(zgsSpostcertificatebase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsSpostcertificatebase.getId());
            zgsReturnrecord.setReturnreason(zgsSpostcertificatebase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE15);
            ZgsSpostcertificatebase baseInfo = getById(zgsSpostcertificatebase.getId());
            spostcertificatebase.setApplydate(baseInfo.getApplydate());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsSpostcertificatebase.getSpStatus() == 1) {
                //通过记录状态==0
                spostcertificatebase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSpostcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsSpostcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsSpostcertificatebase.getStatus())) {
                    spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    spostcertificatebase.setFirstdate(new Date());
                    spostcertificatebase.setFirstperson(sysUser.getRealname());
                    spostcertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    spostcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS2);
                    spostcertificatebase.setFinishperson(sysUser.getRealname());
                    spostcertificatebase.setFinishdate(new Date());
                    spostcertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                spostcertificatebase.setAuditopinion(zgsSpostcertificatebase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSpostcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsSpostcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsSpostcertificatebase.getStatus())) {
                    if (zgsSpostcertificatebase.getSpStatus() == 2) {
                        spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    spostcertificatebase.setFirstdate(new Date());
                    spostcertificatebase.setFirstperson(sysUser.getRealname());
                    spostcertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsSpostcertificatebase.getSpStatus() == 2) {
                        spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS5);
                    } else {
                        spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS15);
                    }
                    spostcertificatebase.setFinishperson(sysUser.getRealname());
                    spostcertificatebase.setFinishdate(new Date());
                    spostcertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsSpostcertificatebase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        spostcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getProjectlibraryguid(), baseInfo.getEnterpriseguid(), zgsSpostcertificatebase.getAuditopinion());
                    }
                }
                if (zgsSpostcertificatebase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsSpostcertificatebase.getAuditopinion())) {
                        zgsSpostcertificatebase.setAuditopinion(" ");
                    }
                    spostcertificatebase.setAuditopinion(zgsSpostcertificatebase.getAuditopinion());
                    spostcertificatebase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsSpostcertificatebase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    spostcertificatebase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_53));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(spostcertificatebase, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }


    /**
     * 顺序编号（科研项目结题证书）
     * 实例：甘建科结[2022] 1 号
     *
     * @return
     */
    @Override
    public String number() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        String qw = "甘建科结[" + currentYear + "]";
        QueryWrapper<ZgsSpostcertificatebase> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("projectnumber", qw);
        List<ZgsSpostcertificatebase> zgsSpostcertificatebases = this.baseMapper.selectList(queryWrapper);
        return qw + " " + (zgsSpostcertificatebases.size() + 1) + " 号";
    }

    @Override
    public List<ZgsSpostcertificatebase> listWarnByFundType(Wrapper<ZgsSpostcertificatebase> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.listWarnByFundType(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }

}
