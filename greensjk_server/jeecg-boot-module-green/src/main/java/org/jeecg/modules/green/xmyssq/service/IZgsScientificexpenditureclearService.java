package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificexpenditureclear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研项目申报验收经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificexpenditureclearService extends IService<ZgsScientificexpenditureclear> {

}
