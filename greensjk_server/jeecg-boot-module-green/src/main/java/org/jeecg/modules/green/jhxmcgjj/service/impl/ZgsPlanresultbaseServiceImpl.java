package org.jeecg.modules.green.jhxmcgjj.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.mapper.ZgsPlanresultbaseMapper;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 项目成果简介
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsPlanresultbaseServiceImpl extends ServiceImpl<ZgsPlanresultbaseMapper, ZgsPlanresultbase> implements IZgsPlanresultbaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;

    @Override
    public int spProject(ZgsPlanresultbase zgsPlanresultbase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPlanresultbase != null && StringUtils.isNotEmpty(zgsPlanresultbase.getId())) {
            ZgsPlanresultbase planresultbase = new ZgsPlanresultbase();
            planresultbase.setId(zgsPlanresultbase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsPlanresultbase.getId());
            zgsReturnrecord.setReturnreason(zgsPlanresultbase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE18);
            ZgsPlanresultbase baseInfo = getById(zgsPlanresultbase.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getResultname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(zgsProjectunitmember.getEnterprisename());
                }
            }
            if (zgsPlanresultbase.getSpStatus() == 1) {
                //通过记录状态==0
                planresultbase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPlanresultbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPlanresultbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPlanresultbase.getStatus())) {
                    planresultbase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    planresultbase.setFirstdate(new Date());
                    planresultbase.setFirstperson(sysUser.getRealname());
                    planresultbase.setFirstdepartment(sysUser.getEnterprisename());
                    planresultbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    planresultbase.setStatus(GlobalConstants.SHENHE_STATUS8);
                    planresultbase.setFinishperson(sysUser.getRealname());
                    planresultbase.setFinishdate(new Date());
                    planresultbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                planresultbase.setAuditopinion(zgsPlanresultbase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPlanresultbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPlanresultbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPlanresultbase.getStatus())) {
                    if (zgsPlanresultbase.getSpStatus() == 2) {
                        planresultbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        planresultbase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    planresultbase.setFirstdate(new Date());
                    planresultbase.setFirstperson(sysUser.getRealname());
                    planresultbase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsPlanresultbase.getSpStatus() == 2) {
                        planresultbase.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        planresultbase.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    planresultbase.setFinishperson(sysUser.getRealname());
                    planresultbase.setFinishdate(new Date());
                    planresultbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsPlanresultbase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        planresultbase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getProjectlibraryguid(), baseInfo.getEnterpriseguid(), zgsPlanresultbase.getAuditopinion());

                    }
                }
                if (zgsPlanresultbase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsPlanresultbase.getAuditopinion())) {
                        zgsPlanresultbase.setAuditopinion(" ");
                    }
                    planresultbase.setAuditopinion(zgsPlanresultbase.getAuditopinion());
                    planresultbase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsPlanresultbase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    planresultbase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
            zgsReturnrecordService.save(zgsReturnrecord);
            return this.baseMapper.updateById(planresultbase);
        }
        return 0;
    }

    @Override
    public Page<ZgsPlanresultbase> listZgsPlanResultBaseListInfo(Wrapper<ZgsPlanresultbase> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsPlanresultbase> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsPlanresultbase> listInfoPage = this.baseMapper.listZgsPlanResultBaseListInfo(pageMode, queryWrapper);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }

    @Override
    public Page<ZgsPlanresultbase> getResultInfo(Wrapper<ZgsPlanresultbase> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsPlanresultbase> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsPlanresultbase> listInfoPage = this.baseMapper.getResultInfo(pageMode, queryWrapper);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }


}
