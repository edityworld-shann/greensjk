package org.jeecg.modules.green.jhxmbgsq.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 建设科技计划项目变更内容记录表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "建设科技计划项目变更内容记录表")
@RestController
@RequestMapping("/jhxmbgsq/zgsPlannedprojectchangedetail")
@Slf4j
public class ZgsPlannedprojectchangedetailController extends JeecgController<ZgsPlannedprojectchangedetail, IZgsPlannedprojectchangedetailService> {
    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;

    /**
     * 分页列表查询
     *
     * @param zgsPlannedprojectchangedetail
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-分页列表查询")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-分页列表查询", notes = "建设科技计划项目变更内容记录表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsPlannedprojectchangedetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsPlannedprojectchangedetail, req.getParameterMap());
        Page<ZgsPlannedprojectchangedetail> page = new Page<ZgsPlannedprojectchangedetail>(pageNo, pageSize);
        IPage<ZgsPlannedprojectchangedetail> pageList = zgsPlannedprojectchangedetailService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    @AutoLog(value = "变更日志记录")
    @ApiOperation(value = "变更日志记录", notes = "变更日志记录")
    @GetMapping(value = "/recordList")
    public Result<?> recordList(@RequestParam(name = "bid") String bid) {
        List<ZgsPlannedprojectchangedetail> pageList = new ArrayList<>();
        if (StringUtils.isEmpty(bid)) {
            return Result.error("无数据");
        }
        QueryWrapper<ZgsPlannedprojectchange> wrapper = new QueryWrapper();
        wrapper.eq("projectlibraryguid", bid);
        wrapper.ne("isdelete", 1);
        wrapper.orderByDesc("createdate");
        List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(wrapper);
        if (zgsPlannedprojectchangeList != null && zgsPlannedprojectchangeList.size() > 0) {
            for (ZgsPlannedprojectchange plannedprojectchange : zgsPlannedprojectchangeList) {
                QueryWrapper<ZgsPlannedprojectchangedetail> queryWrapper = new QueryWrapper();
                queryWrapper.eq("baseguid", plannedprojectchange.getId());
                queryWrapper.ne("isdelete", 1);
                queryWrapper.orderByDesc("createdate");
                pageList.addAll(zgsPlannedprojectchangedetailService.list(queryWrapper));
            }
        }
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptList(pageList, ValidateEncryptEntityUtil.isDecrypt));
    }

    /**
     * 添加
     *
     * @param zgsPlannedprojectchangedetail
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-添加")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-添加", notes = "建设科技计划项目变更内容记录表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail) {
        zgsPlannedprojectchangedetailService.save(zgsPlannedprojectchangedetail);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsPlannedprojectchangedetail
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-编辑")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-编辑", notes = "建设科技计划项目变更内容记录表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail) {
        zgsPlannedprojectchangedetailService.updateById(zgsPlannedprojectchangedetail);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-通过id删除")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-通过id删除", notes = "建设科技计划项目变更内容记录表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsPlannedprojectchangedetailService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-批量删除")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-批量删除", notes = "建设科技计划项目变更内容记录表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsPlannedprojectchangedetailService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更内容记录表-通过id查询")
    @ApiOperation(value = "建设科技计划项目变更内容记录表-通过id查询", notes = "建设科技计划项目变更内容记录表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail = zgsPlannedprojectchangedetailService.getById(id);
        if (zgsPlannedprojectchangedetail == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsPlannedprojectchangedetail);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsPlannedprojectchangedetail
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail) {
        return super.exportXls(request, zgsPlannedprojectchangedetail, ZgsPlannedprojectchangedetail.class, "建设科技计划项目变更内容记录表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsPlannedprojectchangedetail.class);
    }

}
