package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertaward;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库专家获奖情况
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertawardService extends IService<ZgsExpertaward> {

}
