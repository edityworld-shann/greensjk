package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 新建建筑节能情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface ZgsReportEnergyNewinfoTotalMapper extends BaseMapper<ZgsReportEnergyNewinfoTotal> {
    ZgsReportEnergyNewinfoTotal totalMonthNewinfoData(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyNewinfoTotal totalMonthNewinfoDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    ZgsReportEnergyNewinfoTotal totalMonthNewinfoDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyNewinfoTotal totalMonthNewinfoDataYearCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    //省厅账号计算当前月份、当前行政区划合计值
    ZgsReportEnergyNewinfoTotal lastTotalDataProvince(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    //市州账号计算当前月份、当前行政区划合计值
    ZgsReportEnergyNewinfoTotal lastTotalDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    ZgsReportEnergyNewinfoTotal getYearNewBuildArea(@Param("filltm") String filltm, @Param("areacode") String areacode);


    // 只计算当前所查询的城市
    ZgsReportEnergyNewinfoTotal getAreadByCityCode(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    ZgsReportEnergyNewinfoTotal lastTotalDataProvinceByCityCode(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);
}
