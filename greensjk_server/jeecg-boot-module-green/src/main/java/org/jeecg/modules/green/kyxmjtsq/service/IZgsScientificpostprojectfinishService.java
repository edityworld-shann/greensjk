package org.jeecg.modules.green.kyxmjtsq.service;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostprojectfinish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificpostprojectfinishService extends IService<ZgsScientificpostprojectfinish> {

}
