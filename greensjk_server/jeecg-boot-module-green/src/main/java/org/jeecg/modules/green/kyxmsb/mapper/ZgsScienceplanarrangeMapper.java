package org.jeecg.modules.green.kyxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface ZgsScienceplanarrangeMapper extends BaseMapper<ZgsScienceplanarrange> {

}
