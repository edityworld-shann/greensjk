package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.mapper.ZgsDemoprojectacceptanceMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 建设科技示范项目验收申请业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsDemoprojectacceptanceServiceImpl extends ServiceImpl<ZgsDemoprojectacceptanceMapper, ZgsDemoprojectacceptance> implements IZgsDemoprojectacceptanceService {

}
