package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 绿色建筑和建筑节能示范项目库
 * @Author: jeecg-boot
 * @Date: 2022-08-02
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_project")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_project对象", description = "绿色建筑和建筑节能示范项目库")
public class ZgsReportProject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 示范类型
     */
    @Excel(name = "示范类型", width = 15)
    @ApiModelProperty(value = "示范类型")
    private java.lang.String demonstratetype;
    /**
     * 示范类型小类
     */
    @Excel(name = "示范类型小类", width = 15)
    @ApiModelProperty(value = "示范类型小类")
    private java.lang.String demonstratetypenum;
    /**
     * 示范项目名称
     */
    @Excel(name = "示范项目名称", width = 15)
    @ApiModelProperty(value = "示范项目名称")
    private java.lang.String projectname;
    /**
     * 项目地址
     */
    @Excel(name = "项目地址", width = 15)
    @ApiModelProperty(value = "项目地址")
    private java.lang.String projectaddress;
    /**
     * 建筑类型
     */
    @Excel(name = "建筑类型", width = 15)
    @ApiModelProperty(value = "建筑类型")
    private java.lang.String buildtype;
    /**
     * 项目立项时间
     */
    @Excel(name = "项目立项时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目立项时间")
    private java.util.Date projectstartdate;
    /**
     * 项目竣工时间
     */
    @Excel(name = "项目竣工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目竣工时间")
    private java.util.Date projectenddate;
    /**
     * 总建筑面积:万m2
     */
    @Excel(name = "总建筑面积:万m2", width = 15)
    @ApiModelProperty(value = "总建筑面积:万m2")
    private java.math.BigDecimal totalbuiltarea;
    /**
     * 示范面积:万m2
     */
    @Excel(name = "示范面积:万m2", width = 15)
    @ApiModelProperty(value = "示范面积:万m2")
    private java.math.BigDecimal demonstrationarea;
    /**
     * 太阳能光伏装机容量(k/W)
     */
    @Excel(name = "太阳能光伏装机容量(kW)", width = 15)
    @ApiModelProperty(value = "太阳能光伏装机容量(kW)")
    private java.math.BigDecimal solarenergyvo;
    /**
     * 项目总投资:万元
     */
    @Excel(name = "项目总投资:万元", width = 15)
    @ApiModelProperty(value = "项目总投资:万元")
    private java.math.BigDecimal totalinvest;
    /**
     * 资金来源
     */
    @Excel(name = "资金来源", width = 15)
    @ApiModelProperty(value = "资金来源")
    private java.lang.String moneyly;
    /**
     * 可再生能源利用率
     */
    @Excel(name = "可再生能源利用率", width = 15)
    @ApiModelProperty(value = "可再生能源利用率")
    private java.math.BigDecimal kzsrate;
    /**
     * 项目状态
     */
    @Excel(name = "项目状态", width = 15)
    @ApiModelProperty(value = "项目状态")
    private java.lang.String projectStatus;
    /**
     * 项目状态小类
     */
    @Excel(name = "项目状态小类", width = 15)
    @ApiModelProperty(value = "项目状态小类")
    private java.lang.String projectStatussmall;
    /**
     * 可再生能源利用类型
     */
    @Excel(name = "可再生能源利用类型", width = 15)
    @ApiModelProperty(value = "可再生能源利用类型")
    private java.lang.String kzstype;
    /**
     * 建设单位名称
     */
    @Excel(name = "建设单位名称", width = 15)
    @ApiModelProperty(value = "建设单位名称")
    private java.lang.String buildunit;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15)
    @ApiModelProperty(value = "项目负责人")
    private java.lang.String buildcharger;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String buildchargerphone;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String buildlinkman;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String buildlinkmanphone;
    /**
     * 设计单位名称
     */
    @Excel(name = "设计单位名称", width = 15)
    @ApiModelProperty(value = "设计单位名称")
    private java.lang.String designunit;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15)
    @ApiModelProperty(value = "项目负责人")
    private java.lang.String designcharger;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String designchargerphone;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String designlinkman;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String designlinkmanphone;
    /**
     * 施工单位名称
     */
    @Excel(name = "施工单位名称", width = 15)
    @ApiModelProperty(value = "施工单位名称")
    private java.lang.String constructunit;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15)
    @ApiModelProperty(value = "项目负责人")
    private java.lang.String constructcharger;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String constructchargerphone;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String constructlinkman;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String constructlinkmanphone;
    /**
     * 咨询或技术支持单位名称
     */
    @Excel(name = "咨询或技术支持单位名称", width = 15)
    @ApiModelProperty(value = "咨询或技术支持单位名称")
    private java.lang.String seekunit;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15)
    @ApiModelProperty(value = "项目负责人")
    private java.lang.String seekcharger;
    /**
     * 联系方式
     */
//    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String seekchargerphone;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String seeklinkman;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String seeklinkmanphone;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,0或null:未删除；1:已删除
     */
//    @Excel(name = "是否删除,0或null:未删除；1:已删除", width = 15)
    @ApiModelProperty(value = "是否删除,0或null:未删除；1:已删除")
    private java.math.BigDecimal isdelete;
    /**
     * 市州住建部门
     */
    @Excel(name = "市州住建部门", width = 15)
    @ApiModelProperty(value = "市州住建部门")
    private java.lang.String areaunit;
    /**
     * 联系部门
     */
    @Excel(name = "联系部门", width = 15)
    @ApiModelProperty(value = "联系部门")
    private java.lang.String areacharger;
    /**
     * 联系方式
     */
//    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String areachargerphone;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private java.lang.String arealinkman;
    /**
     * 联系方式
     */
    @Excel(name = "联系方式", width = 15)
    @ApiModelProperty(value = "联系方式")
    private java.lang.String arealinkmanphone;
    /**
     * 项目概况：描述地理位置、用地面积、建筑面积、工程性质、工程投资、结构形式、示范特点及范围等情况、并提供项目效果图
     */
    @Excel(name = "项目概况：描述地理位置、用地面积、建筑面积、工程性质、工程投资、结构形式、示范特点及范围等情况、并提供项目效果图", width = 15)
    @ApiModelProperty(value = "项目概况：描述地理位置、用地面积、建筑面积、工程性质、工程投资、结构形式、示范特点及范围等情况、并提供项目效果图")
    private java.lang.String simplesummary;
    /**
     * 主要示范技术措施简介
     */
    @Excel(name = "主要示范技术措施简介", width = 15)
    @ApiModelProperty(value = "主要示范技术措施简介")
    private java.lang.String sfjscsintro;
    /**
     * 项目创新点及推广价值
     */
    @Excel(name = "项目创新点及推广价值", width = 15)
    @ApiModelProperty(value = "项目创新点及推广价值")
    private java.lang.String xmcxdjtgjz;
    /**
     * 综合效益分析
     */
    @Excel(name = "综合效益分析", width = 15)
    @ApiModelProperty(value = "综合效益分析")
    private java.lang.String zhxyfx;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 2 通过 3 退回
     */
//    @Excel(name = "申报状态  0未上报 1 已上报 2 通过 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 2 通过 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 行政区划代码
     */
    @Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    @Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    @TableField(exist = false)
    private Integer spStatus = 0;
    @TableField(exist = false)
    private String auditopinion = "";
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
