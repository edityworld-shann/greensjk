package org.jeecg.modules.green.xmyssq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科研项目申报验收经费决算表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_scientificexpenditureclear")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_scientificexpenditureclear对象", description = "科研项目申报验收经费决算表")
public class ZgsScientificexpenditureclear implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 科研项目申报验收基本信息表主键
     */
    @Excel(name = "科研项目申报验收基本信息表主键", width = 15)
    @ApiModelProperty(value = "科研项目申报验收基本信息表主键")
    private java.lang.String baseguid;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 总收入
     */
    @Excel(name = "总收入", width = 15)
    @ApiModelProperty(value = "总收入")
    private java.math.BigDecimal totalincome;
    /**
     * 省级建设科技计划拨款
     */
    @Excel(name = "省级建设科技计划拨款", width = 15)
    @ApiModelProperty(value = "省级建设科技计划拨款")
    private java.math.BigDecimal provinceappropriation;
    /**
     * 单位自筹
     */
    @Excel(name = "单位自筹", width = 15)
    @ApiModelProperty(value = "单位自筹")
    private java.math.BigDecimal unitraised;
    /**
     * 其他
     */
    @Excel(name = "其他", width = 15)
    @ApiModelProperty(value = "其他")
    private java.math.BigDecimal otherincome;
    /**
     * 总支出
     */
    @Excel(name = "总支出", width = 15)
    @ApiModelProperty(value = "总支出")
    private java.math.BigDecimal totalexpense;
    /**
     * 省级建设科技计划拨款总额
     */
    @Excel(name = "省级建设科技计划拨款总额", width = 15)
    @ApiModelProperty(value = "省级建设科技计划拨款总额")
    private java.math.BigDecimal provinceexpense;
    /**
     * 设备费
     */
    @Excel(name = "设备费", width = 15)
    @ApiModelProperty(value = "设备费")
    private java.math.BigDecimal equipment;
    /**
     * 省级设备费
     */
    @Excel(name = "省级设备费", width = 15)
    @ApiModelProperty(value = "省级设备费")
    private java.math.BigDecimal provinceequipment;
    /**
     * 材料费
     */
    @Excel(name = "材料费", width = 15)
    @ApiModelProperty(value = "材料费")
    private java.math.BigDecimal material;
    /**
     * 省级材料费
     */
    @Excel(name = "省级材料费", width = 15)
    @ApiModelProperty(value = "省级材料费")
    private java.math.BigDecimal provincematerial;
    /**
     * 测试化验加工费
     */
    @Excel(name = "测试化验加工费", width = 15)
    @ApiModelProperty(value = "测试化验加工费")
    private java.math.BigDecimal processcost;
    /**
     * 省级测试化验加工费
     */
    @Excel(name = "省级测试化验加工费", width = 15)
    @ApiModelProperty(value = "省级测试化验加工费")
    private java.math.BigDecimal provinceprocesscost;
    /**
     * 燃料动力费
     */
    @Excel(name = "燃料动力费", width = 15)
    @ApiModelProperty(value = "燃料动力费")
    private java.math.BigDecimal fuelcost;
    /**
     * 省级燃料动力费
     */
    @Excel(name = "省级燃料动力费", width = 15)
    @ApiModelProperty(value = "省级燃料动力费")
    private java.math.BigDecimal provincefuelcost;
    /**
     * 差旅费
     */
    @Excel(name = "差旅费", width = 15)
    @ApiModelProperty(value = "差旅费")
    private java.math.BigDecimal travelexpense;
    /**
     * 省级差旅费
     */
    @Excel(name = "省级差旅费", width = 15)
    @ApiModelProperty(value = "省级差旅费")
    private java.math.BigDecimal provincetravelexpense;
    /**
     * 会议费
     */
    @Excel(name = "会议费", width = 15)
    @ApiModelProperty(value = "会议费")
    private java.math.BigDecimal coferemce;
    /**
     * 省级会议费
     */
    @Excel(name = "省级会议费", width = 15)
    @ApiModelProperty(value = "省级会议费")
    private java.math.BigDecimal provincecoferemce;
    /**
     * 合作与交流费
     */
    @Excel(name = "合作与交流费", width = 15)
    @ApiModelProperty(value = "合作与交流费")
    private java.math.BigDecimal exchange;
    /**
     * 省级合作与交流费
     */
    @Excel(name = "省级合作与交流费", width = 15)
    @ApiModelProperty(value = "省级合作与交流费")
    private java.math.BigDecimal provinceexchange;
    /**
     * 出版/文献/信息传播/知识产权事务费
     */
    @Excel(name = "出版/文献/信息传播/知识产权事务费", width = 15)
    @ApiModelProperty(value = "出版/文献/信息传播/知识产权事务费")
    private java.math.BigDecimal appear;
    /**
     * 省级出版费
     */
    @Excel(name = "省级出版费", width = 15)
    @ApiModelProperty(value = "省级出版费")
    private java.math.BigDecimal provinceappear;
    /**
     * 劳务费
     */
    @Excel(name = "劳务费", width = 15)
    @ApiModelProperty(value = "劳务费")
    private java.math.BigDecimal servicefee;
    /**
     * 省级劳务费
     */
    @Excel(name = "省级劳务费", width = 15)
    @ApiModelProperty(value = "省级劳务费")
    private java.math.BigDecimal provinceservicefee;
    /**
     * 专家咨询费
     */
    @Excel(name = "专家咨询费", width = 15)
    @ApiModelProperty(value = "专家咨询费")
    private java.math.BigDecimal consult;
    /**
     * 省级专家咨询费
     */
    @Excel(name = "省级专家咨询费", width = 15)
    @ApiModelProperty(value = "省级专家咨询费")
    private java.math.BigDecimal provinceconsult;
    /**
     * 其他（含培训费
     */
    @Excel(name = "其他（含培训费", width = 15)
    @ApiModelProperty(value = "其他（含培训费")
    private java.math.BigDecimal otherexpense;
    /**
     * 省级其他费用
     */
    @Excel(name = "省级其他费用", width = 15)
    @ApiModelProperty(value = "省级其他费用")
    private java.math.BigDecimal provinceotherexpense;
    /**
     * createaccountname
     */
    @Excel(name = "createaccountname", width = 15)
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
    @Excel(name = "modifyaccountname", width = 15)
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
    @Excel(name = "modifyusername", width = 15)
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    private java.lang.String status;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
