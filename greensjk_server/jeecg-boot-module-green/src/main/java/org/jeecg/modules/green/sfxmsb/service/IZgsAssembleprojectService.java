package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsAssembleproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 装配式建筑示范工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsAssembleprojectService extends IService<ZgsAssembleproject> {

}
