package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectexecutive;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificprojectexecutiveMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectexecutiveService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研项目验收申请项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsScientificprojectexecutiveServiceImpl extends ServiceImpl<ZgsScientificprojectexecutiveMapper, ZgsScientificprojectexecutive> implements IZgsScientificprojectexecutiveService {

}
