package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目专家关系信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Data
@TableName("zgs_projectlibraryexpert")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectlibraryexpert对象", description = "项目专家关系信息表")
public class ZgsProjectlibraryexpert implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * 项目表GUID,与T_ProjectLibrary.GUID关联
     */
    @Excel(name = "项目表GUID,与T_ProjectLibrary.GUID关联", width = 15)
    @ApiModelProperty(value = "项目表GUID,与T_ProjectLibrary.GUID关联")
    private java.lang.String projectlibraryguid;
    /**
     * 专家GUID,与T_ExpertInfo.guid关联
     */
    @Excel(name = "专家GUID,与T_ExpertInfo.guid关联", width = 15)
    @ApiModelProperty(value = "专家GUID,与T_ExpertInfo.guid关联")
    private java.lang.String expertguid;
    /**
     * 业务GUID,与项目业务表.guid关联
     */
    @Excel(name = "业务GUID,与项目业务表.guid关联", width = 15)
    @ApiModelProperty(value = "业务GUID,与项目业务表.guid关联")
    private java.lang.String businessguid;
    /**
     * 业务类型区分,科技攻关,软科学
     */
    @Excel(name = "业务类型区分,科技攻关,软科学", width = 15)
    @ApiModelProperty(value = "业务类型区分,科技攻关,软科学")
    private java.lang.String businesstype;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createtime;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifytime;
    /**
     * 是否删除,0或null:未删除；1:已删除
     */
    @Excel(name = "是否删除,0或null:未删除；1:已删除", width = 15)
    @ApiModelProperty(value = "是否删除,0或null:未删除；1:已删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 审核结论，同意和不同意名称
     */
    @Excel(name = "审核结论，同意和不同意名称", width = 15)
    @ApiModelProperty(value = "审核结论，同意和不同意名称")
    private java.lang.String auditconclusion;
    /**
     * 审核意见
     */
    @Excel(name = "审核意见", width = 15)
    @ApiModelProperty(value = "审核意见")
    private java.lang.String auditidea;
    /**
     * 审核日期
     */
    @Excel(name = "审核日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "审核日期")
    private java.util.Date auditdate;
    /**
     * 截止审核日期
     */
    @Excel(name = "截止审核日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "截止审核日期")
    private java.util.Date validdate;
    /**
     * 审核结论，同意和不同意编码
     */
    @Excel(name = "审核结论，同意和不同意编码", width = 15)
    @ApiModelProperty(value = "审核结论，同意和不同意编码")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditconclusionnum;
    /**
     * 是否评审，1：已评审（反馈给省厅）；否则未评审；
     */
    @Excel(name = "是否评审，1：已评审（反馈给省厅）；否则未评审；", width = 15)
    @ApiModelProperty(value = "是否评审，1：已评审（反馈给省厅）；否则未评审；")
    private java.math.BigDecimal ispingshen;
    @TableField(exist = false)
    @ValidateEncryptEntity(isEncrypt = true)
    private String username;
    @TableField(exist = false)
    private String projecttypenum;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
