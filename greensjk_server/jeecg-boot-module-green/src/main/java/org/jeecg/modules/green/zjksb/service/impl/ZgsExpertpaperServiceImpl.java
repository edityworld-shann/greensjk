package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertpaper;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertpaperMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertpaperService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库专家论文信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertpaperServiceImpl extends ServiceImpl<ZgsExpertpaperMapper, ZgsExpertpaper> implements IZgsExpertpaperService {

}
