package org.jeecg.modules.green.kyxmsb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;

/**
 * @Description: 联合申报单位表
 * @Author: jeecg-boot
 * @Date: 2022-02-10
 * @Version: V1.0
 */
@Data
@TableName("zgs_sciencejointunit")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_sciencejointunit对象", description = "联合申报单位表")
public class ZgsSciencejointunit implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 对应业务表主键
     */
    @Excel(name = "对应业务表主键", width = 15)
    @ApiModelProperty(value = "对应业务表主键")
    private java.lang.String scienceguid;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 法人姓名
     */
    @Excel(name = "法人姓名", width = 15)
    @ApiModelProperty(value = "法人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalname;
    /**
     * 法人电 话
     */
    @Excel(name = "法人电话", width = 15)
    @ApiModelProperty(value = "法人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legaltel;
    /**
     * 法人邮箱
     */
    @Excel(name = "法人邮箱", width = 15)
    @ApiModelProperty(value = "法人邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalemail;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkman;
    /**
     * 联系人电 话
     */
    @Excel(name = "联系人电 话", width = 15)
    @ApiModelProperty(value = "联系人电 话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkmantel;
    /**
     * 联系人邮箱
     */
    @Excel(name = "联系人邮箱", width = 15)
    @ApiModelProperty(value = "联系人邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkmanemail;
    /**
     * 示范类型
     */
    @Excel(name = "示范类型", width = 15)
    @ApiModelProperty(value = "示范类型")
    private java.lang.String sciencetype;
    /**
     * 申报单位
     */
    @Excel(name = "申报单位", width = 15)
    @ApiModelProperty(value = "申报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunit;

    @ApiModelProperty(value = "任务书标签1，默认为空")
    private java.lang.String tag;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    @ApiModelProperty(value = "通信地址")
    @Column(name = "communication_address")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String communicationAddress;

    @ApiModelProperty(value = "邮政编码")
    @Column(name = "postal_code")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String postalCode;

    @ApiModelProperty(value = "单位属性")
    @Column(name = "unit_attribute")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unitAttribute;


}
