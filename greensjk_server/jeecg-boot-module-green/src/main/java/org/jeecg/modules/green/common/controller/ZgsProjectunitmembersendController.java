package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import org.jeecg.modules.green.common.service.IZgsProjectunitmembersendService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 账号短信下发表
 * @Author: jeecg-boot
 * @Date: 2022-05-06
 * @Version: V1.0
 */
@Api(tags = "账号短信下发表")
@RestController
@RequestMapping("/common/zgsProjectunitmembersend")
@Slf4j
public class ZgsProjectunitmembersendController extends JeecgController<ZgsProjectunitmembersend, IZgsProjectunitmembersendService> {
    @Autowired
    private IZgsProjectunitmembersendService zgsProjectunitmembersendService;

    /**
     * 分页列表查询
     *
     * @param zgsProjectunitmembersend
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "账号短信下发表-分页列表查询")
    @ApiOperation(value = "账号短信下发表-分页列表查询", notes = "账号短信下发表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjectunitmembersend zgsProjectunitmembersend,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsProjectunitmembersend> queryWrapper = QueryGenerator.initQueryWrapper(zgsProjectunitmembersend, req.getParameterMap());
        Page<ZgsProjectunitmembersend> page = new Page<ZgsProjectunitmembersend>(pageNo, pageSize);
        IPage<ZgsProjectunitmembersend> pageList = zgsProjectunitmembersendService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsProjectunitmembersend
     * @return
     */
    @AutoLog(value = "账号短信下发表-添加")
    @ApiOperation(value = "账号短信下发表-添加", notes = "账号短信下发表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjectunitmembersend zgsProjectunitmembersend) {
        zgsProjectunitmembersendService.save(zgsProjectunitmembersend);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsProjectunitmembersend
     * @return
     */
    @AutoLog(value = "账号短信下发表-编辑")
    @ApiOperation(value = "账号短信下发表-编辑", notes = "账号短信下发表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjectunitmembersend zgsProjectunitmembersend) {
        zgsProjectunitmembersendService.updateById(zgsProjectunitmembersend);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "账号短信下发表-通过id删除")
    @ApiOperation(value = "账号短信下发表-通过id删除", notes = "账号短信下发表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjectunitmembersendService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "账号短信下发表-批量删除")
    @ApiOperation(value = "账号短信下发表-批量删除", notes = "账号短信下发表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsProjectunitmembersendService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "账号短信下发表-通过id查询")
    @ApiOperation(value = "账号短信下发表-通过id查询", notes = "账号短信下发表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsProjectunitmembersend zgsProjectunitmembersend = zgsProjectunitmembersendService.getById(id);
        if (zgsProjectunitmembersend == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsProjectunitmembersend);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsProjectunitmembersend
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsProjectunitmembersend zgsProjectunitmembersend) {
        return super.exportXls(request, zgsProjectunitmembersend, ZgsProjectunitmembersend.class, "账号短信下发表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjectunitmembersend.class);
    }

}
