package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import org.jeecg.modules.green.common.mapper.ZgsProjectunithistoryMapper;
import org.jeecg.modules.green.common.service.IZgsProjectunithistoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 黑名单历史记录
 * @Author: jeecg-boot
 * @Date: 2022-03-06
 * @Version: V1.0
 */
@Service
public class ZgsProjectunithistoryServiceImpl extends ServiceImpl<ZgsProjectunithistoryMapper, ZgsProjectunithistory> implements IZgsProjectunithistoryService {

    @Override
    public int updateUserStatus(String userId) {
        return this.baseMapper.updateUserStatus(userId);
    }
}
