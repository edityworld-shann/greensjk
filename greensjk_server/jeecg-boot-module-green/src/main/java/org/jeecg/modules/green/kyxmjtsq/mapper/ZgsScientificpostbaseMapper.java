package org.jeecg.modules.green.kyxmjtsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目申报结项基本信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsScientificpostbaseMapper extends BaseMapper<ZgsScientificpostbase> {

}
