package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenAreadetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: zgs_report_monthgreen_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface ZgsReportMonthgreenAreadetailMapper extends BaseMapper<ZgsReportMonthgreenAreadetail> {

}
