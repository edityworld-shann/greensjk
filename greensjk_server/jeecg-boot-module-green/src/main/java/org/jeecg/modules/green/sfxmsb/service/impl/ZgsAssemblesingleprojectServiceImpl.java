package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemblesingleproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsAssemblesingleprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsAssemblesingleprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 装配式示范工程单体建筑基本信息
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsAssemblesingleprojectServiceImpl extends ServiceImpl<ZgsAssemblesingleprojectMapper, ZgsAssemblesingleproject> implements IZgsAssemblesingleprojectService {

}
