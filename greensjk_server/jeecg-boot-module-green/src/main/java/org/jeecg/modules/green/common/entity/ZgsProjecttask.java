package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.sfxmsb.entity.ZgsCommonFee;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 示范项目任务书
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Data
@TableName("zgs_projecttask")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projecttask对象", description = "示范项目任务书")
public class ZgsProjecttask implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * T_PROJECTLIBRARY.guid
     */
//    @Excel(name = "T_PROJECTLIBRARY.guid", width = 15)
    @ApiModelProperty(value = "T_PROJECTLIBRARY.guid")
    private java.lang.String projectguid;
    /**
     * 示范类别
     */
    @Excel(name = "示范类别", width = 15, orderNum = "4")
    @ApiModelProperty(value = "示范类别")
    private java.lang.String demonstratetype;
    /**
     * 示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)
     */
//    @Excel(name = "示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)", width = 15)
    @ApiModelProperty(value = "示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)")
    private java.lang.String demonstratetypenum;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 组织机构代码
     */
    @Excel(name = "组织机构代码", width = 15, orderNum = "3")
    @ApiModelProperty(value = "组织机构代码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String organizationcode;
    /**
     * 单位通讯地址
     */
    @Excel(name = "单位通讯地址", width = 15, orderNum = "3")
    @ApiModelProperty(value = "单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unitaddress;
    /**
     * 邮编
     */
    @Excel(name = "邮编", width = 15, orderNum = "3")
    @ApiModelProperty(value = "邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String zipcode;
    /**
     * 单位性质编号
     */
    @Excel(name = "单位性质编号", width = 15, orderNum = "3")
    @ApiModelProperty(value = "单位性质编号")
    private java.lang.String unitnaturecode;
    /**
     * 单位性质
     */
    @Excel(name = "单位性质", width = 15, orderNum = "3")
    @ApiModelProperty(value = "单位性质")
    private java.lang.String unitnature;
    /**
     * 上级行政主管部门
     */
//    @Excel(name = "上级行政主管部门", width = 15)
    @ApiModelProperty(value = "上级行政主管部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String department;
    /**
     * 项目负责人性别
     */
    @Excel(name = "项目负责人性别", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headsex;
    /**
     * 项目负责人学位
     */
    @Excel(name = "项目负责人学位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人学位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String degree;
    /**
     * 项目负责人职称
     */
    @Excel(name = "项目负责人职称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String technical;
    /**
     * 项目负责人所在单位
     */
    @Excel(name = "项目负责人所在单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人所在单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headunit;
    /**
     * 身份证号
     */
    @Excel(name = "身份证号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "身份证号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String identitycard;
    /**
     * 项目负责人手机号
     */
    @Excel(name = "项目负责人手机号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人手机号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headmobile;
    /**
     * 项目负责人email
     */
    @Excel(name = "项目负责人email", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人email")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String heademail;
    /**
     * 项目负责人电话
     */
    @Excel(name = "项目负责人电话", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headtel;
    /**
     * 1：上报
     */
//    @Excel(name = "1：上报", width = 15)
    @ApiModelProperty(value = "1：上报")
    private java.lang.String status;
    /**
     * 申报时间
     */
    @Excel(name = "申报时间", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "申报时间")
    private java.util.Date applydate;
    /**
     * 专业
     */
    @Excel(name = "专业", width = 15, orderNum = "3")
    @ApiModelProperty(value = "专业")
    private java.lang.String major;
    /**
     * 业务表主键
     */
//    @Excel(name = "业务表主键", width = 15)
    @ApiModelProperty(value = "业务表主键")
    private java.lang.String buildid;
    /**
     * 项目示范内容及考核目标
     */
    @Excel(name = "项目示范内容及考核目标", width = 15, orderNum = "3")
    @ApiModelProperty(value = "项目示范内容及考核目标")
    private java.lang.String prodemoncontent;
    /**
     * 年度示范内容及考核目标
     */
    @Excel(name = "年度示范内容及考核目标", width = 15, orderNum = "3")
    @ApiModelProperty(value = "年度示范内容及考核目标")
    private java.lang.String annualdemoncontent;
    /**
     * backreason
     */
//    @Excel(name = "backreason", width = 15)
    @ApiModelProperty(value = "backreason")
    private java.lang.String backreason;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype = new BigDecimal(0);
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15)
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15)
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 初审退回意见
     */
//    @Excel(name = "初审退回意见", width = 15)
    @ApiModelProperty(value = "初审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;

    @ApiModelProperty(value = "省科技经费备注")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String fundRemark;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15)
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15)
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15)
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * higherupunit
     */
//    @Excel(name = "higherupunit", width = 15)
    @ApiModelProperty(value = "higherupunit")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * higherupunitnum
     */
//    @Excel(name = "higherupunitnum", width = 15)
    @ApiModelProperty(value = "higherupunitnum")
    private java.lang.String higherupunitnum;
    /**
     * word导出文件路径
     */
//    @Excel(name = "word导出文件路径", width = 15)
    @ApiModelProperty(value = "word导出文件路径")
    private String pdfUrl;
    @ApiModelProperty(value = "是否可强制编辑:默认为空，1为可编辑")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String editStatus;
    @TableField(exist = false)
    private List<ZgsTaskprojectmainparticipant> zgsTaskparticipantList;
    //项目验收结题证书-示范项目验收证书人员
    @TableField(exist = false)
    private List<ZgsExamplecertparticipant> zgsExamplecertparticipantList;
    @TableField(exist = false)
    private List<ZgsBuildjointunit> zgsBuildjointunitList;
    @TableField(exist = false)
    private List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList;
    @TableField(exist = false)
    private ZgsProjectlibrary zgsProjectlibrary;
    @TableField(exist = false)
    private String projectsummary;
    @TableField(exist = false)
    private ZgsCommonFee zgsCommonFee;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    //关联数据
    @TableField(exist = false)
    private String projectnum;
    @TableField(exist = false)
    private String projectname;
    @TableField(exist = false)
    private String bid;
    @TableField(exist = false)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private java.util.Date startdate;
    @TableField(exist = false)
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private java.util.Date enddate;
    @TableField(exist = false)
    private String tasknum;
    @TableField(exist = false)
    private String projectleader;
    @TableField(exist = false)
    private String commitmentunit;
    @TableField(exist = false)
    private String feeStatus;
    /**
     * 0省级科技资金未支持项目,1省级科技资金支持项目
     */
    @ApiModelProperty(value = "0省级科技资金未支持项目,1省级科技资金支持项目")
    private String fundType;
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    @ApiModelProperty(value = "逾期类型：默认为空，0逾期半年，1逾期半年以上")
    private String dealtype;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    //项目可以变更次数
    private Integer bgcount;
    @TableField(exist = false)
    private Integer isYuQ;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
    // 申报年份
    @TableField(exist = false)
    private String sbnf;
}
