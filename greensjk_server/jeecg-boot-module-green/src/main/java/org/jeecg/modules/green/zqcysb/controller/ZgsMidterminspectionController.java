package org.jeecg.modules.green.zqcysb.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import org.jeecg.modules.green.xmyssq.service.IZgsSaexpertService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 建设科技示范项目实施情况中期查验申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "建设科技示范项目实施情况中期查验申请业务主表")
@RestController
@RequestMapping("/zqcysb/zgsMidterminspection")
@Slf4j
public class ZgsMidterminspectionController extends JeecgController<ZgsMidterminspection, IZgsMidterminspectionService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsSaexpertService zgsSaexpertService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;

    /**
     * 分页列表查询
     *
     * @param zgsMidterminspection
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-分页列表查询")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-分页列表查询", notes = "建设科技示范项目实施情况中期查验申请业务主表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsMidterminspection zgsMidterminspection,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsMidterminspection> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsMidterminspection.getProjectname())) {
            queryWrapper.like("projectname", zgsMidterminspection.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsMidterminspection.getProjectnum())) {
            queryWrapper.like("projectnum", zgsMidterminspection.getProjectnum());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnum,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsMidterminspection.getProjectstage())) {
            if (!"-1".equals(zgsMidterminspection.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsMidterminspection.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initMidterminspectionSelect(queryWrapper, zgsMidterminspection.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsMidterminspection.getStatus(), sysUser, 1);
        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    QueryWrapper<ZgsSaexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
                    zgsSofttechexpertQueryWrapper.eq("expertguid", expertId);
                    zgsSofttechexpertQueryWrapper.ne("isdelete", 1);
//                    zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSofttechexpertQueryWrapper);
                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
                        queryWrapper.and(qw -> {
                            qw.and(w1 -> {
                                for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
                                    w1.or(w2 -> {
                                        w2.eq("id", zgsSaexpert.getBusinessguid());
                                    });
                                }
                            });
                        });
                    } else {
                        queryWrapper.eq("id", "null");
                    }
                }
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsMidterminspection.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("projectenddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("projectenddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        }
        Page<ZgsMidterminspection> page = new Page<ZgsMidterminspection>(pageNo, pageSize);
        // IPage<ZgsMidterminspection> pageList = zgsMidterminspectionService.page(page, queryWrapper);

        // 推荐单位查看项目统计，状态为空
        IPage<ZgsMidterminspection> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsMidterminspection> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsMidterminspection> pageList = null;

        // 示范项目中期审查
        redisUtil.expire("sfxmzqsc",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmzqscOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmzqscOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsMidterminspection.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsMidterminspection.getDspForTjdw())) {
            if (zgsMidterminspection.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("sfxmzqsc")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsMidterminspectionService.page(page, queryWrapper);
                    redisUtil.set("sfxmzqsc",pageListForProjectCount.getTotal());
                }
            }
            if (zgsMidterminspection.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("sfxmzqscOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsMidterminspectionService.page(page, queryWrapper);
                    redisUtil.set("sfxmzqscOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsMidterminspection.getDspForTjdw()) && StringUtils.isBlank(zgsMidterminspection.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsMidterminspectionService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsMidterminspection.getDspForTjdw()) && StringUtils.isNotBlank(zgsMidterminspection.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsMidterminspectionService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsMidterminspection.getDspForSt()) && zgsMidterminspection.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsMidterminspectionService.page(page, queryWrapper);
            redisUtil.set("sfxmzqscOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsMidterminspection.getStatus(), sysUser, 1);
            pageList = zgsMidterminspectionService.page(page, queryWrapper);
        }



        /*// 示范项目中期审查
        redisUtil.expire("sfxmzqsc",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmzqsc")) {
            if (StringUtils.isNotBlank(zgsMidterminspection.getFlagByWorkTable()) && zgsMidterminspection.getFlagByWorkTable().equals("1")) {
                redisUtil.set("sfxmzqsc",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmzqscOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmzqscOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsMidterminspection.getDspForSt()) && zgsMidterminspection.getDspForSt().equals("2")) {
                redisUtil.set("sfxmzqscOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmzqscOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmzqscOfTjdw")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsMidterminspection.getDspForTjdw()) && zgsMidterminspection.getDspForTjdw().equals("3")) {
                redisUtil.set("sfxmzqscOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsMidterminspection zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    // 将待审核、初审通过、终审通过、形审通过、专家通过的所有项目的退回原因置空
                    if (zgsInfo.getStatus().equals("1") || zgsInfo.getStatus().equals("4") || zgsInfo.getStatus().equals("8") ||
                            zgsInfo.getStatus().equals("2") || zgsInfo.getStatus().equals("10")) {

                        zgsInfo.setAuditopinion("");
                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //专家审批
                        QueryWrapper<ZgsSaexpert> wrapper = new QueryWrapper();
                        wrapper.eq("expertguid", expertId);
                        wrapper.eq("businessguid", zgsInfo.getId());
                        ZgsSaexpert saexpert = zgsSaexpertService.getOne(wrapper);
                        if (!(saexpert != null && StringUtils.isNotEmpty(saexpert.getAuditconclusionnum()))) {
                            pageList.getRecords().get(m).setSpStatus(2);
                        }
                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsMidterminspection zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    // 将待审核、初审通过、终审通过、形审通过、专家通过的所有项目的退回原因置空
                    if (zgsInfo.getStatus().equals("1") || zgsInfo.getStatus().equals("4") || zgsInfo.getStatus().equals("8") ||
                            zgsInfo.getStatus().equals("2") || zgsInfo.getStatus().equals("10")) {
                        zgsInfo.setAuditopinion("");
                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    } else if (GlobalConstants.SHENHE_STATUS8.equals(zgsInfo.getStatus())) {
                        //分配专家
//                        QueryWrapper<ZgsSaexpert> zgsSaexpertQueryWrapper = new QueryWrapper();
//                        zgsSaexpertQueryWrapper.eq("businessguid", zgsInfo.getId());
//                        zgsSaexpertQueryWrapper.ne("isdelete", 1);
//                        List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSaexpertQueryWrapper);
//                        if (!(zgsSaexpertList != null && zgsSaexpertList.size() > 0)) {
//                            //专家分配
//                            Calendar calendar = new GregorianCalendar();
//                            calendar.setTime(new Date());
//                            calendar.add(calendar.DATE, -15);//15天前未分配的还可以继续分配，超了15天分配按钮消失
//                            if (calendar.getTime().compareTo(zgsInfo.getApplydate()) < 0) {
//                                pageList.getRecords().get(m).setSpStatus(3);
//                            }
//                        }
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid()) && StringUtils.isNotEmpty(zgsInfo.getProjectname())) {
                        QueryWrapper<ZgsProjecttask> queryWrapper0 = new QueryWrapper<ZgsProjecttask>();
                        queryWrapper0.eq("projectguid", zgsInfo.getProjectlibraryguid());
                        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper0.ne("isdelete", 1);
                        ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper0);
                        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                        } else {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                        }
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-撤回")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-撤回", notes = "建设科技示范项目实施情况中期查验申请业务主表-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsMidterminspection zgsMidterminspection) {
        if (zgsMidterminspection != null) {
            String id = zgsMidterminspection.getId();
            String bid = zgsMidterminspection.getProjectlibraryguid();
            String status = zgsMidterminspection.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3).equals(zgsMidterminspection.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsMidterminspection info = new ZgsMidterminspection();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsMidterminspectionService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsMidterminspection
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-审批")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-审批", notes = "建设科技示范项目实施情况中期查验申请业务主表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsMidterminspection zgsMidterminspection) {
        zgsMidterminspectionService.spProject(zgsMidterminspection);
        String projectlibraryguid = zgsMidterminspectionService.getById(zgsMidterminspection.getId()).getProjectlibraryguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsMidterminspection
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-添加")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-添加", notes = "建设科技示范项目实施情况中期查验申请业务主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsMidterminspection zgsMidterminspection) {

        //        1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsMidterminspection.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsMidterminspection.getStatus())) {
            //保存并上报
            zgsMidterminspection.setApplydate(new Date());
        }
        zgsMidterminspection.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsMidterminspection.setCreateaccountname(sysUser.getUsername());
        zgsMidterminspection.setCreateusername(sysUser.getRealname());
        zgsMidterminspection.setCreatedate(new Date());
        //需判断projectlibraryguid不能为空，且不可重复添加
        if (StringUtils.isNotEmpty(zgsMidterminspection.getProjectlibraryguid())) {
            QueryWrapper<ZgsMidterminspection> queryWrapper = new QueryWrapper();
            queryWrapper.eq("projectlibraryguid", zgsMidterminspection.getProjectlibraryguid());
            List<ZgsMidterminspection> list = zgsMidterminspectionService.list(queryWrapper);
            if (list != null && list.size() > 0) {
                // return Result.error("不能重复添加！");
                // 针对中期查验已驳回项目再重新添加，则需删除原有已驳回项目
                for (ZgsMidterminspection z:list
                     ) {
                    zgsMidterminspectionService.removeById(z.getId());
                }
            }
        } else {
            return Result.error("请选择关联申报项目！");
        }
        //判断项目终止
//        QueryWrapper<ZgsProjecttask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("projectguid", zgsMidterminspection.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsProjecttask> listHist = zgsProjecttaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsMidterminspection.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
        zgsMidterminspection.setProjectstagestatus(zgsMidterminspection.getStatus());
        zgsMidterminspectionService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsMidterminspection, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsMidterminspection.getStatus()) && StringUtils.isNotEmpty(zgsMidterminspection.getProjectlibraryguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsMidterminspection.getProjectlibraryguid());
        }
//        redisUtil.set(zgsMidterminspection.getProjectlibraryguid(), "中期查验阶段");
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsMidterminspection.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.MidtermInspection);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }

        //add  添加验收专家名单  20230616  rxl
        /*List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsMidterminspection.getZgsSacceptcertexpertList();
        if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
            for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList.get(a1);
                zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                zgsSacceptcertexpert.setBaseguid(zgsMidterminspection.getProjectlibraryguid());
                zgsSacceptcertexpert.setProjectStage("zqcy");  // 中期查验阶段
                zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());  // 企业ID
                zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                zgsSacceptcertexpert.setCreatedate(new Date());
                zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
            }
        }*/

        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsMidterminspection
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-编辑")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-编辑", notes = "建设科技示范项目实施情况中期查验申请业务主表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsMidterminspection zgsMidterminspection) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsMidterminspection != null && StringUtils.isNotEmpty(zgsMidterminspection.getId())) {
            String id = zgsMidterminspection.getId();
            if (GlobalConstants.handleSubmit.equals(zgsMidterminspection.getStatus())) {
                //保存并上报
                zgsMidterminspection.setApplydate(new Date());
                zgsMidterminspection.setAuditopinion(null);
//                zgsMidterminspection.setReturntype(null);
            } else {
                zgsMidterminspection.setApplydate(null);
                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                ZgsMidterminspection projectTask = zgsMidterminspectionService.getById(id);
                if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                    zgsMidterminspection.setStatus(GlobalConstants.SHENHE_STATUS3);
                }
            }
            zgsMidterminspection.setModifyaccountname(sysUser.getUsername());
            zgsMidterminspection.setModifyusername(sysUser.getRealname());
            zgsMidterminspection.setModifydate(new Date());
            zgsMidterminspection.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
            zgsMidterminspection.setProjectstagestatus(zgsMidterminspection.getStatus());
            zgsMidterminspectionService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsMidterminspection, ValidateEncryptEntityUtil.isEncrypt));


            // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
            QueryWrapper<ZgsSacceptcertexpert> expertQueryWrapper = new QueryWrapper<>();
            expertQueryWrapper.eq("baseguid",zgsMidterminspection.getProjectlibraryguid());
            expertQueryWrapper.eq("project_stage","zqcy");
            expertQueryWrapper.ne("isdelete",1);
            List<ZgsSacceptcertexpert> ZgsSacceptcertexpertList = zgsSacceptcertexpertService.list(expertQueryWrapper);
            if (ZgsSacceptcertexpertList.size() > 0) {
                // 删除历史专家信息
                QueryWrapper<ZgsSacceptcertexpert> expertDeleteWrapper = new QueryWrapper<>();
                expertDeleteWrapper.eq("baseguid",zgsMidterminspection.getProjectlibraryguid());
                expertDeleteWrapper.eq("project_stage","zqcy");
                expertDeleteWrapper.ne("isdelete",1);
                boolean bolResult = zgsSacceptcertexpertService.remove(expertDeleteWrapper);
                // 删除成功后，保存修改后的专家信息  rxl  20230620
                if (bolResult) {
                    List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsMidterminspection.getZgsSacceptcertexpertList();
                    if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
                        for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                            ZgsSacceptcertexpert zgsSacceptcertexpertForUpdate = zgsSacceptcertexpertList.get(a1);
                            zgsSacceptcertexpertForUpdate.setId(UUID.randomUUID().toString());
                            zgsSacceptcertexpertForUpdate.setBaseguid(zgsMidterminspection.getProjectlibraryguid());
                            zgsSacceptcertexpertForUpdate.setProjectStage("zqcy");   // 申报阶段
                            zgsSacceptcertexpertForUpdate.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsSacceptcertexpertForUpdate.setCreateaccountname(sysUser.getUsername());
                            zgsSacceptcertexpertForUpdate.setCreateusername(sysUser.getRealname());
                            zgsSacceptcertexpertForUpdate.setCreatedate(new Date());
                            zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpertForUpdate, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }

            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsMidterminspection.getStatus()) && StringUtils.isNotEmpty(zgsMidterminspection.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsMidterminspection.getProjectlibraryguid());
            }
            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMidterminspection.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.MidtermInspection);
                                zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id删除")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id删除", notes = "建设科技示范项目实施情况中期查验申请业务主表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsMidterminspectionService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-批量删除")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-批量删除", notes = "建设科技示范项目实施情况中期查验申请业务主表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsMidterminspectionService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询", notes = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsMidterminspection zgsMidterminspection = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsMidterminspection = ValidateEncryptEntityUtil.validateDecryptObject(zgsMidterminspectionService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsMidterminspection == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsMidterminspection);
            }
            if ((zgsMidterminspection.getFirstdate() != null || zgsMidterminspection.getFinishdate() != null) && zgsMidterminspection.getApplydate() != null) {
                zgsMidterminspection.setSpLogStatus(1);
            } else {
                zgsMidterminspection.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsMidterminspection = new ZgsMidterminspection();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.MidtermInspection);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsMidterminspection.setZgsMattermaterialList(zgsMattermaterialList);
            zgsMidterminspection.setSpLogStatus(0);
        }
        return Result.OK(zgsMidterminspection);
    }

    private void initProjectSelectById(ZgsMidterminspection zgsMidterminspection) {
        String enterpriseguid = zgsMidterminspection.getEnterpriseguid();
        String buildguid = zgsMidterminspection.getId();
        String projectlibraryguid = zgsMidterminspection.getProjectlibraryguid();  // 企业项目id
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid) && StringUtils.isNotEmpty(projectlibraryguid)) {
            //相关附件
            map.put("buildguid", buildguid);
            //后期查下MidtermInspection出现在哪张表里
            map.put("projecttypenum", GlobalConstants.MidtermInspection);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsMidterminspection.setZgsMattermaterialList(zgsMattermaterialList);
            }
            // add  将中期查验阶段关联的验收专家信息进行返回   rxl  20230619
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", projectlibraryguid);
            queryWrapper4.eq("project_stage", "zqcy");  // 查询当前项目在申报阶段的专家信息
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertexpertService.list(queryWrapper4);
            zgsMidterminspection.setZgsSacceptcertexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertexpertList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsMidterminspection
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsMidterminspection zgsMidterminspection,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsMidterminspection, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsMidterminspection> pageList = (IPage<ZgsMidterminspection>) result.getResult();
        List<ZgsMidterminspection> list = pageList.getRecords();
        List<ZgsMidterminspection> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "建设科技示范项目实施情况中期查验申请业务主表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsMidterminspection.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsMidterminspection, ZgsMidterminspection.class, "建设科技示范项目实施情况中期查验申请业务主表");
        return mv;
    }

    private String getId(ZgsMidterminspection item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsMidterminspection.class);
    }


    /**
     * 通过id查询导出word表
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询导出word表")
    @ApiOperation(value = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询导出word表", notes = "建设科技示范项目实施情况中期查验申请业务主表-通过id查询导出word表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsMidterminspection zgsMidterminspection = ValidateEncryptEntityUtil.validateDecryptObject(zgsMidterminspectionService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsMidterminspection != null) {
            initProjectSelectById(zgsMidterminspection);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "13甘肃省建设科技示范项目实施情况中期查验申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsMidterminspection);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

}
