package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsIndexFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 登录页文件下载表
 * @Author: jeecg-boot
 * @Date:   2022-03-05
 * @Version: V1.0
 */
public interface IZgsIndexFileService extends IService<ZgsIndexFile> {

}
