package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class GreenBuilding implements Serializable {
    private String name;
    private double value;
}
