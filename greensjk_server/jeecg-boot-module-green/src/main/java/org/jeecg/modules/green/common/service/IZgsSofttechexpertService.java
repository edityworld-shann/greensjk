package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 科技类审批记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
public interface IZgsSofttechexpertService extends IService<ZgsSofttechexpert> {
    List<ZgsSofttechexpert> getZgsSofttechexpertList(String businessguid);
}
