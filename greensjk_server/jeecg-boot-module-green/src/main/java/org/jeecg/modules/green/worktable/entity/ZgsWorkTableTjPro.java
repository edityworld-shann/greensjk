package org.jeecg.modules.green.worktable.entity;

import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/2217:06
 */
@Data
public class ZgsWorkTableTjPro {
    @Excel(name = "项目类型", width = 20)
    private String count0;
    @Excel(name = "申报数", width = 15)
    private Integer count1;
    @Excel(name = "通过数", width = 15)
    private Integer count2;
    @Excel(name = "不通过数", width = 15)
    private Integer count3;
    @Excel(name = "立项数", width = 15)
    private Integer count4;
    @Excel(name = "不立项数", width = 15)
    private Integer count5;
    @Excel(name = "财政拨款项目", width = 15)
    private Integer count6;
    private String menuUrl;
}
