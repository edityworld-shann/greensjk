package org.jeecg.modules.green.kyxmjtsq.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostexpendclearService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 科研结题经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Api(tags="科研结题经费决算表")
@RestController
@RequestMapping("/kyxmjtsq/zgsScientificpostexpendclear")
@Slf4j
public class ZgsScientificpostexpendclearController extends JeecgController<ZgsScientificpostexpendclear, IZgsScientificpostexpendclearService> {
	@Autowired
	private IZgsScientificpostexpendclearService zgsScientificpostexpendclearService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsScientificpostexpendclear
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-分页列表查询")
	@ApiOperation(value="科研结题经费决算表-分页列表查询", notes="科研结题经费决算表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsScientificpostexpendclear zgsScientificpostexpendclear,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsScientificpostexpendclear> queryWrapper = QueryGenerator.initQueryWrapper(zgsScientificpostexpendclear, req.getParameterMap());
		Page<ZgsScientificpostexpendclear> page = new Page<ZgsScientificpostexpendclear>(pageNo, pageSize);
		IPage<ZgsScientificpostexpendclear> pageList = zgsScientificpostexpendclearService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsScientificpostexpendclear
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-添加")
	@ApiOperation(value="科研结题经费决算表-添加", notes="科研结题经费决算表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsScientificpostexpendclear zgsScientificpostexpendclear) {
		zgsScientificpostexpendclearService.save(zgsScientificpostexpendclear);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsScientificpostexpendclear
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-编辑")
	@ApiOperation(value="科研结题经费决算表-编辑", notes="科研结题经费决算表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsScientificpostexpendclear zgsScientificpostexpendclear) {
		zgsScientificpostexpendclearService.updateById(zgsScientificpostexpendclear);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-通过id删除")
	@ApiOperation(value="科研结题经费决算表-通过id删除", notes="科研结题经费决算表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsScientificpostexpendclearService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-批量删除")
	@ApiOperation(value="科研结题经费决算表-批量删除", notes="科研结题经费决算表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsScientificpostexpendclearService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科研结题经费决算表-通过id查询")
	@ApiOperation(value="科研结题经费决算表-通过id查询", notes="科研结题经费决算表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsScientificpostexpendclear zgsScientificpostexpendclear = zgsScientificpostexpendclearService.getById(id);
		if(zgsScientificpostexpendclear==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsScientificpostexpendclear);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsScientificpostexpendclear
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsScientificpostexpendclear zgsScientificpostexpendclear) {
        return super.exportXls(request, zgsScientificpostexpendclear, ZgsScientificpostexpendclear.class, "科研结题经费决算表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsScientificpostexpendclear.class);
    }

}
