package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsThreepersonfinal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 三类人员实时信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface IZgsThreepersonfinalService extends IService<ZgsThreepersonfinal> {

}
