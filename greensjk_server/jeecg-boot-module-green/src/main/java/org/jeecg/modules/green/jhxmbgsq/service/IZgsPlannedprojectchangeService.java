package org.jeecg.modules.green.jhxmbgsq.service;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建设科技计划项目变更申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsPlannedprojectchangeService extends IService<ZgsPlannedprojectchange> {
    int spProject(ZgsPlannedprojectchange zgsPlannedprojectchange);

    int updateProjectIsDealOverdue(String projectlibraryguid, String enterpriseguid, String auditopinion);
}
