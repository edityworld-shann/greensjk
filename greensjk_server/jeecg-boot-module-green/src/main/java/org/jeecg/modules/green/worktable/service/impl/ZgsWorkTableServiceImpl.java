package org.jeecg.modules.green.worktable.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchDetailsPo;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchPo;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTable;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTableTjPro;
import org.jeecg.modules.green.worktable.mapper.ZgsWorkTableMapper;
import org.jeecg.modules.green.worktable.service.IZgsWorkTableService;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 工作台
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsWorkTableServiceImpl extends ServiceImpl<ZgsWorkTableMapper, ZgsWorkTable> implements IZgsWorkTableService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private RedisUtil redisUtil;

    //逾期定时
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsExamplecertificatebaseService zgsExamplecertificatebaseService;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;
    @Autowired
    private IZgsSpostcertificatebaseService zgsSpostcertificatebaseService;

    public Map<String, Object> getAllWorkTableMap() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = sysUser.getEnterpriseguid();
        Map<String, Object> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        int alarmStaus = 0;

        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -1);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper = new QueryWrapper();
        queryWrapper.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper.eq("enterpriseguid", enterpriseguid);
        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper);
        if (sciencetechfeasibleList.size() > 0) {
            alarmStaus += 1;
            list.add("<科研项目申报管理> 临近截止日期1个月，有预警提醒");
        }

        //2、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper2 = new QueryWrapper();
        queryWrapper2.between("completedate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper2.eq("enterpriseguid", enterpriseguid);
        List<ZgsProjectlibrary> zgsProjectlibraryList = zgsProjectlibraryService.list(queryWrapper2);
        if (zgsProjectlibraryList.size() > 0) {
            alarmStaus += 1;
            list.add("<示范项目申报管理> 临近截止日期1个月，有预警提醒");
        }

        //3.1、科技项目任务书
        QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
        queryWrapper3.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper3.eq("enterpriseguid", enterpriseguid);
        List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
        if (zgsSciencetechtaskList.size() > 0) {
            alarmStaus += 1;
            list.add("<科技项目任务书> 临近截止日期1个月，有预警提醒");
        }

        //3.2、示范项目任务书
        QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
        queryWrapper32.between("l.completedate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper32.eq("l.enterpriseguid", enterpriseguid);
        List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
        if (listInfoList.size() > 0) {
            alarmStaus += 1;
            list.add("<示范项目任务书> 临近截止日期1个月，有预警提醒");
        }

        //5、项目中期查验
        QueryWrapper<ZgsMidterminspection> queryWrapper5 = new QueryWrapper();
        queryWrapper5.between("projectenddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper5.eq("enterpriseguid", enterpriseguid);
        List<ZgsMidterminspection> zgsMidterminspectionList = zgsMidterminspectionService.list(queryWrapper5);
        if (zgsMidterminspectionList.size() > 0) {
            alarmStaus += 1;
            list.add("<项目中期查验> 临近截止日期1个月，有预警提醒");
        }

        //6、计划项目变更
        QueryWrapper<ZgsPlannedprojectchange> queryWrapper6 = new QueryWrapper();
        queryWrapper6.between("researchenddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper6.eq("enterpriseguid", enterpriseguid);
        List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper6);
        if (zgsPlannedprojectchangeList.size() > 0) {
            alarmStaus += 1;
            list.add("<计划项目变更> 临近截止日期1个月，有预警提醒");
        }

        //7.2、示范项目验收
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
        queryWrapper72.between("projectenddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper72.eq("enterpriseguid", enterpriseguid);
        List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper72);
        if (zgsDemoprojectacceptanceList.size() > 0) {
            alarmStaus += 1;
            list.add("<示范项目验收> 临近截止日期1个月，有预警提醒");
        }

        //7.1、科研项目验收
        QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
        queryWrapper7.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper7.eq("enterpriseguid", enterpriseguid);
        List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper7);
        if (zgsScientificbaseList.size() > 0) {
            alarmStaus += 1;
            list.add("<科研项目验收> 临近截止日期1个月，有预警提醒");
        }

        //4、科研项目结题
        QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
        queryWrapper4.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper4.eq("enterpriseguid", enterpriseguid);
        List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper4);
        if (zgsScientificpostbaseList.size() > 0) {
            alarmStaus += 1;
            list.add("<科研项目结题> 临近截止日期1个月，有预警提醒");
        }

        //8.2、示范项目验收证书
        QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
        queryWrapper82.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper82.eq("enterpriseguid", enterpriseguid);
        List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
        if (zgsExamplecertificatebaseList.size() > 0) {
            alarmStaus += 1;
            list.add("<示范项目验收证书> 临近截止日期1个月，有预警提醒");
        }

        //10、成果简介
        QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
        queryWrapper10.between("enddate", DateUtils.formatDate(calendar.getTime()), dateFormat.format(date));
        queryWrapper10.eq("enterpriseguid", enterpriseguid);
        List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
        if (zgsPlanresultbaseList.size() > 0) {
            alarmStaus += 1;
            list.add("<成果简介> 临近截止日期1个月，有预警提醒");
        }

        if (alarmStaus > 0) {
            map.put("alarmStaus", 1);
            map.put("alarmist", list);
        } else {
            map.put("alarmStaus", 0);
        }
        return map;
    }

    @Override
    public Map<String, Object> getPersonalWorkTableMap() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = sysUser.getEnterpriseguid();
        Map<String, Object> map = new HashMap<>();
        List<ZgsWorkTable> list = new ArrayList<>();
        List<Integer> commonList = this.baseMapper.getPersonalWorkTableCount(enterpriseguid);
        int total = 0;
        initWorkTableData(list, commonList, total);
        map.put("list", list);
        map.put("total", total);
        //任务书通过后登陆弹框，提示打印任务书并盖章
        QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("enterpriseguid", enterpriseguid);
        queryWrapper1.eq("status", GlobalConstants.SHENHE_STATUS8);
        List<ZgsProjecttask> list1 = zgsProjecttaskService.list(queryWrapper1);
        QueryWrapper<ZgsSciencetechtask> queryWrapper2 = new QueryWrapper();
        queryWrapper2.eq("enterpriseguid", enterpriseguid);
        queryWrapper2.eq("status", GlobalConstants.SHENHE_STATUS8);
        List<ZgsSciencetechtask> list2 = zgsSciencetechtaskService.list(queryWrapper2);
        if (list1.size() > 0 || list2.size() > 0) {
            map.put("taskwarn", 1);
        } else {
            map.put("taskwarn", null);
        }
        //当年结题证书终审的项目提醒：新增结题绩效自评
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int year = calendar.get(Calendar.YEAR);
        QueryWrapper<ZgsSpostcertificatebase> queryWrapper3 = new QueryWrapper();
        queryWrapper3.eq("m.enterpriseguid", enterpriseguid);
        queryWrapper3.eq("m.status", GlobalConstants.SHENHE_STATUS2);
        queryWrapper3.apply("date_format(m.finishdate,'%Y')={0}", year);
        List<ZgsSpostcertificatebase> listSPost = zgsSpostcertificatebaseService.listWarnByFundType(queryWrapper3);
        if (listSPost != null && listSPost.size() > 0) {
            map.put("tjjxfund", 1);
        } else {
            map.put("tjjxfund", 0);
        }
//        Map<String, Object> allWorkTableMap = this.getAllWorkTableMap();
//        map.put("alarmStaus", allWorkTableMap.get("alarmStaus"));
//        map.put("alarmist", allWorkTableMap.get("alarmist"));
        return map;
    }

    @Override
    public Map<String, Object> getUnitWorkTableMap() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> map = new HashMap<>();
        List<ZgsWorkTable> list0 = new ArrayList<>();
        List<ZgsWorkTable> list1 = new ArrayList<>();
        String unitmemberid = sysUser.getUnitmemberid();
        //测试地址
//        String unitmemberid = "004226b5-fa1f-4fd5-bfw5-acbfa5a2114";
        if (StringUtils.isNotEmpty(unitmemberid)) {
            ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
            String enterprisename = zgsProjectunitmember.getEnterprisename();
            QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
            wrapper.eq("higherupunit", enterprisename);
            List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
            QueryWrapper<ZgsWorkTable> queryWrapper0 = new QueryWrapper<>();
            QueryWrapper<ZgsWorkTable> queryWrapper0_1 = new QueryWrapper<>();
            StringBuilder stringBuilder0_1 = new StringBuilder();
            if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                queryWrapper0.ne("m.isdelete", 1);
                queryWrapper0.inSql("m.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
//                queryWrapper0.and(qwd -> {
//                            qwd.eq("m.status", GlobalConstants.SHENHE_STATUS1)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS5)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS9)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS14)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS15);
//                        }
//                );
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < zgsProjectunitmemberList.size(); i++) {
                    stringBuilder.append("'");
                    stringBuilder.append(zgsProjectunitmemberList.get(i).getEnterpriseguid());
                    stringBuilder.append("'");
                    if (i < zgsProjectunitmemberList.size() - 1) {
                        stringBuilder.append(",");
                    }
                }
                queryWrapper0.and(qwd -> {
                    qwd.inSql("m.enterpriseguid", stringBuilder.toString());
                    if (sysUser.getRealname().contains("城乡建设局")) {
                        qwd.or(q1 -> {
                            //行政区划等于且sfxmtype1项目库、sfxmstatus3已推荐
                            q1.eq("p.areacode", sysUser.getAreacode());
                            q1.and(q2 -> {
                                q2.eq("p.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_1).or().eq("p.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_3);
                            });
                        });
                    } else {
                        qwd.eq("p.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_2);
                    }
                });
                //
                queryWrapper0_1.ne("m.isdelete", 1);
//                queryWrapper0_1.and(qwd -> {
//                            qwd.eq("m.status", GlobalConstants.SHENHE_STATUS1)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS5)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS9)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS14)
//                                    .or().eq("m.status", GlobalConstants.SHENHE_STATUS15);
//                        }
//                );
                queryWrapper0_1.inSql("m.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                for (int i = 0; i < zgsProjectunitmemberList.size(); i++) {
                    stringBuilder0_1.append("'");
                    stringBuilder0_1.append(zgsProjectunitmemberList.get(i).getEnterpriseguid());
                    stringBuilder0_1.append("'");
                    if (i < zgsProjectunitmemberList.size() - 1) {
                        stringBuilder0_1.append(",");
                    }
                }
                if (stringBuilder0_1.length() > 0) {
                    queryWrapper0_1.inSql("m.enterpriseguid", stringBuilder0_1.toString());
                }
            } else {
                stringBuilder0_1.append("'");
                stringBuilder0_1.append(UUID.randomUUID().toString());
                stringBuilder0_1.append("'");
                queryWrapper0_1.inSql("m.enterpriseguid", stringBuilder0_1.toString());
            }
            QueryWrapper<ZgsWorkTable> queryWrapper1 = new QueryWrapper<>();
            if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                queryWrapper1.ne("m.isdelete", 1);
                queryWrapper1.ne("m.status", GlobalConstants.SHENHE_STATUS0)
                        .ne("m.status", GlobalConstants.SHENHE_STATUS3);
                queryWrapper1.inSql("m.enterpriseguid", stringBuilder0_1.toString());
//                queryWrapper1.and(qw -> {
//                    qw.and(w1 -> {
//                        for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
//                            w1.or(w2 -> {
//                                w2.eq("m.enterpriseguid", projectunitmember.getEnterpriseguid());
//                            });
//                        }
//                    });
//                });
            } else {
                queryWrapper1.eq("m.enterpriseguid", UUID.randomUUID().toString());
                queryWrapper0.eq("m.enterpriseguid", UUID.randomUUID().toString());
            }

            // List<Integer> commonList0 = getInitListTj(queryWrapper0, queryWrapper0_1);
            // for 推荐单位待初审项目
            List<Integer> commonList0 = new ArrayList<>();
            // 科技攻关
            commonList0.add(Integer.parseInt(redisUtil.get("kjggOfTjdw").toString()));

            // 软科学
            commonList0.add(Integer.parseInt(redisUtil.get("rkxOfTjdw").toString()));

            // 绿色建筑
            commonList0.add(Integer.parseInt(redisUtil.get("lsjzOfTjdw").toString()));

            // 建筑工程
            commonList0.add(Integer.parseInt(redisUtil.get("jzgcOfTjdw").toString()));

            // 建筑节能
            commonList0.add(Integer.parseInt(redisUtil.get("jzjnOfTjdw").toString()));

            // 装配式建筑
            commonList0.add(Integer.parseInt(redisUtil.get("zpsjzOfTjdw").toString()));

            // 科技项目任务书
            commonList0.add(Integer.parseInt(redisUtil.get("kjxmrwsOfTjdw").toString()));

            // 示范项目任务书
            commonList0.add(Integer.parseInt(redisUtil.get("sfxmrwsOfTjdw").toString()));

            // 科研项目验收
            commonList0.add(Integer.parseInt(redisUtil.get("kyxmysOfTjdw").toString()));

            // 示范项目验收
            commonList0.add(Integer.parseInt(redisUtil.get("sfxmysOfTjdw").toString()));

            // 科研项目结题
            commonList0.add(Integer.parseInt(redisUtil.get("kyxmjtOfTjdw").toString()));

            // 示范项目中期审查
            commonList0.add(Integer.parseInt(redisUtil.get("sfxmzqscOfTjdw").toString()));

            // 项目变更
            commonList0.add(Integer.parseInt(redisUtil.get("xmbgOfTjdw").toString()));

            // 项目终止
            commonList0.add(Integer.parseInt(redisUtil.get("xmzzOfTjdw").toString()));


            // 科研项目验收证书
            commonList0.add(Integer.parseInt(redisUtil.get("kyxmyszsOfTjdw").toString()));
            // 示范项目验收证书
            commonList0.add(Integer.parseInt(redisUtil.get("sfxmyszsOfTjdw").toString()));


            // 科研项目结题证书
            commonList0.add(Integer.parseInt(redisUtil.get("kyxmjtzsOfTjdw").toString()));

            // 项目成果简介
            commonList0.add(Integer.parseInt(redisUtil.get("xmcgjjOfTjdw").toString()));

            // 绿色建筑  暂不使用
            // commonList1.add(Integer.parseInt(redisUtil.get("lsjz").toString()));
            // 装配式建筑  暂不使用
            // commonList1.add(Integer.parseInt(redisUtil.get("zpsjz").toString()));

            // 年度绩效自评
            commonList0.add(Integer.parseInt(redisUtil.get("ndjxzpOfTjdw").toString()));



            // for 项目统计
            // List<Integer> commonList1 = this.baseMapper.getUnitWorkTableList1(queryWrapper1);  原有统计数量不正确  rxl 20230727
            List<Integer> commonList1 = new ArrayList<>();
            // 科技攻关
            commonList1.add(Integer.parseInt(redisUtil.get("kjgg").toString()));
            // 软科学
            commonList1.add(Integer.parseInt(redisUtil.get("rkx").toString()));
            // 绿色建筑
            commonList1.add(Integer.parseInt(redisUtil.get("lsjz").toString()));
            // 建筑工程
            commonList1.add(Integer.parseInt(redisUtil.get("jzgc").toString()));
            // 建筑节能
            commonList1.add(Integer.parseInt(redisUtil.get("jzjn").toString()));
            // 装配式建筑
            commonList1.add(Integer.parseInt(redisUtil.get("zpsjz").toString()));
            // 科技项目任务书
            commonList1.add(Integer.parseInt(redisUtil.get("kjxmrws").toString()));
            // 示范项目任务书
            commonList1.add(Integer.parseInt(redisUtil.get("sfxmrws").toString()));
            // 科研项目验收
            commonList1.add(Integer.parseInt(redisUtil.get("kyxmys").toString()));
            // 示范项目验收
            commonList1.add(Integer.parseInt(redisUtil.get("sfxmys").toString()));
            // 科研项目结题
            commonList1.add(Integer.parseInt(redisUtil.get("kyxmjt").toString()));
            // 示范项目中期审查
            commonList1.add(Integer.parseInt(redisUtil.get("sfxmzqsc").toString()));
            // 项目变更
            commonList1.add(Integer.parseInt(redisUtil.get("xmbg").toString()));
            // 项目终止
            commonList1.add(Integer.parseInt(redisUtil.get("xmzz").toString()));

            // 科研项目验收证书
            commonList1.add(Integer.parseInt(redisUtil.get("kyxmyszs").toString()));
            // 示范项目验收证书
            commonList1.add(Integer.parseInt(redisUtil.get("sfxmyszs").toString()));
            // 科研项目结题证书
            commonList1.add(Integer.parseInt(redisUtil.get("kyxmjtzs").toString()));
            // 项目成果简介
            commonList1.add(Integer.parseInt(redisUtil.get("xmcgjj").toString()));

            // 绿色建筑  暂不使用
            // commonList1.add(Integer.parseInt(redisUtil.get("lsjz").toString()));
            // 装配式建筑  暂不使用
            // commonList1.add(Integer.parseInt(redisUtil.get("zpsjz").toString()));

            // 年度绩效自评
            commonList1.add(Integer.parseInt(redisUtil.get("ndjxzp").toString()));


            int total = 0;
            initWorkTableData(list0, commonList0, total);
            initWorkTableData(list1, commonList1, total);
        }
        map.put("listSp", list0);
        map.put("listTj", list1);
        return map;
    }

    @Override
    public Map<String, Object> getGovWorkTableMap(Integer year) {
        Calendar cal = Calendar.getInstance();
        if (year == null) {
            year = cal.get(Calendar.YEAR);
        }
        List<Integer> headerList = this.baseMapper.getGovWorkTableHeaderList(year);
        Map<String, Object> map = new HashMap<>();
        List<ZgsWorkTable> list0 = new ArrayList<>();
        QueryWrapper<ZgsWorkTable> queryWrapperA = new QueryWrapper<>();
        queryWrapperA.ne("m.isdelete", 1);
        queryWrapperA.and(qw -> {
            qw.or(w1 -> {
                w1.isNull("m.agreeproject");
                w1.inSql("m.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
            });
            qw.or(w1 -> {
                w1.eq("m.status", GlobalConstants.SHENHE_STATUS8);
                w1.gt("m.year_num", 2022);
                w1.isNull("m.provincialfundtotal");
            });
        });
//        queryWrapperA.isNull("m.agreeproject");
//        queryWrapperA.inSql("m.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//        queryWrapperA.eq("year(m.createdate)", year);
        QueryWrapper<ZgsWorkTable> queryWrapperB = new QueryWrapper<>();
        queryWrapperB.ne("m.isdelete", 1);
        queryWrapperB.eq("m.status", GlobalConstants.SHENHE_STATUS4);
//        queryWrapperB.eq("year(m.createdate)", year);
        QueryWrapper<ZgsWorkTable> queryWrapperC = new QueryWrapper<>();
        queryWrapperC.ne("m.isdelete", 1);
        queryWrapperC.and(qw -> {
            qw.or(w1 -> {
                w1.isNull("m.agreeproject");
                w1.inSql("m.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
            });
            qw.or(w1 -> {
                w1.eq("m.status", GlobalConstants.SHENHE_STATUS8);
                w1.gt("p.year_num", 2022);
                w1.isNull("p.provincialfundtotal");
            });
        });
        // List<Integer> commonList0 = getInitListGov(queryWrapperA, queryWrapperB, queryWrapperC);  原有数据查询待审批项条件单一，查询存在问题  rxl 20230731
        List<Integer> commonList0 = new ArrayList<>();
        commonList0.add(Integer.parseInt(redisUtil.get("kjggOfDsp").toString()));  //科技攻关待审批
        commonList0.add(Integer.parseInt(redisUtil.get("rkxOfDsp").toString()));   // 软科学待审批

        commonList0.add(Integer.parseInt(redisUtil.get("lsjzOfDsp").toString()));   // 绿色建筑待审批
        commonList0.add(Integer.parseInt(redisUtil.get("jzgcOfDsp").toString()));   // 建筑工程待审批
        commonList0.add(Integer.parseInt(redisUtil.get("jzjnOfDsp").toString()));   // 建筑节能待审批
        commonList0.add(Integer.parseInt(redisUtil.get("zpsjzOfDsp").toString()));   // 装配式建筑待审批

        commonList0.add(Integer.parseInt(redisUtil.get("kjxmrwsOfDsp").toString()));   // 科技项目任务书待审批
        commonList0.add(Integer.parseInt(redisUtil.get("sfxmrwsOfDsp").toString()));   // 示范项目任务书待审批

        commonList0.add(Integer.parseInt(redisUtil.get("kyxmysOfDsp").toString()));   // 科研项目验收待审批
        commonList0.add(Integer.parseInt(redisUtil.get("sfxmysOfDsp").toString()));   // 示范项目验收待审批

        commonList0.add(Integer.parseInt(redisUtil.get("kyxmjtOfDsp").toString()));   // 科研项目结题待审批
        commonList0.add(Integer.parseInt(redisUtil.get("sfxmzqscOfDsp").toString()));   // 示范项目中期审查待审批

        commonList0.add(Integer.parseInt(redisUtil.get("xmbgOfDsp").toString()));   // 项目变更待审批
        commonList0.add(Integer.parseInt(redisUtil.get("xmzzOfDsp").toString()));   // 项目终止待审批

        commonList0.add(Integer.parseInt(redisUtil.get("kyxmyszsOfDsp").toString()));   // 科研项目验收证书待审批
        commonList0.add(Integer.parseInt(redisUtil.get("sfxmyszsOfDsp").toString()));   // 示范项目验收证书待审批

        commonList0.add(Integer.parseInt(redisUtil.get("kyxmjtzsOfDsp").toString()));   // 科研项目结题证书待审批

        commonList0.add(Integer.parseInt(redisUtil.get("xmcgjjOfDsp").toString()));   // 项目成果简介待审批

        commonList0.add(Integer.parseInt(redisUtil.get("ndjxzpOfDsp").toString()));   // 年度绩效自评待审批


        int total = 0;
        initWorkTableData(list0, commonList0, total);
        List<ZgsWorkTableTjPro> commonList1 = getGovWorkTableCenterList(year);
        map.put("projectNum", headerList.get(0));
        map.put("fundNum", headerList.get(1));
        map.put("dealNum", headerList.get(2));
        map.put("noPassNum", headerList.get(3));
        map.put("warnNum", headerList.get(4));
        map.put("listSp", list0);
        map.put("listTj", commonList1);
        return map;
    }

    private List<Integer> getInitListGov(QueryWrapper<ZgsWorkTable> qwd0, QueryWrapper<ZgsWorkTable> qwd1, QueryWrapper<ZgsWorkTable> qwd2) {
        List<Integer> list = this.baseMapper.getUnitWorkTableListZero(qwd0);
        List<Integer> list0 = this.baseMapper.getUnitWorkTableListZeroA(qwd2); // rxl 20230506  修改我的工作台_示范项目申报管理_建筑工程示示范  总数去掉申报阶段不立项 项目(只统计申报阶段待审核项目数量)
        List<Integer> list1 = this.baseMapper.getUnitWorkTableListZeroB(qwd1);
        List<Integer> list2 = this.baseMapper.getUnitWorkTableListZeroC(qwd1);
        List<Integer> list3 = this.baseMapper.getUnitWorkTableListZeroD(qwd1);
        List<Integer> listAll = new ArrayList<>();
        listAll.addAll(list);//科研项目申报管理
        listAll.addAll(list0);//示范项目申报管理
        listAll.addAll(list1);
        listAll.addAll(list2);
        listAll.addAll(list3);//绿色发展示范项目
        return listAll;
    }

    private List<Integer> getInitListTj(QueryWrapper<ZgsWorkTable> qwd0, QueryWrapper<ZgsWorkTable> qwd1) {
        List<Integer> list = this.baseMapper.getUnitWorkTableListZero(qwd1);
        List<Integer> list0 = this.baseMapper.getUnitWorkTableListZeroA(qwd0);
        List<Integer> list1 = this.baseMapper.getUnitWorkTableListZeroB(qwd1);
        List<Integer> list2 = this.baseMapper.getUnitWorkTableListZeroC(qwd0);
        List<Integer> list3 = this.baseMapper.getUnitWorkTableListZeroD(qwd1);
        List<Integer> listAll = new ArrayList<>();
        listAll.addAll(list);
        listAll.addAll(list0);
        listAll.addAll(list1);
        listAll.addAll(list2);
        listAll.addAll(list3);
        return listAll;
    }

    @Override
    public List<ZgsWorkTableTjPro> getGovWorkTableCenterList(Integer year) {
        Calendar cal = Calendar.getInstance();
        if (year == null) {
            year = cal.get(Calendar.YEAR);
        }
        return this.baseMapper.getGovWorkTableCenterList(year);
    }

    //TODO hzy
    @Override
    public CityAndStateWorkbenchPo energySavingGreenBuildingProjectData(String year, String areacode) {
        return this.baseMapper.energySavingGreenBuildingProjectData(year, areacode);
    }

    @Override
    public List<ZgsWorkTable> listOfDistrictsAndCounties(String year, String areacode) {
        List<ZgsWorkTable> list = new ArrayList<>();
        CityAndStateWorkbenchDetailsPo cityAndStateWorkbenchDetailsPo = this.baseMapper.energyNewinfo(year, areacode);
        if (cityAndStateWorkbenchDetailsPo != null) {
            ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setCount(Integer.valueOf(cityAndStateWorkbenchDetailsPo.getMonthNumber()));
            zgsWorkTable.setMenuName(cityAndStateWorkbenchDetailsPo.getAreaname());
            zgsWorkTable.setMenuUrl("/green/report/ZgsReportEnergyNewinfoList");
            zgsWorkTable.setMenuNameParent("建筑项目清单");
            list.add(zgsWorkTable);
        }

        CityAndStateWorkbenchDetailsPo cityAndStateWorkbenchDetailsPo1 = this.baseMapper.energyExistinfo(year, areacode);
        if (cityAndStateWorkbenchDetailsPo1 != null) {
            ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setCount(Integer.valueOf(cityAndStateWorkbenchDetailsPo1.getMonthNumber()));
            zgsWorkTable.setMenuName(cityAndStateWorkbenchDetailsPo1.getAreaname());
            zgsWorkTable.setMenuUrl("/green/report/ZgsReportEnergyExistinfoList");
            zgsWorkTable.setMenuNameParent("改造项目清单");
            list.add(zgsWorkTable);
        }
        CityAndStateWorkbenchDetailsPo cityAndStateWorkbenchDetailsPo2 = this.baseMapper.newinfoGreentotal(year, areacode);
        if (cityAndStateWorkbenchDetailsPo2 != null) {
            ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setCount(Integer.valueOf(cityAndStateWorkbenchDetailsPo2.getMonthNumber()));
            zgsWorkTable.setMenuName(cityAndStateWorkbenchDetailsPo2.getAreaname());
            zgsWorkTable.setMenuUrl("/green/report/ZgsReportEnergyNewinfoGreenList");
            zgsWorkTable.setMenuNameParent("绿色建筑项目清单");
            list.add(zgsWorkTable);
        }
        CityAndStateWorkbenchDetailsPo completed = this.baseMapper.completed(year, areacode);
        if (completed != null) {
            ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setCount(Integer.valueOf(completed.getMonthNumber()));
            zgsWorkTable.setMenuName(completed.getAreaname());
            zgsWorkTable.setMenuUrl("/green/report/ZgsReportMonthfabrAreaCityProjectList");
            zgsWorkTable.setMenuNameParent("装配式项目清单");
            list.add(zgsWorkTable);
        }
        CityAndStateWorkbenchDetailsPo productioncapacity = this.baseMapper.productioncapacity(year, areacode);
        if (productioncapacity != null) {
            ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setCount(Integer.valueOf(productioncapacity.getMonthNumber()));
            zgsWorkTable.setMenuName(productioncapacity.getAreaname());
            zgsWorkTable.setMenuUrl("/green/report/ZgsReportMonthfabrAreaCityCompanyList");
            zgsWorkTable.setMenuNameParent("装配式生产产能清单");
            list.add(zgsWorkTable);
        }
        return list;
    }

    @Override
    public Map<String, Object> getExpertWorkTableMap() {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> map = new HashMap<>();
        QueryWrapper<ZgsWorkTable> queryWrapper = new QueryWrapper<>();
        QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
        zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
        ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
        int total = 0;
        if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
            queryWrapper.eq("expertguid", zgsExpertinfo.getId());
            queryWrapper.ne("isdelete", 1);
            queryWrapper.isNull("auditconclusionnum");
            total = this.baseMapper.getExpertWorkTableLeftList(queryWrapper);
        }
        List<Integer> headerList = this.baseMapper.getGovWorkTableHeaderList(null);
        Integer year[] = {2011, 2013, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022};
        String city[] = {"兰州市", "嘉峪关市", "金昌市", "白银市", "天水市", "武威市", "张掖市", "平凉市", "酒泉市", "庆阳市", "定西市", "陇南市", "矿区",
                "临夏回族自治州", "甘南藏族自治州"};
        List<List<Integer>> cityValueList = new ArrayList<>();
        for (int j = 0; j < 15; j++) {
            List<Integer> list = new ArrayList<>();
            Random rand = new Random();
            for (int i = 0; i < 15; i++) {
                list.add(rand.nextInt(100) + 1);
            }
            cityValueList.add(list);
        }
        map.put("total", total);
        map.put("titleName", "甘肃省市州工程项目统计情况（数量：" + headerList.get(0) + "个，造价" + headerList.get(1) / 1000 + "亿）");
        map.put("yearList", year);
        map.put("cityList", city);
        map.put("cityValueList", cityValueList);
        return map;
    }

    private void initWorkTableData(List<ZgsWorkTable> list, List<Integer> commonList, int total) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
        //1、科研项目管理
        //1.1科技攻关项目
        int count_1_1 = commonList.get(0);
        if (count_1_1 > 0) {
            zgsWorkTable.setMenuNameParent("科研项目申报管理");
            zgsWorkTable.setMenuName("科技攻关项目");
            zgsWorkTable.setCount(count_1_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechfeasibleList1");
            list.add(zgsWorkTable);
        }
        //1.2软科学项目
        int count_1_2 = commonList.get(1);
        if (count_1_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目申报管理");
            zgsWorkTable.setMenuName("软科学项目");
            zgsWorkTable.setCount(count_1_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechfeasibleList2");
            list.add(zgsWorkTable);
        }
        //2、示范项目管理
        //2.1绿色建筑示范
        int count_2_1 = commonList.get(2);
        if (count_2_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("绿色建筑示范");
            zgsWorkTable.setCount(count_2_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList1");
            list.add(zgsWorkTable);
        }
        //2.2建筑工程示范
        int count_2_2 = commonList.get(3);
        if (count_2_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("建筑工程示范");
            zgsWorkTable.setCount(count_2_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList2");
            list.add(zgsWorkTable);
        }
        //2.3市政工程示范
        /*int count_2_3 = commonList.get(4);
        if (count_2_3 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("市政工程示范");
            zgsWorkTable.setCount(count_2_3);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList3");
            list.add(zgsWorkTable);
        }*/
        //2.4建筑节能示范
        int count_2_4 = commonList.get(4);
        if (count_2_4 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("建筑节能示范");
            zgsWorkTable.setCount(count_2_4);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList4");
            list.add(zgsWorkTable);
        }
        //2.5装配式建筑示范
        int count_2_5 = commonList.get(5);
        if (count_2_5 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("装配式建筑示范");
            zgsWorkTable.setCount(count_2_5);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList5");
            list.add(zgsWorkTable);
        }
        //3、任务书管理
        //3.1科技项目任务书
        int count_3_1 = commonList.get(6);
        if (count_3_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("任务书管理");
            zgsWorkTable.setMenuName("科技项目任务书");
            zgsWorkTable.setCount(count_3_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechtaskList");
            list.add(zgsWorkTable);
        }
        //3.2示范项目任务书
        int count_3_2 = commonList.get(7);
        if (count_3_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("任务书管理");
            zgsWorkTable.setMenuName("示范项目任务书");
            zgsWorkTable.setCount(count_3_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjecttaskList");
            list.add(zgsWorkTable);
        }
        //4、项目验收管理
        //4.1科研项目验收
        int count_4_1 = commonList.get(8);
        if (count_4_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收管理");
            zgsWorkTable.setMenuName("科研项目验收");
            zgsWorkTable.setCount(count_4_1);
            zgsWorkTable.setMenuUrl("/green/xmyssq/ZgsScientificbaseList1");
            list.add(zgsWorkTable);
        }
        //4.2示范项目验收
        int count_4_2 = commonList.get(9);
        if (count_4_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收管理");
            zgsWorkTable.setMenuName("示范项目验收");
            zgsWorkTable.setCount(count_4_2);
            zgsWorkTable.setMenuUrl("/green/xmyssq/ZgsDemoprojectacceptanceList1");
            list.add(zgsWorkTable);
        }
        //5、科研项目结题
        //5.1科研项目结题
        int count_5_1 = commonList.get(10);
        if (count_5_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目结题");
            zgsWorkTable.setMenuName("科研项目结题");
            zgsWorkTable.setCount(count_5_1);
            zgsWorkTable.setMenuUrl("/green/kyxmjtsq/ZgsScientificpostbaseList");
            list.add(zgsWorkTable);
        }
        //6、中期查验
        //6.1示范项目中期查验
        int count_6_1 = commonList.get(11);
        if (count_6_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("中期查验");
            zgsWorkTable.setMenuName("示范项目中期审查");
            zgsWorkTable.setCount(count_6_1);
            zgsWorkTable.setMenuUrl("/green/zqcysb/ZgsMidterminspectionList");
            list.add(zgsWorkTable);
        }
        //7、计划项目变更
        //7.1项目变更
        int count_7_1 = commonList.get(12);
        if (count_7_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("计划项目变更");
            zgsWorkTable.setMenuName("项目变更");
            zgsWorkTable.setCount(count_7_1);
            zgsWorkTable.setMenuUrl("/green/jhxmbgsq/ZgsPlannedprojectchangeList2");
            list.add(zgsWorkTable);
        }
        //7.2项目终止
        int count_7_2 = commonList.get(13);
        if (count_7_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("计划项目变更");
            zgsWorkTable.setMenuName("项目终止");
            zgsWorkTable.setCount(count_7_2);
            zgsWorkTable.setMenuUrl("/green/jhxmbgsq/ZgsPlannedprojectchangeList1");
            list.add(zgsWorkTable);
        }


        //8、项目验收证书
        //8.1科研项目验收证书
        int count_8_1 = commonList.get(14);
        if (count_8_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收证书");
            zgsWorkTable.setMenuName("科研项目验收证书");
            zgsWorkTable.setCount(count_8_1);
            zgsWorkTable.setMenuUrl("/green/xmyszssq/ZgsSacceptcertificatebaseList");
            list.add(zgsWorkTable);
        }
        //8.2示范项目验收证书
        int count_8_2 = commonList.get(15);
        if (count_8_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收证书");
            zgsWorkTable.setMenuName("示范项目验收证书");
            zgsWorkTable.setCount(count_8_2);
            zgsWorkTable.setMenuUrl("/green/xmyszssq/ZgsExamplecertificatebaseList");
            list.add(zgsWorkTable);
        }
        //9、科研项目结题证书
        //9.1科研项目结题证书
        int count_9_1 = commonList.get(16);
        if (count_9_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目结题证书");
            zgsWorkTable.setMenuName("科研项目结题证书");
            zgsWorkTable.setCount(count_9_1);
            zgsWorkTable.setMenuUrl("/green/kyxmjtzs/ZgsSpostcertificatebaseList");
            list.add(zgsWorkTable);
        }
        //10、计划项目成果简介
        //10.1计划项目成果简介
        int count_10_1 = commonList.get(17);
        if (count_10_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目成果简介");
            zgsWorkTable.setMenuName("项目成果简介");
            zgsWorkTable.setCount(count_10_1);
            zgsWorkTable.setMenuUrl("/green/jhxmcgjj/ZgsPlanresultbaseList");
            list.add(zgsWorkTable);
        }
        //11、绩效自评
        //11.1绩效自评
        /*int count_11_1 = commonList.get(18);
        if (count_11_1 > 0 && sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绩效自评");
            zgsWorkTable.setMenuName("结题验收绩效自评");
            zgsWorkTable.setCount(count_11_1);
            zgsWorkTable.setMenuUrl("/green/jxzpsq/ZgsPerformancebaseList");
            list.add(zgsWorkTable);
        }*/
        //7.2项目终止
        /*int count_7_2 = commonList.get(19);
        if (count_7_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("计划项目变更");
            zgsWorkTable.setMenuName("项目终止");
            zgsWorkTable.setCount(count_7_2);
            zgsWorkTable.setMenuUrl("/green/jhxmbgsq/ZgsPlannedprojectchangeList1");
            list.add(zgsWorkTable);
        }*/
        //12、示范项目库
        //12.1绿色建筑
        /*int count_12_1 = commonList.get(20);
        if (count_12_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绿色发展示范项目");
            zgsWorkTable.setMenuName("绿色建筑");
            zgsWorkTable.setCount(count_12_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryListKu1");
            list.add(zgsWorkTable);
        }*/
        //12.2装配式建筑
        /*int count_12_2 = commonList.get(21);
        if (count_12_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绿色发展示范项目");
            zgsWorkTable.setMenuName("装配式建筑");
            zgsWorkTable.setCount(count_12_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryListKu5");
            list.add(zgsWorkTable);
        }*/
        //13.1新注册审批
        /*int count_13_1 = commonList.get(22);
        if (count_13_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("企业及个人黑名单");
            zgsWorkTable.setMenuName("名单管理");
            zgsWorkTable.setCount(count_13_1);
            zgsWorkTable.setMenuUrl("/user/UserWhiteBlackList");
            list.add(zgsWorkTable);
        }*/
        //11.2绩效自评
        int count_11_2 = commonList.get(18);
        if (count_11_2 > 0 && sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绩效自评");
            zgsWorkTable.setMenuName("年度绩效自评");
            zgsWorkTable.setCount(count_11_2);
            zgsWorkTable.setMenuUrl("/green/jxzpsq/ZgsPerformancebaseList1");
            list.add(zgsWorkTable);
        }
        /*total = count_1_1 + count_1_2 + count_2_1 + count_2_2 + count_2_3 + count_2_4 + count_2_5
                + count_3_1 + count_3_2 + count_4_1 + count_4_2 + count_5_1
                + count_6_1 + count_7_1 + count_7_2 + count_8_1 + count_8_2 + count_9_1 + count_10_1 + count_11_1 + count_11_2 + count_12_1 + count_12_2 + count_13_1;*/
        total = count_1_1 + count_1_2 + count_2_1 + count_2_2  + count_2_4 + count_2_5
                + count_3_1 + count_3_2 + count_4_1 + count_4_2 + count_5_1
                + count_6_1 + count_7_1 + count_7_2 + count_8_1 + count_8_2 + count_9_1 + count_10_1 + count_11_2;
    }


    /**
     * @describe: 因推荐单位查看工作台的项目统计详情时，调用原始接口改动量大，估计单独追加 查看项目详情信息接口
     * @author: renxiaoliang
     * @date: 2023/7/27 14:45
     */
    private void initWorkTableForRecommendedUnit(List<ZgsWorkTable> list, List<Integer> commonList, int total) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsWorkTable zgsWorkTable = new ZgsWorkTable();
        //1、科研项目管理
        //1.1科技攻关项目
        int count_1_1 = commonList.get(0);
        if (count_1_1 > 0) {
            zgsWorkTable.setMenuNameParent("科研项目申报管理");
            zgsWorkTable.setMenuName("科技攻关项目");
            zgsWorkTable.setCount(count_1_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechfeasibleList1");
            list.add(zgsWorkTable);
        }
        //1.2软科学项目
        int count_1_2 = commonList.get(1);
        if (count_1_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目申报管理");
            zgsWorkTable.setMenuName("软科学项目");
            zgsWorkTable.setCount(count_1_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechfeasibleList2");
            list.add(zgsWorkTable);
        }
        //2、示范项目管理
        //2.1绿色建筑示范
        int count_2_1 = commonList.get(2);
        if (count_2_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("绿色建筑示范");
            zgsWorkTable.setCount(count_2_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList1");
            list.add(zgsWorkTable);
        }
        //2.2建筑工程示范
        int count_2_2 = commonList.get(3);
        if (count_2_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("建筑工程示范");
            zgsWorkTable.setCount(count_2_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList2");
            list.add(zgsWorkTable);
        }
        //2.3市政工程示范
        int count_2_3 = commonList.get(4);
        if (count_2_3 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("市政工程示范");
            zgsWorkTable.setCount(count_2_3);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList3");
            list.add(zgsWorkTable);
        }
        //2.4建筑节能示范
        int count_2_4 = commonList.get(5);
        if (count_2_4 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("建筑节能示范");
            zgsWorkTable.setCount(count_2_4);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList4");
            list.add(zgsWorkTable);
        }
        //2.5装配式建筑示范
        int count_2_5 = commonList.get(6);
        if (count_2_5 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("示范项目申报管理");
            zgsWorkTable.setMenuName("装配式建筑示范");
            zgsWorkTable.setCount(count_2_5);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryList5");
            list.add(zgsWorkTable);
        }
        //3、任务书管理
        //3.1科技项目任务书
        int count_3_1 = commonList.get(7);
        if (count_3_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("任务书管理");
            zgsWorkTable.setMenuName("科技项目任务书");
            zgsWorkTable.setCount(count_3_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsSciencetechtaskList");
            list.add(zgsWorkTable);
        }
        //3.2示范项目任务书
        int count_3_2 = commonList.get(8);
        if (count_3_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("任务书管理");
            zgsWorkTable.setMenuName("示范项目任务书");
            zgsWorkTable.setCount(count_3_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjecttaskList");
            list.add(zgsWorkTable);
        }
        //4、项目验收管理
        //4.1科研项目验收
        int count_4_1 = commonList.get(9);
        if (count_4_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收管理");
            zgsWorkTable.setMenuName("科研项目验收");
            zgsWorkTable.setCount(count_4_1);
            zgsWorkTable.setMenuUrl("/green/xmyssq/ZgsScientificbaseList1");
            list.add(zgsWorkTable);
        }
        //4.2示范项目验收
        int count_4_2 = commonList.get(10);
        if (count_4_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收管理");
            zgsWorkTable.setMenuName("示范项目验收");
            zgsWorkTable.setCount(count_4_2);
            zgsWorkTable.setMenuUrl("/green/xmyssq/ZgsDemoprojectacceptanceList1");
            list.add(zgsWorkTable);
        }
        //5、科研项目结题
        //5.1科研项目结题
        int count_5_1 = commonList.get(11);
        if (count_5_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目结题");
            zgsWorkTable.setMenuName("科研项目结题");
            zgsWorkTable.setCount(count_5_1);
            zgsWorkTable.setMenuUrl("/green/kyxmjtsq/ZgsScientificpostbaseList");
            list.add(zgsWorkTable);
        }
        //6、中期查验
        //6.1示范项目中期查验
        int count_6_1 = commonList.get(12);
        if (count_6_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("中期查验");
            zgsWorkTable.setMenuName("示范项目中期审查");
            zgsWorkTable.setCount(count_6_1);
            zgsWorkTable.setMenuUrl("/green/zqcysb/ZgsMidterminspectionList");
            list.add(zgsWorkTable);
        }
        //7、计划项目变更
        //7.1项目变更
        int count_7_1 = commonList.get(13);
        if (count_7_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("计划项目变更");
            zgsWorkTable.setMenuName("项目变更");
            zgsWorkTable.setCount(count_7_1);
            zgsWorkTable.setMenuUrl("/green/jhxmbgsq/ZgsPlannedprojectchangeList2");
            list.add(zgsWorkTable);
        }
        //8、项目验收证书
        //8.1科研项目验收证书
        int count_8_1 = commonList.get(14);
        if (count_8_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收证书");
            zgsWorkTable.setMenuName("科研项目验收证书");
            zgsWorkTable.setCount(count_8_1);
            zgsWorkTable.setMenuUrl("/green/xmyszssq/ZgsSacceptcertificatebaseList");
            list.add(zgsWorkTable);
        }
        //8.2示范项目验收证书
        int count_8_2 = commonList.get(15);
        if (count_8_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目验收证书");
            zgsWorkTable.setMenuName("示范项目验收证书");
            zgsWorkTable.setCount(count_8_2);
            zgsWorkTable.setMenuUrl("/green/xmyszssq/ZgsExamplecertificatebaseList");
            list.add(zgsWorkTable);
        }
        //9、科研项目结题证书
        //9.1科研项目结题证书
        int count_9_1 = commonList.get(16);
        if (count_9_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("科研项目结题证书");
            zgsWorkTable.setMenuName("科研项目结题证书");
            zgsWorkTable.setCount(count_9_1);
            zgsWorkTable.setMenuUrl("/green/kyxmjtzs/ZgsSpostcertificatebaseList");
            list.add(zgsWorkTable);
        }
        //10、计划项目成果简介
        //10.1计划项目成果简介
        int count_10_1 = commonList.get(17);
        if (count_10_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("项目成果简介");
            zgsWorkTable.setMenuName("项目成果简介");
            zgsWorkTable.setCount(count_10_1);
            zgsWorkTable.setMenuUrl("/green/jhxmcgjj/ZgsPlanresultbaseList");
            list.add(zgsWorkTable);
        }
        //11、绩效自评
        //11.1绩效自评
        int count_11_1 = commonList.get(18);
        if (count_11_1 > 0 && sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绩效自评");
            zgsWorkTable.setMenuName("结题验收绩效自评");
            zgsWorkTable.setCount(count_11_1);
            zgsWorkTable.setMenuUrl("/green/jxzpsq/ZgsPerformancebaseList");
            list.add(zgsWorkTable);
        }
        //7.2项目终止
        int count_7_2 = commonList.get(19);
        if (count_7_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("计划项目变更");
            zgsWorkTable.setMenuName("项目终止");
            zgsWorkTable.setCount(count_7_2);
            zgsWorkTable.setMenuUrl("/green/jhxmbgsq/ZgsPlannedprojectchangeList1");
            list.add(zgsWorkTable);
        }
        //12、示范项目库
        //12.1绿色建筑
        int count_12_1 = commonList.get(20);
        if (count_12_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绿色发展示范项目");
            zgsWorkTable.setMenuName("绿色建筑");
            zgsWorkTable.setCount(count_12_1);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryListKu1");
            list.add(zgsWorkTable);
        }
        //12.2装配式建筑
        int count_12_2 = commonList.get(21);
        if (count_12_2 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绿色发展示范项目");
            zgsWorkTable.setMenuName("装配式建筑");
            zgsWorkTable.setCount(count_12_2);
            zgsWorkTable.setMenuUrl("/green/common/ZgsProjectlibraryListKu5");
            list.add(zgsWorkTable);
        }
        //13.1新注册审批
        int count_13_1 = commonList.get(22);
        if (count_13_1 > 0) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("企业及个人黑名单");
            zgsWorkTable.setMenuName("名单管理");
            zgsWorkTable.setCount(count_13_1);
            zgsWorkTable.setMenuUrl("/user/UserWhiteBlackList");
            list.add(zgsWorkTable);
        }
        //11.2绩效自评
        int count_11_2 = commonList.get(23);
        if (count_11_2 > 0 && sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            zgsWorkTable = new ZgsWorkTable();
            zgsWorkTable.setMenuNameParent("绩效自评");
            zgsWorkTable.setMenuName("年度绩效自评");
            zgsWorkTable.setCount(count_11_2);
            zgsWorkTable.setMenuUrl("/green/jxzpsq/ZgsPerformancebaseList1");
            list.add(zgsWorkTable);
        }
        total = count_1_1 + count_1_2 + count_2_1 + count_2_2 + count_2_3 + count_2_4 + count_2_5
                + count_3_1 + count_3_2 + count_4_1 + count_4_2 + count_5_1
                + count_6_1 + count_7_1 + count_7_2 + count_8_1 + count_8_2 + count_9_1 + count_10_1 + count_11_1 + count_11_2 + count_12_1 + count_12_2 + count_13_1;
    }
}
