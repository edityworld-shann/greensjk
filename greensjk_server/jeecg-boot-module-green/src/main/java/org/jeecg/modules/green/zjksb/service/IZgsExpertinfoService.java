package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库表
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertinfoService extends IService<ZgsExpertinfo> {

    int spProject(ZgsExpertinfo zgsExpertinfo);
}
