package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 三类人员类别实时表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
@Data
@TableName("zgs_threepersoncategoryfinal")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="zgs_threepersoncategoryfinal对象", description="三类人员类别实时表")
public class ZgsThreepersoncategoryfinal implements Serializable {
    private static final long serialVersionUID = 1L;

	/**唯一编号*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "唯一编号")
    private java.lang.String id;
	/**实时库表编号,与ThreePersonFinal.GUID关联*/
	@Excel(name = "实时库表编号,与ThreePersonFinal.GUID关联", width = 15)
    @ApiModelProperty(value = "实时库表编号,与ThreePersonFinal.GUID关联")
    private java.lang.String finalguid;
	/**人员类别编码,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员*/
	@Excel(name = "人员类别编码,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员", width = 15)
    @ApiModelProperty(value = "人员类别编码,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员")
    private java.lang.String personcategorynum;
	/**人员类别名称,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员*/
	@Excel(name = "人员类别名称,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员", width = 15)
    @ApiModelProperty(value = "人员类别名称,A证：企业主要负责人,B证：项目负责人,C证：专职安全生产管理人员")
    private java.lang.String personcategoryname;
	/**证书编号*/
	@Excel(name = "证书编号", width = 15)
    @ApiModelProperty(value = "证书编号")
    private java.lang.String certificatenumber;
	/**发证时间*/
	@Excel(name = "发证时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "发证时间")
    private java.util.Date issuedate;
	/**有效期止,自动生成：发证时间+3年*/
	@Excel(name = "有效期止,自动生成：发证时间+3年", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "有效期止,自动生成：发证时间+3年")
    private java.util.Date validenddate;
	/**职称*/
	@Excel(name = "职称", width = 15)
    @ApiModelProperty(value = "职称")
    private java.lang.String technical;
	/**职务*/
	@Excel(name = "职务", width = 15)
    @ApiModelProperty(value = "职务")
    private java.lang.String duty;
	/**注册状态,有效、注销*/
	@Excel(name = "注册状态,有效、注销", width = 15)
    @ApiModelProperty(value = "注册状态,有效、注销")
    private java.lang.String registerstate;
	/**创建人帐号*/
	@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    private java.lang.String createpersonaccount;
	/**创建人姓名*/
	@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    private java.lang.String createpersonname;
	/**创建时间*/
	@Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createtime;
	/**修改人帐号*/
	@Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    private java.lang.String modifypersonaccount;
	/**修改人姓名*/
	@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    private java.lang.String modifypersonname;
	/**修改时间*/
	@Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifytime;
	/**是否删除,0或null:未删除；1:已删除*/
	@Excel(name = "是否删除,0或null:未删除；1:已删除", width = 15)
    @ApiModelProperty(value = "是否删除,0或null:未删除；1:已删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
	/**项目经理级别,一级,二级,临时一级,临时二级*/
	@Excel(name = "项目经理级别,一级,二级,临时一级,临时二级", width = 15)
    @ApiModelProperty(value = "项目经理级别,一级,二级,临时一级,临时二级")
    private java.lang.String grade;
	/**注销时间*/
	@Excel(name = "注销时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "注销时间")
    private java.util.Date logoutdate;
	/**删除说明*/
	@Excel(name = "删除说明", width = 15)
    @ApiModelProperty(value = "删除说明")
    private java.lang.String delreason;
	/**1:导入;0:业务修改过；2:实时库管理修改;*/
	@Excel(name = "1:导入;0:业务修改过；2:实时库管理修改;", width = 15)
    @ApiModelProperty(value = "1:导入;0:业务修改过；2:实时库管理修改;")
    private java.math.BigDecimal importstate;
	/**打印次数*/
	@Excel(name = "打印次数", width = 15)
    @ApiModelProperty(value = "打印次数")
    private java.math.BigDecimal printcertcount;
	/**最后一次打印日期*/
	@Excel(name = "最后一次打印日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "最后一次打印日期")
    private java.util.Date printcerdate;
	/**撤消时间,2018.2.28 ljx-add*/
	@Excel(name = "撤消时间,2018.2.28 ljx-add", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "撤消时间,2018.2.28 ljx-add")
    private java.util.Date canceldate;
	/**自动注销的提示信息;2018.4.9 ljx-add改为以证注销了*/
	@Excel(name = "自动注销的提示信息;2018.4.9 ljx-add改为以证注销了", width = 15)
    @ApiModelProperty(value = "自动注销的提示信息;2018.4.9 ljx-add改为以证注销了")
    private java.lang.String autoprompt;
	/**1:已推送；2：符合推送；2018.6.11 add*/
	@Excel(name = "1:已推送；2：符合推送；2018.6.11 add", width = 15)
    @ApiModelProperty(value = "1:已推送；2：符合推送；2018.6.11 add")
    private java.math.BigDecimal istrans;
	/**最后一次推送的时间2018.6.11 add*/
	@Excel(name = "最后一次推送的时间2018.6.11 add", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "最后一次推送的时间2018.6.11 add")
    private java.util.Date transdate;
	/**推送的数据标志，I：新增；U：更新；D：删除；2018.6.15 add*/
	@Excel(name = "推送的数据标志，I：新增；U：更新；D：删除；2018.6.15 add", width = 15)
    @ApiModelProperty(value = "推送的数据标志，I：新增；U：更新；D：删除；2018.6.15 add")
    private java.lang.String transdataflag;
}
