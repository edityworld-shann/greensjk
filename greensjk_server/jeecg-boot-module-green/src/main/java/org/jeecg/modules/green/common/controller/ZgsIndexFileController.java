package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsIndexFile;
import org.jeecg.modules.green.common.service.IZgsIndexFileService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 登录页文件下载表
 * @Author: jeecg-boot
 * @Date: 2022-03-05
 * @Version: V1.0
 */
@Api(tags = "登录页文件下载表")
@RestController
@RequestMapping("/common/zgsIndexFile")
@Slf4j
public class ZgsIndexFileController extends JeecgController<ZgsIndexFile, IZgsIndexFileService> {
    @Autowired
    private IZgsIndexFileService zgsIndexFileService;
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;

    /**
     * 分页列表查询
     *
     * @param zgsIndexFile
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "登录页文件下载表-分页列表查询")
    @ApiOperation(value = "登录页文件下载表-分页列表查询", notes = "登录页文件下载表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsIndexFile zgsIndexFile,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "assType", required = false) Integer assType,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsIndexFile> queryWrapper = new QueryWrapper();
        if (zgsIndexFile != null && StringUtils.isNotEmpty(zgsIndexFile.getFileName())) {
            queryWrapper.like("file_name", zgsIndexFile.getFileName());
        }
        if (assType != null) {
            if (assType == 2) {
                //首页发布查询
                queryWrapper.eq("send_status", "1");
            }
        }
        queryWrapper.orderByDesc("create_time");
        queryWrapper.orderByDesc("send_time");
        Page<ZgsIndexFile> page = new Page<ZgsIndexFile>(pageNo, pageSize);
        IPage<ZgsIndexFile> pageList = zgsIndexFileService.page(page, queryWrapper);
        pageList.getRecords().forEach(item -> {
            item.setDownUrl(downloadUrl + item.getUrl());
            ValidateEncryptEntityUtil.validateDecryptObject(item, ValidateEncryptEntityUtil.isDecrypt);
        });
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsIndexFile
     * @return
     */
    @AutoLog(value = "登录页文件下载表-添加")
    @ApiOperation(value = "登录页文件下载表-添加", notes = "登录页文件下载表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsIndexFile zgsIndexFile) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsIndexFile.setCreateBy(sysUser.getRealname());
        zgsIndexFile.setCreateTime(new Date());
        zgsIndexFileService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsIndexFile, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsIndexFile
     * @return
     */
    @AutoLog(value = "登录页文件下载表-编辑")
    @ApiOperation(value = "登录页文件下载表-编辑", notes = "登录页文件下载表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsIndexFile zgsIndexFile) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsIndexFile.setUpdateBy(sysUser.getRealname());
        zgsIndexFile.setUpdateTime(new Date());
        zgsIndexFileService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsIndexFile, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 发布或撤销
     *
     * @param zgsIndexFile
     * @return
     */
    @AutoLog(value = "登录页文件下载表-发布或撤销")
    @ApiOperation(value = "登录页文件下载表-发布或撤销", notes = "登录页文件下载表-发布或撤销")
    @PutMapping(value = "/updateStatusById")
    public Result<?> updateStatusById(@RequestBody ZgsIndexFile zgsIndexFile) {
        String result = "";
        if (zgsIndexFile != null && StringUtils.isNotEmpty(zgsIndexFile.getId())) {
            if ("0".equals(zgsIndexFile.getSendStatus())) {
                //撤销
                zgsIndexFile.setCancelTime(new Date());
                result = "撤销";
            } else {
                //发布
                zgsIndexFile.setSendTime(new Date());
                result = "发布";
            }
            zgsIndexFileService.updateById(zgsIndexFile);
        }

        return Result.OK(result + "成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "登录页文件下载表-通过id删除")
    @ApiOperation(value = "登录页文件下载表-通过id删除", notes = "登录页文件下载表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsIndexFileService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "登录页文件下载表-批量删除")
    @ApiOperation(value = "登录页文件下载表-批量删除", notes = "登录页文件下载表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsIndexFileService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "登录页文件下载表-通过id查询")
    @ApiOperation(value = "登录页文件下载表-通过id查询", notes = "登录页文件下载表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsIndexFile zgsIndexFile = zgsIndexFileService.getById(id);
        if (zgsIndexFile == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsIndexFile);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsIndexFile
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsIndexFile zgsIndexFile) {
        return super.exportXls(request, zgsIndexFile, ZgsIndexFile.class, "登录页文件下载表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsIndexFile.class);
    }

}
