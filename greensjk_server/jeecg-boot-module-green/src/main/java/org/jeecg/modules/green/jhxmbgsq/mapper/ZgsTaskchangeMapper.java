package org.jeecg.modules.green.jhxmbgsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsTaskchange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目变更记录
 * @Author: jeecg-boot
 * @Date:   2022-03-15
 * @Version: V1.0
 */
public interface ZgsTaskchangeMapper extends BaseMapper<ZgsTaskchange> {

}
