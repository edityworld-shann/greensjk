package org.jeecg.modules.green.largeScreen.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuildingEnergySavingDetails implements Serializable {
    private String name;
    private String value;
    private String area;
}
