package org.jeecg.modules.green.common.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.common.po.ZgsExpertinfoPo;

/**
 * @Description: 业务库-各类项目工程申报材料
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface ZgsMattermaterialMapper extends BaseMapper<ZgsMattermaterial> {
    List<ZgsMattermaterial> getZgsMattermaterialList(@Param("cm") Map<String, Object> paramMap);

    List<ZgsMattermaterial> getAddZgsMattermaterialList(@Param("cm") Map<String, Object> paramMap);

    List<ZgsExpertinfoPo> getTheNumberOfExpertsFor();

    int getUserCount(@Param("itemValue") String itemValue, @Param("accesstype") Integer type);
}
