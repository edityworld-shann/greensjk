package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface ZgsScientificresearcherMapper extends BaseMapper<ZgsScientificresearcher> {

}
