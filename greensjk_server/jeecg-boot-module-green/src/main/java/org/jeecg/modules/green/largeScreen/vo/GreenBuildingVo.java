package org.jeecg.modules.green.largeScreen.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Name ：GreenBuildingVo
 * @Description ：<对前端进行输出>
 * @Author ：Haozhiyang
 * @Date ：2022/8/22 14:52
 * @Version ：1.0
 * @History ：<修改代码时说明>
 */
@Data
public class GreenBuildingVo implements Serializable {

    private String areaname;
    private String greenBuildingArea;
    private String greenBuildingCount;
    private List<GreenBuildingDetails> detailsList;

}


