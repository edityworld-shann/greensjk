package org.jeecg.modules.green.worktable.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CityAndStateWorkbenchDetailsPo implements Serializable {
    private String areaname;
    private String monthNumber;
}
