package org.jeecg.modules.green.kyxmjtsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研结题主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsScientificpostresearcherMapper extends BaseMapper<ZgsScientificpostresearcher> {

}
