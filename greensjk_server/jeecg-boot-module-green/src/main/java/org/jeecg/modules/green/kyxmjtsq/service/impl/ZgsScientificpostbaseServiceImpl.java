package org.jeecg.modules.green.kyxmjtsq.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostbaseMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 科研项目申报结项基本信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostbaseServiceImpl extends ServiceImpl<ZgsScientificpostbaseMapper, ZgsScientificpostbase> implements IZgsScientificpostbaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;

    @Override
    public int spProject(ZgsScientificpostbase zgsScientificpostbase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsScientificpostbase != null && StringUtils.isNotEmpty(zgsScientificpostbase.getId())) {
            ZgsScientificpostbase scientificpostbase = new ZgsScientificpostbase();
            scientificpostbase.setId(zgsScientificpostbase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsScientificpostbase.getId());
            zgsReturnrecord.setReturnreason(zgsScientificpostbase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE13);
            ZgsScientificpostbase postbaseInfo = getById(zgsScientificpostbase.getId());
            if (postbaseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(postbaseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(postbaseInfo.getApplydate());
                zgsReturnrecord.setProjectname(postbaseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", postbaseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsScientificpostbase.getSpStatus() == 1) {
                //通过记录状态==0
                scientificpostbase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsScientificpostbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsScientificpostbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsScientificpostbase.getStatus())) {
                    scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    scientificpostbase.setFirstdate(new Date());
                    scientificpostbase.setFirstperson(sysUser.getRealname());
                    scientificpostbase.setFirstdepartment(sysUser.getEnterprisename());
                    scientificpostbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS8);
                    scientificpostbase.setFinishperson(sysUser.getRealname());
                    scientificpostbase.setFinishdate(new Date());
                    scientificpostbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                scientificpostbase.setAuditopinion(zgsScientificpostbase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsScientificpostbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsScientificpostbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsScientificpostbase.getStatus())) {
                    if (zgsScientificpostbase.getSpStatus() == 2) {
                        scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    scientificpostbase.setFirstdate(new Date());
                    scientificpostbase.setFirstperson(sysUser.getRealname());
                    scientificpostbase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsScientificpostbase.getSpStatus() == 2) {
                        scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    scientificpostbase.setFinishperson(sysUser.getRealname());
                    scientificpostbase.setFinishdate(new Date());
                    scientificpostbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsScientificpostbase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        scientificpostbase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(postbaseInfo.getScientificbaseguid(), postbaseInfo.getEnterpriseguid(), zgsScientificpostbase.getAuditopinion());

                    }
                }
                if (zgsScientificpostbase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsScientificpostbase.getAuditopinion())) {
                        zgsScientificpostbase.setAuditopinion(" ");
                    }
                    scientificpostbase.setAuditopinion(zgsScientificpostbase.getAuditopinion());
                    scientificpostbase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsScientificpostbase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    scientificpostbase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(postbaseInfo.getScientificbaseguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_43));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(scientificpostbase, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }
}
