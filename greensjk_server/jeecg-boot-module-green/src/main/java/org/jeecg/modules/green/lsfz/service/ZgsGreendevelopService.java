package org.jeecg.modules.green.lsfz.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelop;

/**
 * @describe: 绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/13 14:31
 */
public interface ZgsGreendevelopService extends IService<ZgsGreendevelop> {

    Page<ZgsGreendevelop> greenDevelopList(Wrapper<ZgsGreendevelop> queryWrapper, Integer pageNo, Integer pageSize);



}
