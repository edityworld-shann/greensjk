package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenAreadetail;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuild;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildArea;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildDetail;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenAreadetailService;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildAreaService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthgreenbuild_area
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="绿色建筑（区县）")
@RestController
@RequestMapping("/report/zgsReportMonthgreenbuildArea")
@Slf4j
public class ZgsReportMonthgreenbuildAreaController extends JeecgController<ZgsReportMonthgreenbuildArea, IZgsReportMonthgreenbuildAreaService> {
	@Autowired
	private IZgsReportMonthgreenbuildAreaService zgsReportMonthgreenbuildAreaService;

	 @Autowired
	 private IZgsReportMonthgreenAreadetailService zgsReportMonthgreenAreadetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthgreenbuildArea
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "绿色建筑（区县）-分页列表查询")
	@ApiOperation(value="绿色建筑（区县）-分页列表查询", notes="绿色建筑（区县）-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(name = "prossType", required = false) Integer prossType,
                   @RequestParam(name = "applyState", required = false) String applyState,
								   HttpServletRequest req) {
//		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//		使用登录账号类型：0单位，1个人，2专家库，3住建厅（省），4推荐单位，6县区，7市区
//		sysUser.getLoginUserType()
//		或
//		sysUser.getAreacode()
//		筛选
//		区账号查询各县区汇总
//		省厅账号查询各市区汇总
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		QueryWrapper<ZgsReportMonthgreenbuildArea> qW = new QueryWrapper<>();
		if(zgsReportMonthgreenbuildArea!=null){
			if(zgsReportMonthgreenbuildArea.getApplystate()!=null){
				//有问题，BigDecimal applystate 需要判空？
				BigDecimal applystate  = zgsReportMonthgreenbuildArea.getApplystate();
				qW.eq("applystate",applystate);
			}
			Integer userType = sysUser.getLoginUserType();
			//6县区，7市区
			if(userType==6){
				qW.eq("areacode",sysUser.getAreacode());
			}else if(userType==7){
				qW.like("areacode",sysUser.getAreacode());
			}
			if(StringUtils.isNotEmpty(zgsReportMonthgreenbuildArea.getReporttm())){
				String reporttm  = zgsReportMonthgreenbuildArea.getReporttm();
				qW.eq("reporttm",reporttm);
			}
		}
		//  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
		if(applyState != null){
      splitArray = applyState.split(",");
    }
		if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
		qW.ne("isdelete",1);
		qW.orderByDesc("createtime");

//		QueryWrapper<ZgsReportMonthgreenbuildArea> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthgreenbuildArea, req.getParameterMap());
		Page<ZgsReportMonthgreenbuildArea> page = new Page<ZgsReportMonthgreenbuildArea>(pageNo, pageSize);
		IPage<ZgsReportMonthgreenbuildArea> pageList = zgsReportMonthgreenbuildAreaService.page(page, qW);
		//根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
    if(pageList.getRecords().size() > 0){
      for(ZgsReportMonthgreenbuildArea greenbuildArea : pageList.getRecords()){
        List<ZgsReportMonthgreenAreadetail> areaDetailList = findAreaDetailList(greenbuildArea);
        if(areaDetailList.size() > 0){
          for (int i = 0;i < areaDetailList.size();i++){
            if ("0".equals(areaDetailList.get(i).getProsstype().toString())){
              greenbuildArea.setDt0(areaDetailList.get(i));
            }else if ("1".equals(areaDetailList.get(i).getProsstype().toString())){
              greenbuildArea.setDt1(areaDetailList.get(i));
            }else if ("2".equals(areaDetailList.get(i).getProsstype().toString())){
              greenbuildArea.setDt2(areaDetailList.get(i));
            }else if ("3".equals(areaDetailList.get(i).getProsstype().toString())){
              greenbuildArea.setDt3(areaDetailList.get(i));
            }
          }
        }
      }
    }
		return Result.OK(pageList);
	}


   /**
    * 分页列表查询 根据 ProssType
    *
    * @param zgsReportMonthgreenbuildArea
    * @param pageNo
    * @param pageSize
    * @return
    */
   @AutoLog(value = "绿色建筑（区县）-分页列表查询")
   @ApiOperation(value="绿色建筑（区县）-分页列表查询", notes="绿色建筑（区县）-分页列表查询")
   @GetMapping(value = "/queryPageListByProssType")
   public Result<?> queryPageListByProssType(ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  @RequestParam(name = "prossType", required = false) Integer prossType){
//		使用登录账号类型：0单位，1个人，2专家库，3住建厅（省），4推荐单位，6县区，7市区
     Map countMap = new HashMap();
     LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
     QueryWrapper<ZgsReportMonthgreenbuildArea> qW = new QueryWrapper<>();
     if(zgsReportMonthgreenbuildArea!=null){
       if(zgsReportMonthgreenbuildArea.getApplystate()!=null){
         BigDecimal applystate  = zgsReportMonthgreenbuildArea.getApplystate();
         qW.eq("applystate",applystate);
       }
       Integer userType = sysUser.getLoginUserType();
       //6县区，7市区
       if(userType==6){
         qW.eq("areacode",sysUser.getAreacode());
       }else if(userType==7){
         qW.like("areacode",sysUser.getAreacode());
       }
       if(StringUtils.isNotEmpty(zgsReportMonthgreenbuildArea.getReporttm())){
         String reporttm  = zgsReportMonthgreenbuildArea.getReporttm();
         qW.eq("reporttm",reporttm);
       }
     }
     qW.ne("isdelete",1);
     qW.orderByDesc("createtime");
     Page<ZgsReportMonthgreenbuildArea> page = new Page<ZgsReportMonthgreenbuildArea>(pageNo, pageSize);
     IPage<ZgsReportMonthgreenbuildArea> pageList = zgsReportMonthgreenbuildAreaService.page(page, qW);
     //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
     if(pageList.getRecords().size() > 0){
       for(ZgsReportMonthgreenbuildArea greenbuildArea : pageList.getRecords()){
         List<ZgsReportMonthgreenAreadetail> areaDetailList = findAreaDetailList(greenbuildArea);
         if(areaDetailList.size() > 0){
           if (prossType != null){
             for (int i = 0;i < areaDetailList.size();i++){
               if (prossType.equals(areaDetailList.get(i).getProsstype().toString())){
                 if ("0".equals(prossType)){
                   greenbuildArea.setDt0(areaDetailList.get(i));
                   countMap = this.wkgCount(pageList);
                 }else if ("1".equals(prossType)){
                   greenbuildArea.setDt1(areaDetailList.get(i));
                   countMap = this.ykgCount(pageList);
                 }else if ("2".equals(prossType)){
                   greenbuildArea.setDt2(areaDetailList.get(i));
                   countMap = this.ykgCount(pageList);
                 }else if ("3".equals(prossType)){
                   greenbuildArea.setDt3(areaDetailList.get(i));
                   countMap = this.ykgCount(pageList);
                 }
               }
             }
           }
         }
       }
     }
     Result result = new Result();
     //     Gson gson=new Gson();
     //     operation = gson.fromJson( countMap, Map.class).get("operation").toString();
     //     JSONObject json = new JSONObject(countMap);	//	使用 fastjson 将 内层对象转为 json 格式
     result.setMessage(JSON.toJSONString(countMap));
     result.setResult(pageList);
     return result;
   }


   /**
    *   计算 "统计报表" 已开工 各项 合计
    */
   private Map<String, Object> ykgCount (IPage<ZgsReportMonthgreenbuildArea> wkgList){
     Map<String, Object> map = new HashMap<>();

     Double bybzxxs = 0.0;
     Double bybzxmj = 0.0;
     Double byxs = 0.0;
     Double bymj = 0.0;
     Double bysyzzxqxs = 0.0;
     Double bysyzzxqmj = 0.0;

     Double bydxggjzmj = 0.0;
     Double bydxggjzxs = 0.0;
     Double byzxxggjzxs = 0.0;
     Double byzxxggjzmj = 0.0;
     Double byzftzlggjzxs = 0.0;

     Double byzftzlggjzmj = 0.0;
     Double byzzjzxs = 0.0;
     Double byzzjzmj = 0.0;
     Double bygyjzxs = 0.0;
     Double bygyjzmj = 0.0;

     Double ncxs = 0.0;
     Double ncmj = 0.0;
     Double ncbzxxs = 0.0;
     Double ncbzxmj = 0.0;
     Double ncsyzzxqxs = 0.0;

     Double ncsyzzxqmj = 0.0;
     Double ncdxggjzxs = 0.0;
     Double ncdxggjzmj = 0.0;
     Double nczxsggjzxs = 0.0;
     Double nczxsggjzmj = 0.0;

     Double nczftzlggjzxs = 0.0;
     Double nczftzlggjzmj = 0.0;
     Double nczzjzxs = 0.0;
     Double nczzjzmj = 0.0;
     Double ncgyjzxs = 0.0;
     Double ncgyjzmj = 0.0;

     for (int i = 0;i < wkgList.getRecords().size();i++){
       ZgsReportMonthgreenAreadetail greenAreaDetail = wkgList.getRecords().get(i).getDt0();

       if (greenAreaDetail.getByxs() != null){
         byxs += Double.parseDouble(greenAreaDetail.getByxs().toString());
       }
       if (greenAreaDetail.getBymj() != null){
         bymj += Double.parseDouble(greenAreaDetail.getBymj().toString());
       }
       if (greenAreaDetail.getBybzxxs() != null){
         bybzxxs += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
       }
       if (greenAreaDetail.getBybzxmj() != null){
         bybzxmj += Double.parseDouble(greenAreaDetail.getBybzxmj().toString());
       }
       if (greenAreaDetail.getBysyzzxqxs() != null){
         bysyzzxqxs += Double.parseDouble(greenAreaDetail.getBysyzzxqxs().toString());
       }
       if (greenAreaDetail.getBysyzzxqmj() != null){
         bysyzzxqmj += Double.parseDouble(greenAreaDetail.getBysyzzxqmj().toString());
       }
       if (greenAreaDetail.getBydxggjzmj() != null){
         bydxggjzmj += Double.parseDouble(greenAreaDetail.getBydxggjzmj().toString());
       }
       if (greenAreaDetail.getBydxggjzxs() != null){
         bydxggjzxs += Double.parseDouble(greenAreaDetail.getBydxggjzxs().toString());
       }
       if (greenAreaDetail.getBybzxxs() != null){
         byzxxggjzmj += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
       }if (greenAreaDetail.getBybzxxs() != null){
         byzxxggjzxs += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
       }
       if (greenAreaDetail.getByzftzlggjzxs() != null){
         byzftzlggjzxs += Double.parseDouble(greenAreaDetail.getByzftzlggjzxs().toString());
       }
       if (greenAreaDetail.getByzftzlggjzmj() != null){
         byzftzlggjzmj += Double.parseDouble(greenAreaDetail.getByzftzlggjzmj().toString());
       }
       if (greenAreaDetail.getByzzjzxs() != null){
         byzzjzxs += Double.parseDouble(greenAreaDetail.getByzzjzxs().toString());
       }
       if (greenAreaDetail.getByzzjzmj() != null){
         byzzjzmj += Double.parseDouble(greenAreaDetail.getByzzjzmj().toString());
       }
       if (greenAreaDetail.getBygyjzxs() != null){
         bygyjzxs += Double.parseDouble(greenAreaDetail.getBygyjzxs().toString());
       }
       if (greenAreaDetail.getBygyjzmj() != null){
         bygyjzmj += Double.parseDouble(greenAreaDetail.getBygyjzmj().toString());
       }
       if (greenAreaDetail.getNcxs() != null){
         ncxs += Double.parseDouble(greenAreaDetail.getNcxs().toString());
       }
       if (greenAreaDetail.getByzzjzmj() != null){
         ncmj += Double.parseDouble(greenAreaDetail.getByzzjzmj().toString());
       }
       if (greenAreaDetail.getNcbzxxs() != null){
         ncbzxxs += Double.parseDouble(greenAreaDetail.getNcbzxxs().toString());
       }
       if (greenAreaDetail.getNcbzxmj() != null){
         ncbzxmj += Double.parseDouble(greenAreaDetail.getNcbzxmj().toString());
       }
       if (greenAreaDetail.getNcsyzzxqxs() != null){
         ncsyzzxqxs += Double.parseDouble(greenAreaDetail.getNcsyzzxqxs().toString());
       }
       if (greenAreaDetail.getNcsyzzxqmj() != null){
         ncsyzzxqmj += Double.parseDouble(greenAreaDetail.getNcsyzzxqmj().toString());
       }
       if (greenAreaDetail.getNcdxggjzxs() != null){
         ncdxggjzxs += Double.parseDouble(greenAreaDetail.getNcdxggjzxs().toString());
       }
       if (greenAreaDetail.getNcdxggjzmj() != null){
         ncdxggjzmj += Double.parseDouble(greenAreaDetail.getNcdxggjzmj().toString());
       }
       if (greenAreaDetail.getNczxsggjzxs() != null){
         nczxsggjzxs += Double.parseDouble(greenAreaDetail.getNczxsggjzxs().toString());
       }
       if (greenAreaDetail.getNczxsggjzmj() != null){
         nczxsggjzmj += Double.parseDouble(greenAreaDetail.getNczxsggjzmj().toString());
       }
       if (greenAreaDetail.getNczftzlggjzxs() != null){
         nczftzlggjzxs += Double.parseDouble(greenAreaDetail.getNczftzlggjzxs().toString());
       }
       if (greenAreaDetail.getNczftzlggjzmj() != null){
         nczftzlggjzmj += Double.parseDouble(greenAreaDetail.getNczftzlggjzmj().toString());
       }
       if (greenAreaDetail.getNczzjzxs() != null){
         nczzjzxs += Double.parseDouble(greenAreaDetail.getNczzjzxs().toString());
       }
       if (greenAreaDetail.getNczzjzmj() != null){
         nczzjzmj += Double.parseDouble(greenAreaDetail.getNczzjzmj().toString());
       }
       if (greenAreaDetail.getNcgyjzxs() != null){
         ncgyjzxs += Double.parseDouble(greenAreaDetail.getNcgyjzxs().toString());
       }
       if (greenAreaDetail.getNcgyjzmj() != null){
         ncgyjzmj += Double.parseDouble(greenAreaDetail.getNcgyjzmj().toString());
       }
     }

     map.put("bybzxxs", bybzxxs);
     map.put("bybzxmj", bybzxmj);
     map.put("byxs", byxs);
     map.put("bymj", bymj);
     map.put("bysyzzxqxs", bysyzzxqxs);

     map.put("bydxggjzmj", bydxggjzmj);
     map.put("bydxggjzxs", bydxggjzxs);
     map.put("byzxxggjzxs", byzxxggjzxs);
     map.put("byzxxggjzmj", byzxxggjzmj);
     map.put("byzftzlggjzxs", byzftzlggjzxs);

     map.put("byzftzlggjzmj", byzftzlggjzmj);
     map.put("byzzjzxs", byzzjzxs);
     map.put("byzzjzmj", byzzjzmj);
     map.put("bygyjzxs", bygyjzxs);
     map.put("bygyjzmj", bygyjzmj);

     map.put("ncxs", ncxs);
     map.put("ncmj", ncmj);
     map.put("ncbzxxs", ncbzxxs);
     map.put("ncbzxmj", ncbzxmj);
     map.put("ncsyzzxqxs", ncsyzzxqxs);

     map.put("ncsyzzxqmj", ncsyzzxqmj);
     map.put("ncdxggjzxs", ncdxggjzxs);
     map.put("ncdxggjzmj", ncdxggjzmj);
     map.put("nczxsggjzxs", nczxsggjzxs);
     map.put("nczxsggjzmj", nczxsggjzmj);

     map.put("nczftzlggjzxs", nczftzlggjzxs);
     map.put("nczftzlggjzmj", nczftzlggjzmj);
     map.put("nczzjzxs", nczzjzxs);
     map.put("nczzjzmj", nczzjzmj);
     map.put("ncgyjzxs", ncgyjzxs);
     map.put("ncgyjzmj", ncgyjzmj);
     return map;
   }


   /**
    *   计算 "统计报表" 未开工各项 合计
    *   计算 "区县已竣工验收情况统计" 未开工各项 合计
    */
	private Map<String, Object> wkgCount (IPage<ZgsReportMonthgreenbuildArea> wkgList){
    Map<String, Object> map = new HashMap<>();

    Double bybzxxs = 0.0;
    Double bybzxmj = 0.0;
    Double byxs = 0.0;
    Double bymj = 0.0;
    Double bysyzzxqxs = 0.0;
    Double bysyzzxqmj = 0.0;

    Double bydxggjzmj = 0.0;
    Double bydxggjzxs = 0.0;
    Double byzxxggjzxs = 0.0;
    Double byzxxggjzmj = 0.0;
    Double byzftzlggjzxs = 0.0;

    Double byzftzlggjzmj = 0.0;
    Double byzzjzxs = 0.0;
    Double byzzjzmj = 0.0;
    Double bygyjzxs = 0.0;
    Double bygyjzmj = 0.0;

    Double ncxs = 0.0;
    Double ncmj = 0.0;
    Double ncbzxxs = 0.0;
    Double ncbzxmj = 0.0;
    Double ncsyzzxqxs = 0.0;

    Double ncsyzzxqmj = 0.0;
    Double ncdxggjzxs = 0.0;
    Double ncdxggjzmj = 0.0;
    Double nczxsggjzxs = 0.0;
    Double nczxsggjzmj = 0.0;

    Double nczftzlggjzxs = 0.0;
    Double nczftzlggjzmj = 0.0;
    Double nczzjzxs = 0.0;
    Double nczzjzmj = 0.0;
    Double ncgyjzxs = 0.0;
    Double ncgyjzmj = 0.0;

    Double sjyzzxs = 0.0;
    Double sjyzzmj = 0.0;
    Double sjyggxs = 0.0;
    Double sjyggmj = 0.0;
    Double sjygyxs = 0.0;
    Double sjygymj = 0.0;

    Double sjezzxs = 0.0;
    Double sjezzmj = 0.0;
    Double sjeggxs = 0.0;
    Double sjeggmj = 0.0;
    Double sjegyxs = 0.0;
    Double sjegymj = 0.0;

    Double sjszzxs = 0.0;
    Double sjszzmj = 0.0;
    Double sjsggxs = 0.0;
    Double sjsggmj = 0.0;
    Double sjsgyxs = 0.0;
    Double sjsgymj = 0.0;

    for (int i = 0;i < wkgList.getRecords().size();i++){
      ZgsReportMonthgreenAreadetail greenAreaDetail = wkgList.getRecords().get(i).getDt0();

      if (greenAreaDetail.getByxs() != null){
        byxs += Double.parseDouble(greenAreaDetail.getByxs().toString());
      }
      if (greenAreaDetail.getBymj() != null){
        bymj += Double.parseDouble(greenAreaDetail.getBymj().toString());
      }
      if (greenAreaDetail.getBybzxxs() != null){
        bybzxxs += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
      }
      if (greenAreaDetail.getBybzxmj() != null){
        bybzxmj += Double.parseDouble(greenAreaDetail.getBybzxmj().toString());
      }
      if (greenAreaDetail.getBysyzzxqxs() != null){
        bysyzzxqxs += Double.parseDouble(greenAreaDetail.getBysyzzxqxs().toString());
      }
      if (greenAreaDetail.getBysyzzxqmj() != null){
        bysyzzxqmj += Double.parseDouble(greenAreaDetail.getBysyzzxqmj().toString());
      }
      if (greenAreaDetail.getBydxggjzmj() != null){
        bydxggjzmj += Double.parseDouble(greenAreaDetail.getBydxggjzmj().toString());
      }
      if (greenAreaDetail.getBydxggjzxs() != null){
        bydxggjzxs += Double.parseDouble(greenAreaDetail.getBydxggjzxs().toString());
      }
      if (greenAreaDetail.getBybzxxs() != null){
        byzxxggjzmj += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
      }if (greenAreaDetail.getBybzxxs() != null){
        byzxxggjzxs += Double.parseDouble(greenAreaDetail.getBybzxxs().toString());
      }
      if (greenAreaDetail.getByzftzlggjzxs() != null){
        byzftzlggjzxs += Double.parseDouble(greenAreaDetail.getByzftzlggjzxs().toString());
      }
      if (greenAreaDetail.getByzftzlggjzmj() != null){
        byzftzlggjzmj += Double.parseDouble(greenAreaDetail.getByzftzlggjzmj().toString());
      }
      if (greenAreaDetail.getByzzjzxs() != null){
        byzzjzxs += Double.parseDouble(greenAreaDetail.getByzzjzxs().toString());
      }
      if (greenAreaDetail.getByzzjzmj() != null){
        byzzjzmj += Double.parseDouble(greenAreaDetail.getByzzjzmj().toString());
      }
      if (greenAreaDetail.getBygyjzxs() != null){
        bygyjzxs += Double.parseDouble(greenAreaDetail.getBygyjzxs().toString());
      }
      if (greenAreaDetail.getBygyjzmj() != null){
        bygyjzmj += Double.parseDouble(greenAreaDetail.getBygyjzmj().toString());
      }
      if (greenAreaDetail.getNcxs() != null){
        ncxs += Double.parseDouble(greenAreaDetail.getNcxs().toString());
      }
      if (greenAreaDetail.getByzzjzmj() != null){
        ncmj += Double.parseDouble(greenAreaDetail.getByzzjzmj().toString());
      }
      if (greenAreaDetail.getNcbzxxs() != null){
        ncbzxxs += Double.parseDouble(greenAreaDetail.getNcbzxxs().toString());
      }
      if (greenAreaDetail.getNcbzxmj() != null){
        ncbzxmj += Double.parseDouble(greenAreaDetail.getNcbzxmj().toString());
      }
      if (greenAreaDetail.getNcsyzzxqxs() != null){
        ncsyzzxqxs += Double.parseDouble(greenAreaDetail.getNcsyzzxqxs().toString());
      }
      if (greenAreaDetail.getNcsyzzxqmj() != null){
        ncsyzzxqmj += Double.parseDouble(greenAreaDetail.getNcsyzzxqmj().toString());
      }
      if (greenAreaDetail.getNcdxggjzxs() != null){
        ncdxggjzxs += Double.parseDouble(greenAreaDetail.getNcdxggjzxs().toString());
      }
      if (greenAreaDetail.getNcdxggjzmj() != null){
        ncdxggjzmj += Double.parseDouble(greenAreaDetail.getNcdxggjzmj().toString());
      }
      if (greenAreaDetail.getNczxsggjzxs() != null){
        nczxsggjzxs += Double.parseDouble(greenAreaDetail.getNczxsggjzxs().toString());
      }
      if (greenAreaDetail.getNczxsggjzmj() != null){
        nczxsggjzmj += Double.parseDouble(greenAreaDetail.getNczxsggjzmj().toString());
      }
      if (greenAreaDetail.getNczftzlggjzxs() != null){
        nczftzlggjzxs += Double.parseDouble(greenAreaDetail.getNczftzlggjzxs().toString());
      }
      if (greenAreaDetail.getNczftzlggjzmj() != null){
        nczftzlggjzmj += Double.parseDouble(greenAreaDetail.getNczftzlggjzmj().toString());
      }
      if (greenAreaDetail.getNczzjzxs() != null){
        nczzjzxs += Double.parseDouble(greenAreaDetail.getNczzjzxs().toString());
      }
      if (greenAreaDetail.getNczzjzmj() != null){
        nczzjzmj += Double.parseDouble(greenAreaDetail.getNczzjzmj().toString());
      }
      if (greenAreaDetail.getNcgyjzxs() != null){
        ncgyjzxs += Double.parseDouble(greenAreaDetail.getNcgyjzxs().toString());
      }
      if (greenAreaDetail.getNcgyjzmj() != null){
        ncgyjzmj += Double.parseDouble(greenAreaDetail.getNcgyjzmj().toString());
      }

      if (greenAreaDetail.getSjyzzxs() != null){
        sjyzzxs += Double.parseDouble(greenAreaDetail.getSjyzzxs().toString());
      }
      if (greenAreaDetail.getSjyzzmj() != null){
        sjyzzmj += Double.parseDouble(greenAreaDetail.getSjyzzmj().toString());
      }
      if (greenAreaDetail.getSjyggxs() != null){
        sjyggxs += Double.parseDouble(greenAreaDetail.getSjyggxs().toString());
      }
      if (greenAreaDetail.getSjyggmj() != null){
        sjyggmj += Double.parseDouble(greenAreaDetail.getSjyggmj().toString());
      }
      if (greenAreaDetail.getSjygyxs() != null){
        sjygyxs += Double.parseDouble(greenAreaDetail.getSjygyxs().toString());
      }
      if (greenAreaDetail.getSjygymj() != null){
        sjygymj += Double.parseDouble(greenAreaDetail.getSjygymj().toString());
      }

      if (greenAreaDetail.getSjezzxs() != null){
        sjezzxs += Double.parseDouble(greenAreaDetail.getSjezzxs().toString());
      }
      if (greenAreaDetail.getSjyzzmj() != null){
        sjezzmj += Double.parseDouble(greenAreaDetail.getSjyzzmj().toString());
      }
      if (greenAreaDetail.getSjeggxs() != null){
        sjeggxs += Double.parseDouble(greenAreaDetail.getSjeggxs().toString());
      }
      if (greenAreaDetail.getSjeggmj() != null){
        sjeggmj += Double.parseDouble(greenAreaDetail.getSjeggmj().toString());
      }
      if (greenAreaDetail.getSjegyxs() != null){
        sjegyxs += Double.parseDouble(greenAreaDetail.getSjegyxs().toString());
      }
      if (greenAreaDetail.getSjegymj() != null){
        sjegymj += Double.parseDouble(greenAreaDetail.getSjegymj().toString());
      }

      if (greenAreaDetail.getSjszzxs() != null){
        sjszzxs += Double.parseDouble(greenAreaDetail.getSjszzxs().toString());
      }
      if (greenAreaDetail.getSjszzmj() != null){
        sjszzmj += Double.parseDouble(greenAreaDetail.getSjszzmj().toString());
      }
      if (greenAreaDetail.getSjsggxs() != null){
        sjsggxs += Double.parseDouble(greenAreaDetail.getSjsggxs().toString());
      }
      if (greenAreaDetail.getSjsggmj() != null){
        sjsggmj += Double.parseDouble(greenAreaDetail.getSjsggmj().toString());
      }
      if (greenAreaDetail.getSjsgyxs() != null){
        sjsgyxs += Double.parseDouble(greenAreaDetail.getSjsgyxs().toString());
      }
      if (greenAreaDetail.getSjsgymj() != null){
        sjsgymj += Double.parseDouble(greenAreaDetail.getSjsgymj().toString());
      }
    }

    map.put("bybzxxs", bybzxxs);
    map.put("bybzxmj", bybzxmj);
    map.put("byxs", byxs);
    map.put("bymj", bymj);
    map.put("bysyzzxqxs", bysyzzxqxs);

    map.put("bydxggjzmj", bydxggjzmj);
    map.put("bydxggjzxs", bydxggjzxs);
    map.put("byzxxggjzxs", byzxxggjzxs);
    map.put("byzxxggjzmj", byzxxggjzmj);
    map.put("byzftzlggjzxs", byzftzlggjzxs);

    map.put("byzftzlggjzmj", byzftzlggjzmj);
    map.put("byzzjzxs", byzzjzxs);
    map.put("byzzjzmj", byzzjzmj);
    map.put("bygyjzxs", bygyjzxs);
    map.put("bygyjzmj", bygyjzmj);

    map.put("ncxs", ncxs);
    map.put("ncmj", ncmj);
    map.put("ncbzxxs", ncbzxxs);
    map.put("ncbzxmj", ncbzxmj);
    map.put("ncsyzzxqxs", ncsyzzxqxs);

    map.put("ncsyzzxqmj", ncsyzzxqmj);
    map.put("ncdxggjzxs", ncdxggjzxs);
    map.put("ncdxggjzmj", ncdxggjzmj);
    map.put("nczxsggjzxs", nczxsggjzxs);
    map.put("nczxsggjzmj", nczxsggjzmj);

    map.put("nczftzlggjzxs", nczftzlggjzxs);
    map.put("nczftzlggjzmj", nczftzlggjzmj);
    map.put("nczzjzxs", nczzjzxs);
    map.put("nczzjzmj", nczzjzmj);
    map.put("ncgyjzxs", ncgyjzxs);
    map.put("ncgyjzmj", ncgyjzmj);

    map.put("sjyzzxs", sjyzzxs);
    map.put("sjyzzmj", sjyzzmj);
    map.put("sjyggxs", sjyggxs);
    map.put("sjyggmj", sjyggmj);
    map.put("sjygyxs", sjygyxs);
    map.put("sjygymj", sjygymj);

    map.put("sjezzxs", sjezzxs);
    map.put("sjezzmj", sjezzmj);
    map.put("sjeggxs", sjeggxs);
    map.put("sjeggmj", sjeggmj);
    map.put("sjegyxs", sjegyxs);
    map.put("sjegymj", sjegymj);

    map.put("sjszzxs", sjszzxs);
    map.put("sjszzmj", sjszzmj);
    map.put("sjsggxs", sjsggxs);
    map.put("sjsggmj", sjsggmj);
    map.put("sjsgyxs", sjsgyxs);
    map.put("sjsgymj", sjsgymj);
	  return map;
  }


   /**
    * @return
    */
   @AutoLog(value = "查询有上报记录的最新的月份信息")
   @ApiOperation(value="查询有上报记录的最新的月份信息", notes="查询有上报记录的最新的月份信息")
   @GetMapping(value = "/queryLatestInfo")
   public Result<?> queryLatestInfo(@RequestParam(name="dataSourceName",required=true) String dataSourceName,
                                    @RequestParam(name="processType",required=false) String processType) {
     if ("greenBuild_Qx".equals(dataSourceName)){
       if (processType != null){
          dataSourceName = GlobalConstants.greenBuild_areaDetail_Qx;
       }else {
          dataSourceName = GlobalConstants.greenBuild_Qx;
       }
     }else if ("greenBuild_Sz".equals(dataSourceName)){
       if (processType != null){
         dataSourceName = GlobalConstants.greenBuild_detail_Sz;
       }else {
         dataSourceName = GlobalConstants.greenBuild_Sz;
       }
     }else if ("fabricate_area_Qx".equals(dataSourceName)){
       if (processType != null){
         if ("1".equals(processType) || "2".equals(processType)){
           dataSourceName = GlobalConstants.fabricate_areadetail_Qx;
         }else if ("3".equals(processType)){
           processType = null;
           dataSourceName = GlobalConstants.fabricate_areaScDetail_Qx;
         }
       }else {
         dataSourceName = GlobalConstants.fabricate_area_Qx;
       }
     }else if ("fabricate_Sz".equals(dataSourceName)){
       if (processType != null){
         if ("1".equals(processType) || "2".equals(processType)){
           dataSourceName = GlobalConstants.fabricate_detail_Sz;
         }else if ("3".equals(processType)){
           processType = null;
           dataSourceName = GlobalConstants.fabricate_scDetail_Sz;
         }
       }else {
         dataSourceName = GlobalConstants.fabricate_Sz;
       }
     }else if ("buildingenergy_Qx".equals(dataSourceName)){
       dataSourceName = GlobalConstants.buildingenergy_Qx;
     }else if ("buildingenergy_Sz".equals(dataSourceName)){
       dataSourceName = GlobalConstants.buildingenergy_Sz;
     }else if ("buildingenergy_detail_Qx".equals(dataSourceName)){
       dataSourceName = GlobalConstants.buildingenergy_detail_Qx;
     }
     ZgsReportMonthgreenbuildArea greenObj = zgsReportMonthgreenbuildAreaService.queryLatestInfo(dataSourceName, processType);
     SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
     String latestDate = greenObj.getCreatetime().toString();
     Date dataFormat = null;
     try {
       dataFormat = sdf.parse(latestDate);
     } catch (ParseException e) {
       e.printStackTrace();
     }
     sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
     String date = sdf.format(dataFormat);
     String dateNew = date.substring(0, 7);
     return Result.OK(dateNew);
   }


	/**
	 *   添加
	 *
	 * @param zgsReportMonthgreenbuildArea
	 * @return
	 */
	@AutoLog(value = "绿色建筑（区县）-添加")
	@ApiOperation(value="绿色建筑（区县）-添加", notes="绿色建筑（区县）-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		//生成主表ID
		String id = UUID.randomUUID().toString();
		zgsReportMonthgreenbuildArea.setId(id);
		//申报状态  0未上报
		zgsReportMonthgreenbuildArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
		//行政区域
		zgsReportMonthgreenbuildArea.setAreacode(sysUser.getAreacode());
		zgsReportMonthgreenbuildArea.setAreaname(sysUser.getAreaname());
		zgsReportMonthgreenbuildArea.setIsdelete(new BigDecimal(0));
		zgsReportMonthgreenbuildArea.setCreatepersonaccount(sysUser.getUsername());
		zgsReportMonthgreenbuildArea.setCreatepersonname(sysUser.getRealname());
		zgsReportMonthgreenbuildArea.setCreatetime(new Date());
		zgsReportMonthgreenbuildAreaService.save(zgsReportMonthgreenbuildArea);
    String prosstype = "0";
		//保存各种状态下的明细数据
		//根据工程进度状态命名0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行
		ZgsReportMonthgreenAreadetail dt0 = zgsReportMonthgreenbuildArea.getDt0();
		if(dt0!=null){
		  prosstype = "0";
			saveAreaDetail(dt0,sysUser,id,prosstype);
		}
		ZgsReportMonthgreenAreadetail dt1 = zgsReportMonthgreenbuildArea.getDt1();
		if(dt1!=null){
      prosstype = "1";
      saveAreaDetail(dt1,sysUser,id,prosstype);
		}
		ZgsReportMonthgreenAreadetail dt2 = zgsReportMonthgreenbuildArea.getDt2();
		if(dt2!=null){
      prosstype = "2";
      saveAreaDetail(dt2,sysUser,id,prosstype);
		}
		ZgsReportMonthgreenAreadetail dt3 = zgsReportMonthgreenbuildArea.getDt3();
		if(dt3!=null){
      prosstype = "3";
      saveAreaDetail(dt3,sysUser,id,prosstype);
		}
		return Result.OK("添加成功！");
	}
//保存明细
	public void saveAreaDetail(ZgsReportMonthgreenAreadetail areaDetail,LoginUser sysUser,String id, String prosstype){
			//生成明细表ID
			String id2 = UUID.randomUUID().toString();
			areaDetail.setId(id2);
			//关联主表ID
      areaDetail.setMonthmianguid(id);
      /**0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行*/
      areaDetail.setProsstype(new BigDecimal(prosstype));
      areaDetail.setIsdelete(new BigDecimal(0));
			areaDetail.setCreatepersonaccount(sysUser.getUsername());
			areaDetail.setCreatepersonname(sysUser.getRealname());
			areaDetail.setCreatetime(new Date());
			zgsReportMonthgreenAreadetailService.save(areaDetail);
	}

   //保存明细
   public void saveAreaDetail(ZgsReportMonthgreenAreadetail areaDetail,LoginUser sysUser,String id){
     //生成明细表ID
     String id2 = UUID.randomUUID().toString();
     areaDetail.setId(id2);
     //关联主表ID
     areaDetail.setMonthmianguid(id);
     areaDetail.setCreatepersonaccount(sysUser.getUsername());
     areaDetail.setCreatepersonname(sysUser.getRealname());
     areaDetail.setCreatetime(new Date());
     zgsReportMonthgreenAreadetailService.save(areaDetail);
   }
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthgreenbuildArea
	 * @return
	 */
	@AutoLog(value = "绿色建筑（区县）-编辑")
	@ApiOperation(value="绿色建筑（区县）-编辑", notes="绿色建筑（区县）-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (zgsReportMonthgreenbuildArea != null && StringUtils.isNotEmpty(zgsReportMonthgreenbuildArea.getId())) {
			String id = zgsReportMonthgreenbuildArea.getId();
			Integer applyStatus = zgsReportMonthgreenbuildArea.getApplystate().intValue();
			if (GlobalConstants.apply_state1.equals(applyStatus)) {
				//保存并上报
				zgsReportMonthgreenbuildArea.setApplydate(new Date());
			}
			if (GlobalConstants.apply_state3.equals(applyStatus)) {
				//退回
				zgsReportMonthgreenbuildArea.setBackrdate(new Date());
			}
			zgsReportMonthgreenbuildArea.setModifypersonaccount(sysUser.getUsername());
			zgsReportMonthgreenbuildArea.setModifypersonname(sysUser.getRealname());
			zgsReportMonthgreenbuildArea.setModifytime(new Date());
			zgsReportMonthgreenbuildAreaService.updateById(zgsReportMonthgreenbuildArea);

			//更新明细表数据
			ZgsReportMonthgreenAreadetail dt0 = zgsReportMonthgreenbuildArea.getDt0();
			ZgsReportMonthgreenAreadetail dt1 = zgsReportMonthgreenbuildArea.getDt1();
			ZgsReportMonthgreenAreadetail dt2 = zgsReportMonthgreenbuildArea.getDt2();
			ZgsReportMonthgreenAreadetail dt3 = zgsReportMonthgreenbuildArea.getDt3();
			if(dt0!=null){
				updateDetail(dt0,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype0);
			}
			if(dt1!=null){
				updateDetail(dt1,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype1);
			}
			if(dt2!=null){
				updateDetail(dt2,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype2);
			}
			if(dt3!=null){
				updateDetail(dt3,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype3);
			}

		}


		return Result.OK("编辑成功!");
	}
	 public void updateDetail(ZgsReportMonthgreenAreadetail areaDetail,LoginUser sysUser,String id){
		 if (StringUtils.isNotEmpty(areaDetail.getId())) {
			 //编辑
			 areaDetail.setModifypersonaccount(sysUser.getUsername());
			 areaDetail.setModifypersonname(sysUser.getRealname());
			 areaDetail.setModifytime(new Date());
			 zgsReportMonthgreenAreadetailService.updateById(areaDetail);
		 } else {
			 //新增
			 saveAreaDetail(areaDetail, sysUser, id);
		 }
	 }
	 public void deleteDetail(ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea,Integer prosstype){
		 QueryWrapper<ZgsReportMonthgreenAreadetail> qWdetail = new QueryWrapper<>();
		 qWdetail.eq("monthmianguid",zgsReportMonthgreenbuildArea.getId());
		 qWdetail.ne("isdelete",1);
		 qWdetail.eq("prosstype",prosstype);
		 zgsReportMonthgreenAreadetailService.remove(qWdetail);
	 }
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreenbuild_area-通过id删除")
	@ApiOperation(value="zgs_report_monthgreenbuild_area-通过id删除", notes="zgs_report_monthgreenbuild_area-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthgreenbuildAreaService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreenbuild_area-批量删除")
	@ApiOperation(value="zgs_report_monthgreenbuild_area-批量删除", notes="zgs_report_monthgreenbuild_area-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthgreenbuildAreaService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绿色建筑（区县）-通过id查询")
	@ApiOperation(value="绿色建筑（区县）-通过id查询", notes="绿色建筑（区县）-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea = zgsReportMonthgreenbuildAreaService.getById(id);
		if(zgsReportMonthgreenbuildArea==null) {
			return Result.error("未找到对应数据");
		}else{
			ZgsReportMonthgreenAreadetail dt0= findAreaDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype0);
			if(dt0!=null){
				zgsReportMonthgreenbuildArea.setDt0(dt0);
			}
			ZgsReportMonthgreenAreadetail dt1= findAreaDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype1);
			if(dt1!=null){
				zgsReportMonthgreenbuildArea.setDt1(dt1);
			}
			ZgsReportMonthgreenAreadetail dt2= findAreaDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype2);
			if(dt2!=null){
				zgsReportMonthgreenbuildArea.setDt2(dt2);
			}
			ZgsReportMonthgreenAreadetail dt3= findAreaDetail(zgsReportMonthgreenbuildArea,GlobalConstants.prosstype3);
			if(dt3!=null){
				zgsReportMonthgreenbuildArea.setDt3(dt3);
			}

		}

		return Result.OK(zgsReportMonthgreenbuildArea);
	}
	 //查找明细数据
	 public List<ZgsReportMonthgreenAreadetail> findAreaDetailList(ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea){
		 QueryWrapper<ZgsReportMonthgreenAreadetail> qWdetail = new QueryWrapper<>();
		 qWdetail.eq("monthmianguid",zgsReportMonthgreenbuildArea.getId());
		 qWdetail.ne("isdelete",1);
//		 qWdetail.eq("prosstype",prosstype);
		 List<ZgsReportMonthgreenAreadetail> list = zgsReportMonthgreenAreadetailService.list(qWdetail);
		 return list;
	 }

	//查找明细数据
	public ZgsReportMonthgreenAreadetail findAreaDetail(ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea,Integer prosstype){
		QueryWrapper<ZgsReportMonthgreenAreadetail> qWdetail = new QueryWrapper<>();
		qWdetail.eq("monthmianguid",zgsReportMonthgreenbuildArea.getId());
		qWdetail.ne("isdelete",1);
		qWdetail.eq("prosstype",prosstype);
		ZgsReportMonthgreenAreadetail areadetail = zgsReportMonthgreenAreadetailService.getOne(qWdetail);
		return areadetail;

	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthgreenbuildArea
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthgreenbuildArea zgsReportMonthgreenbuildArea) {
        return super.exportXls(request, zgsReportMonthgreenbuildArea, ZgsReportMonthgreenbuildArea.class, "zgs_report_monthgreenbuild_area");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthgreenbuildArea.class);
    }

}
