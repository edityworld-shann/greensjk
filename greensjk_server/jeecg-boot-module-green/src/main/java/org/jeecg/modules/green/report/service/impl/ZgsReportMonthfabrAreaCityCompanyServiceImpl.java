package org.jeecg.modules.green.report.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompany;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityCompanyMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompanyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 装配式建筑-单位库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreaCityCompanyServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaCityCompanyMapper, ZgsReportMonthfabrAreaCityCompany> implements IZgsReportMonthfabrAreaCityCompanyService {


    /**
     * 补零0️⃣ 清单
     *
     * @param filltm
     * @return
     */
    @Override
    public void addIterm(String filltm) {
        ZgsReportMonthfabrAreaCityCompany zgsReportMonthfabrAreaCityCompany = new ZgsReportMonthfabrAreaCityCompany();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        zgsReportMonthfabrAreaCityCompany.setId(UUID.randomUUID().toString());
        zgsReportMonthfabrAreaCityCompany.setCreatepersonaccount(sysUser.getUsername());
        zgsReportMonthfabrAreaCityCompany.setCreatepersonname(sysUser.getRealname());
        zgsReportMonthfabrAreaCityCompany.setCreatetime(new Date());
        zgsReportMonthfabrAreaCityCompany.setAreacode(sysUser.getAreacode());
        zgsReportMonthfabrAreaCityCompany.setAreaname(sysUser.getAreaname());
        //计算月份、年、季度
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportMonthfabrAreaCityCompany.setFilltm(filltm);
        zgsReportMonthfabrAreaCityCompany.setQuarter(new BigDecimal(quarter));
        zgsReportMonthfabrAreaCityCompany.setYear(new BigDecimal(year));

        //===========处理 主要技术体系（清单列表显示字段）  Begin ==============================================================================================================
        zgsReportMonthfabrAreaCityCompany.setCompanyName("无");
        zgsReportMonthfabrAreaCityCompany.setContactsPhone("无");
        zgsReportMonthfabrAreaCityCompany.setContacts("无");
        zgsReportMonthfabrAreaCityCompany.setCompanyaddress("无");

        zgsReportMonthfabrAreaCityCompany.setZpsmjgyy(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpsmjgscnl(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpsmjgscx(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpsgjgyy(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpsgjgscnl(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpsgjgscx(new BigDecimal(0));

        zgsReportMonthfabrAreaCityCompany.setZpshntsjcn(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpshntshejcn(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompany.setZpshntscx(new BigDecimal(0));

        zgsReportMonthfabrAreaCityCompany.setApplystate(new BigDecimal(1));
        zgsReportMonthfabrAreaCityCompany.setApplydate(null);
        zgsReportMonthfabrAreaCityCompany.setBuildType(null);
        zgsReportMonthfabrAreaCityCompany.setBuildChildType(null);
        zgsReportMonthfabrAreaCityCompany.setBuildType2(null);
        zgsReportMonthfabrAreaCityCompany.setBuildChildType2(null);
        zgsReportMonthfabrAreaCityCompany.setBuildTypeAll("无");

        zgsReportMonthfabrAreaCityCompany.setFillunit("无");
        zgsReportMonthfabrAreaCityCompany.setFillpersonname("无");
        zgsReportMonthfabrAreaCityCompany.setFillpersontel("无");
        zgsReportMonthfabrAreaCityCompany.setFilltm(filltm);
        zgsReportMonthfabrAreaCityCompany.setReporttm(filltm);
        //===========处理 主要技术体系（清单列表显示字段）  End ==============================================================================================================
        this.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompany, ValidateEncryptEntityUtil.isEncrypt));
    }
}
