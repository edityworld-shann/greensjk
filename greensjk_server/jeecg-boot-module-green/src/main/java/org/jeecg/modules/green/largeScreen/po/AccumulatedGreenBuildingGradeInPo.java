package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccumulatedGreenBuildingGradeInPo implements Serializable {
    private String areaname;
    private String yearRankNumber;
    private String yearRankArea;
    private String yearRankNumber1;
    private String yearRankArea1;
    private String yearRankNumber2;
    private String yearRankArea2;
    private String yearRankNumber3;
    private String yearRankArea3;
}
