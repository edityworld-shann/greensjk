package org.jeecg.modules.green.jhxmbgsq.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.mapper.ZgsPlannedprojectchangeMapper;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostresearcherService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostresearcherService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificresearcherService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertparticipantService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertresearcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 建设科技计划项目变更申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsPlannedprojectchangeServiceImpl extends ServiceImpl<ZgsPlannedprojectchangeMapper, ZgsPlannedprojectchange> implements IZgsPlannedprojectchangeService {

    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedchangeparticipantService zgsPlannedchangeparticipantService;
    @Autowired
    private IZgsTaskscienceparticipantService zgsTaskscienceparticipantService;
    @Autowired
    private IZgsTaskprojectmainparticipantService zgsTaskprojectmainparticipantService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsSacceptcertificatebaseService zgsSacceptcertificatebaseService;
    @Autowired
    private IZgsExamplecertificatebaseService zgsExamplecertificatebaseService;
    @Autowired
    private IZgsSpostcertificatebaseService zgsSpostcertificatebaseService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsExamplecertparticipantService zgsExamplecertparticipantService;
    @Autowired
    private IZgsScientificresearcherService zgsScientificresearcherService;
    @Autowired
    private IZgsSpostresearcherService zgsSpostresearcherService;
    @Autowired
    private IZgsSacceptcertresearcherService zgsSacceptcertresearcherService;
    @Autowired
    private IZgsScientificpostresearcherService zgsScientificpostresearcherService;

    @Override
    public int spProject(ZgsPlannedprojectchange zgsPlannedprojectchange) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPlannedprojectchange != null && StringUtils.isNotEmpty(zgsPlannedprojectchange.getId())) {
            ZgsPlannedprojectchange plannedprojectchange = new ZgsPlannedprojectchange();
            plannedprojectchange.setId(zgsPlannedprojectchange.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsPlannedprojectchange.getId());
            zgsReturnrecord.setReturnreason(zgsPlannedprojectchange.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE10);
            ZgsPlannedprojectchange plannedInfo = getById(zgsPlannedprojectchange.getId());
            String projectlibraryguid = null;
            if (plannedInfo != null) {
                projectlibraryguid = plannedInfo.getProjectlibraryguid();
                zgsReturnrecord.setEnterpriseguid(plannedInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(plannedInfo.getApplydate());
                zgsReturnrecord.setProjectname(plannedInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", plannedInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(zgsProjectunitmember.getEnterprisename());
                }
            } else {
                return 0;
            }
            if (zgsPlannedprojectchange.getSpStatus() == 1) {
                //通过记录状态==0
                plannedprojectchange.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPlannedprojectchange.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPlannedprojectchange.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPlannedprojectchange.getStatus())) {
                    plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS4);
                    plannedprojectchange.setFirstdate(new Date());
                    plannedprojectchange.setFirstperson(sysUser.getRealname());
                    plannedprojectchange.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS8);
                    plannedprojectchange.setFinishperson(sysUser.getRealname());
                    plannedprojectchange.setFinishdate(new Date());
                    plannedprojectchange.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //先查示范任务书
                    QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper();
                    queryWrapper1.eq("projectguid", projectlibraryguid);
                    queryWrapper1.ne("isdelete", 1);
                    ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper1);
                    //再查科技类任务书
                    QueryWrapper<ZgsSciencetechtask> queryWrapper2 = new QueryWrapper();
                    queryWrapper2.eq("sciencetechguid", projectlibraryguid);
                    queryWrapper2.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = null;
                    String taskguid = null;
                    int typeTask = 0;
                    Integer bgcount = null;
                    if (zgsProjecttask == null) {
                        typeTask = 1;
                        zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper2);
                        taskguid = zgsSciencetechtask.getId();
                        bgcount = zgsSciencetechtask.getBgcount();
                    } else {
                        typeTask = 2;
                        taskguid = zgsProjecttask.getId();
                        bgcount = zgsProjecttask.getBgcount();
                    }
                    //变更形审通过后，根据intype类型判断是项目终止还是项目变更
                    if ("1".equals(plannedInfo.getIntype())) {
                        if (typeTask == 1) {
                            //科技项目直接终止
                            ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
                            zgsSciencetechfeasible.setId(projectlibraryguid);
                            zgsSciencetechfeasible.setIsdealoverdue(new BigDecimal(2));
                            zgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible);
                        } else {
                            ZgsProjectlibrary zgsProjectlibrary = new ZgsProjectlibrary();
                            zgsProjectlibrary.setId(projectlibraryguid);
                            zgsProjectlibrary.setIsdealoverdue(new BigDecimal(2));
                            zgsProjectlibraryService.updateById(zgsProjectlibrary);
                        }
                    }
                    //任务书变更次数不为空，即可执行以下操作
                    if (bgcount != null) {
                        //查询是否有及时变更条件的项目
                        boolean flag = true;
                        //type=1、示范验收；2、示范证书；3、科研验收4、结题验收5、科研证书6、结题证书
                        //1、示范验收
                        ZgsDemoprojectacceptance info_1 = null;
                        //2、示范证书
                        ZgsExamplecertificatebase info_2 = null;
                        //3、科研验收
                        ZgsScientificbase info_3 = null;
                        //4、结题验收
                        ZgsScientificpostbase info_4 = null;
                        //5、科研证书
                        ZgsSacceptcertificatebase info_5 = null;
                        //6、结题证书
                        ZgsSpostcertificatebase info_6 = null;
                        if (typeTask == 1) {
                            //3、科研验收
                            QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
                            queryWrapper7.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper7.eq("projectlibraryguid", projectlibraryguid);
                            queryWrapper7.ne("isdelete", 1);
                            info_3 = zgsScientificbaseService.getOne(queryWrapper7);
                            //4、结题验收
                            QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
                            queryWrapper4.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper4.eq("scientificbaseguid", projectlibraryguid);
                            queryWrapper4.ne("isdelete", 1);
                            info_4 = zgsScientificpostbaseService.getOne(queryWrapper4);
                            //5、科研证书
                            QueryWrapper<ZgsSacceptcertificatebase> queryWrapper11 = new QueryWrapper();
                            queryWrapper11.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper11.eq("projectlibraryguid", projectlibraryguid);
                            queryWrapper11.ne("isdelete", 1);
                            info_5 = zgsSacceptcertificatebaseService.getOne(queryWrapper11);
                            //6、结题证书
                            QueryWrapper<ZgsSpostcertificatebase> queryWrapper12 = new QueryWrapper();
                            queryWrapper12.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper12.eq("projectlibraryguid", projectlibraryguid);
                            queryWrapper12.ne("isdelete", 1);
                            info_6 = zgsSpostcertificatebaseService.getOne(queryWrapper12);
                            if (info_3 == null && info_4 == null && info_5 == null && info_6 == null) {
                                //不符合条件直接跳出不执行
                                flag = false;
                            }
                        } else {
                            //1、示范验收
                            QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
                            queryWrapper72.eq("projectlibraryguid", projectlibraryguid);
                            queryWrapper72.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper72.ne("isdelete", 1);
                            info_1 = zgsDemoprojectacceptanceService.getOne(queryWrapper72);
                            //2、示范证书
                            QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
                            queryWrapper82.notInSql("status", GlobalConstants.BG_STATUS_2_8_14_15_16);
                            queryWrapper82.eq("projectlibraryguid", projectlibraryguid);
                            queryWrapper82.ne("isdelete", 1);
                            info_2 = zgsExamplecertificatebaseService.getOne(queryWrapper82);
                            if (info_1 == null && info_2 == null) {
                                //不符合条件直接跳出不执行
                                flag = false;
                            }
                        }
                        if (flag) {
                            //先查该项目所有变更记录为下面做筛选条件使用
                            QueryWrapper<ZgsPlannedprojectchange> qwd = new QueryWrapper();
                            qwd.eq("projectlibraryguid", projectlibraryguid);
                            qwd.ne("isdelete", 1);
                            List<ZgsPlannedprojectchange> listBg = this.baseMapper.selectList(qwd);
                            //查变更历史记录（多次变更按照创建日期升序循环遍历）
                            QueryWrapper<ZgsPlannedprojectchangedetail> queryWrapper = new QueryWrapper();
                            queryWrapper.and(qw -> {
                                for (int a = 0; a < listBg.size(); a++) {
                                    ZgsPlannedprojectchange pla = listBg.get(a);
                                    qw.or().eq("baseguid", pla.getId());
                                }
                            });
                            queryWrapper.ne("isdelete", 1);
                            queryWrapper.orderByAsc("createdate");
                            List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList = zgsPlannedprojectchangedetailService.list(queryWrapper);
//                        未使用（0项目名称、1立项编号、2项目承担单位、3任务书编号、9电子邮箱、10邮编）
//                        已使用（4项目责任人、5工作单位、6通信地址、7终止时间、8联系电话、11项目组成员）
                            if (zgsPlannedprojectchangedetailList.size() > 0) {
                                for (ZgsPlannedprojectchangedetail plannedprojectchangedetail : zgsPlannedprojectchangedetailList) {
                                    String itemKey = plannedprojectchangedetail.getItemKey();
                                    if ("11".equals(itemKey)) {
                                        //变更项目组成员
                                        QueryWrapper<ZgsPlannedchangeparticipant> xmzcyBg = new QueryWrapper();
                                        xmzcyBg.eq("taskguid", taskguid);
                                        xmzcyBg.orderByAsc("ordernum");
                                        List<ZgsPlannedchangeparticipant> bgList = zgsPlannedchangeparticipantService.list(xmzcyBg);
                                        try {
                                            //先删除、后添加
                                            //示范证书
                                            if (info_2 != null) {
                                                QueryWrapper<ZgsExamplecertparticipant> wd2 = new QueryWrapper<>();
                                                wd2.eq("baseguid", info_2.getId());
                                                zgsExamplecertparticipantService.remove(wd2);
                                                //循环添加
                                                ZgsExamplecertparticipant zgsExamplecertparticipant = null;
                                                for (ZgsPlannedchangeparticipant pant : bgList) {
                                                    zgsExamplecertparticipant = copy(pant, ZgsExamplecertparticipant.class);
                                                    zgsExamplecertparticipant.setName(pant.getPersonname());
                                                    zgsExamplecertparticipant.setSex(pant.getSex());
                                                    zgsExamplecertparticipant.setIdcard(pant.getIdcard());
                                                    zgsExamplecertparticipant.setTechnicaltitle(pant.getPersonpost());
                                                    zgsExamplecertparticipant.setWorkunit(pant.getUnit());
                                                    zgsExamplecertparticipant.setContribution(pant.getWorkcontent());
                                                    zgsExamplecertparticipant.setEducation(pant.getStandardOfCulture());
                                                    //
                                                    zgsExamplecertparticipant.setBaseguid(info_2.getId());
                                                    zgsExamplecertparticipant.setEnterpriseguid(info_2.getEnterpriseguid());
                                                    zgsExamplecertparticipant.setCreateaccountname(info_2.getCreateaccountname());
                                                    zgsExamplecertparticipant.setCreateusername(info_2.getCreateusername());
                                                    zgsExamplecertparticipant.setCreatedate(new Date());
                                                    //
                                                    zgsExamplecertparticipantService.save(zgsExamplecertparticipant);
                                                }
                                            }
                                            //科研验收
                                            if (info_3 != null) {
                                                QueryWrapper<ZgsScientificresearcher> wd3 = new QueryWrapper<>();
                                                wd3.eq("baseguid", info_3.getId());
                                                zgsScientificresearcherService.remove(wd3);
                                                //循环添加
                                                ZgsScientificresearcher zgsScientificresearcher = null;
                                                for (ZgsPlannedchangeparticipant pant : bgList) {
                                                    zgsScientificresearcher = copy(pant, ZgsScientificresearcher.class);
                                                    zgsScientificresearcher.setName(pant.getPersonname());
                                                    zgsScientificresearcher.setSex(pant.getSex());
                                                    zgsScientificresearcher.setIdcard(pant.getIdcard());
                                                    zgsScientificresearcher.setTechnicaltitle(pant.getPersonpost());
                                                    zgsScientificresearcher.setWorkunit(pant.getUnit());
                                                    zgsScientificresearcher.setContribution(pant.getWorkcontent());
                                                    //
                                                    zgsScientificresearcher.setBaseguid(info_3.getId());
                                                    zgsScientificresearcher.setEnterpriseguid(info_3.getEnterpriseguid());
                                                    zgsScientificresearcher.setCreateaccountname(info_3.getCreateaccountname());
                                                    zgsScientificresearcher.setCreateusername(info_3.getCreateusername());
                                                    zgsScientificresearcher.setCreatedate(new Date());
                                                    zgsScientificresearcherService.save(zgsScientificresearcher);
                                                }
                                            }
                                            //结题验收
                                            if (info_4 != null) {
                                                QueryWrapper<ZgsScientificpostresearcher> wd4 = new QueryWrapper<>();
                                                wd4.eq("baseguid", info_4.getId());
                                                zgsScientificpostresearcherService.remove(wd4);
                                                //循环添加
                                                ZgsScientificpostresearcher zgsScientificpostresearcher = null;
                                                for (ZgsPlannedchangeparticipant pant : bgList) {
                                                    zgsScientificpostresearcher = copy(pant, ZgsScientificpostresearcher.class);
                                                    zgsScientificpostresearcher.setName(pant.getPersonname());
                                                    zgsScientificpostresearcher.setSex(pant.getSex());
                                                    zgsScientificpostresearcher.setIdcard(pant.getIdcard());
                                                    zgsScientificpostresearcher.setTechnicaltitle(pant.getPersonpost());
                                                    zgsScientificpostresearcher.setWorkunit(pant.getUnit());
                                                    zgsScientificpostresearcher.setContribution(pant.getWorkcontent());
                                                    //
                                                    zgsScientificpostresearcher.setBaseguid(info_4.getId());
                                                    zgsScientificpostresearcher.setEnterpriseguid(info_4.getEnterpriseguid());
                                                    zgsScientificpostresearcher.setCreateaccountname(info_4.getCreateaccountname());
                                                    zgsScientificpostresearcher.setCreateusername(info_4.getCreateusername());
                                                    zgsScientificpostresearcher.setCreatedate(new Date());
                                                    zgsScientificpostresearcherService.save(zgsScientificpostresearcher);
                                                }
                                            }
                                            //科研证书
                                            if (info_5 != null) {
                                                QueryWrapper<ZgsSacceptcertresearcher> wd5 = new QueryWrapper<>();
                                                wd5.eq("baseguid", info_5.getId());
                                                zgsSacceptcertresearcherService.remove(wd5);
                                                //循环添加
                                                ZgsSacceptcertresearcher zgsSacceptcertresearcher = null;
                                                for (ZgsPlannedchangeparticipant pant : bgList) {
                                                    zgsSacceptcertresearcher = copy(pant, ZgsSacceptcertresearcher.class);
                                                    zgsSacceptcertresearcher.setName(pant.getPersonname());
                                                    zgsSacceptcertresearcher.setSex(pant.getSex());
                                                    zgsSacceptcertresearcher.setIdcard(pant.getIdcard());
                                                    zgsSacceptcertresearcher.setTechnicaltitle(pant.getPersonpost());
                                                    zgsSacceptcertresearcher.setWorkunit(pant.getUnit());
                                                    zgsSacceptcertresearcher.setContribution(pant.getWorkcontent());
                                                    zgsSacceptcertresearcher.setEducation(pant.getStandardOfCulture());
                                                    //
                                                    zgsSacceptcertresearcher.setBaseguid(info_5.getId());
                                                    zgsSacceptcertresearcher.setEnterpriseguid(info_5.getEnterpriseguid());
                                                    zgsSacceptcertresearcher.setCreateaccountname(info_5.getCreateaccountname());
                                                    zgsSacceptcertresearcher.setCreateusername(info_5.getCreateusername());
                                                    zgsSacceptcertresearcher.setCreatedate(new Date());
                                                    zgsSacceptcertresearcherService.save(zgsSacceptcertresearcher);
                                                }
                                            }
                                            //结题证书
                                            if (info_6 != null) {
                                                QueryWrapper<ZgsSpostresearcher> wd6 = new QueryWrapper<>();
                                                wd6.eq("baseguid", info_6.getId());
                                                zgsSpostresearcherService.remove(wd6);
                                                //循环添加
                                                ZgsSpostresearcher zgsSpostresearcher = null;
                                                for (ZgsPlannedchangeparticipant pant : bgList) {
                                                    zgsSpostresearcher = copy(pant, ZgsSpostresearcher.class);
                                                    zgsSpostresearcher.setName(pant.getPersonname());
                                                    zgsSpostresearcher.setSex(pant.getSex());
                                                    zgsSpostresearcher.setIdcard(pant.getIdcard());
                                                    zgsSpostresearcher.setTechnicaltitle(pant.getPersonpost());
                                                    zgsSpostresearcher.setWorkunit(pant.getUnit());
                                                    zgsSpostresearcher.setContribution(pant.getWorkcontent());
                                                    zgsSpostresearcher.setEducation(pant.getStandardOfCulture());
                                                    //
                                                    zgsSpostresearcher.setBaseguid(info_6.getId());
                                                    zgsSpostresearcher.setEnterpriseguid(info_6.getEnterpriseguid());
                                                    zgsSpostresearcher.setCreateaccountname(info_6.getCreateaccountname());
                                                    zgsSpostresearcher.setCreateusername(info_6.getCreateusername());
                                                    zgsSpostresearcher.setCreatedate(new Date());
                                                    zgsSpostresearcherService.save(zgsSpostresearcher);
                                                }
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        ZgsDemoprojectacceptance up_info1 = new ZgsDemoprojectacceptance();
                                        ZgsExamplecertificatebase up_info2 = new ZgsExamplecertificatebase();
                                        ZgsScientificbase up_info3 = new ZgsScientificbase();
                                        ZgsScientificpostbase up_info4 = new ZgsScientificpostbase();
                                        ZgsSacceptcertificatebase up_info5 = new ZgsSacceptcertificatebase();
                                        ZgsSpostcertificatebase up_info6 = new ZgsSpostcertificatebase();
                                        if ("4".equals(itemKey)) {
                                            //项目负责人
                                            if (info_1 != null) {
                                                up_info1.setId(info_1.getId());
                                                up_info1.setLinkman(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_2 != null) {
                                                up_info2.setId(info_2.getId());
                                                up_info2.setLinkman(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_3 != null) {
                                                up_info3.setId(info_3.getId());
                                                up_info3.setApplylinkman(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_4 != null) {
                                                up_info4.setId(info_4.getId());
                                                up_info4.setApplylinkman(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_5 != null) {
                                                up_info5.setId(info_5.getId());
                                                up_info5.setPrincipalname(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_6 != null) {
                                                up_info6.setId(info_6.getId());
                                                up_info6.setPrincipalname(plannedprojectchangedetail.getChangecontent());
                                            }
                                        } else if ("5".equals(itemKey)) {
                                            //工作单位
                                            if (info_1 != null) {
                                                up_info1.setId(info_1.getId());
                                                up_info1.setImplementunit(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_2 != null) {
                                                up_info2.setId(info_2.getId());
                                                up_info2.setImplementunit(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_3 != null) {
                                                up_info3.setId(info_3.getId());
                                                up_info3.setApplyenterprisename(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_4 != null) {
                                                up_info4.setId(info_4.getId());
                                                up_info4.setApplyenterprisename(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_5 != null) {
                                                up_info5.setId(info_5.getId());
                                                up_info5.setFinishenterprisename(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_6 != null) {
                                                up_info6.setId(info_6.getId());
                                                up_info6.setFinishenterprisename(plannedprojectchangedetail.getChangecontent());
                                            }
                                        } else if ("6".equals(itemKey)) {
                                            //通信地址
                                            if (info_1 != null) {
                                                up_info1.setId(info_1.getId());
                                                up_info1.setAddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_2 != null) {
                                                up_info2.setId(info_2.getId());
                                                up_info2.setPostaddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_3 != null) {
                                                up_info3.setId(info_3.getId());
                                                up_info3.setApplyaddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_4 != null) {
                                                up_info4.setId(info_4.getId());
                                                up_info4.setApplyaddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_5 != null) {
                                                up_info5.setId(info_5.getId());
                                                up_info5.setApplyaddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_6 != null) {
                                                up_info6.setId(info_6.getId());
                                                up_info6.setApplyaddress(plannedprojectchangedetail.getChangecontent());
                                            }
                                        } else if ("7".equals(itemKey)) {
                                            //终止时间
                                            try {
                                                if (info_1 != null) {
                                                    up_info1.setId(info_1.getId());
                                                    up_info1.setProjectenddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                                }
                                                if (info_2 != null) {
                                                    up_info2.setId(info_2.getId());
                                                    up_info2.setEnddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                                }
                                                if (info_3 != null) {
                                                    up_info3.setId(info_3.getId());
                                                    up_info3.setEnddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                                }
                                                if (info_4 != null) {
                                                    up_info4.setId(info_4.getId());
                                                    up_info4.setEnddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                                }
                                                if (info_5 != null) {
                                                    //科研证书无终止时间
                                                }
                                                if (info_6 != null) {
                                                    //结题证书无终止时间
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                        } else if ("8".equals(itemKey)) {
                                            //联系电话
                                            if (info_1 != null) {
                                                up_info1.setId(info_1.getId());
                                                up_info1.setLinkphone(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_2 != null) {
                                                up_info2.setId(info_2.getId());
                                                up_info2.setLinktel(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_3 != null) {
                                                up_info3.setId(info_3.getId());
                                                up_info3.setApplyphone(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_4 != null) {
                                                up_info4.setId(info_4.getId());
                                                up_info4.setApplyphone(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_5 != null) {
                                                up_info5.setId(info_5.getId());
                                                up_info5.setPrincipalphone(plannedprojectchangedetail.getChangecontent());
                                            }
                                            if (info_6 != null) {
                                                up_info6.setId(info_6.getId());
                                                up_info6.setPrincipalphone(plannedprojectchangedetail.getChangecontent());
                                            }
                                        }
                                        if (typeTask == 1) {
                                            if (info_3 != null) {
                                                zgsScientificbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info3, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                            if (info_4 != null) {
                                                zgsScientificpostbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info4, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                            if (info_5 != null) {
                                                zgsSacceptcertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info5, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                            if (info_6 != null) {
                                                zgsSpostcertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info6, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                        } else {
                                            if (info_1 != null) {
                                                zgsDemoprojectacceptanceService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info1, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                            if (info_2 != null) {
                                                zgsExamplecertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(up_info2, ValidateEncryptEntityUtil.isEncrypt));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                plannedprojectchange.setAuditopinion(zgsPlannedprojectchange.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPlannedprojectchange.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPlannedprojectchange.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPlannedprojectchange.getStatus())) {
                    if (zgsPlannedprojectchange.getSpStatus() == 2) {
                        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    plannedprojectchange.setFirstdate(new Date());
                    plannedprojectchange.setFirstperson(sysUser.getRealname());
                    plannedprojectchange.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsPlannedprojectchange.getSpStatus() == 2) {
                        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    plannedprojectchange.setFinishperson(sysUser.getRealname());
                    plannedprojectchange.setFinishdate(new Date());
                    plannedprojectchange.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsPlannedprojectchange.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        updateProjectIsDealOverdue(plannedInfo.getProjectlibraryguid(), plannedInfo.getEnterpriseguid(), zgsPlannedprojectchange.getAuditopinion());
                    }
                }
                if (zgsPlannedprojectchange.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsPlannedprojectchange.getAuditopinion())) {
                        zgsPlannedprojectchange.setAuditopinion(" ");
                    }
                    plannedprojectchange.setAuditopinion(zgsPlannedprojectchange.getAuditopinion());
                    plannedprojectchange.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsPlannedprojectchange.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    plannedprojectchange.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(projectlibraryguid);
            zgsReturnrecord.setProjectstage("计划项目变更");
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(plannedprojectchange, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }

    @Override
    public int updateProjectIsDealOverdue(String projectlibraryguid, String enterpriseguid, String auditopinion) {
        ZgsProjectlibrary zgsProjectlibrary = zgsProjectlibraryService.getById(projectlibraryguid);
        ZgsPlannedprojectchange plannedprojectchange = new ZgsPlannedprojectchange();
        if (zgsProjectlibrary == null) {
//            ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(projectlibraryguid);
            //科技项目
            ZgsSciencetechfeasible sciencetechfeasible = new ZgsSciencetechfeasible();
            sciencetechfeasible.setId(projectlibraryguid);
            sciencetechfeasible.setIsdealoverdue(new BigDecimal(2));
            zgsSciencetechfeasibleService.updateById(sciencetechfeasible);
            //基本信息
            QueryWrapper<ZgsSciencetechtask> queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("sciencetechguid", projectlibraryguid);
            queryWrapper1.ne("isdelete", 1);
            ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper1);
            //开始
            plannedprojectchange.setProjectname(zgsSciencetechtask.getProjectname());
            plannedprojectchange.setSetupnum(zgsSciencetechtask.getProjectnum());
            plannedprojectchange.setResearchstartdate(zgsSciencetechtask.getStartdate());
            plannedprojectchange.setResearchenddate(zgsSciencetechtask.getEnddate());
            plannedprojectchange.setProjectbearunit(zgsSciencetechtask.getHeadunit());
            plannedprojectchange.setProjectleader(zgsSciencetechtask.getProjectleader());
            plannedprojectchange.setLinkphone(zgsSciencetechtask.getHeadmobile());
            plannedprojectchange.setWorkunit(zgsSciencetechtask.getCommitmentunit());
            plannedprojectchange.setEmail(zgsSciencetechtask.getHeademail());
            plannedprojectchange.setAddress(zgsSciencetechtask.getUnitaddress());
            plannedprojectchange.setZipcode(zgsSciencetechtask.getZipcode());
        } else {
            //示范项目
            ZgsProjectlibrary projectlibrary = new ZgsProjectlibrary();
            projectlibrary.setId(projectlibraryguid);
            projectlibrary.setIsdealoverdue(new BigDecimal(2));
            zgsProjectlibraryService.updateById(projectlibrary);
            //基本信息
            QueryWrapper<ZgsProjecttask> queryWrapper2 = new QueryWrapper();
            queryWrapper2.eq("projectguid", projectlibraryguid);
            queryWrapper2.ne("isdelete", 1);
            ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper2);
            //开始
            plannedprojectchange.setProjectname(zgsProjectlibrary.getProjectname());
            plannedprojectchange.setSetupnum(zgsProjectlibrary.getProjectnum());
            plannedprojectchange.setWorkunit(zgsProjectlibrary.getApplyunit());
            plannedprojectchange.setProjectleader(zgsProjectlibrary.getApplyunitprojectleader());
            plannedprojectchange.setResearchstartdate(zgsProjectlibrary.getProjectstartdate());
            plannedprojectchange.setResearchenddate(zgsProjectlibrary.getProjectenddate());
            plannedprojectchange.setProjectbearunit(zgsProjecttask.getHeadunit());
            plannedprojectchange.setLinkphone(zgsProjecttask.getHeadmobile());
            plannedprojectchange.setEmail(zgsProjecttask.getHeademail());
            plannedprojectchange.setAddress(zgsProjecttask.getUnitaddress());
            plannedprojectchange.setZipcode(zgsProjecttask.getZipcode());
        }
        //新增项目库终止数据
        String id = UUID.randomUUID().toString();
        plannedprojectchange.setId(id);
//        plannedprojectchange.setApplydate(new Date());
        plannedprojectchange.setCreatedate(new Date());
        plannedprojectchange.setEnterpriseguid(enterpriseguid);
        plannedprojectchange.setProjectlibraryguid(projectlibraryguid);
        plannedprojectchange.setStatus(GlobalConstants.SHENHE_STATUS16);
        plannedprojectchange.setIntype(GlobalConstants.IN_TYPE_1);
        //
        plannedprojectchange.setClosetype(GlobalConstants.CLOSE_TYPE_2);
        this.baseMapper.insert(plannedprojectchange);
        //添加终止原因
        ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail = new ZgsPlannedprojectchangedetail();
        zgsPlannedprojectchangedetail.setId(UUID.randomUUID().toString());
        zgsPlannedprojectchangedetail.setOrdernum(new BigDecimal(1));
        zgsPlannedprojectchangedetail.setBaseguid(id);
        zgsPlannedprojectchangedetail.setCreatedate(new Date());
        zgsPlannedprojectchangedetail.setChangecontent("省厅终止");
        zgsPlannedprojectchangedetail.setChangereason(auditopinion);
        zgsPlannedprojectchangedetailService.save(zgsPlannedprojectchangedetail);
        return 0;
    }

    public static <T> T copy(Object ob, Class<T> clazz) {
        String oldOb = JSON.toJSONString(ob);
        return JSON.parseObject(oldOb, clazz);
    }
}
