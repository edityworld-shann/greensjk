package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsProjectlibraryexpert;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;

import java.util.List;

/**
 * @Description: 项目专家关系信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
public interface IZgsProjectlibraryexpertService extends IService<ZgsProjectlibraryexpert> {
    List<ZgsProjectlibraryexpert> getZgsProjectlibraryexpertList(String businessguid);
}
