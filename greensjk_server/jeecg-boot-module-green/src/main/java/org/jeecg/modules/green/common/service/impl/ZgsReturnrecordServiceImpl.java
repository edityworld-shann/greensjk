package org.jeecg.modules.green.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.mapper.ZgsReturnrecordMapper;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 退回记录信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Service
public class ZgsReturnrecordServiceImpl extends ServiceImpl<ZgsReturnrecordMapper, ZgsReturnrecord> implements IZgsReturnrecordService {

    @Override
    public void deleteReturnRecordLastLog(String businessguid) {
        QueryWrapper<ZgsReturnrecord> queryWrapper = new QueryWrapper();
        queryWrapper.eq("businessguid", businessguid);
        queryWrapper.and(qw -> {
            qw.eq("returnstep", GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24).or().eq("returnstep", GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
        });
        queryWrapper.orderByDesc("createtime");
        queryWrapper.last(" limit 1");
        //形审或终审
        List<ZgsReturnrecord> list = this.baseMapper.selectList(queryWrapper);
        if (list.size() > 0) {
            ZgsReturnrecord returnRecord = list.get(0);
            if (returnRecord != null) {
                //假删除
                this.baseMapper.updateById(returnRecord.setIsdelete(new BigDecimal(1)));
                //真删除
//            this.baseMapper.deleteById(returnRecord.getId());
            }
        }
    }

    @Override
    public void deleteReturnRecordLastLogByLx(String businessguid) {
        QueryWrapper<ZgsReturnrecord> queryWrapper = new QueryWrapper();
        queryWrapper.eq("businessguid", businessguid);
        queryWrapper.eq("returnstep", GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
        queryWrapper.orderByDesc("createtime");
        queryWrapper.last(" limit 1");
        //立项
        List<ZgsReturnrecord> list = this.baseMapper.selectList(queryWrapper);
        if (list.size() > 0) {
            ZgsReturnrecord returnRecord = list.get(0);
            if (returnRecord != null) {
                //假删除
                this.baseMapper.updateById(returnRecord.setIsdelete(new BigDecimal(1)));
            }
        }
    }
}
