package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 黑名单历史记录
 * @Author: jeecg-boot
 * @Date: 2022-03-06
 * @Version: V1.0
 */
@Data
@TableName("zgs_projectunithistory")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectunithistory对象", description = "黑名单历史记录")
public class ZgsProjectunithistory implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private java.lang.String id;
    /**
     * 黑名单认定证据内容
     */
    @Excel(name = "黑名单认定证据内容", width = 15)
    @ApiModelProperty(value = "黑名单认定证据内容")
    private java.lang.String blackContent;
    /**
     * 处罚文件地址
     */
    @Excel(name = "处罚文件地址", width = 15)
    @ApiModelProperty(value = "处罚文件地址")
    private java.lang.String blackUrl;
    /**
     * 创建人登录名称
     */
    @ApiModelProperty(value = "创建人登录名称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createBy;
    /**
     * 创建日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
    /**
     * 更新人登录名称
     */
    @ApiModelProperty(value = "更新人登录名称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String updateBy;
    /**
     * 更新日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**
     * 认定部门
     */
    @Excel(name = "认定部门", width = 15)
    @ApiModelProperty(value = "认定部门")
    private java.lang.String blackOrg;
    /**
     * 黑名单类型
     */
    @Excel(name = "黑名单类型", width = 15)
    @ApiModelProperty(value = "黑名单类型")
    private java.lang.String type;
    /**
     * 账号id
     */
    @Excel(name = "账号id", width = 15)
    @ApiModelProperty(value = "账号id")
    private java.lang.String userid;
    @TableField(exist = false)
    private Integer status;
    @TableField(exist = false)
    @ApiModelProperty(value = "下载地址")
    private java.lang.String downUrl;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
