package org.jeecg.modules.green.task.service;

import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 任务书项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface IZgsTaskbuildfundbudgetService extends IService<ZgsTaskbuildfundbudget> {

}
