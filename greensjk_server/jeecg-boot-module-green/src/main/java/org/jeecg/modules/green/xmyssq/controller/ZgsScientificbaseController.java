package org.jeecg.modules.green.xmyssq.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.xmyssq.entity.*;
import org.jeecg.modules.green.xmyssq.service.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.xmyssq.service.impl.ZgsScientificresearcherServiceImpl;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科研项目申报验收基本信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-13
 * @Version: V1.0
 */
@Api(tags = "科研项目申报验收基本信息表")
@RestController
@RequestMapping("/xmyssq/zgsScientificbase")
@Slf4j
public class ZgsScientificbaseController extends JeecgController<ZgsScientificbase, IZgsScientificbaseService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsScientificpexecutivelistService zgsScientificpexecutivelistService;
    @Autowired
    private IZgsScientificprojectexecutiveService zgsScientificprojectexecutiveService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;
    @Autowired
    private IZgsScientificresearcherService zgsScientificresearcherService;
    @Autowired
    private IZgsScientificexpenditureclearService zgsScientificexpenditureclearService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsSaexpertService zgsSaexpertService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;


    /**
     * 分页列表查询
     *
     * @param zgsScientificbase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-分页列表查询")
    @ApiOperation(value = "科研项目申报验收基本信息表-分页列表查询", notes = "科研项目申报验收基本信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsScientificbase zgsScientificbase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsScientificbase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsScientificbase.getProjectname())) {
            queryWrapper.like("projectname", zgsScientificbase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsScientificbase.getProjectnumber())) {
            queryWrapper.like("projectnumber", zgsScientificbase.getProjectnumber());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnumber,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsScientificbase.getProjectstage())) {
            if (!"-1".equals(zgsScientificbase.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsScientificbase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                if (StringUtils.isNotEmpty(zgsScientificbase.getStatus())) {
                    if (!"0".equals(zgsScientificbase.getStatus())) {
                        queryWrapper.and(qrt -> {
                            qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42)).or().isNull("projectstage");
                        });
                    }
                } else {
                    queryWrapper.and(qrt -> {
                        qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42)).or().isNull("projectstage");
                    });
                }
            }
        }
//        QueryWrapperUtil.initScientificbaseSelect(queryWrapper, zgsScientificbase.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsScientificbase.getStatus(), sysUser, 1);
        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    QueryWrapper<ZgsSaexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
                    zgsSofttechexpertQueryWrapper.eq("expertguid", expertId);
                    zgsSofttechexpertQueryWrapper.ne("isdelete", 1);
//                    zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSofttechexpertQueryWrapper);
                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
                        queryWrapper.and(qw -> {
                            qw.and(w1 -> {
                                for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
                                    w1.or(w2 -> {
                                        w2.eq("id", zgsSaexpert.getBusinessguid());
                                    });
                                }
                            });
                        });
                    } else {
                        queryWrapper.eq("id", "null");
                    }
                }
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsScientificbase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("enddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
            // queryWrapper.last("ORDER BY IF(isnull(applydate),0,1), applydate DESC");
        }
        Page<ZgsScientificbase> page = new Page<ZgsScientificbase>(pageNo, pageSize);
        List<ZgsScientificbase> ls = new ArrayList<>();
        // IPage<ZgsScientificbase> pageList = zgsScientificbaseService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsScientificbase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsScientificbase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsScientificbase> pageList = null;

        // 示范项目任务书
        redisUtil.expire("kyxmys",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmysOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmysOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsScientificbase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsScientificbase.getDspForTjdw())) {
            if (zgsScientificbase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("kyxmys")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsScientificbaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmys",pageListForProjectCount.getTotal());
                }
            }
            if (zgsScientificbase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("kyxmysOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsScientificbaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmysOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsScientificbase.getDspForTjdw()) && StringUtils.isBlank(zgsScientificbase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsScientificbaseService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsScientificbase.getDspForTjdw()) && StringUtils.isNotBlank(zgsScientificbase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsScientificbaseService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsScientificbase.getDspForSt()) && zgsScientificbase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsScientificbaseService.page(page, queryWrapper);
            redisUtil.set("kyxmysOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsScientificbase.getStatus(), sysUser, 1);
            pageList = zgsScientificbaseService.page(page, queryWrapper);
        }

        /*// 科技项目验收
        redisUtil.expire("kyxmys",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmys")) {
            if (StringUtils.isNotBlank(zgsScientificbase.getFlagByWorkTable()) && zgsScientificbase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("kyxmys",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmysOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmysOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsScientificbase.getDspForSt()) && zgsScientificbase.getDspForSt().equals("2")) {
                redisUtil.set("kyxmysOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmysOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmysOfTjdw")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsScientificbase.getDspForTjdw()) && zgsScientificbase.getDspForTjdw().equals("3")) {
                redisUtil.set("kyxmysOfTjdw",pageList.getTotal());
            }
        }*/
        Set<String> set = new HashSet<>(); // 去掉重复数据
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsScientificbase scientificbase = pageList.getRecords().get(m);
                    if (!set.contains(scientificbase.getProjectlibraryguid())||scientificbase.getProjectlibraryguid()==null || StringUtils.isBlank(scientificbase.getProjectlibraryguid())) {
                        if (StringUtils.isNotBlank(scientificbase.getProjectlibraryguid()) && scientificbase.getProjectlibraryguid() != null) {
                            set.add(scientificbase.getProjectlibraryguid());
                        }
                        pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(scientificbase, ValidateEncryptEntityUtil.isDecrypt));
//                    if (StringUtils.isNotEmpty(scientificbase.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(scientificbase.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收管理阶段");
//                    }
                        if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                            pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
                        }
                        if (sysUser.getEnterpriseguid().equals(scientificbase.getEnterpriseguid())) {
                            //作为上报本单位
                            pageList.getRecords().get(m).setSpStatus(0);
                        } else {
                            //作为审批本单位
                            //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                            if (GlobalConstants.SHENHE_STATUS1.equals(scientificbase.getStatus())
                                    || GlobalConstants.SHENHE_STATUS5.equals(scientificbase.getStatus())
                                    || GlobalConstants.SHENHE_STATUS9.equals(scientificbase.getStatus())
                                    || GlobalConstants.SHENHE_STATUS14.equals(scientificbase.getStatus())
                                    || GlobalConstants.SHENHE_STATUS15.equals(scientificbase.getStatus())
                            ) {
                                pageList.getRecords().get(m).setSpStatus(1);
                            }
                        }
                        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                            //专家审批
                            QueryWrapper<ZgsSaexpert> wrapper = new QueryWrapper();
                            wrapper.eq("expertguid", expertId);
                            wrapper.eq("businessguid", scientificbase.getId());
                            ZgsSaexpert saexpert = zgsSaexpertService.getOne(wrapper);
                            if (!(saexpert != null && StringUtils.isNotEmpty(saexpert.getAuditconclusionnum()))) {
                                pageList.getRecords().get(m).setSpStatus(2);
                            }
                        }
                        ls.add(scientificbase);
                    }

                    // 如果当前登录角色为个人或单位时 根据授权标识判断是否可进行编辑操作
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                        if (StrUtil.isNotBlank(scientificbase.getSqbj()) && scientificbase.getSqbj().equals("1")) {
                            scientificbase.setUpdateFlag("1");   // 可编辑
                            scientificbase.setSqbjBol(false);
                        } else if (StrUtil.isNotBlank(scientificbase.getSqbj()) && scientificbase.getSqbj().equals("2")) {
                            scientificbase.setUpdateFlag("2");  // 取消编辑按钮
                            scientificbase.setSqbjBol(true);
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(scientificbase.getUpdateSave()) && scientificbase.getUpdateSave().equals("1")) {  // 修改未提交
                        scientificbase.setBjzt("1");
                    } else if (StrUtil.isNotBlank(scientificbase.getUpdateSave()) && scientificbase.getUpdateSave().equals("2")) {  // 修改已提交
                        scientificbase.setBjzt("2");
                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {

                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsScientificbase scientificbase = pageList.getRecords().get(m);
                    //解密
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(scientificbase, ValidateEncryptEntityUtil.isDecrypt));

                    if (!set.contains(scientificbase.getProjectlibraryguid())||scientificbase.getProjectlibraryguid()==null || StringUtils.isBlank(scientificbase.getProjectlibraryguid())) {
                        if (StringUtils.isNotBlank(scientificbase.getProjectlibraryguid()) && scientificbase.getProjectlibraryguid() != null) {
                            set.add(scientificbase.getProjectlibraryguid());
                        }
//                    if (StringUtils.isNotEmpty(scientificbase.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(scientificbase.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收管理阶段");
//                    }
                        if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                            pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
                        }
//                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(scientificbase.getFirstdate());
                        if (GlobalConstants.SHENHE_STATUS4.equals(scientificbase.getStatus())) {
                            //形审
                            pageList.getRecords().get(m).setSpStatus(4);
                        } else if (GlobalConstants.SHENHE_STATUS8.equals(scientificbase.getStatus())) {
                            //分配专家
//                        QueryWrapper<ZgsSaexpert> zgsSaexpertQueryWrapper = new QueryWrapper();
//                        zgsSaexpertQueryWrapper.eq("businessguid", scientificbase.getId());
//                        zgsSaexpertQueryWrapper.ne("isdelete", 1);
//                        List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSaexpertQueryWrapper);
//                        if (!(zgsSaexpertList != null && zgsSaexpertList.size() > 0)) {
//                            //专家分配
//                            Calendar calendar = new GregorianCalendar();
//                            calendar.setTime(new Date());
//                            calendar.add(calendar.DATE, -15);//15天前未分配的还可以继续分配，超了15天分配按钮消失
//                            if (calendar.getTime().compareTo(scientificbase.getApplydate()) < 0) {
//                                pageList.getRecords().get(m).setSpStatus(3);
//                            }
//                        }
                        }
                        //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                        QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                        queryWrapper0.eq("sciencetechguid", scientificbase.getProjectlibraryguid());
                        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper0.ne("isdelete", 1);
                        ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                        if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                        } else {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                        }
                        ls.add(scientificbase);
                    }

                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(scientificbase.getUpdateSave()) && scientificbase.getUpdateSave().equals("1")) {  // 修改未提交
                        scientificbase.setBjzt("1");
                    } else if (StrUtil.isNotBlank(scientificbase.getUpdateSave()) && scientificbase.getUpdateSave().equals("2")) {  //  修改已提交
                        scientificbase.setBjzt("2");
                    }

                    if (StrUtil.isNotBlank(scientificbase.getSqbj()) && scientificbase.getSqbj().equals("1")) {  // 授权编辑
                        scientificbase.setSqbjBol(false);
                        scientificbase.setUpdateFlag("1");
                    } else if (StrUtil.isNotBlank(scientificbase.getSqbj()) && scientificbase.getSqbj().equals("2")) {  // 取消编辑
                        scientificbase.setSqbjBol(true);
                        scientificbase.setUpdateFlag("2");
                    }

                }
            }
        }
        pageList.setRecords(ls);  // 处理重复数据，重新添加返回
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsScientificbase
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-添加")
    @ApiOperation(value = "科研项目申报验收基本信息表-添加", notes = "科研项目申报验收基本信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsScientificbase zgsScientificbase) {
        // 项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsScientificbase.setId(id);
        zgsScientificbase.setEnterpriseguid(sysUser.getEnterpriseguid());
        if (GlobalConstants.handleSubmit.equals(zgsScientificbase.getStatus())) {
            //保存并上报
            zgsScientificbase.setApplydate(new Date());
        }
        zgsScientificbase.setCreateaccountname(sysUser.getUsername());
        zgsScientificbase.setCreateusername(sysUser.getRealname());
        zgsScientificbase.setCreatedate(new Date());
        //判断项目终止
//        QueryWrapper<ZgsSciencetechtask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("sciencetechguid", zgsScientificbase.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsSciencetechtask> listHist = zgsSciencetechtaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsScientificbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
        zgsScientificbase.setProjectstagestatus(zgsScientificbase.getStatus());
        zgsScientificbaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificbase, ValidateEncryptEntityUtil.isEncrypt));

        //实时更新阶段和状态（原有更新逻辑）
        if (GlobalConstants.handleSubmit.equals(zgsScientificbase.getStatus()) && StringUtils.isNotEmpty(zgsScientificbase.getProjectlibraryguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsScientificbase.getProjectlibraryguid());
        }

//        redisUtil.set(zgsScientificbase.getProjectlibraryguid(), "验收管理阶段");
        //科研项目验收申请项目执行情况评价奖励列表
        List<ZgsScientificpexecutivelist> zgsScientificpexecutivelistList = zgsScientificbase.getZgsScientificpexecutivelistList();
        if (zgsScientificpexecutivelistList != null && zgsScientificpexecutivelistList.size() > 0) {
            for (int a0 = 0; a0 < zgsScientificpexecutivelistList.size(); a0++) {
                ZgsScientificpexecutivelist zgsScientificpexecutivelist = zgsScientificpexecutivelistList.get(a0);
                zgsScientificpexecutivelist.setId(UUID.randomUUID().toString());
                zgsScientificpexecutivelist.setBaseguid(id);
                zgsScientificpexecutivelist.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificpexecutivelist.setCreateaccountname(sysUser.getUsername());
                zgsScientificpexecutivelist.setCreateusername(sysUser.getRealname());
                zgsScientificpexecutivelist.setCreatedate(new Date());
                zgsScientificpexecutivelistService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpexecutivelist, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //科研项目验收申请项目执行情况评价实体类
        ZgsScientificprojectexecutive zgsScientificprojectexecutive = zgsScientificbase.getZgsScientificprojectexecutive();
        zgsScientificprojectexecutive.setId(UUID.randomUUID().toString());
        zgsScientificprojectexecutive.setBaseguid(id);
        zgsScientificprojectexecutive.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsScientificprojectexecutive.setCreateaccountname(sysUser.getUsername());
        zgsScientificprojectexecutive.setCreateusername(sysUser.getRealname());
        zgsScientificprojectexecutive.setCreatedate(new Date());
        zgsScientificprojectexecutiveService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectexecutive, ValidateEncryptEntityUtil.isEncrypt));
        //主要研究人员名单
        List<ZgsScientificresearcher> zgsScientificresearcherList = zgsScientificbase.getZgsScientificresearcherList();
        if (zgsScientificresearcherList != null && zgsScientificresearcherList.size() > 0) {
            if (zgsScientificresearcherList.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a1 = 0; a1 < zgsScientificresearcherList.size(); a1++) {
                ZgsScientificresearcher ZgsScientificresearcher = zgsScientificresearcherList.get(a1);
                ZgsScientificresearcher.setId(UUID.randomUUID().toString());
                ZgsScientificresearcher.setBaseguid(id);
                ZgsScientificresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                ZgsScientificresearcher.setCreateaccountname(sysUser.getUsername());
                ZgsScientificresearcher.setCreateusername(sysUser.getRealname());
                ZgsScientificresearcher.setCreatedate(new Date());
                zgsScientificresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(ZgsScientificresearcher, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //项目完成单位情况  因申报阶段 项目完成单位信息已填写，验收阶段将不在进行新增或编辑 操作   rxl 20230809
        /*List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificbase.getZgsScientificprojectfinishList();
        if (zgsScientificprojectfinishList != null && zgsScientificprojectfinishList.size() > 0) {
            if (zgsScientificprojectfinishList.size() > 5) {
                return Result.error("项目完成单位情况不得超过5个！");
            }
            for (int a2 = 0; a2 < zgsScientificprojectfinishList.size(); a2++) {
                ZgsScientificprojectfinish zgsScientificprojectfinish = zgsScientificprojectfinishList.get(a2);
                zgsScientificprojectfinish.setId(UUID.randomUUID().toString());
                zgsScientificprojectfinish.setBaseguid(id);
                zgsScientificprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificprojectfinish.setCreateaccountname(sysUser.getUsername());
                zgsScientificprojectfinish.setCreateusername(sysUser.getRealname());
                zgsScientificprojectfinish.setCreatedate(new Date());
                zgsScientificprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
            }
        }*/

        //科研项目验收申请经费决算
        ZgsScientificexpenditureclear zgsScientificexpenditureclear = zgsScientificbase.getZgsScientificexpenditureclear();
        zgsScientificexpenditureclear.setId(UUID.randomUUID().toString());
        zgsScientificexpenditureclear.setBaseguid(id);
        zgsScientificexpenditureclear.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsScientificexpenditureclear.setCreateaccountname(sysUser.getUsername());
        zgsScientificexpenditureclear.setCreateusername(sysUser.getRealname());
        zgsScientificexpenditureclear.setCreatedate(new Date());
        zgsScientificexpenditureclearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificexpenditureclear, ValidateEncryptEntityUtil.isEncrypt));
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsScientificbase.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.ScientificAcceptance);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 撤回
     *
     * @param zgsScientificbase
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-撤回")
    @ApiOperation(value = "科研项目申报验收基本信息表-撤回", notes = "科研项目申报验收基本信息表-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsScientificbase zgsScientificbase) {
        if (zgsScientificbase != null) {
            String id = zgsScientificbase.getId();
            String bid = zgsScientificbase.getProjectlibraryguid();
            String status = zgsScientificbase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42).equals(zgsScientificbase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsScientificbase info = new ZgsScientificbase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsScientificbaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsScientificbase
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-审批")
    @ApiOperation(value = "科研项目申报验收基本信息表-审批", notes = "科研项目申报验收基本信息表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsScientificbase zgsScientificbase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsScientificbase != null && StringUtils.isNotEmpty(zgsScientificbase.getId())) {
            ZgsScientificbase scientificbase = new ZgsScientificbase();
            scientificbase.setId(zgsScientificbase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsScientificbase.getId());
            zgsReturnrecord.setReturnreason(zgsScientificbase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE12);
            ZgsScientificbase baseInfo = zgsScientificbaseService.getById(zgsScientificbase.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsScientificbase.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                scientificbase.setAuditopinion(null);
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsScientificbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsScientificbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsScientificbase.getStatus())) {
                    scientificbase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    scientificbase.setFirstdate(new Date());
                    scientificbase.setFirstperson(sysUser.getRealname());
                    scientificbase.setFirstdepartment(sysUser.getEnterprisename());
                    scientificbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    scientificbase.setStatus(GlobalConstants.SHENHE_STATUS8);
                    scientificbase.setFinishperson(sysUser.getRealname());
                    scientificbase.setFinishdate(new Date());
                    scientificbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审结束查下是否有旧的专家评审历史记录
//                    QueryWrapper<ZgsSaexpert> saexpertQueryWrapper = new QueryWrapper();
//                    saexpertQueryWrapper.eq("businessguid", zgsScientificbase.getId());
//                    saexpertQueryWrapper.ne("isdelete", 1);
//                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(saexpertQueryWrapper);
//                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
//                        for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
//                            ZgsSaexpert zgsSaexpert1 = new ZgsSaexpert();
//                            zgsSaexpert1.setId(zgsSaexpert.getId());
//                            zgsSaexpert1.setIsdelete(new BigDecimal(1));
//                            zgsSaexpertService.updateById(zgsSaexpert1);
//                        }
//                    }
                    //
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                scientificbase.setAuditopinion(zgsScientificbase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsScientificbase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsScientificbase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsScientificbase.getStatus())) {
                    if (zgsScientificbase.getSpStatus() == 2) {
                        scientificbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        scientificbase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    scientificbase.setFirstdate(new Date());
                    scientificbase.setFirstperson(sysUser.getRealname());
                    scientificbase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsScientificbase.getSpStatus() == 2) {
                        scientificbase.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        scientificbase.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    scientificbase.setFinishperson(sysUser.getRealname());
                    scientificbase.setFinishdate(new Date());
                    scientificbase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsScientificbase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        scientificbase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getProjectlibraryguid(), baseInfo.getEnterpriseguid(), zgsScientificbase.getAuditopinion());
                    }
                }
                if (zgsScientificbase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsScientificbase.getAuditopinion())) {
                        zgsScientificbase.setAuditopinion(" ");
                    }
                    scientificbase.setAuditopinion(zgsScientificbase.getAuditopinion());
                    scientificbase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsScientificbase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    scientificbase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            zgsScientificbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(scientificbase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            zgsProjectlibraryService.initStageAndStatus(baseInfo.getProjectlibraryguid());
        }
        return Result.OK("审批成功！");
    }

    /**
     * 编辑
     *
     * @param zgsScientificbase
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-编辑")
    @ApiOperation(value = "科研项目申报验收基本信息表-编辑", notes = "科研项目申报验收基本信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsScientificbase zgsScientificbase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsScientificbase != null && StringUtils.isNotEmpty(zgsScientificbase.getId())) {
            ZgsScientificbase entity = zgsScientificbaseService.getById(zgsScientificbase.getId());
            if (ObjectUtil.isNotNull(entity)) {
                if (StringUtils.isNotBlank(entity.getSqbj()) && entity.getSqbj().equals("2")) {
                    Result result = new Result();

                    result.setMessage("授权编辑已被取消，已修改信息不能保存");
                    result.setCode(0);
                    result.setSuccess(false);
                    return result;

                } else {
                    String id = zgsScientificbase.getId();
                    if (GlobalConstants.handleSubmit.equals(zgsScientificbase.getStatus())) {
                        //保存并上报
                        zgsScientificbase.setApplydate(new Date());
                        zgsScientificbase.setAuditopinion(null);
//                zgsScientificbase.setReturntype(null);
                    } else {
                        zgsScientificbase.setApplydate(null);
                        //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                        ZgsScientificbase projectTask = zgsScientificbaseService.getById(id);
                        if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                            zgsScientificbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                        }
                    }
                    zgsScientificbase.setModifyaccountname(sysUser.getUsername());
                    zgsScientificbase.setModifyusername(sysUser.getRealname());
                    zgsScientificbase.setModifydate(new Date());
                    zgsScientificbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_42));
                    zgsScientificbase.setProjectstagestatus(zgsScientificbase.getStatus());

                    // 将授权编辑变为 取消授权编辑（说明：终审单位的此次授权编辑已使用完，想要修改，必须重新授权）
                    if (StringUtils.isNotBlank(zgsScientificbase.getSqbj()) && StringUtils.isNotBlank(zgsScientificbase.getUpdateSave())) {
                        zgsScientificbase.setSqbj("2");  // 取消编辑
                        zgsScientificbase.setUpdateSave("2");  // 修改已提交
                    }

                    zgsScientificbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificbase, ValidateEncryptEntityUtil.isEncrypt));
                    //实时更新阶段和状态
                    if (GlobalConstants.handleSubmit.equals(zgsScientificbase.getStatus()) && StringUtils.isNotEmpty(zgsScientificbase.getProjectlibraryguid())) {
                        zgsProjectlibraryService.initStageAndStatus(zgsScientificbase.getProjectlibraryguid());
                    }
                    String enterpriseguid = zgsScientificbase.getEnterpriseguid();
                    //科研项目验收申请项目执行情况评价奖励列表
                    List<ZgsScientificpexecutivelist> zgsScientificpexecutivelist0 = zgsScientificbase.getZgsScientificpexecutivelistList();
                    QueryWrapper<ZgsScientificpexecutivelist> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("enterpriseguid", enterpriseguid);
                    queryWrapper1.eq("baseguid", id);
                    queryWrapper1.ne("isdelete", 1);
                    List<ZgsScientificpexecutivelist> zgsScientificpexecutivelist0_1 = zgsScientificpexecutivelistService.list(queryWrapper1);
                    if (zgsScientificpexecutivelist0 != null && zgsScientificpexecutivelist0.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificpexecutivelist0.size(); a0++) {
                            ZgsScientificpexecutivelist zgsScientificpexecutivelist = zgsScientificpexecutivelist0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificpexecutivelist.getId())) {
                                //编辑
                                zgsScientificpexecutivelist.setModifyaccountname(sysUser.getUsername());
                                zgsScientificpexecutivelist.setModifyusername(sysUser.getRealname());
                                zgsScientificpexecutivelist.setModifydate(new Date());
                                zgsScientificpexecutivelistService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpexecutivelist, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificpexecutivelist.setId(UUID.randomUUID().toString());
                                zgsScientificpexecutivelist.setBaseguid(id);
                                zgsScientificpexecutivelist.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificpexecutivelist.setCreateaccountname(sysUser.getUsername());
                                zgsScientificpexecutivelist.setCreateusername(sysUser.getRealname());
                                zgsScientificpexecutivelist.setCreatedate(new Date());
                                zgsScientificpexecutivelistService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificpexecutivelist, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificpexecutivelist zgsScientificpexecutivelist1 : zgsScientificpexecutivelist0_1) {
                                if (!zgsScientificpexecutivelist1.getId().equals(zgsScientificpexecutivelist.getId())) {
                                    map0.put(zgsScientificpexecutivelist1.getId(), zgsScientificpexecutivelist1.getId());
                                } else {
                                    list0.add(zgsScientificpexecutivelist1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificpexecutivelistService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificpexecutivelist0_1 != null && zgsScientificpexecutivelist0_1.size() > 0) {
                            for (ZgsScientificpexecutivelist zgsScientificpexecutivelist : zgsScientificpexecutivelist0_1) {
                                zgsScientificpexecutivelistService.removeById(zgsScientificpexecutivelist.getId());
                            }
                        }
                    }

                    //相关附件
                    List<ZgsMattermaterial> zgsMattermaterialList = zgsScientificbase.getZgsMattermaterialList();
                    if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                        for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                            ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                            String mattermaterialId = zgsMattermaterial.getId();
                            UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                            wrapper.eq("relationid", mattermaterialId);
                            //先删除原来的再重新添加
                            zgsAttachappendixService.remove(wrapper);

                            zgsMattermaterialService.saveOrUpdate(zgsMattermaterial);

                            if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                                for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                                    ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                                    if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                        zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                        zgsAttachappendix.setModifytime(new Date());
                                    } else {
                                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                                        zgsAttachappendix.setRelationid(mattermaterialId);
                                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                        zgsAttachappendix.setCreatetime(new Date());
                                        zgsAttachappendix.setAppendixtype(GlobalConstants.ScientificAcceptance);
                                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                                    }
                                    zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }

                    //科研项目验收申请项目执行情况评价实体类
                    ZgsScientificprojectexecutive zgsScientificprojectexecutive = zgsScientificbase.getZgsScientificprojectexecutive();
                    if (StringUtils.isNotEmpty(zgsScientificprojectexecutive.getId())) {
                        //编辑
                        zgsScientificprojectexecutive.setModifyaccountname(sysUser.getUsername());
                        zgsScientificprojectexecutive.setModifyusername(sysUser.getRealname());
                        zgsScientificprojectexecutive.setModifydate(new Date());
                        zgsScientificprojectexecutiveService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectexecutive, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsScientificprojectexecutive.setId(UUID.randomUUID().toString());
                        zgsScientificprojectexecutive.setBaseguid(id);
                        zgsScientificprojectexecutive.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsScientificprojectexecutive.setCreateaccountname(sysUser.getUsername());
                        zgsScientificprojectexecutive.setCreateusername(sysUser.getRealname());
                        zgsScientificprojectexecutive.setCreatedate(new Date());
                        zgsScientificprojectexecutiveService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectexecutive, ValidateEncryptEntityUtil.isEncrypt));
                    }

                    //科研项目验收申请经费决算
                    ZgsScientificexpenditureclear zgsScientificexpenditureclear = zgsScientificbase.getZgsScientificexpenditureclear();
                    if (StringUtils.isNotEmpty(zgsScientificexpenditureclear.getId())) {
                        //编辑
                        zgsScientificexpenditureclear.setModifyaccountname(sysUser.getUsername());
                        zgsScientificexpenditureclear.setModifyusername(sysUser.getRealname());
                        zgsScientificexpenditureclear.setModifydate(new Date());
                        zgsScientificexpenditureclearService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificexpenditureclear, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsScientificexpenditureclear.setId(UUID.randomUUID().toString());
                        zgsScientificexpenditureclear.setBaseguid(id);
                        zgsScientificexpenditureclear.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsScientificexpenditureclear.setCreateaccountname(sysUser.getUsername());
                        zgsScientificexpenditureclear.setCreateusername(sysUser.getRealname());
                        zgsScientificexpenditureclear.setCreatedate(new Date());
                        zgsScientificexpenditureclearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificexpenditureclear, ValidateEncryptEntityUtil.isEncrypt));
                    }

                    //主要研究人员名单
                    List<ZgsScientificresearcher> zgsScientificresearcherList0 = zgsScientificbase.getZgsScientificresearcherList();
                    QueryWrapper<ZgsScientificresearcher> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("enterpriseguid", enterpriseguid);
                    queryWrapper3.eq("baseguid", id);
                    queryWrapper3.ne("isdelete", 1);
                    queryWrapper3.orderByAsc("ordernum");
                    List<ZgsScientificresearcher> zgsScientificresearcherList0_1 = zgsScientificresearcherService.list(queryWrapper3);
                    if (zgsScientificresearcherList0 != null && zgsScientificresearcherList0.size() > 0) {
                        if (zgsScientificresearcherList0.size() > 15) {
                            return Result.error("人员名单数量不得超过15人！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificresearcherList0.size(); a0++) {
                            ZgsScientificresearcher zgsScientificresearcher = zgsScientificresearcherList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificresearcher.getId())) {
                                //编辑
                                zgsScientificresearcher.setModifyaccountname(sysUser.getUsername());
                                zgsScientificresearcher.setModifyusername(sysUser.getRealname());
                                zgsScientificresearcher.setModifydate(new Date());
                                zgsScientificresearcherService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificresearcher, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificresearcher.setId(UUID.randomUUID().toString());
                                zgsScientificresearcher.setBaseguid(id);
                                zgsScientificresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificresearcher.setCreateaccountname(sysUser.getUsername());
                                zgsScientificresearcher.setCreateusername(sysUser.getRealname());
                                zgsScientificresearcher.setCreatedate(new Date());
//                        zgsScientificpexecutivelist.setPersontype(new BigDecimal(0));
                                zgsScientificresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificresearcher, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificresearcher zgsScientificresearcher1 : zgsScientificresearcherList0_1) {
                                if (!zgsScientificresearcher1.getId().equals(zgsScientificresearcher.getId())) {
                                    map0.put(zgsScientificresearcher1.getId(), zgsScientificresearcher1.getId());
                                } else {
                                    list0.add(zgsScientificresearcher1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificresearcherService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificresearcherList0_1 != null && zgsScientificresearcherList0_1.size() > 0) {
                            for (ZgsScientificresearcher zgsScientificresearcher : zgsScientificresearcherList0_1) {
                                zgsScientificresearcherService.removeById(zgsScientificresearcher.getId());
                            }
                        }
                    }

                    //项目完成单位情况    因申报阶段 项目完成单位信息已填写，此处将不在进行编辑或新增操作   rxl 20230809
                    List<ZgsScientificprojectfinish> zgsScientificprojectfinishList0 = zgsScientificbase.getZgsScientificprojectfinishList();
                    QueryWrapper<ZgsScientificprojectfinish> queryWrapper4 = new QueryWrapper<>();
                    // queryWrapper4.eq("enterpriseguid", enterpriseguid);
                    queryWrapper4.eq("baseguid", id);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.orderByAsc("ordernum");
                    List<ZgsScientificprojectfinish> zgsScientificprojectfinishList0_1 = zgsScientificprojectfinishService.list(queryWrapper4);
                    if (zgsScientificprojectfinishList0 != null && zgsScientificprojectfinishList0.size() > 0) {
                        if (zgsScientificprojectfinishList0.size() > 5) {
                            return Result.error("项目完成单位情况不得超过5个！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificprojectfinishList0.size(); a0++) {
                            ZgsScientificprojectfinish zgsScientificprojectfinish = zgsScientificprojectfinishList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificprojectfinish.getId())) {
                                //编辑
                                zgsScientificprojectfinish.setModifyaccountname(sysUser.getUsername());
                                zgsScientificprojectfinish.setModifyusername(sysUser.getRealname());
                                zgsScientificprojectfinish.setModifydate(new Date());
                                zgsScientificprojectfinishService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificprojectfinish.setId(UUID.randomUUID().toString());
                                zgsScientificprojectfinish.setBaseguid(id);
                                zgsScientificprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificprojectfinish.setCreateaccountname(sysUser.getUsername());
                                zgsScientificprojectfinish.setCreateusername(sysUser.getRealname());
                                zgsScientificprojectfinish.setCreatedate(new Date());
                                zgsScientificprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificprojectfinish zgsScientificprojectfinish1 : zgsScientificprojectfinishList0_1) {
                                if (!zgsScientificprojectfinish1.getId().equals(zgsScientificprojectfinish.getId())) {
                                    map0.put(zgsScientificprojectfinish1.getId(), zgsScientificprojectfinish1.getId());
                                } else {
                                    list0.add(zgsScientificprojectfinish1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificprojectfinishService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificprojectfinishList0_1 != null && zgsScientificprojectfinishList0_1.size() > 0) {
                            for (ZgsScientificprojectfinish zgsScientificprojectfinish : zgsScientificprojectfinishList0_1) {
                                zgsScientificprojectfinishService.removeById(zgsScientificprojectfinish.getId());
                            }
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-通过id删除")
    @ApiOperation(value = "科研项目申报验收基本信息表-通过id删除", notes = "科研项目申报验收基本信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsScientificbaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-批量删除")
    @ApiOperation(value = "科研项目申报验收基本信息表-批量删除", notes = "科研项目申报验收基本信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsScientificbaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-通过id查询")
    @ApiOperation(value = "科研项目申报验收基本信息表-通过id查询", notes = "科研项目申报验收基本信息表-通过id查询")
    @GetMapping(value = "/queryById")
//    public Result<?> queryById(@RequestParam(name = "id", required = false) String id, @RequestParam(name = "pdfStatus", required = false) Integer pdfStatus, HttpServletRequest request) {
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsScientificbase zgsScientificbase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsScientificbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsScientificbase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsScientificbase);
            }
            if ((zgsScientificbase.getFirstdate() != null || zgsScientificbase.getFinishdate() != null) && zgsScientificbase.getApplydate() != null) {
                zgsScientificbase.setSpLogStatus(1);
            } else {
                zgsScientificbase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsScientificbase = new ZgsScientificbase();
            Map<String, Object> map = new HashMap<>();
            //科技攻关ScienceTech、软科学ScienceSoft
            map.put("projecttypenum", GlobalConstants.ScientificAcceptance);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsScientificbase.setZgsMattermaterialList(zgsMattermaterialList);
            zgsScientificbase.setSpLogStatus(0);
        }
        return Result.OK(zgsScientificbase);
    }

    private void initProjectSelectById(ZgsScientificbase zgsScientificbase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = zgsScientificbase.getEnterpriseguid();
        String buildguid = zgsScientificbase.getId();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //屏蔽法人设置，先判断是否为专家用户类型，再查询
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                    String setValue = zgsExpertset.getSetvalue();
                    if (setValue.contains(",")) {
                        String strValue[] = setValue.split(",");
                        for (String set : strValue) {
                            if (GlobalConstants.expert_set_close1.equals(set)) {
                                zgsScientificbase.setApplyenterprisename(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                zgsScientificbase.setApplylinkman(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                zgsScientificbase.setApplyphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(set)) {
                            }
                        }
                    } else {
                        if (GlobalConstants.expert_set_close1.equals(setValue)) {
                            zgsScientificbase.setApplyenterprisename(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                            zgsScientificbase.setApplylinkman(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                            zgsScientificbase.setApplyphone(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                        }
                    }
                }
            }
            //
            //专家评审
            zgsScientificbase.setZgsSofttechexpertList(zgsSaexpertService.getZgsSaexpertList(buildguid));
            //科研项目验收申请项目执行情况评价奖励列表
            QueryWrapper<ZgsScientificpexecutivelist> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            List<ZgsScientificpexecutivelist> zgsScientificpexecutivelistList = zgsScientificpexecutivelistService.list(queryWrapper1);
            zgsScientificbase.setZgsScientificpexecutivelistList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificpexecutivelistList, ValidateEncryptEntityUtil.isDecrypt));
            //科研项目验收申请项目执行情况评价实体类
            QueryWrapper<ZgsScientificprojectexecutive> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("baseguid", buildguid);
            queryWrapper2.ne("isdelete", 1);
            ZgsScientificprojectexecutive zgsScientificprojectexecutive = zgsScientificprojectexecutiveService.getOne(queryWrapper2);
            zgsScientificbase.setZgsScientificprojectexecutive(ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificprojectexecutive, ValidateEncryptEntityUtil.isDecrypt));
            //主要研究人员名单
            QueryWrapper<ZgsScientificresearcher> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", buildguid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            queryWrapper3.last("limit 15");
            List<ZgsScientificresearcher> zgsScientificresearcherList = zgsScientificresearcherService.list(queryWrapper3);
            zgsScientificbase.setZgsScientificresearcherList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificresearcherList, ValidateEncryptEntityUtil.isDecrypt));

            //项目完成单位情况
            QueryWrapper<ZgsScientificprojectfinish> queryWrapper4 = new QueryWrapper<>();
            // queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", zgsScientificbase.getId());
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            queryWrapper4.last("limit 5");
            List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificprojectfinishService.list(queryWrapper4);
            zgsScientificbase.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));

            // 申报阶段_科技攻关项目_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper8 = new QueryWrapper<>();
            queryWrapper8.eq("enterpriseguid", enterpriseguid);
            queryWrapper8.eq("scienceguid", zgsScientificbase.getProjectlibraryguid());
            queryWrapper8.isNull("tag");
            queryWrapper8.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForKjgg = zgsSciencejointunitService.list(queryWrapper8);

            // 申报阶段_软科学_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper9 = new QueryWrapper<>();
            queryWrapper9.eq("enterpriseguid", enterpriseguid);
            queryWrapper9.eq("scienceguid", zgsScientificbase.getProjectlibraryguid());
            queryWrapper9.isNull("tag");
            queryWrapper9.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForRkx = zgsSciencejointunitService.list(queryWrapper9);

            // 申报阶段_完成单位信息
            if (zgsSciencejointunitListForKjgg.size() > 0) {
                // zgsScientificbase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForKjgg, ValidateEncryptEntityUtil.isDecrypt));
            } else if (zgsSciencejointunitListForRkx.size() > 0) {
               // zgsScientificbase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForRkx, ValidateEncryptEntityUtil.isDecrypt));
            }

            //科研项目验收申请经费决算
            QueryWrapper<ZgsScientificexpenditureclear> queryWrapper6 = new QueryWrapper<>();
            queryWrapper6.eq("enterpriseguid", enterpriseguid);
            queryWrapper6.eq("baseguid", buildguid);
            queryWrapper6.ne("isdelete", 1);
            ZgsScientificexpenditureclear zgsScientificexpenditureclear = zgsScientificexpenditureclearService.getOne(queryWrapper6);
            zgsScientificbase.setZgsScientificexpenditureclear(ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificexpenditureclear, ValidateEncryptEntityUtil.isDecrypt));
            //相关附件
            map.put("buildguid", buildguid);
            map.put("projecttypenum", GlobalConstants.ScientificAcceptance);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    if (StrUtil.isBlank(zgsMattermaterialList.get(i).getBuildguid())) {
                        zgsMattermaterialList.get(i).setId(UUID.randomUUID().toString());
                        zgsMattermaterialList.get(i).setBuildguid(buildguid); // 针对后期新增的查重报告缺少业务ID问题，进行补充
                    }
                    QueryWrapper<ZgsAttachappendix> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper5.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper5), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsScientificbase.setZgsMattermaterialList(zgsMattermaterialList);
            }
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsScientificbase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsScientificbase zgsScientificbase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsScientificbase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsScientificbase> pageList = (IPage<ZgsScientificbase>) result.getResult();
        List<ZgsScientificbase> list = pageList.getRecords();
        List<ZgsScientificbase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "科研项目申报验收基本信息表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsScientificbase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsScientificbase, ZgsScientificbase.class, "科研项目申报验收基本信息表");
        return mv;

    }

    private String getId(ZgsScientificbase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsScientificbase.class);
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目申报验收基本信息表-通过id查询导出项目验收申请表")
    @ApiOperation(value = "科研项目申报验收基本信息表-通过id查询导出项目验收申请表", notes = "科研项目申报验收基本信息表-通过id查询导出项目验收申请表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsScientificbase zgsScientificbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsScientificbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsScientificbase != null) {
            initProjectSelectById(zgsScientificbase);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "10甘肃省建设科技科研项目验收申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsScientificbase);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

    /**
     * @describe: 省厅角色下授权编辑按钮状态修改
     * @author: renxiaoliang
     * @date: 2023/11/15 16:08
     */
    @AutoLog(value = "授权编辑—状态修改")
    @ApiOperation(value = "授权编辑—状态修改", notes = "授权编辑—状态修改")
    @PostMapping(value = "/accreditUpdate")
    public Result<?> accreditUpdate(@RequestBody Map<String,Object> params) {
        Result result = new Result();

        String id = params.get("id").toString();
        String sqbj = params.get("sqbj").toString();  // 授权编辑  1 同意编辑  2 取消编辑
        String updateSave = params.get("updateSave").toString();  // 编辑保存  1 修改未提交  2 修改已提交

        if (StrUtil.isNotBlank(id) && StrUtil.isNotBlank(sqbj) && StrUtil.isNotBlank(updateSave)) {
            ZgsScientificbase zgsScientificbase = new ZgsScientificbase();
            zgsScientificbase.setId(id);
            zgsScientificbase.setSqbj(sqbj);
            zgsScientificbase.setUpdateSave(updateSave);
            if (zgsScientificbaseService.updateById(zgsScientificbase)){
                result.setMessage("授权编辑成功");
                result.setCode(1);
                result.setSuccess(true);
            } else {
                result.setMessage("授权编辑失败");
                result.setCode(0);
                result.setSuccess(false);
            }
        } else {
            result.setMessage("授权编辑失败");
            result.setCode(0);
            result.setSuccess(false);
        }
        return result;
    }


}
