package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildArea;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthgreenbuildAreaMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildAreaService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


/**
 * @Description: zgs_report_monthgreenbuild_area
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthgreenbuildAreaServiceImpl extends ServiceImpl<ZgsReportMonthgreenbuildAreaMapper, ZgsReportMonthgreenbuildArea> implements IZgsReportMonthgreenbuildAreaService {

  @Override
  public ZgsReportMonthgreenbuildArea queryLatestInfo (String dataSourceName, String processType){
    return this.baseMapper.queryLatestInfo(dataSourceName, processType);
  }
}
