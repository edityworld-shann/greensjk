package org.jeecg.modules.green.task.service.impl;

import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.mapper.ZgsTaskbuildfundbudgetMapper;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 任务书项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
@Service
public class ZgsTaskbuildfundbudgetServiceImpl extends ServiceImpl<ZgsTaskbuildfundbudgetMapper, ZgsTaskbuildfundbudget> implements IZgsTaskbuildfundbudgetService {

}
