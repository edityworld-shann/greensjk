package org.jeecg.modules.utils;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.lettuce.core.dynamic.annotation.Param;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import org.jeecg.modules.green.common.service.IZgsProjectunitmembersendService;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调用 短信发送 API    实例类
 *
 * @Author gwei
 */
@Transactional
@RestController
@RequestMapping("/sendSMSTemplete")
@Api(tags = "调用-短信发送-API-实例类")
@Slf4j
public class SendSMSTemplate {

    @Value("${jeecg.smsBaseUrl}")
    private String smsBaseUrl;
    @Autowired
    private SMSTokenUtil smsTokenUtil;

    /**
     *
     *      短信接口，调用说明：
     *
     *
     * 1、在需要调用类的上方，先 注入：SMSLogsService、RedisUtils、SMSTokenUtil 类。（因为 spring 机制导致）
     * 2、仿造下方具体实例，直接调用 短信API
     */

    /**
     * 发送短信，测试连接接口（单发、同步数据库记录） 实例
     * <p>
     * templateId： 24
     * 短信参数模板： 请{content}单位保留中期查验过程中所有影像资料。
     * 填充内容约束： content  长度=50；
     *
     * @return
     */
    @AutoLog(value = "发送短信-测试连接接口（单发-同步数据库记录）实例")
    @ApiOperation(value = "发送短信-测试连接接口（单发-同步数据库记录）实例", notes = "发送短信-测试连接接口（单发-同步数据库记录）实例")
    @PostMapping(value = "/sendSMSToAPITest1")
    public Result<?> sendSMSToAPITest1(@RequestBody String phoneNumber) throws Exception {

        HashMap<String, Object> fillContent = new HashMap<>();
        fillContent.put("content", "100000000100000000001000000000010000000000");   //  内容（企业名称）
        HashMap<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("phoneNumber", "18501265058,18500207064");    //  手机号
//    paramsMap.put("phoneNumber", "18501265058,18500207064,18210502471,15011221830,13068040063");    //  手机号
//    String phoneNumberParam = JSONObject.parseObject(phoneNumber).getString("phoneNumber");
//    paramsMap.put("phoneNumber", phoneNumberParam);
        paramsMap.put("smsType", 3);
        paramsMap.put("companyName", "100000000100000000001000000000010000000000");
        paramsMap.put("templateId", 24);
        paramsMap.put("fillContent", fillContent);
        SMSTokenUtil smsTokenUtil = new SMSTokenUtil();
        System.out.println("---------  发送短信   Begin   ！！！  -----------");
        Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
        if (result.getCode() == 200) {
            System.out.println("---------  发送短信成功！！！  -----------");
        } else {
            System.out.println("---------  发送短信失败！！！失败原因：" + result.getMessage() + "  -----------");
        }
        System.out.println("---------  发送短信   End   ！！！  -----------");
        return result;
    }


    /**
     * 发送短信，测试连接接口（单发、同步数据库记录） 实例
     *
     * @return
     */
    @AutoLog(value = "发送短信-测试连接接口（单发-同步数据库记录）实例")
    @ApiOperation(value = "发送短信-测试连接接口（单发-同步数据库记录）实例", notes = "发送短信-测试连接接口（单发-同步数据库记录）实例")
    @PostMapping(value = "/sendSMSToAPITest")
    public Result<?> sendSMSToAPITest(@RequestBody String phoneNumber) throws Exception {
        HashMap<String, Object> fillContent = new HashMap<>(10);
//    fillContent.put("location", "http://36.142.16.227:12065/green_sjk/");
//    fillContent.put("accountname", "甘肃天水绿色装配式建筑产业发展有限公司");
//    fillContent.put("username", "甘肃天水绿色装配式建筑产业发展有限公司");
//    fillContent.put("month", "5");
//    fillContent.put("day", "18");
//    fillContent.put("initPassword", "123456");
//    fillContent.put("QQNum", "686895470");

        fillContent.put("content1", "100000000001000000000010000000000100000000001000000000010000000000100000000001000000000010000000000");
        fillContent.put("content2", "100000000001000000000010000000000100000000001000000000010000000000100000000001000000000010000000000");
        fillContent.put("content3", "100000000001000000000010000000000100000000001000000000010000000000100000000001000000000010000000000");
        fillContent.put("month", "06");
        fillContent.put("day", "06");
        fillContent.put("QQNum", "10000000000");

        HashMap<String, Object> paramsMap = new HashMap<>(10);
        paramsMap.put("phoneNumber", "18501265058,18500207064,18210502471,15011221830,13068040063");    //  手机号
        String phoneNumberParam = JSONObject.parseObject(phoneNumber).getString("phoneNumber");
        paramsMap.put("phoneNumber", phoneNumberParam);
        paramsMap.put("companyName", "10000000010000000000100000000001000000000010000000000");
        paramsMap.put("smsType", 3);
        paramsMap.put("templateId", 20);
        paramsMap.put("fillContent", fillContent);
        SMSTokenUtil smsTokenUtil = new SMSTokenUtil();
        System.out.println("---------  发送短信   Begin   ！！！  -----------");
        Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
        if (result.getCode() == 200) {
            System.out.println("---------  发送短信成功！！！  -----------");
        } else {
            System.out.println("---------  发送短信失败！！！失败原因：" + result.getMessage() + "  -----------");
        }
        System.out.println("---------  发送短信   End   ！！！  -----------");
        return result;
    }


    /**
     * 发送短信，测试连接接口（群发【需要书写循环逻辑，循环赋值】、不同步数据库记录） 实例
     *
     * @return
     */
    @AutoLog(value = "发送短信-测试连接接口（群发【需要书写循环逻辑，循环赋值】-同步数据库记录）实例")
    @ApiOperation(value = "发送短信-测试连接接口（群发【需要书写循环逻辑，循环赋值】-同步数据库记录）实例", notes = "发送短信-测试连接接口（群发【需要书写循环逻辑，循环赋值】-同步数据库记录）实例")
    @PostMapping(value = "/sendSMSToAPITestBatch")
    public Result<?> sendSMSToAPITestBatch(@RequestBody String phoneNumber) {

//    SMSTokenUtil smsTokenUtil = new SMSTokenUtil();
        System.out.println("---------  发送短信   Begin   ！！！  -----------");
        Result<?> result = null;
        String phoneNumberParam = JSONObject.parseObject(phoneNumber).getString("phoneNumber");
        String[] phoneNumberArray = phoneNumberParam.split(",");
        for (int i = 0; i < 3; i++) {
            HashMap<String, Object> fillContent = new HashMap<>(10);
            fillContent.put("location", "http://36.142.16.227:12065/green_sjk/");
            fillContent.put("accountname", "甘肃天水绿色装配式建筑产业发展有限公司" + i);
            fillContent.put("username", "甘肃天水绿色装配式建筑产业发展有限公司" + i);
            fillContent.put("month", "5");
            fillContent.put("day", "18");
            fillContent.put("initPassword", "123456");
            fillContent.put("QQNum", "686895470");
            HashMap<String, Object> paramsMap = new HashMap<>(10);
            paramsMap.put("phoneNumber", phoneNumberArray[i]);
            paramsMap.put("smsType", 3);
            paramsMap.put("companyName", "甘肃天水绿色装配式建筑产业发展有限公司");
            paramsMap.put("templateId", 19);
            paramsMap.put("fillContent", fillContent);

            result = smsTokenUtil.sendSMSBatchService(JSONObject.toJSONString(paramsMap));
            if (result.getCode() == 200) {
                System.out.println("---------  发送短信成功！！！  -----------");
            } else {
                System.out.println("---------  发送短信失败！！！失败原因：" + result.getMessage() + "  -----------");
            }
            System.out.println("---------  发送短信   End   ！！！  -----------");
        }
        return result;
    }


    /**
     * 发送短信，测试连接接口
     *
     * @return
     */
    @AutoLog(value = "发送短信-精简版-临时套用")
    @ApiOperation(value = "发送短信-精简版-临时套用", notes = "发送短信-精简版，临时套用")
    @PostMapping(value = "/sendSMSToAPI")
    public Result<?> sendSMSToAPI() throws Exception {

        HashMap<String, Object> fillContent = new HashMap<>(10);
        fillContent.put("location", "http://36.142.16.227:12065/green_sjk/");
        fillContent.put("accountname", "123123");
        fillContent.put("username", "412");
        fillContent.put("month", "5");
        fillContent.put("day", "18");
        fillContent.put("initPassword", "123456");
        fillContent.put("QQNum", "686895470");

        HashMap<String, Object> headers = new HashMap<>(10);
        HashMap<String, Object> paramsMap = new HashMap<>(10);
        headers.put("Authorization", "Bearer 86b491e9-f28e-47c5-a9fd-a9d8452b17f7");
        paramsMap.put("phoneNumber", "18501265058");
        paramsMap.put("smsType", 3);
        paramsMap.put("companyName", "甘肃天水绿色装配式建筑产业发展有限公司");
        paramsMap.put("templateId", 19);
        paramsMap.put("fillContent", fillContent);

        String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(paramsMap));
        JSONObject resultJson = JSON.parseObject(resultUser);
        Map resultMap = new HashMap();
        if ("00000".equals(resultJson.get("code"))) {
            return Result.OK(resultMap);
        } else if ("D0624".equals(resultJson.get("code"))) {
            return Result.error("同一号码相同内容发送次数太多（默认24小时内，验证码类发送5次或相同内容3次以上会报此错误。");
        } else {
            return Result.error(resultJson.get("msg").toString());
        }
    }
}
