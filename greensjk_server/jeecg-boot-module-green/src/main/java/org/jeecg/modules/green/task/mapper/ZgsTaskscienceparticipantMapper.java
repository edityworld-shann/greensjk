package org.jeecg.modules.green.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface ZgsTaskscienceparticipantMapper extends BaseMapper<ZgsTaskscienceparticipant> {

}
