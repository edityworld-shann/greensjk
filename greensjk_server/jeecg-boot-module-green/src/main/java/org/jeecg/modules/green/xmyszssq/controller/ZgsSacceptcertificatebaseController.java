package org.jeecg.modules.green.xmyszssq.controller;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.afterturn.easypoi.entity.ImageEntity;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.jeecg.modules.green.xmyszssq.entity.*;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertprojectfinishService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertresearcherService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科研项目验收证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "科研项目验收证书")
@RestController
@RequestMapping("/xmyszssq/zgsSacceptcertificatebase")
@Slf4j
public class ZgsSacceptcertificatebaseController extends JeecgController<ZgsSacceptcertificatebase, IZgsSacceptcertificatebaseService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsSacceptcertificatebaseService zgsSacceptcertificatebaseService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;
    @Autowired
    private IZgsSacceptcertprojectfinishService zgsSacceptcertprojectfinishService;
    @Autowired
    private IZgsSacceptcertresearcherService zgsSacceptcertresearcherService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlanresultbaseService planresultbaseService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;

    /**
     * 分页列表查询
     *
     * @param zgsSacceptcertificatebase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科研项目验收证书-分页列表查询")
    @ApiOperation(value = "科研项目验收证书-分页列表查询", notes = "科研项目验收证书-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSacceptcertificatebase zgsSacceptcertificatebase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsSacceptcertificatebase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectname())) {
            queryWrapper.like("projectname", zgsSacceptcertificatebase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectnumber())) {
            queryWrapper.like("projectnumber", zgsSacceptcertificatebase.getProjectnumber());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnumber,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectstage())) {
            if (!"-1".equals(zgsSacceptcertificatebase.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsSacceptcertificatebase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initSacceptcertificateSelect(queryWrapper, zgsSacceptcertificatebase.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSacceptcertificatebase.getStatus(), sysUser, 1);
        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsSacceptcertificatebase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //TODO 修改根据初审日期进行降序 2022-09-21
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            /*queryWrapper.orderByDesc("createdate");
            queryWrapper.orderByDesc("applydate");*/
            queryWrapper.orderByDesc("firstdate");
            queryWrapper.orderByDesc("applydate");
        }
        Page<ZgsSacceptcertificatebase> page = new Page<ZgsSacceptcertificatebase>(pageNo, pageSize);
        // IPage<ZgsSacceptcertificatebase> pageList = zgsSacceptcertificatebaseService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsSacceptcertificatebase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsSacceptcertificatebase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsSacceptcertificatebase> pageList = null;

        // 科研项目验收证书
        redisUtil.expire("kyxmyszs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmyszsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kyxmyszsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsSacceptcertificatebase.getDspForTjdw())) {
            if (zgsSacceptcertificatebase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("kyxmyszs")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsSacceptcertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmyszs",pageListForProjectCount.getTotal());
                }
            }
            if (zgsSacceptcertificatebase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("kyxmyszsOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsSacceptcertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("kyxmyszsOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getDspForTjdw()) && StringUtils.isBlank(zgsSacceptcertificatebase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsSacceptcertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsSacceptcertificatebase.getDspForTjdw()) && StringUtils.isNotBlank(zgsSacceptcertificatebase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsSacceptcertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getDspForSt()) && zgsSacceptcertificatebase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsSacceptcertificatebaseService.page(page, queryWrapper);
            redisUtil.set("kyxmyszsOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSacceptcertificatebase.getStatus(), sysUser, 1);
            pageList = zgsSacceptcertificatebaseService.page(page, queryWrapper);
        }


        /*// 科研项目验收证书
        redisUtil.expire("kyxmyszs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmyszs")) {
            if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getFlagByWorkTable()) && zgsSacceptcertificatebase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("kyxmyszs",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmyszsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmyszsOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getDspForSt()) && zgsSacceptcertificatebase.getDspForSt().equals("2")) {
                redisUtil.set("kyxmyszsOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("kyxmyszsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kyxmyszsOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsSacceptcertificatebase.getDspForTjdw()) && zgsSacceptcertificatebase.getDspForTjdw().equals("3")) {
                redisUtil.set("kyxmyszsOfTjdw",pageList.getTotal());
            }
        }*/

        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSacceptcertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
                    }
                    // 根据主键id获取成果简介信息
                    QueryWrapper<ZgsPlanresultbase> planresultbaseQueryWrapper = new QueryWrapper<>();
                    planresultbaseQueryWrapper.eq("ysid",zgsInfo.getId());
                    zgsInfo.setZgsPlanresultbase(planresultbaseService.getOne(planresultbaseQueryWrapper));

                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSacceptcertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
                    }
                    // 根据主键id获取成果简介信息
                    QueryWrapper<ZgsPlanresultbase> planresultbaseQueryWrapper = new QueryWrapper<>();
                    planresultbaseQueryWrapper.eq("ysid",zgsInfo.getId());
                    zgsInfo.setZgsPlanresultbase(planresultbaseService.getOne(planresultbaseQueryWrapper));

                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //终审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                    queryWrapper0.eq("sciencetechguid", zgsInfo.getProjectlibraryguid());
                    queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                    queryWrapper0.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                    if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }

                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsSacceptcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目验收证书-撤回")
    @ApiOperation(value = "科研项目验收证书-撤回", notes = "科研项目验收证书-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        if (zgsSacceptcertificatebase != null) {
            String id = zgsSacceptcertificatebase.getId();
            String bid = zgsSacceptcertificatebase.getProjectlibraryguid();
            String status = zgsSacceptcertificatebase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52).equals(zgsSacceptcertificatebase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsSacceptcertificatebase info = new ZgsSacceptcertificatebase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsSacceptcertificatebaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsSacceptcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目验收证书-审批")
    @ApiOperation(value = "科研项目验收证书-审批", notes = "科研项目验收证书-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        zgsSacceptcertificatebaseService.spProject(zgsSacceptcertificatebase);
        String projectlibraryguid = zgsSacceptcertificatebaseService.getById(zgsSacceptcertificatebase.getId()).getProjectlibraryguid();
        // 同步更新 科研项目于验收证书阶段的成果展示状态信息



        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }


    /**
     * 添加
     *
     * @param zgsSacceptcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目验收证书-添加")
    @ApiOperation(value = "科研项目验收证书-添加", notes = "科研项目验收证书-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        Result result = new Result();
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsSacceptcertificatebase.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsSacceptcertificatebase.getStatus())) {
            //保存并上报
            zgsSacceptcertificatebase.setApplydate(new Date());
        } else {
            zgsSacceptcertificatebase.setApplydate(null);
        }
        zgsSacceptcertificatebase.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsSacceptcertificatebase.setCreateaccountname(sysUser.getUsername());
        zgsSacceptcertificatebase.setCreateusername(sysUser.getRealname());
        zgsSacceptcertificatebase.setCreatedate(new Date());
        //判断是否添加验收项目，否则给予提醒，需先添加验收项目
        QueryWrapper<ZgsScientificbase> queryWrapper = new QueryWrapper();
        queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS2).or().eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectname())) {
                qwp1.like("projectname", zgsSacceptcertificatebase.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectlibraryguid())) {
                qwp1.or().eq("projectlibraryguid", zgsSacceptcertificatebase.getProjectlibraryguid());
            }
        });
//        List<ZgsScientificbase> list = zgsScientificbaseService.list(queryWrapper);
//        if (list.size() == 0) {
//            return Result.error("请项目验收管理流程结束后，方可进行此阶段申请。！");
//        }
        //判断项目终止
//        QueryWrapper<ZgsSciencetechtask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("sciencetechguid", zgsSacceptcertificatebase.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsSciencetechtask> listHist = zgsSciencetechtaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsSacceptcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
        zgsSacceptcertificatebase.setProjectstagestatus(zgsSacceptcertificatebase.getStatus());
        if (zgsSacceptcertificatebaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertificatebase, ValidateEncryptEntityUtil.isEncrypt))) {
            // 科研项目验收证书保存成功后将id信息进行返回，与项目成果进行关联


            //实时更新阶段和状态
            if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsSacceptcertificatebase.getProjectlibraryguid());
            }
//        redisUtil.set(zgsSacceptcertificatebase.getProjectlibraryguid(), "验收结题证书阶段");
            //主要研究人员名单
            List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList = zgsSacceptcertificatebase.getZgsSacceptcertresearcherList();
            if (zgsSacceptcertresearcherList != null && zgsSacceptcertresearcherList.size() > 0) {
                if (zgsSacceptcertresearcherList.size() > 15) {
                    return Result.error("人员名单数量不得超过15人！");
                }
                for (int a1 = 0; a1 < zgsSacceptcertresearcherList.size(); a1++) {
                    ZgsSacceptcertresearcher zgsSacceptcertresearcher = zgsSacceptcertresearcherList.get(a1);
                    zgsSacceptcertresearcher.setId(UUID.randomUUID().toString());
                    zgsSacceptcertresearcher.setBaseguid(id);
                    zgsSacceptcertresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                    zgsSacceptcertresearcher.setCreateaccountname(sysUser.getUsername());
                    zgsSacceptcertresearcher.setCreateusername(sysUser.getRealname());
                    zgsSacceptcertresearcher.setCreatedate(new Date());
                    zgsSacceptcertresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertresearcher, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //完成单位情况  因申报阶段已填写项目完成单位信息  故验收证书阶段不再进行新增或编辑操作   rxl  20230809
            /*List<ZgsSacceptcertprojectfinish> zgsSacceptcertprojectfinishList = zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList();
            if (zgsSacceptcertprojectfinishList != null && zgsSacceptcertprojectfinishList.size() > 0) {
                if (zgsSacceptcertprojectfinishList.size() > 5) {
                    return Result.error("项目完成单位情况不得超过5个！");
                }
                for (int a1 = 0; a1 < zgsSacceptcertprojectfinishList.size(); a1++) {
                    ZgsSacceptcertprojectfinish zgsSacceptcertprojectfinish = zgsSacceptcertprojectfinishList.get(a1);
                    zgsSacceptcertprojectfinish.setId(UUID.randomUUID().toString());
                    zgsSacceptcertprojectfinish.setBaseguid(id);
                    zgsSacceptcertprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                    zgsSacceptcertprojectfinish.setCreateaccountname(sysUser.getUsername());
                    zgsSacceptcertprojectfinish.setCreateusername(sysUser.getRealname());
                    zgsSacceptcertprojectfinish.setCreatedate(new Date());
                    zgsSacceptcertprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                }
            }*/


            //验收专家名单
        /*List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertificatebase.getZgsSacceptcertexpertList();
        String baseguid = zgsSacceptcertificatebase.getProjectlibraryguid();
        if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
            for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList.get(a1);
                zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                // zgsSacceptcertexpert.setBaseguid(id);
                zgsSacceptcertexpert.setBaseguid(baseguid);
                zgsSacceptcertexpert.setProjectStage("ysjd");  // 验收阶段
                zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                zgsSacceptcertexpert.setCreatedate(new Date());
                zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
            }
        }*/
            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsSacceptcertificatebase.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    String mattermaterialId = UUID.randomUUID().toString();
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    zgsMattermaterial.setId(mattermaterialId);
                    zgsMattermaterial.setBuildguid(id);
                    zgsMattermaterialService.save(zgsMattermaterial);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            zgsAttachappendix.setId(UUID.randomUUID().toString());
                            zgsAttachappendix.setRelationid(mattermaterialId);
                            zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                            zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                            zgsAttachappendix.setCreatetime(new Date());
                            zgsAttachappendix.setAppendixtype(GlobalConstants.SAcceptCertificate);
                            zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
            result.setResult(id);
            result.setSuccess(true);
            result.setMessage("保存成功");
        } else {
            result.setResult("");
            result.setSuccess(false);
            result.setMessage("保存失败");
        }

        return result;
    }

    /**
     * 编辑
     *
     * @param zgsSacceptcertificatebase
     * @return
     */
    @AutoLog(value = "科研项目验收证书-编辑")
    @ApiOperation(value = "科研项目验收证书-编辑", notes = "科研项目验收证书-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSacceptcertificatebase != null && StringUtils.isNotEmpty(zgsSacceptcertificatebase.getId())) {
            String id = zgsSacceptcertificatebase.getId();
            if (GlobalConstants.handleSubmit.equals(zgsSacceptcertificatebase.getStatus())) {
                //保存并上报
                zgsSacceptcertificatebase.setApplydate(new Date());
                zgsSacceptcertificatebase.setAuditopinion(null);
//                zgsSacceptcertificatebase.setReturntype(null);
            } else {
                zgsSacceptcertificatebase.setApplydate(null);
            }
            zgsSacceptcertificatebase.setModifyaccountname(sysUser.getUsername());
            zgsSacceptcertificatebase.setModifyusername(sysUser.getRealname());
            zgsSacceptcertificatebase.setModifydate(new Date());
            zgsSacceptcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
            zgsSacceptcertificatebase.setProjectstagestatus(zgsSacceptcertificatebase.getStatus());
            zgsSacceptcertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertificatebase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (StringUtils.isNotEmpty(zgsSacceptcertificatebase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsSacceptcertificatebase.getProjectlibraryguid());
            }
            String enterpriseguid = zgsSacceptcertificatebase.getEnterpriseguid();
            //主要研究人员名单
            List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList0 = zgsSacceptcertificatebase.getZgsSacceptcertresearcherList();
            QueryWrapper<ZgsSacceptcertresearcher> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", id);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList0_1 = zgsSacceptcertresearcherService.list(queryWrapper1);
            if (zgsSacceptcertresearcherList0 != null && zgsSacceptcertresearcherList0.size() > 0) {
                if (zgsSacceptcertresearcherList0.size() > 15) {
                    return Result.error("人员名单数量不得超过15人！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsSacceptcertresearcherList0.size(); a0++) {
                    ZgsSacceptcertresearcher zgsSacceptcertresearcher = zgsSacceptcertresearcherList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsSacceptcertresearcher.getId())) {
                        //编辑
                        zgsSacceptcertresearcher.setModifyaccountname(sysUser.getUsername());
                        zgsSacceptcertresearcher.setModifyusername(sysUser.getRealname());
                        zgsSacceptcertresearcher.setModifydate(new Date());
                        zgsSacceptcertresearcherService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertresearcher, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsSacceptcertresearcher.setId(UUID.randomUUID().toString());
                        zgsSacceptcertresearcher.setBaseguid(id);
                        zgsSacceptcertresearcher.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsSacceptcertresearcher.setCreateaccountname(sysUser.getUsername());
                        zgsSacceptcertresearcher.setCreateusername(sysUser.getRealname());
                        zgsSacceptcertresearcher.setCreatedate(new Date());
//                        zgsScientificpexecutivelist.setPersontype(new BigDecimal(0));
                        zgsSacceptcertresearcherService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertresearcher, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsSacceptcertresearcher zgsSacceptcertresearcher1 : zgsSacceptcertresearcherList0_1) {
                        if (!zgsSacceptcertresearcher1.getId().equals(zgsSacceptcertresearcher.getId())) {
                            map0.put(zgsSacceptcertresearcher1.getId(), zgsSacceptcertresearcher1.getId());
                        } else {
                            list0.add(zgsSacceptcertresearcher1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsSacceptcertresearcherService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsSacceptcertresearcherList0_1 != null && zgsSacceptcertresearcherList0_1.size() > 0) {
                    for (ZgsSacceptcertresearcher zgsSacceptcertresearcher : zgsSacceptcertresearcherList0_1) {
                        zgsSacceptcertresearcherService.removeById(zgsSacceptcertresearcher.getId());
                    }
                }
            }

            //完成单位情况   因申报阶段已进行完成单位信息新增，故验收证书阶段不再进行单位信息新增或编辑操作   rxl 20230809
            List<ZgsSacceptcertprojectfinish> zgsSacceptcertprojectfinishList0 = zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList();
            QueryWrapper<ZgsSacceptcertprojectfinish> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", id);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            List<ZgsSacceptcertprojectfinish> zgsSacceptcertprojectfinishList0_1 = zgsSacceptcertprojectfinishService.list(queryWrapper3);
            if (zgsSacceptcertprojectfinishList0 != null && zgsSacceptcertprojectfinishList0.size() > 0) {
                if (zgsSacceptcertprojectfinishList0.size() > 5) {
                    return Result.error("项目完成单位情况不得超过5个！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsSacceptcertprojectfinishList0.size(); a0++) {
                    ZgsSacceptcertprojectfinish zgsSacceptcertprojectfinish = zgsSacceptcertprojectfinishList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsSacceptcertprojectfinish.getId())) {
                        //编辑
                        zgsSacceptcertprojectfinish.setModifyaccountname(sysUser.getUsername());
                        zgsSacceptcertprojectfinish.setModifyusername(sysUser.getRealname());
                        zgsSacceptcertprojectfinish.setModifydate(new Date());
                        zgsSacceptcertprojectfinishService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsSacceptcertprojectfinish.setId(UUID.randomUUID().toString());
                        zgsSacceptcertprojectfinish.setBaseguid(id);
                        zgsSacceptcertprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsSacceptcertprojectfinish.setCreateaccountname(sysUser.getUsername());
                        zgsSacceptcertprojectfinish.setCreateusername(sysUser.getRealname());
                        zgsSacceptcertprojectfinish.setCreatedate(new Date());
//                        zgsScientificpexecutivelist.setPersontype(new BigDecimal(0));
                        zgsSacceptcertprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsSacceptcertprojectfinish zgsSacceptcertprojectfinish1 : zgsSacceptcertprojectfinishList0_1) {
                        if (!zgsSacceptcertprojectfinish1.getId().equals(zgsSacceptcertprojectfinish.getId())) {
                            map0.put(zgsSacceptcertprojectfinish1.getId(), zgsSacceptcertprojectfinish1.getId());
                        } else {
                            list0.add(zgsSacceptcertprojectfinish1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsSacceptcertprojectfinishService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsSacceptcertprojectfinishList0_1 != null && zgsSacceptcertprojectfinishList0_1.size() > 0) {
                    for (ZgsSacceptcertprojectfinish zgsSacceptcertprojectfinish : zgsSacceptcertprojectfinishList0_1) {
                        zgsSacceptcertprojectfinishService.removeById(zgsSacceptcertprojectfinish.getId());
                    }
                }
            }

            //验收专家名单（原有的专家信息修改逻辑）
            /*List<ZgsSacceptcertexpert> zgsSacceptcertexpertList0 = zgsSacceptcertificatebase.getZgsSacceptcertexpertList();
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", id);
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList0_1 = zgsSacceptcertexpertService.list(queryWrapper4);
            if (zgsSacceptcertexpertList0 != null && zgsSacceptcertexpertList0.size() > 0) {
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsSacceptcertexpertList0.size(); a0++) {
                    ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsSacceptcertexpert.getId())) {
                        //编辑
                        zgsSacceptcertexpert.setModifyaccountname(sysUser.getUsername());
                        zgsSacceptcertexpert.setModifyusername(sysUser.getRealname());
                        zgsSacceptcertexpert.setModifydate(new Date());
                        zgsSacceptcertexpertService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                        zgsSacceptcertexpert.setBaseguid(id);
                        zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                        zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                        zgsSacceptcertexpert.setCreatedate(new Date());
                        zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsSacceptcertexpert zgsSacceptcertexpert1 : zgsSacceptcertexpertList0_1) {
                        if (!zgsSacceptcertexpert1.getId().equals(zgsSacceptcertexpert.getId())) {
                            map0.put(zgsSacceptcertexpert1.getId(), zgsSacceptcertexpert1.getId());
                        } else {
                            list0.add(zgsSacceptcertexpert1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsSacceptcertexpertService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsSacceptcertexpertList0_1 != null && zgsSacceptcertexpertList0_1.size() > 0) {
                    for (ZgsSacceptcertexpert zgsSacceptcertexpert : zgsSacceptcertexpertList0_1) {
                        zgsSacceptcertexpertService.removeById(zgsSacceptcertexpert.getId());
                    }
                }
            }*/


            // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
            QueryWrapper<ZgsSacceptcertexpert> expertQueryWrapper = new QueryWrapper<>();
            expertQueryWrapper.eq("baseguid",zgsSacceptcertificatebase.getProjectlibraryguid());
            expertQueryWrapper.eq("project_stage","ysjd");
            expertQueryWrapper.ne("isdelete",1);
            List<ZgsSacceptcertexpert> ZgsSacceptcertexpertList = zgsSacceptcertexpertService.list(expertQueryWrapper);
            if (ZgsSacceptcertexpertList.size() > 0) {
                // 删除历史专家信息
                QueryWrapper<ZgsSacceptcertexpert> expertDeleteWrapper = new QueryWrapper<>();
                expertDeleteWrapper.eq("baseguid",zgsSacceptcertificatebase.getProjectlibraryguid());
                expertDeleteWrapper.eq("project_stage","ysjd");
                expertDeleteWrapper.ne("isdelete",1);
                boolean bolResult = zgsSacceptcertexpertService.remove(expertDeleteWrapper);
                // 删除成功后，保存修改后的专家信息  rxl  20230620
                if (bolResult) {
                    List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertificatebase.getZgsSacceptcertexpertList();
                    if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
                        for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                            ZgsSacceptcertexpert zgsSacceptcertexpertForUpdate = zgsSacceptcertexpertList.get(a1);
                            zgsSacceptcertexpertForUpdate.setId(UUID.randomUUID().toString());
                            zgsSacceptcertexpertForUpdate.setBaseguid(zgsSacceptcertificatebase.getProjectlibraryguid());
                            zgsSacceptcertexpertForUpdate.setProjectStage("ysjd");   // 申报阶段
                            zgsSacceptcertexpertForUpdate.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsSacceptcertexpertForUpdate.setCreateaccountname(sysUser.getUsername());
                            zgsSacceptcertexpertForUpdate.setCreateusername(sysUser.getRealname());
                            zgsSacceptcertexpertForUpdate.setCreatedate(new Date());
                            zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpertForUpdate, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }

            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsSacceptcertificatebase.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.SAcceptCertificate);
                                zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目验收证书-通过id删除")
    @ApiOperation(value = "科研项目验收证书-通过id删除", notes = "科研项目验收证书-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSacceptcertificatebaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科研项目验收证书-批量删除")
    @ApiOperation(value = "科研项目验收证书-批量删除", notes = "科研项目验收证书-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSacceptcertificatebaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param
     * @return
     */
    @AutoLog(value = "科研项目验收证书-通过id查询")
    @ApiOperation(value = "科研项目验收证书-通过id查询", notes = "科研项目验收证书-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsSacceptcertificatebase zgsSacceptcertificatebase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsSacceptcertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsSacceptcertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsSacceptcertificatebase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsSacceptcertificatebase);
            }
            if ((zgsSacceptcertificatebase.getFirstdate() != null || zgsSacceptcertificatebase.getFinishdate() != null) && zgsSacceptcertificatebase.getApplydate() != null) {
                zgsSacceptcertificatebase.setSpLogStatus(1);
            } else {
                zgsSacceptcertificatebase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsSacceptcertificatebase = new ZgsSacceptcertificatebase();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.SAcceptCertificate);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsSacceptcertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            zgsSacceptcertificatebase.setSpLogStatus(0);
        }
        return Result.OK(zgsSacceptcertificatebase);
    }

    private void initProjectSelectById(ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        String enterpriseguid = zgsSacceptcertificatebase.getEnterpriseguid();
        String buildguid = zgsSacceptcertificatebase.getId();
        String projectlibarayId = zgsSacceptcertificatebase.getProjectlibraryguid();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {  //  && StringUtils.isNotEmpty(projectlibarayId)
            //主要研究人员名单
            QueryWrapper<ZgsSacceptcertresearcher> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            queryWrapper1.last("limit 15");
            List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList = zgsSacceptcertresearcherService.list(queryWrapper1);
            zgsSacceptcertificatebase.setZgsSacceptcertresearcherList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertresearcherList, ValidateEncryptEntityUtil.isDecrypt));
            //相关附件
            map.put("buildguid", buildguid);
            //后期查下SAcceptCertificate出现在哪张表里
            map.put("projecttypenum", GlobalConstants.SAcceptCertificate);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsSacceptcertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //完成单位情况
            QueryWrapper<ZgsSacceptcertprojectfinish> queryWrapper3 = new QueryWrapper<>();
            // queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", buildguid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            queryWrapper3.last("limit 5");
            List<ZgsSacceptcertprojectfinish> zgsSacceptcertprojectfinishList = zgsSacceptcertprojectfinishService.list(queryWrapper3);
            zgsSacceptcertificatebase.setZgsSacceptcertprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));

            // 申报阶段_科技攻关项目_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper8 = new QueryWrapper<>();
            queryWrapper8.eq("enterpriseguid", enterpriseguid);
            queryWrapper8.eq("scienceguid", zgsSacceptcertificatebase.getProjectlibraryguid());
            queryWrapper8.isNull("tag");
            queryWrapper8.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForKjgg = zgsSciencejointunitService.list(queryWrapper8);

            // 申报阶段_软科学_申报单位信息
            QueryWrapper<ZgsSciencejointunit> queryWrapper9 = new QueryWrapper<>();
            queryWrapper9.eq("enterpriseguid", enterpriseguid);
            queryWrapper9.eq("scienceguid", zgsSacceptcertificatebase.getProjectlibraryguid());
            queryWrapper9.isNull("tag");
            queryWrapper9.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitListForRkx = zgsSciencejointunitService.list(queryWrapper9);

            if (zgsSciencejointunitListForKjgg.size() > 0) { // 申报阶段_科技攻关的完成单位信息
                zgsSacceptcertificatebase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForKjgg, ValidateEncryptEntityUtil.isDecrypt));
            } else if (zgsSciencejointunitListForRkx.size() > 0) { // 申报阶段_软科学的完成单位信息
                zgsSacceptcertificatebase.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForRkx, ValidateEncryptEntityUtil.isDecrypt));
            }


            //项目完成单位情况
            QueryWrapper<ZgsScientificprojectfinish> queryWrapper6 = new QueryWrapper<>();
            // queryWrapper6.eq("enterpriseguid", enterpriseguid);
            queryWrapper6.eq("baseguid", zgsSacceptcertificatebase.getProjectlibraryguid());
            // queryWrapper6.eq("baseguid", buildguid);
            queryWrapper6.ne("isdelete", 1);
            queryWrapper6.orderByAsc("ordernum");
            queryWrapper6.last("limit 5");
            List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificprojectfinishService.list(queryWrapper6);
            zgsSacceptcertificatebase.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));


            //验收专家名单
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            // queryWrapper4.eq("baseguid", buildguid);
            queryWrapper4.eq("baseguid", projectlibarayId);
            queryWrapper4.eq("project_stage", "ysjd");
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");

            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertexpertService.list(queryWrapper4);

            // 验收证书阶段 查询成果简介 进行回显   rxl  20230629
            QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper<>();
            queryWrapper.select("applyrange,enddate,finishenterprise,issecurity,linktel,projectleader,promotiondirection,receptiontime,resultintro,resultname,startdate,status,result_show");
            queryWrapper.eq("projectlibraryguid",projectlibarayId).or().eq("ysid",buildguid);

            List<ZgsPlanresultbase> listZgsPlanresultbase = planresultbaseService.list(queryWrapper);
            if (listZgsPlanresultbase.size() > 0) {
                zgsSacceptcertificatebase.setZgsPlanresultbase(ValidateEncryptEntityUtil.validateDecryptObject(listZgsPlanresultbase.get(0), ValidateEncryptEntityUtil.isDecrypt));
                // zgsSacceptcertificatebase.setZgsPlanresultbase(listZgsPlanresultbase.get(0));
            }
            zgsSacceptcertificatebase.setZgsSacceptcertexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertexpertList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param zgsSacceptcertificatebase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsSacceptcertificatebase zgsSacceptcertificatebase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsSacceptcertificatebase, pageNo, 999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsSacceptcertificatebase> pageList = (IPage<ZgsSacceptcertificatebase>) result.getResult();
        List<ZgsSacceptcertificatebase> list = pageList.getRecords();
        List<ZgsSacceptcertificatebase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "科研项目验收证书";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsSacceptcertificatebase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsSacceptcertificatebase, ZgsSacceptcertificatebase.class, "科研项目验收证书");
        return mv;

    }

    private String getId(ZgsSacceptcertificatebase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSacceptcertificatebase.class);
    }

    /**
     * 通过id查询导出word表
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科研项目验收证书-通过id查询导出word表")
    @ApiOperation(value = "科研项目验收证书-通过id查询导出word表", notes = "科研项目验收证书-通过id查询导出word表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsSacceptcertificatebase zgsSacceptcertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsSacceptcertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsSacceptcertificatebase != null) {
            initProjectSelectById(zgsSacceptcertificatebase);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "15甘肃省建设科技科研项目验收证书.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsSacceptcertificatebase);
        //完成单位情况-start
        StringBuffer sb = new StringBuffer();
        // if (templateMap != null && zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList() != null) {
        //     if (zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList().size() > 0) {
        //         for (int i = 0; i < zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList().size(); i++) {
        //             ZgsSacceptcertprojectfinish zgsSpostprojectfinish = zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList().get(i);
        //             sb.append(zgsSpostprojectfinish.getEnterprisename());
        //             if (i < zgsSacceptcertificatebase.getZgsSacceptcertprojectfinishList().size() - 1) {
        //                 sb.append("、");
        //             }
        //         }
        //     }
        //     templateMap.put("enterprisenameList", sb.toString());
        // }
        if (templateMap != null && null != zgsSacceptcertificatebase && null != zgsSacceptcertificatebase.getZgsScientificprojectfinishList()) {
            if (zgsSacceptcertificatebase.getZgsScientificprojectfinishList().size() > 0) {
                for (int i = 0; i < zgsSacceptcertificatebase.getZgsScientificprojectfinishList().size(); i++) {
                    ZgsScientificprojectfinish zgsSpostprojectfinish = zgsSacceptcertificatebase.getZgsScientificprojectfinishList().get(i);
                    sb.append(zgsSpostprojectfinish.getEnterprisename());
                    if (i < zgsSacceptcertificatebase.getZgsScientificprojectfinishList().size() - 1) {
                        sb.append("、");
                    }
                }
            }
            templateMap.put("enterprisenameList", sb.toString());
        }

        //完成单位情况-end
        //图片处理-start
        ImageEntity imageYiJian = new ImageEntity();
        ImageEntity imageYiJian1 = new ImageEntity();
        ImageEntity imageQianZi = new ImageEntity();
        ImageEntity imageQianZi1 = new ImageEntity();
        String filePath1 = "";
        String filePath2 = "";
        String filePath3 = "";
        String filePath4 = "";
        if (zgsSacceptcertificatebase != null && zgsSacceptcertificatebase.getZgsMattermaterialList().size() > 1) {
            List<ZgsMattermaterial> zgsMattermaterialList = zgsSacceptcertificatebase.getZgsMattermaterialList();
            for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(i);
                if (i == 1 && zgsMattermaterial.getZgsAttachappendixList().size() > 2) {
                    for (int j = 0; j < zgsMattermaterial.getZgsAttachappendixList().size(); j++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(j);
                        if (!zgsAttachappendix.getAppendixname().contains("签字")) {
                            zgsMattermaterialList.get(i).getZgsAttachappendixList().remove(j);
                        }
                    }
                }
            }
            imageYiJian.setHeight(600);
            imageYiJian.setWidth(500);
            filePath1 = upload + File.separator + zgsMattermaterialList.get(0).getZgsAttachappendixList().get(0).getAppendixpath();
            filePath1 = LibreOfficeUtil.wordConverterToJpg(filePath1);
            log.info("filePath1=" + filePath1);
            File file1 = new File(filePath1);
            if (file1.exists()) {
                log.info("file1文件存在-yes");
            } else {
                log.info("file1文件不存在-no");
            }
            //url路径
//            imageYiJian.setUrl(filePath1);
            //字节流读取
            imageYiJian.setData(FileUtil.readBytes(file1));
            //设置读取图⽚⽅式必须
//            imageYiJian.setType(ImageEntity.URL);
            imageYiJian.setType(ImageEntity.Data);
            if (zgsMattermaterialList.get(0).getZgsAttachappendixList().size() > 1) {
                imageYiJian1.setHeight(600);
                imageYiJian1.setWidth(500);
                filePath2 = upload + File.separator + zgsMattermaterialList.get(0).getZgsAttachappendixList().get(1).getAppendixpath();
                filePath2 = LibreOfficeUtil.wordConverterToJpg(filePath2);
                log.info("filePath2=" + filePath2);
                File file2 = new File(filePath2);
                if (file2.exists()) {
                    log.info("file2文件存在-yes");
                } else {
                    log.info("file2文件不存在-no");
                }
//                imageYiJian1.setUrl(filePath2);
                imageYiJian1.setData(FileUtil.readBytes(file2));
                //设置读取图⽚⽅式必须
//                imageYiJian1.setType(ImageEntity.URL);
                imageYiJian1.setType(ImageEntity.Data);
            } else {
                imageYiJian1 = null;
            }
            imageQianZi.setHeight(280);
            imageQianZi.setWidth(580);
            filePath3 = upload + File.separator + zgsMattermaterialList.get(1).getZgsAttachappendixList().get(0).getAppendixpath();
            filePath3 = LibreOfficeUtil.wordConverterToJpg(filePath3);
            log.info("filePath3=" + filePath3);
            File file3 = new File(filePath3);
            if (file3.exists()) {
                log.info("file3文件存在-yes");
            } else {
                log.info("file3文件不存在-no");
            }
//            imageQianZi.setUrl(filePath3);
            imageQianZi.setData(FileUtil.readBytes(file3));
            //设置读取图⽚⽅式必须
//            imageQianZi.setType(ImageEntity.URL);
            imageQianZi.setType(ImageEntity.Data);
            if (zgsMattermaterialList.get(1).getZgsAttachappendixList().size() > 1) {
                imageQianZi1.setHeight(280);
                imageQianZi1.setWidth(580);
                filePath4 = upload + File.separator + zgsMattermaterialList.get(1).getZgsAttachappendixList().get(1).getAppendixpath();
                filePath4 = LibreOfficeUtil.wordConverterToJpg(filePath4);
                log.info("filePath4=" + filePath4);
                File file4 = new File(filePath4);
                if (file4.exists()) {
                    log.info("file4文件存在-yes");
                } else {
                    log.info("file4文件不存在-no");
                }
//                imageQianZi1.setUrl(filePath4);
                imageQianZi1.setData(FileUtil.readBytes(file4));
                //设置读取图⽚⽅式必须
//                imageQianZi1.setType(ImageEntity.URL);
                imageQianZi1.setType(ImageEntity.Data);
            } else {
                imageQianZi1 = null;
            }
        }
        templateMap.put("imageYiJian", imageYiJian);
        templateMap.put("imageYiJian1", imageYiJian1);
        templateMap.put("imageQianZi", imageQianZi);
        templateMap.put("imageQianZi1", imageQianZi1);
        //图片处理-end
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }


    /**
     * @describe: 文件归档
     * @author: renxiaoliang
     * @date: 2024/1/12 17:03
     */
    @AutoLog(value = "项目文件归档")
    @ApiOperation(value = "项目文件归档", notes = "项目文件归档")
    @PostMapping(value = "/induce")
    public Result<?> fileInduce(@RequestBody Map<String,Object> params) {
        Result result = new Result();
        UpdateWrapper<ZgsSacceptcertificatebase> updateWrapper = new UpdateWrapper<>();
        String id = params.get("id").toString();
        String fileState = params.get("fileState").toString();
        if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(fileState)) {
            updateWrapper.set("file_induce", fileState);
            updateWrapper.eq("id", id);
            boolean bol = zgsSacceptcertificatebaseService.update(updateWrapper);
            if (fileState.equals("1") && bol) {
                result.setSuccess(true);
                result.setMessage("已归档");
            } else if (fileState.equals("2") && bol) {
                result.setSuccess(true);
                result.setMessage("取消归档");
            } else {
                result.setSuccess(false);
                result.setMessage("归档失败");
            }

        }

        return result;
    }




}
