package org.jeecg.modules.green.sfxmsb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 装配式示范工程单体建筑基本信息
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_assemblesingleproject")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_assemblesingleproject对象", description = "装配式示范工程单体建筑基本信息")
public class ZgsAssemblesingleproject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 对应业务表T_ASSEMBLEPROJECT主键
     */
    @Excel(name = "对应业务表T_ASSEMBLEPROJECT主键", width = 15)
    @ApiModelProperty(value = "对应业务表T_ASSEMBLEPROJECT主键")
    private java.lang.String buildguid;
    /**
     * 建筑楼号
     */
    @Excel(name = "建筑楼号", width = 15)
    @ApiModelProperty(value = "建筑楼号")
    private java.lang.String buildingnum;
    /**
     * 建筑面积（m2）
     */
    @Excel(name = "建筑面积（m2）", width = 15)
    @ApiModelProperty(value = "建筑面积（m2）")
    private java.math.BigDecimal buildingarea;
    /**
     * 建筑层数
     */
    @Excel(name = "建筑层数", width = 15)
    @ApiModelProperty(value = "建筑层数")
    private java.math.BigDecimal buildingfloor;
    /**
     * 建筑高度（m）
     */
    @Excel(name = "建筑高度（m）", width = 15)
    @ApiModelProperty(value = "建筑高度（m）")
    private java.math.BigDecimal buildingheight;
    /**
     * 装配式建筑评价等级
     */
    @Excel(name = "装配式建筑评价等级", width = 15)
    @ApiModelProperty(value = "装配式建筑评价等级")
    private java.lang.String assbuildstarlevel;
    /**
     * 绿色建筑星级标准
     */
    @Excel(name = "绿色建筑星级标准", width = 15)
    @ApiModelProperty(value = "绿色建筑星级标准")
    private java.lang.String greenbuildstarlevel;
    /**
     * 绿色建筑星级标准:字典GreenBuildStarLevel
     */
    @Excel(name = "绿色建筑星级标准:字典GreenBuildStarLevel", width = 15)
    @ApiModelProperty(value = "绿色建筑星级标准:字典GreenBuildStarLevel")
    private java.lang.String greenbuildstarlevelnum;
    /**
     * 项目开工时间
     */
    @Excel(name = "项目开工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目开工时间")
    private java.util.Date projectstartdate;
    /**
     * 项目竣工时间
     */
    @Excel(name = "项目竣工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目竣工时间")
    private java.util.Date projectenddate;
    /**
     * 装配式类型
     */
    @Excel(name = "装配式类型", width = 15)
    @ApiModelProperty(value = "装配式类型")
    private java.lang.String assembletype;
    /**
     * 装配式类型:字典AssembleType
     */
    @Excel(name = "装配式类型:字典AssembleType", width = 15)
    @ApiModelProperty(value = "装配式类型:字典AssembleType")
    private java.lang.String assembletypenum;
    /**
     * 结构类型
     */
    @Excel(name = "结构类型", width = 15)
    @ApiModelProperty(value = "结构类型")
    private java.lang.String structuretype;
    /**
     * 结构类型:字典StructureType
     */
    @Excel(name = "结构类型:字典StructureType", width = 15)
    @ApiModelProperty(value = "结构类型:字典StructureType")
    private java.lang.String structuretypenum;
    /**
     * 结构类型为其他结构时填写
     */
    @Excel(name = "结构类型为其他结构时填写", width = 15)
    @ApiModelProperty(value = "结构类型为其他结构时填写")
    private java.lang.String structuretyperemark;
    /**
     * 设计单位
     */
    @Excel(name = "设计单位", width = 15)
    @ApiModelProperty(value = "设计单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designunit;
    /**
     * 施工单位
     */
    @Excel(name = "施工单位", width = 15)
    @ApiModelProperty(value = "施工单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructunit;
    /**
     * 装修单位
     */
    @Excel(name = "装修单位", width = 15)
    @ApiModelProperty(value = "装修单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String decorationunit;
    /**
     * 构件供应单位
     */
    @Excel(name = "构件供应单位", width = 15)
    @ApiModelProperty(value = "构件供应单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String componentsupplyunit;
    /**
     * 监理单位
     */
    @Excel(name = "监理单位", width = 15)
    @ApiModelProperty(value = "监理单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchunit;
    /**
     * 装配式混凝土结构建筑类型
     */
    @Excel(name = "装配式混凝土结构建筑类型", width = 15)
    @ApiModelProperty(value = "装配式混凝土结构建筑类型")
    private java.lang.String concretebuildtype;
    /**
     * 装配式混凝土结构建筑类型:字典ConcreteBuildType
     */
    @Excel(name = "装配式混凝土结构建筑类型:字典ConcreteBuildType", width = 15)
    @ApiModelProperty(value = "装配式混凝土结构建筑类型:字典ConcreteBuildType")
    private java.lang.String concretebuildtypenum;
    /**
     * 装配式混凝土结构建筑预制构件部分的混凝土用量（m3）
     */
    @Excel(name = "装配式混凝土结构建筑预制构件部分的混凝土用量（m3）", width = 15)
    @ApiModelProperty(value = "装配式混凝土结构建筑预制构件部分的混凝土用量（m3）")
    private java.math.BigDecimal concretememberamount;
    /**
     * 装配式混凝土结构建筑混凝土总用量（m3）
     */
    @Excel(name = "装配式混凝土结构建筑混凝土总用量（m3）", width = 15)
    @ApiModelProperty(value = "装配式混凝土结构建筑混凝土总用量（m3）")
    private java.math.BigDecimal concretetotalamount;
    /**
     * 钢结构建筑墙板类型
     */
    @Excel(name = "钢结构建筑墙板类型", width = 15)
    @ApiModelProperty(value = "钢结构建筑墙板类型")
    private java.lang.String steelwallboardtype;
    /**
     * 钢结构建筑墙板类型:字典SteelWallboardType
     */
    @Excel(name = "钢结构建筑墙板类型:字典SteelWallboardType", width = 15)
    @ApiModelProperty(value = "钢结构建筑墙板类型:字典SteelWallboardType")
    private java.lang.String steelwallboardtypenum;
    /**
     * 钢结构建筑墙板预制构件部分的混凝土用量（m3）
     */
    @Excel(name = "钢结构建筑墙板预制构件部分的混凝土用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑墙板预制构件部分的混凝土用量（m3）")
    private java.math.BigDecimal steelwallmemberamount;
    /**
     * 钢结构建筑墙板混凝土总用量（m3）
     */
    @Excel(name = "钢结构建筑墙板混凝土总用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑墙板混凝土总用量（m3）")
    private java.math.BigDecimal steelwalltotalamount;
    /**
     * 钢结构建筑预制叠合楼板
     */
    @Excel(name = "钢结构建筑预制叠合楼板", width = 15)
    @ApiModelProperty(value = "钢结构建筑预制叠合楼板")
    private java.lang.String laminatedslab;
    /**
     * 钢结构建筑预制叠合楼板类型:字典LaminatedsType
     */
    @Excel(name = "钢结构建筑预制叠合楼板类型:字典LaminatedsType", width = 15)
    @ApiModelProperty(value = "钢结构建筑预制叠合楼板类型:字典LaminatedsType")
    private java.lang.String laminatedslabnum;
    /**
     * 钢结构建筑预制叠合楼板混凝土用量（m3）
     */
    @Excel(name = "钢结构建筑预制叠合楼板混凝土用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑预制叠合楼板混凝土用量（m3）")
    private java.math.BigDecimal laminatedsmemberamount;
    /**
     * 钢结构建筑预制叠合楼板混凝土总用量（m3）
     */
    @Excel(name = "钢结构建筑预制叠合楼板混凝土总用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑预制叠合楼板混凝土总用量（m3）")
    private java.math.BigDecimal laminatedstotalamount;
    /**
     * 钢结构建筑其他板墙
     */
    @Excel(name = "钢结构建筑其他板墙", width = 15)
    @ApiModelProperty(value = "钢结构建筑其他板墙")
    private java.lang.String buildpacetype;
    /**
     * 钢结构建筑其他板墙:字典BuildPaceType
     */
    @Excel(name = "钢结构建筑其他板墙:字典BuildPaceType", width = 15)
    @ApiModelProperty(value = "钢结构建筑其他板墙:字典BuildPaceType")
    private java.lang.String buildpacetypenum;
    /**
     * 钢结构建筑其他板墙说明，选择“其他”选项时用
     */
    @Excel(name = "钢结构建筑其他板墙说明，选择“其他”选项时用", width = 15)
    @ApiModelProperty(value = "钢结构建筑其他板墙说明，选择“其他”选项时用")
    private java.lang.String buildpaceremark;
    /**
     * 钢结构建筑其他板墙混凝土用量（m3）
     */
    @Excel(name = "钢结构建筑其他板墙混凝土用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑其他板墙混凝土用量（m3）")
    private java.math.BigDecimal buildpacememberamount;
    /**
     * 钢结构建筑其他板墙混凝土总用量（m3）
     */
    @Excel(name = "钢结构建筑其他板墙混凝土总用量（m3）", width = 15)
    @ApiModelProperty(value = "钢结构建筑其他板墙混凝土总用量（m3）")
    private java.math.BigDecimal buildpacetotalamount;
    /**
     * 主要为混凝土材料实际得分值
     */
    @Excel(name = "主要为混凝土材料实际得分值", width = 15)
    @ApiModelProperty(value = "主要为混凝土材料实际得分值")
    private java.math.BigDecimal concretescore;
    /**
     * 金属材料、木材及非水泥基复合材料实际得分值
     */
    @Excel(name = "金属材料、木材及非水泥基复合材料实际得分值", width = 15)
    @ApiModelProperty(value = "金属材料、木材及非水泥基复合材料实际得分值")
    private java.math.BigDecimal metalwoodnonscore;
    /**
     * 梁、板、楼梯、阳台、空调板等实际得分值
     */
    @Excel(name = "梁、板、楼梯、阳台、空调板等实际得分值", width = 15)
    @ApiModelProperty(value = "梁、板、楼梯、阳台、空调板等实际得分值")
    private java.math.BigDecimal bbsbabscore;
    /**
     * 外围护墙非砌筑实际得分值
     */
    @Excel(name = "外围护墙非砌筑实际得分值", width = 15)
    @ApiModelProperty(value = "外围护墙非砌筑实际得分值")
    private java.math.BigDecimal extwallnonnounscore;
    /**
     * 外围护墙墙体与保温（隔热）、装饰一体化实际得分值
     */
    @Excel(name = "外围护墙墙体与保温（隔热）、装饰一体化实际得分值", width = 15)
    @ApiModelProperty(value = "外围护墙墙体与保温（隔热）、装饰一体化实际得分值")
    private java.math.BigDecimal wallinsulationscore;
    /**
     * 内隔墙非砌筑实际得分值
     */
    @Excel(name = "内隔墙非砌筑实际得分值", width = 15)
    @ApiModelProperty(value = "内隔墙非砌筑实际得分值")
    private java.math.BigDecimal innerwallnonnounscore;
    /**
     * 内隔墙墙体与管线、装修一体化实际得分值
     */
    @Excel(name = "内隔墙墙体与管线、装修一体化实际得分值", width = 15)
    @ApiModelProperty(value = "内隔墙墙体与管线、装修一体化实际得分值")
    private java.math.BigDecimal innerwallleadoutscore;
    /**
     * 装修与设备管线全装修实际得分值
     */
    @Excel(name = "装修与设备管线全装修实际得分值", width = 15)
    @ApiModelProperty(value = "装修与设备管线全装修实际得分值")
    private java.math.BigDecimal fullfitupscore;
    /**
     * 装修与设备管线干式工法楼（地）面实际得分值
     */
    @Excel(name = "装修与设备管线干式工法楼（地）面实际得分值", width = 15)
    @ApiModelProperty(value = "装修与设备管线干式工法楼（地）面实际得分值")
    private java.math.BigDecimal drysurfacescore;
    /**
     * 装修与设备管线集成卫生间实际得分值
     */
    @Excel(name = "装修与设备管线集成卫生间实际得分值", width = 15)
    @ApiModelProperty(value = "装修与设备管线集成卫生间实际得分值")
    private java.math.BigDecimal integrationscore;
    /**
     * 装修与设备管线集成厨房实际得分值
     */
    @Excel(name = "装修与设备管线集成厨房实际得分值", width = 15)
    @ApiModelProperty(value = "装修与设备管线集成厨房实际得分值")
    private java.math.BigDecimal integratedkitchenscore;
    /**
     * 装修与设备管线管线与结构分离实际得分值
     */
    @Excel(name = "装修与设备管线管线与结构分离实际得分值", width = 15)
    @ApiModelProperty(value = "装修与设备管线管线与结构分离实际得分值")
    private java.math.BigDecimal linepipeleavescore;
    /**
     * 单体工程排序号
     */
    @Excel(name = "单体工程排序号", width = 15)
    @ApiModelProperty(value = "单体工程排序号")
    private java.math.BigDecimal ordernum;
    /**
     * 装配率
     */
    @Excel(name = "装配率", width = 15)
    @ApiModelProperty(value = "装配率")
    private java.math.BigDecimal assemblyrate;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
