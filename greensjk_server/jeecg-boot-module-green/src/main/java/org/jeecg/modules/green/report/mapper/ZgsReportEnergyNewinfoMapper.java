package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 新建绿色建筑-节能项目清单
 * @Author: jeecg-boot
 * @Date:   2022-05-19
 * @Version: V1.0
 */
public interface ZgsReportEnergyNewinfoMapper extends BaseMapper<ZgsReportEnergyNewinfo> {

}
