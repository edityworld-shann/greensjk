package org.jeecg.modules.green.common.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;

import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description: 建设科技攻关项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-10
 * @Version: V1.0
 */
public interface IZgsSciencetechfeasibleService extends IService<ZgsSciencetechfeasible> {
    ZgsSciencetechfeasible getBySciencetechguid(String sciencetechguid);

    Map<String, String> getExportXlsData(String id, Map<String, String> map);

    int updateAddMoney(ZgsSciencetechfeasible zgsSciencetechfeasible);

    int rollBackSp(String id);

    // List<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(Wrapper<ZgsSciencetechfeasible> queryWrapper);

    Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(Wrapper<ZgsSciencetechfeasible> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleListForExport(String fieldStr,Wrapper<ZgsSciencetechfeasible> queryWrapper, Integer pageNo, Integer pageSize);

}
