package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 既有建筑-节能改造项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface IZgsReportEnergyExistinfoService extends IService<ZgsReportEnergyExistinfo> {

    public void addIterm(String filltm);

    public void addItermInitTask(String filltm, String createpersonaccount, String createpersonname, String areacode, String areaname);

}
