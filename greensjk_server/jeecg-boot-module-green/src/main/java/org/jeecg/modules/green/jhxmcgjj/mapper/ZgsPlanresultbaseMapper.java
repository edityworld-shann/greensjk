package org.jeecg.modules.green.jhxmcgjj.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目成果简介
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface ZgsPlanresultbaseMapper extends BaseMapper<ZgsPlanresultbase> {
    Page<ZgsPlanresultbase> listZgsPlanResultBaseListInfo(Page<ZgsPlanresultbase> page, @Param(Constants.WRAPPER) Wrapper<ZgsPlanresultbase> queryWrapper);

    Page<ZgsPlanresultbase> getResultInfo(Page<ZgsPlanresultbase> page, @Param(Constants.WRAPPER) Wrapper<ZgsPlanresultbase> queryWrapper);

}
