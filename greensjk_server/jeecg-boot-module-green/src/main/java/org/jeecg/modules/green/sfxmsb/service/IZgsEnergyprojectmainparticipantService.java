package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergyprojectmainparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 节能建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsEnergyprojectmainparticipantService extends IService<ZgsEnergyprojectmainparticipant> {

}
