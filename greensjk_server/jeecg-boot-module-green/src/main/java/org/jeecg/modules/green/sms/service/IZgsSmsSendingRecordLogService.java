package org.jeecg.modules.green.sms.service;

import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 短信记录表
 * @Author: jeecg-boot
 * @Date:   2022-06-07
 * @Version: V1.0
 */
public interface IZgsSmsSendingRecordLogService extends IService<ZgsSmsSendingRecordLog> {

}
