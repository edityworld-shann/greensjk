package org.jeecg.modules.green.jxzpsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.sfxmsb.entity.ZgsCommonFee;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 绩效自评申请
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_performancebase")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_performancebase对象", description = "绩效自评申请")
public class ZgsPerformancebase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 读取数据基础库主键
     */
//    @Excel(name = "读取数据基础库主键", width = 15, orderNum = "2")
    @ApiModelProperty(value = "读取数据基础库主键")
    private java.lang.String baseguid;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 评价类型
     */
    //@Excel(name = "评价类型", width = 15, orderNum = "2")
    @ApiModelProperty(value = "评价类型")
    private java.lang.String type;
    /**
     * 管理概况简述
     */
    // @Excel(name = "管理概况简述", width = 15, orderNum = "2")
    @ApiModelProperty(value = "管理概况简述")
    private java.lang.String managebasicfact;
    /**
     * 进展概况简述
     */
    //@Excel(name = "进展概况简述", width = 15, orderNum = "2")
    @ApiModelProperty(value = "进展概况简述")
    private java.lang.String evolvebasicfact;
    /**
     * 财务概况简述
     */
    //@Excel(name = "财务概况简述", width = 15, orderNum = "2")
    @ApiModelProperty(value = "财务概况简述")
    private java.lang.String financebasicfact;
    /**
     * 财务管理情况
     */
    //@Excel(name = "财务管理情况", width = 15, orderNum = "2")
    @ApiModelProperty(value = "财务管理情况")
    private java.lang.String financemanagement;
    /**
     * 项目效益情况
     */
    @Excel(name = "项目效益情况", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目效益情况")
    private java.lang.String projectbenefit;
    /**
     * 自评价结果
     */
    @Excel(name = "自评价结果", width = 15, orderNum = "2")
    @ApiModelProperty(value = "自评价结果")
    private java.lang.String evaluationresult;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
//    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * createaccountname
     */
//    @Excel(name = "createaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
//    @Excel(name = "createusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
//    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
//    @Excel(name = "modifyaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
//    @Excel(name = "modifyusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
//    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    private java.lang.String higherupunitnum;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 实施单位
     */
    @Excel(name = "实施单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "实施单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String implementunit;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 项目实施进展情况
     */
    @Excel(name = "项目实施进展情况", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目实施进展情况")
    private java.lang.String executionsprogress;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    @TableField(exist = false)
    private ZgsPerformancescore zgsPerformancescore;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    @TableField(exist = false)
    private Object cstatus;
    /**
     * 0结题验收绩效自评,1年度绩效自评
     */
    @ApiModelProperty(value = "0结题验收绩效自评,1年度绩效自评")
    private java.lang.String intype;
    /**
     * 0省级科技资金未支持项目,1省级科技资金支持项目
     */
    @ApiModelProperty(value = "0省级科技资金未支持项目,1省级科技资金支持项目")
    private java.lang.String fundType;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人")
    @TableField(exist = false)
    private java.lang.String projectleader;
    /**
     * 项目负责人手机号
     */
    @Excel(name = "项目负责人手机号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人手机号")
    @TableField(exist = false)
    private java.lang.String headmobile;
    @TableField(exist = false)
    private ZgsCommonFee zgsCommonFee;
    @TableField(exist = false)
    private List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;

}
