package org.jeecg.modules.green.kyxmjtsq.service.impl;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostelist;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostelistMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostelistService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostelistServiceImpl extends ServiceImpl<ZgsScientificpostelistMapper, ZgsScientificpostelist> implements IZgsScientificpostelistService {

}
