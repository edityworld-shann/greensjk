package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertworkresume;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertworkresumeMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertworkresumeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库人员工作简历
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertworkresumeServiceImpl extends ServiceImpl<ZgsExpertworkresumeMapper, ZgsExpertworkresume> implements IZgsExpertworkresumeService {

}
