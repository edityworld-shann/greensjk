package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityCompletedMapper;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityProductioncapacityMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompanyService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompletedService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProductioncapacityService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 装配式建筑-生产产能-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreaCityProductioncapacityServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaCityProductioncapacityMapper, ZgsReportMonthfabrAreaCityProductioncapacity> implements IZgsReportMonthfabrAreaCityProductioncapacityService {

    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService zgsReportMonthfabrAreaCityCompanyService;

    @Autowired
    private ZgsReportMonthfabrAreaCityCompletedMapper zgsReportMonthfabrAreaCityCompletedMapper;

    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;

    @Autowired
    private IZgsReportMonthfabrAreaCityProjectService zgsReportMonthfabrAreaCityProjectService;

    @Autowired
    private ZgsReportMonthfabrAreaCityProductioncapacityMapper zgsReportMonthfabrAreaCityProductioncapacityMapper;

    /**
     * 清零 0
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrAreaTotalZero(String year, int quarter, String yearMonth, String projecttype) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityProductioncapacity();
        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        zgsReportMonthfabrAreaCityCompleted.setBackreason("");

        //计算当月上报合计
        //1、查汇总当年最新一条上报记录
        //新方案：每次上报重新计算年累计值
        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityProductioncapacity reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
        //3、本月记录+当年最新一条上报记录

        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZz(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjz(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZzCount(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjzCount(new BigDecimal(0));

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany1(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn1(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx1(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn1(new BigDecimal(0));

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany2(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn2(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx2(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn2(new BigDecimal(0));

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany3(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn3(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx3(new BigDecimal(0));
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn3(new BigDecimal(0));

        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", sysUser.getAreacode());
        queryW.isNull("area_type");
        ZgsReportMonthfabrAreaCityProductioncapacity energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        }
        //上报完修改上报状态
        QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
        wrapper.eq("filltm", yearMonth);
        wrapper.eq("areacode", sysUser.getAreacode());
        ZgsReportMonthfabrAreaCityCompany energyNewinfo = new ZgsReportMonthfabrAreaCityCompany();
        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        energyNewinfo.setBackreason("");
        energyNewinfo.setApplydate(new Date());
        energyNewinfo.setBuildingArea(null);
        zgsReportMonthfabrAreaCityCompanyService.update(energyNewinfo, wrapper);
    }

    /**
     * 区县 一键上报
     * loadTreeRoot
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrAreaTotalALL(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityProductioncapacity();
        if (type == 0) {
            zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
        }
        if (applyState != null) {
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(applyState));
        }
        ZgsReportMonthfabrAreaCityProductioncapacity energyNewinfoTotal = null;
        //新方案：每次上报重新计算年累计值
//    energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, sysUser.getAreacode(),year, projecttype);
        //2、计算本月所有汇总记录
        ZgsReportMonthfabrAreaCityProductioncapacity reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, areacode, null);
        //3、本月记录+当年最新一条上报记录
        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZz(reportEnergyNewinfoTotal.getMonthProdZz());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjz(reportEnergyNewinfoTotal.getMonthProdGgjz());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZzCount(reportEnergyNewinfoTotal.getMonthProdZzCount());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjzCount(reportEnergyNewinfoTotal.getMonthProdGgjzCount());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany1(reportEnergyNewinfoTotal.getMonthProdCompany1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn1(reportEnergyNewinfoTotal.getMonthProdSjcn1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx1(reportEnergyNewinfoTotal.getMonthProdScx1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn1(reportEnergyNewinfoTotal.getMonthProdShijicn1());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany2(reportEnergyNewinfoTotal.getMonthProdCompany2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn2(reportEnergyNewinfoTotal.getMonthProdSjcn2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx2(reportEnergyNewinfoTotal.getMonthProdScx2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn2(reportEnergyNewinfoTotal.getMonthProdShijicn2());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany3(reportEnergyNewinfoTotal.getMonthProdCompany3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn3(reportEnergyNewinfoTotal.getMonthProdSjcn3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx3(reportEnergyNewinfoTotal.getMonthProdScx3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn3(reportEnergyNewinfoTotal.getMonthProdShijicn3());

        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNull("area_type");
        ZgsReportMonthfabrAreaCityProductioncapacity energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
                zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            }
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                //新增
                zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
                }
                zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
                zgsReportMonthfabrAreaCityCompleted.setAreacode(areacode);
                zgsReportMonthfabrAreaCityCompleted.setAreaname(areaname);
                zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
                zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
                zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        if (type == 0) {
            //只有手动上报才可以新增,数据同步时无需新增
            //上报完修改上报状态
            QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
            wrapper.eq("filltm", yearMonth);
            wrapper.eq("areacode", areacode);
            ZgsReportMonthfabrAreaCityCompany energyNewinfo = new ZgsReportMonthfabrAreaCityCompany();
            energyNewinfo.setApplystate(new BigDecimal(applyState));
            energyNewinfo.setBackreason("");
            energyNewinfo.setApplydate(new Date());
            energyNewinfo.setBuildingArea(null);
            zgsReportMonthfabrAreaCityCompanyService.update(energyNewinfo, wrapper);
        }
    }


    /**
     * 区县 一键上报
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
//    @Override
//    public void initMonthfabrAreaTotal(String year, int quarter, String yearMonth, String projecttype) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityProductioncapacity();
//        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
//        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportMonthfabrAreaCityCompleted.setBackreason("");
////        BeanUtils.copyProperties(zgsReportEnergyNewinfo, zgsReportMonthfabrAreaCityCompleted);
//        //计算当月上报合计
//        //1、查汇总当年最新一条上报记录
////    QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapper = new QueryWrapper();
////    queryWrapper.ne("isdelete", 1);
////    queryWrapper.eq("year", year);
////    queryWrapper.eq("areacode", sysUser.getAreacode());
////    queryWrapper.orderByDesc("applydate");
////    queryWrapper.last("LIMIT 1");
////        List<ZgsReportMonthfabrAreaCityCompleted> newinfoTotalList = this.baseMapper.selectList(queryWrapper);
//
////        if (newinfoTotalList.size() > 0) {
////            energyNewinfoTotal = newinfoTotalList.get(0);
////        }
////    if (energyNewinfoTotal == null) {
////            energyNewinfoTotal = new ZgsReportMonthfabrAreaCityCompleted();
////    }
//
//        ZgsReportMonthfabrAreaCityProductioncapacity energyNewinfoTotal = null;
//
//        //新方案：每次上报重新计算年累计值
////    energyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataYear(yearMonth, sysUser.getAreacode(),year, projecttype);
//
//        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityProductioncapacity reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrAreaDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
//
//        //3、本月记录+当年最新一条上报记录
//        //  月
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdZz(reportEnergyNewinfoTotal.getMonthProdZz());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjz(reportEnergyNewinfoTotal.getMonthProdGgjz());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdZzCount(reportEnergyNewinfoTotal.getMonthProdZzCount());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjzCount(reportEnergyNewinfoTotal.getMonthProdGgjzCount());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany1(reportEnergyNewinfoTotal.getMonthProdCompany1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn1(reportEnergyNewinfoTotal.getMonthProdSjcn1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx1(reportEnergyNewinfoTotal.getMonthProdScx1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn1(reportEnergyNewinfoTotal.getMonthProdShijicn1());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany2(reportEnergyNewinfoTotal.getMonthProdCompany2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn2(reportEnergyNewinfoTotal.getMonthProdSjcn2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx2(reportEnergyNewinfoTotal.getMonthProdScx2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn2(reportEnergyNewinfoTotal.getMonthProdShijicn2());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany3(reportEnergyNewinfoTotal.getMonthProdCompany3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn3(reportEnergyNewinfoTotal.getMonthProdSjcn3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx3(reportEnergyNewinfoTotal.getMonthProdScx3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn3(reportEnergyNewinfoTotal.getMonthProdShijicn3());
//
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNull("area_type");
//        ZgsReportMonthfabrAreaCityProductioncapacity energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
//            this.baseMapper.updateById(zgsReportMonthfabrAreaCityCompleted);
//        } else {
//            //新增
//            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
//            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
//            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
//            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
//            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
//            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
//            this.baseMapper.insert(zgsReportMonthfabrAreaCityCompleted);
//        }
//        //上报完修改上报状态
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", yearMonth);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        ZgsReportMonthfabrAreaCityCompany energyNewinfo = new ZgsReportMonthfabrAreaCityCompany();
//        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        energyNewinfo.setBackreason("");
//        energyNewinfo.setApplydate(new Date());
//        energyNewinfo.setBuildingArea(null);
//        zgsReportMonthfabrAreaCityCompanyService.update(energyNewinfo, wrapper);
//    }

    /**
     * 市州 一键上报
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
//    @Override
//    public void initMonthfabrCityTotal(String year, int quarter, String yearMonth, String projecttype) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityProductioncapacity();
//        zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
//        zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportMonthfabrAreaCityCompleted.setBackreason("");
//
//        //1、新方案：每次上报重新计算年累计值
////    ZgsReportMonthfabrAreaCityProductioncapacity energyNewinfoTotal = this.baseMapper.totalMonthfabrCityDataYear(yearMonth.substring(0,4), sysUser.getAreacode(), projecttype);
//
//        //2、计算本月所有汇总记录
//        ZgsReportMonthfabrAreaCityProductioncapacity reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrCityDataMonth(yearMonth, sysUser.getAreacode(), projecttype);
//
//        //3、本月记录+当年最新一条上报记录
//        //  月
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdZz(reportEnergyNewinfoTotal.getMonthProdZz());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjz(reportEnergyNewinfoTotal.getMonthProdGgjz());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdZzCount(reportEnergyNewinfoTotal.getMonthProdZzCount());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjzCount(reportEnergyNewinfoTotal.getMonthProdGgjzCount());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany1(reportEnergyNewinfoTotal.getMonthProdCompany1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn1(reportEnergyNewinfoTotal.getMonthProdSjcn1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx1(reportEnergyNewinfoTotal.getMonthProdScx1());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn1(reportEnergyNewinfoTotal.getMonthProdShijicn1());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany2(reportEnergyNewinfoTotal.getMonthProdCompany2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn2(reportEnergyNewinfoTotal.getMonthProdSjcn2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx2(reportEnergyNewinfoTotal.getMonthProdScx2());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn2(reportEnergyNewinfoTotal.getMonthProdShijicn2());
//
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany3(reportEnergyNewinfoTotal.getMonthProdCompany3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn3(reportEnergyNewinfoTotal.getMonthProdSjcn3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx3(reportEnergyNewinfoTotal.getMonthProdScx3());
//        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn3(reportEnergyNewinfoTotal.getMonthProdShijicn3());
//
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNotNull("area_type");
//        ZgsReportMonthfabrAreaCityProductioncapacity energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
//            this.baseMapper.updateById(zgsReportMonthfabrAreaCityCompleted);
//        } else {
//            //新增
//            zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
//            zgsReportMonthfabrAreaCityCompleted.setAreacode(sysUser.getAreacode());
//            zgsReportMonthfabrAreaCityCompleted.setAreaname(sysUser.getAreaname());
//            zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setFillpersonname(sysUser.getRealname());
//            zgsReportMonthfabrAreaCityCompleted.setFillpersontel(sysUser.getPhone());
//            zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
//            zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
//            zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
//            zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
//            zgsReportMonthfabrAreaCityCompleted.setAreaType("1");
//            this.baseMapper.insert(zgsReportMonthfabrAreaCityCompleted);
//        }
//    }

    /**
     * 市州汇总一键上报（定时任务）
     *
     * @param year
     * @param quarter
     * @param yearMonth
     */
    @Override
    public void initMonthfabrCityTotalALL(int type, Integer applyState, String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser) {
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityProductioncapacity();
        if (type == 0) {
            zgsReportMonthfabrAreaCityCompleted.setApplydate(new Date());
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
        }
        if (applyState != null) {
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
//    ZgsReportMonthfabrAreaCityProductioncapacity energyNewinfoTotal = this.baseMapper.totalMonthfabrCityDataYear(yearMonth.substring(0,4), sysUser.getAreacode(), projecttype);
        //2、计算本月所有汇总记录
        ZgsReportMonthfabrAreaCityProductioncapacity reportEnergyNewinfoTotal = this.baseMapper.totalMonthfabrCityDataMonth(yearMonth, areacode, null);
        //3、本月记录+当年最新一条上报记录
        //  月
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZz(reportEnergyNewinfoTotal.getMonthProdZz());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjz(reportEnergyNewinfoTotal.getMonthProdGgjz());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdZzCount(reportEnergyNewinfoTotal.getMonthProdZzCount());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdGgjzCount(reportEnergyNewinfoTotal.getMonthProdGgjzCount());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany1(reportEnergyNewinfoTotal.getMonthProdCompany1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn1(reportEnergyNewinfoTotal.getMonthProdSjcn1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx1(reportEnergyNewinfoTotal.getMonthProdScx1());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn1(reportEnergyNewinfoTotal.getMonthProdShijicn1());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany2(reportEnergyNewinfoTotal.getMonthProdCompany2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn2(reportEnergyNewinfoTotal.getMonthProdSjcn2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx2(reportEnergyNewinfoTotal.getMonthProdScx2());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn2(reportEnergyNewinfoTotal.getMonthProdShijicn2());

        zgsReportMonthfabrAreaCityCompleted.setMonthProdCompany3(reportEnergyNewinfoTotal.getMonthProdCompany3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdSjcn3(reportEnergyNewinfoTotal.getMonthProdSjcn3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdScx3(reportEnergyNewinfoTotal.getMonthProdScx3());
        zgsReportMonthfabrAreaCityCompleted.setMonthProdShijicn3(reportEnergyNewinfoTotal.getMonthProdShijicn3());

        //判断是编辑还是新增
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNotNull("area_type");
        ZgsReportMonthfabrAreaCityProductioncapacity energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportMonthfabrAreaCityCompleted.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportMonthfabrAreaCityCompleted.setModifypersonaccount(sysUser.getUsername());
                zgsReportMonthfabrAreaCityCompleted.setModifypersonname(sysUser.getRealname());
            }
            zgsReportMonthfabrAreaCityCompleted.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                zgsReportMonthfabrAreaCityCompleted.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonaccount(sysUser.getUsername());
                    zgsReportMonthfabrAreaCityCompleted.setCreatepersonname(sysUser.getRealname());
                }
                zgsReportMonthfabrAreaCityCompleted.setCreatetime(new Date());
                zgsReportMonthfabrAreaCityCompleted.setAreacode(areacode);
                zgsReportMonthfabrAreaCityCompleted.setAreaname(areaname);
                zgsReportMonthfabrAreaCityCompleted.setFilltm(yearMonth);
                if (sysUser != null) {
                    zgsReportMonthfabrAreaCityCompleted.setFillpersonname(sysUser.getRealname());
                    zgsReportMonthfabrAreaCityCompleted.setFillpersontel(sysUser.getPhone());
                }
                zgsReportMonthfabrAreaCityCompleted.setReporttm(yearMonth);
                zgsReportMonthfabrAreaCityCompleted.setIsdelete(new BigDecimal(0));
                zgsReportMonthfabrAreaCityCompleted.setQuarter(new BigDecimal(quarter));
                zgsReportMonthfabrAreaCityCompleted.setYear(new BigDecimal(year));
                zgsReportMonthfabrAreaCityCompleted.setAreaType("1");
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityCompleted, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }

    @Override
    public void spProject(String id, Integer spStatus, String backreason) {

        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity = new ZgsReportMonthfabrAreaCityProductioncapacity();
        zgsReportMonthfabrAreaCityProductioncapacity.setId(id);
//    zgsReportMonthfabrAreaCityCompleted.setMonthFarArea1(null);
//    zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber1(null);
//    zgsReportMonthfabrAreaCityCompleted.setMonthFarArea2(null);
//    zgsReportMonthfabrAreaCityCompleted.setMonthFarNumber2(null);
//    zgsReportMonthfabrAreaCityCompleted.setMonthFarArea3(null);
        //修改项目清单的状态
        ZgsReportMonthfabrAreaCityProductioncapacity newinfoTotal = this.baseMapper.selectById(id);
        ZgsReportMonthfabrAreaCityCompany energyNewinfo = new ZgsReportMonthfabrAreaCityCompany();
        ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject = new ZgsReportMonthfabrAreaCityProject();
        ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted = new ZgsReportMonthfabrAreaCityCompleted();
        energyNewinfo.setBuildingArea(null);
//    energyNewinfo.setLowEnergy(null);
//    energyNewinfo.setGeothermalEnergy(null);
//    energyNewinfo.setSolarEnergy(null);
//    energyNewinfo.setOtherEnergy(null);
        if (spStatus == 0) {
            //驳回
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            energyNewinfo.setBackreason(backreason);
            zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityProductioncapacity.setBackreason(backreason);
            zgsReportMonthfabrAreaCityProject.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityProject.setBackreason(backreason);
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportMonthfabrAreaCityCompleted.setBackreason(backreason);
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                //  省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- 省厅 -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin --
                //  省厅账号退回后，市州汇总表子项状态改为待审批
                //  省厅驳回区县：省厅只修改 汇总表中 合计的那一条，清单不修改
                //  省厅驳回市州：修改清单，汇总表都需要修改
                // ====--====--====--====--====-- 新开工、已竣工 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                ZgsReportMonthfabrAreaCityCompleted infoXkg = new ZgsReportMonthfabrAreaCityCompleted();
                //        BeanUtils.copyProperties(zgsReportMonthfabrAreaCityProductioncapacity, infoXkg);
                infoXkg.setId(null);
                infoXkg.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                infoXkg.setBackreason("");
                QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperXkg = new QueryWrapper();
                wrapperXkg.eq("filltm", newinfoTotal.getFilltm());
                wrapperXkg.likeRight("areacode", newinfoTotal.getAreacode());
                wrapperXkg.isNull("area_type");
                zgsReportMonthfabrAreaCityCompletedMapper.update(infoXkg, wrapperXkg);
                //
                infoXkg.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperXkg1 = new QueryWrapper();
                wrapperXkg1.eq("filltm", newinfoTotal.getFilltm());
                wrapperXkg1.eq("areacode", newinfoTotal.getAreacode());
                wrapperXkg1.isNotNull("area_type");
                zgsReportMonthfabrAreaCityCompletedMapper.update(infoXkg, wrapperXkg1);
                //省退市，删掉该条汇总数据-不留痕
//                zgsReportMonthfabrAreaCityCompletedMapper.delete(wrapperXkg1);
                // ====--====--====--====--====-- 新开工、已竣工 退回   Begin ====--====--====--====--====--====--====--====--====--====--

                // ====--====--====--====--====-- 生产产能 退回   Begin ====--====--====--====--====--====--====--====--====--====--
                ZgsReportMonthfabrAreaCityProductioncapacity infoSccn = new ZgsReportMonthfabrAreaCityProductioncapacity();
                BeanUtils.copyProperties(zgsReportMonthfabrAreaCityProductioncapacity, infoSccn);
                infoSccn.setId(null);
                infoSccn.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                infoSccn.setBackreason("");
                QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperSccn = new QueryWrapper();
                wrapperSccn.eq("filltm", newinfoTotal.getFilltm());
                wrapperSccn.likeRight("areacode", newinfoTotal.getAreacode());
                wrapperSccn.isNull("area_type");
                zgsReportMonthfabrAreaCityProductioncapacityMapper.update(infoSccn, wrapperSccn);
                infoSccn.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapperSccn1 = new QueryWrapper();
                wrapperSccn1.eq("filltm", newinfoTotal.getFilltm());
                wrapperSccn1.eq("areacode", newinfoTotal.getAreacode());
                wrapperSccn1.isNotNull("area_type");
                zgsReportMonthfabrAreaCityProductioncapacityMapper.update(infoSccn, wrapperSccn1);
                //省退市，删掉该条汇总数据-不留痕
//                this.baseMapper.delete(wrapperSccn1);
                // ====--====--====--====--====-- 生产产能 退回    End ====--====--====--====--====--====--====--====--====--====--
                //  省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- 省厅 -- End -- End -- End -- End -- End -- End -- End --
            }
        } else {
            //通过
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            energyNewinfo.setBackreason("");
            zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityProductioncapacity.setBackreason("");
            zgsReportMonthfabrAreaCityProject.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityProject.setBackreason("");
            zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportMonthfabrAreaCityCompleted.setBackreason("");
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市州账号审批
            //  市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- 市州 -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin -- Begin --
            //  省厅账号退回后，市州汇总表子项状态改为待审批
            //  省厅驳回区县：省厅只修改 汇总表中 合计的那一条，清单不修改
            //  省厅驳回市州：修改清单，汇总表都需要修改
            if (spStatus == 0) {
                QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.eq("areacode", newinfoTotal.getAreacode());
                zgsReportMonthfabrAreaCityCompanyService.update(energyNewinfo, wrapper);
                // ====--====--====--====--====-- 新开工、已竣工  退回   Begin ====--====--====--====--====--====--====--====--====--====--
                QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapperProject = new QueryWrapper();
                wrapperProject.eq("reporttm", newinfoTotal.getFilltm());
                wrapperProject.eq("areacode", newinfoTotal.getAreacode());
                wrapperProject.ne("isdelete", 1);
                zgsReportMonthfabrAreaCityProject.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                zgsReportMonthfabrAreaCityProjectService.update(zgsReportMonthfabrAreaCityProject, wrapperProject);
                QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper();
                queryWrapper.eq("reporttm", newinfoTotal.getFilltm());
                queryWrapper.eq("areacode", newinfoTotal.getAreacode());
                queryWrapper.ne("isdelete", 1);
                zgsReportMonthfabrAreaCityCompleted.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                zgsReportMonthfabrAreaCityCompletedService.update(zgsReportMonthfabrAreaCityCompleted, queryWrapper);
                // ====--====--====--====--====-- 新开工、已竣工  退回    End ====--====--====--====--====--====--====--====--====--====--
                //  市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- 市州 -- End -- End -- End -- End -- End -- End -- End --
            } else {
                QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.eq("areacode", newinfoTotal.getAreacode());
                zgsReportMonthfabrAreaCityCompanyService.update(energyNewinfo, wrapper);
            }
        }
        if (spStatus == 0) {
            QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapper2 = new QueryWrapper();
            wrapper2.eq("filltm", newinfoTotal.getFilltm());
            wrapper2.eq("areacode", newinfoTotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                wrapper2.isNotNull("area_type");
            }
            this.baseMapper.update(zgsReportMonthfabrAreaCityProductioncapacity, wrapper2);
        } else {
            this.baseMapper.updateById(zgsReportMonthfabrAreaCityProductioncapacity);
        }
    }


    @Override
    public ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataProvince(String filltm, Integer applystate) {
        return this.baseMapper.lastTotalDataProvince(filltm, applystate);
    }

    @Override
    public ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataCity(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastTotalDataCity(filltm, areacode, applystate);
    }

    /**
     * 滚屏 - 新开工装配式建筑面积
     *
     * @param filltm
     */
    @Override
    public List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionFabricatAreaData(String filltm) {
        return this.baseMapper.totalNewconstructionFabricatAreaData(filltm);
    }

    /**
     * 滚屏 - 新开工装配式建筑面积
     *
     * @param filltm
     */
    @Override
    public List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionAreaData(String filltm) {
        return this.baseMapper.totalNewconstructionAreaData(filltm);
    }

    /**
     * 滚屏 - 新建 - 建筑面积
     *
     * @param filltm
     */
    @Override
    public List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewBuildingAreaData(String filltm) {
        return this.baseMapper.totalNewBuildingAreaData(filltm);
    }

    /**
     * 滚屏 - 新开工装配式建筑面积
     *
     * @param filltm
     */
    @Override
    public List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingAreaData(String filltm) {
        return this.baseMapper.totalGreenBuildingAreaData(filltm);
    }

    /**
     * 滚屏 - 绿色 - 建筑等级
     *
     * @param filltm
     */
    @Override
    public List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingLevelData(String filltm) {
        return this.baseMapper.totalGreenBuildingLevelData(filltm);
    }

}
