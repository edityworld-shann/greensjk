package org.jeecg.modules.green.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.modules.green.common.mapper.ZgsCommonMapper;
import org.jeecg.modules.green.common.service.IZgsCommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 共用 服务实现类
 * </p>
 *
 * @Author lizhi
 * @since 2022-04-12
 */
@Service
@Slf4j
public class ZgsCommonServiceImpl extends ServiceImpl<ZgsCommonMapper, Object> implements IZgsCommonService {

    @Autowired
    private ZgsCommonMapper zgsCommonMapper;

    /**
     * 通过查询指定code 获取字典值text
     *
     * @param code
     * @param key
     * @return
     */

    @Override
    @Cacheable(value = CacheConstant.SYS_DICT_CACHE, key = "#code+':'+#key", unless = "#result == null ")
    public String queryDictTextByKey(String code, String key) {
        return zgsCommonMapper.queryDictTextByKey(code, key);
    }

    /**
     * 通过查询指定code 获取字典值text
     *
     * @param code
     * @return
     */
    @Override
    public String queryAreaTextByCode(String code) {
        return zgsCommonMapper.queryAreaTextByCode(code);
    }

    @Override
    public String queryApplyYear() {
        return zgsCommonMapper.queryApplyYear();
    }
}
