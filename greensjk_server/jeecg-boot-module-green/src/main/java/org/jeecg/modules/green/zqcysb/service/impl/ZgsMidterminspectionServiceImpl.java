package org.jeecg.modules.green.zqcysb.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.mapper.ZgsMidterminspectionMapper;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecg.modules.utils.SMSTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * @Description: 建设科技示范项目实施情况中期查验申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
@Slf4j
public class ZgsMidterminspectionServiceImpl extends ServiceImpl<ZgsMidterminspectionMapper, ZgsMidterminspection> implements IZgsMidterminspectionService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private SMSTokenUtil smsTokenUtil;

    @Override
    public int spProject(ZgsMidterminspection zgsMidterminspection) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsMidterminspection != null && StringUtils.isNotEmpty(zgsMidterminspection.getId())) {
            ZgsMidterminspection midterminspection = new ZgsMidterminspection();
            midterminspection.setId(zgsMidterminspection.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsMidterminspection.getId());
            zgsReturnrecord.setReturnreason(zgsMidterminspection.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE9);
            ZgsMidterminspection spectionInfo = getById(zgsMidterminspection.getId());
            if (spectionInfo != null) {
                zgsReturnrecord.setEnterpriseguid(spectionInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(spectionInfo.getApplydate());
                zgsReturnrecord.setProjectname(spectionInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", spectionInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsMidterminspection.getSpStatus() == 1) {
                midterminspection.setAuditopinion(null);
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsMidterminspection.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsMidterminspection.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsMidterminspection.getStatus())) {
                    midterminspection.setStatus(GlobalConstants.SHENHE_STATUS4);
                    midterminspection.setFirstdate(new Date());
                    midterminspection.setFirstperson(sysUser.getRealname());
                    midterminspection.setFirstdepartment(sysUser.getEnterprisename());
                    midterminspection.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    midterminspection.setStatus(GlobalConstants.SHENHE_STATUS8);
                    midterminspection.setFinishperson(sysUser.getRealname());
                    midterminspection.setFinishdate(new Date());
                    midterminspection.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    //发送短信内容提醒：请XX单位保留中期查验过程中所有影像资料。
                    if (StringUtils.isNotEmpty(spectionInfo.getLinkphone())) {
                        HashMap<String, Object> fillContent = new HashMap<>();
                        fillContent.put("content", spectionInfo.getExecutiveunit());
                        HashMap<String, Object> paramsMap = new HashMap<>();
                        paramsMap.put("phoneNumber", spectionInfo.getLinkphone());
                        paramsMap.put("smsType", 3);
                        paramsMap.put("companyName", spectionInfo.getExecutiveunit());
                        paramsMap.put("templateId", 24);
                        paramsMap.put("fillContent", fillContent);
                        Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                        if (result.getCode() == 200) {
                            log.info("中期查验提醒发送短信成功！");
                        } else {
                            log.info("中期查验提醒发送短信失败！");
                        }
                    }
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                midterminspection.setAuditopinion(zgsMidterminspection.getAuditopinion());
                //驳回 不通过记录状态==1
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsMidterminspection.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsMidterminspection.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsMidterminspection.getStatus())) {
                    if (zgsMidterminspection.getSpStatus() == 2) {
                        midterminspection.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        midterminspection.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    midterminspection.setFirstdate(new Date());
                    midterminspection.setFirstperson(sysUser.getRealname());
                    midterminspection.setFirstdepartment(sysUser.getEnterprisename());
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsMidterminspection.getSpStatus() == 2) {
                        midterminspection.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        midterminspection.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    midterminspection.setFinishperson(sysUser.getRealname());
                    midterminspection.setFinishdate(new Date());
                    midterminspection.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsMidterminspection.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        midterminspection.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(spectionInfo.getProjectlibraryguid(), spectionInfo.getEnterpriseguid(), zgsMidterminspection.getAuditopinion());

                    }
                }
                if (zgsMidterminspection.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsMidterminspection.getAuditopinion())) {
                        zgsMidterminspection.setAuditopinion(" ");
                    }
                    midterminspection.setAuditopinion(zgsMidterminspection.getAuditopinion());
                    midterminspection.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsMidterminspection.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    midterminspection.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(spectionInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_3));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(midterminspection, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }
}
