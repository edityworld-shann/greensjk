package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_monthgreen_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthgreen_areadetail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="zgs_report_monthgreen_areadetail对象", description="zgs_report_monthgreen_areadetail")
public class ZgsReportMonthgreenAreadetail implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**REPORT_MONTHGREENBUILD.guid*/
	@Excel(name = "REPORT_MONTHGREENBUILD.guid", width = 15)
    @ApiModelProperty(value = "REPORT_MONTHGREENBUILD.guid")
    private java.lang.String monthmianguid;
	/**0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行*/
	@Excel(name = "0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行", width = 15)
    @ApiModelProperty(value = "0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行")
    private java.math.BigDecimal prosstype;
	/**删除标志*/
	@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "isdelete")
    private java.math.BigDecimal isdelete;
  /**创建人账号*/
  @Excel(name = "创建人账号", width = 15)
  @ApiModelProperty(value = "createpersonaccount")
  private java.lang.String createpersonaccount;
  /**创建人名称*/
  @Excel(name = "创建人名称", width = 15)
  @ApiModelProperty(value = "createpersonname")
  private java.lang.String createpersonname;
  /**创建时间*/
  @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
  @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
  @DateTimeFormat(pattern="yyyy-MM-dd")
  @ApiModelProperty(value = "createtime")
  private java.util.Date createtime;
  /**修改人账号*/
  @Excel(name = "修改人账号", width = 15)
  @ApiModelProperty(value = "modifypersonaccount")
  private java.lang.String modifypersonaccount;
  /**修改人名称*/
  @Excel(name = "修改人名称", width = 15)
  @ApiModelProperty(value = "modifypersonname")
  private java.lang.String modifypersonname;
  /**修改时间*/
  @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
  @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
  @DateTimeFormat(pattern="yyyy-MM-dd")
  @ApiModelProperty(value = "modifytime")
  private java.util.Date modifytime;
	/**（保障性住房）项数*/
	@Excel(name = "（保障性住房）项数", width = 15)
    @ApiModelProperty(value = "（保障性住房）项数")
    private java.math.BigDecimal bybzxxs;
	/**保障性住房）建筑面积*/
	@Excel(name = "保障性住房）建筑面积", width = 15)
    @ApiModelProperty(value = "保障性住房）建筑面积")
    private java.math.BigDecimal bybzxmj;
	/**（大型公共建筑）项数*/
	@Excel(name = "（大型公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（大型公共建筑）项数")
    private java.math.BigDecimal bydxggjzxs;
	/**（大型公共建筑）建筑面积*/
	@Excel(name = "（大型公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（大型公共建筑）建筑面积")
    private java.math.BigDecimal bydxggjzmj;
	/**（中小型公共建筑）项数*/
	@Excel(name = "（中小型公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（中小型公共建筑）项数")
    private java.math.BigDecimal byzxxggjzxs;
	/**（中小型公共建筑）建筑面积*/
	@Excel(name = "（中小型公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（中小型公共建筑）建筑面积")
    private java.math.BigDecimal byzxxggjzmj;
	/**（其中政府投资类公共建筑）项数*/
	@Excel(name = "（其中政府投资类公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（其中政府投资类公共建筑）项数")
    private java.math.BigDecimal byzftzlggjzxs;
	/**（其中政府投资类公共建筑）建筑面积*/
	@Excel(name = "（其中政府投资类公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（其中政府投资类公共建筑）建筑面积")
    private java.math.BigDecimal byzftzlggjzmj;
	/**（住宅建筑）项数*/
	@Excel(name = "（住宅建筑）项数", width = 15)
    @ApiModelProperty(value = "（住宅建筑）项数")
    private java.math.BigDecimal byzzjzxs;
	/**（住宅建筑）建筑面积*/
	@Excel(name = "（住宅建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（住宅建筑）建筑面积")
    private java.math.BigDecimal byzzjzmj;
	/**（工业建筑）项数*/
	@Excel(name = "（工业建筑）项数", width = 15)
    @ApiModelProperty(value = "（工业建筑）项数")
    private java.math.BigDecimal bygyjzxs;
	/**（工业建筑）建筑面积*/
	@Excel(name = "（工业建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（工业建筑）建筑面积")
    private java.math.BigDecimal bygyjzmj;
	/**本月项数*/
	@Excel(name = "本月项数", width = 15)
    @ApiModelProperty(value = "本月项数")
    private java.math.BigDecimal byxs;
	/**本月建筑面积*/
	@Excel(name = "本月建筑面积", width = 15)
    @ApiModelProperty(value = "本月建筑面积")
    private java.math.BigDecimal bymj;
	/**（保障性住房）项数*/
	@Excel(name = "（保障性住房）项数", width = 15)
    @ApiModelProperty(value = "（保障性住房）项数")
    private java.math.BigDecimal ncbzxxs;
	/**保障性住房）建筑面积*/
	@Excel(name = "保障性住房）建筑面积", width = 15)
    @ApiModelProperty(value = "保障性住房）建筑面积")
    private java.math.BigDecimal ncbzxmj;
	/**（大型公共建筑）项数*/
	@Excel(name = "（大型公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（大型公共建筑）项数")
    private java.math.BigDecimal ncdxggjzxs;
	/**（大型公共建筑）建筑面积*/
	@Excel(name = "（大型公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（大型公共建筑）建筑面积")
    private java.math.BigDecimal ncdxggjzmj;
	/**（中小型公共建筑）项数*/
	@Excel(name = "（中小型公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（中小型公共建筑）项数")
    private java.math.BigDecimal nczxsggjzxs;
	/**（中小型公共建筑）建筑面积*/
	@Excel(name = "（中小型公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（中小型公共建筑）建筑面积")
    private java.math.BigDecimal nczxsggjzmj;
	/**（其中政府投资类公共建筑）项数*/
	@Excel(name = "（其中政府投资类公共建筑）项数", width = 15)
    @ApiModelProperty(value = "（其中政府投资类公共建筑）项数")
    private java.math.BigDecimal nczftzlggjzxs;
	/**（其中政府投资类公共建筑）建筑面积*/
	@Excel(name = "（其中政府投资类公共建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（其中政府投资类公共建筑）建筑面积")
    private java.math.BigDecimal nczftzlggjzmj;
	/**（住宅建筑）项数*/
	@Excel(name = "（住宅建筑）项数", width = 15)
    @ApiModelProperty(value = "（住宅建筑）项数")
    private java.math.BigDecimal nczzjzxs;
	/**（住宅建筑）建筑面积*/
	@Excel(name = "（住宅建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（住宅建筑）建筑面积")
    private java.math.BigDecimal nczzjzmj;
	/**（工业建筑）项数*/
	@Excel(name = "（工业建筑）项数", width = 15)
    @ApiModelProperty(value = "（工业建筑）项数")
    private java.math.BigDecimal ncgyjzxs;
	/**（工业建筑）建筑面积*/
	@Excel(name = "（工业建筑）建筑面积", width = 15)
    @ApiModelProperty(value = "（工业建筑）建筑面积")
    private java.math.BigDecimal ncgyjzmj;
	/** （一星级住宅）项数*/
	@Excel(name = " （一星级住宅）项数", width = 15)
    @ApiModelProperty(value = " （一星级住宅）项数")
    private java.math.BigDecimal sjyzzxs;
	/** （一星级住宅）建筑面积*/
	@Excel(name = " （一星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " （一星级住宅）建筑面积")
    private java.math.BigDecimal sjyzzmj;
	/** （二星级住宅）项数*/
	@Excel(name = " （二星级住宅）项数", width = 15)
    @ApiModelProperty(value = " （二星级住宅）项数")
    private java.math.BigDecimal sjezzxs;
	/** （二星级住宅）建筑面积*/
	@Excel(name = " （二星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " （二星级住宅）建筑面积")
    private java.math.BigDecimal sjezzmj;
	/** （三星级住宅）项数*/
	@Excel(name = " （三星级住宅）项数", width = 15)
    @ApiModelProperty(value = " （三星级住宅）项数")
    private java.math.BigDecimal sjszzxs;
	/** （三星级住宅）建筑面积*/
	@Excel(name = " （三星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " （三星级住宅）建筑面积")
    private java.math.BigDecimal sjszzmj;
	/**（一星级公共）项数*/
	@Excel(name = "（一星级公共）项数", width = 15)
    @ApiModelProperty(value = "（一星级公共）项数")
    private java.math.BigDecimal sjyggxs;
	/** （一星级公共）建筑面积*/
	@Excel(name = " （一星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " （一星级公共）建筑面积")
    private java.math.BigDecimal sjyggmj;
	/** （二星级公共）项数*/
	@Excel(name = " （二星级公共）项数", width = 15)
    @ApiModelProperty(value = " （二星级公共）项数")
    private java.math.BigDecimal sjeggxs;
	/** （二星级公共）建筑面积*/
	@Excel(name = " （二星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " （二星级公共）建筑面积")
    private java.math.BigDecimal sjeggmj;
	/** （三星级公共）项数*/
	@Excel(name = " （三星级公共）项数", width = 15)
    @ApiModelProperty(value = " （三星级公共）项数")
    private java.math.BigDecimal sjsggxs;
	/** （三星级公共）建筑面积*/
	@Excel(name = " （三星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " （三星级公共）建筑面积")
    private java.math.BigDecimal sjsggmj;
	/** 运行（一星级住宅）项数*/
	@Excel(name = " 运行（一星级住宅）项数", width = 15)
    @ApiModelProperty(value = " 运行（一星级住宅）项数")
    private java.math.BigDecimal yxyzzxs;
	/** 运行（一星级住宅）建筑面积*/
	@Excel(name = " 运行（一星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（一星级住宅）建筑面积")
    private java.math.BigDecimal yxyzzmj;
	/** 运行（二星级住宅）项数*/
	@Excel(name = " 运行（二星级住宅）项数", width = 15)
    @ApiModelProperty(value = " 运行（二星级住宅）项数")
    private java.math.BigDecimal yxezzxs;
	/** 运行（二星级住宅）建筑面积*/
	@Excel(name = " 运行（二星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（二星级住宅）建筑面积")
    private java.math.BigDecimal yxezzmj;
	/** 运行（三星级住宅）项数*/
	@Excel(name = " 运行（三星级住宅）项数", width = 15)
    @ApiModelProperty(value = " 运行（三星级住宅）项数")
    private java.math.BigDecimal yxszzxs;
	/** 运行（三星级住宅）建筑面积*/
	@Excel(name = " 运行（三星级住宅）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（三星级住宅）建筑面积")
    private java.math.BigDecimal yxszzmj;
	/** 运行（一星级公共）项数*/
	@Excel(name = " 运行（一星级公共）项数", width = 15)
    @ApiModelProperty(value = " 运行（一星级公共）项数")
    private java.math.BigDecimal yxyggxs;
	/** 运行（一星级公共）建筑面积*/
	@Excel(name = " 运行（一星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（一星级公共）建筑面积")
    private java.math.BigDecimal yxyggmj;
	/** 运行（二星级公共）项数*/
	@Excel(name = " 运行（二星级公共）项数", width = 15)
    @ApiModelProperty(value = " 运行（二星级公共）项数")
    private java.math.BigDecimal yxeggxs;
	/** 运行（二星级公共）建筑面积*/
	@Excel(name = " 运行（二星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（二星级公共）建筑面积")
    private java.math.BigDecimal yxeggmj;
	/** 运行（三星级公共）项数*/
	@Excel(name = " 运行（三星级公共）项数", width = 15)
    @ApiModelProperty(value = " 运行（三星级公共）项数")
    private java.math.BigDecimal yxsggxs;
	/** 运行（三星级公共）建筑面积*/
	@Excel(name = " 运行（三星级公共）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（三星级公共）建筑面积")
    private java.math.BigDecimal yxsggmj;
	/**年初项数*/
	@Excel(name = "年初项数", width = 15)
    @ApiModelProperty(value = "年初项数")
    private java.math.BigDecimal ncxs;
	/**年初建筑面积*/
	@Excel(name = "年初建筑面积", width = 15)
    @ApiModelProperty(value = "年初建筑面积")
    private java.math.BigDecimal ncmj;
	/**（10万平方米以上商业开发住宅小区）项数*/
	@Excel(name = "（10万平方米以上商业开发住宅小区）项数", width = 15)
    @ApiModelProperty(value = "（10万平方米以上商业开发住宅小区）项数")
    private java.math.BigDecimal bysyzzxqxs;
	/**（10万平方米以上商业开发住宅小区）面积*/
	@Excel(name = "（10万平方米以上商业开发住宅小区）面积", width = 15)
    @ApiModelProperty(value = "（10万平方米以上商业开发住宅小区）面积")
    private java.math.BigDecimal bysyzzxqmj;
	/**（10万平方米以上商业开发住宅小区）项数年初*/
	@Excel(name = "（10万平方米以上商业开发住宅小区）项数年初", width = 15)
    @ApiModelProperty(value = "（10万平方米以上商业开发住宅小区）项数年初")
    private java.math.BigDecimal ncsyzzxqxs;
	/**（10万平方米以上商业开发住宅小区）面积年初*/
	@Excel(name = "（10万平方米以上商业开发住宅小区）面积年初", width = 15)
    @ApiModelProperty(value = "（10万平方米以上商业开发住宅小区）面积年初")
    private java.math.BigDecimal ncsyzzxqmj;
	/**（一星级工业）项数*/
	@Excel(name = "（一星级工业）项数", width = 15)
    @ApiModelProperty(value = "（一星级工业）项数")
    private java.math.BigDecimal sjygyxs;
	/**（一星级工业）建筑面积*/
	@Excel(name = "（一星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = "（一星级工业）建筑面积")
    private java.math.BigDecimal sjygymj;
	/**（二星级工业）项数*/
	@Excel(name = "（二星级工业）项数", width = 15)
    @ApiModelProperty(value = "（二星级工业）项数")
    private java.math.BigDecimal sjegyxs;
	/**（二星级工业）建筑面积*/
	@Excel(name = "（二星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = "（二星级工业）建筑面积")
    private java.math.BigDecimal sjegymj;
	/**（三星级工业）项数*/
	@Excel(name = "（三星级工业）项数", width = 15)
    @ApiModelProperty(value = "（三星级工业）项数")
    private java.math.BigDecimal sjsgyxs;
	/**（三星级工业）建筑面积*/
	@Excel(name = "（三星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = "（三星级工业）建筑面积")
    private java.math.BigDecimal sjsgymj;
	/** 运行（一星级工业）项数*/
	@Excel(name = " 运行（一星级工业）项数", width = 15)
    @ApiModelProperty(value = " 运行（一星级工业）项数")
    private java.math.BigDecimal yxygyxs;
	/** 运行（一星级工业）建筑面积*/
	@Excel(name = " 运行（一星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（一星级工业）建筑面积")
    private java.math.BigDecimal yxygymj;
	/** 运行（二星级工业）项数*/
	@Excel(name = " 运行（二星级工业）项数", width = 15)
    @ApiModelProperty(value = " 运行（二星级工业）项数")
    private java.math.BigDecimal yxegyxs;
	/** 运行（二星级工业）建筑面积*/
	@Excel(name = " 运行（二星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（二星级工业）建筑面积")
    private java.math.BigDecimal yxegymj;
	/** 运行（三星级工业）项数*/
	@Excel(name = " 运行（三星级工业）项数", width = 15)
    @ApiModelProperty(value = " 运行（三星级工业）项数")
    private java.math.BigDecimal yxsgyxs;
	/** 运行（三星级工业）建筑面积*/
	@Excel(name = " 运行（三星级工业）建筑面积", width = 15)
    @ApiModelProperty(value = " 运行（三星级工业）建筑面积")
    private java.math.BigDecimal yxsgymj;
}
