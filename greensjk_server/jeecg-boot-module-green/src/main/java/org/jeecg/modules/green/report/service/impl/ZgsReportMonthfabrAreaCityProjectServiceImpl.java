package org.jeecg.modules.green.report.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProject;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaCityProjectMapper;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaprojectMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 装配式建筑-项目库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreaCityProjectServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaCityProjectMapper, ZgsReportMonthfabrAreaCityProject> implements IZgsReportMonthfabrAreaCityProjectService {

    @Autowired
    private ZgsReportMonthfabrAreaCityProjectMapper zgsReportMonthfabrAreaCityProjectMapper;

    @Override
    public List<ZgsReportMonthfabrAreaCityProject> queryProjectByReportTime(@Param("reporttm") String reporttm, @Param("areacode") String areacode) {
        return zgsReportMonthfabrAreaCityProjectMapper.queryProjectByReportTime(reporttm, areacode);
    }

    /**
     * 补零0️⃣ 清单
     *
     * @param filltm
     * @return
     */
    @Override
    public void addIterm(String filltm) {
        ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject = new ZgsReportMonthfabrAreaCityProject();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportMonthfabrAreaCityProject.setId(UUID.randomUUID().toString());
        zgsReportMonthfabrAreaCityProject.setCreatepersonaccount(sysUser.getUsername());
        zgsReportMonthfabrAreaCityProject.setCreatepersonname(sysUser.getRealname());
        zgsReportMonthfabrAreaCityProject.setCreatetime(new Date());
        zgsReportMonthfabrAreaCityProject.setAreacode(sysUser.getAreacode());
        zgsReportMonthfabrAreaCityProject.setAreaname(sysUser.getAreaname());
        //计算月份、年、季度
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportMonthfabrAreaCityProject.setFilltm(filltm);
        zgsReportMonthfabrAreaCityProject.setQuarter(new BigDecimal(quarter));
        zgsReportMonthfabrAreaCityProject.setYear(new BigDecimal(year));

        //===========处理 主要技术体系（清单列表显示字段）  Begin ==============================================================================================================
        // 公共

        zgsReportMonthfabrAreaCityProject.setAssemblyrate(new BigDecimal(0));
        zgsReportMonthfabrAreaCityProject.setQzxzzmj(new BigDecimal(0));
        zgsReportMonthfabrAreaCityProject.setZpszxzzjzmj(new BigDecimal(0));
        zgsReportMonthfabrAreaCityProject.setStructureChildType("无");
        zgsReportMonthfabrAreaCityProject.setStructureType("4");
        zgsReportMonthfabrAreaCityProject.setBuildType("5");
        zgsReportMonthfabrAreaCityProject.setBuildingArea(new BigDecimal(0));

        zgsReportMonthfabrAreaCityProject.setSysCode("无");
        zgsReportMonthfabrAreaCityProject.setProjectname("无");
        zgsReportMonthfabrAreaCityProject.setIsdelete(new BigDecimal(0));
        zgsReportMonthfabrAreaCityProject.setFillunit("无");
        zgsReportMonthfabrAreaCityProject.setFillpersonname("无");
        zgsReportMonthfabrAreaCityProject.setFillpersontel("无");
        zgsReportMonthfabrAreaCityProject.setFilltm(filltm);
        zgsReportMonthfabrAreaCityProject.setReporttm(filltm);

        zgsReportMonthfabrAreaCityProject.setApplystate(new BigDecimal(1));

        ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject1 = new ZgsReportMonthfabrAreaCityProject();
        ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject2 = new ZgsReportMonthfabrAreaCityProject();
        BeanUtils.copyProperties(zgsReportMonthfabrAreaCityProject, zgsReportMonthfabrAreaCityProject1);
        BeanUtils.copyProperties(zgsReportMonthfabrAreaCityProject, zgsReportMonthfabrAreaCityProject2);

        // 特殊 ~ 新开工
        zgsReportMonthfabrAreaCityProject1.setId(UUID.randomUUID().toString());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String tempDate = filltm + "-01 06:00:00";
        Date initDate = null;
        try {
            initDate = simpleDateFormat.parse(tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //TODO 无数据生成一条数据 根据 只有开工和竣工时间不一样
        zgsReportMonthfabrAreaCityProject1.setStartDate(initDate);
        zgsReportMonthfabrAreaCityProject1.setEndDate(initDate);
        zgsReportMonthfabrAreaCityProject1.setProjecttype("1");

        // 特殊 ~ 已竣工
        //计算月份、年、季度
        zgsReportMonthfabrAreaCityProject2.setId(UUID.randomUUID().toString());
        zgsReportMonthfabrAreaCityProject2.setEndDate(initDate);
        zgsReportMonthfabrAreaCityProject2.setProjecttype("2");

        //===========处理 主要技术体系（清单列表显示字段）  End ==============================================================================================================
        this.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityProject1, ValidateEncryptEntityUtil.isEncrypt));
        //this.save(zgsReportMonthfabrAreaCityProject2);
    }
}
