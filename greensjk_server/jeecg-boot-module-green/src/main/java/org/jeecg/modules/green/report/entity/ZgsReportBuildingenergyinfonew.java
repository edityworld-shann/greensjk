package org.jeecg.modules.green.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: zgs_report_buildingenergyinfonew
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_buildingenergyinfonew")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_buildingenergyinfonew对象", description = "zgs_report_buildingenergyinfonew")
public class ZgsReportBuildingenergyinfonew implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * 填报人
     */
    @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String fillpersontel;
    /**
     * 填报时间
     */
    @Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 填报单位
     */
    @Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    private java.lang.String fillunit;
    /**
     * 审核人
     */
    @Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    @Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 删除标志
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "createpersonname")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    private java.lang.String modifypersonaccount;
    /**
     * 修改人名称
     */
    @Excel(name = "修改人名称", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * 行政区域编码
     */
    @Excel(name = "行政区域编码", width = 15)
    @ApiModelProperty(value = "areacode")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    @Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * (居住建筑)项数
     */
    @Excel(name = "(居住建筑)项数", width = 15)
    @ApiModelProperty(value = "(居住建筑)项数")
    private java.math.BigDecimal byjzjzxs;
    /**
     * (居住建筑)面积
     */
    @Excel(name = " (居住建筑)面积", width = 15)
    @ApiModelProperty(value = " (居住建筑)面积")
    private java.math.BigDecimal byjzjzmj;
    /**
     * (公共建筑)项数
     */
    @Excel(name = "(公共建筑)项数", width = 15)
    @ApiModelProperty(value = "(公共建筑)项数")
    private java.math.BigDecimal byggjzxs;
    /**
     * (公共建筑)面积
     */
    @Excel(name = "(公共建筑)面积", width = 15)
    @ApiModelProperty(value = "(公共建筑)面积")
    private java.math.BigDecimal byggjzmj;
    /**
     * 执行建筑节能强制性标准比例(%)
     */
    @Excel(name = " 执行建筑节能强制性标准比例(%)", width = 15)
    @ApiModelProperty(value = " 执行建筑节能强制性标准比例(%)")
    private java.math.BigDecimal bybzbl;
    /**
     * (小计)项数
     */
    @Excel(name = " (小计)项数", width = 15)
    @ApiModelProperty(value = " (小计)项数")
    private java.math.BigDecimal byxjxs;
    /**
     * (小计)面积
     */
    @Excel(name = "(小计)面积", width = 15)
    @ApiModelProperty(value = "(小计)面积")
    private java.math.BigDecimal byxjmj;
    /**
     * (居住建筑)项数
     */
    @Excel(name = "(居住建筑)项数", width = 15)
    @ApiModelProperty(value = "(居住建筑)项数")
    private java.math.BigDecimal ncjzjzxs;
    /**
     * (居住建筑)面积
     */
    @Excel(name = " (居住建筑)面积", width = 15)
    @ApiModelProperty(value = " (居住建筑)面积")
    private java.math.BigDecimal ncjzjzmj;
    /**
     * (公共建筑)项数
     */
    @Excel(name = "(公共建筑)项数", width = 15)
    @ApiModelProperty(value = "(公共建筑)项数")
    private java.math.BigDecimal ncggjzxs;
    /**
     * (公共建筑)面积
     */
    @Excel(name = "(公共建筑)面积", width = 15)
    @ApiModelProperty(value = "(公共建筑)面积")
    private java.math.BigDecimal ncggjzmj;
    /**
     * (小计)项数
     */
    @Excel(name = " (小计)项数", width = 15)
    @ApiModelProperty(value = " (小计)项数")
    private java.math.BigDecimal ncxjxs;
    /**
     * (小计)面积
     */
    @Excel(name = "(小计)面积", width = 15)
    @ApiModelProperty(value = "(小计)面积")
    private java.math.BigDecimal ncxjmj;
    /**
     * 执行建筑节能强制性标准比例(%)
     */
    @Excel(name = "执行建筑节能强制性标准比例(%)", width = 15)
    @ApiModelProperty(value = "执行建筑节能强制性标准比例(%)")
    private java.math.BigDecimal ncbzbl;
    /**
     * (太阳能光热建筑应用)项数
     */
    @Excel(name = "(太阳能光热建筑应用)项数", width = 15)
    @ApiModelProperty(value = "(太阳能光热建筑应用)项数")
    private java.math.BigDecimal ncgrjzyyxs;
    /**
     * (太阳能光热建筑应用)面积
     */
    @Excel(name = "(太阳能光热建筑应用)面积", width = 15)
    @ApiModelProperty(value = "(太阳能光热建筑应用)面积")
    private java.math.BigDecimal ncgrjzyymj;
    /**
     * (太阳能光电建筑应用)项数
     */
    @Excel(name = "(太阳能光电建筑应用)项数", width = 15)
    @ApiModelProperty(value = "(太阳能光电建筑应用)项数")
    private java.math.BigDecimal ncgdjzyyxs;
    /**
     * (太阳能光电建筑应用)面积
     */
    @Excel(name = " (太阳能光电建筑应用)面积", width = 15)
    @ApiModelProperty(value = " (太阳能光电建筑应用)面积")
    private java.math.BigDecimal ncgdjzyymj;
    /**
     * (浅层地能建筑应用)项数
     */
    @Excel(name = "(浅层地能建筑应用)项数", width = 15)
    @ApiModelProperty(value = "(浅层地能建筑应用)项数")
    private java.math.BigDecimal ncdnjzyyxs;
    /**
     * (浅层地能建筑应用)面积
     */
    @Excel(name = "(浅层地能建筑应用)面积", width = 15)
    @ApiModelProperty(value = "(浅层地能建筑应用)面积")
    private java.math.BigDecimal ncdnjzyymj;
    /**
     * (应用其他类型技术)项数
     */
    @Excel(name = " (应用其他类型技术)项数", width = 15)
    @ApiModelProperty(value = " (应用其他类型技术)项数")
    private java.math.BigDecimal qtlxjsxs;
    /**
     * (应用其他类型技术)面积
     */
    @Excel(name = " (应用其他类型技术)面积", width = 15)
    @ApiModelProperty(value = " (应用其他类型技术)面积")
    private java.math.BigDecimal qtlejsmj;
    /**
     * 负责人
     */
    @Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String principal;
    /**
     * 报表时间
     */
    @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 节能改造本月(居住建筑)项数
     */
    @Excel(name = "节能改造本月(居住建筑)项数", width = 15)
    @ApiModelProperty(value = "节能改造本月(居住建筑)项数")
    private java.math.BigDecimal byjngzjzxs;
    /**
     * 节能改造本月(居住建筑)面积
     */
    @Excel(name = "节能改造本月(居住建筑)面积", width = 15)
    @ApiModelProperty(value = "节能改造本月(居住建筑)面积")
    private java.math.BigDecimal byjngzjzjzmj;
    /**
     * 节能改造本月(公共建筑)项数
     */
    @Excel(name = "节能改造本月(公共建筑)项数", width = 15)
    @ApiModelProperty(value = "节能改造本月(公共建筑)项数")
    private java.math.BigDecimal byjngzggjzxs;
    /**
     * 节能改造本月(公共建筑)面积
     */
    @Excel(name = "节能改造本月(公共建筑)面积", width = 15)
    @ApiModelProperty(value = "节能改造本月(公共建筑)面积")
    private java.math.BigDecimal byjngzggjzmj;
    /**
     * 节能改造本月(小计)项数
     */
    @Excel(name = "节能改造本月(小计)项数", width = 15)
    @ApiModelProperty(value = "节能改造本月(小计)项数")
    private java.math.BigDecimal byjngzxjxs;
    /**
     * 节能改造本月(小计)面积
     */
    @Excel(name = "节能改造本月(小计)面积", width = 15)
    @ApiModelProperty(value = "节能改造本月(小计)面积")
    private java.math.BigDecimal byjngzxjmj;
    /**
     * 节能改造自年初(居住建筑)项数
     */
    @Excel(name = "节能改造自年初(居住建筑)项数", width = 15)
    @ApiModelProperty(value = "节能改造自年初(居住建筑)项数")
    private java.math.BigDecimal ncjngzjzjzxs;
    /**
     * 节能改造自年初(居住建筑)面积
     */
    @Excel(name = "节能改造自年初(居住建筑)面积", width = 15)
    @ApiModelProperty(value = "节能改造自年初(居住建筑)面积")
    private java.math.BigDecimal ncjngzjzjzmj;
    /**
     * 节能改造自年初(公共建筑)项数
     */
    @Excel(name = "节能改造自年初(公共建筑)项数", width = 15)
    @ApiModelProperty(value = "节能改造自年初(公共建筑)项数")
    private java.math.BigDecimal ncjngzggjzxs;
    /**
     * 节能改造自年初(公共建筑)面积
     */
    @Excel(name = "节能改造自年初(公共建筑)面积", width = 15)
    @ApiModelProperty(value = "节能改造自年初(公共建筑)面积")
    private java.math.BigDecimal ncjngzggjzmj;
    /**
     * 节能改造自年初(小计)项数
     */
    @Excel(name = "节能改造自年初(小计)项数", width = 15)
    @ApiModelProperty(value = "节能改造自年初(小计)项数")
    private java.math.BigDecimal ncjngzxjxs;
    /**
     * 节能改造自年初(小计)面积
     */
    @Excel(name = "节能改造自年初(小计)面积", width = 15)
    @ApiModelProperty(value = "节能改造自年初(小计)面积")
    private java.math.BigDecimal ncjngzxjmj;

    @TableField(exist = false)
    private List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList;
}
