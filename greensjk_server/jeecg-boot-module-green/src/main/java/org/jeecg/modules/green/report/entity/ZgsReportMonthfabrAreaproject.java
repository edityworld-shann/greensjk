package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_areaproject")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "项目库-对象", description = "项目库-对象")
public class ZgsReportMonthfabrAreaproject implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * REPORT_MONTHFABR_AREADETAIL的guid
     */
    @Excel(name = "REPORT_MONTHFABR_AREADETAIL的guid", width = 15)
    @ApiModelProperty(value = "REPORT_MONTHFABR_AREADETAIL的guid")
    private java.lang.String monthfabrareadetailguid;
    /**
     * projectname
     */
    @Excel(name = "项目名称", width = 200)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 报表时间
     */
    @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 保障性住房、商品住房、公共建筑、农村及旅游景观项目、其它
     */
    @Excel(name = "保障性住房、商品住房、公共建筑、农村及旅游景观项目、其它", width = 15)
    @ApiModelProperty(value = "保障性住房、商品住房、公共建筑、农村及旅游景观项目、其它")
    private java.lang.String jngztype;
    /**
     * 建筑面积
     */
    @Excel(name = "建筑面积", width = 15)
    @ApiModelProperty(value = "建筑面积")
    private java.math.BigDecimal jngzjzmj;

    /**
     * 全装修住宅面积
     */
    @Excel(name = "全装修住宅面积", width = 15)
    @ApiModelProperty(value = "全装修住宅面积")
    private java.math.BigDecimal qzxzzmj;
    /**
     * 装配式装修 住宅建筑
     */
    @Excel(name = "装配式装修 住宅建筑", width = 15)
    @ApiModelProperty(value = "装配式装修 住宅建筑")
    private java.math.BigDecimal zpszxzzjzmj;

    /**
     * 装配式混凝土结构建筑、装配式钢结构建筑、装配式木结构建筑、其它
     */
    @Excel(name = "装配式混凝土结构建筑、装配式钢结构建筑、装配式木结构建筑、其它", width = 15)
    @ApiModelProperty(value = "装配式混凝土结构建筑、装配式钢结构建筑、装配式木结构建筑、其它")
    private java.lang.String jngzstructure;
    /**
     * 项目位置
     */
    @Excel(name = "项目位置", width = 15)
    @ApiModelProperty(value = "项目位置")
    private java.lang.String projectsite;
    /**
     * 工程进度
     */
    @Excel(name = "工程进度", width = 15)
    @ApiModelProperty(value = "工程进度")
    private java.lang.String jngzprogress;
    /**
     * 项目开工竣工时间
     */
    @Excel(name = "项目开工竣工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目开工竣工时间")
    private java.util.Date jngzprojectdate;
    /**
     * 备注
     */
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String jngzremark;
    /**
     * 1:新开工2:已竣工验收
     */
    @Excel(name = "1:新开工2:已竣工验收", width = 15)
    @ApiModelProperty(value = "1:新开工2:已竣工验收")
    private java.lang.String projecttype;
    /**
     * 区域编码
     */
    @Excel(name = "区域编码", width = 15)
    @ApiModelProperty(value = "区域编码")
    private java.lang.String areacode;
    /**
     * 装配率（%）
     */
    @Excel(name = "装配率（%）", width = 15)
    @ApiModelProperty(value = "装配率（%）")
    private java.lang.String assemblyrate;


    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "创建人账号")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "创建人名称")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createtime;
}
