package org.jeecg.modules.green.common.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 示范项目任务书
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Api(tags = "示范项目任务书")
@RestController
@RequestMapping("/common/zgsProjecttask")
@Slf4j
public class ZgsProjecttaskController extends JeecgController<ZgsProjecttask, IZgsProjecttaskService> {
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsTaskprojectmainparticipantService zgsTaskprojectmainparticipantService;
    @Autowired
    private IZgsBuildjointunitService zgsBuildjointunitService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;
    @Autowired
    private IZgsTaskbuildfundbudgetService zgsTaskbuildfundbudgetService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    /**
     * 分页列表查询
     *
     * @param zgsProjecttask
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "示范项目任务书-分页列表查询")
    @ApiOperation(value = "示范项目任务书-分页列表查询", notes = "示范项目任务书-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjecttask zgsProjecttask,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "numArrow", required = false) Integer numArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsProjecttask> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsProjecttask.getProjectname())) {
            queryWrapper.like("l.projectname", zgsProjecttask.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsProjecttask.getProjectnum())) {
            queryWrapper.like("l.projectnum", zgsProjecttask.getProjectnum());
        }
        if (StringUtils.isNotEmpty(zgsProjecttask.getFundType())) {
            String fundType = zgsProjecttask.getFundType();   // 0 省科技经费未支持   1 省科技经费支持
            // queryWrapper.eq("t.fund_type", fundType);
            if (fundType.equals("0")) {  // 省科技资金未支持
                queryWrapper.and(ft -> {
                    ft.eq("z.provincialfund", 0).or().eq("z.provincialfund",null);
                });
            } else { // 省科技资金支持，资金大于0
                queryWrapper.gt("z.provincialfund", 0);
            }
        }
        if (StringUtils.isNotEmpty(zgsProjecttask.getDealtype())) {
            if (!"-1".equals(zgsProjecttask.getDealtype())) {
                queryWrapper.eq("t.dealtype", zgsProjecttask.getDealtype());
            }
        }
        if (StringUtils.isNotEmpty(zgsProjecttask.getProjectstage())) {
            if (!"-1".equals(zgsProjecttask.getProjectstage())) {
                queryWrapper.eq("t.projectstage", QueryWrapperUtil.getProjectstage(zgsProjecttask.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("t.projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2)).or().isNull("t.projectstage");
                });
            }
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(t.applydate,'%Y')={0}", year);
            queryWrapper.eq("l.year_num", year);
        }
//        QueryWrapperUtil.initLibraryTaskSelect(queryWrapper, zgsProjecttask.getStatus(), sysUser);
      //  QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjecttask.getStatus(), sysUser, 2);
        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("t.enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("t.status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("t.enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("t.enterpriseguid", UUID.randomUUID().toString());
                }
//                else {
//                    queryWrapper.eq("t.enterpriseguid", "null");
//                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("t.enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("t.status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsProjecttask.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("t.finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("t.status", GlobalConstants.SHENHE_STATUS0).ne("t.status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        queryWrapper.ne("l.isdelete", 1);
        queryWrapper.ne("t.isdelete", 1);
        queryWrapper.isNotNull("t.projectguid");
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("t.applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("t.applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("l.completedate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("l.completedate");
            }
        }
        if (numArrow != null) {
            if (numArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("l.projectnum");
            } else if (numArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("l.projectnum");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("t.firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("t.firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(t.firstdate),0,1), t.firstdate DESC");
        }
        // 推荐单位查看项目统计，状态为空
        IPage<ZgsProjectlibraryListInfo> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsProjectlibraryListInfo> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsProjectlibraryListInfo> pageList = null;

        // 示范项目任务书
        redisUtil.expire("sfxmrws",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmrwsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmrwsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsProjecttask.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsProjecttask.getDspForTjdw())) {
            if (zgsProjecttask.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("sfxmrws")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 2);
                    pageListForProjectCount = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);
                    redisUtil.set("sfxmrws",pageListForProjectCount.getTotal());
                }
            }
            if (zgsProjecttask.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("sfxmrwsOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 2);
                    pageListForDsp = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);
                    redisUtil.set("sfxmrwsOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsProjecttask.getDspForTjdw()) && StringUtils.isBlank(zgsProjecttask.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 2);
            pageList = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isBlank(zgsProjecttask.getDspForTjdw()) && StringUtils.isNotBlank(zgsProjecttask.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 2);
            pageList = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isNotBlank(zgsProjecttask.getDspForSt()) && zgsProjecttask.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
                // 省厅查看待审批项目信息
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 2);
                pageList = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);
                redisUtil.set("sfxmrwsOfDsp",pageList.getTotal());
           // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjecttask.getStatus(), sysUser, 2);
            pageList = zgsProjecttaskService.listZgsProjecttaskListInfo(queryWrapper, pageNo, pageSize);
        }

        /*// 示范项目任务书
        redisUtil.expire("sfxmrws",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmrws")) {
            if (StringUtils.isNotBlank(zgsProjecttask.getFlagByWorkTable()) && zgsProjecttask.getFlagByWorkTable().equals("1")) {
                redisUtil.set("sfxmrws",pageListForProjectCount.getTotal());
            }
        }
        redisUtil.expire("sfxmrwsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmrwsOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsProjecttask.getDspForSt()) && zgsProjecttask.getDspForSt().equals("2")) {
                redisUtil.set("sfxmrwsOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmrwsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmrwsOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsProjecttask.getDspForTjdw()) && zgsProjecttask.getDspForTjdw().equals("3")) {
                redisUtil.set("sfxmrwsOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
            if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsProjectlibraryListInfo zgsInfo = pageList.getRecords().get(m);
                    if (StringUtils.isNotEmpty(zgsInfo.getProjecttypenum()) && GlobalConstants.AssemBuidType.equals(zgsInfo.getProjecttypenum())) {
                        pageList.getRecords().get(m).setCompletedate(zgsInfo.getProjectenddate());
                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    }
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsInfo.getPdfUrl());
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsProjectlibraryListInfo zgsInfo = pageList.getRecords().get(m);
                    if (StringUtils.isNotEmpty(zgsInfo.getProjecttypenum()) && GlobalConstants.AssemBuidType.equals(zgsInfo.getProjecttypenum())) {
                        pageList.getRecords().get(m).setCompletedate(zgsInfo.getProjectenddate());
                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    }
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsInfo.getPdfUrl());
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsProjecttask
     * @return
     */
    @AutoLog(value = "示范项目任务书-撤回")
    @ApiOperation(value = "示范项目任务书-撤回", notes = "示范项目任务书-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsProjecttask zgsProjecttask) {
        if (zgsProjecttask != null) {
            String id = zgsProjecttask.getId();
            String bid = zgsProjecttask.getBid();
            String status = zgsProjecttask.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2).equals(zgsProjecttask.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsProjecttask info = new ZgsProjecttask();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsProjecttaskService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsProjecttask
     * @return
     */
    @AutoLog(value = "示范项目任务书-审批")
    @ApiOperation(value = "示范项目任务书-审批", notes = "示范项目任务书-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsProjecttask zgsProjecttask) {
        zgsProjecttaskService.spProject(zgsProjecttask);
        //实时更新阶段和状态
        String projectlibraryguid = zgsProjecttaskService.getById(zgsProjecttask.getId()).getProjectguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 项目变更次数+1
     *
     * @param zgsProjecttask
     * @return
     */
    @AutoLog(value = "项目变更次数+1")
    @ApiOperation(value = "项目变更次数+1", notes = "项目变更次数+1")
    @PutMapping(value = "/changeBg")
    public Result<?> changeBg(@RequestBody ZgsProjecttask zgsProjecttask) {
        int status = zgsProjecttaskService.changeBg(zgsProjecttask);
        if (status == 0) {
//            return Result.error("首次变更无需操作！");
        }
        return Result.OK("变更次数+1成功！");
    }

    /**
     * 添加
     *
     * @param zgsProjecttask
     * @return
     */
    @AutoLog(value = "示范项目任务书-添加")
    @ApiOperation(value = "示范项目任务书-添加", notes = "示范项目任务书-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjecttask zgsProjecttask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsProjecttask.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsProjecttask.getStatus())) {
            //保存并上报
            zgsProjecttask.setApplydate(new Date());
        } else {
            zgsProjecttask.setApplydate(null);
        }
        if (StringUtils.isEmpty(zgsProjecttask.getProjectguid())) {
            return Result.error("信息填写不完整，提交失败！");
        }
        //查下是否已存在关联项目的新增操作
        if (zgsProjecttaskService.selectProjectExist(zgsProjecttask.getProjectguid())) {
            return Result.error("不能重复添加关联项目任务书！");
        }
        zgsProjecttask.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsProjecttask.setCreateaccountname(sysUser.getUsername());
        zgsProjecttask.setCreateusername(sysUser.getRealname());
        zgsProjecttask.setCreatedate(new Date());
        zgsProjecttask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
        zgsProjecttask.setProjectstagestatus(zgsProjecttask.getStatus());
        zgsProjecttaskService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsProjecttask, ValidateEncryptEntityUtil.isEncrypt));
        //上报-实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsProjecttask.getStatus()) && StringUtils.isNotEmpty(zgsProjecttask.getProjectguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsProjecttask.getProjectguid());
        }
        //修改申报项目位已立项状态
        zgsProjectlibraryService.updateAgreeProject(zgsProjecttask.getProjectguid());
//        redisUtil.set(zgsProjecttask.getProjectguid(), "任务书阶段");
        //项目组成员
        List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = zgsProjecttask.getZgsTaskparticipantList();
        if (zgsTaskprojectmainparticipantList != null && zgsTaskprojectmainparticipantList.size() > 0) {
            if (zgsTaskprojectmainparticipantList.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a0 = 0; a0 < zgsTaskprojectmainparticipantList.size(); a0++) {
                ZgsTaskprojectmainparticipant zgsTaskscienceparticipant = zgsTaskprojectmainparticipantList.get(a0);
                zgsTaskscienceparticipant.setId(UUID.randomUUID().toString());
                zgsTaskscienceparticipant.setTaskguid(id);
                zgsTaskscienceparticipant.setCreateaccountname(sysUser.getUsername());
                zgsTaskscienceparticipant.setCreateusername(sysUser.getRealname());
                zgsTaskscienceparticipant.setCreatedate(new Date());
                zgsTaskprojectmainparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskscienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //示范工程联合申报单位表（注：跟主表共用一个因此不做新增）
        List<ZgsBuildjointunit> zgsBuildjointunitList = zgsProjecttask.getZgsBuildjointunitList();
        if (zgsBuildjointunitList != null && zgsBuildjointunitList.size() > 0) {
            QueryWrapper<ZgsBuildjointunit> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("buildguid", zgsProjecttask.getBuildid());
            queryWrapper.eq("tag", "1");
            queryWrapper.ne("isdelete", 1);
            queryWrapper.orderByAsc("createdate");
            List<ZgsBuildjointunit> zgsBuildjointunitList1 = zgsBuildjointunitService.list(queryWrapper);
            if (zgsBuildjointunitList1 != null && zgsBuildjointunitList1.size() > 0) {
                for (ZgsBuildjointunit zgsBuildjointunit : zgsBuildjointunitList1) {
                    zgsBuildjointunitService.removeById(zgsBuildjointunit.getId());
                }
            }
            for (int a0 = 0; a0 < zgsBuildjointunitList.size(); a0++) {
                ZgsBuildjointunit zgsBuildjointunit = zgsBuildjointunitList.get(a0);
                zgsBuildjointunit.setId(UUID.randomUUID().toString());
                zgsBuildjointunit.setBuildguid(zgsProjecttask.getBuildid());
                zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                zgsBuildjointunit.setCreatedate(new Date());
                zgsBuildjointunit.setTag("1");
                zgsBuildjointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //经费小项
        zgsProjectlibraryService.updateFeeSb(zgsProjecttask);
        //经费预算
        List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsProjecttask.getZgsTaskbuildfundbudgetList();
        if (zgsTaskbuildfundbudgetList != null && zgsTaskbuildfundbudgetList.size() > 0) {
            for (int a0 = 0; a0 < zgsTaskbuildfundbudgetList.size(); a0++) {
                ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(a0);
                zgsTaskbuildfundbudget.setId(UUID.randomUUID().toString());
                zgsTaskbuildfundbudget.setTaskguid(id);
                zgsTaskbuildfundbudget.setCreateaccountname(sysUser.getUsername());
                zgsTaskbuildfundbudget.setCreateusername(sysUser.getRealname());
                zgsTaskbuildfundbudget.setCreatedate(new Date());
                zgsTaskbuildfundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsProjecttask
     * @return
     */
    @AutoLog(value = "示范项目任务书-编辑")
    @ApiOperation(value = "示范项目任务书-编辑", notes = "示范项目任务书-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjecttask zgsProjecttask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getId())) {
            String id = zgsProjecttask.getId();
            if (StringUtils.isNotEmpty(zgsProjecttask.getEditStatus())) {
                //强制编辑
                zgsProjecttask.setEditStatus(null);
                //修改申报单位项目名称或负责人啥的
                if (StringUtils.isNotEmpty(zgsProjecttask.getProjectguid())) {
                    ZgsProjectlibrary zgsProjectlibrary = new ZgsProjectlibrary();
                    zgsProjectlibrary.setId(zgsProjecttask.getProjectguid());
                    zgsProjectlibrary.setProjectname(zgsProjecttask.getZgsProjectlibrary().getProjectname());
                    zgsProjectlibrary.setFeeStatus(null);
                    zgsProjectlibrary.setApplyunitprojectleader(zgsProjecttask.getZgsProjectlibrary().getApplyunitprojectleader());
                    zgsProjectlibraryService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsProjectlibrary, ValidateEncryptEntityUtil.isEncrypt));
                }
            } else {
                if (GlobalConstants.handleSubmit.equals(zgsProjecttask.getStatus())) {
                    //保存并上报
                    zgsProjecttask.setApplydate(new Date());
                    zgsProjecttask.setAuditopinion(null);
                    zgsProjecttask.setReturntype(null);
                } else {
                    zgsProjecttask.setApplydate(null);
                    //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                    ZgsProjecttask projectTask = zgsProjecttaskService.getById(id);
                    if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                        zgsProjecttask.setStatus(GlobalConstants.SHENHE_STATUS3);
                    }
                }
            }
            zgsProjecttask.setModifyaccountname(sysUser.getUsername());
            zgsProjecttask.setModifyusername(sysUser.getRealname());
            zgsProjecttask.setModifydate(new Date());
//            zgsProjecttask.setReturntype(new BigDecimal(0));
            zgsProjecttask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
            zgsProjecttask.setProjectstagestatus(zgsProjecttask.getStatus());
            zgsProjecttaskService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsProjecttask, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsProjecttask.getStatus()) && StringUtils.isNotEmpty(zgsProjecttask.getProjectguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsProjecttask.getProjectguid());
            }
            //强制更新限制细节修改开启
            if (StringUtils.isEmpty(zgsProjecttask.getEditStatus())) {
                //项目组成员
                List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = zgsProjecttask.getZgsTaskparticipantList();
                QueryWrapper<ZgsTaskprojectmainparticipant> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("taskguid", id);
                queryWrapper1.ne("isdelete", 1);
                queryWrapper1.orderByAsc("ordernum");
                List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList1 = zgsTaskprojectmainparticipantService.list(queryWrapper1);
                if (zgsTaskprojectmainparticipantList != null && zgsTaskprojectmainparticipantList.size() > 0) {
                    if (zgsTaskprojectmainparticipantList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsTaskprojectmainparticipantList.size(); a0++) {
                        ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = zgsTaskprojectmainparticipantList.get(a0);
                        if (StringUtils.isNotEmpty(zgsTaskprojectmainparticipant.getId())) {
                            //编辑
                            zgsTaskprojectmainparticipant.setModifyaccountname(sysUser.getUsername());
                            zgsTaskprojectmainparticipant.setModifyusername(sysUser.getRealname());
                            zgsTaskprojectmainparticipant.setModifydate(new Date());
                            zgsTaskprojectmainparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskprojectmainparticipant, ValidateEncryptEntityUtil.isEncrypt));
                            generateProjectReport(id);
                        } else {
                            //新增
                            zgsTaskprojectmainparticipant.setId(UUID.randomUUID().toString());
                            zgsTaskprojectmainparticipant.setTaskguid(id);
                            zgsTaskprojectmainparticipant.setCreateaccountname(sysUser.getUsername());
                            zgsTaskprojectmainparticipant.setCreateusername(sysUser.getRealname());
                            zgsTaskprojectmainparticipant.setCreatedate(new Date());
                            zgsTaskprojectmainparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskprojectmainparticipant, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant1 : zgsTaskprojectmainparticipantList1) {
                            if (!zgsTaskprojectmainparticipant1.getId().equals(zgsTaskprojectmainparticipant.getId())) {
                                map0.put(zgsTaskprojectmainparticipant1.getId(), zgsTaskprojectmainparticipant1.getId());
                            } else {
                                list0.add(zgsTaskprojectmainparticipant1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsTaskprojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsTaskprojectmainparticipantList1 != null && zgsTaskprojectmainparticipantList1.size() > 0) {
                        for (ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant : zgsTaskprojectmainparticipantList1) {
                            zgsTaskprojectmainparticipantService.removeById(zgsTaskprojectmainparticipant.getId());
                        }
                    }
                }
                //示范工程联合申报单位（注：跟主表共用一个因此不做修改）
                List<ZgsBuildjointunit> zgsBuildjointunitList = zgsProjecttask.getZgsBuildjointunitList();
                QueryWrapper<ZgsBuildjointunit> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("buildguid", zgsProjecttask.getBuildid());
                queryWrapper.eq("tag", "1");
                queryWrapper.ne("isdelete", 1);
                queryWrapper.orderByAsc("createdate");
                List<ZgsBuildjointunit> zgsBuildjointunitList1 = zgsBuildjointunitService.list(queryWrapper);
                if (zgsBuildjointunitList != null && zgsBuildjointunitList.size() > 0) {
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsBuildjointunitList.size(); a0++) {
                        ZgsBuildjointunit zgsBuildjointunit = zgsBuildjointunitList.get(a0);
                        if (StringUtils.isNotEmpty(zgsBuildjointunit.getId())) {
                            //先查询一下判断类型
                            ZgsBuildjointunit buildjointunit = zgsBuildjointunitService.getById(zgsBuildjointunit.getId());
                            if (buildjointunit != null && StringUtils.isNotEmpty(buildjointunit.getTag())) {
                                //编辑
                                zgsBuildjointunit.setModifyaccountname(sysUser.getUsername());
                                zgsBuildjointunit.setModifyusername(sysUser.getRealname());
                                zgsBuildjointunit.setModifydate(new Date());
                                zgsBuildjointunitService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsBuildjointunit.setId(UUID.randomUUID().toString());
                                zgsBuildjointunit.setBuildguid(zgsProjecttask.getBuildid());
                                zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                                zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                                zgsBuildjointunit.setCreatedate(new Date());
                                zgsBuildjointunit.setTag("1");
                                zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsBuildjointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt));
                            }
                        } else {
                            //新增
                            zgsBuildjointunit.setId(UUID.randomUUID().toString());
                            zgsBuildjointunit.setBuildguid(zgsProjecttask.getBuildid());
                            zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                            zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                            zgsBuildjointunit.setCreatedate(new Date());
                            zgsBuildjointunit.setTag("1");
                            zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsBuildjointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsBuildjointunit zgsBuildjointunit1 : zgsBuildjointunitList1) {
                            if (!zgsBuildjointunit1.getId().equals(zgsBuildjointunit.getId())) {
                                map0.put(zgsBuildjointunit1.getId(), zgsBuildjointunit1.getId());
                            } else {
                                list0.add(zgsBuildjointunit1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsBuildjointunitService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsBuildjointunitList1 != null && zgsBuildjointunitList1.size() > 0) {
                        for (ZgsBuildjointunit zgsBuildjointunit : zgsBuildjointunitList1) {
                            zgsBuildjointunitService.removeById(zgsBuildjointunit.getId());
                        }
                    }
                }
                //经费小项
                zgsProjectlibraryService.updateFeeSb(zgsProjecttask);
                //经费预算
                List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsProjecttask.getZgsTaskbuildfundbudgetList();
                QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("taskguid", id);
                queryWrapper2.ne("isdelete", 1);
                queryWrapper2.orderByAsc("year");
                List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList1 = zgsTaskbuildfundbudgetService.list(queryWrapper2);
                if (zgsTaskbuildfundbudgetList != null && zgsTaskbuildfundbudgetList.size() > 0) {
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsTaskbuildfundbudgetList.size(); a0++) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(a0);
                        if (StringUtils.isNotEmpty(zgsTaskbuildfundbudget.getId())) {
                            //编辑
                            zgsTaskbuildfundbudget.setModifyaccountname(sysUser.getUsername());
                            zgsTaskbuildfundbudget.setModifyusername(sysUser.getRealname());
                            zgsTaskbuildfundbudget.setModifydate(new Date());
                            zgsTaskbuildfundbudgetService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
                        } else {
                            //新增
                            zgsTaskbuildfundbudget.setId(UUID.randomUUID().toString());
                            zgsTaskbuildfundbudget.setTaskguid(id);
                            zgsTaskbuildfundbudget.setCreateaccountname(sysUser.getUsername());
                            zgsTaskbuildfundbudget.setCreateusername(sysUser.getRealname());
                            zgsTaskbuildfundbudget.setCreatedate(new Date());
                            zgsTaskbuildfundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsTaskbuildfundbudget zgsTaskbuildfundbudget1 : zgsTaskbuildfundbudgetList1) {
                            if (!zgsTaskbuildfundbudget1.getId().equals(zgsTaskbuildfundbudget.getId())) {
                                map0.put(zgsTaskbuildfundbudget1.getId(), zgsTaskbuildfundbudget1.getId());
                            } else {
                                list0.add(zgsTaskbuildfundbudget1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsTaskbuildfundbudgetService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsTaskbuildfundbudgetList1 != null && zgsTaskbuildfundbudgetList1.size() > 0) {
                        for (ZgsTaskbuildfundbudget zgsTaskbuildfundbudget : zgsTaskbuildfundbudgetList1) {
                            zgsTaskbuildfundbudgetService.removeById(zgsTaskbuildfundbudget.getId());
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目任务书-通过id删除")
    @ApiOperation(value = "示范项目任务书-通过id删除", notes = "示范项目任务书-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjecttaskService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "示范项目任务书-批量删除")
    @ApiOperation(value = "示范项目任务书-批量删除", notes = "示范项目任务书-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsProjecttaskService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目任务书-通过id查询")
    @ApiOperation(value = "示范项目任务书-通过id查询", notes = "示范项目任务书-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsProjecttask zgsProjecttask = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjecttask == null) {
                return Result.error("未找到对应数据");
            } else {
                zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
            }
            if ((zgsProjecttask.getFirstdate() != null || zgsProjecttask.getFinishdate() != null) && zgsProjecttask.getApplydate() != null) {
                zgsProjecttask.setSpLogStatus(1);
            } else {
                zgsProjecttask.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsProjecttask = new ZgsProjecttask();
            zgsProjecttask.setSpLogStatus(0);
        }
        return Result.OK(zgsProjecttask);
    }

    /**
     * 通过projectguid查询
     * assType菜单对应类型
     *
     * @param projectguid
     * @param accessType
     * @return
     */
    @AutoLog(value = "示范项目任务书-通过projectguid查询")
    @ApiOperation(value = "示范项目任务书-通过projectguid查询", notes = "示范项目任务书-通过projectguid查询")
    @GetMapping(value = "/queryByProjectGuid")
    public Result<?> queryByProjectGuid(@RequestParam(name = "projectguid") String projectguid, @RequestParam(name = "accessType", required = false) Integer accessType) {
        Object object = zgsProjecttaskService.queryByProjectGuid(projectguid, accessType);
        if (object == null) {
            return Result.error("还未创建任务书，需要先创建任务书！");
        }
        return Result.OK(object);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsProjecttask
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsProjecttask zgsProjecttask,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "numArrow", required = false) Integer numArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsProjecttask, 1, 9999, year, sbArrow, wcArrow, numArrow, csArrow, req);
        IPage<ZgsProjectlibraryListInfo> pageList = (IPage<ZgsProjectlibraryListInfo>) result.getResult();
        List<ZgsProjectlibraryListInfo> list = pageList.getRecords();
        List<ZgsProjectlibraryListInfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        String title = "示范项目任务书";
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsProjectlibraryListInfo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        mv.addObject(NormalExcelConstants.EXPORT_FIELDS, "projectname,applyunit,applyunitprojectleader,applyunitphone,demonstratetype,applydate,completedate");

        //        return super.exportXls(request, zgsProjecttask, ZgsProjecttask.class, "示范项目任务书");
        return mv;
    }

    private String getId(ZgsProjectlibraryListInfo item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjecttask.class);
    }


    /**
     * 通过id查询生成任务书
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目任务书-通过id查询生成任务书")
    @ApiOperation(value = "示范项目任务书-通过id查询生成任务书", notes = "示范项目任务书-通过id查询生成任务书")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsProjecttask zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsProjecttask != null) {
            zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "8建设厅任务书-示范工程.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsProjecttask);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }


    @AutoLog(value = "示范项目任务书-生成")
    @ApiOperation(value = "示范项目任务书-生成", notes = "示范项目任务书-生成")
    @PutMapping(value = "/generateDemoReport")
    public Result<?> generateDemoProjectReport(@RequestParam(name = "id", required = false) String id) {
        log.info("终审通过异步生成示范任务书pdf-------开始");
        try {
            ZgsProjecttask zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjecttask != null) {
                zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
                ZgsProjectlibrary zgsProjectlibrary = zgsProjecttask.getZgsProjectlibrary();
                if (zgsProjectlibrary != null) {
                    String fileName = zgsProjectlibrary.getProjectname();
                    if (StringUtils.isNotEmpty(fileName)) {
                        String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "8建设厅任务书-示范工程.docx", BeanUtil.beanToMap(zgsProjecttask));
                        ZgsProjecttask projecttask = new ZgsProjecttask();
                        projecttask.setId(zgsProjecttask.getId());
                        projecttask.setPdfUrl(filePath.replace(".doc", ".pdf"));
                        zgsProjecttaskService.updateById(projecttask);
                        String pdf_filePath = upload + File.separator + filePath;
                        pdf_filePath = pdf_filePath.replace("//", "\\");
                        final String newPath = pdf_filePath;
                        File file = new File(pdf_filePath);
                        if (file.exists() && file.length() > 0) {
                            log.info("文件转换即将开始");
//                            LibreOfficeUtil.wordConverterToPdf("D:\\opt\\upFiles\\green_task\\2022-05-16\\关联项目测试使用被修改1.doc");
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(10000);
                                        LibreOfficeUtil.wordConverterToPdf(newPath);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } else {
                            log.info("文件转换条件不成立");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("终审通过异步生成示范任务书pdf-------结束");

        return Result.OK("任务书生成结束");
    }



    public void generateProjectReport(String id) {
        log.info("终审通过异步生成示范任务书pdf-------开始");
        try {
            ZgsProjecttask zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjecttask != null) {
                zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
                ZgsProjectlibrary zgsProjectlibrary = zgsProjecttask.getZgsProjectlibrary();
                if (zgsProjectlibrary != null) {
                    String fileName = zgsProjectlibrary.getProjectname();
                    if (StringUtils.isNotEmpty(fileName)) {
                        String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "8建设厅任务书-示范工程.docx", BeanUtil.beanToMap(zgsProjecttask));
                        ZgsProjecttask projecttask = new ZgsProjecttask();
                        projecttask.setId(zgsProjecttask.getId());
                        projecttask.setPdfUrl(filePath.replace(".doc", ".pdf"));
                        zgsProjecttaskService.updateById(projecttask);
                        String pdf_filePath = upload + File.separator + filePath;
                        pdf_filePath = pdf_filePath.replace("//", "\\");
                        final String newPath = pdf_filePath;
                        File file = new File(pdf_filePath);
                        if (file.exists() && file.length() > 0) {
                            log.info("文件转换即将开始");
//                            LibreOfficeUtil.wordConverterToPdf("D:\\opt\\upFiles\\green_task\\2022-05-16\\关联项目测试使用被修改1.doc");
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(10000);
                                        LibreOfficeUtil.wordConverterToPdf(newPath);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } else {
                            log.info("文件转换条件不成立");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
