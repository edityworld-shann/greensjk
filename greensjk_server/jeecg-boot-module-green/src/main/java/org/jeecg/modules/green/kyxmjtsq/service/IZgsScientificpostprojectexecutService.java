package org.jeecg.modules.green.kyxmjtsq.service;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostprojectexecut;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificpostprojectexecutService extends IService<ZgsScientificpostprojectexecut> {

}
