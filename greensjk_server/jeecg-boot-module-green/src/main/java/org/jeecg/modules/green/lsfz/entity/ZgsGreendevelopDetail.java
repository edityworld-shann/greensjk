package org.jeecg.modules.green.lsfz.entity;

import cn.hutool.core.annotation.Alias;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @describe:绿色发展业务详情表
 * @author: renxiaoliang
 * @date: 2023/12/12 14:24
 */
@Data
@TableName("zgs_greendevelop_detail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_greendevelop_detail对象", description = "绿色发展业务详情表")
public class ZgsGreendevelopDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private String detailId;
    /**
     * 绿色项目业务主键
     */
    @Excel(name = "绿色项目业务主键", width = 15)
    @ApiModelProperty(value = "绿色项目业务主键")
    private String greenprojectkey;
    /**
     * 建设规模（万平方米）
     */
    @Excel(name = "建设规模（万平方米）", width = 15)
    @ApiModelProperty(value = "建设规模（万平方米）")
    private String constructionScale;
    /**
     * 项目投资金额（万元）
     */
    @Excel(name = "项目投资金额（万元）", width = 15)
    @ApiModelProperty(value = "项目投资金额（万元）")
    private String investMoney;
    /**
     * 填报日期
     */
    /*@Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "填报日期")
    private Date informantDate;*/
    /**
     * 申请绿色金融产品
     */
    @Excel(name = "申请绿色金融产品", width = 15)
    @ApiModelProperty(value = "申请绿色金融产品")
    private String applyFinance;
    /**
     * 申请绿色金融数额（万元）
     */
    @Excel(name = "申请绿色金融数额（万元）", width = 15)
    @ApiModelProperty(value = "申请绿色金融数额（万元）")
    private String applyGreenfinanceMoney;
    /**
     * 申请绿色金融主体单位
     */
    @Excel(name = "申请绿色金融主体单位", width = 15)
    @ApiModelProperty(value = "申请绿色金融主体单位")
    private String applyGreenfinanceUnit;
    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    private String applyGreenfinanceLeader;     //projectLeaderConstruct;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String  applyGreenfinancePhone;  //constructPhone;
    /**
     * 建设单位
     */
    @Excel(name = "建设单位", width = 15)
    @ApiModelProperty(value = "建设单位")
    private String constructUnit;
    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    private String constructUnitLeader;    //projectLeaderDesign;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String constructUnitPhone;  // designPhone;
    /**
     * 设计单位
     */
    @Excel(name = "设计单位", width = 15)
    @ApiModelProperty(value = "设计单位")
    private String designUnit;
    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    private String designUnitLeader;  //projectLeaderAsk;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String designUnitPhone; //askPhone;
    /**
     * 咨询单位
     */
    @Excel(name = "咨询单位", width = 15)
    @ApiModelProperty(value = "咨询单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String askUnit;

    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private String askUnitLeader;   //projectLeaderBuild;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String askUnitPhone;  //buildPhone;
    /**
     * 施工单位
     */
    @Excel(name = "施工单位", width = 15)
    @ApiModelProperty(value = "施工单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String buildUnit;
    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private String buildUnitLeader;//projectLeaderOperate;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String buildUnitPhone; //operatePhone;
    /**
     * 运营/养护单位
     */
    @Excel(name = "运营/养护单位", width = 15)
    @ApiModelProperty(value = "运营/养护单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String operateUnit;

    /**
     * 项目负责人/联系人
     */
    @Excel(name = "项目负责人/联系人", width = 15)
    @ApiModelProperty(value = "项目负责人/联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private String operateUnitLeader;  // projectLeaderService;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String operateUnitPhone;  //servicePhone;
    /**
     * 服务单位
     */
    @Excel(name = "服务单位", width = 15)
    @ApiModelProperty(value = "服务单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String serviceUnit;

    /**
     * 服务单位负责人
     */
    @Excel(name = "服务单位负责人", width = 15)
    @ApiModelProperty(value = "服务单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private String serviceUnitLeader;
    /**
     * 服务单位负责人电话
     */
    @Excel(name = "服务单位电话", width = 15)
    @ApiModelProperty(value = "服务单位电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private String serviceUnitPhone;
    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "创建人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private String createusername;
    /**
     * 创建日期
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createdate;
    /**
     * 修改人账号
     */
    @Excel(name = "modifyaccountname", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "modifyusername", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private String modifyusername;
    /**
     * 修改日期
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private BigDecimal isdelete = new BigDecimal(0);

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
