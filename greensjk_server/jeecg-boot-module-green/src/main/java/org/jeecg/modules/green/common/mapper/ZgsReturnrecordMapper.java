package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 退回记录信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface ZgsReturnrecordMapper extends BaseMapper<ZgsReturnrecord> {

}
