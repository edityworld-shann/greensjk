package org.jeecg.modules.green.report.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;

/**
 * @Description: zgs_report_buildingenergyinfocity
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface ZgsReportHomePageMapper extends BaseMapper<ZgsReportBuildingenergyinfocity> {

}
