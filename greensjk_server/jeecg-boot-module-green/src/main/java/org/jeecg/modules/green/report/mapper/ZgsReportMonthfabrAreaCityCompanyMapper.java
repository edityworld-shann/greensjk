package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式建筑-单位库-申报
 * @Author: jeecg-boot
 * @Date:   2022-05-20
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaCityCompanyMapper extends BaseMapper<ZgsReportMonthfabrAreaCityCompany> {

}
