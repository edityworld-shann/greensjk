package org.jeecg.modules.green.worktable.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CityAndStateWorkbenchPo implements Serializable {
    /**
     * 本年度新建建筑竣工累计项数
     */
    private String buildCount;
    /**
     * 本年度既有建筑节能改造完成累计项数
     */
    private String existinfoCount;
    /**
     * 自年初新开工装配式建筑项数
     */
    private String completedCount;
    /**
     * 自年初已竣工验收装配式建筑项数
     */
    private String prefabricatedCount;
}
