package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportAreacodeValues;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式新开工和已竣工建筑总面积表
 * @Author: jeecg-boot
 * @Date:   2022-07-29
 * @Version: V1.0
 */
public interface ZgsReportAreacodeValuesMapper extends BaseMapper<ZgsReportAreacodeValues> {

}
