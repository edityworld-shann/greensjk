package org.jeecg.modules.green.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import org.jeecg.modules.green.common.service.AsyncScienceService;
import org.jeecg.modules.green.common.service.IZgsSciencetechtaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/5/1523:33
 */
@Service
@Slf4j
public class AsyncScienceServiceImpl implements AsyncScienceService {

    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;

    @Override
    @Async
    public void initScienceTechtaskPdfUrl(String id) {
        log.info("终审通过异步生成科技任务书pdf-------开始");
        try {
            ZgsSciencetechtask zgsSciencetechtask = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechtaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsSciencetechtask != null) {
                zgsSciencetechtaskService.initProjectSelectById(zgsSciencetechtask);
                String fileName = zgsSciencetechtask.getProjectname();
                if (StringUtils.isNotEmpty(fileName)) {
                    String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "9科技项目任务书.docx", BeanUtil.beanToMap(zgsSciencetechtask));
                    ZgsSciencetechtask projecttask = new ZgsSciencetechtask();
                    projecttask.setId(zgsSciencetechtask.getId());
                    projecttask.setPdfUrl(filePath.replace(".doc", ".pdf"));
                    zgsSciencetechtaskService.updateById(projecttask);
                    String pdf_filePath = upload + File.separator + filePath;
                    pdf_filePath = pdf_filePath.replace("//", "\\");
                    final String newPath = pdf_filePath;
                    File file = new File(pdf_filePath);
                    if (file.exists() && file.length() > 0) {
                        log.info("文件转换即将开始");
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(10000);
                                    LibreOfficeUtil.wordConverterToPdf(newPath);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    } else {
                        log.info("文件转换条件不成立");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("终审通过异步生成科技任务书pdf-------结束");
    }
}
