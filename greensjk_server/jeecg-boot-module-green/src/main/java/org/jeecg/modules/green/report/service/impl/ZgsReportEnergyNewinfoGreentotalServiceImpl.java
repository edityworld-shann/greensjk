package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyNewinfoGreentotalMapper;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoGreentotalService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoTotalService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 绿色建筑汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Service
public class ZgsReportEnergyNewinfoGreentotalServiceImpl extends ServiceImpl<ZgsReportEnergyNewinfoGreentotalMapper, ZgsReportEnergyNewinfoGreentotal> implements IZgsReportEnergyNewinfoGreentotalService {
    @Autowired
    private IZgsReportEnergyNewinfoService zgsReportEnergyNewinfoService;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService zgsReportEnergyNewinfoTotalService;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService zgsReportEnergyNewinfoGreentotalService;



    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
    @Override
    public void initEnergyNewinfoGreenTotalZero(String year, int quarter, String yearMonth) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //二、绿色建筑汇总
        energyNewinfoGreenTotalZero(sysUser, year, quarter, yearMonth);
    }

    private void energyNewinfoGreenTotalZero(LoginUser sysUser, String year, int quarter, String yearMonth) {
        ZgsReportEnergyNewinfoGreentotal totalInfo = new ZgsReportEnergyNewinfoGreentotal();
        totalInfo.setApplydate(new Date());
        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        totalInfo.setBackreason("");
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoGreentotal totalYearInfo = this.baseMapper.totalMonthNewinfoGreenDataYear(yearMonth, sysUser.getAreacode(), year);
        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoGreentotal totalMonthInfo = this.baseMapper.totalMonthNewinfoGreenData(yearMonth, sysUser.getAreacode());
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(0);
        totalInfo.setMonthArea(new BigDecimal(0));
        totalInfo.setMonthNewEndArea(new BigDecimal(0));
        totalInfo.setMonthZb(new BigDecimal(0));
        //
        totalInfo.setYearNumber(totalYearInfo.getYearNumber());
        totalInfo.setYearArea(totalYearInfo.getYearArea());
        totalInfo.setYearNewEndArea(totalYearInfo.getYearNewEndArea());
        totalInfo.setYearZb(totalYearInfo.getYearZb());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        totalInfo.setYearBuildindustryNumber(totalYearInfo.getYearBuildindustryNumber());
        totalInfo.setYearBuildindustryArea(totalYearInfo.getYearBuildindustryArea());
        totalInfo.setYearGreenNumber(totalYearInfo.getYearGreenNumber());
        totalInfo.setYearGreenArea(totalYearInfo.getYearGreenArea());
        totalInfo.setYearRankNumber(totalYearInfo.getYearRankNumber());
        totalInfo.setYearRankArea(totalYearInfo.getYearRankArea());
        totalInfo.setYearRankNumber1(totalYearInfo.getYearRankNumber1());
        totalInfo.setYearRankArea1(totalYearInfo.getYearRankArea1());
        totalInfo.setYearRankNumber2(totalYearInfo.getYearRankNumber2());
        totalInfo.setYearRankArea2(totalYearInfo.getYearRankArea2());
        totalInfo.setYearRankNumber3(totalYearInfo.getYearRankNumber3());
        totalInfo.setYearRankArea3(totalYearInfo.getYearRankArea3());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", sysUser.getAreacode());
        queryW.isNull("area_type");
        ZgsReportEnergyNewinfoGreentotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            totalInfo.setModifypersonaccount(sysUser.getUsername());
            totalInfo.setModifypersonname(sysUser.getRealname());
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            totalInfo.setId(UUID.randomUUID().toString());
            totalInfo.setCreatepersonaccount(sysUser.getUsername());
            totalInfo.setCreatepersonname(sysUser.getRealname());
            totalInfo.setCreatetime(new Date());
            totalInfo.setAreacode(sysUser.getAreacode());
            totalInfo.setAreaname(sysUser.getAreaname());
            totalInfo.setFilltm(yearMonth);
            totalInfo.setQuarter(new BigDecimal(quarter));
            totalInfo.setYear(new BigDecimal(year));
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        }
    }
    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--


    @Override
    public void energyNewinfoGreenTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        ZgsReportEnergyNewinfoGreentotal totalInfo = new ZgsReportEnergyNewinfoGreentotal();
        if (type == 0) {
            totalInfo.setApplydate(new Date());
            totalInfo.setBackreason("");
        }
        if (applyState != null) {
            totalInfo.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoGreentotal totalYearInfo = this.baseMapper.totalMonthNewinfoGreenDataYear(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyNewinfoGreentotal totalMonthInfo = this.baseMapper.totalMonthNewinfoGreenData(yearMonth, areacode);
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
        totalInfo.setMonthNewEndArea(totalMonthInfo.getMonthNewEndArea());
        totalInfo.setMonthZb(totalMonthInfo.getMonthZb());
        //
        totalInfo.setYearNumber(totalYearInfo.getYearNumber());
        totalInfo.setYearArea(totalYearInfo.getYearArea());
        totalInfo.setYearNewEndArea(totalYearInfo.getYearNewEndArea());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        totalInfo.setYearBuildindustryNumber(totalYearInfo.getYearBuildindustryNumber());
        totalInfo.setYearBuildindustryArea(totalYearInfo.getYearBuildindustryArea());
        totalInfo.setYearGreenNumber(totalYearInfo.getYearGreenNumber());
        totalInfo.setYearGreenArea(totalYearInfo.getYearGreenArea());
        totalInfo.setYearRankNumber(totalYearInfo.getYearRankNumber());
        totalInfo.setYearRankArea(totalYearInfo.getYearRankArea());
        totalInfo.setYearRankNumber1(totalYearInfo.getYearRankNumber1());
        totalInfo.setYearRankArea1(totalYearInfo.getYearRankArea1());
        totalInfo.setYearRankNumber2(totalYearInfo.getYearRankNumber2());
        totalInfo.setYearRankArea2(totalYearInfo.getYearRankArea2());
        totalInfo.setYearRankNumber3(totalYearInfo.getYearRankNumber3());
        totalInfo.setYearRankArea3(totalYearInfo.getYearRankArea3());
        totalInfo.setYearZb(totalYearInfo.getYearZb());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNull("area_type");
        ZgsReportEnergyNewinfoGreentotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            if (sysUser != null) {
                totalInfo.setModifypersonaccount(sysUser.getUsername());
                totalInfo.setModifypersonname(sysUser.getRealname());
            }
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                //新增
                totalInfo.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    totalInfo.setCreatepersonaccount(sysUser.getUsername());
                    totalInfo.setCreatepersonname(sysUser.getRealname());
                }
                totalInfo.setCreatetime(new Date());
                totalInfo.setAreacode(areacode);
                totalInfo.setAreaname(areaname);
                totalInfo.setFilltm(yearMonth);
                totalInfo.setQuarter(new BigDecimal(quarter));
                totalInfo.setYear(new BigDecimal(year));
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }


//    @Override
//    public void initEnergyNewinfoGreenTotal(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        //二、绿色建筑汇总
//        energyNewinfoGreenTotal(sysUser, year, quarter, yearMonth);
//    }

//    private void energyNewinfoGreenTotal(LoginUser sysUser, String year, int quarter, String yearMonth) {
//        ZgsReportEnergyNewinfoGreentotal totalInfo = new ZgsReportEnergyNewinfoGreentotal();
//        totalInfo.setApplydate(new Date());
//        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        totalInfo.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyNewinfoGreentotal totalYearInfo = this.baseMapper.totalMonthNewinfoGreenDataYear(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoGreentotal totalMonthInfo = this.baseMapper.totalMonthNewinfoGreenData(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
//        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
//        totalInfo.setMonthNewEndArea(totalMonthInfo.getMonthNewEndArea());
//        totalInfo.setMonthZb(totalMonthInfo.getMonthZb());
//        //
//        totalInfo.setYearNumber(totalYearInfo.getYearNumber());
//        totalInfo.setYearArea(totalYearInfo.getYearArea());
//        totalInfo.setYearNewEndArea(totalYearInfo.getYearNewEndArea());
//        totalInfo.setYearZb(totalYearInfo.getYearZb());
//        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
//        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
//        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
//        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
//        totalInfo.setYearBuildindustryNumber(totalYearInfo.getYearBuildindustryNumber());
//        totalInfo.setYearBuildindustryArea(totalYearInfo.getYearBuildindustryArea());
//        totalInfo.setYearGreenNumber(totalYearInfo.getYearGreenNumber());
//        totalInfo.setYearGreenArea(totalYearInfo.getYearGreenArea());
//        totalInfo.setYearRankNumber(totalYearInfo.getYearRankNumber());
//        totalInfo.setYearRankArea(totalYearInfo.getYearRankArea());
//        totalInfo.setYearRankNumber1(totalYearInfo.getYearRankNumber1());
//        totalInfo.setYearRankArea1(totalYearInfo.getYearRankArea1());
//        totalInfo.setYearRankNumber2(totalYearInfo.getYearRankNumber2());
//        totalInfo.setYearRankArea2(totalYearInfo.getYearRankArea2());
//        totalInfo.setYearRankNumber3(totalYearInfo.getYearRankNumber3());
//        totalInfo.setYearRankArea3(totalYearInfo.getYearRankArea3());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNull("area_type");
//        ZgsReportEnergyNewinfoGreentotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            totalInfo.setId(energyInfo.getId());
//            totalInfo.setModifypersonaccount(sysUser.getUsername());
//            totalInfo.setModifypersonname(sysUser.getRealname());
//            totalInfo.setModifytime(new Date());
//            this.baseMapper.updateById(totalInfo);
//        } else {
//            //新增
//            totalInfo.setId(UUID.randomUUID().toString());
//            totalInfo.setCreatepersonaccount(sysUser.getUsername());
//            totalInfo.setCreatepersonname(sysUser.getRealname());
//            totalInfo.setCreatetime(new Date());
//            totalInfo.setAreacode(sysUser.getAreacode());
//            totalInfo.setAreaname(sysUser.getAreaname());
//            totalInfo.setFilltm(yearMonth);
//            totalInfo.setQuarter(new BigDecimal(quarter));
//            totalInfo.setYear(new BigDecimal(year));
//            this.baseMapper.insert(totalInfo);
//        }
//    }

//    @Override
//    public void initEnergyNewinfoGreenTotalCity(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportEnergyNewinfoGreentotal totalInfo = new ZgsReportEnergyNewinfoGreentotal();
//        totalInfo.setApplydate(new Date());
//        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        totalInfo.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyNewinfoGreentotal totalYearInfo = this.baseMapper.totalMonthNewinfoGreenDataYearCity(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoGreentotal totalMonthInfo = this.baseMapper.totalMonthNewinfoGreenDataCity(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
//        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
//        totalInfo.setMonthNewEndArea(totalMonthInfo.getMonthNewEndArea());
//        totalInfo.setMonthZb(totalMonthInfo.getMonthZb());
//        //
//        totalInfo.setYearNumber(totalYearInfo.getYearNumber());
//        totalInfo.setYearArea(totalYearInfo.getYearArea());
//        totalInfo.setYearNewEndArea(totalYearInfo.getYearNewEndArea());
//        totalInfo.setYearZb(totalYearInfo.getYearZb());
//        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
//        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
//        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
//        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
//        totalInfo.setYearBuildindustryNumber(totalYearInfo.getYearBuildindustryNumber());
//        totalInfo.setYearBuildindustryArea(totalYearInfo.getYearBuildindustryArea());
//        totalInfo.setYearGreenNumber(totalYearInfo.getYearGreenNumber());
//        totalInfo.setYearGreenArea(totalYearInfo.getYearGreenArea());
//        totalInfo.setYearRankNumber(totalYearInfo.getYearRankNumber());
//        totalInfo.setYearRankArea(totalYearInfo.getYearRankArea());
//        totalInfo.setYearRankNumber1(totalYearInfo.getYearRankNumber1());
//        totalInfo.setYearRankArea1(totalYearInfo.getYearRankArea1());
//        totalInfo.setYearRankNumber2(totalYearInfo.getYearRankNumber2());
//        totalInfo.setYearRankArea2(totalYearInfo.getYearRankArea2());
//        totalInfo.setYearRankNumber3(totalYearInfo.getYearRankNumber3());
//        totalInfo.setYearRankArea3(totalYearInfo.getYearRankArea3());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNotNull("area_type");
//        ZgsReportEnergyNewinfoGreentotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            totalInfo.setId(energyInfo.getId());
//            totalInfo.setModifypersonaccount(sysUser.getUsername());
//            totalInfo.setModifypersonname(sysUser.getRealname());
//            totalInfo.setModifytime(new Date());
//            this.baseMapper.updateById(totalInfo);
//        } else {
//            //新增
//            totalInfo.setId(UUID.randomUUID().toString());
//            totalInfo.setCreatepersonaccount(sysUser.getUsername());
//            totalInfo.setCreatepersonname(sysUser.getRealname());
//            totalInfo.setCreatetime(new Date());
//            totalInfo.setAreacode(sysUser.getAreacode());
//            totalInfo.setAreaname(sysUser.getAreaname());
//            totalInfo.setFilltm(yearMonth);
//            totalInfo.setQuarter(new BigDecimal(quarter));
//            totalInfo.setYear(new BigDecimal(year));
//            totalInfo.setAreaType("1");
//            this.baseMapper.insert(totalInfo);
//        }
//    }

    @Override
    public void initEnergyNewinfoGreenTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState) {
        ZgsReportEnergyNewinfoGreentotal totalInfo = new ZgsReportEnergyNewinfoGreentotal();
        if (type == 0) {
            totalInfo.setApplydate(new Date());
            totalInfo.setBackreason("");
        }
        if (applyState != null) {
            totalInfo.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoGreentotal totalYearInfo = this.baseMapper.totalMonthNewinfoGreenDataYearCity(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyNewinfoGreentotal totalMonthInfo = this.baseMapper.totalMonthNewinfoGreenDataCity(yearMonth, areacode);
        //以上方案取消，市区计算县区合计值，只计算通过的数据合计值，待审核和已退回不计入合计值内
//        ZgsReportEnergyNewinfoGreentotal cityTotal = lastGreenTotalDataCity(yearMonth, areacode, 2);
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
        totalInfo.setMonthNewEndArea(totalMonthInfo.getMonthNewEndArea());
        totalInfo.setMonthZb(totalMonthInfo.getMonthZb());
        //
        totalInfo.setYearNumber(totalYearInfo.getYearNumber());
        totalInfo.setYearArea(totalYearInfo.getYearArea());
        totalInfo.setYearNewEndArea(totalYearInfo.getYearNewEndArea());
        totalInfo.setYearZb(totalYearInfo.getYearZb());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        totalInfo.setYearBuildindustryNumber(totalYearInfo.getYearBuildindustryNumber());
        totalInfo.setYearBuildindustryArea(totalYearInfo.getYearBuildindustryArea());
        totalInfo.setYearGreenNumber(totalYearInfo.getYearGreenNumber());
        totalInfo.setYearGreenArea(totalYearInfo.getYearGreenArea());
        totalInfo.setYearRankNumber(totalYearInfo.getYearRankNumber());
        totalInfo.setYearRankArea(totalYearInfo.getYearRankArea());
        totalInfo.setYearRankNumber1(totalYearInfo.getYearRankNumber1());
        totalInfo.setYearRankArea1(totalYearInfo.getYearRankArea1());
        totalInfo.setYearRankNumber2(totalYearInfo.getYearRankNumber2());
        totalInfo.setYearRankArea2(totalYearInfo.getYearRankArea2());
        totalInfo.setYearRankNumber3(totalYearInfo.getYearRankNumber3());
        totalInfo.setYearRankArea3(totalYearInfo.getYearRankArea3());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNotNull("area_type");
        ZgsReportEnergyNewinfoGreentotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            if (sysUser != null) {
                totalInfo.setModifypersonaccount(sysUser.getUsername());
                totalInfo.setModifypersonname(sysUser.getRealname());
            }
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                totalInfo.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    totalInfo.setCreatepersonaccount(sysUser.getUsername());
                    totalInfo.setCreatepersonname(sysUser.getRealname());
                }
                totalInfo.setCreatetime(new Date());
                totalInfo.setAreacode(areacode);
                totalInfo.setAreaname(areaname);
                totalInfo.setFilltm(yearMonth);
                totalInfo.setQuarter(new BigDecimal(quarter));
                totalInfo.setYear(new BigDecimal(year));
                totalInfo.setAreaType("1");
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }

    @Override
    public void spProject(String id, Integer spStatus, String backreason) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        UpdateWrapper<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfoUpdateWrapper = new UpdateWrapper();
        UpdateWrapper<ZgsReportEnergyNewinfoTotal> zgsReportEnergyNewinfoTotalUpdateWrapper = new UpdateWrapper();
        UpdateWrapper<ZgsReportEnergyNewinfoTotal> zgsReportEnergyNewinfoTotalUpdateWrapperForZx = new UpdateWrapper();
        UpdateWrapper<ZgsReportEnergyNewinfoGreentotal> zgsReportEnergyNewinfoGreentotalUpdateWrapper = new UpdateWrapper();
        boolean spBol = false;

        // 绿色建筑汇总
        ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal = new ZgsReportEnergyNewinfoGreentotal();
        zgsReportEnergyNewinfoGreentotal.setId(id);
        zgsReportEnergyNewinfoGreentotal.setMonthNumber(null);
        zgsReportEnergyNewinfoGreentotal.setMonthArea(null);
        zgsReportEnergyNewinfoGreentotal.setMonthNewEndArea(null);
        zgsReportEnergyNewinfoGreentotal.setMonthZb(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildhouseNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildhouseArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildpublicArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildpublicNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildindustryArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildindustryNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearNewEndArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearZb(null);
        zgsReportEnergyNewinfoGreentotal.setYearGreenNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearGreenArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber1(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea1(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber2(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea2(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber3(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea3(null);


        //市州上报节能汇总全部被退回
        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
        zgsReportEnergyNewinfoTotal.setMonthNumber(null);
        zgsReportEnergyNewinfoTotal.setMonthArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(null);
        zgsReportEnergyNewinfoTotal.setYearLowArea(null);
        zgsReportEnergyNewinfoTotal.setYearLowNumber(null);
        zgsReportEnergyNewinfoTotal.setYearZeroArea(null);
        zgsReportEnergyNewinfoTotal.setYearZeroNumber(null);
        zgsReportEnergyNewinfoTotal.setYearSolarArea(null);
        zgsReportEnergyNewinfoTotal.setYearSolarNumber(null);
        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(null);
        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(null);
        zgsReportEnergyNewinfoTotal.setYearOtherArea(null);
        zgsReportEnergyNewinfoTotal.setYearOtherNumber(null);


        //修改项目清单的状态
        ZgsReportEnergyNewinfoGreentotal newinfoGreentotal = this.baseMapper.selectById(id);

        ZgsReportEnergyNewinfo energyNewinfo = new ZgsReportEnergyNewinfo();
        energyNewinfo.setBuildingArea(null);
        energyNewinfo.setLowEnergy(null);
        energyNewinfo.setZeroEnergy(null);
        energyNewinfo.setGeothermalEnergy(null);
        energyNewinfo.setGeothermalEnergyHigh(null);
        energyNewinfo.setSolarEnergy(null);
        energyNewinfo.setSolarEnergyHigh(null);
        energyNewinfo.setOtherEnergy(null);
        if (spStatus == 0) {

            //驳回
            // 修改清单状态（绿色清单+新建建筑清单）
            zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state1));
            zgsReportEnergyNewinfoUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoGreentotal.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoGreentotal.setBackreason(backreason);

            // 绿色建筑汇总退回
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoGreentotalUpdateWrapper.eq("filltm", newinfoGreentotal.getFilltm());
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.eq("areacode", newinfoGreentotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                zgsReportEnergyNewinfoGreentotalUpdateWrapper.isNotNull("area_type");
            } else {
                zgsReportEnergyNewinfoGreentotalUpdateWrapper.isNull("area_type");
            }
            zgsReportEnergyNewinfoGreentotalService.update(null, zgsReportEnergyNewinfoGreentotalUpdateWrapper);


            // 修改新建建筑汇总
            zgsReportEnergyNewinfoTotalUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoTotalUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoTotalUpdateWrapper.eq("filltm", newinfoGreentotal.getFilltm());
            zgsReportEnergyNewinfoTotalUpdateWrapper.eq("areacode", newinfoGreentotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                zgsReportEnergyNewinfoTotalUpdateWrapper.isNotNull("area_type");
            } else {
                zgsReportEnergyNewinfoTotalUpdateWrapper.isNull("area_type");
            }
            zgsReportEnergyNewinfoTotalService.update(null, zgsReportEnergyNewinfoTotalUpdateWrapper);



            // 原有update会将未赋值数据进行置空 rxl 20230726

            // 新建建筑汇总 退回

            /*zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoTotal.setBackreason(backreason);*/

            /*QueryWrapper<ZgsReportEnergyNewinfoTotal> wrapperAll = new QueryWrapper();
            wrapperAll.eq("filltm", newinfoTotal.getFilltm());
            wrapperAll.eq("areacode", newinfoTotal.getAreacode());*/

            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                //省厅账号退回后，市州汇总表子项状态改为待审批
                ZgsReportEnergyNewinfoGreentotal info = new ZgsReportEnergyNewinfoGreentotal();
                BeanUtils.copyProperties(zgsReportEnergyNewinfoGreentotal, info);
                info.setId(null);
                info.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                info.setBackreason("");
                QueryWrapper<ZgsReportEnergyNewinfoGreentotal> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoGreentotal.getFilltm());
                wrapper.likeRight("areacode", newinfoGreentotal.getAreacode());
                wrapper.isNull("area_type");
                this.baseMapper.update(info, wrapper);
                //省退市，删掉该条汇总数据-不留痕
//                this.baseMapper.deleteById(id);

                // 将原有修改方法进行修改(原因：原有方法会导致竣工时间置空)  rxl 20230726
                //修改子项状态
                zgsReportEnergyNewinfoTotalUpdateWrapperForZx.set("applystate",new BigDecimal(GlobalConstants.apply_state1));
                // zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                // zgsReportEnergyNewinfoTotal.setBackreason("");
                zgsReportEnergyNewinfoTotalUpdateWrapperForZx.set("backreason","");
                // QueryWrapper<ZgsReportEnergyNewinfoTotal> wrapperAllSon = new QueryWrapper();
                // wrapperAllSon.eq("filltm", newinfoTotal.getFilltm());
                // wrapperAllSon.likeRight("areacode", newinfoTotal.getAreacode());
                // wrapperAllSon.isNull("area_type");
                zgsReportEnergyNewinfoTotalUpdateWrapperForZx.eq("filltm", newinfoGreentotal.getFilltm());
                zgsReportEnergyNewinfoTotalUpdateWrapperForZx.likeRight("areacode", newinfoGreentotal.getAreacode());
                zgsReportEnergyNewinfoTotalUpdateWrapperForZx.isNull("area_type");
                zgsReportEnergyNewinfoTotalService.update(null, zgsReportEnergyNewinfoTotalUpdateWrapperForZx);


                // 修改清单状态
                zgsReportEnergyNewinfoUpdateWrapper.eq("filltm", newinfoGreentotal.getFilltm());
                zgsReportEnergyNewinfoUpdateWrapper.likeRight("areacode", newinfoGreentotal.getAreacode());
                this.zgsReportEnergyNewinfoService.update(null, zgsReportEnergyNewinfoUpdateWrapper);

            }
        } else {
            spBol = true;  // 根据项目通过或驳回状态  更新清单列表数据状态
            //通过
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            energyNewinfo.setBackreason("");

            zgsReportEnergyNewinfoGreentotal.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportEnergyNewinfoGreentotal.setBackreason("");

            zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportEnergyNewinfoTotal.setBackreason("");

        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市州账号审批
            // 修改清单（新建建筑清单+绿色建筑清单，为同一张表）   原有更新存在竣工时间置空问题  rxl 20230729
            if (spBol) { // true 上报通过   false 上报驳回
                zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state2));
                zgsReportEnergyNewinfoUpdateWrapper.set("backreason","");
            } else {
                zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
                zgsReportEnergyNewinfoUpdateWrapper.set("backreason",backreason);
            }

            zgsReportEnergyNewinfoUpdateWrapper.eq("filltm", newinfoGreentotal.getFilltm());
            zgsReportEnergyNewinfoUpdateWrapper.eq("areacode", newinfoGreentotal.getAreacode());
            zgsReportEnergyNewinfoUpdateWrapper.isNotNull("end_date");
            zgsReportEnergyNewinfoService.update(null, zgsReportEnergyNewinfoUpdateWrapper);


            //市退县，删掉该条汇总数据-不留痕
//            if (spStatus == 0) {
//                this.baseMapper.deleteById(id);
//            }
            //每次市州审批完更新合计值
//            String filltm = newinfoTotal.getFilltm();
//            String year = filltm.substring(0, 4);
//            String month = filltm.substring(5, 7);
//            int intMonth = Integer.parseInt(month);
//            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//            initEnergyNewinfoGreenTotalCityAll(year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser, 1);
        }
        // 修改新建建筑汇总表中最后一行合计值的状态
        // this.baseMapper.updateById(zgsReportEnergyNewinfoGreentotal);
        // 修改绿色建筑汇总表中最后一行合计值的状态
        // this.zgsReportEnergyNewinfoTotalService.updateById(zgsReportEnergyNewinfoTotal);
        // 修改清单数据状态（绿色建筑清单+新建建筑清单）
        this.baseMapper.updateById(zgsReportEnergyNewinfoGreentotal);
    }

    @Override
    public ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataProvince(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastGreenTotalDataProvince(filltm, areacode, applystate);
    }

    @Override
    public ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataCity(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastGreenTotalDataCity(filltm, areacode, applystate);
    }
}
