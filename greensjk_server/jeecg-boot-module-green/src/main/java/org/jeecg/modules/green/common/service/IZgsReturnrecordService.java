package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 退回记录信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
public interface IZgsReturnrecordService extends IService<ZgsReturnrecord> {

    void deleteReturnRecordLastLog(String businessguid);

    void deleteReturnRecordLastLogByLx(String businessguid);
}
