package org.jeecg.modules.green.lsfz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelop;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelopDetail;

/**
 * @describe: 绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/13 14:31
 */
public interface ZgsGreendevelopDetailService extends IService<ZgsGreendevelopDetail> {





}
