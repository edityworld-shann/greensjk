package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificexpenditureclear;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目申报验收经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsScientificexpenditureclearMapper extends BaseMapper<ZgsScientificexpenditureclear> {

}
