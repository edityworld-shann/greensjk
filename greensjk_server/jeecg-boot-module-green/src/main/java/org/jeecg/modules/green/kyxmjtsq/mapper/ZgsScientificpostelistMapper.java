package org.jeecg.modules.green.kyxmjtsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostelist;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研结题项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsScientificpostelistMapper extends BaseMapper<ZgsScientificpostelist> {

}
