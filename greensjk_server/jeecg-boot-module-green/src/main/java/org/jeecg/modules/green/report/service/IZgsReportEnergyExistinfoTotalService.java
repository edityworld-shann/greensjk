package org.jeecg.modules.green.report.service;

import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfoTotal;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;

import java.math.BigDecimal;

/**
 * @Description: 既有建筑节能改造完成情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface IZgsReportEnergyExistinfoTotalService extends IService<ZgsReportEnergyExistinfoTotal> {

//    void initEnergyExistinfoTotal(String year, int quarter, String yearMonth);

//    void initEnergyExistinfoTotalCity(String year, int quarter, String yearMonth);

    void initEnergyExistinfoTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState);

    void spProject(String id, Integer spStatus, String backreason);

    ZgsReportEnergyExistinfoTotal lastExistTotalDataProvince(String filltm, String areacode, Integer applystate);

    ZgsReportEnergyExistinfoTotal lastExistTotalDataCity(String filltm, String areacode, Integer applystate);

    void initEnergyExistinfoTotalZero(String year, int quarter, String yearMonth);

    void energyExistinfoTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser);

    void initAddZeroExistinfoNotInExistinfoTotal();
}
