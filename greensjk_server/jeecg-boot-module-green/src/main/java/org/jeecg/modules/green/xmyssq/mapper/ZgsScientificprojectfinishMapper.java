package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface ZgsScientificprojectfinishMapper extends BaseMapper<ZgsScientificprojectfinish> {

}
