package org.jeecg.modules.green.report.service.impl;

import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProject;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyNewinfoMapper;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 新建绿色建筑-节能项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Service
public class ZgsReportEnergyNewinfoServiceImpl extends ServiceImpl<ZgsReportEnergyNewinfoMapper, ZgsReportEnergyNewinfo> implements IZgsReportEnergyNewinfoService {


    /**
     * 补零0️⃣ 清单
     *
     * @return
     */
    @Override
    public void addIterm(String filltm) {
        try {
            ZgsReportEnergyNewinfo zgsReportEnergyNewinfo = new ZgsReportEnergyNewinfo();
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            zgsReportEnergyNewinfo.setId(UUID.randomUUID().toString());
            zgsReportEnergyNewinfo.setCreatepersonaccount(sysUser.getUsername());
            zgsReportEnergyNewinfo.setCreatepersonname(sysUser.getRealname());
            zgsReportEnergyNewinfo.setCreatetime(new Date());
            zgsReportEnergyNewinfo.setAreacode(sysUser.getAreacode());
            zgsReportEnergyNewinfo.setAreaname(sysUser.getAreaname());
            //计算月份、年、季度
            String year = filltm.substring(0, 4);
            String month = filltm.substring(5, 7);
            int intMonth = Integer.parseInt(month);
            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
            zgsReportEnergyNewinfo.setFilltm(filltm);
            zgsReportEnergyNewinfo.setQuarter(new BigDecimal(quarter));
            zgsReportEnergyNewinfo.setYear(new BigDecimal(year));
            zgsReportEnergyNewinfo.setBuildingArea(new BigDecimal(0));
            Date format = new Date();
            Date currentTime = new Date(format.getTime());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String tempDate = filltm + "-01 06:00:00";
            Date initDate = simpleDateFormat.parse(tempDate);
            zgsReportEnergyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
            zgsReportEnergyNewinfo.setApplydate(currentTime);
            zgsReportEnergyNewinfo.setStartDate(initDate);
            zgsReportEnergyNewinfo.setBuildType("1");
            zgsReportEnergyNewinfo.setEndDate(initDate);
            zgsReportEnergyNewinfo.setFillpersonname("无");
            zgsReportEnergyNewinfo.setFillpersontel("无");
            zgsReportEnergyNewinfo.setFillunit("无");
            zgsReportEnergyNewinfo.setGeothermalEnergy(new BigDecimal(0));
            zgsReportEnergyNewinfo.setGeothermalEnergyHigh(new BigDecimal(0));
            zgsReportEnergyNewinfo.setGreenRank("基本级");
            zgsReportEnergyNewinfo.setLowEnergy(new BigDecimal(0));
            zgsReportEnergyNewinfo.setOtherEnergy(new BigDecimal(0));
            zgsReportEnergyNewinfo.setProjectname("无");
            zgsReportEnergyNewinfo.setRunDate(initDate);
            zgsReportEnergyNewinfo.setSolarEnergy(new BigDecimal(0));
            zgsReportEnergyNewinfo.setSolarEnergyHigh(new BigDecimal(0));
            zgsReportEnergyNewinfo.setSysCode("无");
            zgsReportEnergyNewinfo.setZeroEnergy(new BigDecimal(0));
            ZgsReportEnergyNewinfo zgsReportEnergyNewinfo1 = new ZgsReportEnergyNewinfo();
            ZgsReportEnergyNewinfo zgsReportEnergyNewinfo2 = new ZgsReportEnergyNewinfo();
            BeanUtils.copyProperties(zgsReportEnergyNewinfo, zgsReportEnergyNewinfo1);
            BeanUtils.copyProperties(zgsReportEnergyNewinfo, zgsReportEnergyNewinfo2);
            //1、新建绿色建筑-节能项目清单
            zgsReportEnergyNewinfo1.setId(UUID.randomUUID().toString());
            zgsReportEnergyNewinfo1.setIsNo("1");
            //1、新建建筑节能情况
            zgsReportEnergyNewinfo2.setId(UUID.randomUUID().toString());
            zgsReportEnergyNewinfo2.setIsNo("0");
            //默认生成绿色清单
            this.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfo1, ValidateEncryptEntityUtil.isEncrypt));
//            this.save(zgsReportEnergyNewinfo2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
