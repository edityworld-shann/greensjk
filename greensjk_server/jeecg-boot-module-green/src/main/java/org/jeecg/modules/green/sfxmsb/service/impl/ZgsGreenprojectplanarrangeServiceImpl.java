package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenprojectplanarrange;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsGreenprojectplanarrangeMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsGreenprojectplanarrangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 绿色建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsGreenprojectplanarrangeServiceImpl extends ServiceImpl<ZgsGreenprojectplanarrangeMapper, ZgsGreenprojectplanarrange> implements IZgsGreenprojectplanarrangeService {

}
