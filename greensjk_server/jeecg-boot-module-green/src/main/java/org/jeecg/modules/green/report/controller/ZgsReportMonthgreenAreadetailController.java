package org.jeecg.modules.green.report.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenAreadetail;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenAreadetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthgreen_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_monthgreen_areadetail")
@RestController
@RequestMapping("/report/zgsReportMonthgreenAreadetail")
@Slf4j
public class ZgsReportMonthgreenAreadetailController extends JeecgController<ZgsReportMonthgreenAreadetail, IZgsReportMonthgreenAreadetailService> {
	@Autowired
	private IZgsReportMonthgreenAreadetailService zgsReportMonthgreenAreadetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthgreenAreadetail
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-分页列表查询")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-分页列表查询", notes="zgs_report_monthgreen_areadetail-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthgreenAreadetail zgsReportMonthgreenAreadetail,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportMonthgreenAreadetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthgreenAreadetail, req.getParameterMap());
		Page<ZgsReportMonthgreenAreadetail> page = new Page<ZgsReportMonthgreenAreadetail>(pageNo, pageSize);
		IPage<ZgsReportMonthgreenAreadetail> pageList = zgsReportMonthgreenAreadetailService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportMonthgreenAreadetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-添加")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-添加", notes="zgs_report_monthgreen_areadetail-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthgreenAreadetail zgsReportMonthgreenAreadetail) {
		zgsReportMonthgreenAreadetailService.save(zgsReportMonthgreenAreadetail);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthgreenAreadetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-编辑")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-编辑", notes="zgs_report_monthgreen_areadetail-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthgreenAreadetail zgsReportMonthgreenAreadetail) {
		zgsReportMonthgreenAreadetailService.updateById(zgsReportMonthgreenAreadetail);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-通过id删除")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-通过id删除", notes="zgs_report_monthgreen_areadetail-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthgreenAreadetailService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-批量删除")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-批量删除", notes="zgs_report_monthgreen_areadetail-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthgreenAreadetailService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreen_areadetail-通过id查询")
	@ApiOperation(value="zgs_report_monthgreen_areadetail-通过id查询", notes="zgs_report_monthgreen_areadetail-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthgreenAreadetail zgsReportMonthgreenAreadetail = zgsReportMonthgreenAreadetailService.getById(id);
		if(zgsReportMonthgreenAreadetail==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportMonthgreenAreadetail);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthgreenAreadetail
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthgreenAreadetail zgsReportMonthgreenAreadetail) {
        return super.exportXls(request, zgsReportMonthgreenAreadetail, ZgsReportMonthgreenAreadetail.class, "zgs_report_monthgreen_areadetail");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthgreenAreadetail.class);
    }

}
