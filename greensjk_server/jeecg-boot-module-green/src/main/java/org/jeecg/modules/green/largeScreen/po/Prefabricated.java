package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class Prefabricated implements Serializable {
    /**
     * 建筑总面积
     */
    private String name;

    /**
     * 装配式开工总面积
     */
    private double value;
}
