package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 既有建筑-节能改造项目清单
 * @Author: jeecg-boot
 * @Date:   2022-05-19
 * @Version: V1.0
 */
public interface ZgsReportEnergyExistinfoMapper extends BaseMapper<ZgsReportEnergyExistinfo> {

}
