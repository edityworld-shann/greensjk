package org.jeecg.modules.green.xmyszssq.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsExamplecertificatebaseMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 示范项目验收证书基础表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsExamplecertificatebaseServiceImpl extends ServiceImpl<ZgsExamplecertificatebaseMapper, ZgsExamplecertificatebase> implements IZgsExamplecertificatebaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;
    @Autowired
    private IZgsPlanresultbaseService planresultbaseService;

    @Override
    public int spProject(ZgsExamplecertificatebase zgsExamplecertificatebase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsExamplecertificatebase != null && StringUtils.isNotEmpty(zgsExamplecertificatebase.getId())) {
            ZgsExamplecertificatebase examplecertificatebase = new ZgsExamplecertificatebase();
            examplecertificatebase.setId(zgsExamplecertificatebase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsExamplecertificatebase.getId());
            zgsReturnrecord.setReturnreason(zgsExamplecertificatebase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE16);
            ZgsExamplecertificatebase baseInfo = getById(zgsExamplecertificatebase.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsExamplecertificatebase.getSpStatus() == 1) {
                //通过记录状态==0
                examplecertificatebase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsExamplecertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsExamplecertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsExamplecertificatebase.getStatus())) {
                    examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    examplecertificatebase.setFirstdate(new Date());
                    examplecertificatebase.setFirstperson(sysUser.getRealname());
                    examplecertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    examplecertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS2);
                    examplecertificatebase.setFinishperson(sysUser.getRealname());
                    examplecertificatebase.setFinishdate(new Date());
                    examplecertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    // 因成果信息展示只在终审通过后做显示，在系统列表上不查看其状态，因此在每个角色审批下不做状态信息修改，只在示范项目验收证书终审通过时 对应的项目成果信息状态进行更新（通过）  rxl 20230629
                    // 二次修改，验收证书终审通过后且申报单位同意进行成果信息发布，则对该项目在成果简介阶段的状态直接进行更新，更新为"终审通过"状态，否则该项目不能进入成果简介阶段
                        /*QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper();
                        queryWrapper.eq("projectlibraryguid",baseInfo.getProjectlibraryguid());
                        ZgsPlanresultbase zgsPlanresultbase = zgsPlanresultbaseService.getOne(queryWrapper);
                        if (zgsPlanresultbase != null && zgsPlanresultbase.getResultShow().equals("1")) {
                            zgsPlanresultbase.setId(zgsPlanresultbase.getId());
                            zgsPlanresultbase.setStatus(GlobalConstants.SHENHE_STATUS8);  // 设置项目成果信息状态为 终审通过
                            zgsPlanresultbaseService.updateById(zgsPlanresultbase);
                        }*/

                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                examplecertificatebase.setAuditopinion(zgsExamplecertificatebase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsExamplecertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsExamplecertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsExamplecertificatebase.getStatus())) {
                    if (zgsExamplecertificatebase.getSpStatus() == 2) {
                        examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    examplecertificatebase.setFirstdate(new Date());
                    examplecertificatebase.setFirstperson(sysUser.getRealname());
                    examplecertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsExamplecertificatebase.getSpStatus() == 2) {
                        examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS5);
                    } else {
                        examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS15);
                    }
                    examplecertificatebase.setFinishperson(sysUser.getRealname());
                    examplecertificatebase.setFinishdate(new Date());
                    examplecertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsExamplecertificatebase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        examplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getProjectlibraryguid(), baseInfo.getEnterpriseguid(), zgsExamplecertificatebase.getAuditopinion());
                    }
                }
                if (zgsExamplecertificatebase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsExamplecertificatebase.getAuditopinion())) {
                        zgsExamplecertificatebase.setAuditopinion(" ");
                    }
                    examplecertificatebase.setAuditopinion(zgsExamplecertificatebase.getAuditopinion());
                    examplecertificatebase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsExamplecertificatebase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    examplecertificatebase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(examplecertificatebase, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }


    /**
     * 顺序编号（示范项目验收证书）
     * 实例：甘建科示验[2022] 1 号
     *
     * @return
     */
    @Override
    public String number() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        String qw = "甘建科示验[" + currentYear + "]";
        QueryWrapper<ZgsExamplecertificatebase> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("projectnumber", qw);
        int count = this.baseMapper.selectCount(queryWrapper);
        return qw + " " + (count + 1) + " 号";
    }
}
