package org.jeecg.modules.green.zjksb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 专家库表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Data
@TableName("zgs_expertinfo")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_expertinfo对象", description = "专家库表")
public class ZgsExpertinfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 上报日期
     */
   @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，3退回，4初审通过，5退回初审，2终审通过
     */
   @Excel(name = "审核状态", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，3退回，4初审通过，5退回初审，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15)
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15)
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15)
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String username;
    /**
     * 性别
     */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String sex;
    /**
     * 民族
     */
    @Excel(name = "民族", width = 15)
    @ApiModelProperty(value = "民族")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String national;
    /**
     * 证件类型
     */
//    @Excel(name = "证件类型", width = 15)
    @ApiModelProperty(value = "证件类型")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String certtype;
    /**
     * 证件编号
     */
//    @Excel(name = "证件编号", width = 15)
    @ApiModelProperty(value = "证件编号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String certnum;
    /**
     * 出生日期
     */
    @Excel(name = "出生日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "出生日期")
    private java.util.Date birthdate;
    /**
     * 是否院士
     */
//    @Excel(name = "是否院士", width = 15)
    @ApiModelProperty(value = "是否院士")
    private java.lang.String isacademician;
    /**
     * 党派
     */
//    @Excel(name = "党派", width = 15)
    @ApiModelProperty(value = "党派")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String parties;
    /**
     * 最高学位
     */
//    @Excel(name = "最高学位", width = 15)
    @ApiModelProperty(value = "最高学位")
    private java.lang.String highdegree;
    /**
     * 毕业学校
     */
//    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    private java.lang.String graduateschool;
    /**
     * 是否博导
     */
//    @Excel(name = "是否博导", width = 15)
    @ApiModelProperty(value = "是否博导")
    private java.lang.String isboguide;
    /**
     * 单位
     */
    @Excel(name = "单位", width = 25)
    @ApiModelProperty(value = "单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unit;
    /**
     * 现任职务
     */
    @Excel(name = "现任职务", width = 15)
    @ApiModelProperty(value = "现任职务")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String currentposition;
    /**
     * 技术职称
     */
    @Excel(name = "技术职称", width = 15)
    @ApiModelProperty(value = "技术职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String techtitle;
    /**
     * 文化程度
     */
    @Excel(name = "文化程度", width = 15)
    @ApiModelProperty(value = "文化程度")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String education;
    /**
     * 社会兼职
     */
//    @Excel(name = "社会兼职", width = 15)
    @ApiModelProperty(value = "社会兼职")
    private java.lang.String socialparttm;
    /**
     * 多个逗号隔开 专家库类别
     */
//    @Excel(name = "多个逗号隔开 专家库类别", width = 15)
    @ApiModelProperty(value = "多个逗号隔开 专家库类别")
    private java.lang.String expertlibcategory;
    /**
     * 简历
     */
//    @Excel(name = "简历", width = 15)
    @ApiModelProperty(value = "简历")
    private java.lang.String resume;
    /**
     * 个人学术背景
     */
//    @Excel(name = "个人学术背景", width = 15)
    @ApiModelProperty(value = "个人学术背景")
    private java.lang.String academicbackground;
    /**
     * 获奖情况
     */
//    @Excel(name = "获奖情况", width = 15)
    @ApiModelProperty(value = "获奖情况")
    private java.lang.String academicachievements;
    /**
     * 工程实践
     */
//    @Excel(name = "工程实践", width = 15)
    @ApiModelProperty(value = "工程实践")
    private java.lang.String engineerpractice;
    /**
     * 论文发表
     */
//    @Excel(name = "论文发表", width = 15)
    @ApiModelProperty(value = "论文发表")
    private java.lang.String paperspublished;
    /**
     * 其他（针对选取的专家库填写相应的资料）
     */
//    @Excel(name = "其他（针对选取的专家库填写相应的资料）", width = 15)
    @ApiModelProperty(value = "其他（针对选取的专家库填写相应的资料）")
    private java.lang.String othercontent;
    /**
     * 通讯地址
     */
//    @Excel(name = "通讯地址", width = 15)
    @ApiModelProperty(value = "通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String adderss;
    /**
     * 邮政编码
     */
//    @Excel(name = "邮政编码", width = 15)
    @ApiModelProperty(value = "邮政编码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String zipcode;
    /**
     * 电子邮箱
     */
    @Excel(name = "电子邮箱", width = 25)
    @ApiModelProperty(value = "电子邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String email;
    /**
     * 工作电话
     */
//    @Excel(name = "工作电话", width = 15)
    @ApiModelProperty(value = "工作电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String worktel;
    /**
     * 移动电话
     */
    @Excel(name = "移动电话", width = 15)
    @ApiModelProperty(value = "移动电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String mobile;
    /**
     * 秘书姓名
     */
//    @Excel(name = "秘书姓名", width = 15)
    @ApiModelProperty(value = "秘书姓名")
    private java.lang.String secretariatname;
    /**
     * 秘书邮箱
     */
//    @Excel(name = "秘书邮箱", width = 15)
    @ApiModelProperty(value = "秘书邮箱")
    private java.lang.String secretariatemail;
    /**
     * 秘书手机
     */
//    @Excel(name = "秘书手机", width = 15)
    @ApiModelProperty(value = "秘书手机")
    private java.lang.String secretariattel;
    /**
     * 外键 T_PROJECTUNITMEMBER.guid
     */
//    @Excel(name = "外键 T_PROJECTUNITMEMBER.guid", width = 15)
    @ApiModelProperty(value = "外键 T_PROJECTUNITMEMBER.guid")
    private java.lang.String userid;
    /**
     * 所学专业
     */
    @Excel(name = "所学专业", width = 15)
    @ApiModelProperty(value = "所学专业")
    private java.lang.String majorstudy;
    /**
     * 从事专业（大类）
     */
//    @Excel(name = "从事专业（大类）", width = 15)
    @ApiModelProperty(value = "从事专业（大类）")
    private java.lang.String majorwork;
    /**
     * 研究方向
     */
//    @Excel(name = "研究方向", width = 15)
    @ApiModelProperty(value = "研究方向")
    private java.lang.String studydirection;
    /**
     * 多个逗号隔开专家库类别
     */
//    @Excel(name = "多个逗号隔开专家库类别", width = 15)
    @ApiModelProperty(value = "多个逗号隔开专家库类别")
    private java.lang.String expertlibcategorynum;
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15)
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15)
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 健康状况
     */
//    @Excel(name = "健康状况", width = 15)
    @ApiModelProperty(value = "健康状况")
    private java.lang.String healthcondition;
    /**
     * 健康状况编码，字典：HEALTHCONDITION
     */
//    @Excel(name = "健康状况编码，字典：HEALTHCONDITION", width = 15)
    @ApiModelProperty(value = "健康状况编码，字典：HEALTHCONDITION")
    private java.lang.String healthconditionnum;
    /**
     * 身份证号码
     */
    @Excel(name = "身份证号码", width = 25)
    @ApiModelProperty(value = "身份证号码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String idcard;
    /**
     * 注册证书编号
     */
//    @Excel(name = "注册证书编号", width = 15)
    @ApiModelProperty(value = "注册证书编号")
    private java.lang.String registecertificate;
    /**
     * 注册证书类别
     */
//    @Excel(name = "注册证书类别", width = 15)
    @ApiModelProperty(value = "注册证书类别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String registecertificatetype;
    /**
     * 注册证书类别编码，字典：RegisteCertificateType
     */
//    @Excel(name = "注册证书类别编码，字典：RegisteCertificateType", width = 15)
    @ApiModelProperty(value = "注册证书类别编码，字典：RegisteCertificateType")
    private java.lang.String registecertificatetypenum;
    /**
     * 部门或院系
     */
//    @Excel(name = "部门或院系", width = 15)
    @ApiModelProperty(value = "部门或院系")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String department;
    /**
     * 申请加入其他专业详情填写
     */
//    @Excel(name = "申请加入其他专业详情填写", width = 15)
    @ApiModelProperty(value = "申请加入其他专业详情填写")
    private java.lang.String otherexpert;
    /**
     * 学术专长或研究方向
     */
//    @Excel(name = "学术专长或研究方向", width = 15)
    @ApiModelProperty(value = "学术专长或研究方向")
    private java.lang.String researcharea;
    /**
     * 从事专业（大类）编号，字典：MajorWorkType
     */
//    @Excel(name = "从事专业（大类）编号，字典：MajorWorkType", width = 15)
    @ApiModelProperty(value = "从事专业（大类）编号，字典：MajorWorkType")
    private java.lang.String majorworknum;
    /**
     * 从事专业（小类）
     */
//    @Excel(name = "从事专业（小类）", width = 15)
    @ApiModelProperty(value = "从事专业（小类）")
    private java.lang.String majorworkrefine;
    /**
     * 从事专业（小类）编号
     */
//    @Excel(name = "从事专业（小类）编号", width = 15)
    @ApiModelProperty(value = "从事专业（小类）编号")
    private java.lang.String majorworkrefinenum;
    /**
     * 申报人声明
     */
//    @Excel(name = "申报人声明", width = 15)
    @ApiModelProperty(value = "申报人声明")
    private java.lang.String declaration;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(exist = false)
    private List<ZgsExpertworkresume> zgsExpertworkresumeList;
    @TableField(exist = false)
    private List<ZgsExpertaward> zgsExpertawardList;
    @TableField(exist = false)
    private List<ZgsExpertpaper> zgsExpertpaperList;
    @TableField(exist = false)
    private List<ZgsExpertmonograph> zgsExpertmonographList;
    @TableField(exist = false)
    private List<ZgsExpertpatent> zgsExpertpatentList;
    @TableField(exist = false)
    private List<ZgsExpertprojectbear> zgsExpertprojectbearList;

    @TableField(exist = false)
    private Integer spStatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
