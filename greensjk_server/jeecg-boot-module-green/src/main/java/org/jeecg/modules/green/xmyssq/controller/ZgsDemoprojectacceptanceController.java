package org.jeecg.modules.green.xmyssq.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectapplicationService;
import org.jeecg.modules.green.xmyssq.service.IZgsSaexpertService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 建设科技示范项目验收申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-13
 * @Version: V1.0
 */
@Api(tags = "建设科技示范项目验收申请业务主表")
@RestController
@RequestMapping("/xmyssq/zgsDemoprojectacceptance")
@Slf4j
public class ZgsDemoprojectacceptanceController extends JeecgController<ZgsDemoprojectacceptance, IZgsDemoprojectacceptanceService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsDemoprojectapplicationService zgsDemoprojectapplicationService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsSaexpertService zgsSaexpertService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;

    /**
     * 分页列表查询
     *
     * @param zgsDemoprojectacceptance
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-分页列表查询")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-分页列表查询", notes = "建设科技示范项目验收申请业务主表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsDemoprojectacceptance zgsDemoprojectacceptance,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper = new QueryWrapper<>();
        if (zgsDemoprojectacceptance != null && StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectname())) {
            queryWrapper.like("projectname", zgsDemoprojectacceptance.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectnum())) {
            queryWrapper.like("projectnum", zgsDemoprojectacceptance.getProjectnum());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnum,3,4)", year);
        }
        //默认不显示历史数据
//        if (StringUtils.isNotEmpty(zgsDemoprojectacceptance.getStatus())) {
//            if (!"4".equals(zgsDemoprojectacceptance.getStatus())) {
//                queryWrapper.eq("ishistory", 0);
//            }
//        } else {
//            queryWrapper.and(qwr -> {
//                qwr.ne("ishistory", 1).or().isNull("ishistory");
//            });
//        }
        if (StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectstage())) {
            if (!"-1".equals(zgsDemoprojectacceptance.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsDemoprojectacceptance.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initDemoprojectacceptanceSelect(queryWrapper, zgsDemoprojectacceptance.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsDemoprojectacceptance.getStatus(), sysUser, 1);

       // QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, zgsDemoprojectacceptance.getStatus(), sysUser, 1);


        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    QueryWrapper<ZgsSaexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
                    zgsSofttechexpertQueryWrapper.eq("expertguid", expertId);
                    zgsSofttechexpertQueryWrapper.ne("isdelete", 1);
//                    zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSofttechexpertQueryWrapper);
                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
                        queryWrapper.and(qw -> {
                            qw.and(w1 -> {
                                for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
                                    w1.or(w2 -> {
                                        w2.eq("id", zgsSaexpert.getBusinessguid());
                                    });
                                }
                            });
                        });
                    } else {
                        queryWrapper.eq("id", "null");
                    }
                }
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsDemoprojectacceptance.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
//                queryWrapper.orderByAsc("applydate");
                queryWrapper.orderByDesc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("projectenddate");
            } else if (wcArrow == 1) {
                flag = false;
                // queryWrapper.orderByDesc("projectenddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        } else {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        }
        Page<ZgsDemoprojectacceptance> page = new Page<ZgsDemoprojectacceptance>(pageNo, pageSize);
        // IPage<ZgsDemoprojectacceptance> pageList = zgsDemoprojectacceptanceService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsDemoprojectacceptance> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsDemoprojectacceptance> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsDemoprojectacceptance> pageList = null;

        // 示范项目任务书
        redisUtil.expire("sfxmys",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmysOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmysOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsDemoprojectacceptance.getDspForTjdw())) {
            if (zgsDemoprojectacceptance.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("sfxmys")) {
                    QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsDemoprojectacceptanceService.page(page, queryWrapper);
                    redisUtil.set("sfxmys",pageListForProjectCount.getTotal());
                }
            }
            if (zgsDemoprojectacceptance.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("sfxmysOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsDemoprojectacceptanceService.page(page, queryWrapper);
                    redisUtil.set("sfxmysOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getDspForTjdw()) && StringUtils.isBlank(zgsDemoprojectacceptance.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsDemoprojectacceptanceService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsDemoprojectacceptance.getDspForTjdw()) && StringUtils.isNotBlank(zgsDemoprojectacceptance.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, "", sysUser, 1);
            pageList = zgsDemoprojectacceptanceService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getDspForSt()) && zgsDemoprojectacceptance.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsDemoprojectacceptanceService.page(page, queryWrapper);
            redisUtil.set("sfxmysOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryForWorkbench(queryWrapper, zgsDemoprojectacceptance.getStatus(), sysUser, 1);
            pageList = zgsDemoprojectacceptanceService.page(page, queryWrapper);
        }


        /*// 示范项目验收
        redisUtil.expire("sfxmys",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmys")) {
            if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getFlagByWorkTable()) && zgsDemoprojectacceptance.getFlagByWorkTable().equals("1")) {
                redisUtil.set("sfxmys",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmysOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmysOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getDspForSt()) && zgsDemoprojectacceptance.getDspForSt().equals("2")) {
                redisUtil.set("sfxmysOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmysOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmysOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getDspForTjdw()) && zgsDemoprojectacceptance.getDspForTjdw().equals("3")) {
                redisUtil.set("sfxmysOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsDemoprojectacceptance demoprojectacceptance = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(demoprojectacceptance, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
                    }
                    if (sysUser.getEnterpriseguid().equals(demoprojectacceptance.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(demoprojectacceptance.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(demoprojectacceptance.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(demoprojectacceptance.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(demoprojectacceptance.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(demoprojectacceptance.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //专家审批
                        QueryWrapper<ZgsSaexpert> wrapper = new QueryWrapper();
                        wrapper.eq("expertguid", expertId);
                        wrapper.eq("businessguid", demoprojectacceptance.getId());
                        ZgsSaexpert saexpert = zgsSaexpertService.getOne(wrapper);
                        if (!(saexpert != null && StringUtils.isNotEmpty(saexpert.getAuditconclusionnum()))) {
                            pageList.getRecords().get(m).setSpStatus(2);
                        }
                    }

                    // 如果当前登录角色为个人或单位时 根据授权标识判断是否可进行编辑操作
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                        if (StrUtil.isNotBlank(demoprojectacceptance.getSqbj()) && demoprojectacceptance.getSqbj().equals("1")) {
                            demoprojectacceptance.setUpdateFlag("1");   // 可编辑
                            demoprojectacceptance.setSqbjBol(false);
                        } else if (StrUtil.isNotBlank(demoprojectacceptance.getSqbj()) && demoprojectacceptance.getSqbj().equals("2")) {
                            demoprojectacceptance.setUpdateFlag("2");  // 取消编辑按钮
                            demoprojectacceptance.setSqbjBol(true);
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(demoprojectacceptance.getUpdateSave()) && demoprojectacceptance.getUpdateSave().equals("1")) {  // 修改未提交
                        demoprojectacceptance.setBjzt("1");
                    } else if (StrUtil.isNotBlank(demoprojectacceptance.getUpdateSave()) && demoprojectacceptance.getUpdateSave().equals("2")) {  // 修改已提交
                        demoprojectacceptance.setBjzt("2");
                    }

                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsDemoprojectacceptance demoprojectacceptance = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(demoprojectacceptance, ValidateEncryptEntityUtil.isDecrypt));
                    // 将待审核、初审通过、终审通过、形审通过、专家通过的所有项目的退回原因置空
                    if (demoprojectacceptance.getStatus().equals("1") || demoprojectacceptance.getStatus().equals("4") || demoprojectacceptance.getStatus().equals("8") ||
                            demoprojectacceptance.getStatus().equals("2") || demoprojectacceptance.getStatus().equals("10")) {

                        demoprojectacceptance.setAuditopinion("");
                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(demoprojectacceptance.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    } else if (GlobalConstants.SHENHE_STATUS8.equals(demoprojectacceptance.getStatus())) {
                        //分配专家
//                        QueryWrapper<ZgsSaexpert> zgsSaexpertQueryWrapper = new QueryWrapper();
//                        zgsSaexpertQueryWrapper.eq("businessguid", demoprojectacceptance.getId());
//                        zgsSaexpertQueryWrapper.ne("isdelete", 1);
//                        List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(zgsSaexpertQueryWrapper);
//                        if (!(zgsSaexpertList != null && zgsSaexpertList.size() > 0)) {
//                            //专家分配
//                            Calendar calendar = new GregorianCalendar();
//                            calendar.setTime(new Date());
//                            calendar.add(calendar.DATE, -15);//15天前未分配的还可以继续分配，超了15天分配按钮消失
//                            if (calendar.getTime().compareTo(demoprojectacceptance.getApplydate()) < 0) {
//                                pageList.getRecords().get(m).setSpStatus(3);
//                            }
//                        }
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    if (StringUtils.isNotEmpty(demoprojectacceptance.getProjectlibraryguid()) && StringUtils.isNotEmpty(demoprojectacceptance.getProjectname())) {
                        QueryWrapper<ZgsProjecttask> queryWrapper0 = new QueryWrapper<ZgsProjecttask>();
                        queryWrapper0.eq("projectguid", demoprojectacceptance.getProjectlibraryguid());
                        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper0.ne("isdelete", 1);
                        ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper0);
                        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                        } else {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                        }
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }

                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(demoprojectacceptance.getUpdateSave()) && demoprojectacceptance.getUpdateSave().equals("1")) {  // 修改未提交
                        demoprojectacceptance.setBjzt("1");
                    } else if (StrUtil.isNotBlank(demoprojectacceptance.getUpdateSave()) && demoprojectacceptance.getUpdateSave().equals("2")) {  // 修改已提交
                        demoprojectacceptance.setBjzt("2");
                    }

                    if (StrUtil.isNotBlank(demoprojectacceptance.getSqbj()) && demoprojectacceptance.getSqbj().equals("1")) {
                        demoprojectacceptance.setSqbjBol(false);
                        demoprojectacceptance.setUpdateFlag("1");
                    } else if (StrUtil.isNotBlank(demoprojectacceptance.getSqbj()) && demoprojectacceptance.getSqbj().equals("2")) {
                        demoprojectacceptance.setSqbjBol(true);
                        demoprojectacceptance.setUpdateFlag("2");
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsDemoprojectacceptance
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-添加")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-添加", notes = "建设科技示范项目验收申请业务主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsDemoprojectacceptance zgsDemoprojectacceptance) {
    //        1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsDemoprojectacceptance.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsDemoprojectacceptance.getStatus())) {
            //保存并上报
            zgsDemoprojectacceptance.setApplydate(new Date());
        } else {
            zgsDemoprojectacceptance.setApplydate(null);
        }
        zgsDemoprojectacceptance.setIshistory("0");
        zgsDemoprojectacceptance.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsDemoprojectacceptance.setCreateaccountname(sysUser.getUsername());
        zgsDemoprojectacceptance.setCreateusername(sysUser.getRealname());
        zgsDemoprojectacceptance.setCreatedate(new Date());
        //判断是否已添加中期查验申报项目
        QueryWrapper<ZgsMidterminspection> queryWrapper = new QueryWrapper();
        queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectname())) {
                qwp1.like("projectname", zgsDemoprojectacceptance.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectlibraryguid())) {
                qwp1.or().eq("projectlibraryguid", zgsDemoprojectacceptance.getProjectlibraryguid());
            }
        });
//        List<ZgsMidterminspection> list = zgsMidterminspectionService.list(queryWrapper);
//        if (list.size() == 0) {
//            return Result.error("请中期查验流程结束后，方可进行此阶段申请！");
//        }
        //判断项目终止
//        QueryWrapper<ZgsProjecttask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("projectguid", zgsDemoprojectacceptance.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsProjecttask> listHist = zgsProjecttaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsDemoprojectacceptance.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
        zgsDemoprojectacceptance.setProjectstagestatus(zgsDemoprojectacceptance.getStatus());
        zgsDemoprojectacceptanceService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsDemoprojectacceptance, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsDemoprojectacceptance.getStatus()) && StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectlibraryguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsDemoprojectacceptance.getProjectlibraryguid());
        }
//        redisUtil.set(zgsDemoprojectacceptance.getProjectlibraryguid(), "验收管理阶段");
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsDemoprojectacceptance.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.TechAcceptance);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }
//      建设科技示范项目验收应用情况信息表
        List<ZgsDemoprojectapplication> zgsDemoprojectapplicationList = zgsDemoprojectacceptance.getZgsDemoprojectapplicationList();
        if (zgsDemoprojectapplicationList != null && zgsDemoprojectapplicationList.size() > 0) {
            for (int a0 = 0; a0 < zgsDemoprojectapplicationList.size(); a0++) {
                ZgsDemoprojectapplication zgsDemoprojectapplication = zgsDemoprojectapplicationList.get(a0);
                zgsDemoprojectapplication.setId(UUID.randomUUID().toString());
                zgsDemoprojectapplication.setBaseguid(id);
                zgsDemoprojectapplication.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsDemoprojectapplication.setCreateaccountname(sysUser.getUsername());
                zgsDemoprojectapplication.setCreateusername(sysUser.getRealname());
                zgsDemoprojectapplication.setCreatedate(new Date());
                zgsDemoprojectapplicationService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsDemoprojectapplication, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 撤回
     *
     * @param zgsDemoprojectacceptance
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-撤回")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-撤回", notes = "建设科技示范项目验收申请业务主表-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsDemoprojectacceptance zgsDemoprojectacceptance) {
        if (zgsDemoprojectacceptance != null) {
            String id = zgsDemoprojectacceptance.getId();
            String bid = zgsDemoprojectacceptance.getProjectlibraryguid();
            String status = zgsDemoprojectacceptance.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41).equals(zgsDemoprojectacceptance.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsDemoprojectacceptance info = new ZgsDemoprojectacceptance();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsDemoprojectacceptanceService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsDemoprojectacceptance
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-审批")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-审批", notes = "建设科技示范项目验收申请业务主表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsDemoprojectacceptance zgsDemoprojectacceptance) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsDemoprojectacceptance != null && StringUtils.isNotEmpty(zgsDemoprojectacceptance.getId())) {
            ZgsDemoprojectacceptance demoprojectacceptance = new ZgsDemoprojectacceptance();
            demoprojectacceptance.setId(zgsDemoprojectacceptance.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsDemoprojectacceptance.getId());
            zgsReturnrecord.setReturnreason(zgsDemoprojectacceptance.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE11);
            ZgsDemoprojectacceptance acceptanceInfo = zgsDemoprojectacceptanceService.getById(zgsDemoprojectacceptance.getId());
            if (acceptanceInfo != null) {
                zgsReturnrecord.setEnterpriseguid(acceptanceInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(acceptanceInfo.getApplydate());
                zgsReturnrecord.setProjectname(acceptanceInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", acceptanceInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsDemoprojectacceptance.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                demoprojectacceptance.setAuditopinion(null);
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsDemoprojectacceptance.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsDemoprojectacceptance.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsDemoprojectacceptance.getStatus())) {
                    demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS4);
                    demoprojectacceptance.setFirstdate(new Date());
                    demoprojectacceptance.setFirstperson(sysUser.getRealname());
                    demoprojectacceptance.setFirstdepartment(sysUser.getEnterprisename());
                    demoprojectacceptance.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS8);
                    demoprojectacceptance.setFinishperson(sysUser.getRealname());
                    demoprojectacceptance.setFinishdate(new Date());
                    demoprojectacceptance.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审结束查下是否有旧的专家评审历史记录
//                    QueryWrapper<ZgsSaexpert> saexpertQueryWrapper = new QueryWrapper();
//                    saexpertQueryWrapper.eq("businessguid", zgsDemoprojectacceptance.getId());
//                    saexpertQueryWrapper.ne("isdelete", 1);
//                    List<ZgsSaexpert> zgsSaexpertList = zgsSaexpertService.list(saexpertQueryWrapper);
//                    if (zgsSaexpertList != null && zgsSaexpertList.size() > 0) {
//                        for (ZgsSaexpert zgsSaexpert : zgsSaexpertList) {
//                            ZgsSaexpert zgsSaexpert1 = new ZgsSaexpert();
//                            zgsSaexpert1.setId(zgsSaexpert.getId());
//                            zgsSaexpert1.setIsdelete(new BigDecimal(1));
//                            zgsSaexpertService.updateById(zgsSaexpert1);
//                        }
//                    }
                    //
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                demoprojectacceptance.setAuditopinion(zgsDemoprojectacceptance.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsDemoprojectacceptance.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsDemoprojectacceptance.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsDemoprojectacceptance.getStatus())) {
                    if (zgsDemoprojectacceptance.getSpStatus() == 2) {
                        demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    demoprojectacceptance.setFirstdate(new Date());
                    demoprojectacceptance.setFirstperson(sysUser.getRealname());
                    demoprojectacceptance.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsDemoprojectacceptance.getSpStatus() == 2) {
                        demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    demoprojectacceptance.setFinishperson(sysUser.getRealname());
                    demoprojectacceptance.setFinishdate(new Date());
                    demoprojectacceptance.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsDemoprojectacceptance.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        demoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(acceptanceInfo.getProjectlibraryguid(), acceptanceInfo.getEnterpriseguid(), zgsDemoprojectacceptance.getAuditopinion());

                    }
                }
                if (zgsDemoprojectacceptance.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsDemoprojectacceptance.getAuditopinion())) {
                        zgsDemoprojectacceptance.setAuditopinion(" ");
                    }
                    demoprojectacceptance.setAuditopinion(zgsDemoprojectacceptance.getAuditopinion());
                    demoprojectacceptance.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsDemoprojectacceptance.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    demoprojectacceptance.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(acceptanceInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            zgsDemoprojectacceptanceService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(demoprojectacceptance, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            zgsProjectlibraryService.initStageAndStatus(acceptanceInfo.getProjectlibraryguid());
        }
        return Result.OK("审批成功！");
    }

    /**
     * 编辑
     *
     * @param zgsDemoprojectacceptance
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-编辑")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-编辑", notes = "建设科技示范项目验收申请业务主表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsDemoprojectacceptance zgsDemoprojectacceptance) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsDemoprojectacceptance != null && StringUtils.isNotEmpty(zgsDemoprojectacceptance.getId())) {

            ZgsDemoprojectacceptance entity = zgsDemoprojectacceptanceService.getById(zgsDemoprojectacceptance.getId());
            if (ObjectUtil.isNotNull(entity)) {
                if (StrUtil.isNotBlank(entity.getSqbj()) && entity.getSqbj().equals("2")) {
                    Result result = new Result();

                    result.setMessage("授权编辑已被取消，已修改信息不能保存");
                    result.setCode(0);
                    result.setSuccess(false);
                    return result;

                } else {
                    String id = zgsDemoprojectacceptance.getId();
                    if (GlobalConstants.handleSubmit.equals(zgsDemoprojectacceptance.getStatus())) {
                        //保存并上报
                        zgsDemoprojectacceptance.setApplydate(new Date());
                        zgsDemoprojectacceptance.setAuditopinion(null);
//                zgsDemoprojectacceptance.setReturntype(null);
                    } else {
                        zgsDemoprojectacceptance.setApplydate(null);
                        //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                        ZgsDemoprojectacceptance projectTask = zgsDemoprojectacceptanceService.getById(id);
                        if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                            zgsDemoprojectacceptance.setStatus(GlobalConstants.SHENHE_STATUS3);
                        }
                    }
                    zgsDemoprojectacceptance.setModifyaccountname(sysUser.getUsername());
                    zgsDemoprojectacceptance.setModifyusername(sysUser.getRealname());
                    zgsDemoprojectacceptance.setModifydate(new Date());
                    zgsDemoprojectacceptance.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_41));
                    zgsDemoprojectacceptance.setProjectstagestatus(zgsDemoprojectacceptance.getStatus());

                    // 将授权编辑变为 取消授权编辑（说明：终审单位的此次授权编辑已使用完，想要修改，必须重新授权）
                    if (StringUtils.isNotBlank(zgsDemoprojectacceptance.getSqbj()) && StringUtils.isNotBlank(zgsDemoprojectacceptance.getUpdateSave())) {
                        zgsDemoprojectacceptance.setSqbj("2");  // 取消编辑
                        zgsDemoprojectacceptance.setUpdateSave("2");  // 修改已提交
                    }

                    zgsDemoprojectacceptanceService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsDemoprojectacceptance, ValidateEncryptEntityUtil.isEncrypt));
                    //实时更新阶段和状态
                    if (GlobalConstants.handleSubmit.equals(zgsDemoprojectacceptance.getStatus()) && StringUtils.isNotEmpty(zgsDemoprojectacceptance.getProjectlibraryguid())) {
                        zgsProjectlibraryService.initStageAndStatus(zgsDemoprojectacceptance.getProjectlibraryguid());
                    }
                    String enterpriseguid = zgsDemoprojectacceptance.getEnterpriseguid();
                    //示范项目验收应用情况信息
                    List<ZgsDemoprojectapplication> zgsDemoprojectapplicationList0 = zgsDemoprojectacceptance.getZgsDemoprojectapplicationList();
                    QueryWrapper<ZgsDemoprojectapplication> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("enterpriseguid", enterpriseguid);
                    queryWrapper1.eq("baseguid", id);
                    queryWrapper1.ne("isdelete", 1);
                    queryWrapper1.orderByAsc("ordernum");
                    List<ZgsDemoprojectapplication> zgsDemoprojectapplicationList0_1 = zgsDemoprojectapplicationService.list(queryWrapper1);
                    if (zgsDemoprojectapplicationList0 != null && zgsDemoprojectapplicationList0.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsDemoprojectapplicationList0.size(); a0++) {
                            ZgsDemoprojectapplication zgsDemoprojectapplication = zgsDemoprojectapplicationList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsDemoprojectapplication.getId())) {
                                //编辑
                                zgsDemoprojectapplication.setModifyaccountname(sysUser.getUsername());
                                zgsDemoprojectapplication.setModifyusername(sysUser.getRealname());
                                zgsDemoprojectapplication.setModifydate(new Date());
                                zgsDemoprojectapplicationService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsDemoprojectapplication, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsDemoprojectapplication.setId(UUID.randomUUID().toString());
                                zgsDemoprojectapplication.setBaseguid(id);
                                zgsDemoprojectapplication.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsDemoprojectapplication.setCreateaccountname(sysUser.getUsername());
                                zgsDemoprojectapplication.setCreateusername(sysUser.getRealname());
                                zgsDemoprojectapplication.setCreatedate(new Date());
                                zgsDemoprojectapplicationService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsDemoprojectapplication, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsDemoprojectapplication zgsDemoprojectapplication1 : zgsDemoprojectapplicationList0_1) {
                                if (!zgsDemoprojectapplication1.getId().equals(zgsDemoprojectapplication.getId())) {
                                    map0.put(zgsDemoprojectapplication1.getId(), zgsDemoprojectapplication1.getId());
                                } else {
                                    list0.add(zgsDemoprojectapplication1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsDemoprojectapplicationService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsDemoprojectapplicationList0_1 != null && zgsDemoprojectapplicationList0_1.size() > 0) {
                            for (ZgsDemoprojectapplication zgsDemoprojectapplication : zgsDemoprojectapplicationList0_1) {
                                zgsDemoprojectapplicationService.removeById(zgsDemoprojectapplication.getId());
                            }
                        }
                    }

                    //相关附件
                    List<ZgsMattermaterial> zgsMattermaterialList = zgsDemoprojectacceptance.getZgsMattermaterialList();
                    if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                        for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                            ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                            String mattermaterialId = zgsMattermaterial.getId();
                            UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                            wrapper.eq("relationid", mattermaterialId);
                            //先删除原来的再重新添加
                            zgsAttachappendixService.remove(wrapper);
                            if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                                for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                                    ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                                    if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                        zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                        zgsAttachappendix.setModifytime(new Date());
                                    } else {
                                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                                        zgsAttachappendix.setRelationid(mattermaterialId);
                                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                        zgsAttachappendix.setCreatetime(new Date());
                                        zgsAttachappendix.setAppendixtype(GlobalConstants.TechAcceptance);
                                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                                    }
                                    zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-通过id删除")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-通过id删除", notes = "建设科技示范项目验收申请业务主表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsDemoprojectacceptanceService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-批量删除")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-批量删除", notes = "建设科技示范项目验收申请业务主表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsDemoprojectacceptanceService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-通过id查询")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-通过id查询", notes = "建设科技示范项目验收申请业务主表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsDemoprojectacceptance zgsDemoprojectacceptance = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsDemoprojectacceptance = ValidateEncryptEntityUtil.validateDecryptObject(zgsDemoprojectacceptanceService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsDemoprojectacceptance == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsDemoprojectacceptance);
            }
            if ((zgsDemoprojectacceptance.getFirstdate() != null || zgsDemoprojectacceptance.getFinishdate() != null) && zgsDemoprojectacceptance.getApplydate() != null) {
                zgsDemoprojectacceptance.setSpLogStatus(1);
            } else {
                zgsDemoprojectacceptance.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsDemoprojectacceptance = new ZgsDemoprojectacceptance();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.TechAcceptance);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsDemoprojectacceptance.setZgsMattermaterialList(zgsMattermaterialList);
            zgsDemoprojectacceptance.setSpLogStatus(0);
        }
        return Result.OK(zgsDemoprojectacceptance);
    }

    private void initProjectSelectById(ZgsDemoprojectacceptance zgsDemoprojectacceptance) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = zgsDemoprojectacceptance.getEnterpriseguid();
        String buildguid = zgsDemoprojectacceptance.getId();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //屏蔽法人设置，先判断是否为专家用户类型，再查询
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                    String setValue = zgsExpertset.getSetvalue();
                    if (setValue.contains(",")) {
                        String strValue[] = setValue.split(",");
                        for (String set : strValue) {
                            if (GlobalConstants.expert_set_close1.equals(set)) {
                                zgsDemoprojectacceptance.setImplementunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                zgsDemoprojectacceptance.setLinkman(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                zgsDemoprojectacceptance.setLinkphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(set)) {
                            }
                        }
                    } else {
                        if (GlobalConstants.expert_set_close1.equals(setValue)) {
                            zgsDemoprojectacceptance.setImplementunit(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                            zgsDemoprojectacceptance.setLinkman(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                            zgsDemoprojectacceptance.setLinkphone(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                        }
                    }
                }
            }
            //
            //专家评审
            zgsDemoprojectacceptance.setZgsSofttechexpertList(zgsSaexpertService.getZgsSaexpertList(buildguid));
            //示范项目验收应用情况信息
            QueryWrapper<ZgsDemoprojectapplication> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsDemoprojectapplication> zgsDemoprojectapplicationList = zgsDemoprojectapplicationService.list(queryWrapper1);
            zgsDemoprojectacceptance.setZgsDemoprojectapplicationList(zgsDemoprojectapplicationList);

            //相关附件
            map.put("buildguid", buildguid);
            //后期查下TechAcceptance出现在哪张表里
            map.put("projecttypenum", GlobalConstants.TechAcceptance);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsDemoprojectacceptance.setZgsMattermaterialList(zgsMattermaterialList);
            }
        }
    }

    /**
     * 导出excel
     *
     * @param zgsDemoprojectacceptance
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsDemoprojectacceptance zgsDemoprojectacceptance,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsDemoprojectacceptance, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsDemoprojectacceptance> pageList = (IPage<ZgsDemoprojectacceptance>) result.getResult();
        List<ZgsDemoprojectacceptance> list = pageList.getRecords();
        List<ZgsDemoprojectacceptance> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "建设科技示范项目验收申请";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsDemoprojectacceptance.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsDemoprojectacceptance, ZgsDemoprojectacceptance.class, "建设科技示范项目验收申请业务主表");
        return mv;
    }

    private String getId(ZgsDemoprojectacceptance item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsDemoprojectacceptance.class);
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技示范项目验收申请业务主表-通过id查询导出项目验收申请表")
    @ApiOperation(value = "建设科技示范项目验收申请业务主表-通过id查询导出项目验收申请表", notes = "建设科技示范项目验收申请业务主表-通过id查询导出项目验收申请表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsDemoprojectacceptance zgsDemoprojectacceptance = ValidateEncryptEntityUtil.validateDecryptObject(zgsDemoprojectacceptanceService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsDemoprojectacceptance != null) {
            initProjectSelectById(zgsDemoprojectacceptance);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "11甘肃省建设科技示范项目验收申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsDemoprojectacceptance);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }



    /**
     * @describe: 省厅角色下授权编辑按钮状态修改
     * @author: renxiaoliang
     * @date: 2023/11/15 16:08
     */
    @AutoLog(value = "授权编辑—状态修改")
    @ApiOperation(value = "授权编辑—状态修改", notes = "授权编辑—状态修改")
    @PostMapping(value = "/accreditUpdate")
    public Result<?> accreditUpdate(@RequestBody Map<String,Object> params) {
        Result result = new Result();

        String id = params.get("id").toString();
        String sqbj = params.get("sqbj").toString();  // 授权编辑  1 同意编辑  2 取消编辑
        String updateSave = params.get("updateSave").toString();  // 编辑保存  1 修改未提交  2 修改已提交

        if (StrUtil.isNotBlank(id) && StrUtil.isNotBlank(sqbj) && StrUtil.isNotBlank(updateSave)) {
            ZgsDemoprojectacceptance zgsDemoprojectacceptance = new ZgsDemoprojectacceptance();
            zgsDemoprojectacceptance.setId(id);
            zgsDemoprojectacceptance.setSqbj(sqbj);
            zgsDemoprojectacceptance.setUpdateSave(updateSave);
            if (zgsDemoprojectacceptanceService.updateById(zgsDemoprojectacceptance)){
                result.setMessage("授权编辑成功");
                result.setCode(1);
                result.setSuccess(true);
            } else {
                result.setMessage("授权编辑失败");
                result.setCode(0);
                result.setSuccess(false);
            }
        } else {
            result.setMessage("授权编辑失败");
            result.setCode(0);
            result.setSuccess(false);
        }
        return result;
    }


}
