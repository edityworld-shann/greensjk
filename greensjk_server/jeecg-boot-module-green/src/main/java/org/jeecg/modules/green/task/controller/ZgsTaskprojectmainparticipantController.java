package org.jeecg.modules.green.task.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 任务书项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
@Api(tags="任务书项目主要参加人员")
@RestController
@RequestMapping("/task/zgsTaskprojectmainparticipant")
@Slf4j
public class ZgsTaskprojectmainparticipantController extends JeecgController<ZgsTaskprojectmainparticipant, IZgsTaskprojectmainparticipantService> {
	@Autowired
	private IZgsTaskprojectmainparticipantService zgsTaskprojectmainparticipantService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsTaskprojectmainparticipant
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-分页列表查询")
	@ApiOperation(value="任务书项目主要参加人员-分页列表查询", notes="任务书项目主要参加人员-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsTaskprojectmainparticipant> queryWrapper = QueryGenerator.initQueryWrapper(zgsTaskprojectmainparticipant, req.getParameterMap());
		Page<ZgsTaskprojectmainparticipant> page = new Page<ZgsTaskprojectmainparticipant>(pageNo, pageSize);
		IPage<ZgsTaskprojectmainparticipant> pageList = zgsTaskprojectmainparticipantService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsTaskprojectmainparticipant
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-添加")
	@ApiOperation(value="任务书项目主要参加人员-添加", notes="任务书项目主要参加人员-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant) {
		zgsTaskprojectmainparticipantService.save(zgsTaskprojectmainparticipant);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsTaskprojectmainparticipant
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-编辑")
	@ApiOperation(value="任务书项目主要参加人员-编辑", notes="任务书项目主要参加人员-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant) {
		zgsTaskprojectmainparticipantService.updateById(zgsTaskprojectmainparticipant);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-通过id删除")
	@ApiOperation(value="任务书项目主要参加人员-通过id删除", notes="任务书项目主要参加人员-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsTaskprojectmainparticipantService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-批量删除")
	@ApiOperation(value="任务书项目主要参加人员-批量删除", notes="任务书项目主要参加人员-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsTaskprojectmainparticipantService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "任务书项目主要参加人员-通过id查询")
	@ApiOperation(value="任务书项目主要参加人员-通过id查询", notes="任务书项目主要参加人员-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = zgsTaskprojectmainparticipantService.getById(id);
		if(zgsTaskprojectmainparticipant==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsTaskprojectmainparticipant);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsTaskprojectmainparticipant
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant) {
        return super.exportXls(request, zgsTaskprojectmainparticipant, ZgsTaskprojectmainparticipant.class, "任务书项目主要参加人员");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsTaskprojectmainparticipant.class);
    }

}
