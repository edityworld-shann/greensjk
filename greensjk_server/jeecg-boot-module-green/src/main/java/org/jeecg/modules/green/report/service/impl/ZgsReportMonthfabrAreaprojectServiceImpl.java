package org.jeecg.modules.green.report.service.impl;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreaprojectMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaprojectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreaprojectServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaprojectMapper, ZgsReportMonthfabrAreaproject> implements IZgsReportMonthfabrAreaprojectService {

  @Autowired
  private ZgsReportMonthfabrAreaprojectMapper zgsReportMonthfabrAreaprojectMapper;


  @Override
  public ZgsReportMonthfabrAreaproject queryProjectByReportTime(@Param("reporttm") String reporttm){
    return zgsReportMonthfabrAreaprojectMapper.queryProjectByReportTime(reporttm);
  }
}
