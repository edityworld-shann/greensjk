package org.jeecg.modules.green.zjksb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertworkresume;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 专家库人员工作简历
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface ZgsExpertworkresumeMapper extends BaseMapper<ZgsExpertworkresume> {

}
