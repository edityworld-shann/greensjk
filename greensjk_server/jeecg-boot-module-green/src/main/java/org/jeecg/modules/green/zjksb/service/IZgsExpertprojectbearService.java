package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertprojectbear;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库项目承担情况
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertprojectbearService extends IService<ZgsExpertprojectbear> {

}
