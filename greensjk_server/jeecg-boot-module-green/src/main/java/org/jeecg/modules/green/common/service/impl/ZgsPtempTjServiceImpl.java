package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsPtempTj;
import org.jeecg.modules.green.common.mapper.ZgsPtempTjMapper;
import org.jeecg.modules.green.common.service.IZgsPtempTjService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 统计报表区县市州用户添加账号匹配表
 * @Author: jeecg-boot
 * @Date:   2022-06-01
 * @Version: V1.0
 */
@Service
public class ZgsPtempTjServiceImpl extends ServiceImpl<ZgsPtempTjMapper, ZgsPtempTj> implements IZgsPtempTjService {

}
