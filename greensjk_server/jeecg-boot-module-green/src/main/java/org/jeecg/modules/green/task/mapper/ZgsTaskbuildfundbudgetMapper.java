package org.jeecg.modules.green.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 任务书项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface ZgsTaskbuildfundbudgetMapper extends BaseMapper<ZgsTaskbuildfundbudget> {

}
