package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;
import org.jeecg.modules.green.report.mapper.ZgsReportBuildingenergyinfocityMapper;
import org.jeecg.modules.green.report.mapper.ZgsReportHomePageMapper;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfocityService;
import org.jeecg.modules.green.report.service.IZgsReportHomePageService;
import org.springframework.stereotype.Service;

/**
 * @Description: zgs_report_buildingenergyinfocity
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportHomePageServiceImpl extends ServiceImpl<ZgsReportHomePageMapper, ZgsReportBuildingenergyinfocity> implements IZgsReportHomePageService {

}
