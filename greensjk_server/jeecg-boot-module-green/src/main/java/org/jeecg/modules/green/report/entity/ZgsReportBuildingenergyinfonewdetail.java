package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_buildingenergyinfonewdetail
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_buildingenergyinfonewdetail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_buildingenergyinfonewdetail对象", description = "zgs_report_buildingenergyinfonewdetail")
public class ZgsReportBuildingenergyinfonewdetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * buildingenergyinfonewguid
     */
    @Excel(name = "buildingenergyinfonewguid", width = 15)
    @ApiModelProperty(value = "buildingenergyinfonewguid")
    private java.lang.String buildingenergyinfonewguid;
    /**
     * projectname
     */
    @Excel(name = "projectname", width = 15)
    @ApiModelProperty(value = "projectname")
    private java.lang.String projectname;
    /**
     * 居住建筑、公共建筑
     */
    @Excel(name = "居住建筑、公共建筑", width = 15)
    @ApiModelProperty(value = "居住建筑、公共建筑")
    private java.lang.String jngztype;
    /**
     * 建筑面积
     */
    @Excel(name = "建筑面积", width = 15)
    @ApiModelProperty(value = "建筑面积")
    private java.math.BigDecimal jngzjzmj;
}
