package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 黑名单历史记录
 * @Author: jeecg-boot
 * @Date: 2022-03-06
 * @Version: V1.0
 */
public interface IZgsProjectunithistoryService extends IService<ZgsProjectunithistory> {
    public int updateUserStatus(String userId);
}
