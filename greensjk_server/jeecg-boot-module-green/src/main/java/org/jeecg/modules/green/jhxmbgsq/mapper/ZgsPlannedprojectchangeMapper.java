package org.jeecg.modules.green.jhxmbgsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 建设科技计划项目变更申请业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsPlannedprojectchangeMapper extends BaseMapper<ZgsPlannedprojectchange> {

}
