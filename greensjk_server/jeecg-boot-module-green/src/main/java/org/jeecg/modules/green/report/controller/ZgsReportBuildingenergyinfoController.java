package org.jeecg.modules.green.report.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfo;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Arrays;

 /**
 * @Description: zgs_report_buildingenergyinfo
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_buildingenergyinfo")
@RestController
@RequestMapping("/report/zgsReportBuildingenergyinfo")
@Slf4j
public class ZgsReportBuildingenergyinfoController extends JeecgController<ZgsReportBuildingenergyinfo, IZgsReportBuildingenergyinfoService> {
	@Autowired
	private IZgsReportBuildingenergyinfoService zgsReportBuildingenergyinfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportBuildingenergyinfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-分页列表查询")
	@ApiOperation(value="zgs_report_buildingenergyinfo-分页列表查询", notes="zgs_report_buildingenergyinfo-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportBuildingenergyinfo zgsReportBuildingenergyinfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportBuildingenergyinfo> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportBuildingenergyinfo, req.getParameterMap());
		Page<ZgsReportBuildingenergyinfo> page = new Page<ZgsReportBuildingenergyinfo>(pageNo, pageSize);
		IPage<ZgsReportBuildingenergyinfo> pageList = zgsReportBuildingenergyinfoService.page(page, queryWrapper);
		return Result.OK(pageList);

	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportBuildingenergyinfo
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-添加")
	@ApiOperation(value="zgs_report_buildingenergyinfo-添加", notes="zgs_report_buildingenergyinfo-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportBuildingenergyinfo zgsReportBuildingenergyinfo) {
		zgsReportBuildingenergyinfoService.save(zgsReportBuildingenergyinfo);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportBuildingenergyinfo
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-编辑")
	@ApiOperation(value="zgs_report_buildingenergyinfo-编辑", notes="zgs_report_buildingenergyinfo-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportBuildingenergyinfo zgsReportBuildingenergyinfo) {
		zgsReportBuildingenergyinfoService.updateById(zgsReportBuildingenergyinfo);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-通过id删除")
	@ApiOperation(value="zgs_report_buildingenergyinfo-通过id删除", notes="zgs_report_buildingenergyinfo-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportBuildingenergyinfoService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-批量删除")
	@ApiOperation(value="zgs_report_buildingenergyinfo-批量删除", notes="zgs_report_buildingenergyinfo-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportBuildingenergyinfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfo-通过id查询")
	@ApiOperation(value="zgs_report_buildingenergyinfo-通过id查询", notes="zgs_report_buildingenergyinfo-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportBuildingenergyinfo zgsReportBuildingenergyinfo = zgsReportBuildingenergyinfoService.getById(id);
		if(zgsReportBuildingenergyinfo==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportBuildingenergyinfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportBuildingenergyinfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportBuildingenergyinfo zgsReportBuildingenergyinfo) {
        return super.exportXls(request, zgsReportBuildingenergyinfo, ZgsReportBuildingenergyinfo.class, "zgs_report_buildingenergyinfo");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportBuildingenergyinfo.class);
    }

}
