package org.jeecg.modules.green.report.service;

import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProductioncapacity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 装配式建筑-生产产能-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaCityProductioncapacityService extends IService<ZgsReportMonthfabrAreaCityProductioncapacity> {
    void initMonthfabrAreaTotalALL(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser);

//    void initMonthfabrAreaTotal(String year, int quarter, String yearMonth, String projecttype);

    void initMonthfabrAreaTotalZero(String year, int quarter, String yearMonth, String projecttype);

//    void initMonthfabrCityTotal(String year, int quarter, String yearMonth, String projecttype);

    void initMonthfabrCityTotalALL(int type, Integer applyState, String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser);

    void spProject(String id, Integer spStatus, String backreason);

    ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataProvince(String filltm, Integer applystate);

    ZgsReportMonthfabrAreaCityProductioncapacity lastTotalDataCity(String filltm, String areacode, Integer applystate);

    /**
     * 滚屏 - 新开工 装配式建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionFabricatAreaData(String filltm);

    /**
     * 滚屏 - 新开工 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewconstructionAreaData(String filltm);

    /**
     * 滚屏 -  新建 - 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalNewBuildingAreaData(String filltm);

    /**
     * 滚屏 - 绿色 - 建筑面积
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingAreaData(String filltm);

    /**
     * 滚屏 - 绿色 - 建筑等级
     *
     * @param filltm
     */
    List<ZgsReportMonthfabrAreaCityProductioncapacity> totalGreenBuildingLevelData(String filltm);
}
