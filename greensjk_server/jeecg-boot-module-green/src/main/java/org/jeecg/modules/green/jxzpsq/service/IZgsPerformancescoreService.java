package org.jeecg.modules.green.jxzpsq.service;

import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 计划项目执行情况绩效自评表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsPerformancescoreService extends IService<ZgsPerformancescore> {

}
