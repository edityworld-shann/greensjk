package org.jeecg.modules.green.kyxmsb.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.demo.poi.view.JeecgTemplateExcelView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencefundbudgetService;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceplanarrangeService;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertprojectfinish;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertprojectfinishService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;

import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 建设科技攻关项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-10queryById
 * @Version: V1.0
 */
@Api(tags = "建设科技攻关项目库")
@RestController
@RequestMapping("/common/zgsSciencetechfeasible")
@Slf4j
public class ZgsSciencetechfeasibleController extends JeecgController<ZgsSciencetechfeasible, IZgsSciencetechfeasibleService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsScienceparticipantService zgsScienceparticipantService;
    @Autowired
    private IZgsScienceplanarrangeService zgsScienceplanarrangeService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsSciencefundbudgetService zgsSciencefundbudgetService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsSofttechexpertService zgsSofttechexpertService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsCommonService zgsCommonService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;
    @Autowired
    private IZgsSacceptcertprojectfinishService zgsSacceptcertprojectfinishService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;

    /**
     * 分页列表查询
     *
     * @param zgsSciencetechfeasible
     * @param pageNo
     * @param pageSize
     * @param accessType             新增任务书及以下菜单关联查询标识
     * @param req
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-分页列表查询")
    @ApiOperation(value = "建设科技攻关项目库-分页列表查询", notes = "建设科技攻关项目库-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSciencetechfeasible zgsSciencetechfeasible,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "accessType", required = false) Integer accessType,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getSciencetype())) {
            queryWrapper.eq("sciencetype", zgsSciencetechfeasible.getSciencetype());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getProjectname())) {
            queryWrapper.like("projectname", zgsSciencetechfeasible.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getCommitmentunit())) {
            // queryWrapper.eq("commitmentunit", Sm4Util.encryptEcb(zgsSciencetechfeasible.getCommitmentunit()));
            queryWrapper.like("commitmentunit", zgsSciencetechfeasible.getCommitmentunit());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getProjectnum())) {
            queryWrapper.like("projectnum", zgsSciencetechfeasible.getProjectnum());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("year_num", year);
        }
        if (zgsSciencetechfeasible.getAgreeproject() != null) {
            queryWrapper.eq("agreeproject", zgsSciencetechfeasible.getAgreeproject());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getProjectstage())) {
            if (!"-1".equals(zgsSciencetechfeasible.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsSciencetechfeasible.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initSciencetechfeasibleSelect(queryWrapper, zgsSciencetechfeasible.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSciencetechfeasible.getStatus(), sysUser, 3);
        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    QueryWrapper<ZgsSofttechexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
                    zgsSofttechexpertQueryWrapper.eq("expertguid", expertId);
                    zgsSofttechexpertQueryWrapper.ne("isdelete", 1);
//                    zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
                    List<ZgsSofttechexpert> zgsSofttechexpertList = zgsSofttechexpertService.list(zgsSofttechexpertQueryWrapper);
                    if (zgsSofttechexpertList != null && zgsSofttechexpertList.size() > 0) {
                        queryWrapper.and(qw -> {
                            qw.and(w1 -> {
                                for (ZgsSofttechexpert zgsSofttechexpert : zgsSofttechexpertList) {
                                    w1.or(w2 -> {
                                        w2.eq("id", zgsSofttechexpert.getBusinessguid());
                                    });
                                }
                            });
                        });
                    } else {
                        queryWrapper.eq("id", "null");
                    }
                }
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsSciencetechfeasible.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3).ne("status", GlobalConstants.SHENHE_STATUS13);
                    });
                });
            }
        }
        //关联数据筛选使用accessType默认传1为立项项目
        if (accessType != null) {
            if (accessType == 0) {
                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS2);
            } else {
                queryWrapper.eq("agreeproject", GlobalConstants.SHENHE_STATUS2);
            }
        }
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                // queryWrapper.orderByAsc("applydate");
                queryWrapper.last("ORDER BY IF(isnull(applydate),0,1), applydate DESC");
            } else if (sbArrow == 1) {
                flag = false;
                // queryWrapper.orderByDesc("applydate");
                queryWrapper.last("ORDER BY IF(isnull(applydate),0,1), applydate DESC");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                // queryWrapper.orderByAsc("enddate");
                queryWrapper.last("ORDER BY IF(isnull(enddate),0,1), enddate DESC");
            } else if (wcArrow == 1) {
                flag = false;
                // queryWrapper.orderByDesc("enddate");
                queryWrapper.last("ORDER BY IF(isnull(enddate),0,1), enddate DESC");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                // queryWrapper.orderByAsc("firstdate");
                queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
            } else if (csArrow == 1) {
                flag = false;
                // queryWrapper.orderByDesc("firstdate");
                queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        }

        // 推荐单位查看项目统计，状态为空
        IPage<ZgsSciencetechfeasible> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsSciencetechfeasible> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsSciencetechfeasible> pageList = null;

        // 科技攻关
        redisUtil.expire("kjgg",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kjggOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kjggOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 软科学
        redisUtil.expire("rkx",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("rkxOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("rkxOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）


        Page<ZgsSciencetechfeasible> page = new Page<ZgsSciencetechfeasible>(pageNo, pageSize);
        // IPage<ZgsSciencetechfeasible> pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);
        // 1 表示从工作台进来，将项目数量存储redis 再更新原有工作台项目数量信息  rxl 20230727
        // 先判度redis是否已存储项目数量，如果未存储则进行redis存储，防止多次从工作台进入菜单走各个阶段的查询接口导致系统变慢
        if (zgsSciencetechfeasible.getSciencetype().equals("ScienceTech")) {
            // 科技攻关
            // 默认初始进来 查询推荐单位待初审、项目统计数量
            if (StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw())) {
                if (zgsSciencetechfeasible.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                    if (!redisUtil.hasKey("kjgg")) {
                        QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 3);
                        pageListForProjectCount = zgsSciencetechfeasibleService.page(page, queryWrapper);
                        redisUtil.set("kjgg",pageListForProjectCount.getTotal());
                    }
                }
                if (zgsSciencetechfeasible.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                    if (!redisUtil.hasKey("kjggOfTjdw")) {
                        QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 3);
                        pageListForDsp = zgsSciencetechfeasibleService.page(page, queryWrapper);
                        redisUtil.set("kjggOfTjdw",pageListForDsp.getTotal());
                    }
                }

            } else if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw()) && StringUtils.isBlank(zgsSciencetechfeasible.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);

            } else if (StringUtils.isBlank(zgsSciencetechfeasible.getDspForTjdw()) && StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);

            } else if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForSt()) && zgsSciencetechfeasible.getDspForSt().equals("2")) { // for 省厅待审批项目
                // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                // 省厅查看待审批项目信息
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);
                redisUtil.set("kjggOfDsp",pageList.getTotal());
                // }
            } else { // 正常菜单进入
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSciencetechfeasible.getStatus(), sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);
            }

            /*redisUtil.expire("kjgg",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("kjgg")) {  // for 项目统计
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable()) && zgsSciencetechfeasible.getFlagByWorkTable().equals("1")) {
                    redisUtil.set("kjgg",pageList.getTotal());
                }
            }
            redisUtil.expire("kjggOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("kjggOfDsp")) {  // for 省厅待审批项目
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForSt()) && zgsSciencetechfeasible.getDspForSt().equals("2")) {
                    redisUtil.set("kjggOfDsp",pageList.getTotal());
                }
            }

            redisUtil.expire("kjggOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("kjggOfTjdw")) {  // for 推荐单位待审批项目
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw()) && zgsSciencetechfeasible.getDspForTjdw().equals("3")) {
                    redisUtil.set("kjggOfTjdw",pageList.getTotal());
                }
            }*/
        } else if (zgsSciencetechfeasible.getSciencetype().equals("ScienceSoft")) {
            // 默认初始进来 查询推荐单位待初审、项目统计数量
            if (StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw())) {
                if (zgsSciencetechfeasible.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                    if (!redisUtil.hasKey("rkx")) {
                        QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 3);
                        pageListForProjectCount = zgsSciencetechfeasibleService.page(page, queryWrapper);
                        redisUtil.set("rkx",pageListForProjectCount.getTotal());
                    }
                }
                if (zgsSciencetechfeasible.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                    if (!redisUtil.hasKey("rkxOfTjdw")) {
                        QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 3);
                        pageListForDsp = zgsSciencetechfeasibleService.page(page, queryWrapper);
                        redisUtil.set("rkxOfTjdw",pageListForDsp.getTotal());
                    }
                }

            } else if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw()) && StringUtils.isBlank(zgsSciencetechfeasible.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);

            } else if (StringUtils.isBlank(zgsSciencetechfeasible.getDspForTjdw()) && StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);

            } else if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForSt()) && zgsSciencetechfeasible.getDspForSt().equals("2")) { // for 省厅待审批项目
                // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                // 省厅查看待审批项目信息
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);
                redisUtil.set("rkxOfDsp",pageList.getTotal());
                // }
            } else { // 正常菜单进入
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSciencetechfeasible.getStatus(), sysUser, 3);
                pageList = zgsSciencetechfeasibleService.page(page, queryWrapper);
            }

            /*// 软科学
            redisUtil.expire("rkx",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("rkx")) {  // for 项目统计
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getFlagByWorkTable()) && zgsSciencetechfeasible.getFlagByWorkTable().equals("1")) {
                    redisUtil.set("rkx",pageList.getTotal());
                }
            }
            redisUtil.expire("rkxOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("rkxOfDsp")) {  // for 省厅待审批项目
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForSt()) && zgsSciencetechfeasible.getDspForSt().equals("2")) {
                    redisUtil.set("rkxOfDsp",pageList.getTotal());
                }
            }
            redisUtil.expire("rkxOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
            if (!redisUtil.hasKey("rkxOfTjdw")) {  // for 推荐单位待审批项目
                if (StringUtils.isNotBlank(zgsSciencetechfeasible.getDspForTjdw()) && zgsSciencetechfeasible.getDspForTjdw().equals("3")) {
                    redisUtil.set("rkxOfTjdw",pageList.getTotal());
                }
            }*/
        }
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSciencetechfeasible sciencetechfeasible = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(sciencetechfeasible, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    }
                    if (sysUser.getEnterpriseguid().equals(sciencetechfeasible.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        if (GlobalConstants.SHENHE_STATUS1.equals(sciencetechfeasible.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(sciencetechfeasible.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(sciencetechfeasible.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(sciencetechfeasible.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(sciencetechfeasible.getStatus())) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //专家审批
                        QueryWrapper<ZgsSofttechexpert> wrapper = new QueryWrapper();
                        wrapper.eq("expertguid", expertId);
                        wrapper.eq("businessguid", sciencetechfeasible.getId());
                        ZgsSofttechexpert softtechexpert = zgsSofttechexpertService.getOne(wrapper);
                        if (!(softtechexpert != null && StringUtils.isNotEmpty(softtechexpert.getAuditconclusionnum()))) {
                            pageList.getRecords().get(m).setSpStatus(2);
                        }
                    }

                    // 如果当前登录角色为个人或单位时 根据授权标识判断是否可进行编辑操作
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                        if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("1")) {
                            sciencetechfeasible.setUpdateFlag("1");   // 可编辑
                        } else if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("2")) {
                            sciencetechfeasible.setUpdateFlag("2");  // 取消编辑按钮
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(sciencetechfeasible.getUpdateSave()) && sciencetechfeasible.getUpdateSave().equals("1")) {
                        sciencetechfeasible.setBjzt("1");
                    } else if (StrUtil.isNotBlank(sciencetechfeasible.getUpdateSave()) && sciencetechfeasible.getUpdateSave().equals("2")) {
                        sciencetechfeasible.setBjzt("2");
                    }
                    if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("1")) {
                        sciencetechfeasible.setSqbjBol(false);
                    } else if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("2")) {
                        sciencetechfeasible.setSqbjBol(true);
                    }
                }
            }
        } else {
            if (pageList.getRecords() != null && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSciencetechfeasible sciencetechfeasible = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(sciencetechfeasible, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(sciencetechfeasible.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    } else if (GlobalConstants.SHENHE_STATUS8.equals(sciencetechfeasible.getStatus())) {
                        //分配专家
                        pageList.getRecords().get(m).setSpStatus(3);
                        if (sciencetechfeasible.getAgreeproject() != null) {
                            if (sciencetechfeasible.getProvincialfundtotal() == null) {
                                if (new BigDecimal(2).compareTo(sciencetechfeasible.getAgreeproject()) == 0 && StringUtils.isNotEmpty(sciencetechfeasible.getYearNum())) {
                                    if (Integer.parseInt(sciencetechfeasible.getYearNum()) > 2022) {
                                        pageList.getRecords().get(m).setMoneyStatus(1);
                                    }
                                }
                            }
                        }
                    } else if (GlobalConstants.SHENHE_STATUS12.equals(sciencetechfeasible.getStatus())) {
                        //是否显示立项按钮操作
                        pageList.getRecords().get(m).setSpStatus(5);
                    }
                    // 根据项目负责人及申报单位信息，查询往期申报是否存在逾期项目
                    String enterId = sciencetechfeasible.getEnterpriseguid();
                    String leaderName = sciencetechfeasible.getProjectleader();
                    if (StrUtil.isNotBlank(enterId) && StrUtil.isNotBlank(leaderName)) {
                        QueryWrapper<ZgsSciencetechtask> queryWrapperForYq = new QueryWrapper<>();
                        queryWrapperForYq.eq("s.enterpriseguid",enterId);
                        queryWrapperForYq.eq("s.projectleader",leaderName);
                        queryWrapperForYq.eq("s.STATUS",8);
                        queryWrapperForYq.isNotNull("s.projectname");
                        // queryWrapperForYq.eq("s.dealtype","0").or().eq("s.dealtype","1");
                        List<ZgsSciencetechtask> listForYq = zgsSciencetechtaskService.zgsScienceTechTaskListForYq(queryWrapperForYq);
                        if (ObjectUtil.isNotNull(listForYq) && listForYq.size() > 0) {
                            sciencetechfeasible.setYqProjectList(listForYq);
                            sciencetechfeasible.setYqFlag("1");
                        } else {
                            sciencetechfeasible.setYqProjectList(listForYq);
                            sciencetechfeasible.setYqFlag("0");
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(sciencetechfeasible.getUpdateSave()) && sciencetechfeasible.getUpdateSave().equals("1")) {  // 修改未提交
                        sciencetechfeasible.setBjzt("1");  // 修改未提交
                    } else if (StrUtil.isNotBlank(sciencetechfeasible.getUpdateSave()) && sciencetechfeasible.getUpdateSave().equals("2")) {  // 修改已提交
                        sciencetechfeasible.setBjzt("2");  // 修改已提交
                    }
                    if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("1")) {  // 授权编辑
                        sciencetechfeasible.setSqbjBol(false);  // 授权编辑
                        sciencetechfeasible.setUpdateFlag("1"); // 授权编辑

                    } else if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("2")) {  // 取消编辑
                        sciencetechfeasible.setSqbjBol(true);  // 取消编辑
                        sciencetechfeasible.setUpdateFlag("2");  // 取消编辑
                    }


                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-添加")
    @ApiOperation(value = "建设科技攻关项目库-添加", notes = "建设科技攻关项目库-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsSciencetechfeasible.setId(id);
        zgsSciencetechfeasible.setEnterpriseguid(sysUser.getEnterpriseguid());
        if (GlobalConstants.handleSubmit.equals(zgsSciencetechfeasible.getStatus())) {
            //保存并上报
            zgsSciencetechfeasible.setApplydate(new Date());
        }
        zgsSciencetechfeasible.setCreateaccountname(sysUser.getUsername());
        zgsSciencetechfeasible.setCreateusername(sysUser.getRealname());
        zgsSciencetechfeasible.setCreatedate(new Date());
        zgsSciencetechfeasible.setYearNum(zgsCommonService.queryApplyYear());
        if (!VerificationUtil.projectLeaderVerification(zgsSciencetechfeasible.getProjectleader())) {
            return Result.error("项目负责人名称填写不规范！");
        }
        int mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader("",zgsSciencetechfeasible.getCommitmentunit(), zgsSciencetechfeasible.getProjectleader(),zgsSciencetechfeasible.getStartdate(), 1);
        if (mesNum != -1) {
//            return Result.error("在研阶段，项目负责人仅可上报2个项目！");
            return Result.error(GreenUtilSelf.getMesByNum(mesNum));
        }
        zgsSciencetechfeasible.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
        zgsSciencetechfeasible.setProjectstagestatus(zgsSciencetechfeasible.getStatus());
        zgsSciencetechfeasibleService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencetechfeasible, ValidateEncryptEntityUtil.isEncrypt));
//        redisUtil.set(zgsSciencetechfeasible.getId(), "申报阶段");
        //负责单位
        List<ZgsScienceparticipant> zgsScienceparticipantList0 = zgsSciencetechfeasible.getZgsScienceparticipantList0();
        if (zgsScienceparticipantList0 != null && zgsScienceparticipantList0.size() > 0) {
            for (int a0 = 0; a0 < zgsScienceparticipantList0.size(); a0++) {
                ZgsScienceparticipant zgsScienceparticipant = zgsScienceparticipantList0.get(a0);
                zgsScienceparticipant.setId(UUID.randomUUID().toString());
                zgsScienceparticipant.setSciencebaseguid(id);
                zgsScienceparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScienceparticipant.setCreateaccountname(sysUser.getUsername());
                zgsScienceparticipant.setCreateusername(sysUser.getRealname());
                zgsScienceparticipant.setCreatedate(new Date());
                zgsScienceparticipant.setPersontype(new BigDecimal(0));
                zgsScienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        // 项目完成单位
        // 将项目完成单位添加至申报阶段进行填写，验收阶段、验收证书阶段只进行信息回显  rxl 20230809
        List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsSciencetechfeasible.getZgsScientificprojectfinishList();
        if (zgsScientificprojectfinishList != null && zgsScientificprojectfinishList.size() > 0) {
            if (zgsScientificprojectfinishList.size() > 5) {
                return Result.error("项目完成单位情况不得超过5个！");
            }
            for (int a2 = 0; a2 < zgsScientificprojectfinishList.size(); a2++) {
                ZgsScientificprojectfinish zgsScientificprojectfinish = zgsScientificprojectfinishList.get(a2);
                zgsScientificprojectfinish.setId(UUID.randomUUID().toString());
                zgsScientificprojectfinish.setBaseguid(id);
                zgsScientificprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScientificprojectfinish.setCreateaccountname(sysUser.getUsername());
                zgsScientificprojectfinish.setCreateusername(sysUser.getRealname());
                zgsScientificprojectfinish.setCreatedate(new Date());
                zgsScientificprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
            }
        }


        //参加单位及人员
        List<ZgsScienceparticipant> zgsScienceparticipantList1 = zgsSciencetechfeasible.getZgsScienceparticipantList1();
        if (zgsScienceparticipantList1 != null && zgsScienceparticipantList1.size() > 0) {
            if (zgsScienceparticipantList1.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a1 = 0; a1 < zgsScienceparticipantList1.size(); a1++) {
                ZgsScienceparticipant zgsScienceparticipant = zgsScienceparticipantList1.get(a1);
                zgsScienceparticipant.setId(UUID.randomUUID().toString());
                zgsScienceparticipant.setSciencebaseguid(id);
                zgsScienceparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScienceparticipant.setCreateaccountname(sysUser.getUsername());
                zgsScienceparticipant.setCreateusername(sysUser.getRealname());
                zgsScienceparticipant.setCreatedate(new Date());
                zgsScienceparticipant.setPersontype(new BigDecimal(1));
                zgsScienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //计划进度
        List<ZgsScienceplanarrange> zgsScienceplanarrangeList = zgsSciencetechfeasible.getZgsScienceplanarrangeList();
        if (zgsScienceplanarrangeList != null && zgsScienceplanarrangeList.size() > 0) {
            for (int a2 = 0; a2 < zgsScienceplanarrangeList.size(); a2++) {
                ZgsScienceplanarrange zgsScienceplanarrange = zgsScienceplanarrangeList.get(a2);
                zgsScienceplanarrange.setId(UUID.randomUUID().toString());
                zgsScienceplanarrange.setScienceguid(id);
                zgsScienceplanarrange.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsScienceplanarrange.setCreateaccountname(sysUser.getUsername());
                zgsScienceplanarrange.setCreateusername(sysUser.getRealname());
                zgsScienceplanarrange.setCreatedate(new Date());
                zgsScienceplanarrangeService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceplanarrange, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //申报单位_科技攻关&软科学使用同一张表
        List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencetechfeasible.getZgsSciencejointunitList();
        if (zgsSciencejointunitList.size() > 5) {
            return Result.error("项目完成单位不得超过5个！");
        }
        if (zgsSciencejointunitList != null && zgsSciencejointunitList.size() > 0) {
            for (int a3 = 0; a3 < zgsSciencejointunitList.size(); a3++) {
                ZgsSciencejointunit zgsSciencejointunit = zgsSciencejointunitList.get(a3);
                zgsSciencejointunit.setId(UUID.randomUUID().toString());
                zgsSciencejointunit.setScienceguid(id);
                zgsSciencejointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSciencejointunit.setCreateaccountname(sysUser.getUsername());
                zgsSciencejointunit.setCreateusername(sysUser.getRealname());
                zgsSciencejointunit.setCreatedate(new Date());
                zgsSciencejointunit.setSciencetype(zgsSciencetechfeasible.getSciencetype());
                zgsSciencejointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));


                // 将通信地址、邮政编码、单位属性信息新增至验收证书阶段的项目完成单位表中，关联表 zgs_sacceptcertprojectfinish
                /*ZgsSacceptcertprojectfinish zgsSacceptcertprojectfinish = new ZgsSacceptcertprojectfinish();
                zgsSacceptcertprojectfinish.setId(UUID.randomUUID().toString());
                // zgsSacceptcertprojectfinish.setBaseguid(id);  此id为验收证书阶段项目新增产生的id  故此处不在使用，替换为申报阶段项目新增的主键id  rxl 20230728
                zgsSacceptcertprojectfinish.setKysbid(id);   // 申报阶段 科研项目的主键
                zgsSacceptcertprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSacceptcertprojectfinish.setEnterprisename(zgsSciencejointunitList.get(a3).getApplyunit()); // 完成单位名称为申报阶段填写的 联合申报单位
                zgsSacceptcertprojectfinish.setEnterpriseaddress(zgsSciencejointunitList.get(a3).getCommunicationAddress());  // 通信地址
                zgsSacceptcertprojectfinish.setPostalcode(zgsSciencejointunitList.get(a3).getPostalCode());  // 邮政编码
                zgsSacceptcertprojectfinish.setUnitstate(zgsSciencejointunitList.get(a3).getUnitAttribute());  // 单位属性
                zgsSacceptcertprojectfinish.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertprojectfinish.setCreateusername(sysUser.getRealname());
                zgsSacceptcertprojectfinish.setCreatedate(new Date());
                zgsSacceptcertprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertprojectfinish, ValidateEncryptEntityUtil.isEncrypt));*/

            }
        }
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsSciencetechfeasible.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(zgsSciencetechfeasible.getSciencetype());
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }

        //经费预算
        List<ZgsSciencefundbudget> zgsSciencefundbudgetList = zgsSciencetechfeasible.getZgsSciencefundbudgetList();
        if (zgsSciencefundbudgetList != null && zgsSciencefundbudgetList.size() > 0) {
            for (int a5 = 0; a5 < zgsSciencefundbudgetList.size(); a5++) {
                ZgsSciencefundbudget zgsSciencefundbudget = zgsSciencefundbudgetList.get(a5);
                zgsSciencefundbudget.setId(UUID.randomUUID().toString());
                zgsSciencefundbudget.setSciencebaseguid(id);
                zgsSciencefundbudget.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSciencefundbudget.setCreateaccountname(sysUser.getUsername());
                zgsSciencefundbudget.setCreateusername(sysUser.getRealname());
                zgsSciencefundbudget.setCreatedate(new Date());
                zgsSciencefundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencefundbudget, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        //add  添加验收专家名单  20230616  rxl
        /*List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSciencetechfeasible.getZgsSacceptcertexpertList();
        if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
            for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList.get(a1);
                zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                zgsSacceptcertexpert.setBaseguid(id);
                zgsSacceptcertexpert.setProjectStage("sbjd");  // 申报阶段
                zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                zgsSacceptcertexpert.setCreatedate(new Date());
                zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
            }
        }*/
        return Result.OK("添加成功！");
    }

    /**
     * 撤回
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-撤回")
    @ApiOperation(value = "建设科技攻关项目库-撤回", notes = "建设科技攻关项目库-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        if (zgsSciencetechfeasible != null) {
            String id = zgsSciencetechfeasible.getId();
            String status = zgsSciencetechfeasible.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1).equals(zgsSciencetechfeasible.getProjectstage())) {
                    if (zgsSciencetechfeasible.getAgreeproject() == null) {
                        //未立项
                        //更新项目状态
                        //更新退回原因为空
                        //更新各阶段状态
                        ZgsSciencetechfeasible info = new ZgsSciencetechfeasible();
                        info.setId(id);
                        info.setStatus(GlobalConstants.SHENHE_STATUS4);
                        info.setReturntype(null);
                        info.setAuditopinion(null);
                        info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                        info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                        zgsSciencetechfeasibleService.updateById(info);
                        //删除审批记录
                        zgsReturnrecordService.deleteReturnRecordLastLog(id);
                    } else {
                        //已立项
//                        ZgsSciencetechfeasible info = new ZgsSciencetechfeasible();
//                        info.setId(id);
//                        info.setStatus(GlobalConstants.SHENHE_STATUS12);
//                        info.setReturntype(null);
//                        info.setAuditopinion(null);
//                        info.setAgreeproject(null);
//                        info.setProvincialfundtotal(null);
//                        info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
//                        info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
//                        zgsSciencetechfeasibleService.updateById(info);
                        //修改位sql操作
                        zgsSciencetechfeasibleService.rollBackSp(id);
                        //删除审批记录
                        zgsReturnrecordService.deleteReturnRecordLastLogByLx(id);
                    }
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-审批")
    @ApiOperation(value = "建设科技攻关项目库-审批", notes = "建设科技攻关项目库-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getId())) {
            ZgsSciencetechfeasible sciencetechfeasible = new ZgsSciencetechfeasible();
            sciencetechfeasible.setId(zgsSciencetechfeasible.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsSciencetechfeasible.getId());
            zgsReturnrecord.setReturnreason(zgsSciencetechfeasible.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            ZgsSciencetechfeasible feaInfo = zgsSciencetechfeasibleService.getById(zgsSciencetechfeasible.getId());
            if (feaInfo != null) {
                if (GlobalConstants.ScienceTech.equals(feaInfo.getSciencetype())) {
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE5);
                } else {
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE6);
                }
                if (zgsSciencetechfeasible.getSpStatus() == 1 && GlobalConstants.SHENHE_STATUS1.equals(zgsSciencetechfeasible.getStatus())) {
                    //更新当前项目阶段
                    sciencetechfeasible.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                }
                zgsReturnrecord.setEnterpriseguid(feaInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(feaInfo.getApplydate());
                zgsReturnrecord.setProjectname(feaInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", feaInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            //以下修改覆盖
            if (zgsSciencetechfeasible.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                sciencetechfeasible.setAuditopinion(null);
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSciencetechfeasible.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsSciencetechfeasible.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsSciencetechfeasible.getStatus())) {
                    sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS4);
                    sciencetechfeasible.setFirstdate(new Date());
                    sciencetechfeasible.setFirstperson(sysUser.getRealname());
                    sciencetechfeasible.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsSciencetechfeasible.getStatus())) {
                    sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS8);
                    sciencetechfeasible.setFinishperson(sysUser.getRealname());
                    sciencetechfeasible.setFinishdate(new Date());
                    sciencetechfeasible.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //立项审批-补正修改
                    if (feaInfo != null && StringUtils.isNotEmpty(feaInfo.getLxbzStatus())) {
                        sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS12);
                    }
                }
            } else {
                //驳回-退回（补正修改） 不通过记录状态==1
                sciencetechfeasible.setAuditopinion(zgsSciencetechfeasible.getAuditopinion());
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSciencetechfeasible.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsSciencetechfeasible.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsSciencetechfeasible.getStatus())) {
                    if (zgsSciencetechfeasible.getSpStatus() == 2) {
                        sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    sciencetechfeasible.setFirstdate(new Date());
                    sciencetechfeasible.setFirstperson(sysUser.getRealname());
                    sciencetechfeasible.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsSciencetechfeasible.getStatus())) {
                    if (zgsSciencetechfeasible.getSpStatus() == 2) {
                        sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    sciencetechfeasible.setFinishperson(sysUser.getRealname());
                    sciencetechfeasible.setFinishdate(new Date());
                    sciencetechfeasible.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
                if (zgsSciencetechfeasible.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(sciencetechfeasible.getAuditopinion())) {
                        sciencetechfeasible.setAuditopinion(" ");
                    }
                    sciencetechfeasible.setAuditopinion(sciencetechfeasible.getAuditopinion());
                    sciencetechfeasible.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else {
                    //驳回
                    sciencetechfeasible.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                    if (GlobalConstants.SHENHE_STATUS14.equals(zgsSciencetechfeasible.getStatus())) {
                        sciencetechfeasible.setAgreeproject(new BigDecimal(1));
                    }
                }
            }
            //以上修改覆盖
            zgsReturnrecord.setProjectlibraryguid(zgsSciencetechfeasible.getId());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            zgsSciencetechfeasibleService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(sciencetechfeasible, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            zgsProjectlibraryService.initStageAndStatus(zgsSciencetechfeasible.getId());
        }
        return Result.OK("审批成功！");
    }

    /**
     * 审批
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "工程示范项目库-立项")
    @ApiOperation(value = "工程示范项目库-立项", notes = "工程示范项目库-立项")
    @PutMapping(value = "/lxProject")
    public Result<?> lxProject(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getId())) {
            String id = zgsSciencetechfeasible.getId();
            ZgsSciencetechfeasible sciencetechfeasible = new ZgsSciencetechfeasible();
            sciencetechfeasible.setId(id);
//            String projectNum = zgsProjectlibraryService.getProjectNumCurrent();
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(id);
            zgsReturnrecord.setReturnreason(zgsSciencetechfeasible.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            ZgsSciencetechfeasible feaInfo = zgsSciencetechfeasibleService.getById(id);
            if (feaInfo != null) {
                if (GlobalConstants.ScienceTech.equals(feaInfo.getSciencetype())) {
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE5);
                } else {
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE6);
                }
                zgsReturnrecord.setEnterpriseguid(feaInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(feaInfo.getApplydate());
                zgsReturnrecord.setProjectname(feaInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", feaInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsSciencetechfeasible.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                sciencetechfeasible.setAuditopinion(null);
                //生成项目编号，修改位手动填写
//                sciencetechfeasible.setProjectnum(projectNum);
                sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS8);
                sciencetechfeasible.setAgreeproject(new BigDecimal(2));
            } else if (zgsSciencetechfeasible.getSpStatus() == 2) {
                //立项环节补正修改
                zgsReturnrecord.setReturntype(new BigDecimal(2));
                sciencetechfeasible.setAuditopinion(zgsSciencetechfeasible.getAuditopinion());
                sciencetechfeasible.setReturntype(new BigDecimal(2));
                sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS9);
                sciencetechfeasible.setLxbzStatus("1");
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                sciencetechfeasible.setAuditopinion(zgsSciencetechfeasible.getAuditopinion());
                sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS8);
                sciencetechfeasible.setAgreeproject(new BigDecimal(1));
            }
            //添加审批记录
            //立项
            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
            zgsReturnrecord.setProjectlibraryguid(zgsSciencetechfeasible.getId());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            zgsSciencetechfeasibleService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(sciencetechfeasible, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            zgsProjectlibraryService.initStageAndStatus(id);
        }
        return Result.OK("立项成功！");
    }

    /**
     * 新增省科技经费总额
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "新增省科技经费总额")
    @ApiOperation(value = "新增省科技经费总额", notes = "新增省科技经费总额")
    @PutMapping(value = "/addMoney")
    public Result<?> addMoney(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getId())) {
            zgsSciencetechfeasibleService.updateAddMoney(zgsSciencetechfeasible);
        }
        return Result.OK("省科技经费添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsSciencetechfeasible
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-编辑")
    @ApiOperation(value = "建设科技攻关项目库-编辑", notes = "建设科技攻关项目库-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSciencetechfeasible zgsSciencetechfeasible) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getId())) {
            String id = zgsSciencetechfeasible.getId();
            ZgsSciencetechfeasible sciencetechfeasible = zgsSciencetechfeasibleService.getById(id);

            if (ObjectUtil.isNotNull(sciencetechfeasible)) {

                if (StrUtil.isNotBlank(sciencetechfeasible.getSqbj()) && sciencetechfeasible.getSqbj().equals("2")) {  // 终审单位取消编辑,已修改的信息则不能保存
                    Result result = new Result();

                    result.setMessage("授权编辑已被取消，已修改信息不能保存");
                    result.setCode(0);
                    result.setSuccess(false);
                    return result;

                } else {
                    if (GlobalConstants.handleSubmit.equals(zgsSciencetechfeasible.getStatus())) {
                        zgsSciencetechfeasible.setAuditopinion(null);
                        //保存并上报
                        zgsSciencetechfeasible.setApplydate(new Date());
//                zgsSciencetechfeasible.setReturntype(null);
                        //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                        if (GlobalConstants.SHENHE_STATUS6.equals(sciencetechfeasible.getStatus())) {
                            zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS8);
                            //查分配所有修改专家状态
                            QueryWrapper<ZgsSofttechexpert> queryWrapper = new QueryWrapper();
                            queryWrapper.eq("businessguid", id);
                            List<ZgsSofttechexpert> zgsSofttechexpertList = zgsSofttechexpertService.list(queryWrapper);
                            if (zgsSofttechexpertList != null && zgsSofttechexpertList.size() > 0) {
                                for (ZgsSofttechexpert zgsSofttechexpert : zgsSofttechexpertList) {
                                    ZgsSofttechexpert softtechexpert = new ZgsSofttechexpert();
                                    softtechexpert.setId(zgsSofttechexpert.getId());
                                    softtechexpert.setAuditconclusionnum(null);
                                    zgsSofttechexpertService.updateById(softtechexpert);
                                }
                            }
                        }
                    } else {
                        zgsSciencetechfeasible.setApplydate(null);
                        //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                        if (GlobalConstants.SHENHE_STATUS6.equals(sciencetechfeasible.getStatus())) {
                            zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS6);
                        }
                        //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                        if (StringUtils.isNotEmpty(sciencetechfeasible.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(sciencetechfeasible.getStatus())) {
                            zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS3);
                        }
                    }
                    zgsSciencetechfeasible.setModifyaccountname(sysUser.getUsername());
                    zgsSciencetechfeasible.setModifyusername(sysUser.getRealname());
                    zgsSciencetechfeasible.setModifydate(new Date());
                    zgsSciencetechfeasible.setCreatedate(null);
                    zgsSciencetechfeasible.setFinishdate(null);
                    zgsSciencetechfeasible.setFirstdate(null);
                    if (!VerificationUtil.projectLeaderVerification(zgsSciencetechfeasible.getProjectleader())) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                    int mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader("", zgsSciencetechfeasible.getCommitmentunit(), zgsSciencetechfeasible.getProjectleader(), zgsSciencetechfeasible.getStartdate(), 2);
                    // if (mesNum != -1) {
                    if (mesNum != -1 && 3 != sysUser.getLoginUserType()) {  //管理员可以编辑
                        return Result.error(GreenUtilSelf.getMesByNum(mesNum));
                    }
                    zgsSciencetechfeasible.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    zgsSciencetechfeasible.setProjectstagestatus(zgsSciencetechfeasible.getStatus());

                    // 将授权编辑变为 取消授权编辑（说明：终审单位的此次授权编辑已使用完，想要修改，必须重新授权）
                    if (StringUtils.isNotBlank(zgsSciencetechfeasible.getSqbj()) && StringUtils.isNotBlank(zgsSciencetechfeasible.getUpdateSave())) {
                        zgsSciencetechfeasible.setSqbj("2");  // 取消编辑
                        zgsSciencetechfeasible.setUpdateSave("2");  // 修改已提交
                    }

                    zgsSciencetechfeasibleService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencetechfeasible, ValidateEncryptEntityUtil.isEncrypt));

                    // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
                    QueryWrapper<ZgsSacceptcertexpert> expertQueryWrapper = new QueryWrapper<>();
                    expertQueryWrapper.eq("baseguid", zgsSciencetechfeasible.getId());
                    expertQueryWrapper.eq("project_stage", "sbjd");
                    expertQueryWrapper.ne("isdelete", 1);
                    List<ZgsSacceptcertexpert> ZgsSacceptcertexpertList = zgsSacceptcertexpertService.list(expertQueryWrapper);
                    if (ZgsSacceptcertexpertList.size() > 0) {
                        // 删除历史专家信息
                        QueryWrapper<ZgsSacceptcertexpert> expertDeleteWrapper = new QueryWrapper<>();
                        expertDeleteWrapper.eq("baseguid", zgsSciencetechfeasible.getId());
                        expertDeleteWrapper.eq("project_stage", "sbjd");
                        expertDeleteWrapper.ne("isdelete", 1);
                        boolean bolResult = zgsSacceptcertexpertService.remove(expertDeleteWrapper);
                        // 删除成功后，保存修改后的专家信息  rxl  20230620
                        if (bolResult) {
                            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSciencetechfeasible.getZgsSacceptcertexpertList();
                            if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
                                for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                                    ZgsSacceptcertexpert zgsSacceptcertexpertForUpdate = zgsSacceptcertexpertList.get(a1);
                                    zgsSacceptcertexpertForUpdate.setId(UUID.randomUUID().toString());
                                    zgsSacceptcertexpertForUpdate.setBaseguid(zgsSciencetechfeasible.getId());
                                    zgsSacceptcertexpertForUpdate.setProjectStage("sbjd");   // 申报阶段
                                    zgsSacceptcertexpertForUpdate.setEnterpriseguid(sysUser.getEnterpriseguid());
                                    zgsSacceptcertexpertForUpdate.setCreateaccountname(sysUser.getUsername());
                                    zgsSacceptcertexpertForUpdate.setCreateusername(sysUser.getRealname());
                                    zgsSacceptcertexpertForUpdate.setCreatedate(new Date());
                                    zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpertForUpdate, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }

                    //负责单位
                    List<ZgsScienceparticipant> zgsScienceparticipantList0 = zgsSciencetechfeasible.getZgsScienceparticipantList0();
                    QueryWrapper<ZgsScienceparticipant> queryWrapper0 = new QueryWrapper<>();
                    queryWrapper0.eq("sciencebaseguid", id);
                    queryWrapper0.eq("persontype", "0");
                    queryWrapper0.ne("isdelete", 1);
                    queryWrapper0.orderByAsc("ordernum");
                    List<ZgsScienceparticipant> zgsScienceparticipantList0_1 = zgsScienceparticipantService.list(queryWrapper0);
                    if (zgsScienceparticipantList0 != null && zgsScienceparticipantList0.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScienceparticipantList0.size(); a0++) {
                            ZgsScienceparticipant zgsScienceparticipant = zgsScienceparticipantList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScienceparticipant.getId())) {
                                //编辑
                                zgsScienceparticipant.setModifyaccountname(sysUser.getUsername());
                                zgsScienceparticipant.setModifyusername(sysUser.getRealname());
                                zgsScienceparticipant.setModifydate(new Date());
                                zgsScienceparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScienceparticipant.setId(UUID.randomUUID().toString());
                                zgsScienceparticipant.setSciencebaseguid(id);
                                // zgsScienceparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScienceparticipant.setEnterpriseguid(sysUser.getLoginUserType() == 3 ? zgsSciencetechfeasible.getEnterpriseguid() : sysUser.getEnterpriseguid());
                                zgsScienceparticipant.setCreateaccountname(sysUser.getUsername());
                                zgsScienceparticipant.setCreateusername(sysUser.getRealname());
                                zgsScienceparticipant.setCreatedate(new Date());
                                zgsScienceparticipant.setPersontype(new BigDecimal(0));
                                zgsScienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScienceparticipant zgsScienceparticipant1 : zgsScienceparticipantList0_1) {
                                if (!zgsScienceparticipant1.getId().equals(zgsScienceparticipant.getId())) {
                                    map0.put(zgsScienceparticipant1.getId(), zgsScienceparticipant1.getId());
                                } else {
                                    list0.add(zgsScienceparticipant1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScienceparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScienceparticipantList0_1 != null && zgsScienceparticipantList0_1.size() > 0) {
                            for (ZgsScienceparticipant zgsScienceparticipant : zgsScienceparticipantList0_1) {
                                zgsScienceparticipantService.removeById(zgsScienceparticipant.getId());
                            }
                        }
                    }
                    //参加单位及人员
                    QueryWrapper<ZgsScienceparticipant> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("sciencebaseguid", id);
                    queryWrapper1.eq("persontype", "1");
                    queryWrapper1.ne("isdelete", 1);
                    queryWrapper1.orderByAsc("ordernum");
                    List<ZgsScienceparticipant> zgsScienceparticipantList1_1 = zgsScienceparticipantService.list(queryWrapper1);
                    List<ZgsScienceparticipant> zgsScienceparticipantList1 = zgsSciencetechfeasible.getZgsScienceparticipantList1();
                    if (zgsScienceparticipantList1 != null && zgsScienceparticipantList1.size() > 0) {
                        if (zgsScienceparticipantList1.size() > 15) {
                            return Result.error("人员名单数量不得超过15人！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a1 = 0; a1 < zgsScienceparticipantList1.size(); a1++) {
                            ZgsScienceparticipant zgsScienceparticipant = zgsScienceparticipantList1.get(a1);
                            if (StringUtils.isNotEmpty(zgsScienceparticipant.getId())) {
                                //编辑
                                zgsScienceparticipant.setModifyaccountname(sysUser.getUsername());
                                zgsScienceparticipant.setModifyusername(sysUser.getRealname());
                                zgsScienceparticipant.setModifydate(new Date());
                                zgsScienceparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScienceparticipant.setId(UUID.randomUUID().toString());
                                zgsScienceparticipant.setSciencebaseguid(id);
                                // zgsScienceparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScienceparticipant.setEnterpriseguid(sysUser.getLoginUserType() == 3 ? zgsSciencetechfeasible.getEnterpriseguid() : sysUser.getEnterpriseguid());
                                zgsScienceparticipant.setCreateaccountname(sysUser.getUsername());
                                zgsScienceparticipant.setCreateusername(sysUser.getRealname());
                                zgsScienceparticipant.setCreatedate(new Date());
                                zgsScienceparticipant.setPersontype(new BigDecimal(1));
                                zgsScienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScienceparticipant zgsScienceparticipant1 : zgsScienceparticipantList1_1) {
                                if (!zgsScienceparticipant1.getId().equals(zgsScienceparticipant.getId())) {
                                    map0.put(zgsScienceparticipant1.getId(), zgsScienceparticipant1.getId());
                                } else {
                                    list0.add(zgsScienceparticipant1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScienceparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScienceparticipantList1_1 != null && zgsScienceparticipantList1_1.size() > 0) {
                            for (ZgsScienceparticipant zgsScienceparticipant : zgsScienceparticipantList1_1) {
                                zgsScienceparticipantService.removeById(zgsScienceparticipant.getId());
                            }
                        }
                    }
                    //计划进度
                    List<ZgsScienceplanarrange> zgsScienceplanarrangeList = zgsSciencetechfeasible.getZgsScienceplanarrangeList();
                    QueryWrapper<ZgsScienceplanarrange> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("scienceguid", id);
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsScienceplanarrange> zgsScienceplanarrangeList_1 = zgsScienceplanarrangeService.list(queryWrapper2);
                    if (zgsScienceplanarrangeList != null && zgsScienceplanarrangeList.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a2 = 0; a2 < zgsScienceplanarrangeList.size(); a2++) {
                            ZgsScienceplanarrange zgsScienceplanarrange = zgsScienceplanarrangeList.get(a2);
                            if (StringUtils.isNotEmpty(zgsScienceplanarrange.getId())) {
                                //编辑
                                zgsScienceplanarrange.setModifyaccountname(sysUser.getUsername());
                                zgsScienceplanarrange.setModifyusername(sysUser.getRealname());
                                zgsScienceplanarrange.setModifydate(new Date());
                                zgsScienceplanarrangeService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceplanarrange, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScienceplanarrange.setId(UUID.randomUUID().toString());
                                zgsScienceplanarrange.setScienceguid(id);
                                zgsScienceplanarrange.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScienceplanarrange.setCreateaccountname(sysUser.getUsername());
                                zgsScienceplanarrange.setCreateusername(sysUser.getRealname());
                                zgsScienceplanarrange.setCreatedate(new Date());
                                zgsScienceplanarrangeService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScienceplanarrange, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScienceplanarrange zgsScienceplanarrange1 : zgsScienceplanarrangeList_1) {
                                if (!zgsScienceplanarrange1.getId().equals(zgsScienceplanarrange.getId())) {
                                    map0.put(zgsScienceplanarrange1.getId(), zgsScienceplanarrange1.getId());
                                } else {
                                    list0.add(zgsScienceplanarrange1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScienceplanarrangeService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScienceplanarrangeList_1 != null && zgsScienceplanarrangeList_1.size() > 0) {
                            for (ZgsScienceplanarrange zgsScienceplanarrange : zgsScienceplanarrangeList_1) {
                                zgsScienceplanarrangeService.removeById(zgsScienceplanarrange.getId());
                            }
                        }
                    }
                    //申报单位
                    List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencetechfeasible.getZgsSciencejointunitList();
                    QueryWrapper<ZgsSciencejointunit> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("scienceguid", id);
                    queryWrapper3.ne("isdelete", 1);
                    List<ZgsSciencejointunit> zgsSciencejointunitList_1 = zgsSciencejointunitService.list(queryWrapper3);
                    if (zgsSciencejointunitList != null && zgsSciencejointunitList.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a3 = 0; a3 < zgsSciencejointunitList.size(); a3++) {
                            ZgsSciencejointunit zgsSciencejointunit = zgsSciencejointunitList.get(a3);
                            if (StringUtils.isNotEmpty(zgsSciencejointunit.getId())) {
                                //编辑
                                zgsSciencejointunit.setModifyaccountname(sysUser.getUsername());
                                zgsSciencejointunit.setModifyusername(sysUser.getRealname());
                                zgsSciencejointunit.setModifydate(new Date());
                                zgsSciencejointunitService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsSciencejointunit.setId(UUID.randomUUID().toString());
                                zgsSciencejointunit.setScienceguid(id);
                                zgsSciencejointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsSciencejointunit.setCreateaccountname(sysUser.getUsername());
                                zgsSciencejointunit.setCreateusername(sysUser.getRealname());
                                zgsSciencejointunit.setCreatedate(new Date());
                                zgsSciencejointunit.setSciencetype(zgsSciencetechfeasible.getSciencetype());
                                zgsSciencejointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsSciencejointunit zgsSciencejointunit1 : zgsSciencejointunitList_1) {
                                if (!zgsSciencejointunit1.getId().equals(zgsSciencejointunit.getId())) {
                                    map0.put(zgsSciencejointunit1.getId(), zgsSciencejointunit1.getId());
                                } else {
                                    list0.add(zgsSciencejointunit1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsSciencejointunitService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsSciencejointunitList_1 != null && zgsSciencejointunitList_1.size() > 0) {
                            for (ZgsSciencejointunit zgsSciencejointunit : zgsSciencejointunitList_1) {
                                zgsSciencejointunitService.removeById(zgsSciencejointunit.getId());
                            }
                        }
                    }

                    //项目完成单位   申报阶段新增项目完成单位信息，故验收阶段、验收证书阶段不再进行信息新增或编辑操作，只进行信息回显  rxl 20230809
                    List<ZgsScientificprojectfinish> zgsScientificprojectfinishList0 = zgsSciencetechfeasible.getZgsScientificprojectfinishList();
                    QueryWrapper<ZgsScientificprojectfinish> queryWrapper4 = new QueryWrapper<>();
                    // queryWrapper4.eq("enterpriseguid", sysUser.getEnterpriseguid());
                    queryWrapper4.eq("baseguid", id);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.orderByAsc("ordernum");
                    List<ZgsScientificprojectfinish> zgsScientificprojectfinishList0_1 = zgsScientificprojectfinishService.list(queryWrapper4);
                    if (zgsScientificprojectfinishList0 != null && zgsScientificprojectfinishList0.size() > 0) {
                        if (zgsScientificprojectfinishList0.size() > 5) {
                            return Result.error("项目完成单位情况不得超过5个！");
                        }
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsScientificprojectfinishList0.size(); a0++) {
                            ZgsScientificprojectfinish zgsScientificprojectfinish = zgsScientificprojectfinishList0.get(a0);
                            if (StringUtils.isNotEmpty(zgsScientificprojectfinish.getId())) {
                                //编辑
                                zgsScientificprojectfinish.setModifyaccountname(sysUser.getUsername());
                                zgsScientificprojectfinish.setModifyusername(sysUser.getRealname());
                                zgsScientificprojectfinish.setModifydate(new Date());
                                zgsScientificprojectfinishService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsScientificprojectfinish.setId(UUID.randomUUID().toString());
                                zgsScientificprojectfinish.setBaseguid(id);
                                zgsScientificprojectfinish.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsScientificprojectfinish.setCreateaccountname(sysUser.getUsername());
                                zgsScientificprojectfinish.setCreateusername(sysUser.getRealname());
                                zgsScientificprojectfinish.setCreatedate(new Date());
                                zgsScientificprojectfinishService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsScientificprojectfinish, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsScientificprojectfinish zgsScientificprojectfinish1 : zgsScientificprojectfinishList0_1) {
                                if (!zgsScientificprojectfinish1.getId().equals(zgsScientificprojectfinish.getId())) {
                                    map0.put(zgsScientificprojectfinish1.getId(), zgsScientificprojectfinish1.getId());
                                } else {
                                    list0.add(zgsScientificprojectfinish1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsScientificprojectfinishService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsScientificprojectfinishList0_1 != null && zgsScientificprojectfinishList0_1.size() > 0) {
                            for (ZgsScientificprojectfinish zgsScientificprojectfinish : zgsScientificprojectfinishList0_1) {
                                zgsScientificprojectfinishService.removeById(zgsScientificprojectfinish.getId());
                            }
                        }
                    }


                    //相关附件
                    List<ZgsMattermaterial> zgsMattermaterialList = zgsSciencetechfeasible.getZgsMattermaterialList();
                    if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                        for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                            ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                            String mattermaterialId = zgsMattermaterial.getId();
                            UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                            wrapper.eq("relationid", mattermaterialId);
                            //先删除原来的再重新添加
                            zgsAttachappendixService.remove(wrapper);
                            if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                                for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                                    ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                                    if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                        zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                        zgsAttachappendix.setModifytime(new Date());
                                    } else {
                                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                                        zgsAttachappendix.setRelationid(mattermaterialId);
                                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                        zgsAttachappendix.setCreatetime(new Date());
                                        zgsAttachappendix.setAppendixtype(zgsSciencetechfeasible.getSciencetype());
                                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                                    }
                                    zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }
                    //经费预算
                    List<ZgsSciencefundbudget> zgsSciencefundbudgetList = zgsSciencetechfeasible.getZgsSciencefundbudgetList();
                    QueryWrapper<ZgsSciencefundbudget> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("sciencebaseguid", id);
                    queryWrapper5.ne("isdelete", 1);
                    queryWrapper5.orderByAsc("year");
                    List<ZgsSciencefundbudget> zgsSciencefundbudgetList_1 = zgsSciencefundbudgetService.list(queryWrapper5);
                    if (zgsSciencefundbudgetList != null && zgsSciencefundbudgetList.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a5 = 0; a5 < zgsSciencefundbudgetList.size(); a5++) {
                            ZgsSciencefundbudget zgsSciencefundbudget = zgsSciencefundbudgetList.get(a5);
                            if (StringUtils.isNotEmpty(zgsSciencefundbudget.getId())) {
                                //编辑
                                zgsSciencefundbudget.setModifyaccountname(sysUser.getUsername());
                                zgsSciencefundbudget.setModifyusername(sysUser.getRealname());
                                zgsSciencefundbudget.setModifydate(new Date());
                                zgsSciencefundbudgetService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencefundbudget, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsSciencefundbudget.setId(UUID.randomUUID().toString());
                                zgsSciencefundbudget.setSciencebaseguid(id);
                                zgsSciencefundbudget.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsSciencefundbudget.setCreateaccountname(sysUser.getUsername());
                                zgsSciencefundbudget.setCreateusername(sysUser.getRealname());
                                zgsSciencefundbudget.setCreatedate(new Date());
                                zgsSciencefundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencefundbudget, ValidateEncryptEntityUtil.isEncrypt));
                            }
                            for (ZgsSciencefundbudget zgsSciencefundbudget1 : zgsSciencefundbudgetList_1) {
                                if (!zgsSciencefundbudget1.getId().equals(zgsSciencefundbudget.getId())) {
                                    map0.put(zgsSciencefundbudget1.getId(), zgsSciencefundbudget1.getId());
                                } else {
                                    list0.add(zgsSciencefundbudget1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsSciencefundbudgetService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsSciencefundbudgetList_1 != null && zgsSciencefundbudgetList_1.size() > 0) {
                            for (ZgsSciencefundbudget zgsSciencefundbudget : zgsSciencefundbudgetList_1) {
                                zgsSciencefundbudgetService.removeById(zgsSciencefundbudget.getId());
                            }
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-通过id删除")
    @ApiOperation(value = "建设科技攻关项目库-通过id删除", notes = "建设科技攻关项目库-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSciencetechfeasibleService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-批量删除")
    @ApiOperation(value = "建设科技攻关项目库-批量删除", notes = "建设科技攻关项目库-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSciencetechfeasibleService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-通过id查询")
    @ApiOperation(value = "建设科技攻关项目库-通过id查询", notes = "建设科技攻关项目库-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id, @RequestParam(name = "sciencetype") String sciencetype) {
        ZgsSciencetechfeasible zgsSciencetechfeasible = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(id);
            if (zgsSciencetechfeasible == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsSciencetechfeasible);
            }
            if ((zgsSciencetechfeasible.getFirstdate() != null || zgsSciencetechfeasible.getFinishdate() != null) && zgsSciencetechfeasible.getApplydate() != null) {
                zgsSciencetechfeasible.setSpLogStatus(1);
            } else {
                zgsSciencetechfeasible.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsSciencetechfeasible = new ZgsSciencetechfeasible();
            Map<String, Object> map = new HashMap<>();
            //科技攻关ScienceTech、软科学ScienceSoft
            map.put("projecttypenum", sciencetype);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsSciencetechfeasible.setZgsMattermaterialList(zgsMattermaterialList);
            zgsSciencetechfeasible.setSpLogStatus(0);
        }
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechfeasible, ValidateEncryptEntityUtil.isDecrypt));
    }

    private void initProjectSelectById(ZgsSciencetechfeasible zgsSciencetechfeasible) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = zgsSciencetechfeasible.getEnterpriseguid();
        String scienceguid = zgsSciencetechfeasible.getId();
        String sciencetype = zgsSciencetechfeasible.getSciencetype();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(scienceguid)) {

            //add  查询验收专家名单  20230616  rxl
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", scienceguid);
            queryWrapper4.eq("project_stage", "sbjd");  // 查询当前项目在申报阶段的专家信息
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertexpertService.list(queryWrapper4);
            zgsSciencetechfeasible.setZgsSacceptcertexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertexpertList, ValidateEncryptEntityUtil.isDecrypt));

            //屏蔽法人设置，先判断是否为专家用户类型，再查询
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                zgsExpertsetQueryWrapper.eq("businessguid", scienceguid);
                ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                    String setValue = zgsExpertset.getSetvalue();
                    if (setValue.contains(",")) {
                        String strValue[] = setValue.split(",");
                        for (String set : strValue) {
                            if (GlobalConstants.expert_set_close1.equals(set)) {
                                zgsSciencetechfeasible.setCommitmentunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                zgsSciencetechfeasible.setProjectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                zgsSciencetechfeasible.setProjectleadertel(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(set)) {
                            }
                        }
                    } else {
                        if (GlobalConstants.expert_set_close1.equals(setValue)) {
                            zgsSciencetechfeasible.setCommitmentunit(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                            zgsSciencetechfeasible.setProjectleader(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                            zgsSciencetechfeasible.setProjectleadertel(GlobalConstants.expert_set_default);
                        } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                        }
                    }
                }
            }
            //
            //专家评审
            zgsSciencetechfeasible.setZgsSofttechexpertList(zgsSofttechexpertService.getZgsSofttechexpertList(scienceguid));
            //
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", sciencetype);
            map.put("buildguid", scienceguid);
            //项目主要参加人员（0：负责人 1：基本参加人员）
            QueryWrapper<ZgsScienceparticipant> queryWrapper0 = new QueryWrapper<>();
            queryWrapper0.eq("enterpriseguid", enterpriseguid);
            queryWrapper0.eq("sciencebaseguid", scienceguid);
            //queryWrapper0.eq("persontype", "0");
            queryWrapper0.ne("isdelete", 1);
            if (zgsSciencetechfeasible.getCreatedate() != null) {
                if (GreenUtilSelf.compareToCreateDate(zgsSciencetechfeasible.getCreatedate())) {
                    queryWrapper0.last("limit 15");
                }
            }
            queryWrapper0.orderByAsc("ordernum");
            List<ZgsScienceparticipant> zgsScienceparticipantList0 = zgsScienceparticipantService.list(queryWrapper0);
            for (int i = 0; i < zgsScienceparticipantList0.size(); i++) {
                zgsScienceparticipantList0.get(i).setOrdernum(BigDecimal.valueOf(i + 1));
            }
            zgsSciencetechfeasible.setZgsScienceparticipantList0(ValidateEncryptEntityUtil.validateDecryptList(zgsScienceparticipantList0, ValidateEncryptEntityUtil.isDecrypt));
//            QueryWrapper<ZgsScienceparticipant> queryWrapper1 = new QueryWrapper<>();
//            queryWrapper1.eq("enterpriseguid", enterpriseguid);
//            queryWrapper1.eq("sciencebaseguid", scienceguid);
//            queryWrapper1.eq("persontype", "1");
//            queryWrapper1.ne("isdelete", 1);
//            queryWrapper1.orderByAsc("ordernum");
//            queryWrapper1.last("limit 15");
//            List<ZgsScienceparticipant> zgsScienceparticipantList1 = zgsScienceparticipantService.list(queryWrapper1);
//            zgsSciencetechfeasible.setZgsScienceparticipantList1(zgsScienceparticipantList1);
            //计划进度与安排赋值
            QueryWrapper<ZgsScienceplanarrange> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("scienceguid", scienceguid);
            queryWrapper2.ne("isdelete", 1);
            queryWrapper2.orderByAsc("startdate");
            List<ZgsScienceplanarrange> zgsScienceplanarrangeList = zgsScienceplanarrangeService.list(queryWrapper2);
            zgsSciencetechfeasible.setZgsScienceplanarrangeList(ValidateEncryptEntityUtil.validateDecryptList(zgsScienceplanarrangeList, ValidateEncryptEntityUtil.isDecrypt));
            //联合申报单位
            QueryWrapper<ZgsSciencejointunit> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("scienceguid", scienceguid);
            queryWrapper3.isNull("tag");
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("createdate");
            List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper3);
            zgsSciencetechfeasible.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitList, ValidateEncryptEntityUtil.isDecrypt));

            // 项目完成单位  申报阶段项目完成单位回显   rxl  20230810
            QueryWrapper<ZgsScientificprojectfinish> queryWrapper8 = new QueryWrapper<>();
            // queryWrapper8.eq("enterpriseguid", enterpriseguid);
            queryWrapper8.eq("baseguid", scienceguid);
            queryWrapper8.ne("isdelete", 1);
            queryWrapper8.orderByAsc("ordernum");
            List<ZgsScientificprojectfinish> zgsScientificprojectfinishList0_1 = zgsScientificprojectfinishService.list(queryWrapper8);
            zgsSciencetechfeasible.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList0_1, ValidateEncryptEntityUtil.isDecrypt));

            // 项目附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper5.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper5), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsSciencetechfeasible.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //经费预算
            QueryWrapper<ZgsSciencefundbudget> queryWrapper5 = new QueryWrapper<>();
            queryWrapper5.eq("enterpriseguid", enterpriseguid);
            queryWrapper5.eq("sciencebaseguid", scienceguid);
            queryWrapper5.ne("isdelete", 1);
            queryWrapper5.orderByAsc("year");
            List<ZgsSciencefundbudget> zgsSciencefundbudgetList = zgsSciencefundbudgetService.list(queryWrapper5);
            zgsSciencetechfeasible.setZgsSciencefundbudgetList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencefundbudgetList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsSciencetechfeasible
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsSciencetechfeasible zgsSciencetechfeasible,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "accessType", required = false) Integer accessType,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsSciencetechfeasible, 1, 9999, accessType, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsSciencetechfeasible> pageList = (IPage<ZgsSciencetechfeasible>) result.getResult();
        List<ZgsSciencetechfeasible> list = pageList.getRecords();
        List<ZgsSciencetechfeasible> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        List<Map<String, String>> listMap = new ArrayList<>();
        if (exportList != null && exportList.size() > 0) {
            for (int i = 0; i < exportList.size(); i++) {
                ZgsSciencetechfeasible info = exportList.get(i);
                Map<String, String> lm = new HashMap<>();
                zgsSciencetechfeasibleService.getExportXlsData(info.getId(), lm);
                lm.put("id", i + 1 + "");
                lm.put("link", info.getCommitmentunit() + "\n" + info.getProjectleader() + "\n" + info.getProjectleadertel());
                lm.put("projectname", info.getProjectname());
                lm.put("startenddate", DateUtils.formatDate(info.getStartdate()) + "\n" + DateUtils.formatDate(info.getEnddate()));
                lm.put("content", info.getSimpleresearch());
//                lm.put("projectname", info.getProjectname());
//                lm.put("startenddate", "2022-12-22\n2023-12-22");
//                lm.put("content", "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试");
//                lm.put("link", "测试测试测试测单位\n立德\n18210502471");
//                lm.put("unit", "测试测试测试测单位1\n测试测试测试测单位2");
//                lm.put("bk", "0");
//                lm.put("zc", "100");
                listMap.add(lm);
            }
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateExcelView());
        if (StringUtils.isNotEmpty(zgsSciencetechfeasible.getSciencetype())) {
            if (GlobalConstants.ScienceTech.equals(zgsSciencetechfeasible.getSciencetype())) {
                //科技攻关
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-6申报书汇总表.xlsx" + "");
            } else {
                //软科学
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-7申报书汇总表.xlsx" + "");
            }
        }
        mv.addObject(NormalExcelConstants.MAP_LIST, listMap);
//        String title = "建设科技攻关项目库";
//        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
//        mv.addObject(NormalExcelConstants.CLASS, ZgsSciencetechfeasible.class);
//        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
//        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsSciencetechfeasible, ZgsSciencetechfeasible.class, "建设科技攻关项目库");
        return mv;
    }

    private String getId(ZgsSciencetechfeasible item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSciencetechfeasible.class);
    }

    /**
     * 导出WORD
     *
     * @param request
     * @param id
     */
    @RequestMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(id);
        if (zgsSciencetechfeasible != null) {
            initProjectSelectById(zgsSciencetechfeasible);
            zgsSciencetechfeasible = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechfeasible, ValidateEncryptEntityUtil.isDecrypt);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "6科技攻关项目.docx";
        if (zgsSciencetechfeasible.getSciencetype().equals(GlobalConstants.ScienceTech)) {
            templateName = "6科技攻关项目.docx";
        } else if (zgsSciencetechfeasible.getSciencetype().equals(GlobalConstants.ScienceSoft)) {
            templateName = "7软科学项目.docx";
        }
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsSciencetechfeasible);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }


    /**
     * @describe: 示范项目总览分页列表查询
     * @author: renxiaoliang
     * @date: 2023/6/20 15:59
     */
    @AutoLog(value = "科研项目项目总览-分页列表查询")
    @ApiOperation(value = "科研项目总览-分页列表查询", notes = "科研项目总览-分页列表查询")
    @GetMapping(value = "/queryScientificProjectList")
    public Result<?> queryScientificProjectList(@RequestParam Map<String,Object> params,HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Integer pageNo = Integer.parseInt(params.get("pageNo").toString());
        Integer pageSize = Integer.parseInt(params.get("pageSize").toString());
        String newFieldStr = "";
        if (ObjectUtil.isNotEmpty(params.get("fieldArray"))) {
            String[] newFieldStrArray = params.get("fieldArray").toString().split(",");
            for (int i = 0; i < newFieldStrArray.length; i++) {
                if (newFieldStrArray[i].equals("agreeprojectStr")) {
                    newFieldStr += "case when  z.agreeproject = 1 then '不同意立项'\n" +
                            "when z.agreeproject = 2 then '同意立项'\n" +
                            "else '' end agreeprojectStr, ";
                } else if (newFieldStrArray[i].equals("sfhs")) {
                    newFieldStr += "if(z.sfhs = 1, '是','否') as sfhs, ";
                } else {
                    newFieldStr += "z." + newFieldStrArray[i] + ",";
                }
            }
            newFieldStr = newFieldStr.substring(0,newFieldStr.lastIndexOf(","));
        }

        Map<String,Object> map = new HashMap<>();

        String agreeproject = ""; // 是否立项  1 ：不同意立项 ，2：同意立项
        String sfhs = "";  // 是否会审（1是，0否）
        String projectName = "";  // 项目名称
        String projectNum = ""; // 项目编号
        String commitmentunit = "";  // 申报单位
        String year = ""; // 年度
        String status = ""; // 状态  4：初审通过   8：形审通过   9：形审退回   14：形审驳回
        String fundType = "";  // 资金类型   1：省科技资金支持   0：省科技资金未支持
        String assType = "";  // 科研项目   ScienceTech：科技攻关    ScienceSoft：软科学
        String projectstage = ""; // 项目阶段

        QueryWrapper<ZgsSciencetechfeasible> queryWrapper = new QueryWrapper<>();  // 科研项目查询条件组装

        if (ObjectUtil.isNotNull(params.get("agreeproject")) && ObjectUtil.isNotEmpty(params.get("agreeproject"))) {
            agreeproject = params.get("agreeproject").toString();
            queryWrapper.eq("agreeproject", agreeproject);
        }

        if (ObjectUtil.isNotNull(params.get("sfhs")) && ObjectUtil.isNotEmpty(params.get("sfhs"))) {
            sfhs = params.get("sfhs").toString();
            queryWrapper.eq("sfhs", sfhs);
        }

        if (ObjectUtil.isNotNull(params.get("projectName")) && ObjectUtil.isNotEmpty(params.get("projectName"))) {
            projectName = params.get("projectName").toString();
            String finalProjectName = projectName;
            queryWrapper.and(wrapper -> wrapper.eq("z.projectname", Sm4Util.encryptEcb(finalProjectName)).or().like("z.projectname", finalProjectName));
        }

        if (ObjectUtil.isNotNull(params.get("projectNum")) && ObjectUtil.isNotEmpty(params.get("projectNum"))) {
            projectNum = params.get("projectNum").toString();
            queryWrapper.like("z.projectnum", projectNum);
        }


        if (ObjectUtil.isNotNull(params.get("commitmentunit")) && ObjectUtil.isNotEmpty(params.get("commitmentunit"))) {
            commitmentunit = params.get("commitmentunit").toString();
            String finalCommitmentunit = commitmentunit;
            queryWrapper.and(wrapper -> wrapper.eq("z.commitmentunit", Sm4Util.encryptEcb(finalCommitmentunit)).or().like("z.commitmentunit", finalCommitmentunit));
            // queryWrapper.like("z.commitmentunit", commitmentunit);
        }


        if (ObjectUtil.isNotNull(params.get("Year")) && ObjectUtil.isNotEmpty(params.get("Year"))) {
            year = params.get("Year").toString();
            queryWrapper.eq("z.year_num", year);
        }

        if (ObjectUtil.isNotNull(params.get("Status")) && ObjectUtil.isNotEmpty(params.get("Status"))) {
            status = params.get("Status").toString();
            queryWrapper.eq("z.status", status);
        }

        if (ObjectUtil.isNotNull(params.get("fundType")) && ObjectUtil.isNotEmpty(params.get("fundType"))) {
            fundType = params.get("fundType").toString();
            // 1：省科技经费支持   0：省科技经费未支持
            if (fundType.equals("1")) {
                queryWrapper.gt("t.provincialfund", 0);
            } else {
                queryWrapper.eq("t.provincialfund", 0);
            }
        }

        if (ObjectUtil.isNotNull(params.get("projectstage")) && ObjectUtil.isNotEmpty(params.get("projectstage"))) {
            projectstage = params.get("projectstage").toString();
            queryWrapper.eq("z.projectstage", projectstage);
        }

        // 过滤掉待上报的数据
        queryWrapper.ne("z.status",0);

        queryWrapper.orderByDesc("z.applydate");
        queryWrapper.orderByDesc("z.firstdate");


        // assType不为空则按类型查询，否则查询所有
        if (ObjectUtil.isNotNull(params.get("assType")) && ObjectUtil.isNotEmpty(params.get("assType"))) {
            assType = params.get("assType").toString();
            if (assType.equals("1")) {
                queryWrapper.eq("z.sciencetype", "ScienceTech");
            } else {
                queryWrapper.eq("z.sciencetype", "ScienceSoft");
            }
        }
        IPage<ZgsSciencetechfeasible> pageList = null;
        if (StrUtil.isNotBlank(newFieldStr)) {
             pageList = zgsSciencetechfeasibleService.listZgsSciencetechfeasibleListForExport(newFieldStr,queryWrapper, 1, 99999);
        } else {
             pageList = zgsSciencetechfeasibleService.listZgsSciencetechfeasibleList(queryWrapper, pageNo, pageSize);
        }

        // List<ZgsSciencetechfeasible> list = zgsSciencetechfeasibleService.listZgsSciencetechfeasibleList(queryWrapper);
        /*if (pageList.size() > 0) {
            map.put("total",list.size());
            // map.put("list",dealPaging(list,pageNo,pageSize));
            map.put("list",list);
            map.put("msg","查询成功");
            result.setResult(map);
        } else {
            map.put("total",0);
            map.put("list",list);
            map.put("msg","暂无数据");
            result.setResult(map);
        }*/
        return Result.ok(pageList);
    }


    @RequestMapping(value = "/dataSummaryExport")
    public ModelAndView exportXls (@RequestParam Map < String, Object > params,HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();


        Result<?> result = queryScientificProjectList(params,req);
        IPage<ZgsSciencetechfeasible> pageList = (IPage<ZgsSciencetechfeasible>) result.getResult();
        List<ZgsSciencetechfeasible> list = pageList.getRecords();
        List<ZgsSciencetechfeasible> exportList = list;


        String title = "科研项目汇总信息";
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsSciencetechfeasible.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        if (ObjectUtil.isNotEmpty(params.get("fieldArray"))) {
            mv.addObject(NormalExcelConstants.EXPORT_FIELDS, params.get("fieldArray").toString());
        }
        // mv.addObject(NormalExcelConstants.EXPORT_FIELDS, "projectname,projectnum");

        return mv;
    }




    /**
     * @describe: 集合分页
     * @author: renxiaoliang
     * @date: 2023/6/21 14:17
     */
    public  List<ZgsSciencetechfeasible> dealPaging(List<ZgsSciencetechfeasible> f, int pageNo, int dataSize) {
        /*
         * 经过测试发现当pageNo为0或者小于时，也就是第0页时，程序会报错，所以需要处理一下pageNo的值
         *
         * 先进行空值的判断，避免程序出现null异常
         *
         * 当pageNo的值小于等于0时，我们让它的值为1
         */
        //参数的校验
        if (f == null) {//当传入过来的list集合为null时，先进行实例化
            f = new ArrayList<ZgsSciencetechfeasible>();
        }
        if ((Object) pageNo == null) {//当传入过来的pageNo为null时，先进行赋值操作
            pageNo = 1;
        }
        if ((Object) dataSize == null) {//当传入过来的dataSize为null时，先进行赋值操作
            dataSize = 1;
        }
        if (pageNo <= 0) {
            pageNo = 1;
        }

        //记录一下数据一共有多少条
        int totalitems = f.size();
        //实例化一个接受分页处理之后的数据
        List<ZgsSciencetechfeasible> afterList = new ArrayList<ZgsSciencetechfeasible>();
        /*
         * 进行分页处理,采用for循环的方式来进行处理
         *
         * 首先for循环中，i应该从哪里开始:i应该从 (当前是第几页 -1 乘以 条数) 开始
         *
         * 然后for循环应该到哪里结束，也就是i应该小于:判断(开始的索引+显示条数)是不是大于总条数，如果大于就是总条数，如果小于就是(开始的索引+显示条数)
         *
         * 然后让i++
         */

        for(int i = (pageNo - 1) * dataSize;
            i < (((pageNo - 1) * dataSize) + dataSize >
                    totalitems ? totalitems : ((pageNo - 1) * dataSize) + dataSize);
            i++) {
            //然后将数据存入afterList中

            afterList.add(f.get(i));
        }
        //然后将处理后的数据集合进行返回
        return afterList;
    }


    /**
     * @describe: 省厅角色下授权编辑按钮状态修改
     * @author: renxiaoliang
     * @date: 2023/11/15 16:08
     */
    @AutoLog(value = "授权编辑—状态修改")
    @ApiOperation(value = "授权编辑—状态修改", notes = "授权编辑—状态修改")
    @PostMapping(value = "/accreditUpdate")
    public Result<?> accreditUpdate(@RequestBody Map<String,Object> params) {
        Result result = new Result();

        String id = params.get("id").toString();
        String sqbj = params.get("sqbj").toString();  // 授权编辑  1 同意编辑  2 取消编辑
        String updateSave = params.get("updateSave").toString();  // 编辑保存  1 修改未提交  2 修改已提交

        if (StrUtil.isNotBlank(id) && StrUtil.isNotBlank(sqbj) && StrUtil.isNotBlank(updateSave)) {
            ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
            zgsSciencetechfeasible.setId(id);
            zgsSciencetechfeasible.setSqbj(sqbj);
            zgsSciencetechfeasible.setUpdateSave(updateSave);
            if (zgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible)){
                result.setMessage("授权编辑成功");
                result.setCode(1);
                result.setSuccess(true);
            } else {
                result.setMessage("授权编辑失败");
                result.setCode(0);
                result.setSuccess(false);
            }
        } else {
            result.setMessage("授权编辑失败");
            result.setCode(0);
            result.setSuccess(false);
        }
        return result;
    }

}
