package org.jeecg.modules.green.worktable.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.service.IZgsAttachappendixService;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchPo;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTable;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTableTjPro;
import org.jeecg.modules.green.worktable.service.IZgsWorkTableService;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.jeecg.common.util.DateUtils.yyy;

/**
 * @Description: 工作台
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Api(tags = "工作台")
@RestController
@RequestMapping("/worktable/index")
@Slf4j
public class ZgsWorkTableController extends JeecgController<ZgsWorkTable, IZgsWorkTableService> {
    @Autowired
    private IZgsWorkTableService zgsWorkTableService;

    /**
     * 个人/单位工作台查询
     *
     * @param req
     * @return
     */
    @AutoLog(value = "个人/单位工作台查询")
    @ApiOperation(value = "个人/单位工作台查询", notes = "个人/单位工作台查询")
    @GetMapping(value = "/personalWorkTableMap")
    public Result<?> personalWorkTableMap(HttpServletRequest req) {
        return Result.OK(zgsWorkTableService.getPersonalWorkTableMap());
    }

    /**
     * 推荐单位工作台查询
     *
     * @param req
     * @return
     */
    @AutoLog(value = "推荐单位工作台查询")
    @ApiOperation(value = "推荐单位工作台查询", notes = "推荐单位工作台查询")
    @GetMapping(value = "/unitWorkTableMap")
    public Result<?> unitWorkTableMap(HttpServletRequest req) {
        return Result.OK(zgsWorkTableService.getUnitWorkTableMap());
    }

    /**
     * 科教处工作台查询
     *
     * @param year
     * @return
     */
    @AutoLog(value = "科教处工作台查询")
    @ApiOperation(value = "科教处工作台查询", notes = "科教处工作台查询")
    @GetMapping(value = "/govWorkTableMap")
    public Result<?> govWorkTableMap(@RequestParam(name = "year", required = false) Integer year) {
        return Result.OK(zgsWorkTableService.getGovWorkTableMap(year));
    }

    /**
     * 专家工作台查询
     *
     * @param req
     * @return
     */
    @AutoLog(value = "专家工作台查询")
    @ApiOperation(value = "专家工作台查询", notes = "专家工作台查询")
    @GetMapping(value = "/expertWorkTableMap")
    public Result<?> expertWorkTableMap(HttpServletRequest req) {
        return Result.OK(zgsWorkTableService.getExpertWorkTableMap());
    }

    /**
     * 导出excel
     *
     * @param year
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(@RequestParam(name = "year", required = false) Integer year) {
        String title = "工作台项目申报统计";
        List<ZgsWorkTableTjPro> list = zgsWorkTableService.getGovWorkTableCenterList(year);
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsWorkTableTjPro.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    /**
     * @Name ：ZgsWorkTableController
     * @Description ：<节能绿色建筑项目数据看板的数据>
     * @Author ：Haozhiyang
     * @Date ：2022/8/26 10:41
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "节能绿色建筑项目数据看板的数据")
    @ApiOperation(value = "节能绿色建筑项目数据看板的数据", notes = "节能绿色建筑项目数据看板的数据")
    @RequestMapping(value = "/energySavingGreenBuildingProjectData")
    public Result<?> energySavingGreenBuildingProjectData(@RequestParam(name = "year", required = false) String year, @RequestParam(name = "areacode", required = false) String areacode) {
        if (year == null) {
            year = yyy();
        }
        CityAndStateWorkbenchPo cityAndStateWorkbenchPo = zgsWorkTableService.energySavingGreenBuildingProjectData(year, areacode);
        if (cityAndStateWorkbenchPo == null) {
            CityAndStateWorkbenchPo cityAndStateWorkbenchPoNew = new CityAndStateWorkbenchPo();
            cityAndStateWorkbenchPoNew.setBuildCount("0");
            cityAndStateWorkbenchPoNew.setCompletedCount("0");
            cityAndStateWorkbenchPoNew.setExistinfoCount("0");
            cityAndStateWorkbenchPoNew.setPrefabricatedCount("0");
            return Result.OK(cityAndStateWorkbenchPoNew);
        }
        return Result.OK(cityAndStateWorkbenchPo);
    }

    @AutoLog(value = "区县清单")
    @ApiOperation(value = "区县清单", notes = "区县清单")
    @RequestMapping(value = "/listOfDistrictsAndCounties")
    public Result<?> listOfDistrictsAndCounties(@RequestParam(name = "year", required = false) String year, @RequestParam("areacode") String areacode) {
        return Result.OK(zgsWorkTableService.listOfDistrictsAndCounties(year, areacode));
    }
}
