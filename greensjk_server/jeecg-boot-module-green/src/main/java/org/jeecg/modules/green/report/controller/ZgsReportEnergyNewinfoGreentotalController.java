package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hpsf.Decimal;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoGreentotalService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoTotalService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.entity.enmus.ExcelType;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 绿色建筑汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Api(tags = "绿色建筑汇总表")
@RestController
@RequestMapping("/report/zgsReportEnergyNewinfoGreentotal")
@Slf4j
public class ZgsReportEnergyNewinfoGreentotalController extends JeecgController<ZgsReportEnergyNewinfoGreentotal, IZgsReportEnergyNewinfoGreentotalService> {
    @Autowired
    private IZgsReportEnergyNewinfoTotalService zgsReportEnergyNewinfoTotalService;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService zgsReportEnergyNewinfoGreentotalService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsReportEnergyNewinfoService zgsReportEnergyNewinfoService;

    /**
     * 分页列表查询
     *
     * @param zgsReportEnergyNewinfoGreentotal
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-分页列表查询")
    @ApiOperation(value = "绿色建筑汇总表-分页列表查询", notes = "绿色建筑汇总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "99") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportEnergyNewinfoGreentotal != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoGreentotal.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyNewinfoGreentotal.getFilltm());
            }
            // 原有业务逻辑暂存  rxl 20230720
            if (zgsReportEnergyNewinfoGreentotal.getApplystate() != null) {
                if (zgsReportEnergyNewinfoGreentotal.getApplystate().intValue() != -1) {  //如果选择查询状态 则按查询状态进行查询（选择待审核状态：查询待审核数据；   选择上报通过状态：查询已审核通过的数据）  rxl 20230720
                    queryWrapper.eq("applystate", zgsReportEnergyNewinfoGreentotal.getApplystate());
                    applystate = zgsReportEnergyNewinfoGreentotal.getApplystate().intValue();
                }
            }
            // 变更后的查询逻辑  -1=查全部；   1=已上报；  2=上报通过
            /*if (zgsReportEnergyNewinfoGreentotal.getApplystate() != null) {
                if (zgsReportEnergyNewinfoGreentotal.getApplystate().intValue() != -1) {  //如果选择查询状态 则按查询状态进行查询（选择待审核状态：查询待审核数据；   选择上报通过状态：查询已审核通过的数据）  rxl 20230720
                    queryWrapper.eq("applystate", zgsReportEnergyNewinfoGreentotal.getApplystate());
                    applystate = zgsReportEnergyNewinfoGreentotal.getApplystate().intValue();
                } else if (zgsReportEnergyNewinfoGreentotal.getApplystate().intValue() == -1) {
                    // -1 查询全部，如果为省厅角色查询已上报、上报通过数据，如果是区县角色查询已上报、上报通过、退回数据
                    if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {  // 省市角色
                        queryWrapper.eq("applystate", GlobalConstants.apply_state1).or().eq("applystate",GlobalConstants.apply_state2);
                    } else {  // 区县角色
                        queryWrapper.ne("applystate",GlobalConstants.apply_state1);
                    }
                }
            } else { // 未选择查询状态则查询全部数据，省市角色查询待初审及审核通过数据，(如果查询全部，则判断省市角色下可看到待上报、上报通过数据)
                if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
                    queryWrapper.eq("applystate", GlobalConstants.apply_state1).or().eq("applystate",GlobalConstants.apply_state2);
                } else { // 区县角色查询全部上报数据可查看到 待上报、上报通过、退回数据
                    queryWrapper.ne("applystate",GlobalConstants.apply_state1);
                }
            }*/
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到   原有业务逻辑暂不使用    rxl 20230721
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                if (zgsReportEnergyNewinfoGreentotal.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportEnergyNewinfoGreentotal.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
                } else if (zgsReportEnergyNewinfoGreentotal.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfoGreentotal.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                if (zgsReportEnergyNewinfoGreentotal.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfoGreentotal.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportEnergyNewinfoGreentotal.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.isNull("area_type");
                queryWrapper.orderByAsc("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByAsc("areacode");
        Page<ZgsReportEnergyNewinfoGreentotal> page = new Page<ZgsReportEnergyNewinfoGreentotal>(pageNo, 99);
        IPage<ZgsReportEnergyNewinfoGreentotal> pageList = zgsReportEnergyNewinfoGreentotalService.page(page, queryWrapper);

        ZgsReportEnergyNewinfoGreentotal total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                if (zgsReportEnergyNewinfoGreentotal.getAreacode().length() == 4 || zgsReportEnergyNewinfoGreentotal.getAreacode().length() == 6) {
                    total = zgsReportEnergyNewinfoGreentotalService.lastGreenTotalDataCity(zgsReportEnergyNewinfoGreentotal.getFilltm(), zgsReportEnergyNewinfoGreentotal.getAreacode(), applystate);
                } else {
                    total = zgsReportEnergyNewinfoGreentotalService.lastGreenTotalDataProvince(zgsReportEnergyNewinfoGreentotal.getFilltm(), zgsReportEnergyNewinfoGreentotal.getAreacode(), applystate);
                }
            } else {
                total = zgsReportEnergyNewinfoGreentotalService.lastGreenTotalDataProvince(zgsReportEnergyNewinfoGreentotal.getFilltm(), zgsReportEnergyNewinfoGreentotal.getAreacode(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportEnergyNewinfoGreentotal> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("filltm", zgsReportEnergyNewinfoGreentotal.getFilltm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            ZgsReportEnergyNewinfoGreentotal totalCity = zgsReportEnergyNewinfoGreentotalService.getOne(totalQueryWrapper);
            total = zgsReportEnergyNewinfoGreentotalService.lastGreenTotalDataCity(zgsReportEnergyNewinfoGreentotal.getFilltm(), sysUser.getAreacode(), applystate);
            if (total != null) {
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (pageList.getRecords().size() > 0) {
            // 绿色建筑竣工面积占新建建筑竣工面积比重 规则修改  updater：rxl  20230413
            List<ZgsReportEnergyNewinfoGreentotal>  records = pageList.getRecords();
            BigDecimal valueSec = new BigDecimal(100);
            // 重新计算本月绿色建筑竣工汇总面积，本月新建建筑竣工汇总面积、年度累计绿色建筑竣工汇总面积、年度累计新建建筑竣工汇总面积
            BigDecimal localMonthTotalGreenArea = new BigDecimal(0.00);
            BigDecimal localMonthTotalNewBuildArea = new BigDecimal(0.00);
            BigDecimal annualNewBuildTotalGreenArea = new BigDecimal(0.00);
            BigDecimal annualNewBuildTotalArea = new BigDecimal(0.00);

            for (ZgsReportEnergyNewinfoGreentotal greenEntity:records
                 ) {
                // 年度累计绿色建筑竣工面积减去绿色建筑工业面积得到 居住建筑面积与公共建筑面积之和
                BigDecimal yearAreanForLiveAndDustry = greenEntity.getYearArea().subtract(greenEntity.getYearBuildindustryArea());
                // 重新计算年度累计绿色建筑竣工汇总面积
                annualNewBuildTotalGreenArea = annualNewBuildTotalGreenArea.add(yearAreanForLiveAndDustry);
                // 更新年度绿色建筑竣工面积
                greenEntity.setYearArea(yearAreanForLiveAndDustry);

                // 年度累计新建建筑去掉工业建筑面积
                BigDecimal cummulitiveNewBuildArean = new BigDecimal(0.00);
                BigDecimal comulativeNewBuildArea = greenEntity.getYearNewEndArea();  // 年度累计新建建筑竣面积
                ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
                zgsReportEnergyNewinfoTotal.setFilltm(zgsReportEnergyNewinfoGreentotal.getFilltm());
                zgsReportEnergyNewinfoTotal.setAreacode(greenEntity.getAreacode());
                zgsReportEnergyNewinfoTotal.setApplystate(zgsReportEnergyNewinfoGreentotal.getApplystate());
                ZgsReportEnergyNewinfoTotal entity = null;
                // 如果是省厅角色则查询年度累计新建建筑竣工 工业面积
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                    // 省厅角色未选择查询区域则统计当前市、区下的所有累计面积；  如果是市、区角色，汇总数据只查询市及区县各自的数据，不做数据汇总
                    if (StringUtils.isNotBlank(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                        zgsReportEnergyNewinfoTotal.setQybm("2");  // 精准查询
                    } else {
                        zgsReportEnergyNewinfoTotal.setQybm("1");  // 模糊查询
                    }
                    entity = queryNewBuildDustruyArea(zgsReportEnergyNewinfoTotal);  // 查询累计新建建筑竣工 工业面积
                    // 省厅角色下：重新计算年度累计新建建筑面（之前取的year_new_end_area数据有问题）
                    if (StringUtils.isBlank(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                        ZgsReportEnergyNewinfoTotal  YearNewBuildArea = zgsReportEnergyNewinfoTotalService.getYearNewBuildArea(zgsReportEnergyNewinfoGreentotal.getFilltm(),greenEntity.getAreacode());
                        // yearNewBuildEntity不为空时，获取年度累计新建建筑面积
                        if (ObjectUtil.isNotNull(YearNewBuildArea) && ObjectUtil.isNotNull(YearNewBuildArea.getYearBuildindustryArea())) {
                            // 对绿色建筑列表上查询出的年度累计新建建筑面积重新进行赋值计算
                            comulativeNewBuildArea = YearNewBuildArea.getYearBuildArea();
                        }
                    }
                    if (entity != null) {
                        BigDecimal YearBuildindustryArea = entity.getYearBuildindustryArea();  // 年度累计新建建筑竣工工业面积  5.28
                        cummulitiveNewBuildArean = comulativeNewBuildArea.subtract(YearBuildindustryArea);
                    }
                } else { // 否则只查询当前所在市区县数据
                    // 如果是市、区、县角色登录，则只查询当前各自的数据，总览数据不做汇总
                    zgsReportEnergyNewinfoTotal.setQybm("2");  // 精准查询
                    entity = queryNewBuildDustruyArea(zgsReportEnergyNewinfoTotal);  // 查询累计新建建筑竣工 工业面积
                    if (entity != null) {
                        BigDecimal YearBuildindustryArea = entity.getYearBuildindustryArea();  // 年度累计新建建筑竣工工业面积
                        cummulitiveNewBuildArean = comulativeNewBuildArea.subtract(YearBuildindustryArea);
                    }
                   /* List<ZgsReportEnergyNewinfoTotal> list = getAreaByCode(zgsReportEnergyNewinfoTotal);
                    if (list.size() > 0) {
                        cummulitiveNewBuildArean = list.get(0).getYearBuildhouseArea().add(list.get(0).getYearBuildpublicArea());
                    }*/
                }

                // 重新计算年度累计新建建筑竣工汇总面积
                annualNewBuildTotalArea = annualNewBuildTotalArea.add(cummulitiveNewBuildArean);
                // 更新年度累计新建建筑竣工面积（已去除工业面积）
                greenEntity.setYearNewEndArea(cummulitiveNewBuildArean);
                if (cummulitiveNewBuildArean.compareTo(BigDecimal.ZERO) == 0) {
                    greenEntity.setYearZb(cummulitiveNewBuildArean);
                } else {
                    BigDecimal divide = yearAreanForLiveAndDustry.divide(cummulitiveNewBuildArean , 4, RoundingMode.HALF_UP);
                    BigDecimal multiplyVal = divide.multiply(valueSec).setScale(2);
                    greenEntity.setYearZb(multiplyVal);
                }

                /* 获取当前查询月份之前的所有 年度累计绿色工业建筑竣工面积之和
                ZgsReportEnergyNewinfoTotal eneryEntity = zgsReportEnergyNewinfoTotalService.queryEnergNewInfoList(greenEntity.getAreacode(),greenEntity.getFilltm());
                if (eneryEntity != null) {
                    BigDecimal yearEneryAreanForLiveAndDustry = eneryEntity.getYearBuildArea().subtract(eneryEntity.getYearBuildindustryArea());
                    if (yearEneryAreanForLiveAndDustry.compareTo(BigDecimal.ZERO) == 0) {
                        greenEntity.setYearZb(yearEneryAreanForLiveAndDustry);
                    } else {

                        // 重新计算  本月绿色建筑竣工面积占新建建筑竣工面积比重(去除工业面积)
                        BigDecimal divide = yearAreanForLiveAndDustry.divide(yearEneryAreanForLiveAndDustry, 4, RoundingMode.HALF_UP);
                        BigDecimal multiplyVal = divide.multiply(valueSec).setScale(2);
                        greenEntity.setYearZb(multiplyVal);
                    }
                } else {
                    greenEntity.setYearZb(new BigDecimal(0.00));
                }*/

                // 1 重新计算  本月绿色建筑竣工面积(去除掉工业建筑面积)
                ZgsReportEnergyNewinfo zgsReportEnergyNewinfo = new ZgsReportEnergyNewinfo();
                zgsReportEnergyNewinfo.setFilltm(greenEntity.getFilltm());
                zgsReportEnergyNewinfo.setAreacode(greenEntity.getAreacode());
                if (StringUtils.isNotBlank(zgsReportEnergyNewinfoGreentotal.getAreacode())) {
                    zgsReportEnergyNewinfo.setQybm("2");  // 按区域编码查询
                } else {
                    zgsReportEnergyNewinfo.setQybm("1");  // 模糊查询全部
                }
                BigDecimal greenArea = BigDecimal.valueOf(0);
                BigDecimal newBuildArea = BigDecimal.valueOf(0);
                List<ZgsReportEnergyNewinfo> greenBuildDustryArea = queryBuildAreaOfMonth(zgsReportEnergyNewinfo,2);
                if (greenBuildDustryArea.size() > 0) {
                    // 本月绿色建筑面积(工业+居住+公共) 0.5 减去其中的工业面积  1.7
                    BigDecimal monthGreenArea = greenEntity.getMonthArea();
                    if (monthGreenArea.compareTo(BigDecimal.ZERO) == 0) {
                        greenEntity.setMonthArea(greenArea);
                    } else {
                        greenArea = greenEntity.getMonthArea().subtract(greenBuildDustryArea.get(0).getBuildingArea());
                        greenEntity.setMonthArea(greenArea);
                    }
                    // 重新计算本月绿色建筑累计竣工面积
                    localMonthTotalGreenArea = localMonthTotalGreenArea.add(greenArea);

                }
                // 2 重新计算  本月新建建筑竣工面积(去除掉工业建筑面积)
                    List<ZgsReportEnergyNewinfo> newBuildDustryArea = queryBuildAreaOfMonth(zgsReportEnergyNewinfo,1);
                if (newBuildDustryArea.size() > 0) {
                    // 本月新建建筑面积(工业+居住+公共) 减去其中的工业面积
                    BigDecimal  monthNewArea = greenEntity.getMonthNewEndArea();
                    if (monthNewArea.compareTo(BigDecimal.ZERO) == 0) {
                        greenEntity.setMonthNewEndArea(newBuildArea);
                    } else {
                        newBuildArea = greenEntity.getMonthNewEndArea().subtract(newBuildDustryArea.get(0).getBuildingArea());
                        greenEntity.setMonthNewEndArea(newBuildArea);
                    }


                    // 重新计算本月新建建筑竣工累计面积
                    localMonthTotalNewBuildArea = localMonthTotalNewBuildArea.add(newBuildArea);


                    if (newBuildArea.compareTo(BigDecimal.ZERO) == 0) {
                        greenEntity.setMonthZb(newBuildArea);
                    } else {
                        BigDecimal divide = greenArea.divide(newBuildArea, 4, RoundingMode.HALF_UP);
                        BigDecimal multiplyVal = divide.multiply(valueSec).setScale(2);
                        greenEntity.setMonthZb(multiplyVal);

                    }
                } else {
                    greenEntity.setMonthZb(newBuildArea);
                }

                // 3 重新计算  本月绿色建筑竣工面积占新建建筑竣工面积比重
                /*BigDecimal monthNewEndArea = greenEntity.getMonthNewEndArea();
                if (monthNewEndArea.compareTo(BigDecimal.ZERO) == 0) {
                    greenEntity.setMonthZb(monthNewEndArea);
                } else {
                    BigDecimal divide = greenArea.divide(monthNewEndArea, 4, RoundingMode.HALF_UP);
                    BigDecimal multiplyVal = divide.multiply(valueSec).setScale(2);
                    greenEntity.setMonthZb(multiplyVal);
                }*/
            }

            if (total != null) {
                // 对汇总数据重新进行赋值更新
                total.setMonthArea(localMonthTotalGreenArea);
                total.setMonthNewEndArea(localMonthTotalNewBuildArea);
                if (localMonthTotalNewBuildArea.compareTo(BigDecimal.ZERO) == 0) {
                    total.setMonthZb(localMonthTotalNewBuildArea);
                } else {
                    total.setMonthZb(localMonthTotalGreenArea.divide(localMonthTotalNewBuildArea, 4, RoundingMode.HALF_UP).multiply(valueSec).setScale(2));
                }


                total.setYearArea(annualNewBuildTotalGreenArea);
                total.setYearNewEndArea(annualNewBuildTotalArea);
                if (annualNewBuildTotalArea.compareTo(BigDecimal.ZERO) == 0) {
                    total.setYearZb(annualNewBuildTotalArea);
                } else {
                    total.setYearZb(annualNewBuildTotalGreenArea.divide(annualNewBuildTotalArea, 4, RoundingMode.HALF_UP).multiply(valueSec).setScale(2));
                }
                pageList.getRecords().add(total);
            }
        }
        if (pageList.getRecords().size() > 1) {
            pageList.getRecords().get(pageList.getRecords().size() - 1).setTotalTag("1");
            pageList.getRecords().get(pageList.getRecords().size() - 1).setAreaname("合计");
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                pageList.getRecords().get(pageList.getRecords().size() - 1).setApplystate(null);
            }
        }
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    // add 计算月度 绿色建筑 工业面积  rxl  accessType:  1 新建建筑；  2 绿色建筑；
     private List<ZgsReportEnergyNewinfo> queryBuildAreaOfMonth(ZgsReportEnergyNewinfo zgsReportEnergyNewinfo,
                                   @RequestParam(name = "accessType") Integer accessType) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (accessType != null && accessType == 2) {
            queryWrapper.eq("is_no", "1");
        }
        if (zgsReportEnergyNewinfo != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getProjectname())) {
                queryWrapper.like("projectname", zgsReportEnergyNewinfo.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyNewinfo.getFilltm());
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省    .or().eq("applystate", GlobalConstants.apply_state1)  省厅退回工业面积计算
            queryWrapper.eq("applystate", GlobalConstants.apply_state2);
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getAreacode())) {
                if (zgsReportEnergyNewinfo.getAreacode().length() == 4) {
                    if (zgsReportEnergyNewinfo.getQybm().equals("1")) {
                        queryWrapper.likeRight("areacode", zgsReportEnergyNewinfo.getAreacode());
                    } else {
                        queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                    }
                    // queryWrapper.likeRight("areacode", zgsReportEnergyNewinfo.getAreacode());
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportEnergyNewinfo.getAreacode().length() == 6) {
                    if (zgsReportEnergyNewinfo.getQybm().equals("1")) {
                        queryWrapper.likeRight("areacode", zgsReportEnergyNewinfo.getAreacode());
                    } else {
                        queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                    }
                } else {
//                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
//                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getAreacode())) {
                if (zgsReportEnergyNewinfo.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                } else {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfo.getAreacode());
                }
            } else {
                queryWrapper.eq("areacode", sysUser.getAreacode());
                //再加上县区已审核通过的
                queryWrapper.or(w1 -> {
                    if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getFilltm())) {
                        w1.eq("filltm", zgsReportEnergyNewinfo.getFilltm());
                    }
                    if (StringUtils.isNotEmpty(zgsReportEnergyNewinfo.getProjectname())) {
                        w1.like("projectname", zgsReportEnergyNewinfo.getProjectname());
                    }
                    if (accessType != null && accessType == 2) {
                        w1.eq("is_no", "1");
                    }
                    w1.likeRight("areacode", sysUser.getAreacode());
                    w1.and(w2 -> {
                        w2.eq("applystate", GlobalConstants.apply_state2).or().eq("applystate", GlobalConstants.apply_state1);
                        w2.last(" and length(areacode)=6");
                    });
                });
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.eq("build_type",3);  // build_type=3  工业建筑面积
        queryWrapper.ne("applystate",'0');
         queryWrapper.ne("applystate",'3');
        queryWrapper.select(
                "IFNULL(SUM(building_area),0) buildingArea"
            );
        queryWrapper.orderByAsc("areacode");

        return zgsReportEnergyNewinfoService.list(queryWrapper);
    }


    // add 计算新建建筑累计竣工 工业面积
    private ZgsReportEnergyNewinfoTotal queryNewBuildDustruyArea(ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportEnergyNewinfoTotal != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyNewinfoTotal.getFilltm());
            }
            if (zgsReportEnergyNewinfoTotal.getApplystate() != null) {
                if (zgsReportEnergyNewinfoTotal.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportEnergyNewinfoTotal.getApplystate());
                    applystate = zgsReportEnergyNewinfoTotal.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }

        // if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getAreacode())) {
                if (zgsReportEnergyNewinfoTotal.getAreacode().length() == 4) {
                    if (zgsReportEnergyNewinfoTotal.getQybm().equals("1")) {
                        queryWrapper.likeRight("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                        // queryWrapper.eq("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                    } else {
                        queryWrapper.eq("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                    }
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
                } else if (zgsReportEnergyNewinfoTotal.getAreacode().length() == 6) {
                    if (zgsReportEnergyNewinfoTotal.getQybm().equals("1")) {
                        queryWrapper.likeRight("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                    } else {
                        queryWrapper.eq("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                    }
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
            }
        // }
        queryWrapper.orderByAsc("areacode");
        ZgsReportEnergyNewinfoTotal total = null;
        //获取合计数据
        // if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getAreacode())) {
                if (zgsReportEnergyNewinfoTotal.getAreacode().length() == 4 || zgsReportEnergyNewinfoTotal.getAreacode().length() == 6) {
                    if (zgsReportEnergyNewinfoTotal.getQybm().equals("1")) {
                        total = zgsReportEnergyNewinfoTotalService.lastTotalDataCity(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
                        // total = zgsReportEnergyNewinfoTotalService.getAreadByCityCode(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
                    } else {
                        total = zgsReportEnergyNewinfoTotalService.getAreadByCityCode(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
                    }
                } else {
                    if (zgsReportEnergyNewinfoTotal.getQybm().equals("1")) {
                        total = zgsReportEnergyNewinfoTotalService.lastTotalDataProvince(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
                    } else {
                        total = zgsReportEnergyNewinfoTotalService.lastTotalDataProvinceByCityCode(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
                    }
                }
            } else {
                total = zgsReportEnergyNewinfoTotalService.lastTotalDataProvince(zgsReportEnergyNewinfoTotal.getFilltm(), zgsReportEnergyNewinfoTotal.getAreacode(), applystate);
            }
       // }
        /*else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportEnergyNewinfoTotal> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("filltm", zgsReportEnergyNewinfoTotal.getFilltm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            ZgsReportEnergyNewinfoTotal totalCity = zgsReportEnergyNewinfoTotalService.getOne(totalQueryWrapper);
            total = zgsReportEnergyNewinfoTotalService.lastTotalDataCity(zgsReportEnergyNewinfoTotal.getFilltm(), sysUser.getAreacode(), applystate);
            if (total != null) {
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                if (totalCity.getApplystate() != null) {
                    total.setApplystate(totalCity.getApplystate());
                } else {
                    total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                }
                total.setBackreason(totalCity.getBackreason());
            }
        }*/

        return total;
    }



    // add 单独计算每个areaCode下的 新建建筑竣工面积
    private List<ZgsReportEnergyNewinfoTotal> getAreaByCode(ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportEnergyNewinfoTotal != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyNewinfoTotal.getFilltm());
            }
            if (zgsReportEnergyNewinfoTotal.getApplystate() != null) {
                if (zgsReportEnergyNewinfoTotal.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportEnergyNewinfoTotal.getApplystate());
                    applystate = zgsReportEnergyNewinfoTotal.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }

        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getAreacode())) {
                if (zgsReportEnergyNewinfoTotal.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                    queryWrapper.isNull("area_type");
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportEnergyNewinfoTotal.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                }
            } else {
                queryWrapper.isNotNull("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyNewinfoTotal.getAreacode())) {
                queryWrapper.eq("areacode", zgsReportEnergyNewinfoTotal.getAreacode());
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByDesc("id");
        queryWrapper.last("limit 1");
        List<ZgsReportEnergyNewinfoTotal> ZgsReportEnergyNewinfoList = zgsReportEnergyNewinfoTotalService.list(queryWrapper);
        return ZgsReportEnergyNewinfoList;
    }





    /**
     * 绿色建筑汇总表-审批
     *
     * @param id
     * @param spStatus
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-审批")
    @ApiOperation(value = "绿色建筑汇总表-审批", notes = "绿色建筑汇总表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestParam(name = "id") String id,
                               @RequestParam(name = "spStatus") Integer spStatus,
                               @RequestParam(name = "backreason", required = false) String backreason) {
        zgsReportEnergyNewinfoGreentotalService.spProject(id, spStatus, backreason);
        return Result.OK("操作成功!");
    }

    /**
     * 绿色建筑汇总表-本月一键上报（市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-本月一键上报（市州）")
    @ApiOperation(value = "绿色建筑汇总表-本月一键上报（市州）", notes = "绿色建筑汇总表-本月一键上报（市州）")
    @GetMapping(value = "/initMonthNewInfoTotal")
    public Result<?> initMonthNewInfoTotal(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "Green" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> queryGreenWrapper = new QueryWrapper();
        queryGreenWrapper.eq("filltm", filltm);
        queryGreenWrapper.likeRight("areacode", sysUser.getAreacode());
        queryGreenWrapper.isNull("area_type");
        queryGreenWrapper.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportEnergyNewinfoGreentotal> listGreen = zgsReportEnergyNewinfoGreentotalService.list(queryGreenWrapper);
        if (listGreen.size() > 0) {
            return Result.error("请先处理绿色建筑汇总待审批记录！");
        }
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryWrapper = new QueryWrapper();
        queryWrapper.eq("filltm", filltm);
        queryWrapper.likeRight("areacode", sysUser.getAreacode());
        queryWrapper.isNull("area_type");
        queryWrapper.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportEnergyNewinfoTotal> list = zgsReportEnergyNewinfoTotalService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("请先处理新建建筑节能情况汇总待审批记录！");
        }
        //再判断本月是否已上报
        QueryWrapper<ZgsReportEnergyNewinfoGreentotal> wrapper = new QueryWrapper();
        wrapper.eq("filltm", filltm);
        wrapper.eq("areacode", sysUser.getAreacode());
        wrapper.eq("applystate", GlobalConstants.apply_state2);
        wrapper.isNotNull("area_type");
        List<ZgsReportEnergyNewinfoGreentotal> listTotal = zgsReportEnergyNewinfoGreentotalService.list(wrapper);
        if (listTotal.size() > 0) {
            return Result.error("本月汇总数据已上报！");
        }
//        zgsReportEnergyNewinfoGreentotalService.initEnergyNewinfoGreenTotalCity(year, quarter, filltm);
        zgsReportEnergyNewinfoGreentotalService.initEnergyNewinfoGreenTotalCityAll(year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser, 0, GlobalConstants.apply_state1);
        zgsReportEnergyNewinfoTotalService.initEnergyNewinfoTotalCityAll(year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser, 0, GlobalConstants.apply_state1);
        return Result.OK("本月汇总一键上报成功！");
    }

    /**
     * 添加
     *
     * @param zgsReportEnergyNewinfoGreentotal
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-添加")
    @ApiOperation(value = "绿色建筑汇总表-添加", notes = "绿色建筑汇总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal) {
        zgsReportEnergyNewinfoGreentotalService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoGreentotal, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportEnergyNewinfoGreentotal
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-编辑")
    @ApiOperation(value = "绿色建筑汇总表-编辑", notes = "绿色建筑汇总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal) {
        zgsReportEnergyNewinfoGreentotalService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoGreentotal, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-通过id删除")
    @ApiOperation(value = "绿色建筑汇总表-通过id删除", notes = "绿色建筑汇总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportEnergyNewinfoGreentotalService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-批量删除")
    @ApiOperation(value = "绿色建筑汇总表-批量删除", notes = "绿色建筑汇总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportEnergyNewinfoGreentotalService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绿色建筑汇总表-通过id查询")
    @ApiOperation(value = "绿色建筑汇总表-通过id查询", notes = "绿色建筑汇总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportEnergyNewinfoGreentotalService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportEnergyNewinfoGreentotal == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportEnergyNewinfoGreentotal);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportEnergyNewinfoGreentotal
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsReportEnergyNewinfoGreentotal, 1, 9999, req);
        IPage<ZgsReportEnergyNewinfoGreentotal> pageList = (IPage<ZgsReportEnergyNewinfoGreentotal>) result.getResult();
        List<ZgsReportEnergyNewinfoGreentotal> list = pageList.getRecords();
        List<ZgsReportEnergyNewinfoGreentotal> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "绿色建筑汇总表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportEnergyNewinfoGreentotal.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportEnergyNewinfoGreentotal, ZgsReportEnergyNewinfoGreentotal.class, "绿色建筑汇总表");
        return mv;
    }

    private String getId(ZgsReportEnergyNewinfoGreentotal item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportEnergyNewinfoGreentotal.class);
    }

}
