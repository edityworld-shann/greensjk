package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportProject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;

/**
 * @Description: 绿色建筑和建筑节能示范项目库
 * @Author: jeecg-boot
 * @Date: 2022-08-02
 * @Version: V1.0
 */
public interface IZgsReportProjectService extends IService<ZgsReportProject> {
    int spProject(ZgsReportProject zgsReportProject);
}
