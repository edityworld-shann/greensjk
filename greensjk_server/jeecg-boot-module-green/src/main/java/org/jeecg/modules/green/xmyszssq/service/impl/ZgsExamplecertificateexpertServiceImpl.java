package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificateexpert;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsExamplecertificateexpertMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificateexpertService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 示范验收证书验收专家名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsExamplecertificateexpertServiceImpl extends ServiceImpl<ZgsExamplecertificateexpertMapper, ZgsExamplecertificateexpert> implements IZgsExamplecertificateexpertService {

}
