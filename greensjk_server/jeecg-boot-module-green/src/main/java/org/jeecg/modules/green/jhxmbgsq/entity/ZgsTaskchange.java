package org.jeecg.modules.green.jhxmbgsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 项目变更记录
 * @Author: jeecg-boot
 * @Date: 2022-03-15
 * @Version: V1.0
 */
@Data
@TableName("zgs_taskchange")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_taskchange对象", description = "项目变更记录")
public class ZgsTaskchange implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * mainguid
     */
    @Excel(name = "mainguid", width = 15)
    @ApiModelProperty(value = "mainguid")
    private java.lang.String mainguid;
    /**
     * TechTask科技任务书Projecttask示范项目任务是
     */
    @Excel(name = "TechTask科技任务书Projecttask示范项目任务是", width = 15)
    @ApiModelProperty(value = "TechTask科技任务书Projecttask示范项目任务是")
    private java.lang.String type;
    /**
     * 变更内容
     */
    @Excel(name = "变更内容", width = 15)
    @ApiModelProperty(value = "变更内容")
    private java.lang.String changecontent;
    /**
     * createaccountname
     */
    @Excel(name = "createaccountname", width = 15)
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
