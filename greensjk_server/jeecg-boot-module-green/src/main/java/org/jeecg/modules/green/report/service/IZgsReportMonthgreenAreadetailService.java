package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenAreadetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: zgs_report_monthgreen_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportMonthgreenAreadetailService extends IService<ZgsReportMonthgreenAreadetail> {

}
