package org.jeecg.modules.green.zqcysb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建设科技示范项目实施情况中期查验申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_midterminspection")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_midterminspection对象", description = "建设科技示范项目实施情况中期查验申请业务主表")
public class ZgsMidterminspection implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 建筑工程项目库ID
     */
//    @Excel(name = "建筑工程项目库ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "建筑工程项目库ID")
    private java.lang.String projectlibraryguid;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "工程项目名称")
    private java.lang.String projectname;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 执行单位
     */
    //@Excel(name = "执行单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "执行单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String executiveunit;
    /**
     * 项目地址
     */
    //@Excel(name = "项目地址", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectaddress;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleader;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15, orderNum = "2")
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkphone;
    /**
     * 项目开工时间
     */
    @Excel(name = "项目开工时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目开工时间")
    private java.util.Date projectstartdate;
    /**
     * 项目计划竣工时间
     */
    //@Excel(name = "项目计划竣工时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目计划竣工时间")
    private java.util.Date projectenddate;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 检查内容
     */
    // @Excel(name = "检查内容", width = 15, orderNum = "2")
    @ApiModelProperty(value = "检查内容")
    private java.lang.String inspectioncontent;
    /**
     * 申请单位意见
     */
//    @Excel(name = "申请单位意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "申请单位意见")
    private java.lang.String declaration;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审
     */
//    @Excel(name = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 初审部门
     */
    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15, orderNum = "2")
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15, orderNum = "2")
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 申请查验时间
     */
//    @Excel(name = "申请查验时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "申请查验时间")
    private java.util.Date applycheckdate;
    /**
     * firstperson
     */
//    @Excel(name = "firstperson", width = 15, orderNum = "2")
    @ApiModelProperty(value = "firstperson")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 专家审核时间
     */
//    @Excel(name = "专家审核时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "专家审核时间")
    private java.util.Date expertdate;
    /**
     * 专家审核人
     */
//    @Excel(name = "专家审核人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "专家审核人")
    private java.lang.String expertperson;
    /**
     * 专家审核意见
     */
//    @Excel(name = "专家审核意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "专家审核意见")
    private java.lang.String expertauditopinion;
    /**
     * 专家审核结果
     */
//    @Excel(name = "专家审核结果", width = 15, orderNum = "2")
    @ApiModelProperty(value = "专家审核结果")
    private java.lang.String expertauditresult;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    @TableField(exist = false)
    private Object cstatus;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    // 添加专家信息
    @TableField(exist = false)
    private List<ZgsSacceptcertexpert> zgsSacceptcertexpertList;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
}
