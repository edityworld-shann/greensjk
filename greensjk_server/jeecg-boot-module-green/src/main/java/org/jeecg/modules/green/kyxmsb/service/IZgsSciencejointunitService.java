package org.jeecg.modules.green.kyxmsb.service;

import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface IZgsSciencejointunitService extends IService<ZgsSciencejointunit> {

}
