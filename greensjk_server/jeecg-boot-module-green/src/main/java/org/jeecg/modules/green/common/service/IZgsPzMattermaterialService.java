package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsPzMattermaterial;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 配置库-各类项目工程申报材料配置表
 * @Author: jeecg-boot
 * @Date:   2022-02-09
 * @Version: V1.0
 */
public interface IZgsPzMattermaterialService extends IService<ZgsPzMattermaterial> {

}
