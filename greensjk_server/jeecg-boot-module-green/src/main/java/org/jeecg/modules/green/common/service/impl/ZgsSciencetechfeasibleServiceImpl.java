package org.jeecg.modules.green.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.mapper.ZgsSciencetechfeasibleMapper;
import org.jeecg.modules.green.common.service.IZgsSciencetechfeasibleService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencefundbudgetService;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssembleproject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description: 建设科技攻关项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-10
 * @Version: V1.0
 */
@Service
public class ZgsSciencetechfeasibleServiceImpl extends ServiceImpl<ZgsSciencetechfeasibleMapper, ZgsSciencetechfeasible> implements IZgsSciencetechfeasibleService {

    @Autowired
    private IZgsScienceparticipantService zgsScienceparticipantService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsSciencefundbudgetService zgsSciencefundbudgetService;

    @Override
    public ZgsSciencetechfeasible getBySciencetechguid(String sciencetechguid) {
        ZgsSciencetechfeasible zgsSciencetechfeasible = ValidateEncryptEntityUtil.validateDecryptObject(this.baseMapper.selectById(sciencetechguid), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsSciencetechfeasible != null) {
            String enterpriseguid = zgsSciencetechfeasible.getEnterpriseguid();
            String scienceguid = zgsSciencetechfeasible.getId();
            if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(scienceguid)) {
                //项目组成员
                QueryWrapper<ZgsScienceparticipant> queryWrapper0 = new QueryWrapper<>();
                queryWrapper0.eq("enterpriseguid", enterpriseguid);
                queryWrapper0.eq("sciencebaseguid", scienceguid);
                queryWrapper0.ne("isdelete", 1);
                queryWrapper0.orderByAsc("persontype,ordernum");
                queryWrapper0.last("limit 15");
                List<ZgsScienceparticipant> zgsScienceparticipantList0 = zgsScienceparticipantService.list(queryWrapper0);
                zgsSciencetechfeasible.setZgsScienceparticipantList0(ValidateEncryptEntityUtil.validateDecryptList(zgsScienceparticipantList0, ValidateEncryptEntityUtil.isDecrypt));
                //联合申报单位
                QueryWrapper<ZgsSciencejointunit> queryWrapper3 = new QueryWrapper<>();
                queryWrapper3.eq("enterpriseguid", enterpriseguid);
                queryWrapper3.eq("scienceguid", scienceguid);
                queryWrapper3.isNull("tag");
                queryWrapper3.ne("isdelete", 1);
                List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper3);
                zgsSciencetechfeasible.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitList, ValidateEncryptEntityUtil.isDecrypt));
                //经费预算
                QueryWrapper<ZgsSciencefundbudget> queryWrapper5 = new QueryWrapper<>();
                queryWrapper5.eq("enterpriseguid", enterpriseguid);
                queryWrapper5.eq("sciencebaseguid", scienceguid);
                queryWrapper5.ne("isdelete", 1);
                queryWrapper5.orderByAsc("year");
                List<ZgsSciencefundbudget> zgsSciencefundbudgetList = zgsSciencefundbudgetService.list(queryWrapper5);
                zgsSciencetechfeasible.setZgsSciencefundbudgetList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencefundbudgetList, ValidateEncryptEntityUtil.isDecrypt));
            }
        }
        return zgsSciencetechfeasible;
    }

    @Override
    public Map<String, String> getExportXlsData(String id, Map<String, String> map) {
        QueryWrapper<ZgsSciencejointunit> queryWrapper = new QueryWrapper();
        queryWrapper.eq("scienceguid", id);
        queryWrapper.isNull("tag");
        queryWrapper.ne("isdelete", 1);
        List<ZgsSciencejointunit> buildjointunitList = zgsSciencejointunitService.list(queryWrapper);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < buildjointunitList.size(); i++) {
            if (StringUtils.isNotEmpty(buildjointunitList.get(i).getApplyunit())) {
                sb.append(Sm4Util.decryptEcb(buildjointunitList.get(i).getApplyunit()));
            }
            sb.append("\n");
        }
        map.put("unit", sb.toString());
        QueryWrapper<ZgsSciencefundbudget> queryWrapper6 = new QueryWrapper<>();
        queryWrapper6.eq("sciencebaseguid", id);
        queryWrapper6.ne("isdelete", 1);
        queryWrapper6.select(
                "IFNULL(SUM(provincialfund),0) provincialfund",
                "IFNULL(SUM(unitselffund),0) unitselffund"
        );
        List<ZgsSciencefundbudget> zgsBuildfundbudgetList = zgsSciencefundbudgetService.list(queryWrapper6);
        if (zgsBuildfundbudgetList != null && zgsBuildfundbudgetList.size() == 1) {
            map.put("bk", zgsBuildfundbudgetList.get(0).getProvincialfund().toString());
            map.put("zc", zgsBuildfundbudgetList.get(0).getUnitselffund().toString());
        } else {
            map.put("bk", "0");
            map.put("zc", "0");
        }
        return null;
    }

    @Override
    public int updateAddMoney(ZgsSciencetechfeasible zgsSciencetechfeasible) {
        return this.baseMapper.updateAddMoney(zgsSciencetechfeasible.getId(), zgsSciencetechfeasible.getProvincialfundtotal(), zgsSciencetechfeasible.getProjectnum(),zgsSciencetechfeasible.getFundRemark());
    }

    @Override
    public int rollBackSp(String id) {
        return this.baseMapper.rollBackSp(id);
    }

    /*@Override
    public List<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(Wrapper<ZgsSciencetechfeasible> queryWrapper) {
        List<ZgsSciencetechfeasible> listInfo = this.baseMapper.listZgsSciencetechfeasibleList(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }*/

    @Override
    public Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(Wrapper<ZgsSciencetechfeasible> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsSciencetechfeasible> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsSciencetechfeasible> listInfoPage = this.baseMapper.listZgsSciencetechfeasibleList(pageMode, queryWrapper);
        List<ZgsSciencetechfeasible> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleListForExport(String fieldStr, Wrapper<ZgsSciencetechfeasible> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsSciencetechfeasible> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsSciencetechfeasible> listInfoPage = this.baseMapper.listZgsSciencetechfeasibleListForExport(fieldStr, pageMode, queryWrapper);
        List<ZgsSciencetechfeasible> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }




}
