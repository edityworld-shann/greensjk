package org.jeecg.modules.green.report.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewdetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_buildingenergyinfonewdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_buildingenergyinfonewdetail")
@RestController
@RequestMapping("/report/zgsReportBuildingenergyinfonewdetail")
@Slf4j
public class ZgsReportBuildingenergyinfonewdetailController extends JeecgController<ZgsReportBuildingenergyinfonewdetail, IZgsReportBuildingenergyinfonewdetailService> {
	@Autowired
	private IZgsReportBuildingenergyinfonewdetailService zgsReportBuildingenergyinfonewdetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportBuildingenergyinfonewdetail
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-分页列表查询")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-分页列表查询", notes="zgs_report_buildingenergyinfonewdetail-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportBuildingenergyinfonewdetail zgsReportBuildingenergyinfonewdetail,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportBuildingenergyinfonewdetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportBuildingenergyinfonewdetail, req.getParameterMap());
		Page<ZgsReportBuildingenergyinfonewdetail> page = new Page<ZgsReportBuildingenergyinfonewdetail>(pageNo, pageSize);
		IPage<ZgsReportBuildingenergyinfonewdetail> pageList = zgsReportBuildingenergyinfonewdetailService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportBuildingenergyinfonewdetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-添加")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-添加", notes="zgs_report_buildingenergyinfonewdetail-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportBuildingenergyinfonewdetail zgsReportBuildingenergyinfonewdetail) {
		zgsReportBuildingenergyinfonewdetailService.save(zgsReportBuildingenergyinfonewdetail);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportBuildingenergyinfonewdetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-编辑")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-编辑", notes="zgs_report_buildingenergyinfonewdetail-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportBuildingenergyinfonewdetail zgsReportBuildingenergyinfonewdetail) {
		zgsReportBuildingenergyinfonewdetailService.updateById(zgsReportBuildingenergyinfonewdetail);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-通过id删除")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-通过id删除", notes="zgs_report_buildingenergyinfonewdetail-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportBuildingenergyinfonewdetailService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-批量删除")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-批量删除", notes="zgs_report_buildingenergyinfonewdetail-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportBuildingenergyinfonewdetailService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonewdetail-通过id查询")
	@ApiOperation(value="zgs_report_buildingenergyinfonewdetail-通过id查询", notes="zgs_report_buildingenergyinfonewdetail-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportBuildingenergyinfonewdetail zgsReportBuildingenergyinfonewdetail = zgsReportBuildingenergyinfonewdetailService.getById(id);
		if(zgsReportBuildingenergyinfonewdetail==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportBuildingenergyinfonewdetail);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportBuildingenergyinfonewdetail
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportBuildingenergyinfonewdetail zgsReportBuildingenergyinfonewdetail) {
        return super.exportXls(request, zgsReportBuildingenergyinfonewdetail, ZgsReportBuildingenergyinfonewdetail.class, "zgs_report_buildingenergyinfonewdetail");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportBuildingenergyinfonewdetail.class);
    }

}
