package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsBuildjointunit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 示范工程联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsBuildjointunitService extends IService<ZgsBuildjointunit> {

}
