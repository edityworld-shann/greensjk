package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 账号短信下发表
 * @Author: jeecg-boot
 * @Date: 2022-05-06
 * @Version: V1.0
 */
@Data
@TableName("zgs_projectunitmembersend")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectunitmembersend对象", description = "账号短信下发表")
public class ZgsProjectunitmembersend implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 登录名
     */
    @Excel(name = "登录名", width = 15)
    @ApiModelProperty(value = "登录名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String loginname;
    /**
     * 登录密码
     */
    @Excel(name = "登录密码", width = 15)
    @ApiModelProperty(value = "登录密码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String loginpassword;
    /**
     * 项目单位名称/人员姓名（帐号类型为个人时填写）
     */
    @Excel(name = "项目单位名称/人员姓名（帐号类型为个人时填写）", width = 15)
    @ApiModelProperty(value = "项目单位名称/人员姓名（帐号类型为个人时填写）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String enterprisename;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 单位
     */
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunit;
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitprojectleader;
    /**
     * 手机
     */
    @Excel(name = "手机", width = 15)
    @ApiModelProperty(value = "手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitphone;
    /**
     * 类型
     */
    @Excel(name = "类型", width = 15)
    @ApiModelProperty(value = "类型")
    private java.lang.String type;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15)
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 状态
     */
    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private java.lang.String isflag;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    @ApiModelProperty(value = "模板ID")
    private java.lang.String templateid;
}
