package org.jeecg.modules.green.jxzpsq.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.mapper.ZgsPerformancebaseMapper;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 绩效自评申请
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsPerformancebaseServiceImpl extends ServiceImpl<ZgsPerformancebaseMapper, ZgsPerformancebase> implements IZgsPerformancebaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private RedisUtil redisUtil;

    @Override
    public int spProject(ZgsPerformancebase zgsPerformancebase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPerformancebase != null && StringUtils.isNotEmpty(zgsPerformancebase.getId())) {
            ZgsPerformancebase performancebase = new ZgsPerformancebase();
            performancebase.setId(zgsPerformancebase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsPerformancebase.getId());
            zgsReturnrecord.setReturnreason(zgsPerformancebase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE17);
            ZgsPerformancebase baseInfo = getById(zgsPerformancebase.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsPerformancebase.getSpStatus() == 1) {
                //通过记录状态==0
                performancebase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPerformancebase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPerformancebase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPerformancebase.getStatus())) {
                    performancebase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    performancebase.setFirstdate(new Date());
                    performancebase.setFirstperson(sysUser.getRealname());
                    performancebase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    performancebase.setStatus(GlobalConstants.SHENHE_STATUS8);
                    performancebase.setFinishperson(sysUser.getRealname());
                    performancebase.setFinishdate(new Date());
                    performancebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                performancebase.setAuditopinion(zgsPerformancebase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsPerformancebase.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsPerformancebase.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsPerformancebase.getStatus())) {
                    if (zgsPerformancebase.getSpStatus() == 2) {
                        performancebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        performancebase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    performancebase.setFirstdate(new Date());
                    performancebase.setFirstperson(sysUser.getRealname());
                    performancebase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsPerformancebase.getSpStatus() == 2) {
                        performancebase.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        performancebase.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    performancebase.setFinishperson(sysUser.getRealname());
                    performancebase.setFinishdate(new Date());
                    performancebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsPerformancebase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        performancebase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getBaseguid(), baseInfo.getEnterpriseguid(), zgsPerformancebase.getAuditopinion());
                    }
                }
                if (zgsPerformancebase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsPerformancebase.getAuditopinion())) {
                        zgsPerformancebase.setAuditopinion(" ");
                    }
                    performancebase.setAuditopinion(zgsPerformancebase.getAuditopinion());
                    if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
                        performancebase.setReturntype(new BigDecimal(2));
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                    } else {
                        //退回操作
                        performancebase.setReturntype(new BigDecimal(4));
                        zgsReturnrecord.setReturntype(new BigDecimal(4));
                    }
                } else if (zgsPerformancebase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    performancebase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getBaseguid());
            if ("0".equals(baseInfo.getIntype())) {
                zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
            } else {
                zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6_1));
            }
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(performancebase, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }

    @Override
    public String zgsInfoOne(String baseguid) {
        if (StrUtil.isNotBlank(this.baseMapper.zgsInfoOne(baseguid))) {
            return Sm4Util.decryptEcb(this.baseMapper.zgsInfoOne(baseguid));
        }
        return "";
    }

    @Override
    public Page<ZgsPerformancebase> listZgsPerformancebase(Wrapper<ZgsPerformancebase> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsPerformancebase> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsPerformancebase> listInfoPage = this.baseMapper.listZgsPerformancebase(pageMode, queryWrapper);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }


    @Override
    public List<ZgsPerformancebase> listZgsPerformancebaseForJxzp(Wrapper<ZgsPerformancebase> queryWrapper) {
        return baseMapper.listZgsPerformancebaseForJxzp(queryWrapper);
    }

}
