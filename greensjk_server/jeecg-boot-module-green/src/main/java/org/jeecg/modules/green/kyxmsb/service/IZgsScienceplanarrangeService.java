package org.jeecg.modules.green.kyxmsb.service;

import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface IZgsScienceplanarrangeService extends IService<ZgsScienceplanarrange> {

}
