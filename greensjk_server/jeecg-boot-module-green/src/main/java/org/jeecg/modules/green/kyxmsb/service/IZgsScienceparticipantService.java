package org.jeecg.modules.green.kyxmsb.service;

import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface IZgsScienceparticipantService extends IService<ZgsScienceparticipant> {

}
