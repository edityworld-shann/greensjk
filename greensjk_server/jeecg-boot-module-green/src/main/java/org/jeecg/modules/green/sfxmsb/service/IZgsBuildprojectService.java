package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsBuildproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsBuildprojectService extends IService<ZgsBuildproject> {

}
