package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreadetail;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabrAreadetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreadetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabr_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabrAreadetailServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreadetailMapper, ZgsReportMonthfabrAreadetail> implements IZgsReportMonthfabrAreadetailService {

}
