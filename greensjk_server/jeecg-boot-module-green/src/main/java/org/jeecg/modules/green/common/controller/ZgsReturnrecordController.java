package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 退回记录信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Api(tags = "退回记录信息表")
@RestController
@RequestMapping("/common/zgsReturnrecord")
@Slf4j
public class ZgsReturnrecordController extends JeecgController<ZgsReturnrecord, IZgsReturnrecordService> {
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    /**
     * 分页列表查询
     *
     * @param zgsReturnrecord
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "退回记录信息表-分页列表查询")
    @ApiOperation(value = "退回记录信息表-分页列表查询", notes = "退回记录信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReturnrecord zgsReturnrecord,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsReturnrecord> queryWrapper = QueryGenerator.initQueryWrapper(zgsReturnrecord, req.getParameterMap());
        Page<ZgsReturnrecord> page = new Page<ZgsReturnrecord>(pageNo, pageSize);
        IPage<ZgsReturnrecord> pageList = zgsReturnrecordService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 分页列表查询
     *
     * @param bid
     * @return
     */
    @AutoLog(value = "退回记录信息表-分页列表查询")
    @ApiOperation(value = "退回记录信息表-分页列表查询", notes = "退回记录信息表-分页列表查询")
    @GetMapping(value = "/recordList")
    public Result<?> recordList(@RequestParam(name = "pid", required = false) String pid, @RequestParam(name = "bid", required = false) String bid) {
        QueryWrapper<ZgsReturnrecord> queryWrapper = new QueryWrapper();
        if (StringUtils.isNotEmpty(pid)) {
            queryWrapper.eq("projectlibraryguid", pid);
        } else {
            queryWrapper.eq("businessguid", bid);
        }
        queryWrapper.ne("isdelete", 1);
        queryWrapper.orderByDesc("createtime");
        List<ZgsReturnrecord> pageList = zgsReturnrecordService.list(queryWrapper);
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptList(pageList, ValidateEncryptEntityUtil.isDecrypt));
    }

    /**
     * 添加
     *
     * @param zgsReturnrecord
     * @return
     */
    @AutoLog(value = "退回记录信息表-添加")
    @ApiOperation(value = "退回记录信息表-添加", notes = "退回记录信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReturnrecord zgsReturnrecord) {
        zgsReturnrecordService.save(zgsReturnrecord);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReturnrecord
     * @return
     */
    @AutoLog(value = "退回记录信息表-编辑")
    @ApiOperation(value = "退回记录信息表-编辑", notes = "退回记录信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReturnrecord zgsReturnrecord) {
        zgsReturnrecordService.updateById(zgsReturnrecord);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "退回记录信息表-通过id删除")
    @ApiOperation(value = "退回记录信息表-通过id删除", notes = "退回记录信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReturnrecordService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "退回记录信息表-批量删除")
    @ApiOperation(value = "退回记录信息表-批量删除", notes = "退回记录信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReturnrecordService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "退回记录信息表-通过id查询")
    @ApiOperation(value = "退回记录信息表-通过id查询", notes = "退回记录信息表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReturnrecord zgsReturnrecord = zgsReturnrecordService.getById(id);
        if (zgsReturnrecord == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReturnrecord);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsReturnrecord
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReturnrecord zgsReturnrecord) {
        return super.exportXls(request, zgsReturnrecord, ZgsReturnrecord.class, "退回记录信息表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReturnrecord.class);
    }

}
