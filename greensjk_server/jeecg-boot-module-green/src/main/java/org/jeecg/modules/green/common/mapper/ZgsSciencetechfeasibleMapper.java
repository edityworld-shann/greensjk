package org.jeecg.modules.green.common.mapper;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 建设科技攻关项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-10
 * @Version: V1.0
 */
public interface ZgsSciencetechfeasibleMapper extends BaseMapper<ZgsSciencetechfeasible> {

    int updateAddMoney(@Param("id") String id, @Param("provincialfundtotal") BigDecimal provincialfundtotal, @Param("projectnum") String projectnum,@Param("fundRemark") String fundRemark);

    int rollBackSp(@Param("id") String id);

    // List<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(@Param(Constants.WRAPPER) Wrapper<ZgsSciencetechfeasible> queryWrapper);

    Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleList(Page<ZgsSciencetechfeasible> page, @Param(Constants.WRAPPER) Wrapper<ZgsSciencetechfeasible> queryWrapper);

    Page<ZgsSciencetechfeasible> listZgsSciencetechfeasibleListForExport(@Param("fieldStr") String fieldStr, Page<ZgsSciencetechfeasible> page, @Param(Constants.WRAPPER) Wrapper<ZgsSciencetechfeasible> queryWrapper);


}
