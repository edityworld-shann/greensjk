package org.jeecg.modules.green.worktable.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchDetailsPo;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchPo;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTable;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTableTjPro;

import java.util.List;
import java.util.Map;

/**
 * @Description: 工作台
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface ZgsWorkTableMapper extends BaseMapper<ZgsWorkTable> {
    List<Integer> getPersonalWorkTableCount(@Param("enterpriseguid") String enterpriseguid);

    List<Integer> getUnitWorkTableListZero(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getUnitWorkTableListZeroA(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getUnitWorkTableListZeroB(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getUnitWorkTableListZeroC(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getUnitWorkTableListZeroD(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getUnitWorkTableList1(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    List<Integer> getGovWorkTableHeaderList(@Param("year") Integer year);

    List<ZgsWorkTableTjPro> getGovWorkTableCenterList(@Param("year") Integer year);

    int getExpertWorkTableLeftList(@Param(Constants.WRAPPER) Wrapper<ZgsWorkTable> queryWrapper);

    CityAndStateWorkbenchPo energySavingGreenBuildingProjectData(@Param("year") String year, @Param("areacode") String areacode);

    CityAndStateWorkbenchDetailsPo energyNewinfo(@Param("year") String year, @Param("areacode") String areacode);

    CityAndStateWorkbenchDetailsPo energyExistinfo(@Param("year") String year, @Param("areacode") String areacode);

    CityAndStateWorkbenchDetailsPo newinfoGreentotal(@Param("year") String year, @Param("areacode") String areacode);

    CityAndStateWorkbenchDetailsPo completed(@Param("year") String year, @Param("areacode") String areacode);

    CityAndStateWorkbenchDetailsPo productioncapacity(@Param("year") String year, @Param("areacode") String areacode);
}
