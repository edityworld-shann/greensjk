package org.jeecg.modules.utils;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/12/2615:25
 */
@Data
public class SendSmsInfo {
    //多个手机号码，使用逗号隔开
    private String phoneNumber;
    //多个名称，使用逗号隔开
    private String companyName;
    //多个内容
//    private Content fillContent;
    //模板编号
    private String templateId;
    //区分类型：==1zgs_projectunitmembersend循环执行，否则使用默认传递模板
    private Integer type = 0;
    private Integer smsType = 3;
    private Map<String, Object> fillContent;

//    @Data
//    public static class Content implements Serializable {
//        private String content1;
//        private String content2;
//        private String content3;
//        private String month;
//        private String day;
//        @JsonProperty("QQNum")
//        private String QQNum;
//    }
}
