package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class City implements Serializable {
    private String code;
    private String name;
}
