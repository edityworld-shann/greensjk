package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @Name ：MoonGreenBuildingPo
 * @Description ：<按照月进行返回的>
 * @Author ：Haozhiyang
 * @Date ：2022/8/22 14:48
 * @Version ：1.0
 * @History ：<修改代码时说明>
 */
@Data
public class MoonGreenBuildingPo implements Serializable {
    private String areaname;

    //住宅
    private String yearBuildhouseNumber;
    private String yearBuildhouseArea;
    //工业
    private String yearBuildindustryNumber;
    private String yearBuildindustryArea;
    //建筑
    private String yearBuildpublicNumber;
    private String yearBuildpublicArea;
}
