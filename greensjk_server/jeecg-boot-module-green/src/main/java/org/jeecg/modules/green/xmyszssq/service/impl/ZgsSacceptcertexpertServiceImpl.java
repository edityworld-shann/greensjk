package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsSacceptcertexpertMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研验收证书验收专家名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSacceptcertexpertServiceImpl extends ServiceImpl<ZgsSacceptcertexpertMapper, ZgsSacceptcertexpert> implements IZgsSacceptcertexpertService {

}
