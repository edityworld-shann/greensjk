package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import org.jeecg.modules.green.report.mapper.ZgsReportBuildingenergyinfonewdetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewdetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_buildingenergyinfonewdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportBuildingenergyinfonewdetailServiceImpl extends ServiceImpl<ZgsReportBuildingenergyinfonewdetailMapper, ZgsReportBuildingenergyinfonewdetail> implements IZgsReportBuildingenergyinfonewdetailService {

}
