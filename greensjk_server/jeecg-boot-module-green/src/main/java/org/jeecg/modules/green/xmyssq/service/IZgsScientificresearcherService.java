package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsScientificresearcherService extends IService<ZgsScientificresearcher> {

}
