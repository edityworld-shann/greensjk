package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 建设科技示范项目验收申请业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface ZgsDemoprojectacceptanceMapper extends BaseMapper<ZgsDemoprojectacceptance> {

}
