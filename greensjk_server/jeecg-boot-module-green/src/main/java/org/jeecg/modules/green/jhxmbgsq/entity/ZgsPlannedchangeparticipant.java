package org.jeecg.modules.green.jhxmbgsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 计划变更项目组成员表
 * @Author: jeecg-boot
 * @Date: 2022-07-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_plannedchangeparticipant")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_plannedchangeparticipant对象", description = "计划变更项目组成员表")
public class ZgsPlannedchangeparticipant implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 对应任务书表主键
     */
    @Excel(name = "对应任务书表主键", width = 15)
    @ApiModelProperty(value = "对应任务书表主键")
    private java.lang.String taskguid;
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personname;
    /**
     * 职务
     */
    @Excel(name = "职务", width = 15)
    @ApiModelProperty(value = "职务")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personduty;
    /**
     * 职称
     */
    @Excel(name = "职称", width = 15)
    @ApiModelProperty(value = "职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String personpost;
    /**
     * 在本课题承担的任务
     */
    @Excel(name = "在本课题承担的任务", width = 15)
    @ApiModelProperty(value = "在本课题承担的任务")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String workcontent;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete;
    /**
     * 年龄
     */
    @Excel(name = "年龄", width = 15)
    @ApiModelProperty(value = "年龄")
    private java.math.BigDecimal age;
    /**
     * 单位名称
     */
    @Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unit;
    /**
     * 0：负责人 1：基本参加人员
     */
    @Excel(name = "0：负责人 1：基本参加人员", width = 15)
    @ApiModelProperty(value = "0：负责人 1：基本参加人员")
    private java.math.BigDecimal persontype;
    /**
     * 排序号
     */
    @Excel(name = "排序号", width = 15)
    @ApiModelProperty(value = "排序号")
    private java.math.BigDecimal ordernum;
    /**
     * 性别
     */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String sex;
    /**
     * 专业
     */
    @Excel(name = "专业", width = 15)
    @ApiModelProperty(value = "专业")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String professional;
    /**
     * 身份证号码
     */
    @Excel(name = "身份证号码", width = 15)
    @ApiModelProperty(value = "身份证号码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String idcard;
    /**
     * 业务主表ID
     */
    @Excel(name = "业务主表ID", width = 15)
    @ApiModelProperty(value = "业务主表ID")
    private java.lang.String baseguid;
    /**
     * 文化程度
     */
    @Excel(name = "文化程度", width = 15)
    @ApiModelProperty(value = "文化程度")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String standardOfCulture;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
