package org.jeecg.modules.green.common.service.impl;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.mapper.ZgsProjectunitmemberMapper;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.utils.SMSTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

/**
 * @Description: 项目单位注册信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Service
@Slf4j
public class ZgsProjectunitmemberServiceImpl extends ServiceImpl<ZgsProjectunitmemberMapper, ZgsProjectunitmember> implements IZgsProjectunitmemberService {

    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private SMSTokenUtil smsTokenUtil;

    @Override
    public int updateUserRegistStatus(ZgsProjectunitmember zgsProjectunitmember) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectunitmember != null && StringUtils.isNotEmpty(zgsProjectunitmember.getId())) {
            ZgsProjectunitmember member = ValidateEncryptEntityUtil.validateDecryptObject(this.baseMapper.selectById(zgsProjectunitmember.getId()), ValidateEncryptEntityUtil.isDecrypt);
            ZgsProjectunitmember projectunitmember = new ZgsProjectunitmember();
            projectunitmember.setId(zgsProjectunitmember.getId());
            projectunitmember.setAuditopinion(zgsProjectunitmember.getAuditopinion());
            projectunitmember.setAuditdate(new Date());
            projectunitmember.setAuditer(sysUser.getUsername());
            HashMap<String, Object> fillContent = new HashMap<>();
            //
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsProjectunitmember.getId());
            zgsReturnrecord.setReturnreason(zgsProjectunitmember.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE27);
            zgsReturnrecord.setEnterpriseguid(zgsProjectunitmember.getEnterpriseguid());
            zgsReturnrecord.setEnterprisename(zgsProjectunitmember.getEnterprisename());
            boolean flag = false;
            if (zgsProjectunitmember.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                //通过
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectunitmember.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectunitmember.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectunitmember.getStatus())) {
                    //初审
                    projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS4);
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    //形审
                    flag = true;
                    projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS8);
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //通过审批新注册用户
                    //修改注册用户或单位基本信息表
                    this.baseMapper.updateUserRegistStatus(zgsProjectunitmember.getUserid());
                    fillContent.put("operation", "通过");
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectunitmember.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectunitmember.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectunitmember.getStatus())) {
                    if (zgsProjectunitmember.getSpStatus() == 2) {
                        projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS3);
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                    } else {
                        projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                    if (member.getAuditdate() == null) {
                        flag = true;
                        if (zgsProjectunitmember.getSpStatus() == 2) {
                            //初审首次补正修改可再次注册
                            this.baseMapper.updateUserSpDelete(zgsProjectunitmember.getUserid());
                        } else {
                            //初审首次驳回直接加入黑名单不让再注册了
                            this.baseMapper.updateUserBlackStatus(zgsProjectunitmember.getUserid());
                        }
                        fillContent.put("operation", "不通过");
                    }
                } else {
                    flag = true;
                    if (zgsProjectunitmember.getSpStatus() == 2) {
                        projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        //补正修改可再次注册
                        this.baseMapper.updateUserSpDelete(zgsProjectunitmember.getUserid());
                    } else {
                        projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS14);
                        //驳回直接加入黑名单不让再注册了
                        this.baseMapper.updateUserBlackStatus(zgsProjectunitmember.getUserid());
                    }
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    fillContent.put("operation", "不通过");
                }
            }
            if (flag) {
                HashMap<String, Object> paramsMap = new HashMap<>();
                paramsMap.put("phoneNumber", member.getMobilephone());
                paramsMap.put("smsType", 3);
                paramsMap.put("companyName", "");
                paramsMap.put("templateId", 15);
                paramsMap.put("fillContent", fillContent);
                Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                log.info(result.toString() + result.getMessage());
            }
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(projectunitmember, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }

    @Override
    public int updateUserRegistStatusByAdmin(ZgsProjectunitmember zgsProjectunitmember) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectunitmember != null && StringUtils.isNotEmpty(zgsProjectunitmember.getId())) {
            ZgsProjectunitmember member = this.baseMapper.selectById(zgsProjectunitmember.getId());
            ZgsProjectunitmember projectunitmember = new ZgsProjectunitmember();
            projectunitmember.setId(zgsProjectunitmember.getId());
            projectunitmember.setAuditdate(new Date());
            projectunitmember.setAuditer(sysUser.getUsername());
            HashMap<String, Object> fillContent = new HashMap<>();
            //
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsProjectunitmember.getId());
            zgsReturnrecord.setReturnreason("管理员审批通过！");
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE27);
            zgsReturnrecord.setEnterpriseguid(zgsProjectunitmember.getEnterpriseguid());
            zgsReturnrecord.setEnterprisename(zgsProjectunitmember.getEnterprisename());
            //通过记录状态==0
            zgsReturnrecord.setReturntype(new BigDecimal(0));
            projectunitmember.setStatus(GlobalConstants.SHENHE_STATUS8);
            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
            //通过审批新注册用户
            //修改注册用户或单位基本信息表
            this.baseMapper.updateUserRegistStatus(zgsProjectunitmember.getUserid());
            fillContent.put("operation", "通过");
            HashMap<String, Object> paramsMap = new HashMap<>();
            paramsMap.put("phoneNumber", member.getMobilephone());
            paramsMap.put("smsType", 3);
            paramsMap.put("companyName", "");
            paramsMap.put("templateId", 15);
            paramsMap.put("fillContent", fillContent);
            Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
            log.info(result.toString() + result.getMessage());
            zgsReturnrecordService.save(zgsReturnrecord);
            return this.baseMapper.updateById(projectunitmember);
        }
        return 0;
    }
}
