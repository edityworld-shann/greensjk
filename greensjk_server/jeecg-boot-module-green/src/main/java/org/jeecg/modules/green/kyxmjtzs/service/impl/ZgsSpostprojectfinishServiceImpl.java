package org.jeecg.modules.green.kyxmjtzs.service.impl;

import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.kyxmjtzs.mapper.ZgsSpostprojectfinishMapper;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostprojectfinishService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSpostprojectfinishServiceImpl extends ServiceImpl<ZgsSpostprojectfinishMapper, ZgsSpostprojectfinish> implements IZgsSpostprojectfinishService {

}
