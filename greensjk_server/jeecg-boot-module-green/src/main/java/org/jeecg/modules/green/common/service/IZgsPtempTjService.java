package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsPtempTj;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 统计报表区县市州用户添加账号匹配表
 * @Author: jeecg-boot
 * @Date:   2022-06-01
 * @Version: V1.0
 */
public interface IZgsPtempTjService extends IService<ZgsPtempTj> {

}
