package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertaward;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertawardMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertawardService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库专家获奖情况
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertawardServiceImpl extends ServiceImpl<ZgsExpertawardMapper, ZgsExpertaward> implements IZgsExpertawardService {

}
