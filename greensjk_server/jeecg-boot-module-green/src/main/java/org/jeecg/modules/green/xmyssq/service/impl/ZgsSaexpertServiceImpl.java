package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import org.jeecg.modules.green.xmyssq.mapper.ZgsSaexpertMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsSaexpertService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 项目验收专家审批记录
 * @Author: jeecg-boot
 * @Date: 2022-03-15
 * @Version: V1.0
 */
@Service
public class ZgsSaexpertServiceImpl extends ServiceImpl<ZgsSaexpertMapper, ZgsSaexpert> implements IZgsSaexpertService {

    @Override
    public List<ZgsSaexpert> getZgsSaexpertList(String businessguid) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.getZgsSaexpertList(businessguid), ValidateEncryptEntityUtil.isDecrypt);
    }
}
