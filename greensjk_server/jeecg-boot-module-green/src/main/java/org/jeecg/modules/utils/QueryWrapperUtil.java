package org.jeecg.modules.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;

import java.io.Serializable;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/4/240:24
 */
public class QueryWrapperUtil<T> {
    //0待上报、1待审核
    //4初审通过、3初审退回、13初审驳回
    //8形审通过、9形审退回、14形审驳回
    //2终审通过、5终审退回、15终审驳回
    //16省厅终止、17历史数据

    /**
     * 状态筛选公共方法
     *
     * @param queryWrapper
     * @param status
     * @param sysUser
     * @param type
     */
    public static <T> void initCommonQueryWrapper(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3); 
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }

    //示范工程
//    {label:"待补充",value:-1},
//    {label:"未审核",value:0},
//    {label:"已审核",value:1},
//    {label:"已退回",value:2},
//    {label:"不立项",value:3},
//    {label:"历史记录",value:4},
    public static void initLibrarySelect(QueryWrapper<ZgsProjectlibrary> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                        queryWrapper.isNull("b.agreeproject");
                        break;
                    case 4://推荐
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
                queryWrapper.eq("b.agreeproject", GlobalConstants.SHENHE_STATUS1);
            } else if ("4".equals(status)) {
                queryWrapper.eq("b.ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //示范工程任务书
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initLibraryTaskSelect(QueryWrapper<ZgsProjecttask> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("t.ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科技攻关
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
//    {label:"不立项",value:3},
    public static void initSciencetechfeasibleSelect(QueryWrapper<ZgsSciencetechfeasible> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                        queryWrapper.isNull("agreeproject");
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
                queryWrapper.eq("agreeproject", GlobalConstants.SHENHE_STATUS1);
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科技攻关任务书
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initSciencetechfeasibleTaskSelect(QueryWrapper<ZgsSciencetechtask> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //中期查验
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initMidterminspectionSelect(QueryWrapper<ZgsMidterminspection> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //变更
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initPlannedSelect(QueryWrapper<ZgsPlannedprojectchange> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //示范验收
    //项目结题或验收阶段
//    projectFinishStatus:[
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
//    {label:"专家评审",value:3},],
    public static void initDemoprojectacceptanceSelect(QueryWrapper<ZgsDemoprojectacceptance> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科技验收
    //项目结题或验收阶段
//    projectFinishStatus:[
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
//    {label:"专家评审",value:3},],
    public static void initScientificbaseSelect(QueryWrapper<ZgsScientificbase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科技验收结题
    //项目结题或验收阶段
//    projectFinishStatus:[
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
//    {label:"专家评审",value:3},],
    public static void initScientificpostbaseSelect(QueryWrapper<ZgsScientificpostbase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科研项目验收证书
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initSacceptcertificateSelect(QueryWrapper<ZgsSacceptcertificatebase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //科研项目结题证书
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initSpostcertificateSelect(QueryWrapper<ZgsSpostcertificatebase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //示范项目验收证书
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initExamplecertificatebaseSelect(QueryWrapper<ZgsExamplecertificatebase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //绩效
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initPerformancebaseSelect(QueryWrapper<ZgsPerformancebase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    //成果
//    {label:"未审核",value:0},
//    {label:"已审核",value: 1},
//    {label:"已退回",value:2},
    public static void initPlanresultSelect(QueryWrapper<ZgsPlanresultbase> queryWrapper, String status, LoginUser sysUser) {
        if (StringUtils.isNotEmpty(status)) {
            if ("0".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                        break;
                    case 3://省
                        queryWrapper.eq("l.status", GlobalConstants.SHENHE_STATUS4);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                        break;
                }
            } else if ("1".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_Y_2_4_8);
                        break;
                    case 3://省
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_Y_2_8_12);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_YC_2_4_8);
                        break;
                }

            } else if ("2".equals(status)) {
                switch (sysUser.getLoginUserType()) {
                    case 0://单位
                    case 1://个人
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_T_3_5_9_13_14_15);
                        break;
                    case 3://省
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_T_5_15);
                        break;
                    case 4://推荐
                        queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_SB_TC_5_9_14_15);
                        break;
                }
            } else if ("3".equals(status)) {
            } else if ("4".equals(status)) {
                queryWrapper.eq("l.ishistory", 1);
            } else if ("-1".equals(status)) {
                queryWrapper.inSql("l.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
            }
        }
    }

    /**
     * 筛选框-项目阶段
     *
     * @param projectstage
     * @return
     */
    public static String getProjectstage(String projectstage) {
        String pStage = null;
        if (GlobalConstants.PROJECT_STAGE_1.equals(projectstage)) {
            pStage = "申报阶段";
        } else if (GlobalConstants.PROJECT_STAGE_2.equals(projectstage)) {
            pStage = "任务书阶段";
        } else if (GlobalConstants.PROJECT_STAGE_3.equals(projectstage)) {
            pStage = "中期查验阶段";
        } else if (GlobalConstants.PROJECT_STAGE_41.equals(projectstage)) {
            pStage = "示范项目验收";
        } else if (GlobalConstants.PROJECT_STAGE_42.equals(projectstage)) {
            pStage = "科研项目验收";
        } else if (GlobalConstants.PROJECT_STAGE_43.equals(projectstage)) {
            pStage = "科研项目结题";
        } else if (GlobalConstants.PROJECT_STAGE_51.equals(projectstage)) {
            pStage = "示范项目验收证书";
        } else if (GlobalConstants.PROJECT_STAGE_52.equals(projectstage)) {
            pStage = "科研项目验收证书";
        } else if (GlobalConstants.PROJECT_STAGE_53.equals(projectstage)) {
            pStage = "科研项目结题证书";
        } else if (GlobalConstants.PROJECT_STAGE_6.equals(projectstage)) {
            pStage = "结题验收绩效自评阶段";
        } else if (GlobalConstants.PROJECT_STAGE_7.equals(projectstage)) {
            pStage = "成果简介阶段";
        } else {
            pStage = "年度绩效自评";
        }
        return pStage;
    }

    public static boolean ifRollBackSp(String status) {
        //2终审通过,5终审退回,15终审驳回,8形审通过,9形审退回,14形审驳回(可撤回标识)
        boolean flag = false;
        if (GlobalConstants.SHENHE_STATUS2.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS5.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS8.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS9.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS12.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS14.equals(status)) {
            flag = true;
        } else if (GlobalConstants.SHENHE_STATUS15.equals(status)) {
            flag = true;
        }
        return flag;
    }



    public static <T> void initCommonQuery(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("p.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
////                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }


    public static <T> void initCommonQueryForDsp(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("p.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
////                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("p.status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }



    public static <T> void initCommonQueryForWorkbench(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
////                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }



    public static <T> void initCommonQueryWrapperForState(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("s.ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
////                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("s.status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }
    // 绩效自评
    public static <T> void initCommonQueryForJxzp(QueryWrapper<T> queryWrapper, String status, LoginUser sysUser, int type) {
        //type=0带前缀b.status(示范项目申报、成果简介)、type=1不带前缀status、type=2带前缀t.status(示范任务书)
        if (StringUtils.isNotEmpty(status)) {
            int position = Integer.parseInt(status);
            if (position == 17) {
                //历史数据
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.ishistory", 1);
                        break;
                    case 1:
                        queryWrapper.eq("ishistory", 1);
                        break;
                    case 2:
                        queryWrapper.eq("t.ishistory", 1);
                        break;
                    case 3:
                        queryWrapper.eq("ishistory", 1);
                        break;
                }
            } else if (position == -1) {
                //个人或单位工作台待补充
                switch (type) {
                    case 0:
                        queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 1:
                        queryWrapper.inSql("p.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 2:
                        queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                    case 3:
                        queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
                        break;
                }
            } else if (position == 0) {
                //初审单位、省厅待审核（工作台直接跳转携带条件筛选）
                switch (type) {
                    case 0:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
//                                queryWrapper.isNull("b.agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("b.agreeproject");
                                        w1.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("b.status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("l.year_num", 2022);
                                        w1.isNull("l.provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("b.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 1:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("p.status", GlobalConstants.SHENHE_STATUS4);
                                // queryWrapper.eq("s.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                // queryWrapper.inSql("s.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                queryWrapper.inSql("p.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 2:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
                                queryWrapper.eq("t.status", GlobalConstants.SHENHE_STATUS4);
                                break;
                            case 4://推荐
                                queryWrapper.inSql("t.status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                        }
                        break;
                    case 3:
                        switch (sysUser.getLoginUserType()) {
                            case 3://省
//                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
////                                queryWrapper.isNull("agreeproject");
                                queryWrapper.and(qw -> {
                                    qw.or(w1 -> {
                                        w1.isNull("agreeproject");
                                        w1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_4_8_12);
                                    });
                                    qw.or(w1 -> {
                                        w1.eq("status", GlobalConstants.SHENHE_STATUS8);
                                        w1.gt("year_num", 2022);
                                        w1.isNull("provincialfundtotal");
                                    });
                                });
                                break;
                            case 4://推荐
                                queryWrapper.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_5_9_13_14_15);
                                break;
                            case 0:
                            case 1:
                                queryWrapper.eq("status", status);
                                break;
                        }
                        break;
                }
            } else {
                //审批状态
                switch (type) {
                    case 0:
                        queryWrapper.eq("b.status", status);
                        break;
                    case 1:
                        queryWrapper.eq("status", status);
                        break;
                    case 2:
                        queryWrapper.eq("t.status", status);
                        break;
                    case 3:
                        queryWrapper.eq("status", status);
                        break;
                }
            }
        }
    }


}
