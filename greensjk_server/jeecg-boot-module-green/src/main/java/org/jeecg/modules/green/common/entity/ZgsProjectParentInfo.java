package org.jeecg.modules.green.common.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 关联项目示范项目和科技项目数据列表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ZgsProjectParentInfo对象", description = "联项目示范项目和科技项目数据列表")
public class ZgsProjectParentInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主表关联主键ID")
    private String id;

    /**
     * 工程项目名称
     */
    @ApiModelProperty(value = "工程项目名称")
    private String projectname;
    @ApiModelProperty(value = "项目类型")
    private String projecttype;
    /**
     * 项目类型:字典DemonstrateType
     */
    @ApiModelProperty(value = "项目类型:字典DemonstrateType")
    private String projecttypenum;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private Date applydate;
    /**
     * 项目编号
     */
    @ApiModelProperty(value = "项目编号")
    private String projectnum;

    /**
     * 是否删除,1删除
     */
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);

    /**
     * 企业ID
     */
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    @ApiModelProperty(value = "专家评审结果 1 ：同意立项 ，0：不同意立项")
    private java.math.BigDecimal agreeproject;
    @ApiModelProperty(value = "任务书类型")
    private String tasktype;
    private Integer bgcount;

    @TableField(exist = false)
    private String glid;

}
