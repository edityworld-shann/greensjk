package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricateArea;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabricateAreaMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateAreaService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabricate_area
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabricateAreaServiceImpl extends ServiceImpl<ZgsReportMonthfabricateAreaMapper, ZgsReportMonthfabricateArea> implements IZgsReportMonthfabricateAreaService {

}
