package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsExpertset;
import org.jeecg.modules.green.common.mapper.ZgsExpertsetMapper;
import org.jeecg.modules.green.common.service.IZgsExpertsetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家评审屏蔽设置表
 * @Author: jeecg-boot
 * @Date:   2022-02-26
 * @Version: V1.0
 */
@Service
public class ZgsExpertsetServiceImpl extends ServiceImpl<ZgsExpertsetMapper, ZgsExpertset> implements IZgsExpertsetService {

}
