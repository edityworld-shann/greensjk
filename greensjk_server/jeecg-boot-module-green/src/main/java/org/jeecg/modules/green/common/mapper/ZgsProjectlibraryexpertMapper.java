package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryexpert;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;

/**
 * @Description: 项目专家关系信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
public interface ZgsProjectlibraryexpertMapper extends BaseMapper<ZgsProjectlibraryexpert> {
    List<ZgsProjectlibraryexpert> getZgsProjectlibraryexpertList(@Param("businessguid") String businessguid);
}
