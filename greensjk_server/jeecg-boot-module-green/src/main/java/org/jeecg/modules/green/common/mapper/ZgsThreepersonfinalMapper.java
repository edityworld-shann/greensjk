package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsThreepersonfinal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 三类人员实时信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface ZgsThreepersonfinalMapper extends BaseMapper<ZgsThreepersonfinal> {

}
