package org.jeecg.modules.green.common.entity;

import lombok.Data;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;

import java.util.List;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/2/2118:08
 */
@Data
public class ZgsProjectSoftExpertinfo {
    private String id;
    private String bid;
    private String set;
    private String projecttypenum;
    private List<ZgsExpertinfo> zgsExpertinfoList;
    private Integer type;
    private Integer fpType;
    private Integer isChouQu;
    private String sfhs;
}
