package org.jeecg.modules.green.kyxmjtzs.service;

import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsSpostprojectfinishService extends IService<ZgsSpostprojectfinish> {

}
