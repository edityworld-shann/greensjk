package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertprojectfinish;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsSacceptcertprojectfinishMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertprojectfinishService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研验收证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSacceptcertprojectfinishServiceImpl extends ServiceImpl<ZgsSacceptcertprojectfinishMapper, ZgsSacceptcertprojectfinish> implements IZgsSacceptcertprojectfinishService {

}
