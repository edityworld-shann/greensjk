package org.jeecg.modules.green.kyxmjtsq.service.impl;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostprojectexecut;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostprojectexecutMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostprojectexecutService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostprojectexecutServiceImpl extends ServiceImpl<ZgsScientificpostprojectexecutMapper, ZgsScientificpostprojectexecut> implements IZgsScientificpostprojectexecutService {

}
