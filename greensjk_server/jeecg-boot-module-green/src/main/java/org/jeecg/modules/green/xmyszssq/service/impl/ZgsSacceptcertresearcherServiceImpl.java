package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsSacceptcertresearcherMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertresearcherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研验收证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSacceptcertresearcherServiceImpl extends ServiceImpl<ZgsSacceptcertresearcherMapper, ZgsSacceptcertresearcher> implements IZgsSacceptcertresearcherService {

}
