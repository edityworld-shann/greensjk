package org.jeecg.modules.green.report.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricate;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricateDetail;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateDetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthfabricate_detail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_monthfabricate_detail")
@RestController
@RequestMapping("/report/zgsReportMonthfabricateDetail")
@Slf4j
public class ZgsReportMonthfabricateDetailController extends JeecgController<ZgsReportMonthfabricateDetail, IZgsReportMonthfabricateDetailService> {
	@Autowired
	private IZgsReportMonthfabricateDetailService zgsReportMonthfabricateDetailService;

	@Autowired
  private IZgsReportMonthfabricateService zgsReportMonthfabricateService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthfabricateDetail
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-分页列表查询")
	@ApiOperation(value="zgs_report_monthfabricate_detail-分页列表查询", notes="zgs_report_monthfabricate_detail-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthfabricateDetail zgsReportMonthfabricateDetail,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportMonthfabricateDetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabricateDetail, req.getParameterMap());
    queryWrapper.ne("isdelete",1);
    queryWrapper.orderByDesc("createtime");
		Page<ZgsReportMonthfabricateDetail> page = new Page<ZgsReportMonthfabricateDetail>(pageNo, pageSize);
		IPage<ZgsReportMonthfabricateDetail> pageList = zgsReportMonthfabricateDetailService.page(page, queryWrapper);
    if (pageList.getRecords().size() > 0){
      for (int i = 0;i < pageList.getRecords().size();i++){
        ZgsReportMonthfabricate zgsReportMonthfabricate = zgsReportMonthfabricateService.getById(pageList.getRecords().get(i).getMonthmianguid());
        if (zgsReportMonthfabricate != null){
          pageList.getRecords().get(i).setAreaname(zgsReportMonthfabricate.getAreaname());
          pageList.getRecords().get(i).setAreacode(zgsReportMonthfabricate.getAreacode());
        }
      }
    }
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportMonthfabricateDetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-添加")
	@ApiOperation(value="zgs_report_monthfabricate_detail-添加", notes="zgs_report_monthfabricate_detail-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthfabricateDetail zgsReportMonthfabricateDetail) {
		zgsReportMonthfabricateDetailService.save(zgsReportMonthfabricateDetail);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthfabricateDetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-编辑")
	@ApiOperation(value="zgs_report_monthfabricate_detail-编辑", notes="zgs_report_monthfabricate_detail-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthfabricateDetail zgsReportMonthfabricateDetail) {
		zgsReportMonthfabricateDetailService.updateById(zgsReportMonthfabricateDetail);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-通过id删除")
	@ApiOperation(value="zgs_report_monthfabricate_detail-通过id删除", notes="zgs_report_monthfabricate_detail-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthfabricateDetailService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-批量删除")
	@ApiOperation(value="zgs_report_monthfabricate_detail-批量删除", notes="zgs_report_monthfabricate_detail-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthfabricateDetailService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_detail-通过id查询")
	@ApiOperation(value="zgs_report_monthfabricate_detail-通过id查询", notes="zgs_report_monthfabricate_detail-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthfabricateDetail zgsReportMonthfabricateDetail = zgsReportMonthfabricateDetailService.getById(id);
		if(zgsReportMonthfabricateDetail==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportMonthfabricateDetail);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthfabricateDetail
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabricateDetail zgsReportMonthfabricateDetail) {
        return super.exportXls(request, zgsReportMonthfabricateDetail, ZgsReportMonthfabricateDetail.class, "zgs_report_monthfabricate_detail");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabricateDetail.class);
    }

}
