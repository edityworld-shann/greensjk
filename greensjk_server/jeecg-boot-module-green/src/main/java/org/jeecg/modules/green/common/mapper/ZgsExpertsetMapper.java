package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsExpertset;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 专家评审屏蔽设置表
 * @Author: jeecg-boot
 * @Date:   2022-02-26
 * @Version: V1.0
 */
public interface ZgsExpertsetMapper extends BaseMapper<ZgsExpertset> {

}
