package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsBuildrojectmainparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsBuildrojectmainparticipantService extends IService<ZgsBuildrojectmainparticipant> {

}
