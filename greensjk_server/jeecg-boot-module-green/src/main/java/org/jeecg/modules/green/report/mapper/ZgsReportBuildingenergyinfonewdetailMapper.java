package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: zgs_report_buildingenergyinfonewdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface ZgsReportBuildingenergyinfonewdetailMapper extends BaseMapper<ZgsReportBuildingenergyinfonewdetail> {

}
