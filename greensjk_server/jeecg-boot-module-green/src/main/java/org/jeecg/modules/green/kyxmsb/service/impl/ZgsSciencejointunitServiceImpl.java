package org.jeecg.modules.green.kyxmsb.service.impl;

import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.mapper.ZgsSciencejointunitMapper;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Service
public class ZgsSciencejointunitServiceImpl extends ServiceImpl<ZgsSciencejointunitMapper, ZgsSciencejointunit> implements IZgsSciencejointunitService {

}
