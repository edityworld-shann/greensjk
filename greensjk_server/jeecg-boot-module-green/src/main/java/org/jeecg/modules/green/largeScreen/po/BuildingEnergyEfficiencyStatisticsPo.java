package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuildingEnergyEfficiencyStatisticsPo implements Serializable {
    /**
     * 新建建筑竣工面积
     */
    private String yearBuildArea;
    /**
     * 竣工项目数
     */
    private String yearBuildNumber;
}
