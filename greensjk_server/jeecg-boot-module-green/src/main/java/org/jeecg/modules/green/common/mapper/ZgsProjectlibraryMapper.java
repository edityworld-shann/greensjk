package org.jeecg.modules.green.common.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itextpdf.text.pdf.parser.XObjectDoHandler;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectParentInfo;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface ZgsProjectlibraryMapper extends BaseMapper<ZgsProjectlibrary> {
    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo1(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo2(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo3(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo4(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo5(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo6(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    Page<ZgsProjectlibraryListInfo> listVerificationUnitAndProjectLeader(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfo(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjectCommonLibrary(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);

    String getProjectNumCurrent(@Param("year") String year);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin1(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin2(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin3_1(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin3_2(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin4(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin5(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin6(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin7(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin8(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin9(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin10(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin11(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin12(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    List<ZgsProjectParentInfo> listZgsProjectParentInfoJoin13(@Param(Constants.WRAPPER) Wrapper<ZgsProjectParentInfo> queryWrapper);

    int updateAddMoney(@Param("id") String id, @Param("provincialfundtotal") BigDecimal provincialfundtotal, @Param("projectnum") String projectnum, @Param("sfxmstatus") String sfxmstatus,@Param("fundRemark") String fundRemark);

    int rollBackSp(@Param("id") String id);

    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOne(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOneForExport(@Param("fieldStr") String fieldStr,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwo(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwoForExport(@Param("fieldStr") String fieldStr,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThree(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThreeForExport(@Param("fieldStr") String fieldStr,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFour(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFourForExport(@Param("fieldStr") String fieldStr,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFive(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFiveForExport(@Param("fieldStr") String fieldStr,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    List<ZgsProjectlibraryListInfo> selectAllData(@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);
    List<ZgsProjectlibraryListInfo> selectAllDataForExport(@Param("fieldStrOfAllData") String fieldStrOfAllData,@Param(Constants.WRAPPER) Wrapper<ZgsProjectlibrary> queryWrapper);


    Map<String, Object> getProjectInfo(@Param("baseguid") String baseguid);


}
