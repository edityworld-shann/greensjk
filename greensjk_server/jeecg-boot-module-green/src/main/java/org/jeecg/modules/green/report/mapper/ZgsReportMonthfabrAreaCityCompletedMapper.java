package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;

/**
 * @Description: 装配式建筑-已竣工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaCityCompletedMapper extends BaseMapper<ZgsReportMonthfabrAreaCityCompleted> {
    ZgsReportMonthfabrAreaproject queryProjectByReportTime(String reporttm);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrAreaDataMonth(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrAreaDataMonthTask(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrAreaDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrAreaDataYearTask(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrCityDataMonth(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrCityToProDataMonth(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrCityDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year, @Param("projecttype") String projecttype);

    ZgsReportMonthfabrAreaCityCompleted totalMonthfabrCityToProDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year, @Param("projecttype") String projecttype);

    //省厅账号计算当前月份、当前行政区划合计值
    ZgsReportMonthfabrAreaCityCompleted lastTotalDataProvince(@Param("filltm") String filltm, @Param("projecttype") String projecttype, @Param("applystate") Integer applystate);

    //市州账号计算当前月份、当前行政区划合计值
    ZgsReportMonthfabrAreaCityCompleted lastTotalDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("projecttype") String projecttype, @Param("applystate") Integer applystate);
}
