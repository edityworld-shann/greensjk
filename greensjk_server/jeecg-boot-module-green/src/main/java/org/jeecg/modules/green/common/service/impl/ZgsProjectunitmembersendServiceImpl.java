package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import org.jeecg.modules.green.common.mapper.ZgsProjectunitmembersendMapper;
import org.jeecg.modules.green.common.service.IZgsProjectunitmembersendService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 账号短信下发表
 * @Author: jeecg-boot
 * @Date:   2022-05-06
 * @Version: V1.0
 */
@Service
public class ZgsProjectunitmembersendServiceImpl extends ServiceImpl<ZgsProjectunitmembersendMapper, ZgsProjectunitmembersend> implements IZgsProjectunitmembersendService {

}
