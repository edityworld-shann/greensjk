package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: zgs_report_buildingenergyinfocity
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportBuildingenergyinfocityService extends IService<ZgsReportBuildingenergyinfocity> {

}
