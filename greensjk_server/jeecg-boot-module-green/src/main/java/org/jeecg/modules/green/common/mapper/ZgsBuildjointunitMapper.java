package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsBuildjointunit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 示范工程联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsBuildjointunitMapper extends BaseMapper<ZgsBuildjointunit> {

}
