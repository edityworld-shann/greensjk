package org.jeecg.modules.green.common.service.impl;

import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryexpert;
import org.jeecg.modules.green.common.mapper.ZgsProjectlibraryexpertMapper;
import org.jeecg.modules.green.common.service.IZgsProjectlibraryexpertService;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssembleproject;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 项目专家关系信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsProjectlibraryexpertServiceImpl extends ServiceImpl<ZgsProjectlibraryexpertMapper, ZgsProjectlibraryexpert> implements IZgsProjectlibraryexpertService {

    @Override
    public List<ZgsProjectlibraryexpert> getZgsProjectlibraryexpertList(String businessguid) {
        List<ZgsProjectlibraryexpert> list = this.baseMapper.getZgsProjectlibraryexpertList(businessguid);
        if (list != null && list.size() > 0) {
            list = ValidateEncryptEntityUtil.validateDecryptList(list, ValidateEncryptEntityUtil.isDecrypt);
        }
        return list;
    }
}
