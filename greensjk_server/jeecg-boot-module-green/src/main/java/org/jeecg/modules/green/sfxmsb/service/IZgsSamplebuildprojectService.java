package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsSamplebuildproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 市政公用工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsSamplebuildprojectService extends IService<ZgsSamplebuildproject> {

}
