package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertimplementunit;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsExamplecertimplementunitMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertimplementunitService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 示范验收证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsExamplecertimplementunitServiceImpl extends ServiceImpl<ZgsExamplecertimplementunitMapper, ZgsExamplecertimplementunit> implements IZgsExamplecertimplementunitService {

}
