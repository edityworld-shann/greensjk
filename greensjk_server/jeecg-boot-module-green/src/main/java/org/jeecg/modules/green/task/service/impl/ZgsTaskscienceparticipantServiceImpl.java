package org.jeecg.modules.green.task.service.impl;

import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.mapper.ZgsTaskscienceparticipantMapper;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
@Service
public class ZgsTaskscienceparticipantServiceImpl extends ServiceImpl<ZgsTaskscienceparticipantMapper, ZgsTaskscienceparticipant> implements IZgsTaskscienceparticipantService {

}
