package org.jeecg.modules.green.kyxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface ZgsSciencejointunitMapper extends BaseMapper<ZgsSciencejointunit> {

}
