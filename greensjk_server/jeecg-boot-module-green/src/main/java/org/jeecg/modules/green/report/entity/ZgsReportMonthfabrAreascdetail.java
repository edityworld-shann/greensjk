package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_monthfabr_areascdetail
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_areascdetail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_monthfabr_areascdetail对象", description = "zgs_report_monthfabr_areascdetail")
public class ZgsReportMonthfabrAreascdetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * REPORT_MONTHFABRICATE_Area.guid
     */
    @Excel(name = "REPORT_MONTHFABRICATE_Area.guid", width = 15)
    @ApiModelProperty(value = "REPORT_MONTHFABRICATE_Area.guid")
    private java.lang.String monthmianguid;
    /**
     * isdelete
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "isdelete")
    private java.math.BigDecimal isdelete;
    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "createpersonname")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    private java.lang.String modifypersonaccount;
    /**
     * 修改人名称
     */
    @Excel(name = "修改人名称", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * 主要技术体系住宅
     */
    @Excel(name = "主要技术体系住宅", width = 15)
    @ApiModelProperty(value = "主要技术体系住宅")
    private java.math.BigDecimal zyjstxzz;
    /**
     * 主要技术体系公共建筑
     */
    @Excel(name = "主要技术体系公共建筑", width = 15)
    @ApiModelProperty(value = "主要技术体系公共建筑")
    private java.math.BigDecimal zyjstxjz;
    /**
     * 装配式混凝土预制构配件企业数量（家）
     */
    @Excel(name = "装配式混凝土预制构配件企业数量（家）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件企业数量（家）")
    private java.math.BigDecimal zpshntqysl;
    /**
     * 装配式混凝土预制构配件生产线（条）
     */
    @Excel(name = "装配式混凝土预制构配件生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产线（条）")
    private java.math.BigDecimal zpshntscx;
    /**
     * 装配式混凝土预制构配件设计产能（万立方米）
     */
    @Excel(name = "装配式混凝土预制构配件设计产能（万立方米）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件设计产能（万立方米）")
    private java.math.BigDecimal zpshntshejcn;
    /**
     * 装配式混凝土预制构配件实际产能（万立方米）
     */
    @Excel(name = "装配式混凝土预制构配件实际产能（万立方米）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件实际产能（万立方米）")
    private java.math.BigDecimal zpshntsjcn;
    /**
     * 装配式钢结构构件企业数量（家）
     */
    @Excel(name = "装配式钢结构构件企业数量（家）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件企业数量（家）")
    private java.math.BigDecimal zpsgjgqysl;
    /**
     * 装配式钢结构构件生产线（条）
     */
    @Excel(name = "装配式钢结构构件生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产线（条）")
    private java.math.BigDecimal zpsgjgscx;
    /**
     * 装配式钢结构构件生产能力（万吨）
     */
    @Excel(name = "装配式钢结构构件生产能力（万吨）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产能力（万吨）")
    private java.math.BigDecimal zpsgjgscnl;
    /**
     * 装配式钢结构构件应用（万平方米）
     */
    @Excel(name = "装配式钢结构构件应用（万平方米）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件应用（万平方米）")
    private java.math.BigDecimal zpsgjgyy;
    /**
     * 结构形式企业数量（家）
     */
    @Excel(name = "结构形式企业数量（家）", width = 15)
    @ApiModelProperty(value = "结构形式企业数量（家）")
    private java.math.BigDecimal jgxsqysl;
    /**
     * 结构形式产线（条）
     */
    @Excel(name = "结构形式产线（条）", width = 15)
    @ApiModelProperty(value = "结构形式产线（条）")
    private java.math.BigDecimal jgxsscx;
    /**
     * 结构形式生产能力（万平方米）
     */
    @Excel(name = "结构形式生产能力（万平方米）", width = 15)
    @ApiModelProperty(value = "结构形式生产能力（万平方米）")
    private java.math.BigDecimal jgxsscnl;
    /**
     * 结构形式应用（万平方米）
     */
    @Excel(name = "结构形式应用（万平方米）", width = 15)
    @ApiModelProperty(value = "结构形式应用（万平方米）")
    private java.math.BigDecimal jgxsyy;
}
