package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaprojectMapper extends BaseMapper<ZgsReportMonthfabrAreaproject> {

  public ZgsReportMonthfabrAreaproject queryProjectByReportTime(@Param("reporttm") String reporttm);
}
