package org.jeecg.modules.green.jhxmbgsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 计划变更项目组成员表
 * @Author: jeecg-boot
 * @Date:   2022-07-14
 * @Version: V1.0
 */
public interface ZgsPlannedchangeparticipantMapper extends BaseMapper<ZgsPlannedchangeparticipant> {

}
