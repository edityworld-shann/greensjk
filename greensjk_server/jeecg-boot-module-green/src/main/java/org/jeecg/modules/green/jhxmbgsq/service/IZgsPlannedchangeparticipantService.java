package org.jeecg.modules.green.jhxmbgsq.service;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 计划变更项目组成员表
 * @Author: jeecg-boot
 * @Date:   2022-07-14
 * @Version: V1.0
 */
public interface IZgsPlannedchangeparticipantService extends IService<ZgsPlannedchangeparticipant> {

}
