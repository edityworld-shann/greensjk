/**
 * Copyright 2013-2015 JEECG (jeecgos@163.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jeecg.modules.demo.poi.view;

import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import cn.afterturn.easypoi.word.WordExportUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Word模板导出
 *
 * @author JEECG
 * @date 2014年6月30日 下午9:15:49
 */
@SuppressWarnings("unchecked")
@Controller(TemplateWordConstants.JEECG_TEMPLATE_WORD_VIEW)
public class JeecgTemplateWordView extends AbstractView {

    private static final String CONTENT_TYPE = "application/msword";

    public JeecgTemplateWordView() {
        setContentType(CONTENT_TYPE);
    }

    public boolean isIE(HttpServletRequest request) {
        return (request.getHeader("USER-AGENT").toLowerCase().indexOf("msie") > 0 || request.getHeader("USER-AGENT").toLowerCase().indexOf("rv:11.0") > 0) ? true : false;
    }

    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String codedFileName = "临时文件.docx";
        if (model.containsKey(TemplateWordConstants.FILE_NAME)) {
            codedFileName = (String) model.get(TemplateWordConstants.FILE_NAME) + ".docx";
        }
        if (isIE(request)) {
            codedFileName = java.net.URLEncoder.encode(codedFileName, "UTF8");
        } else {
            codedFileName = new String(codedFileName.getBytes("UTF-8"), "ISO-8859-1");
        }
        response.setHeader("content-disposition", "attachment;filename=" + codedFileName);
        XWPFDocument document = WordExportUtil.exportWord07((String) model.get(TemplateWordConstants.URL), (Map<String, Object>) model.get(TemplateWordConstants.MAP_DATA));

        // add by liur for redonly
        document.enforceReadonlyProtection("bT+ej.@y$7ap", HashAlgorithm.sha512);
        document.enforceFillingFormsProtection("bT+ej.@y$7ap", HashAlgorithm.sha512);
        // end by liur

        ServletOutputStream out = response.getOutputStream();
        document.write(out);
        out.flush();
    }

    /**
     * @param fileName     生成的文件名
     * @param wordTemplate 模板的主路径
     * @param templateName 模板的名称
     * @param params       数据
     * @return
     */
    public String exportWordOut(String fileName, String wordTemplate, String upload, String templateName, Map<String, Object> params) {
        String filePath = null;
        try {
            String tempBefore = "green_task";
            String nowDay = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String tempPath = File.separator + nowDay + File.separator;
            String temDir = upload + File.separator + tempBefore + tempPath;
            File dir = new File(temDir);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            XWPFDocument doc = WordExportUtil.exportWord07(wordTemplate + File.separator + templateName, params);
            filePath = tempBefore + tempPath + fileName;
            FileOutputStream fos = new FileOutputStream(temDir + fileName);
            doc.write(fos);
            fos.flush();
            doc.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }
}
