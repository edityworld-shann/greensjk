package org.jeecg.modules.green.sms.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 短信记录表
 * @Author: jeecg-boot
 * @Date:   2022-06-07
 * @Version: V1.0
 */
public interface ZgsSmsSendingRecordLogMapper extends BaseMapper<ZgsSmsSendingRecordLog> {

}
