package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 退回记录信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Data
@TableName("zgs_returnrecord")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_returnrecord对象", description = "退回记录信息表")
public class ZgsReturnrecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * businessguid
     */
    @Excel(name = "businessguid", width = 15)
    @ApiModelProperty(value = "businessguid")
    private java.lang.String businessguid;
    /**
     * enterpriseguid
     */
    @Excel(name = "enterpriseguid", width = 15)
    @ApiModelProperty(value = "enterpriseguid")
    private java.lang.String enterpriseguid;
    /**
     * enterprisename
     */
    @Excel(name = "enterprisename", width = 15)
    @ApiModelProperty(value = "enterprisename")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String enterprisename;
    /**
     * projectname
     */
    @Excel(name = "projectname", width = 15)
    @ApiModelProperty(value = "projectname")
    private java.lang.String projectname;
    /**
     * applydate
     */
    @Excel(name = "applydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "applydate")
    private java.util.Date applydate;
    /**
     * returndate
     */
    @Excel(name = "returndate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "returndate")
    private java.util.Date returndate;
    /**
     * returnperson
     */
    @Excel(name = "returnperson", width = 15)
    @ApiModelProperty(value = "returnperson")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String returnperson;
    /**
     * returnreason
     */
    @Excel(name = "returnreason", width = 15)
    @ApiModelProperty(value = "returnreason")
    private java.lang.String returnreason;
    /**
     * returndepartment
     */
    @Excel(name = "returndepartment", width = 15)
    @ApiModelProperty(value = "returndepartment")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String returndepartment;
    /**
     * returnstep
     */
    @Excel(name = "returnstep", width = 15)
    @ApiModelProperty(value = "returnstep")
    private java.lang.String returnstep;
    /**
     * businesstype
     */
    @Excel(name = "businesstype", width = 15)
    @ApiModelProperty(value = "businesstype")
    private java.lang.String businesstype;
    /**
     * createpersonaccount
     */
    @Excel(name = "createpersonaccount", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * createpersonname
     */
    @Excel(name = "createpersonname", width = 15)
    @ApiModelProperty(value = "createpersonname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * createtime
     */
    @Excel(name = "createtime", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * modifypersonaccount
     */
    @Excel(name = "modifypersonaccount", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * modifypersonname
     */
    @Excel(name = "modifypersonname", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * modifytime
     */
    @Excel(name = "modifytime", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * isdelete
     */
    @Excel(name = "isdelete", width = 15)
    @ApiModelProperty(value = "isdelete")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 审批类型，0：通过；1：不通过；2：补正修改；3：项目终止；
     */
    @Excel(name = "审批类型，0：通过；1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "审批类型，0：通过；1：不通过；2：补正修改；3：项目终止；")
    private java.math.BigDecimal returntype;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目库ID")
    private String projectlibraryguid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
