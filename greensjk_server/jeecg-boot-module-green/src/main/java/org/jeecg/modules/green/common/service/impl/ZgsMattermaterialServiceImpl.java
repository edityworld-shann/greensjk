package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.common.mapper.ZgsMattermaterialMapper;
import org.jeecg.modules.green.common.po.ZgsExpertinfoPo;
import org.jeecg.modules.green.common.service.IZgsMattermaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;
import java.util.Map;

/**
 * @Description: 业务库-各类项目工程申报材料
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsMattermaterialServiceImpl extends ServiceImpl<ZgsMattermaterialMapper, ZgsMattermaterial> implements IZgsMattermaterialService {

    @Override
    public List<ZgsMattermaterial> getZgsMattermaterialList(Map<String, Object> map) {
        return this.baseMapper.getZgsMattermaterialList(map);
    }

    @Override
    public List<ZgsMattermaterial> getAddZgsMattermaterialList(Map<String, Object> map) {
        return this.baseMapper.getAddZgsMattermaterialList(map);
    }

    @Override
    public List<ZgsExpertinfoPo> getTheNumberOfExpertsFor() {
        return this.baseMapper.getTheNumberOfExpertsFor();
    }

    @Override
    public int getUserCount(String itemValue, Integer type) {
        return this.baseMapper.getUserCount(itemValue, type);
    }
}
