package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProject;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;

import java.util.List;

/**
 * @Description: 装配式建筑-项目库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaCityProjectService extends IService<ZgsReportMonthfabrAreaCityProject> {
    public List<ZgsReportMonthfabrAreaCityProject> queryProjectByReportTime(String reporttm, String areacode);

    public void addIterm(String filltm);
}
