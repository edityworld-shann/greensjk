package org.jeecg.modules.green.common.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsExpertset;
import org.jeecg.modules.green.common.entity.ZgsProjectSoftExpertinfo;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.common.service.IZgsExpertsetService;
import org.jeecg.modules.green.common.service.IZgsSciencetechfeasibleService;
import org.jeecg.modules.green.common.service.IZgsSofttechexpertService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.SMSTokenUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科技类审批记录
 * @Author: jeecg-boot
 * @Date: 2022-02-21
 * @Version: V1.0
 */
@Api(tags = "科技类审批记录")
@RestController
@RequestMapping("/common/zgsSofttechexpert")
@Slf4j
public class ZgsSofttechexpertController extends JeecgController<ZgsSofttechexpert, IZgsSofttechexpertService> {
    @Autowired
    private IZgsSofttechexpertService zgsSofttechexpertService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private SMSTokenUtil smsTokenUtil;


    /**
     * 分页列表查询
     *
     * @param zgsSofttechexpert
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科技类审批记录-分页列表查询")
    @ApiOperation(value = "科技类审批记录-分页列表查询", notes = "科技类审批记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSofttechexpert zgsSofttechexpert,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsSofttechexpert> queryWrapper = QueryGenerator.initQueryWrapper(zgsSofttechexpert, req.getParameterMap());
        Page<ZgsSofttechexpert> page = new Page<ZgsSofttechexpert>(pageNo, pageSize);
        IPage<ZgsSofttechexpert> pageList = zgsSofttechexpertService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsSofttechexpert
     * @return
     */
    @AutoLog(value = "科技类审批记录-添加")
    @ApiOperation(value = "科技类审批记录-添加", notes = "科技类审批记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSofttechexpert zgsSofttechexpert) {
        zgsSofttechexpertService.save(zgsSofttechexpert);
        return Result.OK("添加成功！");
    }

    /**
     * 专家分配
     *
     * @param zgsProjectSoftExpertinfo
     * @return
     */
    @AutoLog(value = "科技类审批记录-专家分配")
    @ApiOperation(value = "科技类审批记录-专家分配", notes = "科技类审批记录-专家分配")
    @PostMapping(value = "/addFp")
    public Result<?> add(@RequestBody ZgsProjectSoftExpertinfo zgsProjectSoftExpertinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectSoftExpertinfo != null) {
            String projectId = zgsProjectSoftExpertinfo.getId();
            ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(projectId);
            String sciencetype = zgsSciencetechfeasible.getSciencetype();
            if (GlobalConstants.ScienceTech.equals(sciencetype)) {
                sciencetype = GlobalConstants.ScienceTech_VALUE;
            } else if (GlobalConstants.ScienceSoft.equals(sciencetype)) {
                sciencetype = GlobalConstants.ScienceSoft_VALUE;
            }
            ZgsSciencetechfeasible sciencetechfeasible = new ZgsSciencetechfeasible();
            sciencetechfeasible.setId(projectId);
            sciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS12);
            sciencetechfeasible.setSfhs(zgsProjectSoftExpertinfo.getSfhs());
            zgsSciencetechfeasibleService.updateById(sciencetechfeasible);
            //不抽取专家
            if (zgsProjectSoftExpertinfo.getIsChouQu() == 0) {

            } else {
                //抽取专家发短信
                if (zgsProjectSoftExpertinfo.getZgsExpertinfoList() != null && zgsProjectSoftExpertinfo.getZgsExpertinfoList().size() > 0) {
                    for (int i = 0; i < zgsProjectSoftExpertinfo.getZgsExpertinfoList().size(); i++) {
                        ZgsSofttechexpert zgsSofttechexpert = new ZgsSofttechexpert();
                        ZgsExpertinfo zgsExpertinfo = zgsProjectSoftExpertinfo.getZgsExpertinfoList().get(i);
                        zgsSofttechexpert.setId(UUID.randomUUID().toString());
                        zgsSofttechexpert.setExpertguid(zgsExpertinfo.getId());
                        zgsSofttechexpert.setBusinessguid(projectId);
                        zgsSofttechexpert.setBusinesstype(sciencetype);
                        zgsSofttechexpert.setCreatepersonaccount(sysUser.getUsername());
                        zgsSofttechexpert.setCreatepersonname(sysUser.getRealname());
                        zgsSofttechexpert.setCreatetime(new Date());
                        boolean flag = zgsSofttechexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSofttechexpert, ValidateEncryptEntityUtil.isEncrypt));
                        if (flag) {
                            //给选中专家发送短信
                            if (StringUtils.isNotEmpty(zgsExpertinfo.getMobile())) {
                                HashMap<String, Object> fillContent = new HashMap<>();
                                fillContent.put("content", zgsExpertinfo.getCurrentposition());
                                fillContent.put("projectName", "1");
                                fillContent.put("year", "2022");
                                fillContent.put("month", "09");
                                fillContent.put("day", "14");
                                fillContent.put("time", "14:00");
                                fillContent.put("location", "1");
                                fillContent.put("locationName", "2");
                                HashMap<String, Object> paramsMap = new HashMap<>();
//                                    paramsMap.put("phoneNumber", zgsExpertinfo.getMobile());
                                paramsMap.put("phoneNumber", "18210502471");
                                paramsMap.put("smsType", 3);
                                paramsMap.put("companyName", zgsExpertinfo.getUsername());
                                paramsMap.put("templateId", 12);
                                paramsMap.put("fillContent", fillContent);
                                Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                                if (result.getCode() == 200) {
                                    log.info("申报项目给抽取专家发送短信成功！");
                                } else {
                                    log.info("申报项目给抽取专家发送短信失败！" + result.getMessage());
                                }
                            }
                        }
                        //添加屏蔽专家数据
                        ZgsExpertset zgsExpertset = new ZgsExpertset();
                        zgsExpertset.setId(UUID.randomUUID().toString());
                        zgsExpertset.setExpertguid(zgsExpertinfo.getId());
                        zgsExpertset.setBusinessguid(projectId);
                        zgsExpertset.setCreatepersonaccount(sysUser.getUsername());
                        zgsExpertset.setCreatepersonname(sysUser.getRealname());
                        zgsExpertset.setCreatetime(new Date());
                        zgsExpertset.setSetvalue(zgsProjectSoftExpertinfo.getSet());
                        zgsExpertsetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertset, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        } else {
            return Result.error("操作失败!");
        }
        return Result.OK("操作成功！");
    }

    /**
     * 专家-审批
     *
     * @param zgsSofttechexpert
     * @return
     */
    @AutoLog(value = "科技类审批记录-专家-审批")
    @ApiOperation(value = "科技类审批记录-专家-审批", notes = "科技类审批记录-专家-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSofttechexpert zgsSofttechexpert) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String businessguid = zgsSofttechexpert.getId();
        zgsSofttechexpert.setModifypersonaccount(sysUser.getUsername());
        zgsSofttechexpert.setModifypersonname(sysUser.getRealname());
        zgsSofttechexpert.setModifytime(new Date());
        zgsSofttechexpert.setAuditdate(new Date());
        if (zgsSofttechexpert != null && StringUtils.isNotEmpty(zgsSofttechexpert.getAuditconclusionnum())) {
            if (GlobalConstants.AGREE_1.equals(zgsSofttechexpert.getAuditconclusionnum())) {
                zgsSofttechexpert.setAuditconclusion("同意");
            } else {
                zgsSofttechexpert.setAuditconclusion("不同意");
            }
        }
        QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
        zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
        ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
        QueryWrapper<ZgsSofttechexpert> zgsSofttechexpertQueryWrapper = new QueryWrapper();
        zgsSofttechexpertQueryWrapper.eq("businessguid", businessguid);
        zgsSofttechexpertQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
        zgsSofttechexpertQueryWrapper.isNull("auditconclusionnum");
        ZgsSofttechexpert softtechexpert = zgsSofttechexpertService.getOne(zgsSofttechexpertQueryWrapper);
        zgsSofttechexpert.setId(softtechexpert.getId());
        zgsSofttechexpertService.updateById(zgsSofttechexpert);
        //判断是否为驳回
        QueryWrapper<ZgsSofttechexpert> softtechexpertQueryWrapper = new QueryWrapper();
        softtechexpertQueryWrapper.eq("businessguid", businessguid);
        softtechexpertQueryWrapper.ne("isdelete", 1);
        List<ZgsSofttechexpert> zgsSofttechexpertList = zgsSofttechexpertService.list(softtechexpertQueryWrapper);
        if (zgsSofttechexpertList != null && zgsSofttechexpertList.size() > 0) {
            int size = zgsSofttechexpertList.size();
            int count1 = 0;
            int count0 = 0;
            for (ZgsSofttechexpert softtechexpert1 : zgsSofttechexpertList) {
                //1同意0不同意
                String auditconclusionnum = softtechexpert1.getAuditconclusionnum();
                if (StringUtils.isNotEmpty(auditconclusionnum)) {
                    if (GlobalConstants.AGREE_1.equals(auditconclusionnum)) {
                        count1++;//同意
                    } else {
                        count0++;//不同意
                    }
                }
            }
            if ((count1 + count0) == size) {
                ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
                zgsSciencetechfeasible.setId(businessguid);
                if ((double) count1 / size < 0.6) {
                    //同意率小于百分之60直接驳回
                    zgsSciencetechfeasible.setAuditopinion("专家审批通过率低于60%直接驳回");
//                    zgsSciencetechfeasible.setAgreeproject(new BigDecimal(1));
                    zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS6);

                } else {
                    zgsSciencetechfeasible.setAuditopinion(null);
//                    zgsSciencetechfeasible.setAgreeproject(new BigDecimal(2));
                    zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS10);
                }
                zgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible);
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 编辑
     *
     * @param zgsSofttechexpert
     * @return
     */
    @AutoLog(value = "科技类审批记录-编辑")
    @ApiOperation(value = "科技类审批记录-编辑", notes = "科技类审批记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSofttechexpert zgsSofttechexpert) {
        zgsSofttechexpertService.updateById(zgsSofttechexpert);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科技类审批记录-通过id删除")
    @ApiOperation(value = "科技类审批记录-通过id删除", notes = "科技类审批记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSofttechexpertService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科技类审批记录-批量删除")
    @ApiOperation(value = "科技类审批记录-批量删除", notes = "科技类审批记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSofttechexpertService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科技类审批记录-通过id查询")
    @ApiOperation(value = "科技类审批记录-通过id查询", notes = "科技类审批记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsSofttechexpert zgsSofttechexpert = zgsSofttechexpertService.getById(id);
        if (zgsSofttechexpert == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsSofttechexpert);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsSofttechexpert
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsSofttechexpert zgsSofttechexpert) {
        return super.exportXls(request, zgsSofttechexpert, ZgsSofttechexpert.class, "科技类审批记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSofttechexpert.class);
    }

}
