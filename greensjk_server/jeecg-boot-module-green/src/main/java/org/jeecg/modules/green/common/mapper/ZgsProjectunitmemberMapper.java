package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目单位注册信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface ZgsProjectunitmemberMapper extends BaseMapper<ZgsProjectunitmember> {
    public int updateUserBlackStatus(@Param("userId") String userId);

    public int updateUserRegistStatus(@Param("userId") String userId);

    public int updateUserSpDelete(@Param("userId") String userId);
}
