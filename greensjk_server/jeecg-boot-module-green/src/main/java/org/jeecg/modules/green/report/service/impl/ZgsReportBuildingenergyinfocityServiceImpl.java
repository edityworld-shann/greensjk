package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;
import org.jeecg.modules.green.report.mapper.ZgsReportBuildingenergyinfocityMapper;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfocityService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_buildingenergyinfocity
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportBuildingenergyinfocityServiceImpl extends ServiceImpl<ZgsReportBuildingenergyinfocityMapper, ZgsReportBuildingenergyinfocity> implements IZgsReportBuildingenergyinfocityService {

}
