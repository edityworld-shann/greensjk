package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfo;
import org.jeecg.modules.green.report.mapper.ZgsReportBuildingenergyinfoMapper;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_buildingenergyinfo
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportBuildingenergyinfoServiceImpl extends ServiceImpl<ZgsReportBuildingenergyinfoMapper, ZgsReportBuildingenergyinfo> implements IZgsReportBuildingenergyinfoService {

}
