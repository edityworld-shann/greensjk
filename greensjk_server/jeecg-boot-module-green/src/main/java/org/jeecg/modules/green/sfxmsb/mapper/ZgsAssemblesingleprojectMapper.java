package org.jeecg.modules.green.sfxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemblesingleproject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式示范工程单体建筑基本信息
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsAssemblesingleprojectMapper extends BaseMapper<ZgsAssemblesingleproject> {

}
