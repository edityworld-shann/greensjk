package org.jeecg.modules.green.kyxmjtzs.service.impl;

import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.mapper.ZgsSpostresearcherMapper;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostresearcherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSpostresearcherServiceImpl extends ServiceImpl<ZgsSpostresearcherMapper, ZgsSpostresearcher> implements IZgsSpostresearcherService {

}
