package org.jeecg.modules.green.kyxmsb.service.impl;

import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.mapper.ZgsScienceparticipantMapper;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Service
public class ZgsScienceparticipantServiceImpl extends ServiceImpl<ZgsScienceparticipantMapper, ZgsScienceparticipant> implements IZgsScienceparticipantService {

}
