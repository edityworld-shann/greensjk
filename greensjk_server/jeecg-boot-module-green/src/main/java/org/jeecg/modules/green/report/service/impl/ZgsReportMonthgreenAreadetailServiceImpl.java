package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenAreadetail;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthgreenAreadetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenAreadetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthgreen_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthgreenAreadetailServiceImpl extends ServiceImpl<ZgsReportMonthgreenAreadetailMapper, ZgsReportMonthgreenAreadetail> implements IZgsReportMonthgreenAreadetailService {

}
