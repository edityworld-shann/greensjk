package org.jeecg.modules.green.jhxmbgsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建设科技计划项目变更申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_plannedprojectchange")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_plannedprojectchange对象", description = "建设科技计划项目变更申请业务主表")
public class ZgsPlannedprojectchange implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 建筑工程项目库ID
     */
//    @Excel(name = "建筑工程项目库ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "建筑工程项目库ID")
    private java.lang.String projectlibraryguid;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "工程项目名称")
    private java.lang.String projectname;
    /**
     * 立项编号
     */
    @Excel(name = "立项编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "立项编号")
    private java.lang.String setupnum;
    /**
     * 任务书编号
     */
    @Excel(name = "任务书编号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "任务书编号")
    private java.lang.String tasknum;
    /**
     * 研究起始时间
     */
    @Excel(name = "研究起始时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "研究起始时间")
    private java.util.Date researchstartdate;
    /**
     * 研究终止时间
     */
    @Excel(name = "研究终止时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "研究终止时间")
    private java.util.Date researchenddate;
    /**
     * 项目承担单位
     */
    @Excel(name = "项目承担单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目承担单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectbearunit;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleader;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15, orderNum = "2")
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkphone;
    /**
     * 工作单位
     */
    @Excel(name = "工作单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "工作单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String workunit;
    /**
     * 电子邮箱
     */
    //@Excel(name = "电子邮箱", width = 15, orderNum = "2")
    @ApiModelProperty(value = "电子邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String email;
    /**
     * 通信地址
     */
    //@Excel(name = "通信地址", width = 15, orderNum = "2")
    @ApiModelProperty(value = "通信地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String address;
    /**
     * 邮编
     */
    //@Excel(name = "邮编", width = 15, orderNum = "2")
    @ApiModelProperty(value = "邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String zipcode;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审
     */
//    @Excel(name = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，2终审通过，3退回企业，4初审通过，5退回初审")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15, orderNum = "2")
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15, orderNum = "2")
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(exist = false)
    private List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList;
    @TableField(exist = false)
    private List<ZgsTaskscienceparticipant> zgsTaskparticipantList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    /**
     * 1延期申请,2其他类申请
     */
    @ApiModelProperty(value = "1项目终止申请,2信息变更申请（完成日期、项目组成员等信息变更）")
    private String intype;
    /**
     * 终止类型：1申请终止，2省厅终止
     */
    @ApiModelProperty(value = "终止类型：1申请终止，2省厅终止")
    @Dict(dicCode = "closeType")
    private String closetype;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
}
