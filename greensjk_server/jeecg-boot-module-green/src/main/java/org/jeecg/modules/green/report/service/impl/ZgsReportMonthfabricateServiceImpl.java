package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricate;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabricateMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabricate
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabricateServiceImpl extends ServiceImpl<ZgsReportMonthfabricateMapper, ZgsReportMonthfabricate> implements IZgsReportMonthfabricateService {

}
