package org.jeecg.modules.green.lsfz.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemprojectmainparticipant;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @describe:绿色发展业务主表
 * @author: renxiaoliang
 * @date: 2023/12/12 14:24
 */
@Data
@TableName("zgs_greendevelop")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_greendevelop对象", description = "绿色发展项目主表")
public class ZgsGreendevelop implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private String id;
    /**
     * 申报年度
     */
    @Excel(name = "申报年度", width = 15)
    @ApiModelProperty(value = "申报年度")
    private String year = Integer.toString(LocalDate.now().getYear());

    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private String enterpriseguid;
    /**
     * 上报状态：0 保存， 1 保存并上报
     */
    @Excel(name = "上报状态：0 保存， 1 保存并上报", width = 15)
    @ApiModelProperty(value = "上报状态：0 保存， 1 保存并上报")
    private String status;
    /**
     * 项目进度阶段
     */
    @Excel(name = "项目阶段状态 0未上报,1待审核,4初审通过,3初审退回,2终审通过,5终审退回", width = 15)
    @ApiModelProperty(value = "项目阶段状态 0未上报,1待审核,4初审通过,3初审退回,2终审通过,5终审退回")
    private String projectstageState;


    @Excel(name = "项目进度阶段", width = 15)
    @ApiModelProperty(value = "项目进度阶段")
    private String projectstage;

    @Excel(name = "项目进度阶段详情", width = 15)
    @ApiModelProperty(value = "项目进度阶段详情")
    private String projectstageDetails;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private String projectName;
    /**
     * 填报人
     */
    @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    private String informant;
    /**
     * 填报日期
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "填报日期")
    private Date informantDate;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String phone;
    /**
     * 填报单位
     */
    @Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    private String fillUnit;
    /**
     * 一级建筑分类
     */
    @Excel(name = "一级建筑分类", width = 15)
    @ApiModelProperty(value = "一级建筑分类")
    private String buildFirstcategory;
    /**
     * 二级建筑分类
     */
    @Excel(name = "二级建筑分类", width = 15)
    @ApiModelProperty(value = "二级建筑分类")
    private String buildSecondcategory;

    /**
     * 绿色建筑等级
     */
    @Excel(name = "绿色建筑等级", width = 15)
    @ApiModelProperty(value = "绿色建筑等级")
    private String greenbuildgrade;
    /**
     * 建筑类型
     */
    @Excel(name = "建筑类型", width = 15)
    @ApiModelProperty(value = "建筑类型")
    private String buildtype;
    /**
     * 项目进度阶段
     */
    /*@Excel(name = "项目进度阶段", width = 15)
    @ApiModelProperty(value = "项目进度阶段")
    private String projectstage;*/
    /**
     * 项目阶段详情
     */
    /*@Excel(name = "项目阶段详情", width = 15)
    @ApiModelProperty(value = "项目阶段详情")
    private String projectstageDetails;*/
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15)
    @ApiModelProperty(value = "上报日期")
    private Date applydate;
    /**
     * 金融审批状态
     */
    @Excel(name = "金融审批状态 1 审批通过； 0 审批不通过", width = 15)
    @ApiModelProperty(value = "金融审批状态 1 审批通过； 0 审批不通过")
    private String financeState;
    /**
     * 金融审批意见
     */
    @Excel(name = "金融审批意见", width = 15)
    @ApiModelProperty(value = "金融审批意见")
    private String financeOpinion;
    /**
     * 推介审批情况
     */
    /*@Excel(name = "推介审批情况", width = 15)
    @ApiModelProperty(value = "推介审批情况")
    private String recommendApproval;*/
    /**
     * 金融机构名称
     */
    @Excel(name = "金融机构名称", width = 15)
    @ApiModelProperty(value = "金融机构名称")
    private String financeUnitName;
    /**
     * 获得金额
     */
    @Excel(name = "获得金额", width = 15)
    @ApiModelProperty(value = "获得金额")
    private String obtainMoney;
    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "创建人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private String createusername;
    /**
     * 创建日期
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createdate;
    /**
     * 修改人账号
     */
    @Excel(name = "modifyaccountname", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "modifyusername", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private String modifyusername;
    /**
     * 修改日期
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private String isdelete = "0";

    // 建设内容
    @Excel(name = "建设内容", width = 15)
    @ApiModelProperty(value = "建设内容")
    private String buildContent;

    // 项目规模等级
    @Excel(name = "项目规模等级", width = 15)
    @ApiModelProperty(value = "项目规模等级")
    private String projectScaleGrade;

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    // 绿色发展项目详情信息（负责人、负责人电话、单位信息）
    @TableField(exist = false)
    private List<ZgsGreendevelopDetail> zgsGreendevelopDetails;
    // 绿色发展附件
    /*@TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;*/

    @TableField(exist = false)
    private List<ZgsAttachappendix> zgsAttachappendixList;

    // 建筑规模
    @TableField(exist = false)
    private String constructionScale;
    // 项目投资金额
    @TableField(exist = false)
    private String investMoney;
    // 申请绿色金融产品
    @TableField(exist = false)
    private String applyFinance;
    // 申请绿色金融数额
    @TableField(exist = false)
    private String applyGreenfinanceMoney;
    // 申请绿色金融主题单位
    @TableField(exist = false)
    private String applyGreenfinanceUnit;
    // 项目负责人
    @TableField(exist = false)
    private String applyGreenfinanceLeader;
    // 联系电话
    @TableField(exist = false)
    private String applyGreenfinancePhone;
    // 建设单位
    @TableField(exist = false)
    private String constructUnit;
    // 项目负责人
    @TableField(exist = false)
    private String constructUnitLeader;
    // 联系电话
    @TableField(exist = false)
    private String constructUnitPhone;
    // 设计单位
    @TableField(exist = false)
    private String designUnit;
    // 项目负责人
    @TableField(exist = false)
    private String designUnitLeader;
    // 联系电话
    @TableField(exist = false)
    private String designUnitPhone;
    // 咨询单位
    @TableField(exist = false)
    private String askUnit;
    // 项目负责人
    @TableField(exist = false)
    private String askUnitLeader;
    // 联系电话
    @TableField(exist = false)
    private String askUnitPhone;
    // 施工单位
    @TableField(exist = false)
    private String buildUnit;
    // 项目负责人
    @TableField(exist = false)
    private String buildUnitLeader;
    // 联系电话
    @TableField(exist = false)
    private String buildUnitPhone;
    // 养护单位
    @TableField(exist = false)
    private String operateUnit;
    // 项目负责人
    @TableField(exist = false)
    private String operateUnitLeader;
    // 联系电话
    @TableField(exist = false)
    private String operateUnitPhone;
    // 服务单位
    @TableField(exist = false)
    private String serviceUnit;
    // 服务单位负责人
    @TableField(exist = false)
    private String serviceUnitLeader;
    // 服务单位电话
    @TableField(exist = false)
    private String serviceUnitPhone;

    // 初审意见
    private String auditopinion;
    // 初审审批状态  1 通过  0 不通过
    @TableField(exist = false)
    private String spStatus;
    // 建筑类型下拉选择的类别
    @TableField(exist = false)
    private String[] buildtypeArray;


}
