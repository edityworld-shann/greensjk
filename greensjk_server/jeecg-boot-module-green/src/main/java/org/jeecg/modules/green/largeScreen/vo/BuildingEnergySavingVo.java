package org.jeecg.modules.green.largeScreen.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BuildingEnergySavingVo implements Serializable {
    private String areaname;
    private String greenBuildingArea;
    private String greenBuildingCount;
    private List<BuildingEnergySavingDetails> detailsList;
}
