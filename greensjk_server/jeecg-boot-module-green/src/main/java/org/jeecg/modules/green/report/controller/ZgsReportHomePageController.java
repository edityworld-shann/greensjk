package org.jeecg.modules.green.report.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuild;
import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildDetail;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfocityService;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewdetailService;
import org.jeecg.modules.green.report.service.IZgsReportHomePageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

/**
* @Description: zgs_report_buildingenergyinfocity
* @Author: jeecg-boot
* @Date:   2022-03-16
* @Version: V1.0
*/
@Api(tags="数据大屏首页")
@RestController
@RequestMapping("/report/zgsReportReportHomePage")
@Slf4j
public class ZgsReportHomePageController extends JeecgController<ZgsReportBuildingenergyinfocity, IZgsReportHomePageService> {
   @Autowired
   private IZgsReportHomePageService zgsReportBuildingenergyinfocityService;

   @Autowired
   private IZgsReportBuildingenergyinfonewdetailService zgsReportBuildingenergyinfonewdetailService;

   /**
    * 分页列表查询
    *
    * @param zgsReportBuildingenergyinfocity
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "建筑节能统计管理（市州）-分页列表查询")
   @ApiOperation(value="建筑节能统计管理（市州）-分页列表查询", notes="建筑节能统计管理（市州）-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
       QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
       if(zgsReportBuildingenergyinfocity!=null){
           if(zgsReportBuildingenergyinfocity.getApplystate()!=null){
               //有问题，BigDecimal applystate 需要判空？
               BigDecimal applystate  = zgsReportBuildingenergyinfocity.getApplystate();
               qW.eq("applystate",applystate);
           }
           Integer userType = sysUser.getLoginUserType();
           //6县区，7市区
           if(userType==7){
               qW.eq("areacode",sysUser.getAreacode());
           }
           if(StringUtils.isNotEmpty(zgsReportBuildingenergyinfocity.getReporttm())){
               String reporttm  = zgsReportBuildingenergyinfocity.getReporttm();
               qW.eq("reporttm",reporttm);
           }
       }
       qW.ne("isdelete",1);
       qW.orderByDesc("applydate");
       Page<ZgsReportBuildingenergyinfocity> page = new Page<ZgsReportBuildingenergyinfocity>(pageNo, pageSize);
       IPage<ZgsReportBuildingenergyinfocity> pageList = zgsReportBuildingenergyinfocityService.page(page, qW);
       for(ZgsReportBuildingenergyinfocity b : pageList.getRecords()){
           QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
           qWdetail.eq("buildingenergyinfonewguid",b.getId());
           List<ZgsReportBuildingenergyinfonewdetail> detailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
           zgsReportBuildingenergyinfocity.setZgsReportBuildingenergyinfonewdetailList(detailList);
       }
       return Result.OK(pageList);

   }


   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "建筑节能统计管理（市州）-通过id查询")
   @ApiOperation(value="建筑节能统计管理（市州）-通过id查询", notes="建筑节能统计管理（市州）-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity = zgsReportBuildingenergyinfocityService.getById(id);
       if(zgsReportBuildingenergyinfocity==null) {
           return Result.error("未找到对应数据");
       }else{
           QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
           qWdetail.eq("buildingenergyinfonewguid",id);
           List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
           zgsReportBuildingenergyinfocity.setZgsReportBuildingenergyinfonewdetailList(zgsReportBuildingenergyinfonewdetailList);
       }

       return Result.OK(zgsReportBuildingenergyinfocity);
   }






    /**
     * 分页列表查询
     *
     * @param zgsReportBuildingenergyinfocity
     * @param req
     * @return
     */
    @AutoLog(value = "大屏建筑节能查询")
    @ApiOperation(value="大屏建筑节能查询", notes="大屏建筑节能查询")
    @GetMapping(value = "/screanQueryList")
    public Result<?> screanQueryList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
        String reporttm = DateUtils.formatDate(new Date(),"YYYY-MM");
        if(zgsReportBuildingenergyinfocity!=null){
            if(zgsReportBuildingenergyinfocity.getReporttm()!=null){
               reporttm = zgsReportBuildingenergyinfocity.getReporttm();
           }
            if(zgsReportBuildingenergyinfocity.getAreacode()!=null){
               qW.eq("areacode",zgsReportBuildingenergyinfocity.getAreacode());
           }
        }
        qW.eq("reporttm",reporttm);
        qW.ne("isdelete",1);
        qW.orderByDesc("applydate");
        List<ZgsReportBuildingenergyinfocity> buildingEnergyList=zgsReportBuildingenergyinfocityService.list(qW);
        return Result.OK(buildingEnergyList);

    }

    /**
     * 本月完成节能改造情况(项数)
     *
     * @param areacode
     * @param reporttm
     * @param req
     * @return x,y1,y2
     */
    @AutoLog(value = "大屏建筑节能本月完成节能改造情况(项数)")
    @ApiOperation(value="大屏建筑节能本月完成节能改造情况(项数)", notes="大屏建筑节能本月完成节能改造情况(项数)")
    @GetMapping(value = "/screanQuerybywcjngz")
    public Result<Map<String,Object[]>> screanQuerybywcjngz(
            @RequestParam(name = "reporttm", required = false) String reporttm,
           @RequestParam(name = "areacode", required = true) String areacode,
           HttpServletRequest req) {
        String reportmonth = DateUtils.formatDate(new Date(),"YYYY-MM");
        if(StringUtils.isNotEmpty(reportmonth)){
            reportmonth = reporttm;
        }
        String reportYear = reportmonth.split("-")[0];
        String endMonth =reportmonth.split("-")[1];
        //x轴数据
        ArrayList<String> x = new ArrayList<String>();
        ArrayList<String> y1 = new ArrayList<String>();
        ArrayList<String> y2 = new ArrayList<String>();
        for(int i=1;i<=Integer.parseInt(endMonth);i++){
            x.add(String.valueOf(i));
            String monthParam = String.valueOf(i);
            if(monthParam.length()<2){
               monthParam = "0"+monthParam;
           }
            String reporttmParam = reportYear+monthParam;
            //查询数据，取单条，没有数据添加0补数据。
            findByData(areacode,reporttmParam,y1,y2);
        }
        //y轴数据 竣工项目验收情况
        //y轴数据 既有建筑节能改造
        HashMap<String,Object[]> resultData = new HashMap<>();
        resultData.put("x",x.toArray());
        resultData.put("y1",y1.toArray());
        resultData.put("y2",y2.toArray());
        return Result.OK(resultData);

    }

    public void findByData(String areacode,String reporttm,ArrayList<String> y1,ArrayList<String> y2){
        QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
        qW.eq("areacode",areacode);
        qW.eq("reporttm",reporttm);
        qW.ne("isdelete",1);
        ZgsReportBuildingenergyinfocity info = zgsReportBuildingenergyinfocityService.getOne(qW);
        if(info!=null){
            y1.add(info.getByxjxs().toString());
            y2.add(info.getNcxjxs().toString());
        }else{
            y1.add("0");
            y2.add("0");
        }
    }

    /**
     * 大屏建筑节能本年月数据统计
     *
     * @param zgsReportBuildingenergyinfocity
     * @param req
     * @return
     */
    @AutoLog(value = "大屏建筑节能本年月数据统计")
    @ApiOperation(value="大屏建筑节能本年月数据统计", notes="大屏建筑节能本年月数据统计")
    @GetMapping(value = "/screanQueryMonthList")
    public Result<?> screanQueryMonthList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
                                     HttpServletRequest req) {
        QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
        String reporttm = DateUtils.formatDate(new Date(),"YYYY");
        if(zgsReportBuildingenergyinfocity!=null){
            if(zgsReportBuildingenergyinfocity.getReporttm()!=null){
                reporttm = zgsReportBuildingenergyinfocity.getReporttm();
            }
            if(zgsReportBuildingenergyinfocity.getAreacode()!=null){
                qW.eq("areacode",zgsReportBuildingenergyinfocity.getAreacode());
            }
        }
        qW.like("reporttm",reporttm);
        qW.ne("isdelete",1);
        qW.orderByAsc("reporttm");
        List<ZgsReportBuildingenergyinfocity> resultList = zgsReportBuildingenergyinfocityService.list(qW);
        return Result.OK(resultList);
    }


    /**
     * 大屏 - 首页 - 中部 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-中部-查询")
    @ApiOperation(value="大屏-首页-中部-查询", notes="大屏-首页-中部-查询")
    @GetMapping(value = "/screanHomePageMiddleList")
    public Result<Map<String ,Object>> screanHomePageMiddleList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        List<Map> greenList = new ArrayList<>();
        Map yjjcMap = new HashMap();
        yjjcMap.put("yjjc", "北京市互联网项目");
        yjjcMap.put("cscs", "1天");
        yjjcMap.put("zjpscs", "2天");
        yjjcMap.put("zscs", "7天");
        greenList.add(yjjcMap);
        Map yjjcMap2 = new HashMap();
        yjjcMap2.put("yjjc", "兰州市报警系统检查项目");
        yjjcMap2.put("cscs", "5天");
        yjjcMap2.put("zjpscs", "6天");
        yjjcMap2.put("zscs", "9天");
        greenList.add(yjjcMap2);
        Map yjjcMap3 = new HashMap();
        yjjcMap3.put("yjjc", "北京市通州区市政办公项目");
        yjjcMap3.put("cscs", "3天");
        yjjcMap3.put("zjpscs", "8天");
        yjjcMap3.put("zscs", "9天");
        greenList.add(yjjcMap3);
        Map yjjcMap4 = new HashMap();
        yjjcMap4.put("yjjc", "天津市投标保函业务项目");
        yjjcMap4.put("cscs", "4天");
        yjjcMap4.put("zjpscs", "5天");
        yjjcMap4.put("zscs", "20天");
        greenList.add(yjjcMap4);
        Map yjjcMap5 = new HashMap();
        yjjcMap5.put("yjjc", "甘肃省新兴类示范类项目");
        yjjcMap5.put("cscs", "6天");
        yjjcMap5.put("zjpscs", "10天");
        yjjcMap5.put("zscs", "15天");
        greenList.add(yjjcMap5);
        Map yjjcMap6 = new HashMap();
        yjjcMap6.put("yjjc", "山西省建筑类科研项目");
        yjjcMap6.put("cscs", "9天");
        yjjcMap6.put("zjpscs", "12天");
        yjjcMap6.put("zscs", "16天");
        greenList.add(yjjcMap6);
        //大屏列表数据
        resultMap.put("greenList",greenList);
        //大屏顶部数据 大屏项目总数
        resultMap.put("homePageCount_xmzs","30641");
        //大屏顶部数据 大屏绿色建筑总数
        resultMap.put("homePageCount_jzkj","12690");
        //大屏顶部数据 大屏强制推广类总数
        resultMap.put("homePageCount_lsjzsf","8960");
        //大屏顶部数据 未开工情况数量
        resultMap.put("homePageCount_jzgcsf","4568");
        resultMap.put("homePageCount_szgcsf","2589");
        resultMap.put("homePageCount_zpsjz","1453");
        resultMap.put("homePageCount_jzjnsf","787");
        return Result.OK(resultMap);
    }


    /**
     * 大屏 - 首页 - 具体数据信息 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-具体数据信息-查询")
    @ApiOperation(value="大屏-首页-具体数据信息-查询", notes="大屏-首页-具体数据信息-查询")
    @GetMapping(value = "/screanHomePageJtsjxxList")
    public Result<Map<String ,Object>> screanHomePageJtsjxxList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("lsjzpfmjCount","4853万");
        resultMap.put("lsjzCount","2698万");
        resultMap.put("zpsjzCount","896万");
        resultMap.put("jzjnCount","1259万");
        return Result.OK(resultMap);
    }



    /**
     * 大屏 - 首页 - 项目占比统计 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-项目占比统计-查询")
    @ApiOperation(value="大屏-首页-项目占比统计-查询", notes="大屏-首页-项目占比统计-查询")
    @GetMapping(value = "/screanHomePageXmzbqkList")
    public Result<Map<String ,Object>> screanHomePageXmzbqkList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("绿色建筑示范","30");
        jsonObject.put("建筑工程示范","15");
        jsonObject.put("市政工程示范","10");
        jsonObject.put("建筑节能示范","12");
        jsonObject.put("装配式建筑","15");
        jsonObject.put("科技类项目","16");
        resultMap.put("listData", jsonObject);
        return Result.OK(resultMap);
    }



    /**
     * 大屏 - 首页 - 提交申请统计曲线图 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-提交申请统计曲线图-查询")
    @ApiOperation(value="大屏-首页-提交申请统计曲线图-查询", notes="大屏-首页-提交申请统计曲线图-查询")
    @GetMapping(value = "/screanHomePageTjsqtjqxtList")
    public Result<Map<String ,Object>> screanHomePageTjsqtjqxtList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("1","10");
        jsonObject.put("2","15");
        jsonObject.put("3","20");
        jsonObject.put("4","16");
        jsonObject.put("5","28");
        jsonObject.put("6","35");
        jsonObject.put("7", "23");
        jsonObject.put("8", "28");
        jsonObject.put("9", "19");
        jsonObject.put("10", "24");
        jsonObject.put("11", "15");
        jsonObject.put("12", "6");
        resultMap.put("kylxm", jsonObject);
        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("1","6");
        jsonObject2.put("2","12");
        jsonObject2.put("3","24");
        jsonObject2.put("4","26");
        jsonObject2.put("5","15");
        jsonObject2.put("6","11");
        jsonObject2.put("7", "19");
        jsonObject2.put("8", "28");
        jsonObject2.put("9", "25");
        jsonObject2.put("10", "19");
        jsonObject2.put("11", "15");
        jsonObject2.put("12", "6");
        resultMap.put("sflxm", jsonObject2);
        return Result.OK(resultMap);
    }



    /**
     * 大屏 - 首页 - 申报通过率 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-申报通过率-查询")
    @ApiOperation(value="大屏-首页-申报通过率-查询", notes="大屏-首页-申报通过率-查询")
    @GetMapping(value = "/screanHomePageSbtglList")
    public Result<Map<String ,Object>> screanHomePageSbtglList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("sbzs","2000");
        resultMap.put("hgs","1500");
        resultMap.put("tgl","75");
        resultMap.put("wtg","25");
        return Result.OK(resultMap);
    }



    /**
     * 大屏 - 首页 - 甘肃省申报实时数据 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-甘肃省申报实时数据-查询")
    @ApiOperation(value="大屏-首页-甘肃省申报实时数据-查询", notes="大屏-首页-甘肃省申报实时数据-查询")
    @GetMapping(value = "/screanHomePageGsssbsssjList")
    public Result<Map<String ,Object>> screanHomePageGsssbsssjList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        HashMap<String,Object> resultMap = new HashMap<String,Object>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("兰州市", "100");
        jsonObject.put("天水市", "80");
        jsonObject.put("白银市", "79");
        jsonObject.put("平凉市", "65");
        jsonObject.put("庆阳市", "72");
        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("兰州市", "93");
        jsonObject2.put("天水市", "48");
        jsonObject2.put("白银市", "39");
        jsonObject2.put("平凉市", "29");
        jsonObject2.put("庆阳市", "46");
        resultMap.put("kylxm", jsonObject);
        resultMap.put("sflxm", jsonObject2);
        return Result.OK(resultMap);
    }



    /**
     * 大屏 - 首页 - 实时滚动数据 - 查询
     *
     * @param zgsReportMonthgreenbuild
     * @param req
     * @return Map<String ,Object>
     */
    @AutoLog(value = "大屏-首页-实时滚动数据-查询")
    @ApiOperation(value="大屏-首页-实时滚动数据-查询", notes="大屏-首页-实时滚动数据-查询")
    @GetMapping(value = "/screanHomePageSsgdsjList")
    public Result<JSONArray> screanHomePageSsgdsjList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
        Map mapOne = new HashMap();
        mapOne.put("xmmc", "北京市大兴A类科研项目");
        mapOne.put("sbss", "2020-02-09");
        mapOne.put("zt", "初审");
        mapOne.put("xq", "...");
        Map mapTwo = new HashMap();
        mapTwo.put("xmmc", "河北省建筑系科研项目");
        mapTwo.put("sbss", "2021-05-25");
        mapTwo.put("zt", "终审");
        mapTwo.put("xq", "...");
        Map mapThree = new HashMap();
        mapThree.put("xmmc", "山西省建筑类科研项目");
        mapThree.put("sbss", "2020-04-19");
        mapThree.put("zt", "终审");
        mapThree.put("xq", "...");
        Map mapFour = new HashMap();
        mapFour.put("xmmc", "河南省洛阳市科研项目");
        mapFour.put("sbss", "2021-06-24");
        mapFour.put("zt", "终审");
        mapFour.put("xq", "...");
        Map mapFive = new HashMap();
        mapFive.put("xmmc", "北京市通州区建筑类科研项目");
        mapFive.put("sbss", "2021-08-13");
        mapFive.put("zt", "初审");
        mapFive.put("xq", "...");
        Map mapSix = new HashMap();
        mapSix.put("xmmc", "甘肃省新兴类示范类项目");
        mapSix.put("sbss", "2021-05-09");
        mapSix.put("zt", "初审");
        mapSix.put("xq", "...");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(mapOne);
        jsonArray.add(mapTwo);
        jsonArray.add(mapThree);
        jsonArray.add(mapFour);
        jsonArray.add(mapFive);
        jsonArray.add(mapSix);
        return Result.OK(jsonArray);
    }
}
