package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenprojectplanarrange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 绿色建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsGreenprojectplanarrangeService extends IService<ZgsGreenprojectplanarrange> {

}
