package org.jeecg.modules.green.kyxmjtsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研结题经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsScientificpostexpendclearMapper extends BaseMapper<ZgsScientificpostexpendclear> {

}
