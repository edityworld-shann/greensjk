package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 专家评审屏蔽设置表
 * @Author: jeecg-boot
 * @Date: 2022-02-26
 * @Version: V1.0
 */
@Data
@TableName("zgs_expertset")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_expertset对象", description = "专家评审屏蔽设置表")
public class ZgsExpertset implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * 专家主键
     */
    @Excel(name = "专家主键", width = 15)
    @ApiModelProperty(value = "专家主键")
    private java.lang.String expertguid;
    /**
     * 项目主键
     */
    @Excel(name = "项目主键", width = 15)
    @ApiModelProperty(value = "项目主键")
    private java.lang.String businessguid;
    /**
     * 屏蔽配置组合值
     */
    @Excel(name = "屏蔽配置组合值", width = 15)
    @ApiModelProperty(value = "屏蔽配置组合值")
    private java.lang.String setvalue;
    /**
     * 创建账号
     */
    @Excel(name = "创建账号", width = 15)
    @ApiModelProperty(value = "创建账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建用户名称
     */
    @Excel(name = "创建用户名称", width = 15)
    @ApiModelProperty(value = "创建用户名称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    @Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
