package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfocity;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonew;
import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfocityService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewdetailService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_buildingenergyinfocity
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="建筑节能统计管理（市州）")
@RestController
@RequestMapping("/report/zgsReportBuildingenergyinfocity")
@Slf4j
public class ZgsReportBuildingenergyinfocityController extends JeecgController<ZgsReportBuildingenergyinfocity, IZgsReportBuildingenergyinfocityService> {
	@Autowired
	private IZgsReportBuildingenergyinfocityService zgsReportBuildingenergyinfocityService;

	@Autowired
	private IZgsReportBuildingenergyinfonewdetailService zgsReportBuildingenergyinfonewdetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportBuildingenergyinfocity
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（市州）-分页列表查询")
	@ApiOperation(value="建筑节能统计管理（市州）-分页列表查询", notes="建筑节能统计管理（市州）-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                   @RequestParam(name = "applyState", required = false) String applyState,
								   HttpServletRequest req) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
		if(zgsReportBuildingenergyinfocity!=null){
			if(zgsReportBuildingenergyinfocity.getApplystate()!=null){
				//有问题，BigDecimal applystate 需要判空？
				BigDecimal applystate  = zgsReportBuildingenergyinfocity.getApplystate();
				qW.eq("applystate",applystate);
			}
			Integer userType = sysUser.getLoginUserType();
			//6县区，7市区
			if(userType==7){
				qW.eq("areacode",sysUser.getAreacode());
			}
			if(StringUtils.isNotEmpty(zgsReportBuildingenergyinfocity.getReporttm())){
				String reporttm  = zgsReportBuildingenergyinfocity.getReporttm();
				qW.eq("reporttm",reporttm);
			}
		}
    //  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
    if(applyState != null){
      splitArray = applyState.split(",");
    }
    if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
		qW.ne("isdelete",1);
		qW.orderByDesc("createtime");
		Page<ZgsReportBuildingenergyinfocity> page = new Page<ZgsReportBuildingenergyinfocity>(pageNo, pageSize);
		IPage<ZgsReportBuildingenergyinfocity> pageList = zgsReportBuildingenergyinfocityService.page(page, qW);
		for(ZgsReportBuildingenergyinfocity b : pageList.getRecords()){
			QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
			qWdetail.eq("buildingenergyinfonewguid",b.getId());
			List<ZgsReportBuildingenergyinfonewdetail> detailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
			zgsReportBuildingenergyinfocity.setZgsReportBuildingenergyinfonewdetailList(detailList);
		}
		return Result.OK(pageList);

	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportBuildingenergyinfocity
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（市州）-添加")
	@ApiOperation(value="建筑节能统计管理（市州）-添加", notes="建筑节能统计管理（市州）-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity) {

		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		//生成主表ID
		String id = UUID.randomUUID().toString();
		zgsReportBuildingenergyinfocity.setId(id);
		//申报状态  0未上报
		zgsReportBuildingenergyinfocity.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
		//行政区域
    zgsReportBuildingenergyinfocity.setAreacode(sysUser.getAreacode());
    zgsReportBuildingenergyinfocity.setAreaname(sysUser.getAreaname());
    zgsReportBuildingenergyinfocity.setIsdelete(new BigDecimal(0));
    zgsReportBuildingenergyinfocity.setCreatepersonaccount(sysUser.getUsername());
    zgsReportBuildingenergyinfocity.setCreatepersonname(sysUser.getRealname());
    zgsReportBuildingenergyinfocity.setCreatetime(new Date());
//    zgsReportBuildingenergyinfocity.setApplydate(new Date());
		zgsReportBuildingenergyinfocityService.save(zgsReportBuildingenergyinfocity);
		//新增明细数据
		List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfocity.getZgsReportBuildingenergyinfonewdetailList();

		if(zgsReportBuildingenergyinfonewdetailList!=null&&zgsReportBuildingenergyinfonewdetailList.size()>0){
			for(ZgsReportBuildingenergyinfonewdetail dtl:zgsReportBuildingenergyinfonewdetailList){
				//生成明细表ID
				String id2 = UUID.randomUUID().toString();
				dtl.setId(id2);
				//关联主表ID
				dtl.setBuildingenergyinfonewguid(id);
				zgsReportBuildingenergyinfonewdetailService.save(dtl);
			}
		}
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportBuildingenergyinfocity
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（市州）-编辑")
	@ApiOperation(value="建筑节能统计管理（市州）-编辑", notes="建筑节能统计管理（市州）-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (zgsReportBuildingenergyinfocity != null && StringUtils.isNotEmpty(zgsReportBuildingenergyinfocity.getId())) {
			String id = zgsReportBuildingenergyinfocity.getId();
			if (GlobalConstants.apply_state1.equals(zgsReportBuildingenergyinfocity.getApplystate())) {
				//保存并上报
				zgsReportBuildingenergyinfocity.setApplydate(new Date());
			}
			if (GlobalConstants.apply_state3.equals(zgsReportBuildingenergyinfocity.getApplystate())) {
				//退回
				zgsReportBuildingenergyinfocity.setBackrdate(new Date());
			}
			zgsReportBuildingenergyinfocity.setModifypersonaccount(sysUser.getUsername());
			zgsReportBuildingenergyinfocity.setModifypersonname(sysUser.getRealname());
			zgsReportBuildingenergyinfocity.setModifytime(new Date());
			zgsReportBuildingenergyinfocityService.updateById(zgsReportBuildingenergyinfocity);
			//更新明细表数据
			List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfocitydetailList = zgsReportBuildingenergyinfocity.getZgsReportBuildingenergyinfonewdetailList();
			if(zgsReportBuildingenergyinfocitydetailList!=null&&zgsReportBuildingenergyinfocitydetailList.size()>0){
				for(ZgsReportBuildingenergyinfonewdetail dtl:zgsReportBuildingenergyinfocitydetailList){
					if (StringUtils.isNotEmpty(dtl.getId())) {
						//编辑
						zgsReportBuildingenergyinfonewdetailService.updateById(dtl);
					} else {
						//新增//生成明细表ID
						String id2 = UUID.randomUUID().toString();
						dtl.setId(id2);
						//关联主表ID
						dtl.setBuildingenergyinfonewguid(id);
						zgsReportBuildingenergyinfonewdetailService.save(dtl);
					}

				}
			}else{
				//删除明细表数据
				QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
				qWdetail.eq("buildingenergyinfonewguid",id);
				zgsReportBuildingenergyinfonewdetailService.remove(qWdetail);
			}
		}

		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfocity-通过id删除")
	@ApiOperation(value="zgs_report_buildingenergyinfocity-通过id删除", notes="zgs_report_buildingenergyinfocity-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportBuildingenergyinfocityService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfocity-批量删除")
	@ApiOperation(value="zgs_report_buildingenergyinfocity-批量删除", notes="zgs_report_buildingenergyinfocity-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportBuildingenergyinfocityService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（市州）-通过id查询")
	@ApiOperation(value="建筑节能统计管理（市州）-通过id查询", notes="建筑节能统计管理（市州）-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity = zgsReportBuildingenergyinfocityService.getById(id);
		if(zgsReportBuildingenergyinfocity==null) {
			return Result.error("未找到对应数据");
		}else{
			QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
			qWdetail.eq("buildingenergyinfonewguid",id);
			List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
			zgsReportBuildingenergyinfocity.setZgsReportBuildingenergyinfonewdetailList(zgsReportBuildingenergyinfonewdetailList);
		}

		return Result.OK(zgsReportBuildingenergyinfocity);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportBuildingenergyinfocity
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity) {
        return super.exportXls(request, zgsReportBuildingenergyinfocity, ZgsReportBuildingenergyinfocity.class, "zgs_report_buildingenergyinfocity");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportBuildingenergyinfocity.class);
    }


	 /**
	  * 分页列表查询
	  *
	  * @param zgsReportBuildingenergyinfocity
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "大屏建筑节能查询")
	 @ApiOperation(value="大屏建筑节能查询", notes="大屏建筑节能查询")
	 @GetMapping(value = "/screanQueryList")
	 public Result<?> screanQueryList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
									HttpServletRequest req) {
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
		 String reporttm = DateUtils.formatDate(new Date(),"YYYY-MM");
		 if(zgsReportBuildingenergyinfocity!=null){
		 	if(zgsReportBuildingenergyinfocity.getReporttm()!=null){
				reporttm = zgsReportBuildingenergyinfocity.getReporttm();
			}
		 	if(zgsReportBuildingenergyinfocity.getAreacode()!=null){
				qW.eq("areacode",zgsReportBuildingenergyinfocity.getAreacode());
			}
		 }
		 qW.eq("reporttm",reporttm);
		 qW.ne("isdelete",1);
		 qW.orderByDesc("applydate");
		 List<ZgsReportBuildingenergyinfocity> buildingEnergyList=zgsReportBuildingenergyinfocityService.list(qW);
		 return Result.OK(buildingEnergyList);

	 }

	 /**
	  * 本月完成节能改造情况(项数)
	  *
	  * @param areacode
	  * @param reporttm
	  * @param req
	  * @return x,y1,y2
	  */
	 @AutoLog(value = "大屏建筑节能本月完成节能改造情况(项数)")
	 @ApiOperation(value="大屏建筑节能本月完成节能改造情况(项数)", notes="大屏建筑节能本月完成节能改造情况(项数)")
	 @GetMapping(value = "/screanQuerybywcjngz")
	 public Result<Map<String,Object[]>> screanQuerybywcjngz(
	 		@RequestParam(name = "reporttm", required = false) String reporttm,
			@RequestParam(name = "areacode", required = true) String areacode,
			HttpServletRequest req) {
		 String reportmonth = DateUtils.formatDate(new Date(),"YYYY-MM");
		 if(StringUtils.isNotEmpty(reportmonth)){
			 reportmonth = reporttm;
		 }
		 String reportYear = reportmonth.split("-")[0];
		 String endMonth =reportmonth.split("-")[1];
		 //x轴数据
		 ArrayList<String> x = new ArrayList<String>();
		 ArrayList<String> y1 = new ArrayList<String>();
		 ArrayList<String> y2 = new ArrayList<String>();
		 for(int i=1;i<=Integer.parseInt(endMonth);i++){
		 	x.add(String.valueOf(i));
		 	String monthParam = String.valueOf(i);
		 	if(monthParam.length()<2){
				monthParam = "0"+monthParam;
			}
			 String reporttmParam = reportYear+monthParam;
			 //查询数据，取单条，没有数据添加0补数据。
			 findByData(areacode,reporttmParam,y1,y2);
		 }
		 //y轴数据 竣工项目验收情况
		 //y轴数据 既有建筑节能改造
		 HashMap<String,Object[]> resultData = new HashMap<>();
		 resultData.put("x",x.toArray());
		 resultData.put("y1",y1.toArray());
		 resultData.put("y2",y2.toArray());
		 return Result.OK(resultData);

	 }

	 public void findByData(String areacode,String reporttm,ArrayList<String> y1,ArrayList<String> y2){
		 QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
		 qW.eq("areacode",areacode);
		 qW.eq("reporttm",reporttm);
		 qW.ne("isdelete",1);
		 ZgsReportBuildingenergyinfocity info = zgsReportBuildingenergyinfocityService.getOne(qW);
		 if(info!=null){
			 y1.add(info.getByxjxs().toString());
			 y2.add(info.getNcxjxs().toString());
		 }else{
			 y1.add("0");
			 y2.add("0");
		 }
	 }

	 /**
	  * 大屏建筑节能本年月数据统计
	  *
	  * @param zgsReportBuildingenergyinfocity
	  * @param req
	  * @return
	  */
	 @AutoLog(value = "大屏建筑节能本年月数据统计")
	 @ApiOperation(value="大屏建筑节能本年月数据统计", notes="大屏建筑节能本年月数据统计")
	 @GetMapping(value = "/screanQueryMonthList")
	 public Result<?> screanQueryMonthList(ZgsReportBuildingenergyinfocity zgsReportBuildingenergyinfocity,
									  HttpServletRequest req) {
		 QueryWrapper<ZgsReportBuildingenergyinfocity> qW = new QueryWrapper<>();
		 String reporttm = DateUtils.formatDate(new Date(),"YYYY");
		 if(zgsReportBuildingenergyinfocity!=null){
			 if(zgsReportBuildingenergyinfocity.getReporttm()!=null){
				 reporttm = zgsReportBuildingenergyinfocity.getReporttm();
			 }
			 if(zgsReportBuildingenergyinfocity.getAreacode()!=null){
				 qW.eq("areacode",zgsReportBuildingenergyinfocity.getAreacode());
			 }
		 }
		 qW.like("reporttm",reporttm);
		 qW.ne("isdelete",1);
		 qW.orderByAsc("reporttm");
		 List<ZgsReportBuildingenergyinfocity> resultList = zgsReportBuildingenergyinfocityService.list(qW);
		 return Result.OK(resultList);
	 }
 }
