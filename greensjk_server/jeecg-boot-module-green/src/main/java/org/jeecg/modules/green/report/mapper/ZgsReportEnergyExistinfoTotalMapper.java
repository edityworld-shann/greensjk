package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfoTotal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;

/**
 * @Description: 既有建筑节能改造完成情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface ZgsReportEnergyExistinfoTotalMapper extends BaseMapper<ZgsReportEnergyExistinfoTotal> {
    ZgsReportEnergyExistinfoTotal totalMonthExistinfoData(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyExistinfoTotal totalMonthExistinfoDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    ZgsReportEnergyExistinfoTotal totalMonthExistinfoDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyExistinfoTotal totalMonthExistinfoDataYearCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    //省厅账号计算市州合计值（过滤条件：审核通过状态）
    ZgsReportEnergyExistinfoTotal lastExistTotalDataProvince(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    //市州账号计算区县合计值（过滤条件：审核通过状态）
    ZgsReportEnergyExistinfoTotal lastExistTotalDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    /**
     * 查询所有既有汇总存在，且既有清单不存在的既有汇总数据
     *
     * @return
     */
    List<ZgsReportEnergyExistinfoTotal> selectExistInfoTotalNotInExistInfoList();
}
