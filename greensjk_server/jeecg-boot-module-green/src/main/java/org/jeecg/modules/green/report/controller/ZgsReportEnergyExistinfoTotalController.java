package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoTotalService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 既有建筑节能改造完成情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Api(tags = "既有建筑节能改造完成情况汇总表")
@RestController
@RequestMapping("/report/zgsReportEnergyExistinfoTotal")
@Slf4j
public class ZgsReportEnergyExistinfoTotalController extends JeecgController<ZgsReportEnergyExistinfoTotal, IZgsReportEnergyExistinfoTotalService> {
    @Autowired
    private IZgsReportEnergyExistinfoTotalService zgsReportEnergyExistinfoTotalService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页列表查询
     *
     * @param zgsReportEnergyExistinfoTotal
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-分页列表查询")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-分页列表查询", notes = "既有建筑节能改造完成情况汇总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "99") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportEnergyExistinfoTotal != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfoTotal.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyExistinfoTotal.getFilltm());
            }
            if (zgsReportEnergyExistinfoTotal.getApplystate() != null) {
                if (zgsReportEnergyExistinfoTotal.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportEnergyExistinfoTotal.getApplystate());
                    applystate = zgsReportEnergyExistinfoTotal.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfoTotal.getAreacode())) {
                if (zgsReportEnergyExistinfoTotal.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportEnergyExistinfoTotal.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
                } else if (zgsReportEnergyExistinfoTotal.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyExistinfoTotal.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfoTotal.getAreacode())) {
                if (zgsReportEnergyExistinfoTotal.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyExistinfoTotal.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportEnergyExistinfoTotal.getAreacode());
                    queryWrapper.isNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.isNull("area_type");
                queryWrapper.orderByAsc("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByAsc("areacode");
        Page<ZgsReportEnergyExistinfoTotal> page = new Page<ZgsReportEnergyExistinfoTotal>(pageNo, 99);
        IPage<ZgsReportEnergyExistinfoTotal> pageList = zgsReportEnergyExistinfoTotalService.page(page, queryWrapper);
        ZgsReportEnergyExistinfoTotal total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加所有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfoTotal.getAreacode())) {
                if (zgsReportEnergyExistinfoTotal.getAreacode().length() == 4 || zgsReportEnergyExistinfoTotal.getAreacode().length() == 6) {
                    total = zgsReportEnergyExistinfoTotalService.lastExistTotalDataCity(zgsReportEnergyExistinfoTotal.getFilltm(), zgsReportEnergyExistinfoTotal.getAreacode(), applystate);
                } else {
                    total = zgsReportEnergyExistinfoTotalService.lastExistTotalDataProvince(zgsReportEnergyExistinfoTotal.getFilltm(), zgsReportEnergyExistinfoTotal.getAreacode(), applystate);
                }
            } else {
                total = zgsReportEnergyExistinfoTotalService.lastExistTotalDataProvince(zgsReportEnergyExistinfoTotal.getFilltm(), zgsReportEnergyExistinfoTotal.getAreacode(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportEnergyExistinfoTotal> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("filltm", zgsReportEnergyExistinfoTotal.getFilltm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            ZgsReportEnergyExistinfoTotal totalCity = zgsReportEnergyExistinfoTotalService.getOne(totalQueryWrapper);
            total = zgsReportEnergyExistinfoTotalService.lastExistTotalDataCity(zgsReportEnergyExistinfoTotal.getFilltm(), sysUser.getAreacode(), applystate);
            if (total != null) {
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (pageList.getRecords().size() > 0) {
            if (total != null) {
                pageList.getRecords().add(total);
            }
        }
        if (pageList.getRecords().size() > 1) {
            pageList.getRecords().get(pageList.getRecords().size() - 1).setTotalTag("1");
            pageList.getRecords().get(pageList.getRecords().size() - 1).setAreaname("合计");
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                pageList.getRecords().get(pageList.getRecords().size() - 1).setApplystate(null);
            }
        }
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 既有建筑节能改造完成情况汇总表-审批
     *
     * @param id
     * @param spStatus
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-审批")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-审批", notes = "既有建筑节能改造完成情况汇总表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestParam(name = "id") String id,
                               @RequestParam(name = "spStatus") Integer spStatus,
                               @RequestParam(name = "backreason", required = false) String backreason) {
        zgsReportEnergyExistinfoTotalService.spProject(id, spStatus, backreason);
        return Result.OK("操作成功!");
    }

    /**
     * 绿色建筑汇总表-本月一键上报（市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-本月一键上报（市州）")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-本月一键上报（市州）", notes = "既有建筑节能改造完成情况汇总表-本月一键上报（市州）")
    @GetMapping(value = "/initMonthNewInfoTotal")
    public Result<?> initMonthNewInfoTotal(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "Exist" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (StringUtils.isNotEmpty(filltm)) {
            yearMonth = filltm.substring(0, 7);
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        if (!filltm.equals(yearMonth)) {
//            return Result.error("上报日期错误！");
        }
        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryWrapper = new QueryWrapper();
        queryWrapper.eq("filltm", filltm);
        queryWrapper.likeRight("areacode", sysUser.getAreacode());
        queryWrapper.isNull("area_type");
        queryWrapper.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportEnergyExistinfoTotal> list = zgsReportEnergyExistinfoTotalService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("请先处理既有建筑节能改造完成情况汇总待审批记录！");
        }
        //再判断本月是否已上报
        QueryWrapper<ZgsReportEnergyExistinfoTotal> wrapper = new QueryWrapper();
        wrapper.eq("filltm", filltm);
        wrapper.eq("areacode", sysUser.getAreacode());
        wrapper.eq("applystate", GlobalConstants.apply_state2);
        wrapper.isNotNull("area_type");
        List<ZgsReportEnergyExistinfoTotal> listTotal = zgsReportEnergyExistinfoTotalService.list(wrapper);
        if (listTotal.size() > 0) {
            return Result.error("本月汇总数据已上报！");
        }
//        zgsReportEnergyExistinfoTotalService.initEnergyExistinfoTotalCity(year, quarter, filltm);
        zgsReportEnergyExistinfoTotalService.initEnergyExistinfoTotalCityAll(year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser, 0, GlobalConstants.apply_state1);
        return Result.OK("本月汇总一键上报成功！");
    }

    /**
     * 添加
     *
     * @param zgsReportEnergyExistinfoTotal
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-添加")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-添加", notes = "既有建筑节能改造完成情况汇总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal) {
        zgsReportEnergyExistinfoTotalService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportEnergyExistinfoTotal
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-编辑")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-编辑", notes = "既有建筑节能改造完成情况汇总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal) {
        zgsReportEnergyExistinfoTotalService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-通过id删除")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-通过id删除", notes = "既有建筑节能改造完成情况汇总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportEnergyExistinfoTotalService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-批量删除")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-批量删除", notes = "既有建筑节能改造完成情况汇总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportEnergyExistinfoTotalService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "既有建筑节能改造完成情况汇总表-通过id查询")
    @ApiOperation(value = "既有建筑节能改造完成情况汇总表-通过id查询", notes = "既有建筑节能改造完成情况汇总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportEnergyExistinfoTotalService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportEnergyExistinfoTotal == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportEnergyExistinfoTotal);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportEnergyExistinfoTotal
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  HttpServletRequest req) {
        String title = "既有建筑节能改造完成情况汇总表";
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsReportEnergyExistinfoTotal, 1, 9999, req);
        IPage<ZgsReportEnergyExistinfoTotal> pageList = (IPage<ZgsReportEnergyExistinfoTotal>) result.getResult();
        List<ZgsReportEnergyExistinfoTotal> list = pageList.getRecords();
        List<ZgsReportEnergyExistinfoTotal> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportEnergyExistinfoTotal.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportEnergyExistinfoTotal, ZgsReportEnergyExistinfoTotal.class, "既有建筑节能改造完成情况汇总表");
        return mv;
    }

    private String getId(ZgsReportEnergyExistinfoTotal item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportEnergyExistinfoTotal.class);
    }

}
