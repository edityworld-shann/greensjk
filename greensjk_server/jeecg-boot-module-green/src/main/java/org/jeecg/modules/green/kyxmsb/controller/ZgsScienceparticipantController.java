package org.jeecg.modules.green.kyxmsb.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Api(tags="参加单位及人员的基本情况")
@RestController
@RequestMapping("/kyxmsb/zgsScienceparticipant")
@Slf4j
public class ZgsScienceparticipantController extends JeecgController<ZgsScienceparticipant, IZgsScienceparticipantService> {
	@Autowired
	private IZgsScienceparticipantService zgsScienceparticipantService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsScienceparticipant
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-分页列表查询")
	@ApiOperation(value="参加单位及人员的基本情况-分页列表查询", notes="参加单位及人员的基本情况-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsScienceparticipant zgsScienceparticipant,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsScienceparticipant> queryWrapper = QueryGenerator.initQueryWrapper(zgsScienceparticipant, req.getParameterMap());
		Page<ZgsScienceparticipant> page = new Page<ZgsScienceparticipant>(pageNo, pageSize);
		IPage<ZgsScienceparticipant> pageList = zgsScienceparticipantService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsScienceparticipant
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-添加")
	@ApiOperation(value="参加单位及人员的基本情况-添加", notes="参加单位及人员的基本情况-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsScienceparticipant zgsScienceparticipant) {
		zgsScienceparticipantService.save(zgsScienceparticipant);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsScienceparticipant
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-编辑")
	@ApiOperation(value="参加单位及人员的基本情况-编辑", notes="参加单位及人员的基本情况-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsScienceparticipant zgsScienceparticipant) {
		zgsScienceparticipantService.updateById(zgsScienceparticipant);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-通过id删除")
	@ApiOperation(value="参加单位及人员的基本情况-通过id删除", notes="参加单位及人员的基本情况-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsScienceparticipantService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-批量删除")
	@ApiOperation(value="参加单位及人员的基本情况-批量删除", notes="参加单位及人员的基本情况-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsScienceparticipantService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "参加单位及人员的基本情况-通过id查询")
	@ApiOperation(value="参加单位及人员的基本情况-通过id查询", notes="参加单位及人员的基本情况-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsScienceparticipant zgsScienceparticipant = zgsScienceparticipantService.getById(id);
		if(zgsScienceparticipant==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsScienceparticipant);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsScienceparticipant
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsScienceparticipant zgsScienceparticipant) {
        return super.exportXls(request, zgsScienceparticipant, ZgsScienceparticipant.class, "参加单位及人员的基本情况");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsScienceparticipant.class);
    }

}
