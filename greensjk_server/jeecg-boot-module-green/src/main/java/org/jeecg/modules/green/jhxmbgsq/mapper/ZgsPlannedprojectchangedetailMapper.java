package org.jeecg.modules.green.jhxmbgsq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 建设科技计划项目变更内容记录表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsPlannedprojectchangedetailMapper extends BaseMapper<ZgsPlannedprojectchangedetail> {

}
