package org.jeecg.modules.green.largeScreen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.green.largeScreen.po.*;
import org.jeecg.modules.green.largeScreen.service.LargeScreenZgsReportEnergyNewinfoGreentotalService;
import org.jeecg.modules.green.largeScreen.vo.PrefabricaeZbVo;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyNewinfoGreentotalMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LargeScreenZgsReportEnergyNewinfoGreentotalServiceImpl extends ServiceImpl<ZgsReportEnergyNewinfoGreentotalMapper, ZgsReportEnergyNewinfoGreentotal> implements LargeScreenZgsReportEnergyNewinfoGreentotalService {
    @Override
    public SpecificDataInformationPo specificDataInformation(String year, String areacodel) {
        return this.baseMapper.specificDataInformation(year, areacodel);
    }

    @Override
    public AssemblyDataStatisticsPo assemblyDataStatistics(String year) {
        return this.baseMapper.assemblyDataStatistics(year);
    }

    @Override
    public TotalTitemsPo totalTitems(String year, String areacodel) {
        return this.baseMapper.totalTitems(year, areacodel);
    }

    @Override
    public List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYear(String year) {
        return this.baseMapper.accumulatedGreenBuildingGradeInThisYear(year);
    }

    @Override
    public List<MoonGreenBuildingPo> greenBuildingData(String year, String month, String areacodel) {
        return this.baseMapper.greenBuildingData(year, month, areacodel);
    }

    @Override
    public PrefabricatedBuildingPo prefabricatedBuilding(String year, String month, String areacodel) {
        return this.baseMapper.prefabricatedBuilding(year, month, areacodel);
    }

    @Override
    public String newConstructiontoTalArea(String year, String month, String areacodel) {
        return this.baseMapper.newConstructiontoTalArea(year, month, areacodel);
    }

    @Override
    public PrefabricatedBuildingPo fabricatedCompletion(String year, String month, String areacodel) {
        return this.baseMapper.fabricatedCompletion(year, month, areacodel);
    }

    @Override
    public String totalBuildingArea(String year, String month, String areacodel) {
        return this.baseMapper.totalBuildingArea(year, month, areacodel);
    }

    @Override
    public List<BuildingEnergySavingPo> buildingEnergySavingData(String year, String month, String areacodel) {
        return this.baseMapper.buildingEnergySavingData(year, month, areacodel);
    }

    @Override
    public BuildingEnergySavingTotalTitemsPo buildingEnergySavingTotalTitems(String year, String month, String areacode) {
        return this.baseMapper.buildingEnergySavingTotalTitems(year, month, areacode);
    }

    @Override
    public List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergy(String year) {
        return this.baseMapper.utilizationOfRenewableEnergy(year);
    }

    @Override
    public BuildingEnergyEfficiencyStatisticsPo buildingEnergyEfficiencyStatistics(String year, String month, String areacodel) {
        return this.baseMapper.buildingEnergyEfficiencyStatistics(year, month, areacodel);
    }

    @Override
    public List<BuildingEnergySavingPo> buildingEnergySavingExisting(String year, String month, String areacodel) {
        return this.baseMapper.buildingEnergySavingExisting(year, month, areacodel);
    }

    @Override
    public List<City> getAllCities() {
        return this.baseMapper.getAllCities();
    }

    @Override
    public List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYearNew(String year, String code) {
        return this.baseMapper.accumulatedGreenBuildingGradeInThisYearNew(year, code);
    }

    @Override
    public List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyNew(String year, String code) {
        return this.baseMapper.utilizationOfRenewableEnergyNew(year, code);
    }

    @Override
    public List<MoonGreenBuildingPo> moonGreenBuildingPo(String year, String month, String code) {
        return this.baseMapper.moonGreenBuildingPo(year, month, code);
    }

    @Override
    public SpecificDataInformationPo specificDataInformationNew(String year, String areacodel) {
        return this.baseMapper.specificDataInformationNew(year, areacodel);
    }

    @Override
    public BuildingEnergySavingPo buildingEnergySavingPo(String year, String month, String code) {
        return this.baseMapper.buildingEnergySavingPo(year, month, code);
    }

    @Override
    public BuildingEnergySavingPo buildingEnergySavingExistingPo(String year, String month, String code) {
        return this.baseMapper.buildingEnergySavingExistingPo(year, month, code);
    }

    @Override
    public List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInPoList(String year, String areacodel) {
        return this.baseMapper.accumulatedGreenBuildingGradeInPoList(year, areacodel);
    }

    @Override
    public List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyList(String year, String areacode) {
        return this.baseMapper.utilizationOfRenewableEnergyList(year, areacode);
    }

    @Override
    public String getCityOne(String areacodel) {
        return this.baseMapper.getCityOne(areacodel);
    }

    @Override
    public MoonGreenBuildingPo monthMoonGreenBuildingPo(String year, String month, String code) {
        return this.baseMapper.monthMoonGreenBuildingPo(year, month, code);
    }

    @Override
    public List<MoonGreenBuildingPo> monthGreenBuildingData(String year, String month, String areacodel) {
        return this.baseMapper.monthGreenBuildingData(year, month, areacodel);
    }

    @Override
    public List<BuildingEnergySavingPo> monthBuildingEnergySavingPo(String year, String month, String code) {
        return this.baseMapper.monthBuildingEnergySavingPo(year, month, code);
    }

    @Override
    public List<BuildingEnergySavingPo>  monthBuildingEnergySavingExistingPo(String year, String month, String code) {
        return this.baseMapper.monthBuildingEnergySavingExistingPo(year, month, code);
    }

    @Override
    public PrefabricaeZbVo prefabricateBulidngZb(String month, String areacode,String prdType) {
        return this.baseMapper.prefabricateBulidngZb(month, areacode,prdType);
    }

    @Override
    public List<MoonGreenBuildingPo> moonGreenBuildingPoTow(String year, String month, String areacodel) {
        return this.baseMapper.moonGreenBuildingPoTow(month,areacodel);
    }
}
