package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertworkresume;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库人员工作简历
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertworkresumeService extends IService<ZgsExpertworkresume> {

}
