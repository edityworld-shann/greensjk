package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;
import org.apache.poi.hwpf.model.ListData;

import java.io.Serializable;
import java.util.List;

@Data
public class PrefabricatedBuildingPo implements Serializable {
    /**
     * 新开工装配式建筑总面积
     */
    private String totalArea;
    /**
     * 保障性住房
     */
    private String bzArea;

    /**
     * 商品住房
     */
    private String spArea;

    /**
     * 公共建筑
     */
    private String ggArea;
    /**
     * 新开工建筑总面积
     */
    private String newConstructiontoTalArea;

    private List<GreenBuilding> listData;
    private List<GreenBuilding> listDataNew;
}
