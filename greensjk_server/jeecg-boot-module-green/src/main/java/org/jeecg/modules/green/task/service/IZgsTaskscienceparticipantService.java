package org.jeecg.modules.green.task.service;

import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface IZgsTaskscienceparticipantService extends IService<ZgsTaskscienceparticipant> {

}
