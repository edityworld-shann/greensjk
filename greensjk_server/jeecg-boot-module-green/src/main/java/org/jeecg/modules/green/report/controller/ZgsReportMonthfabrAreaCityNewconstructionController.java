package org.jeecg.modules.green.report.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompletedNewPo;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityNewconstruction;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompletedService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityNewconstructionService;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Description: 现用来同步数据使用
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Api(tags = "现用来同步数据使用")
@RestController
@RequestMapping("/report/zgsReportMonthfabrAreaCityNewconstruction")
@Slf4j
public class ZgsReportMonthfabrAreaCityNewconstructionController extends JeecgController<ZgsReportMonthfabrAreaCityNewconstruction, IZgsReportMonthfabrAreaCityNewconstructionService> {
    @Autowired
    private IZgsReportMonthfabrAreaCityNewconstructionService zgsReportMonthfabrAreaCityNewconstructionService;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 数据同步接口
     *
     * @param areaCode
     * @param type
     * @return
     */
    @AutoLog(value = "市州数据同步接口")
    @ApiOperation(value = "市州数据同步接口", notes = "市州数据同步接口")
    @GetMapping(value = "/initDataByAreaCode")
    public Result<?> queryPageList(@RequestParam(name = "areaCode", required = false) String areaCode, @RequestParam(name = "type", required = false) Integer type) {
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lockByTime(areaCode + "-initData", String.valueOf(new Date().getTime()), 60)) {
            return Result.error("数据同步操作频繁，请稍后尝试！");
        }
        if ("甘肃省".equals(areaCode)) {
            areaCode = null;
        }
        fun(areaCode, type);
        return Result.OK("数据同步已在后台运行，请稍后查看！");
    }

    private ExecutorService executor = Executors.newCachedThreadPool();

    public void fun(String areaCode, Integer type) {
        executor.submit(new Runnable() {
            public void run() {
                try {
                    //要执行的业务代码，我们这里没有写方法，可以让线程休息几秒进行测试
                    zgsReportMonthfabrAreaCityNewconstructionService.dataInitByAreaCode(areaCode, type);
                    Thread.sleep(1000);
                } catch (Exception e) {
                    throw new RuntimeException("数据同步异常！！");
                }
            }
        });
    }

    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabrAreaCityNewconstruction
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-分页列表查询")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-分页列表查询", notes = "装配式建筑-新开工-汇总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabrAreaCityNewconstruction zgsReportMonthfabrAreaCityNewconstruction,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsReportMonthfabrAreaCityNewconstruction> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabrAreaCityNewconstruction, req.getParameterMap());
        Page<ZgsReportMonthfabrAreaCityNewconstruction> page = new Page<ZgsReportMonthfabrAreaCityNewconstruction>(pageNo, pageSize);
        IPage<ZgsReportMonthfabrAreaCityNewconstruction> pageList = zgsReportMonthfabrAreaCityNewconstructionService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsReportMonthfabrAreaCityNewconstruction
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-添加")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-添加", notes = "装配式建筑-新开工-汇总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabrAreaCityNewconstruction zgsReportMonthfabrAreaCityNewconstruction) {
        zgsReportMonthfabrAreaCityNewconstructionService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityNewconstruction, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportMonthfabrAreaCityNewconstruction
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-编辑")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-编辑", notes = "装配式建筑-新开工-汇总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabrAreaCityNewconstruction zgsReportMonthfabrAreaCityNewconstruction) {
        zgsReportMonthfabrAreaCityNewconstructionService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityNewconstruction, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-通过id删除")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-通过id删除", notes = "装配式建筑-新开工-汇总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportMonthfabrAreaCityNewconstructionService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-批量删除")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-批量删除", notes = "装配式建筑-新开工-汇总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabrAreaCityNewconstructionService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-新开工-汇总表-通过id查询")
    @ApiOperation(value = "装配式建筑-新开工-汇总表-通过id查询", notes = "装配式建筑-新开工-汇总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabrAreaCityNewconstruction zgsReportMonthfabrAreaCityNewconstruction = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportMonthfabrAreaCityNewconstructionService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportMonthfabrAreaCityNewconstruction == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportMonthfabrAreaCityNewconstruction);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabrAreaCityCompleted zgsReportMonthfabrAreaCityCompleted) {

        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        queryWrapper.orderByAsc("areacode");
        Integer applystate = null;
        if (zgsReportMonthfabrAreaCityCompleted != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityCompleted.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityCompleted.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityCompleted.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityCompleted.getApplystate());
                    applystate = zgsReportMonthfabrAreaCityCompleted.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
                    //                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
                    //                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
                //                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityCompleted.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.orderByAsc("area_type");
            }
            queryWrapper.isNull("area_type");
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.likeRight("areacode", sysUser.getAreacode());
            queryWrapper.isNull("area_type");
        }
        queryWrapper.orderByDesc("createtime");
        if (StringUtils.isNotBlank(zgsReportMonthfabrAreaCityCompleted.getProjecttype())) {
            queryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityCompleted.getProjecttype());
        }

        List<ZgsReportMonthfabrAreaCityCompleted> areaCityCompletedList = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapper);
        List<ZgsReportMonthfabrAreaCityCompletedNewPo> listNew = new ArrayList<>();
        for (ZgsReportMonthfabrAreaCityCompleted zgsReport : areaCityCompletedList
        ) {
            ZgsReportMonthfabrAreaCityCompletedNewPo zgsReportMonthfabrAreaCityCompletedNewPo = new ZgsReportMonthfabrAreaCityCompletedNewPo();
            BeanUtils.copyProperties(zgsReport, zgsReportMonthfabrAreaCityCompletedNewPo);
            listNew.add(zgsReportMonthfabrAreaCityCompletedNewPo);
        }
        ZgsReportMonthfabrAreaCityCompleted total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityCompleted.getAreacode())) {
                if (zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 4 || zgsReportMonthfabrAreaCityCompleted.getAreacode().length() == 6) {
                    total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataCity(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getAreacode(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
                } else {
                    total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataProvince(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
                }
            } else {
                total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataProvince(zgsReportMonthfabrAreaCityCompleted.getReporttm(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityCompleted.getReporttm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            totalQueryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityCompleted.getProjecttype());
            totalQueryWrapper.ne("isdelete", 1);
            totalQueryWrapper.orderByAsc(" createtime");
            ZgsReportMonthfabrAreaCityCompleted totalCity = zgsReportMonthfabrAreaCityCompletedService.getOne(totalQueryWrapper);
            total = zgsReportMonthfabrAreaCityCompletedService.lastTotalDataCity(zgsReportMonthfabrAreaCityCompleted.getReporttm(), sysUser.getAreacode(), zgsReportMonthfabrAreaCityCompleted.getProjecttype(), applystate);
            if (total != null) {
                //计算月份、年、季度
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                String yearMonth = simpleDateFormat.format(new Date());
                String year = yearMonth.substring(0, 4);
                String month = yearMonth.substring(5, 7);
                int intMonth = Integer.parseInt(month);
                int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
                total.setCreatepersonaccount(sysUser.getUsername());
                total.setCreatepersonname(sysUser.getRealname());
                total.setCreatetime(new Date());
                total.setAreacode(sysUser.getAreacode());
                total.setAreaname(sysUser.getAreaname());
                total.setFilltm(zgsReportMonthfabrAreaCityCompleted.getReporttm());
                total.setReporttm(zgsReportMonthfabrAreaCityCompleted.getReporttm());
                total.setIsdelete(new BigDecimal(0));
                total.setQuarter(new BigDecimal(quarter));
                total.setYear(new BigDecimal(year));
                total.setProjecttype(zgsReportMonthfabrAreaCityCompleted.getProjecttype());
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));

                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (areaCityCompletedList.size() > 0) {
            if (total != null) {
                total.setAreaname("合计");
                areaCityCompletedList.add(total);
                ZgsReportMonthfabrAreaCityCompletedNewPo zgsReportMonthfabrAreaCityCompletedNewPo = new ZgsReportMonthfabrAreaCityCompletedNewPo();
                BeanUtils.copyProperties(total, zgsReportMonthfabrAreaCityCompletedNewPo);
                listNew.add(zgsReportMonthfabrAreaCityCompletedNewPo);
            }
        }
        if (listNew.size() > 1) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                listNew.get(areaCityCompletedList.size() - 1).setApplystate(null);
            }
        }

        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "装配式建筑-新开工-汇总表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportMonthfabrAreaCityCompletedNewPo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, listNew);
        //return super.exportXls(request, zgsReportMonthfabrAreaCityNewconstruction, ZgsReportMonthfabrAreaCityNewconstruction.class, "装配式建筑-新开工-汇总表");
        return mv;
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabrAreaCityNewconstruction.class);
    }

}
