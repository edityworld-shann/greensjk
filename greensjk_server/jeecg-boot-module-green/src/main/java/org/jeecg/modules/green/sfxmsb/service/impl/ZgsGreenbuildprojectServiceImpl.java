package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenbuildproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsGreenbuildprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsGreenbuildprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 绿色建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsGreenbuildprojectServiceImpl extends ServiceImpl<ZgsGreenbuildprojectMapper, ZgsGreenbuildproject> implements IZgsGreenbuildprojectService {

}
