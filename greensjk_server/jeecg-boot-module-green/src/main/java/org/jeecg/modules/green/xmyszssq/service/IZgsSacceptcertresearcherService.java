package org.jeecg.modules.green.xmyszssq.service;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研验收证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsSacceptcertresearcherService extends IService<ZgsSacceptcertresearcher> {

}
