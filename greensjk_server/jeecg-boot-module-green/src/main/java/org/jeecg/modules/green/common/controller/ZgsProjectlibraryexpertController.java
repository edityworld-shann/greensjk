package org.jeecg.modules.green.common.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.IZgsExpertsetService;
import org.jeecg.modules.green.common.service.IZgsProjectlibraryService;
import org.jeecg.modules.green.common.service.IZgsProjectlibraryexpertService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.SMSTokenUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 项目专家关系信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Api(tags = "项目专家关系信息表")
@RestController
@RequestMapping("/common/zgsProjectlibraryexpert")
@Slf4j
public class ZgsProjectlibraryexpertController extends JeecgController<ZgsProjectlibraryexpert, IZgsProjectlibraryexpertService> {
    @Autowired
    private IZgsProjectlibraryexpertService zgsProjectlibraryexpertService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private SMSTokenUtil smsTokenUtil;

    /**
     * 分页列表查询
     *
     * @param zgsProjectlibraryexpert
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-分页列表查询")
    @ApiOperation(value = "项目专家关系信息表-分页列表查询", notes = "项目专家关系信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjectlibraryexpert zgsProjectlibraryexpert,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = QueryGenerator.initQueryWrapper(zgsProjectlibraryexpert, req.getParameterMap());
        Page<ZgsProjectlibraryexpert> page = new Page<ZgsProjectlibraryexpert>(pageNo, pageSize);
        IPage<ZgsProjectlibraryexpert> pageList = zgsProjectlibraryexpertService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsProjectlibraryexpert
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-添加")
    @ApiOperation(value = "项目专家关系信息表-添加", notes = "项目专家关系信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjectlibraryexpert zgsProjectlibraryexpert) {
        zgsProjectlibraryexpertService.save(zgsProjectlibraryexpert);
        return Result.OK("添加成功！");
    }

    /**
     * 专家分配
     *
     * @param zgsProjectSoftExpertinfo
     * @return
     */
    @AutoLog(value = "工程示范-专家分配")
    @ApiOperation(value = "工程示范-专家分配", notes = "工程示范-专家分配")
    @PostMapping(value = "/addFp")
    public Result<?> add(@RequestBody ZgsProjectSoftExpertinfo zgsProjectSoftExpertinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String businesstype = zgsProjectSoftExpertinfo.getProjecttypenum();
        if (zgsProjectSoftExpertinfo.getType() != null && zgsProjectSoftExpertinfo.getType() == 1) {
            if (GlobalConstants.GreenBuildType.equals(businesstype)) {
                ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                zgsGreenbuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS11);
                zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
            } else if (GlobalConstants.ConstructBuidType.equals(businesstype)) {
                ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                zgsBuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS11);
                zgsBuildprojectService.updateById(zgsBuildproject);
            } else if (GlobalConstants.AssemBuidType.equals(businesstype)) {
                ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                zgsAssembleproject.setId(zgsProjectSoftExpertinfo.getBid());
                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS11);
                zgsAssembleprojectService.updateById(zgsAssembleproject);
            } else if (GlobalConstants.EnergyBuildType.equals(businesstype)) {
                ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                zgsEnergybuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS11);
                zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
            } else if (GlobalConstants.SampleBuidType.equals(businesstype)) {
                ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                zgsSamplebuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS11);
                zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
            }
        } else {
            //正常业务走该位置判断
            if (zgsProjectSoftExpertinfo != null) {
                //不管是否抽取专家，状态都会被修改为待立项
                if (GlobalConstants.GreenBuildType.equals(businesstype)) {
                    ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                    zgsGreenbuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                    zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                    zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                } else if (GlobalConstants.ConstructBuidType.equals(businesstype)) {
                    ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                    zgsBuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                    zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                    zgsBuildprojectService.updateById(zgsBuildproject);
                } else if (GlobalConstants.AssemBuidType.equals(businesstype)) {
                    ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                    zgsAssembleproject.setId(zgsProjectSoftExpertinfo.getBid());
                    zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                    zgsAssembleprojectService.updateById(zgsAssembleproject);
                } else if (GlobalConstants.EnergyBuildType.equals(businesstype)) {
                    ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                    zgsEnergybuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                    zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                    zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                } else if (GlobalConstants.SampleBuidType.equals(businesstype)) {
                    ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                    zgsSamplebuildproject.setId(zgsProjectSoftExpertinfo.getBid());
                    zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                    zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                }
                //
                if (StringUtils.isNotEmpty(zgsProjectSoftExpertinfo.getId())) {
                    ZgsProjectlibrary projectlibrary = new ZgsProjectlibrary();
                    projectlibrary.setId(zgsProjectSoftExpertinfo.getId());
                    projectlibrary.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
                    projectlibrary.setSfhs(zgsProjectSoftExpertinfo.getSfhs());
                    zgsProjectlibraryService.updateById(projectlibrary);
                }
                //不抽取专家
                if (zgsProjectSoftExpertinfo.getIsChouQu() == 0) {

                } else {
                    //抽取专家发短信
                    if (zgsProjectSoftExpertinfo.getZgsExpertinfoList() != null && zgsProjectSoftExpertinfo.getZgsExpertinfoList().size() > 0) {
                        String pId = zgsProjectSoftExpertinfo.getId();
                        String bId = zgsProjectSoftExpertinfo.getBid();
                        String businesstypeName = null;
                        if (GlobalConstants.GreenBuildType.equals(businesstype)) {
                            businesstypeName = GlobalConstants.GreenBuildType_VALUE;
                        } else if (GlobalConstants.ConstructBuidType.equals(businesstype)) {
                            businesstypeName = GlobalConstants.ConstructBuidType_VALUE;
                        } else if (GlobalConstants.AssemBuidType.equals(businesstype)) {
                            businesstypeName = GlobalConstants.AssemBuidType_VALUE;
                        } else if (GlobalConstants.EnergyBuildType.equals(businesstype)) {
                            businesstypeName = GlobalConstants.EnergyBuildType_VALUE;
                        } else if (GlobalConstants.SampleBuidType.equals(businesstype)) {
                            businesstypeName = GlobalConstants.SampleBuidType_VALUE;
                        }
                        //日期加+7天
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(new Date());
                        calendar.add(calendar.DATE, 7);
                        Date validdate = calendar.getTime();
                        for (int i = 0; i < zgsProjectSoftExpertinfo.getZgsExpertinfoList().size(); i++) {
                            ZgsProjectlibraryexpert zgsProjectlibraryexpert = new ZgsProjectlibraryexpert();
                            ZgsExpertinfo zgsExpertinfo = zgsProjectSoftExpertinfo.getZgsExpertinfoList().get(i);
                            zgsProjectlibraryexpert.setId(UUID.randomUUID().toString());
                            zgsProjectlibraryexpert.setExpertguid(zgsExpertinfo.getId());
                            zgsProjectlibraryexpert.setProjectlibraryguid(pId);
                            zgsProjectlibraryexpert.setBusinessguid(bId);
                            zgsProjectlibraryexpert.setBusinesstype(businesstypeName);
                            zgsProjectlibraryexpert.setCreatepersonaccount(sysUser.getUsername());
                            zgsProjectlibraryexpert.setCreatepersonname(sysUser.getRealname());
                            zgsProjectlibraryexpert.setCreatetime(new Date());
                            zgsProjectlibraryexpert.setValiddate(validdate);
                            boolean flag = zgsProjectlibraryexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsProjectlibraryexpert, ValidateEncryptEntityUtil.isEncrypt));
                            if (flag) {
                                //给选中专家发送短信
                                if (StringUtils.isNotEmpty(zgsExpertinfo.getMobile())) {
                                    HashMap<String, Object> fillContent = new HashMap<>();
                                    fillContent.put("content", zgsExpertinfo.getCurrentposition());
                                    fillContent.put("projectName", "1");
                                    fillContent.put("year", "2022");
                                    fillContent.put("month", "09");
                                    fillContent.put("day", "14");
                                    fillContent.put("time", "14:00");
                                    fillContent.put("location", "1");
                                    fillContent.put("locationName", "2");
                                    HashMap<String, Object> paramsMap = new HashMap<>();
//                                    paramsMap.put("phoneNumber", zgsExpertinfo.getMobile());
                                    paramsMap.put("phoneNumber", "18210502471");
                                    paramsMap.put("smsType", 3);
                                    paramsMap.put("companyName", zgsExpertinfo.getUsername());
                                    paramsMap.put("templateId", 12);
                                    paramsMap.put("fillContent", fillContent);
                                    Result<?> result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                                    if (result.getCode() == 200) {
                                        log.info("申报项目给抽取专家发送短信成功！");
                                    } else {
                                        log.info("申报项目给抽取专家发送短信失败！" + result.getMessage());
                                    }
                                }
                            }
                            //添加屏蔽专家数据
                            if (StringUtils.isNotEmpty(zgsProjectSoftExpertinfo.getSet())) {
                                ZgsExpertset zgsExpertset = new ZgsExpertset();
                                zgsExpertset.setId(UUID.randomUUID().toString());
                                zgsExpertset.setExpertguid(zgsExpertinfo.getId());
                                zgsExpertset.setBusinessguid(bId);
                                zgsExpertset.setCreatepersonaccount(sysUser.getUsername());
                                zgsExpertset.setCreatepersonname(sysUser.getRealname());
                                zgsExpertset.setCreatetime(new Date());
                                zgsExpertset.setSetvalue(zgsProjectSoftExpertinfo.getSet());
                                zgsExpertsetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertset, ValidateEncryptEntityUtil.isEncrypt));
                            }
                        }
                    }
                }
            } else {
                return Result.error("操作失败!");
            }
        }
        return Result.OK("操作成功！");
    }

    /**
     * 专家-审批
     *
     * @param zgsProjectlibraryexpert
     * @return
     */
    @AutoLog(value = "工程示范-专家-审批")
    @ApiOperation(value = "工程示范-专家-审批", notes = "工程示范-专家-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsProjectlibraryexpert zgsProjectlibraryexpert) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String businessguid = zgsProjectlibraryexpert.getBusinessguid();
        zgsProjectlibraryexpert.setModifypersonaccount(sysUser.getUsername());
        zgsProjectlibraryexpert.setModifypersonname(sysUser.getRealname());
        zgsProjectlibraryexpert.setModifytime(new Date());
        zgsProjectlibraryexpert.setAuditdate(new Date());
        if (zgsProjectlibraryexpert != null && StringUtils.isNotEmpty(zgsProjectlibraryexpert.getAuditconclusionnum())) {
            zgsProjectlibraryexpert.setIspingshen(new BigDecimal(1));
            if (GlobalConstants.AGREE_1.equals(zgsProjectlibraryexpert.getAuditconclusionnum())) {
                zgsProjectlibraryexpert.setAuditconclusion("同意");
                zgsProjectlibraryexpert.setAuditconclusionnum("1");
            } else {
                zgsProjectlibraryexpert.setAuditconclusion("不同意");
                zgsProjectlibraryexpert.setAuditconclusionnum("0");
            }
        }
        QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
        zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
        ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
//        QueryWrapper<ZgsProjectlibraryexpert> zgsProjectlibraryexpertQueryWrapper = new QueryWrapper();
//        zgsProjectlibraryexpertQueryWrapper.eq("businessguid", businessguid);
//        zgsProjectlibraryexpertQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
//        zgsProjectlibraryexpertQueryWrapper.isNull("auditconclusionnum");
//        ZgsProjectlibraryexpert projectlibraryexpert = zgsProjectlibraryexpertService.getOne(zgsProjectlibraryexpertQueryWrapper);
//        zgsProjectlibraryexpert.setId(projectlibraryexpert.getId());
//        zgsProjectlibraryexpert.setId(projectlibraryexpert.getId());
//        zgsProjectlibraryexpertService.updateById(zgsProjectlibraryexpert);
        QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
        queryWrapper.eq("projectlibraryguid", zgsProjectlibraryexpert.getId());
        queryWrapper.eq("businessguid", businessguid);
        queryWrapper.eq("expertguid", zgsExpertinfo.getId());
        zgsProjectlibraryexpertService.update(zgsProjectlibraryexpert, queryWrapper);
        //判断是否为驳回
        QueryWrapper<ZgsProjectlibraryexpert> projectlibraryexpertQueryWrapper = new QueryWrapper();
        projectlibraryexpertQueryWrapper.eq("businessguid", businessguid);
        projectlibraryexpertQueryWrapper.ne("isdelete", 1);
        List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(projectlibraryexpertQueryWrapper);
        if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
            int size = zgsProjectlibraryexpertList.size();
            int count1 = 0;
            int count0 = 0;
            for (ZgsProjectlibraryexpert projectlibraryexpert1 : zgsProjectlibraryexpertList) {
                //1同意0不同意
                String auditconclusionnum = projectlibraryexpert1.getAuditconclusionnum();
                if (StringUtils.isNotEmpty(auditconclusionnum)) {
                    if (GlobalConstants.AGREE_1.equals(auditconclusionnum)) {
                        count1++;//同意
                    } else {
                        count0++;//不同意
                    }
                }
            }

            if ((count1 + count0) == size) {
                if (StringUtils.isNotEmpty(zgsProjectlibraryexpert.getProjecttypenum())) {
                    if (GlobalConstants.GreenBuildType.equals(zgsProjectlibraryexpert.getProjecttypenum())) {
                        ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                        zgsGreenbuildproject.setId(businessguid);
                        if ((double) count1 / size < 0.6) {
                            //同意率小于百分之60直接驳回
                            zgsGreenbuildproject.setAuditopinion("专家审批通过率低于60%直接驳回");
//                            zgsGreenbuildproject.setAgreeproject(new BigDecimal(1));
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                        } else {
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS10);
                            zgsGreenbuildproject.setAuditopinion(null);
//                            zgsGreenbuildproject.setAgreeproject(new BigDecimal(2));
                        }
                        zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                    } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibraryexpert.getProjecttypenum())) {
                        ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                        zgsBuildproject.setId(businessguid);
                        if ((double) count1 / size < 0.6) {
                            //同意率小于百分之60直接驳回
                            zgsBuildproject.setAuditopinion("专家审批通过率低于60%直接驳回");
//                            zgsBuildproject.setAgreeproject(new BigDecimal(1));
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                        } else {
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS10);
                            zgsBuildproject.setAuditopinion(null);
//                            zgsBuildproject.setAgreeproject(new BigDecimal(2));
                        }
                        zgsBuildprojectService.updateById(zgsBuildproject);
                    } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibraryexpert.getProjecttypenum())) {
                        ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                        zgsAssembleproject.setId(businessguid);
                        if ((double) count1 / size < 0.6) {
                            //同意率小于百分之60直接驳回
                            zgsAssembleproject.setAuditopinion("专家审批通过率低于60%直接驳回");
//                            zgsAssembleproject.setAgreeproject(new BigDecimal(1));
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                        } else {
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS10);
                            zgsAssembleproject.setAuditopinion(null);
//                            zgsAssembleproject.setAgreeproject(new BigDecimal(2));
                        }
                        zgsAssembleprojectService.updateById(zgsAssembleproject);
                    } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibraryexpert.getProjecttypenum())) {
                        ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                        zgsEnergybuildproject.setId(businessguid);
                        if ((double) count1 / size < 0.6) {
                            //同意率小于百分之60直接驳回
                            zgsEnergybuildproject.setAuditopinion("专家审批通过率低于60%直接驳回");
//                            zgsEnergybuildproject.setAgreeproject(new BigDecimal(1));
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                        } else {
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS10);
                            zgsEnergybuildproject.setAuditopinion(null);
//                            zgsEnergybuildproject.setAgreeproject(new BigDecimal(2));
                        }
                        zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                    } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibraryexpert.getProjecttypenum())) {
                        ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                        zgsSamplebuildproject.setId(businessguid);
                        if ((double) count1 / size < 0.6) {
                            //同意率小于百分之60直接驳回
                            zgsSamplebuildproject.setAuditopinion("专家审批通过率低于60%直接驳回");
//                            zgsSamplebuildproject.setAgreeproject(new BigDecimal(1));
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                        } else {
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS10);
                            zgsSamplebuildproject.setAuditopinion(null);
//                            zgsSamplebuildproject.setAgreeproject(new BigDecimal(2));
                        }
                        zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 编辑
     *
     * @param zgsProjectlibraryexpert
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-编辑")
    @ApiOperation(value = "项目专家关系信息表-编辑", notes = "项目专家关系信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjectlibraryexpert zgsProjectlibraryexpert) {
        zgsProjectlibraryexpertService.updateById(zgsProjectlibraryexpert);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-通过id删除")
    @ApiOperation(value = "项目专家关系信息表-通过id删除", notes = "项目专家关系信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjectlibraryexpertService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-批量删除")
    @ApiOperation(value = "项目专家关系信息表-批量删除", notes = "项目专家关系信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsProjectlibraryexpertService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目专家关系信息表-通过id查询")
    @ApiOperation(value = "项目专家关系信息表-通过id查询", notes = "项目专家关系信息表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsProjectlibraryexpert zgsProjectlibraryexpert = zgsProjectlibraryexpertService.getById(id);
        if (zgsProjectlibraryexpert == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsProjectlibraryexpert);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsProjectlibraryexpert
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsProjectlibraryexpert zgsProjectlibraryexpert) {
        return super.exportXls(request, zgsProjectlibraryexpert, ZgsProjectlibraryexpert.class, "项目专家关系信息表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjectlibraryexpert.class);
    }

}
