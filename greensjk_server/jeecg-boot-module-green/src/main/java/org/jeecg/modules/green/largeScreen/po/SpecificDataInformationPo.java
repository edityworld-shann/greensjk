package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SpecificDataInformationPo implements Serializable {
    private String areaname;
    /**
     * 绿色建筑竣工面积
     */
    private String yearNewEndArea;
    /**
     * 住宅建筑
     */
    private String yearBuildhouseArea;
    /**
     * 公共建筑
     */
    private String yearBuildpublicArea;
    /**
     * 工业建筑
     */
    private String yearBuildindustryArea;

    /**
     * 绿色建筑总面积占比
     */
    private String yearZb;
    private List<GreenBuilding> listData;
    /**
     * 绿色建筑记录
     */
    private String yearNewEndAreaCount;
}
