package org.jeecg.modules.green.jxzpsq.service.impl;

import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import org.jeecg.modules.green.jxzpsq.mapper.ZgsPerformancescoreMapper;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancescoreService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 计划项目执行情况绩效自评表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsPerformancescoreServiceImpl extends ServiceImpl<ZgsPerformancescoreMapper, ZgsPerformancescore> implements IZgsPerformancescoreService {

}
