package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import net.sf.saxon.functions.Exists;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 业务库-各类项目工程申报材料
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_mattermaterial")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_mattermaterial对象", description = "业务库-各类项目工程申报材料")
public class ZgsMattermaterial implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 表T_PZ_MATTERMATERIAL主键GUID
     */
    @Excel(name = "表T_PZ_MATTERMATERIAL主键GUID", width = 15)
    @ApiModelProperty(value = "表T_PZ_MATTERMATERIAL主键GUID")
    private java.lang.String materialid;
    /**
     * 各类项目工程业务表主键GUID
     */
    @Excel(name = "各类项目工程业务表主键GUID", width = 15)
    @ApiModelProperty(value = "各类项目工程业务表主键GUID")
    private java.lang.String buildguid;
    /**
     * 材料名称
     */
    @Excel(name = "材料名称", width = 15)
    @ApiModelProperty(value = "材料名称")
    private java.lang.String materialname;
    /**
     * 是否必传:1必传,默认0
     */
    @Excel(name = "是否必传:1必传,默认0", width = 15)
    @ApiModelProperty(value = "是否必传:1必传,默认0")
    private java.math.BigDecimal isrequired;
    /**
     * 材料形式:原件,复印件
     */
    @Excel(name = "材料形式:原件,复印件", width = 15)
    @ApiModelProperty(value = "材料形式:原件,复印件")
    private java.lang.String materialtype;
    /**
     * 排序号
     */
    @Excel(name = "排序号", width = 15)
    @ApiModelProperty(value = "排序号")
    private java.math.BigDecimal ordernum;
    /**
     * 备注
     */
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
    /**
     * 是否已上传
     */
    @Excel(name = "是否已上传", width = 15)
    @ApiModelProperty(value = "是否已上传")
    private java.math.BigDecimal isuploaded;
    @TableField(exist = false)
    private List<ZgsAttachappendix> zgsAttachappendixList;
    /*@TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;*/

    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    @ApiModelProperty(value = "项目类型:字典DemonstrateType")
    @TableField(exist = false)
    private java.lang.String projecttypenum;
    @TableField(exist = false)
    private java.lang.String projectlibraryguid;
    @TableField(exist = false)
    private String type;

}
