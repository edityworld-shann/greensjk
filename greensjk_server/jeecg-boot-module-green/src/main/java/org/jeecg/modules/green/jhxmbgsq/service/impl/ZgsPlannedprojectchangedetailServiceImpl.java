package org.jeecg.modules.green.jhxmbgsq.service.impl;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.mapper.ZgsPlannedprojectchangedetailMapper;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 建设科技计划项目变更内容记录表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsPlannedprojectchangedetailServiceImpl extends ServiceImpl<ZgsPlannedprojectchangedetailMapper, ZgsPlannedprojectchangedetail> implements IZgsPlannedprojectchangedetailService {

}
