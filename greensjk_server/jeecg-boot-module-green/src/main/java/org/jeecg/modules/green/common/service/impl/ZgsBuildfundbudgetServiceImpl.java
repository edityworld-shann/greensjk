package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsBuildfundbudget;
import org.jeecg.modules.green.common.mapper.ZgsBuildfundbudgetMapper;
import org.jeecg.modules.green.common.service.IZgsBuildfundbudgetService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsBuildfundbudgetServiceImpl extends ServiceImpl<ZgsBuildfundbudgetMapper, ZgsBuildfundbudget> implements IZgsBuildfundbudgetService {

}
