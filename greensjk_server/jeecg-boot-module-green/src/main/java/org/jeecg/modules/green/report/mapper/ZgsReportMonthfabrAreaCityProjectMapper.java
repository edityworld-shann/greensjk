package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;

/**
 * @Description: 装配式建筑-项目库-申报
 * @Author: jeecg-boot
 * @Date:   2022-05-20
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaCityProjectMapper extends BaseMapper<ZgsReportMonthfabrAreaCityProject> {
  public List<ZgsReportMonthfabrAreaCityProject> queryProjectByReportTime(String reporttm, String areacode);
}
