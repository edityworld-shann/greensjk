package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificpexecutivelist;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsScientificpexecutivelistService extends IService<ZgsScientificpexecutivelist> {

}
