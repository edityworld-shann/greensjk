package org.jeecg.modules.green.sms.service.impl;

import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import org.jeecg.modules.green.sms.mapper.ZgsSmsSendingRecordLogMapper;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 短信记录表
 * @Author: jeecg-boot
 * @Date:   2022-06-07
 * @Version: V1.0
 */
@Service
public class ZgsSmsSendingRecordLogServiceImpl extends ServiceImpl<ZgsSmsSendingRecordLogMapper, ZgsSmsSendingRecordLog> implements IZgsSmsSendingRecordLogService {

}
