package org.jeecg.modules.green.xmyszssq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 示范项目验收证书基础表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_examplecertificatebase")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_examplecertificatebase对象", description = "示范项目验收证书基础表")
public class ZgsExamplecertificatebase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 科研项目申报验收基本信息表ID
     */
//    @Excel(name = "科研项目申报验收基本信息表ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "科研项目申报验收基本信息表ID")
    private java.lang.String projectlibraryguid;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 开工时间
     */
    @Excel(name = "开工时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开工时间")
    private java.util.Date startdate;
    /**
     * 竣工时间
     */
    @Excel(name = "竣工时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "竣工时间")
    private java.util.Date enddate;
    /**
     * 实施单位
     */
    @Excel(name = "实施单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "实施单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String implementunit;
    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkman;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15, orderNum = "2")
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linktel;
    /**
     * 邮政编码
     */
    //@Excel(name = "邮政编码", width = 15, orderNum = "2")
    @ApiModelProperty(value = "邮政编码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String postalcode;
    /**
     * 通信地址
     */
    //@Excel(name = "通信地址", width = 15, orderNum = "2")
    @ApiModelProperty(value = "通信地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String postaddress;
    /**
     * 工程概况
     */
    @Excel(name = "工程概况", width = 15, orderNum = "1")
    @ApiModelProperty(value = "工程概况")
    private java.lang.String projectprofile;
    /**
     * 应用新技术名称
     */
    @Excel(name = "应用新技术名称", width = 15, orderNum = "2")
    @ApiModelProperty(value = "应用新技术名称")
    private java.lang.String applytechnology;
    /**
     * 技术创新内容
     */
    @Excel(name = "技术创新内容", width = 15, orderNum = "2")
    @ApiModelProperty(value = "技术创新内容")
    private java.lang.String technologyinnovation;
    /**
     * 社会效益和环境效益
     */
    @Excel(name = "社会效益和环境效益", width = 15, orderNum = "2")
    @ApiModelProperty(value = "社会效益和环境效益")
    private java.lang.String socialbenefit;
    /**
     * 经济效益率
     */
    @Excel(name = "经济效益率", width = 15, orderNum = "2")
    @ApiModelProperty(value = "经济效益率")
    private java.lang.String economybenefit;
    /**
     * 行政处罚
     */
    @Excel(name = "行政处罚", width = 15, orderNum = "2")
    @ApiModelProperty(value = "行政处罚")
    private java.lang.String administratpenalty;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
//    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * createaccountname
     */
//    @Excel(name = "createaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
//    @Excel(name = "createusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
//    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
//    @Excel(name = "modifyaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
//    @Excel(name = "modifyusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
//    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    private java.lang.String higherupunitnum;
    /**
     * 主要文件
     */
    @Excel(name = "主要文件", width = 15, orderNum = "2")
    @ApiModelProperty(value = "主要文件")
    private java.lang.String mainfile;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    /**
     * 项目编号,2019.11.8 ljx-add
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号,2019.11.8 ljx-add")
    private java.lang.String projectnumber;

    // 资料归档
    @ApiModelProperty(value = "文件归档：1 已归档； 2 未归档")
    private java.lang.String fileInduce;

    @TableField(exist = false)
    private boolean fileInduceBol;

    /**
     * 是否闭合，1：闭合；2019.11.8 ljx-add
     */
//    @Excel(name = "是否闭合，1：闭合；2019.11.8 ljx-add", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否闭合，1：闭合；2019.11.8 ljx-add")
    private java.math.BigDecimal isclose;
    @TableField(exist = false)
    private List<ZgsExamplecertparticipant> zgsExamplecertparticipantList;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(exist = false)
    private List<ZgsExamplecertimplementunit> zgsExamplecertimplementunitList;
    @TableField(exist = false)
    private List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    @TableField(exist = false)
    private Object cstatus;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    @TableField(exist = false)
    private List<ZgsSacceptcertexpert> zgsSacceptcertexpertList;

    // 成果简介信息
    @TableField(exist = false)
    private ZgsPlanresultbase zgsPlanresultbase;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
}
