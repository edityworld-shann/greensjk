package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: zgs_report_monthgreenbuild_area
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportMonthgreenbuildAreaService extends IService<ZgsReportMonthgreenbuildArea> {

  ZgsReportMonthgreenbuildArea queryLatestInfo (String dataSourceName, String processType);
}
