package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 装配式建筑-生产产能-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_area_city_productioncapacity")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_monthfabr_area_city_productioncapacity对象", description = "装配式建筑-生产产能-汇总表")
public class ZgsReportMonthfabrAreaCityProductioncapacity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    //@Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 报表时间
     */
    //@Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 创建人帐号
     */
    //@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    //@Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;


    /**
     * 住宅-总合计
     */
    @Excel(name = "住宅", width = 15)
    @ApiModelProperty(value = "住宅-总合计")
    private java.math.BigDecimal monthProdZz;


    /**
     * 住宅-数量
     */
    //@Excel(name = "住宅-数量", width = 15)
    @ApiModelProperty(value = "住宅-数量")
    private java.math.BigDecimal monthProdZzCount;

    /**
     * 公共建筑-总合计
     */
    @Excel(name = "公共建筑", width = 15)
    @ApiModelProperty(value = "公共建筑-总合计")
    private java.math.BigDecimal monthProdGgjz;

    /**
     * 公共建筑-数量
     */
    //@Excel(name = "公共建筑-数量", width = 15)
    @ApiModelProperty(value = "公共建筑-数量")
    private java.math.BigDecimal monthProdGgjzCount;

    /**
     * 装配式混凝土预制构配件生产情况-企业数量（家）-总合计
     */
    @Excel(name = "装配式混凝土预制构配件生产情况-企业数量（家）-总合计", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产情况-企业数量（家）-总合计")
    private java.math.BigDecimal monthProdCompany1;

    /**
     * 装配式混凝土预制构配件生产情况-生产线（条）-总合计
     */
    @Excel(name = "装配式混凝土预制构配件生产情况-生产线（条）-总合计", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产情况-生产线（条）-总合计")
    private java.math.BigDecimal monthProdScx1;

    /**
     * 装配式混凝土预制构配件生产情况-设计产能（万立方米）-总合计
     */
    @Excel(name = "装配式混凝土预制构配件生产情况-设计产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产情况-设计产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdSjcn1;

    /**
     * 装配式混凝土预制构配件生产情况-实际产能（万立方米）-总合计
     */
    @Excel(name = "装配式混凝土预制构配件生产情况-实际产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产情况-实际产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdShijicn1;


    /**
     * 装配式钢结构构件生产情况-企业数量（家）-总合计
     */
    @Excel(name = "装配式钢结构构件生产情况-企业数量（家）-总合计", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产情况-企业数量（家）-总合计")
    private java.math.BigDecimal monthProdCompany2;

    /**
     * 装配式钢结构构件生产情况-生产线（条）-总合计
     */
    @Excel(name = "装配式钢结构构件生产情况-生产线（条）-总合计", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产情况-生产线（条）-总合计")
    private java.math.BigDecimal monthProdScx2;

    /**
     * 装配式钢结构构件生产情况-设计产能（万立方米）-总合计
     */
    @Excel(name = "装配式钢结构构件生产情况-设计产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产情况-设计产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdSjcn2;

    /**
     * 装配式钢结构构件生产情况-实际产能（万立方米）-总合计
     */
    @Excel(name = "装配式钢结构构件生产情况-实际产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产情况-实际产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdShijicn2;


    /**
     * 装配式木结构构件生产情况-企业数量（家）-总合计
     */
    @Excel(name = "装配式木结构构件生产情况-企业数量（家）-总合计", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产情况-企业数量（家）-总合计")
    private java.math.BigDecimal monthProdCompany3;

    /**
     * 装配式木结构构件生产情况-生产线（条）-总合计
     */
    @Excel(name = "装配式木结构构件生产情况-生产线（条）-总合计", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产情况-生产线（条）-总合计")
    private java.math.BigDecimal monthProdScx3;

    /**
     * 装配式木结构构件生产情况-设计产能（万立方米）-总合计
     */
    @Excel(name = "装配式木结构构件生产情况-设计产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产情况-设计产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdSjcn3;

    /**
     * 装配式木结构构件生产情况-实际产能（万立方米）-总合计
     */
    @Excel(name = "装配式木结构构件生产情况-实际产能（万立方米）-总合计", width = 15)
    @ApiModelProperty(value = "装配式木结构构件生产情况-实际产能（万立方米）-总合计")
    private java.math.BigDecimal monthProdShijicn3;


    /**
     * 默认为空：市州内上报，1市州向省厅上报
     */
    //@Excel(name = "默认为空：市州内上报，1市州向省厅上报", width = 15)
    @ApiModelProperty(value = "默认为空：市州内上报，1市州向省厅上报")
    private java.lang.String areaType;

    /**
     * 1:新开工2:已竣工验收
     */
    //@Excel(name = "1:新开工2:已竣工验收", width = 3)
    @ApiModelProperty(value = "1:新开工2:已竣工验收")
    private java.lang.String projecttype;


    /**
     * 总面积
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "总面积")
    private Double yearArea;

    /**
     * 个数
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "个数")
    private Integer yearNumber;

    /**
     * 等级类型
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "等级类型")
    private java.lang.String baseType;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
