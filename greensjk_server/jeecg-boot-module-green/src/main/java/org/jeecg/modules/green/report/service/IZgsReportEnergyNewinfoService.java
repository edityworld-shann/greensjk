package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 新建绿色建筑-节能项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface IZgsReportEnergyNewinfoService extends IService<ZgsReportEnergyNewinfo> {

    public void addIterm(String filltm);

}
