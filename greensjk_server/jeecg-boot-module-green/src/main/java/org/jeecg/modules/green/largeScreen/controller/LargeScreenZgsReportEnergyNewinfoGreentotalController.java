package org.jeecg.modules.green.largeScreen.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.green.largeScreen.po.*;
import org.jeecg.modules.green.largeScreen.service.LargeScreenZgsReportEnergyNewinfoGreentotalService;
import org.jeecg.modules.green.largeScreen.vo.*;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoGreentotalService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.jeecg.common.util.DateUtils.dateMinusMonth;
import static org.jeecg.common.util.DateUtils.formatMonth;
import static org.jeecg.common.util.DateUtils.yyy;

/**
 * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
 * @Description ：<大屏绿色建筑统计>
 * @Author ：Haozhiyang
 * @Date ：2022/8/19 9:55
 * @Version ：1.0
 * @History ：<修改代码时说明>
 */
@Api(tags = "绿色建筑统计")
@RestController
@RequestMapping("/largeScreen/greenBuildingStatistics")
@Slf4j
public class LargeScreenZgsReportEnergyNewinfoGreentotalController extends JeecgController<ZgsReportEnergyNewinfoGreentotal, IZgsReportEnergyNewinfoGreentotalService> {
    @Resource
    private LargeScreenZgsReportEnergyNewinfoGreentotalService largeScreenZgsReportEnergyNewinfoGreentotalService;

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<具体数据信息>
     * @Author ：Haozhiyang
     * @Date ：2022/8/19 9:58
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "具体数据信息")
    @ApiOperation(value = "具体数据信息", notes = "具体数据信息")
    @GetMapping(value = "/specificDataInformation")
    public Result<?> specificDataInformation(@RequestParam("areacodel") String areacodel) throws Exception {
        String year = yyy();
        String yearMonth = formatMonth();
        String month = dateMinusMonth(yearMonth);
        SpecificDataInformationPo specificDataInformationPo = new SpecificDataInformationPo();
        if (StringUtils.isNotEmpty(areacodel) && areacodel.equals("62")) {
            areacodel = "";
            specificDataInformationPo = largeScreenZgsReportEnergyNewinfoGreentotalService.specificDataInformation(month, areacodel);
        } else {
            specificDataInformationPo = largeScreenZgsReportEnergyNewinfoGreentotalService.specificDataInformationNew(month, areacodel);
        }
        List<GreenBuilding> list = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");
        String money = "0.00";
        if (StringUtils.isNotEmpty(specificDataInformationPo.getYearZb())) {
            money = decimalFormat.format(Double.parseDouble(specificDataInformationPo.getYearZb()));
        }
        if (StringUtils.isNotEmpty(money)) {
            GreenBuilding greenBuilding = new GreenBuilding();
            greenBuilding.setName("绿色建筑占比");
            greenBuilding.setValue(Double.parseDouble(money));
            list.add(greenBuilding);
            specificDataInformationPo.setListData(list);
        }
        return Result.OK(specificDataInformationPo);
    }

    @AutoLog(value = "装配式数据统计")
    @ApiOperation(value = "装配式数据统计", notes = "装配式数据统计")
    @GetMapping(value = "/assemblyDataStatistics")
    public Result<?> assemblyDataStatistics() throws Exception {
        String yearMonth = formatMonth();
        String month = dateMinusMonth(yearMonth);
        AssemblyDataStatisticsPo assemblyDataStatisticsPo = largeScreenZgsReportEnergyNewinfoGreentotalService.assemblyDataStatistics(month);
        List<Prefabricated> list = new ArrayList<>();
        List<Prefabricated> listZb = new ArrayList<>();
        if (assemblyDataStatisticsPo != null) {
            Prefabricated prefabricated = new Prefabricated();
            double yearFarArea1 = 0.0, yearFarArea2 = 0.0, yearFarArea3 = 0.0;
            prefabricated.setName("装配式开工总面积");
            /*if (StringUtils.isNotEmpty(assemblyDataStatisticsPo.getYearFarArea1())) {
                yearFarArea1 = Double.parseDouble(assemblyDataStatisticsPo.getYearFarArea1());
            }
            if (StringUtils.isNotEmpty(assemblyDataStatisticsPo.getYearFarArea2())) {
                yearFarArea2 = Double.parseDouble(assemblyDataStatisticsPo.getYearFarArea2());
            }
            if (StringUtils.isNotEmpty(assemblyDataStatisticsPo.getYearFarArea3())) {
                yearFarArea3 = Double.parseDouble(assemblyDataStatisticsPo.getYearFarArea3());
            }
            double total = yearFarArea1 + yearFarArea2 + yearFarArea3;
            assemblyDataStatisticsPo.setTotal(String.valueOf(total));*/
            prefabricated.setValue(Double.parseDouble(assemblyDataStatisticsPo.getTotal()));
            list.add(prefabricated);
        } else {
            assemblyDataStatisticsPo = new AssemblyDataStatisticsPo();
            Prefabricated prefabricated = new Prefabricated();
            prefabricated.setName("装配式开工总面积");
            prefabricated.setValue(0.00);
            list.add(prefabricated);
        }
        if (assemblyDataStatisticsPo != null && StringUtils.isNotEmpty(assemblyDataStatisticsPo.getTotalAll())) {
            Prefabricated prefabricated = new Prefabricated();
            prefabricated.setName("建筑总面积");
            prefabricated.setValue(Double.parseDouble(assemblyDataStatisticsPo.getTotalAll()));
            list.add(prefabricated);
        } else {
            assemblyDataStatisticsPo = new AssemblyDataStatisticsPo();
            Prefabricated prefabricated = new Prefabricated();
            prefabricated.setName("建筑总面积");
            prefabricated.setValue(0.00);
            list.add(prefabricated);
        }
        String areacode = "";
        String prdType = "1";
        PrefabricaeZbVo prefabricaeZb = largeScreenZgsReportEnergyNewinfoGreentotalService.prefabricateBulidngZb(month, areacode, prdType);
        Prefabricated prefabricated = new Prefabricated();
        prefabricated.setName("装配式建筑开工总面积占比");
        prefabricated.setValue(prefabricaeZb.getYearFarAssemblyrate());
        listZb.add(prefabricated);
        assemblyDataStatisticsPo.setListDataNew(listZb);

       /* Prefabricated greenBuilding = new Prefabricated();
        greenBuilding.setName("装配式建筑开工总面积占比");
        greenBuilding.setValue((Double.parseDouble(assemblyDataStatisticsPo.getTotal())/Double.parseDouble(assemblyDataStatisticsPo.getTotalAll()))*100);
        list.add(greenBuilding);*/
        assemblyDataStatisticsPo.setListData(list);
        return Result.OK(assemblyDataStatisticsPo);
    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<大屏项目总数和绿色项目总数>
     * @Author ：Haozhiyang
     * @Date ：2022/8/22 12:05
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "大屏项目总数和绿色项目总数")
    @ApiOperation(value = "大屏项目总数和绿色项目总数", notes = "大屏项目总数和绿色项目总数")
    @GetMapping(value = "/totalTitems")
    public Result<?> totalTitems(@RequestParam("areacodel") String areacode) throws Exception {
        if (areacode.equals("62")) {
            areacode = "";
        }
        String yearMonth = formatMonth();
        String year = dateMinusMonth(yearMonth);
        TotalTitemsPo totalTitemsPo = largeScreenZgsReportEnergyNewinfoGreentotalService.totalTitems(year, areacode);
        return Result.OK(totalTitemsPo);
    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<用一句话描述功能>
     * @Author ：Haozhiyang
     * @Date ：2022/8/22 12:28
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "本年度累计绿色建筑等级(旧)")
    @ApiOperation(value = "本年度累计绿色建筑等级", notes = "本年度累计绿色建筑等级")
    @GetMapping(value = "/accumulatedGreenBuildingGradeInThisYearLoad")
    public Result<?> accumulatedGreenBuildingGradeInThisYearLoad(@RequestParam("areacodel") String areacode) {
        String year = yyy();
        List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInPo = largeScreenZgsReportEnergyNewinfoGreentotalService.accumulatedGreenBuildingGradeInThisYear(year);
        return Result.OK(accumulatedGreenBuildingGradeInPo);
    }

    @AutoLog(value = "本年度累计绿色建筑等级(新)")
    @ApiOperation(value = "本年度累计绿色建筑等级", notes = "本年度累计绿色建筑等级")
    @GetMapping(value = "/accumulatedGreenBuildingGradeInThisYear")
    public Result<?> accumulatedGreenBuildingGradeInThisYear(@RequestParam("areacodel") String areacode) throws Exception {
        //String year = yyy();
        String yearMonth = formatMonth();
        String year = dateMinusMonth(yearMonth);
        List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInPoList = new ArrayList<>();
        //获取所有的市
        //List<City> codeS = largeScreenZgsReportEnergyNewinfoGreentotalService.getAllCities();
        if (areacode.equals("62")) {
            //if (codeS.size() > 0) {
            //for (City city : codeS) {
            // if (!city.getCode().equals("62")) {
            year = dateMinusMonth(yearMonth);
            AccumulatedGreenBuildingGradeInPo accmulated = new AccumulatedGreenBuildingGradeInPo();
            accumulatedGreenBuildingGradeInPoList = largeScreenZgsReportEnergyNewinfoGreentotalService.accumulatedGreenBuildingGradeInThisYearNew(year, "");
//                        if (accmulated != null) {
//                            accmulated.setAreaname(city.getName());
//                            accumulatedGreenBuildingGradeInPoList.add(accmulated);
//                       // }
//                    //}
//                }
            //}
        } else {
            accumulatedGreenBuildingGradeInPoList = largeScreenZgsReportEnergyNewinfoGreentotalService.accumulatedGreenBuildingGradeInPoList(year, areacode);
            return Result.OK(accumulatedGreenBuildingGradeInPoList);
        }
        return Result.OK(accumulatedGreenBuildingGradeInPoList);
    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<绿色建筑数据>
     * @Author ：Haozhiyang
     * @Date ：2022/8/22 14:40
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "绿色建筑数据")
    @ApiOperation(value = "绿色建筑数据", notes = "绿色建筑数据")
    @GetMapping(value = "/greenBuildingData")
    public Result<?> greenBuildingData(@RequestParam("dateType") String dateType, @RequestParam("buildingType") String buildingType, @RequestParam("areacodel") String areacodel) throws Exception {
        List<GreenBuildingVo> greenBuildingVos = new ArrayList<>();
        List<MoonGreenBuildingPo> greenBuildingData = new ArrayList<>();

        List<GreenBuildingDetails> greenBuildingDetailsList = new ArrayList<>();
        GreenBuildingVo greenBuildingVo = new GreenBuildingVo();
        String month = "";
        if (dateType.equals("0")) {
            String yearMonth = formatMonth();
            month = dateMinusMonth(yearMonth);

        } else {
            if (dateType.length() > 2) {
                month = yyy() + "-" + dateType;
            } else {
                month = yyy() + "-0" + dateType;
            }
        }
        String cityName = largeScreenZgsReportEnergyNewinfoGreentotalService.getCityOne(areacodel);
        String year = null;
        if (areacodel.equals("62")) {
            areacodel="";
            //List<City> codeS = largeScreenZgsReportEnergyNewinfoGreentotalService.getAllCities();
            //if (codeS.size() > 0) {
               // for (City city : codeS) {
                  //  if (!city.getCode().equals("62")) {
                       // MoonGreenBuildingPo moonGreenBuildingPo = new MoonGreenBuildingPo();
                        //if (dateType.equals("0")) {
                         //   greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.moonGreenBuildingPo(year, month, areacodel);
                        //}
//                        else {
//                            greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.monthMoonGreenBuildingPo(year, month, areacodel);
//                        }
                       /* if (moonGreenBuildingPo != null) {
                            moonGreenBuildingPo.setAreaname(city.getName());
                            greenBuildingData.add(moonGreenBuildingPo);
                        }*/
                    //}
                //}
            //}
            greenBuildingVo.setAreaname("甘肃省");
        }
        if (dateType.equals("0")) {
            greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.moonGreenBuildingPo(year, month, areacodel);
        }else{
            greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.moonGreenBuildingPoTow(year, month, areacodel);
        }
//        else {
//            if (StringUtils.isNotEmpty(dateType)) {
//                if (dateType.equals("1")) {
//                    month = dateMinusMonth(yearMonth);
//                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.greenBuildingData(year, month, areacodel);
//                } else {
//                    month = formatMonth();
//                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.monthGreenBuildingData(year, month, areacodel);
//                }
//            }
//
//        }

        double dArea = 0.00;
        int number = 0;
        List<MoonGreenBuildingPo> greenBuildingDataTOw = new ArrayList<>();
        if (greenBuildingData != null && greenBuildingData.size() > 0) {
            // List<MoonGreenBuildingPo> moonGreenBuildingPos = distinctList1(greenBuildingDataTOw, greenBuildingData);
            if (StringUtils.isNotEmpty(cityName)) {
                greenBuildingVo.setAreaname(cityName);
            }
            //1.公共建筑 2.住宅建筑 3.工业建筑
            if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("1")) {
                for (MoonGreenBuildingPo moonGreenBuildingPo : greenBuildingData) {
                    GreenBuildingDetails greenBuildingDetails = new GreenBuildingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildpublicArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildpublicNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }

            } else if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("3")) {
                for (MoonGreenBuildingPo moonGreenBuildingPo : greenBuildingData) {
                    GreenBuildingDetails greenBuildingDetails = new GreenBuildingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildindustryArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildindustryNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }
            } else if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("2")) {
                for (MoonGreenBuildingPo moonGreenBuildingPo : greenBuildingData) {
                    GreenBuildingDetails greenBuildingDetails = new GreenBuildingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildhouseArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildhouseNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }
            } else {
                if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("1")) {
                    for (MoonGreenBuildingPo moonGreenBuildingPo : greenBuildingData) {
                        GreenBuildingDetails greenBuildingDetails = new GreenBuildingDetails();
                        greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                        greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildpublicArea());
                        greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildpublicNumber());
                        if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                            greenBuildingDetailsList.add(greenBuildingDetails);
                            dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                            number = number + Integer.parseInt(greenBuildingDetails.getValue());
                        }
                        greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                    }
                }
            }
            DecimalFormat df = new DecimalFormat("#0.00");
            String format = df.format(dArea);
            greenBuildingVo.setGreenBuildingArea(format);
            greenBuildingVo.setGreenBuildingCount(Integer.toString(number));
            greenBuildingVos.add(greenBuildingVo);
        } else {
            greenBuildingVos = new ArrayList<>();
        }
        if (greenBuildingVos.size() == 0) {
            List<GreenBuildingVo> greenBuildingVosNew = new ArrayList<>();
            List<GreenBuildingDetails> list = new ArrayList<>();
            GreenBuildingVo greenBuildingVo1 = new GreenBuildingVo();
            greenBuildingVo1.setDetailsList(list);
            greenBuildingVosNew.add(greenBuildingVo1);
            return Result.OK(greenBuildingVosNew);
        }
        return Result.OK(greenBuildingVos);
    }

    private List<MoonGreenBuildingPo> distinctList1(List<MoonGreenBuildingPo> jackpotList1, List<MoonGreenBuildingPo> jackpotList) {
        jackpotList1.addAll(jackpotList);
        return jackpotList1.stream().collect(
                Collectors.collectingAndThen(
                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(MoonGreenBuildingPo::getAreaname))), ArrayList::new
                )
        );
    }
    /****************装配式建筑*********************/
    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<新开工装配式数据统计>
     * @Author ：Haozhiyang
     * @Date ：2022/8/23 14:35
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "具体数据信息")
    @ApiOperation(value = "具体数据信息", notes = "具体数据信息")
    @GetMapping(value = "/prefabricatedBuilding")
    public Result<?> prefabricatedBuilding(@RequestParam("areacodel") String areacode, @RequestParam("dateType") String dateType) throws Exception {

        if (StringUtils.isNotEmpty(areacode) && areacode.equals("62")) {
            areacode = "";
        }
        String year = "";
        String month = "";
        List<GreenBuilding> listZb = new ArrayList<>();
        if (dateType.equals("0")) {
            String yearMonth = formatMonth();
            month = dateMinusMonth(yearMonth);

        } else {
            if (dateType.length() > 2) {
                month = yyy() + "-" + dateType;
            } else {
                month = yyy() + "-0" + dateType;
            }
        }
        String prdType = "1";
        PrefabricaeZbVo prefabricaeZb = largeScreenZgsReportEnergyNewinfoGreentotalService.prefabricateBulidngZb(month, areacode, prdType);
        PrefabricatedBuildingPo prefabricatedBuildingPo = largeScreenZgsReportEnergyNewinfoGreentotalService.prefabricatedBuilding(year, month, areacode);
        //String newConstructiontoTalArea = largeScreenZgsReportEnergyNewinfoGreentotalService.newConstructiontoTalArea(year, month, areacode);
        /*double totalArea = 0.00, bzArea = 0.00, spArea = 0.00, ggArea = 0.00;
        if (prefabricatedBuildingPo != null) {
            if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getBzArea())) {
                bzArea = Double.parseDouble(prefabricatedBuildingPo.getBzArea());
            }
            if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getGgArea())) {
                ggArea = Double.parseDouble(prefabricatedBuildingPo.getGgArea());
            }
            if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getSpArea())) {
                spArea = Double.parseDouble(prefabricatedBuildingPo.getSpArea());
            }
            totalArea = bzArea + spArea + ggArea;
            DecimalFormat df = new DecimalFormat("#0.00");
            String formatTotalArea = df.format(totalArea);
            prefabricatedBuildingPo.setTotalArea(formatTotalArea);
        }*/
       /* if (StringUtils.isEmpty(newConstructiontoTalArea)) {
            newConstructiontoTalArea = "0.00";
        }*/
        /*if (prefabricatedBuildingPo == null) {
            prefabricatedBuildingPo = new PrefabricatedBuildingPo();
            prefabricatedBuildingPo.setTotalArea("0.00");
            prefabricatedBuildingPo.setBzArea("0.00");
            prefabricatedBuildingPo.setGgArea("0.00");
            prefabricatedBuildingPo.setSpArea("0.00");
        }*/
        List<GreenBuilding> list = new ArrayList<>();
        if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getNewConstructiontoTalArea())) {
            GreenBuilding greenBuilding = new GreenBuilding();
            greenBuilding.setName("新开工建筑总面积");
            greenBuilding.setValue(Double.parseDouble(prefabricatedBuildingPo.getNewConstructiontoTalArea()));
            list.add(greenBuilding);
            prefabricatedBuildingPo.setListData(list);
        }
//        } else {
//            prefabricatedBuildingPo = new PrefabricatedBuildingPo();
//            prefabricatedBuildingPo.setNewConstructiontoTalArea("0.00");
//            GreenBuilding greenBuilding = new GreenBuilding();
//            greenBuilding.setName("新开工建筑总面积");
//            greenBuilding.setValue(Double.parseDouble("0.00"));
//            list.add(greenBuilding);
//            prefabricatedBuildingPo.setListData(list);
//        }
        if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getTotalArea())) {
            GreenBuilding greenBuilding = new GreenBuilding();
            greenBuilding.setName("装配式建筑总面积");
            greenBuilding.setValue(Double.parseDouble(prefabricatedBuildingPo.getTotalArea()));
            list.add(greenBuilding);
            prefabricatedBuildingPo.setListData(list);
        }
        GreenBuilding greenBuilding = new GreenBuilding();
        greenBuilding.setName("新开工装配式占比");


        if (dateType.equals("0")) {
            greenBuilding.setValue(prefabricaeZb.getYearFarAssemblyrate());
        } else {
            greenBuilding.setValue(prefabricaeZb.getMonthFarAssemblyrate());

        }
        prefabricatedBuildingPo.setListDataNew(listZb);
        listZb.add(greenBuilding);

//        } else {
//            //prefabricatedBuildingPo = new PrefabricatedBuildingPo();
//            greenBuilding.setName("装配式建筑总面积");
//            greenBuilding.setValue(Double.parseDouble("0.00"));
//            list.add(greenBuilding);
//            prefabricatedBuildingPo.setListData(list);
//        }
        return Result.OK(prefabricatedBuildingPo);
    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<竣工装配式建筑数据统计>
     * @Author ：Haozhiyang
     * @Date ：2022/8/23 14:35
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    //TODO 427
    @AutoLog(value = "具体数据信息")
    @ApiOperation(value = "具体数据信息", notes = "具体数据信息")
    @GetMapping(value = "/fabricatedCompletion")
    public Result<?> fabricatedCompletion(@RequestParam("areacodel") String areacode, @RequestParam("dateType") String dateType) throws Exception {
        List<GreenBuilding> listZb = new ArrayList<>();
        if (StringUtils.isNotEmpty(areacode) && areacode.equals("62")) {
            areacode = "";
        }
        String year = "";
        String month = "";
        if (dateType.equals("0")) {
            String yearMonth = formatMonth();
            month = dateMinusMonth(yearMonth);
        } else {
            if (dateType.length() > 2) {
                month = yyy() + "-" + dateType;
            } else {
                month = yyy() + "-0" + dateType;
            }
        }
        String prdType = "2";
        PrefabricaeZbVo prefabricaeZb = largeScreenZgsReportEnergyNewinfoGreentotalService.prefabricateBulidngZb(month, areacode, prdType);

        PrefabricatedBuildingPo prefabricatedBuildingPo = largeScreenZgsReportEnergyNewinfoGreentotalService.fabricatedCompletion(year, month, areacode);
        //String totalBuildingArea = largeScreenZgsReportEnergyNewinfoGreentotalService.totalBuildingArea(year, month, areacode);
        double totalArea = 0.00;
        if (prefabricatedBuildingPo != null) {
            /*if (StringUtils.isEmpty(prefabricatedBuildingPo.getTotalArea())) {
                if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getBzArea())) {
                    totalArea = totalArea + Double.parseDouble(prefabricatedBuildingPo.getBzArea());

                }
                if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getGgArea())) {
                    totalArea = totalArea + Double.parseDouble(prefabricatedBuildingPo.getGgArea());
                }
                if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getSpArea())) {
                    totalArea = totalArea + Double.parseDouble(prefabricatedBuildingPo.getSpArea());
                }
                prefabricatedBuildingPo.setTotalArea(String.valueOf(totalArea));
            }*/
            List<GreenBuilding> list = new ArrayList<>();

            if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getNewConstructiontoTalArea())) {
                GreenBuilding greenBuilding = new GreenBuilding();
                greenBuilding.setName("建筑总面积");
                greenBuilding.setValue(Double.parseDouble(prefabricatedBuildingPo.getNewConstructiontoTalArea()));
                list.add(greenBuilding);
                prefabricatedBuildingPo.setListData(list);
            }
            if (StringUtils.isNotEmpty(prefabricatedBuildingPo.getTotalArea())) {
                GreenBuilding greenBuilding = new GreenBuilding();
                greenBuilding.setName("竣工装配式总面积");
                greenBuilding.setValue(Double.parseDouble(prefabricatedBuildingPo.getTotalArea()));
                list.add(greenBuilding);
                prefabricatedBuildingPo.setListData(list);
            }

        }
        GreenBuilding greenBuilding = new GreenBuilding();
        greenBuilding.setName("竣工装配式总面积占比");


        if (dateType.equals("0")) {
            greenBuilding.setValue(prefabricaeZb.getYearFarAssemblyrate());
        } else {
            greenBuilding.setValue(prefabricaeZb.getMonthFarAssemblyrate());

        }
        listZb.add(greenBuilding);
        prefabricatedBuildingPo.setListDataNew(listZb);
        /*if (prefabricatedBuildingPo == null) {
            PrefabricatedBuildingPo prefabricatedBuildingPo1 = new PrefabricatedBuildingPo();
            List<GreenBuilding> list = new ArrayList<>();
            prefabricatedBuildingPo1.setTotalArea("0.00");
            prefabricatedBuildingPo1.setNewConstructiontoTalArea("0.00");
            prefabricatedBuildingPo1.setBzArea("0.00");
            prefabricatedBuildingPo1.setGgArea("0.00");
            prefabricatedBuildingPo1.setSpArea("0.00");
            prefabricatedBuildingPo1.setNewConstructiontoTalArea("0.00");
            GreenBuilding greenBuilding = new GreenBuilding();
            greenBuilding.setName("建筑总面积");
            greenBuilding.setValue(Double.parseDouble("0.00"));
            list.add(greenBuilding);
            prefabricatedBuildingPo1.setListData(list);
            GreenBuilding greenBuilding1 = new GreenBuilding();
            greenBuilding1.setName("竣工装配式总面积");
            greenBuilding1.setValue(Double.parseDouble("0.00"));
            list.add(greenBuilding1);
            prefabricatedBuildingPo1.setListData(list);
            return Result.OK(prefabricatedBuildingPo1);
        }*/
        return Result.OK(prefabricatedBuildingPo);
    }

    /********************* 建筑节能Building energy saving***************************/
    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<建筑节能统计>
     * @Author ：Haozhiyang
     * @Date ：2022/8/23 16:05
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "建筑节能数据")
    @ApiOperation(value = "建筑节能数据", notes = "建筑节能数据")
    @GetMapping(value = "/buildingEnergySaving")
    public Result<?> buildingEnergySaving(@RequestParam("dateType") String dateType, @RequestParam("buildingType") String buildingType, @RequestParam("areacodel") String areacodel) throws Exception {
        List<BuildingEnergySavingVo> greenBuildingVos = new ArrayList<>();
        List<BuildingEnergySavingPo> greenBuildingData = new ArrayList<>();
        List<BuildingEnergySavingDetails> greenBuildingDetailsList = new ArrayList<>();
        BuildingEnergySavingVo greenBuildingVo = new BuildingEnergySavingVo();
        String cityName = largeScreenZgsReportEnergyNewinfoGreentotalService.getCityOne(areacodel);
        String year = "";//yyy();
        String month = "";
        String yearMonth = formatMonth();
        month = dateMinusMonth(yearMonth);
        if (areacodel.equals("62")) {
            List<City> codeS = largeScreenZgsReportEnergyNewinfoGreentotalService.getAllCities();
            if (codeS.size() > 0) {
                for (City city : codeS) {
                    if (!city.getCode().equals("62")) {
                        BuildingEnergySavingPo buildingEnergySavingPo = new BuildingEnergySavingPo();
                        if (dateType.equals("1")) {
                            buildingEnergySavingPo = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingPo(year, month, city.getCode());
                        }
                        if (dateType.equals("2")) {
                            month = formatMonth();
                            greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.monthBuildingEnergySavingPo(year, month, areacodel);
                        }
                        if (dateType.equals("3")) {
                            buildingEnergySavingPo = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingExistingPo(year, month, city.getCode());
                        }
                        if (dateType.equals("4")) {
                            month = formatMonth();
                            greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.monthBuildingEnergySavingExistingPo(year, month, areacodel);
                        }
                        if (buildingEnergySavingPo != null) {
                            buildingEnergySavingPo.setAreaname(city.getName());
                            greenBuildingData.add(buildingEnergySavingPo);
                        }
                    }
                }
            }
            greenBuildingVo.setAreaname("甘肃省");
        } else {
            if (StringUtils.isNotEmpty(dateType)) {
                if (dateType.equals("1")) {
                    String yearMonth1 = formatMonth();
                    month = dateMinusMonth(yearMonth1);
                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingData(year, month, areacodel);
                }
                if (dateType.equals("2")) {
                    month = formatMonth();
                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingData(year, month, areacodel);
                }
                if (dateType.equals("3")) {
                    String yearMonth1 = formatMonth();
                    month = dateMinusMonth(yearMonth1);
                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingExisting(year, month, areacodel);
                }
                if (dateType.equals("4")) {
                    month = formatMonth();
                    greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingExisting(year, month, areacodel);
                }
            } /*else {
                greenBuildingData = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingData(year, month, areacodel);
            }*/
        }
        double dArea = 0.00;
        int number = 0;
        if (greenBuildingData != null && greenBuildingData.size() > 0) {
            if (StringUtils.isNotEmpty(cityName)) {
                greenBuildingVo.setAreaname(cityName);
            }
            //1.公共建筑 2.住宅建筑 3.工业建筑
            if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("1")) {
                for (BuildingEnergySavingPo moonGreenBuildingPo : greenBuildingData) {
                    BuildingEnergySavingDetails greenBuildingDetails = new BuildingEnergySavingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildpublicArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildpublicNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }

            } else if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("2")) {
                for (BuildingEnergySavingPo moonGreenBuildingPo : greenBuildingData) {
                    BuildingEnergySavingDetails greenBuildingDetails = new BuildingEnergySavingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildhouseArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildhouseNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }
            } else if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("3")) {
                for (BuildingEnergySavingPo moonGreenBuildingPo : greenBuildingData) {
                    BuildingEnergySavingDetails greenBuildingDetails = new BuildingEnergySavingDetails();
                    greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                    greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildindustryArea());
                    greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildindustryNumber());
                    if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                        greenBuildingDetailsList.add(greenBuildingDetails);
                        dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                        number = number + Integer.parseInt(greenBuildingDetails.getValue());
                    }
                    greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                }
            } else {
                if (StringUtils.isNotEmpty(buildingType) && buildingType.equals("1")) {
                    for (BuildingEnergySavingPo moonGreenBuildingPo : greenBuildingData) {
                        BuildingEnergySavingDetails greenBuildingDetails = new BuildingEnergySavingDetails();
                        greenBuildingDetails.setName(moonGreenBuildingPo.getAreaname());
                        greenBuildingDetails.setArea(moonGreenBuildingPo.getYearBuildpublicArea());
                        greenBuildingDetails.setValue(moonGreenBuildingPo.getYearBuildpublicNumber());
                        if (StringUtils.isNotEmpty(greenBuildingDetails.getValue()) && !greenBuildingDetails.getValue().equals("0")) {
                            greenBuildingDetailsList.add(greenBuildingDetails);
                            dArea = dArea + Double.parseDouble(greenBuildingDetails.getArea());
                            number = number + Integer.parseInt(greenBuildingDetails.getValue());
                        }
                        greenBuildingVo.setDetailsList(greenBuildingDetailsList);
                    }
                }
            }
            DecimalFormat df = new DecimalFormat("#.00");
            String format = df.format(dArea);
            greenBuildingVo.setGreenBuildingArea(format);
            greenBuildingVo.setGreenBuildingCount(Integer.toString(number));
            greenBuildingVos.add(greenBuildingVo);
        }
        if (greenBuildingVos.size() == 0) {
            List<BuildingEnergySavingVo> greenBuildingVosNew = new ArrayList<>();
            List<BuildingEnergySavingDetails> list = new ArrayList<>();
            BuildingEnergySavingVo greenBuildingVo1 = new BuildingEnergySavingVo();
            greenBuildingVo1.setDetailsList(list);
            greenBuildingVosNew.add(greenBuildingVo1);
            return Result.OK(greenBuildingVosNew);
        }
        return Result.OK(greenBuildingVos);


    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<建筑节能统计>
     * @Author ：Haozhiyang
     * @Date ：2022/8/24 10:25
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "大屏建筑节能统计项目总数和绿色项目总数")
    @ApiOperation(value = "大屏建筑节能统计项目总数和绿色项目总数", notes = "大屏建筑节能统计项目总数和绿色项目总数")
    @GetMapping(value = "/buildingEnergySavingTotalTitems")
    public Result<?> buildingEnergySavingTotalTitems(@RequestParam("areacodel") String areacode) throws Exception {
        String year = "";
        String yearMonth = formatMonth();
        String month = dateMinusMonth(yearMonth);
        BuildingEnergySavingTotalTitemsPo totalTitemsPo = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergySavingTotalTitems(year, month, areacode);
        if (totalTitemsPo == null) {
            totalTitemsPo = new BuildingEnergySavingTotalTitemsPo();
            totalTitemsPo.setMonthArea("0.00");
            totalTitemsPo.setMonthNumber("0");
            totalTitemsPo.setYearBuildArea("0.00");
            totalTitemsPo.setYearBuildNumber("0");
        }
        return Result.OK(totalTitemsPo);
    }

    /**
     * @Name ：LargeScreenZgsReportEnergyNewinfoGreentotalController
     * @Description ：<可再生能源利用情况>
     * @Author ：Haozhiyang
     * @Date ：2022/8/24 11:03
     * @Version ：1.0
     * @History ：<修改代码时说明>
     */
    @AutoLog(value = "可再生能源利用情况")
    @ApiOperation(value = "可再生能源利用情况", notes = "可再生能源利用情况")
    @GetMapping(value = "/utilizationOfRenewableEnergy")
    public Result<?> utilizationOfRenewableEnergy(@RequestParam("areacodel") String areacode) throws Exception {
        String year;
        String yearMonth = formatMonth();
        year = dateMinusMonth(yearMonth);
        List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergy = new ArrayList<>();//获取所有的市
        //List<City> codeS = largeScreenZgsReportEnergyNewinfoGreentotalService.getAllCities();
        List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyPoList = new ArrayList<>();
        // if (codeS.size() > 0) {
        if (areacode.equals("62")) {
            areacode = "";
            //for (City city : codeS) {
            //if (!city.getCode().equals("62")) {
            //UtilizationOfRenewableEnergyPo utilization = new UtilizationOfRenewableEnergyPo();
            utilizationOfRenewableEnergyPoList = largeScreenZgsReportEnergyNewinfoGreentotalService.utilizationOfRenewableEnergyNew(year, areacode);
                       /* if (utilization != null) {
                            utilization.setAreaname(city.getName());
                            utilizationOfRenewableEnergy.add(utilization);
                        }*/
            // }
            //}
        } else {
            utilizationOfRenewableEnergyPoList = largeScreenZgsReportEnergyNewinfoGreentotalService.utilizationOfRenewableEnergyList(year, areacode);
        }
        //}
        return Result.OK(utilizationOfRenewableEnergyPoList);
    }

    @AutoLog(value = "建筑节能统计-具体数据信息")
    @ApiOperation(value = "建筑节能统计-具体数据信息", notes = "建筑节能统计-具体数据信息")
    @GetMapping(value = "/buildingEnergyEfficiencyStatistics")
    public Result<?> buildingEnergyEfficiencyStatistics(@RequestParam("areacodel") String areacode, @RequestParam("dateType") String dateType) throws Exception {
        if (StringUtils.isNotEmpty(areacode) && areacode.equals("62")) {
            areacode = "";
        }
        String year = "";
        String month = "";
        if (dateType.equals("1")) {
            //year = yyy();
            String yearMonth = formatMonth();
            month = dateMinusMonth(yearMonth);
        } else {
            month = formatMonth();
        }
        BuildingEnergyEfficiencyStatisticsPo buildingEnergyEfficiencyStatisticsPo = largeScreenZgsReportEnergyNewinfoGreentotalService.buildingEnergyEfficiencyStatistics(year, month, areacode);
        if (buildingEnergyEfficiencyStatisticsPo == null) {
            buildingEnergyEfficiencyStatisticsPo = new BuildingEnergyEfficiencyStatisticsPo();
        }
        return Result.OK(buildingEnergyEfficiencyStatisticsPo);
    }

}
