package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificresearcherMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificresearcherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsScientificresearcherServiceImpl extends ServiceImpl<ZgsScientificresearcherMapper, ZgsScientificresearcher> implements IZgsScientificresearcherService {

}
