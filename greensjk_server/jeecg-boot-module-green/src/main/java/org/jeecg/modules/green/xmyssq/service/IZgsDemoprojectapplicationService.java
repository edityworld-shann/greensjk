package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建设科技示范项目验收应用情况信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsDemoprojectapplicationService extends IService<ZgsDemoprojectapplication> {

}
