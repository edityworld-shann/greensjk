package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificpexecutivelist;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificpexecutivelistMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificpexecutivelistService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsScientificpexecutivelistServiceImpl extends ServiceImpl<ZgsScientificpexecutivelistMapper, ZgsScientificpexecutivelist> implements IZgsScientificpexecutivelistService {

}
