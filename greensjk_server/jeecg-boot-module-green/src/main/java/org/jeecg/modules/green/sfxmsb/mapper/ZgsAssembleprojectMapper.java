package org.jeecg.modules.green.sfxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssembleproject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式建筑示范工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsAssembleprojectMapper extends BaseMapper<ZgsAssembleproject> {

}
