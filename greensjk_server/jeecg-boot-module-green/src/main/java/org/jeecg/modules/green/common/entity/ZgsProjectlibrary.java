package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_projectlibrary")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_projectlibrary对象", description = "建筑工程项目库")
public class ZgsProjectlibrary implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 15, orderNum = "0")
    @ApiModelProperty(value = "工程项目名称")
    private java.lang.String projectname;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 申报单位
     */
    @Excel(name = "申报单位", width = 15, orderNum = "1")
    @ApiModelProperty(value = "申报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunit;
    /**
     * 申报单位项目负责人
     */
    @Excel(name = "申报单位项目负责人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "申报单位项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitprojectleader;
    /**
     * 申报单位联系电话
     */
    @Excel(name = "申报单位联系电话", width = 15, orderNum = "3")
    @ApiModelProperty(value = "申报单位联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitphone;
    /**
     * 完成日期(示范/)
     */
    @Excel(name = "完成日期(示范/)", width = 15, format = "yyyy-MM-dd", orderNum = "4")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "完成日期(示范/)")
    private java.util.Date completedate;
    /**
     * 项目类型
     */
    @Excel(name = "项目类型", width = 15, orderNum = "5")
    @ApiModelProperty(value = "项目类型")
    private java.lang.String projecttype;
    /**
     * 项目类型:字典DemonstrateType
     */
//    @Excel(name = "项目类型:字典DemonstrateType", width = 15)
    @ApiModelProperty(value = "项目类型:字典DemonstrateType")
    private java.lang.String projecttypenum;
    /**
     * 示范类别
     */
    @Excel(name = "示范类别", width = 15, orderNum = "6")
    @ApiModelProperty(value = "示范类别")
    private java.lang.String demonstratetype;
    /**
     * 示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)
     */
//    @Excel(name = "示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)", width = 15)
    @ApiModelProperty(value = "示范类别:字典(绿色建筑:GreenBuildType, 建筑工程:ConstructBuidType, 示范工程:SampleBuidType)")
    private java.lang.String demonstratetypenum;
    /**
     * 建筑类型
     */
    @Excel(name = "建筑类型", width = 15, orderNum = "7")
    @ApiModelProperty(value = "建筑类型")
    private java.lang.String projectbuildtype;
    /**
     * 建筑类型编码:字典ProjectBuildType
     */
//    @Excel(name = "建筑类型编码:字典ProjectBuildType", width = 15)
    @ApiModelProperty(value = "建筑类型编码:字典ProjectBuildType")
    private java.lang.String projectbuildtypenum;
    /**
     * 建筑用途类型
     */
    @Excel(name = "建筑用途类型", width = 15, orderNum = "8")
    @ApiModelProperty(value = "建筑用途类型")
    private java.lang.String projectpurpose;
    /**
     * 建筑用途类型编码:字典ProjectPurposeType
     */
//    @Excel(name = "建筑用途类型编码:字典ProjectPurposeType", width = 15)
    @ApiModelProperty(value = "建筑用途类型编码:字典ProjectPurposeType")
    private java.lang.String projectpurposenum;
    /**
     * 投资类型编码
     */
    @Excel(name = "投资类型编码", width = 15, orderNum = "9")
    @ApiModelProperty(value = "投资类型编码")
    private java.lang.String projectinvesttype;
    /**
     * 投资类型编码:字典ProjectInvestType
     */
//    @Excel(name = "投资类型编码:字典ProjectInvestType", width = 15)
    @ApiModelProperty(value = "投资类型编码:字典ProjectInvestType")
    private java.lang.String projectinvesttypenum;
    /**
     * 项目立项时间
     */
    @Excel(name = "项目立项时间", width = 15, format = "yyyy-MM-dd", orderNum = "10")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目立项时间")
    private java.util.Date projectstartdate;
    /**
     * 项目竣工时间
     */
    @Excel(name = "项目竣工时间", width = 15, format = "yyyy-MM-dd", orderNum = "11")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "项目竣工时间")
    private java.util.Date projectenddate;
    /**
     * 项目目前进展情况
     */
    @Excel(name = "项目目前进展情况", width = 15, orderNum = "12")
    @ApiModelProperty(value = "项目目前进展情况")
    private java.lang.String projectprogress;
    /**
     * 项目目前进展情况:字典ProjectProgress
     */
//    @Excel(name = "项目目前进展情况:字典ProjectProgress", width = 15)
    @ApiModelProperty(value = "项目目前进展情况:字典ProjectProgress")
    private java.lang.String projectprogressnum;
    /**
     * 总建筑面积:万m2
     */
    @Excel(name = "总建筑面积:万m2", width = 15, orderNum = "13")
    @ApiModelProperty(value = "总建筑面积:万m2")
    private java.math.BigDecimal totalbuiltarea;
    /**
     * 示范面积:万m2
     */
    @Excel(name = "示范面积:万m2", width = 15, orderNum = "14")
    @ApiModelProperty(value = "示范面积:万m2")
    private java.math.BigDecimal demonstrationarea;
    /**
     * 总投资:万元
     */
    @Excel(name = "总投资:万元", width = 15, orderNum = "15")
    @ApiModelProperty(value = "总投资:万元")
    private java.math.BigDecimal totalinvest;
    /**
     * 绿色建筑增量成本:元/m2
     */
    @Excel(name = "绿色建筑增量成本:元/m2", width = 15, orderNum = "16")
    @ApiModelProperty(value = "绿色建筑增量成本:元/m2")
    private java.math.BigDecimal greenbuildbulkingcost;
    /**
     * 是否发生重大质量安全事故:字典Accident
     */
    @Excel(name = "是否发生重大质量安全事故:字典Accident", width = 15, dicCode = "green_yn", orderNum = "16")
    @ApiModelProperty(value = "是否发生重大质量安全事故:字典Accident")
    private java.lang.String ishasaccident;
    /**
     * 施工许可证编号
     */
    @Excel(name = "施工许可证编号", width = 15, orderNum = "17")
    @ApiModelProperty(value = "施工许可证编号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructionlicence;
    /**
     * 业主单位
     */
    @Excel(name = "业主单位", width = 15, orderNum = "18")
    @ApiModelProperty(value = "业主单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerunit;
    /**
     * 业主单位传真
     */
    @Excel(name = "业主单位传真", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerfax;
    /**
     * 业主单位通讯地址
     */
    @Excel(name = "业主单位通讯地址", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String owneraddress;
    /**
     * 业主单位邮编
     */
    @Excel(name = "业主单位邮编", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerzipcode;
    /**
     * 业主单位负责人
     */
    @Excel(name = "业主单位负责人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownercharger;
    /**
     * 业主单位负责人电话
     */
    @Excel(name = "业主单位负责人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerchargerphone;
    /**
     * 业主单位负责人手机
     */
    @Excel(name = "业主单位负责人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位负责人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerchargermobile;
    /**
     * 业主单位联系人
     */
    @Excel(name = "业主单位联系人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerlinkman;
    /**
     * 业主单位联系人电话
     */
    @Excel(name = "业主单位联系人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位联系人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerlinkmanphone;
    /**
     * 业主单位联系人手机
     */
    @Excel(name = "业主单位联系人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "业主单位联系人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ownerlinkmanmobile;
    /**
     * 勘察单位
     */
    @Excel(name = "勘察单位", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveyunit;
    /**
     * 勘察单位传真
     */
    @Excel(name = "勘察单位传真", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveyfax;
    /**
     * 勘察单位通讯地址
     */
    @Excel(name = "勘察单位通讯地址", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveyaddress;
    /**
     * 勘察单位邮编
     */
    @Excel(name = "勘察单位邮编", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveyzipcode;
    /**
     * 勘察单位负责人
     */
    @Excel(name = "勘察单位负责人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveycharger;
    /**
     * 勘察单位负责人电话
     */
    @Excel(name = "勘察单位负责人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveychargerphone;
    /**
     * 勘察单位负责人手机
     */
    @Excel(name = "勘察单位负责人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位负责人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveychargermobile;
    /**
     * 勘察单位联系人
     */
    @Excel(name = "勘察单位联系人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveylinkman;
    /**
     * 勘察单位联系人电话
     */
    @Excel(name = "勘察单位联系人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位联系人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveylinkmanphone;
    /**
     * 勘察单位联系人手机
     */
    @Excel(name = "勘察单位联系人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "勘察单位联系人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String surveylinkmanmobile;
    /**
     * 设计单位
     */
    @Excel(name = "设计单位", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designunit;
    /**
     * 设计单位传真
     */
    @Excel(name = "设计单位传真", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designfax;
    /**
     * 设计单位通讯地址
     */
    @Excel(name = "设计单位通讯地址", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designaddress;
    /**
     * 设计单位邮编
     */
    @Excel(name = "设计单位邮编", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designzipcode;
    /**
     * 设计单位负责人
     */
    @Excel(name = "设计单位负责人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designcharger;
    /**
     * 设计单位负责人电话
     */
    @Excel(name = "设计单位负责人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designchargerphone;
    /**
     * 设计单位负责人手机
     */
    @Excel(name = "设计单位负责人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位负责人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designchargermobile;
    /**
     * 设计单位联系人
     */
    @Excel(name = "设计单位联系人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designlinkman;
    /**
     * 设计单位联系人电话
     */
    @Excel(name = "设计单位联系人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位联系人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designlinkmanphone;
    /**
     * 设计单位联系人手机
     */
    @Excel(name = "设计单位联系人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "设计单位联系人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String designlinkmanmobile;
    /**
     * 施工单位
     */
    @Excel(name = "施工单位", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructunit;
    /**
     * 施工单位传真
     */
    @Excel(name = "施工单位传真", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructfax;
    /**
     * 施工单位通讯地址
     */
    @Excel(name = "施工单位通讯地址", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructaddress;
    /**
     * 施工单位邮编
     */
    @Excel(name = "施工单位邮编", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructzipcode;
    /**
     * 施工单位负责人
     */
    @Excel(name = "施工单位负责人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructcharger;
    /**
     * 施工单位负责人电话
     */
    @Excel(name = "施工单位负责人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructchargerphone;
    /**
     * 施工单位负责人手机
     */
    @Excel(name = "施工单位负责人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位负责人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructchargermobile;
    /**
     * 施工单位联系人
     */
    @Excel(name = "施工单位联系人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructlinkman;
    /**
     * 施工单位联系人电话
     */
    @Excel(name = "施工单位联系人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位联系人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructlinkmanphone;
    /**
     * 施工单位联系人手机
     */
    @Excel(name = "施工单位联系人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "施工单位联系人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String constructlinkmanmobile;
    /**
     * 监理单位
     */
    @Excel(name = "监理单位", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchunit;
    /**
     * 监理单位传真
     */
    @Excel(name = "监理单位传真", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位传真")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchfax;
    /**
     * 监理单位通讯地址
     */
    @Excel(name = "监理单位通讯地址", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchaddress;
    /**
     * 监理单位邮编
     */
    @Excel(name = "监理单位邮编", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchzipcode;
    /**
     * 监理单位负责人
     */
    @Excel(name = "监理单位负责人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchcharger;
    /**
     * 监理单位负责人电话
     */
    @Excel(name = "监理单位负责人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位负责人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchchargerphone;
    /**
     * 监理单位负责人手机
     */
    @Excel(name = "监理单位负责人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位负责人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchchargermobile;
    /**
     * 监理单位联系人
     */
    @Excel(name = "监理单位联系人", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位联系人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchlinkman;
    /**
     * 监理单位联系人电话
     */
    @Excel(name = "监理单位联系人电话", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位联系人电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchlinkmanphone;
    /**
     * 监理单位联系人手机
     */
    @Excel(name = "监理单位联系人手机", width = 15, orderNum = "19")
    @ApiModelProperty(value = "监理单位联系人手机")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String watchlinkmanmobile;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 开始日期(示范/)
     */
    @Excel(name = "开始日期(示范/)", width = 15, format = "yyyy-MM-dd", orderNum = "5")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始日期(示范/)")
    private java.util.Date startdate;
    /**
     * 项目概况、示范工程主要实施内容（包括工程概况、主要技术、创新<难点及关键>技术、保障措施，500字以内）
     */
    @Excel(name = "项目概况、示范工程主要实施内容（包括工程概况、主要技术、创新<难点及关键>技术、保障措施，500字以内）", width = 15, orderNum = "5")
    @ApiModelProperty(value = "项目概况、示范工程主要实施内容（包括工程概况、主要技术、创新<难点及关键>技术、保障措施，500字以内）")
    private java.lang.String simplesummary;
    /**
     * 是否获过省级财政补贴:字典Financial
     */
    @Excel(name = "是否获过省级财政补贴:字典Financial", width = 15, orderNum = "19", dicCode = "green_yn")
    @ApiModelProperty(value = "是否获过省级财政补贴:字典Financial")
    private java.lang.String isfinancial;
    /**
     * 补贴金额（万元）
     */
    @Excel(name = "补贴金额（万元）", width = 15, orderNum = "19")
    @ApiModelProperty(value = "补贴金额（万元）")
    private java.math.BigDecimal financialamount;
    /**
     * 项目地址
     */
    @Excel(name = "项目地址", width = 15, orderNum = "3")
    @ApiModelProperty(value = "项目地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectaddress;
    /**
     * 真实完成时间
     */
//    @Excel(name = "真实完成时间", width = 15, format = "yyyy-MM-dd", orderNum = "5")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "真实完成时间")
    private java.util.Date realenddate;
    /**
     * applyunitemail
     */
//    @Excel(name = "applyunitemail", width = 15, orderNum = "5")
    @ApiModelProperty(value = "applyunitemail")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitemail;
    /**
     * applyunitlinkman
     */
//    @Excel(name = "applyunitlinkman", width = 15, orderNum = "5")
    @ApiModelProperty(value = "applyunitlinkman")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkman;
    /**
     * applyunitlinkmanphone
     */
//    @Excel(name = "applyunitlinkmanphone", width = 15, orderNum = "5")
    @ApiModelProperty(value = "applyunitlinkmanphone")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkmanphone;
    /**
     * applyunitlinkmanemail
     */
//    @Excel(name = "applyunitlinkmanemail", width = 15, orderNum = "5")
    @ApiModelProperty(value = "applyunitlinkmanemail")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String applyunitlinkmanemail;
    /**
     * 1：终止日期延期2:闭合-终止
     */
//    @Excel(name = "1：终止日期延期2:闭合-终止", width = 15)
    @ApiModelProperty(value = "1：终止日期延期2:闭合-终止")
    private java.math.BigDecimal isdealoverdue;
    /**
     * 过期处理时间,2019.10.14 add
     */
//    @Excel(name = "过期处理时间,2019.10.14 add", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "过期处理时间,2019.10.14 add")
    private java.util.Date dealdate;
    /**
     * 过期处理说明,2019.10.14 add
     */
//    @Excel(name = "过期处理说明,2019.10.14 add", width = 15)
    @ApiModelProperty(value = "过期处理说明,2019.10.14 add")
    private java.lang.String dealmark;
    /**
     * 项目负责人年龄
     */
    @Excel(name = "项目负责人年龄", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人年龄")
    private java.math.BigDecimal age;
    /**
     * 费用状态
     */
    @ApiModelProperty(value = "费用状态")
    private String feeStatus = "0";
    /**
     * 1项目库2示范项目
     */
    @ApiModelProperty(value = "1项目库2示范项目")
    private String sfxmtype;
    /**
     * 项目库状态：1未入库2已入库3已推荐
     */
    @ApiModelProperty(value = "项目库状态：1未入库2已入库3已推荐")
    private String sfxmstatus;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(exist = false)
    private ZgsGreenbuildproject zgsGreenbuildproject;
    @TableField(exist = false)
    private ZgsBuildproject zgsBuildproject;
    @TableField(exist = false)
    private ZgsSamplebuildproject zgsSamplebuildproject;
    @TableField(exist = false)
    private ZgsEnergybuildproject zgsEnergybuildproject;
    @TableField(exist = false)
    private ZgsAssembleproject zgsAssembleproject;
    @TableField(exist = false)
    private List<ZgsGreenprojectplanarrange> zgsGreenprojectplanarrangeList;
    @TableField(exist = false)
    private List<ZgsEnergyprojectplanarrange> zgsEnergyprojectplanarrangeList;
    @TableField(exist = false)
    private List<ZgsAssemblesingleproject> zgsAssemblesingleprojectList;
    @TableField(exist = false)
    private ZgsAssemblesingleproject zgsAssemblesingleproject;
    @TableField(exist = false)
    private List<ZgsBuildjointunit> zgsBuildjointunitList;
    @TableField(exist = false)
    private List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList;
    @TableField(exist = false)
    private List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList;
    @TableField(exist = false)
    private List<ZgsSamplerojectmainparticipant> zgsSamplerojectmainparticipantList;
    @TableField(exist = false)
    private List<ZgsEnergyprojectmainparticipant> zgsEnergyprojectmainparticipantList;
    @TableField(exist = false)
    private List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList0;
    @TableField(exist = false)
    private List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList1;
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    @TableField(exist = false)
    private List<ZgsBuildfundbudget> zgsBuildfundbudgetList;
    @TableField(exist = false)
    private Integer spStatus;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private List<ZgsProjectlibraryexpert> zgsSofttechexpertList;
    @TableField(exist = false)
    private String status;
    @TableField(exist = false)
    private java.math.BigDecimal agreeproject;
    /**
     * 行政区域代码
     */
    @Excel(name = "行政区域代码", width = 15)
    @ApiModelProperty(value = "行政区域代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    @ApiModelProperty(value = "申报年度编码")
    private java.lang.String yearNum;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    /**
     * 是否会审
     */
    private String sfhs;
    @ApiModelProperty(value = "省科技经费总额")
//    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal provincialfundtotal;

    // 添加专家信息
    @TableField(exist = false)
    private List<ZgsSacceptcertexpert> zgsSacceptcertexpertList;

    @ApiModelProperty(value = "授权编辑 1同意编辑  2取消编辑")
    private String sqbj;

    @ApiModelProperty(value = "编辑保存 1修改未提交  2修改已提交")
    private String updateSave;

    @ApiModelProperty(value = "经费备注")
    private java.lang.String fundRemark;



}
