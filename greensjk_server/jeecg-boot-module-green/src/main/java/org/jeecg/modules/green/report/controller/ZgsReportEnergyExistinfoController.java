package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfo;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfoTotal;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoTotalService;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 既有建筑-节能改造项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Api(tags = "既有建筑-节能改造项目清单")
@RestController
@RequestMapping("/report/zgsReportEnergyExistinfo")
@Slf4j
public class ZgsReportEnergyExistinfoController extends JeecgController<ZgsReportEnergyExistinfo, IZgsReportEnergyExistinfoService> {
    @Autowired
    private IZgsReportEnergyExistinfoService zgsReportEnergyExistinfoService;
    @Autowired
    private IZgsReportEnergyExistinfoTotalService zgsReportEnergyExistinfoTotalService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页列表查询
     *
     * @param zgsReportEnergyExistinfo
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-分页列表查询")
    @ApiOperation(value = "既有建筑-节能改造项目清单-分页列表查询", notes = "既有建筑-节能改造项目清单-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportEnergyExistinfo zgsReportEnergyExistinfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyExistinfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (zgsReportEnergyExistinfo != null) {
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getProjectname())) {
                queryWrapper.like("projectname", zgsReportEnergyExistinfo.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getFilltm())) {
                queryWrapper.eq("filltm", zgsReportEnergyExistinfo.getFilltm());
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            queryWrapper.eq("applystate", GlobalConstants.apply_state2);
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getAreacode())) {
                if (zgsReportEnergyExistinfo.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportEnergyExistinfo.getAreacode());
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportEnergyExistinfo.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportEnergyExistinfo.getAreacode());
                } else {
//                    queryWrapper.last(" and length(areacode)=4");
                }
            } else {
//                queryWrapper.last(" and length(areacode)=4");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getAreacode())) {
                if (zgsReportEnergyExistinfo.getAreacode().length() == 6) {
                    queryWrapper.eq("applystate", GlobalConstants.apply_state2);
                    queryWrapper.eq("areacode", zgsReportEnergyExistinfo.getAreacode());
                } else {
                    queryWrapper.eq("areacode", zgsReportEnergyExistinfo.getAreacode());
                }
            } else {
                queryWrapper.eq("areacode", sysUser.getAreacode());
                //再加上县区已审核通过的
                queryWrapper.or(w1 -> {
                    if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getFilltm())) {
                        w1.eq("filltm", zgsReportEnergyExistinfo.getFilltm());
                    }
                    if (StringUtils.isNotEmpty(zgsReportEnergyExistinfo.getProjectname())) {
                        w1.like("projectname", zgsReportEnergyExistinfo.getProjectname());
                    }
                    w1.likeRight("areacode", sysUser.getAreacode());
                    w1.and(w2 -> {
                        w2.eq("applystate", GlobalConstants.apply_state2).or().eq("applystate", GlobalConstants.apply_state1);
                        w2.last(" and length(areacode)=6");
                    });
                });
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByAsc("areacode");
        Page<ZgsReportEnergyExistinfo> page = new Page<ZgsReportEnergyExistinfo>(pageNo, pageSize);
        IPage<ZgsReportEnergyExistinfo> pageList = zgsReportEnergyExistinfoService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsReportEnergyExistinfo
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-添加")
    @ApiOperation(value = "既有建筑-节能改造项目清单-添加", notes = "既有建筑-节能改造项目清单-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportEnergyExistinfo zgsReportEnergyExistinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportEnergyExistinfo.setCreatepersonaccount(sysUser.getUsername());
        zgsReportEnergyExistinfo.setCreatepersonname(sysUser.getRealname());
        zgsReportEnergyExistinfo.setCreatetime(new Date());
        zgsReportEnergyExistinfo.setAreacode(sysUser.getAreacode());
        zgsReportEnergyExistinfo.setAreaname(sysUser.getAreaname());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (zgsReportEnergyExistinfo.getEndDate() != null) {
            yearMonth = simpleDateFormat.format(zgsReportEnergyExistinfo.getEndDate());
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportEnergyExistinfo.setFilltm(yearMonth);
        zgsReportEnergyExistinfo.setQuarter(new BigDecimal(quarter));
        zgsReportEnergyExistinfo.setYear(new BigDecimal(year));
        //添加前先查询是否本月已上报
        QueryWrapper<ZgsReportEnergyExistinfo> queryWrapper = new QueryWrapper<ZgsReportEnergyExistinfo>();
        queryWrapper.eq("filltm", yearMonth);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportEnergyExistinfo> list = zgsReportEnergyExistinfoService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("本月已审批，退回后方可新增！");
        }
        zgsReportEnergyExistinfoService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfo, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }


    /**
     * 零上报
     * <p>
     * 既有建筑-节能改造项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "零上报-既有建筑-节能改造项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "零上报-既有建筑-节能改造项目清单-本月一键上报（县区或市州）", notes = "零上报-既有建筑-节能改造项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/initMonthNewInfoZero")
    public Result<?> initMonthNewInfoZero(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "ExistZeroItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        //再判断本月是否已上报
//        QueryWrapper<ZgsReportEnergyExistinfoTotal> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", filltm);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        wrapper.eq("applystate", GlobalConstants.apply_state2);
//        List<ZgsReportEnergyExistinfoTotal> listTotal = zgsReportEnergyExistinfoTotalService.list(wrapper);
        QueryWrapper<ZgsReportEnergyExistinfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("filltm", filltm);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.ne("isdelete", 1);
        List<ZgsReportEnergyExistinfo> list = zgsReportEnergyExistinfoService.list(queryWrapper);
//        if (listTotal.size() > 0 && list.size() > 0) {
        if (list.size() > 0) {
            return Result.error("本月项目清单删除后，方可进行无数据上报操作！");
        }
        zgsReportEnergyExistinfoService.addIterm(filltm);
        zgsReportEnergyExistinfoTotalService.initEnergyExistinfoTotalZero(year, quarter, filltm);
        return Result.OK("本月一键上报成功！");
    }


    /**
     * 既有建筑-节能改造项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "既有建筑-节能改造项目清单-本月一键上报（县区或市州）", notes = "既有建筑-节能改造项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/initMonthNewInfo")
    public Result<?> initMonthNewInfo(@RequestParam(name = "filltm") String filltm) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "ExistItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        QueryWrapper<ZgsReportEnergyExistinfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("filltm", filltm);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.isNotNull("end_date");
        List<ZgsReportEnergyExistinfo> list = zgsReportEnergyExistinfoService.list(queryWrapper);
        if (list.size() == 0) {
            return Result.error("请完整填写项目清单！");
        }
        //再判断本月是否已上报
        QueryWrapper<ZgsReportEnergyExistinfoTotal> wrapper = new QueryWrapper();
        wrapper.eq("filltm", filltm);
        wrapper.eq("areacode", sysUser.getAreacode());
        wrapper.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportEnergyExistinfoTotal> listTotal = zgsReportEnergyExistinfoTotalService.list(wrapper);
        if (listTotal.size() > 0) {
            return Result.error("本月数据已上报！");
        }
//        zgsReportEnergyExistinfoTotalService.initEnergyExistinfoTotal(year, quarter, filltm);
        zgsReportEnergyExistinfoTotalService.energyExistinfoTotalAll(0, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
        return Result.OK("本月一键上报成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportEnergyExistinfo
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-编辑")
    @ApiOperation(value = "既有建筑-节能改造项目清单-编辑", notes = "既有建筑-节能改造项目清单-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportEnergyExistinfo zgsReportEnergyExistinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportEnergyExistinfo.setModifypersonaccount(sysUser.getUsername());
        zgsReportEnergyExistinfo.setModifypersonname(sysUser.getRealname());
        zgsReportEnergyExistinfo.setModifytime(new Date());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (zgsReportEnergyExistinfo.getEndDate() != null) {
            yearMonth = simpleDateFormat.format(zgsReportEnergyExistinfo.getEndDate());
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportEnergyExistinfo.setFilltm(yearMonth);
        zgsReportEnergyExistinfo.setQuarter(new BigDecimal(quarter));
        zgsReportEnergyExistinfo.setYear(new BigDecimal(year));
        zgsReportEnergyExistinfoService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfo, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-通过id删除")
    @ApiOperation(value = "既有建筑-节能改造项目清单-通过id删除", notes = "既有建筑-节能改造项目清单-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        ZgsReportEnergyExistinfo project = zgsReportEnergyExistinfoService.getById(id);
        if (project != null) {
            if ("0".equals(project.getApplystate().toString()) || "3".equals(project.getApplystate().toString())) {
                zgsReportEnergyExistinfoService.removeById(id);
            } else {
                return Result.error("清单已上报，禁止删除！");
            }
        }
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-批量删除")
    @ApiOperation(value = "既有建筑-节能改造项目清单-批量删除", notes = "既有建筑-节能改造项目清单-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportEnergyExistinfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "既有建筑-节能改造项目清单-通过id查询")
    @ApiOperation(value = "既有建筑-节能改造项目清单-通过id查询", notes = "既有建筑-节能改造项目清单-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportEnergyExistinfo zgsReportEnergyExistinfo = zgsReportEnergyExistinfoService.getById(id);
        if (zgsReportEnergyExistinfo == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportEnergyExistinfo);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportEnergyExistinfo
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportEnergyExistinfo zgsReportEnergyExistinfo,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String title = "既有建筑-节能改造项目清单";
        Result<?> result = queryPageList(zgsReportEnergyExistinfo, 1, 9999, req);
        IPage<ZgsReportEnergyExistinfo> pageList = (IPage<ZgsReportEnergyExistinfo>) result.getResult();
        List<ZgsReportEnergyExistinfo> list = pageList.getRecords();
        //1、住宅建筑面积2、公共建筑面积
        for (ZgsReportEnergyExistinfo zgsR : list) {
            switch (zgsR.getBuildType()) {
                case "1":
                    zgsR.setBuildType("住宅建筑");
                    break;
                case "2":
                    zgsR.setBuildType("公共建筑");
                    break;
            }
        }
        List<ZgsReportEnergyExistinfo> listNew = list;
        List<ZgsReportEnergyExistinfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportEnergyExistinfo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportEnergyExistinfo, ZgsReportEnergyExistinfo.class, "既有建筑-节能改造项目清单");
        return mv;
    }

    private String getId(ZgsReportEnergyExistinfo item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportEnergyExistinfo.class);
    }

}
