package org.jeecg.modules.green.task.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 任务书项目经费预算
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Data
@TableName("zgs_taskbuildfundbudget")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_taskbuildfundbudget对象", description = "任务书项目经费预算")
public class ZgsTaskbuildfundbudget implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 对应业务表主键
     */
    @Excel(name = "对应业务表主键", width = 15)
    @ApiModelProperty(value = "对应业务表主键")
    private java.lang.String taskguid;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 年度
     */
    @Excel(name = "年度", width = 15)
    @ApiModelProperty(value = "年度")
    private java.math.BigDecimal year;
    /**
     * 省科技经费
     */
    @Excel(name = "省科技经费", width = 15)
    @ApiModelProperty(value = "省科技经费")
    private java.math.BigDecimal provincialfund;
    /**
     * 银行贷款
     */
    @Excel(name = "银行贷款", width = 15)
    @ApiModelProperty(value = "银行贷款")
    private java.math.BigDecimal bankloans;
    /**
     * 单位自筹
     */
    @Excel(name = "单位自筹", width = 15)
    @ApiModelProperty(value = "单位自筹")
    private java.math.BigDecimal unitselffund;
    /**
     * 国家投资
     */
    @Excel(name = "国家投资", width = 15)
    @ApiModelProperty(value = "国家投资")
    private java.math.BigDecimal nationalinvest;
    /**
     * 总计（人民币）
     */
    @Excel(name = "总计（人民币）", width = 15)
    @ApiModelProperty(value = "总计（人民币）")
    private java.math.BigDecimal totalfund;
    /**
     * 省科技经费总额
     */
    @Excel(name = "省科技经费总额", width = 15)
    @ApiModelProperty(value = "省科技经费总额")
    private java.math.BigDecimal provincialfundtotal;
    /**
     * 示范类型
     */
    @Excel(name = "示范类型", width = 15)
    @ApiModelProperty(value = "示范类型")
    private java.lang.String buildtype;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;

    @Excel(name = "省科技经费备注", width = 15)
    @ApiModelProperty(value = "省科技经费备注")
    @TableField(exist = false)
    private java.lang.String provinceFundRemark;
}
