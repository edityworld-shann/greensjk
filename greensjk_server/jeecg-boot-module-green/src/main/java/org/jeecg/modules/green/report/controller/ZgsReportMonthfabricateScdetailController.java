package org.jeecg.modules.green.report.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricate;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricateScdetail;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateScdetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthfabricate_scdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_monthfabricate_scdetail")
@RestController
@RequestMapping("/report/zgsReportMonthfabricateScdetail")
@Slf4j
public class ZgsReportMonthfabricateScdetailController extends JeecgController<ZgsReportMonthfabricateScdetail, IZgsReportMonthfabricateScdetailService> {
	@Autowired
	private IZgsReportMonthfabricateScdetailService zgsReportMonthfabricateScdetailService;

   @Autowired
   private IZgsReportMonthfabricateService zgsReportMonthfabricateService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthfabricateScdetail
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-分页列表查询")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-分页列表查询", notes="zgs_report_monthfabricate_scdetail-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthfabricateScdetail zgsReportMonthfabricateScdetail,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportMonthfabricateScdetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabricateScdetail, req.getParameterMap());
		Page<ZgsReportMonthfabricateScdetail> page = new Page<ZgsReportMonthfabricateScdetail>(pageNo, pageSize);
		IPage<ZgsReportMonthfabricateScdetail> pageList = zgsReportMonthfabricateScdetailService.page(page, queryWrapper);
		if (pageList.getRecords().size() > 0){
      for (int i = 0;i < pageList.getRecords().size();i++){
        ZgsReportMonthfabricate zgsReportMonthfabricate = zgsReportMonthfabricateService.getById(pageList.getRecords().get(i).getMonthmianguid());
        if (zgsReportMonthfabricate != null){
          pageList.getRecords().get(i).setAreaname(zgsReportMonthfabricate.getAreaname());
          pageList.getRecords().get(i).setAreacode(zgsReportMonthfabricate.getAreacode());
        }
      }
    }
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportMonthfabricateScdetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-添加")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-添加", notes="zgs_report_monthfabricate_scdetail-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthfabricateScdetail zgsReportMonthfabricateScdetail) {
		zgsReportMonthfabricateScdetailService.save(zgsReportMonthfabricateScdetail);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthfabricateScdetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-编辑")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-编辑", notes="zgs_report_monthfabricate_scdetail-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthfabricateScdetail zgsReportMonthfabricateScdetail) {
		zgsReportMonthfabricateScdetailService.updateById(zgsReportMonthfabricateScdetail);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-通过id删除")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-通过id删除", notes="zgs_report_monthfabricate_scdetail-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthfabricateScdetailService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-批量删除")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-批量删除", notes="zgs_report_monthfabricate_scdetail-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthfabricateScdetailService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabricate_scdetail-通过id查询")
	@ApiOperation(value="zgs_report_monthfabricate_scdetail-通过id查询", notes="zgs_report_monthfabricate_scdetail-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthfabricateScdetail zgsReportMonthfabricateScdetail = zgsReportMonthfabricateScdetailService.getById(id);
		if(zgsReportMonthfabricateScdetail==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportMonthfabricateScdetail);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthfabricateScdetail
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabricateScdetail zgsReportMonthfabricateScdetail) {
        return super.exportXls(request, zgsReportMonthfabricateScdetail, ZgsReportMonthfabricateScdetail.class, "zgs_report_monthfabricate_scdetail");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabricateScdetail.class);
    }

}
