package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertmonograph;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertmonographMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertmonographService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库专家专著信息
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertmonographServiceImpl extends ServiceImpl<ZgsExpertmonographMapper, ZgsExpertmonograph> implements IZgsExpertmonographService {

}
