package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import org.jeecg.modules.green.xmyssq.mapper.ZgsDemoprojectapplicationMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectapplicationService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 建设科技示范项目验收应用情况信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsDemoprojectapplicationServiceImpl extends ServiceImpl<ZgsDemoprojectapplicationMapper, ZgsDemoprojectapplication> implements IZgsDemoprojectapplicationService {

}
