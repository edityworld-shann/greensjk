package org.jeecg.modules.green.report.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreacompany;

/**
 * @Description: zgs_report_monthfabr_areaproject
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreacompanyService extends IService<ZgsReportMonthfabrAreacompany> {

}
