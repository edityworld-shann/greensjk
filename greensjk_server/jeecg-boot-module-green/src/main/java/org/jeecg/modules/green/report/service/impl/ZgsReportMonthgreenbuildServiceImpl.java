package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuild;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthgreenbuildMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthgreenbuild
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthgreenbuildServiceImpl extends ServiceImpl<ZgsReportMonthgreenbuildMapper, ZgsReportMonthgreenbuild> implements IZgsReportMonthgreenbuildService {

}
