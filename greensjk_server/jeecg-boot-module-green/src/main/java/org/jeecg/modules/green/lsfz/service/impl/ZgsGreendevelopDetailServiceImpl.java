package org.jeecg.modules.green.lsfz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelopDetail;
import org.jeecg.modules.green.lsfz.mapper.ZgsGreendevelopDetailMapper;
import org.jeecg.modules.green.lsfz.service.ZgsGreendevelopDetailService;
import org.springframework.stereotype.Service;

/**
 * @describe: 绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/13 14:31
 */
@Service
public class ZgsGreendevelopDetailServiceImpl extends ServiceImpl<ZgsGreendevelopDetailMapper, ZgsGreendevelopDetail> implements ZgsGreendevelopDetailService {





}
