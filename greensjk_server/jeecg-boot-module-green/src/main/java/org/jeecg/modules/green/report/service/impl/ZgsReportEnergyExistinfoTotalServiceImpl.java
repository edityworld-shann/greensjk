package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyExistinfoTotalMapper;
import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoTotalService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 既有建筑节能改造完成情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Service
public class ZgsReportEnergyExistinfoTotalServiceImpl extends ServiceImpl<ZgsReportEnergyExistinfoTotalMapper, ZgsReportEnergyExistinfoTotal> implements IZgsReportEnergyExistinfoTotalService {
    @Autowired
    private IZgsReportEnergyExistinfoService zgsReportEnergyExistinfoService;


    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
    @Override
    public void initEnergyExistinfoTotalZero(String year, int quarter, String yearMonth) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //计算当月上报合计
        //三、既有建筑节能情况
        energyExistinfoTotalZero(sysUser, year, quarter, yearMonth);
    }


    private void energyExistinfoTotalZero(LoginUser sysUser, String year, int quarter, String yearMonth) {
        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
        totalInfo.setApplydate(new Date());
        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        totalInfo.setBackreason("");
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyExistinfoTotal totalYearInfo = this.baseMapper.totalMonthExistinfoDataYear(yearMonth, sysUser.getAreacode(), year);
        //2、计算本月所有汇总记录
//        ZgsReportEnergyExistinfoTotal totalMonthInfo = this.baseMapper.totalMonthExistinfoData(yearMonth, sysUser.getAreacode());
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(0);
        totalInfo.setMonthArea(new BigDecimal(0));
        totalInfo.setMonthHouseNumber(0);
        totalInfo.setMonthHouseArea(new BigDecimal(0));
        totalInfo.setMonthPublicNumber(0);
        totalInfo.setMonthPublicArea(new BigDecimal(0));
        //
        totalInfo.setYearBuildNumber(totalYearInfo.getYearBuildNumber());
        totalInfo.setYearBuildArea(totalYearInfo.getYearBuildArea());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", sysUser.getAreacode());
        queryW.isNull("area_type");
        ZgsReportEnergyExistinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            totalInfo.setModifypersonaccount(sysUser.getUsername());
            totalInfo.setModifypersonname(sysUser.getRealname());
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            totalInfo.setId(UUID.randomUUID().toString());
            totalInfo.setCreatepersonaccount(sysUser.getUsername());
            totalInfo.setCreatepersonname(sysUser.getRealname());
            totalInfo.setCreatetime(new Date());
            totalInfo.setAreacode(sysUser.getAreacode());
            totalInfo.setAreaname(sysUser.getAreaname());
            totalInfo.setFilltm(yearMonth);
            totalInfo.setQuarter(new BigDecimal(quarter));
            totalInfo.setYear(new BigDecimal(year));
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        }
    }
    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--

    @Override
    public void energyExistinfoTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        //计算当月上报合计
        //三、既有建筑节能情况
        energyExistinfoTotalChildAll(type, year, quarter, yearMonth, areacode, areaname, applyState, sysUser);
        if (type == 0) {
            //上报完修改上报状态
            QueryWrapper<ZgsReportEnergyExistinfo> wrapper = new QueryWrapper();
            wrapper.eq("filltm", yearMonth);
            wrapper.eq("areacode", areacode);
            wrapper.isNotNull("end_date");
            ZgsReportEnergyExistinfo energyNewinfo = new ZgsReportEnergyExistinfo();
            energyNewinfo.setApplystate(new BigDecimal(applyState));
            energyNewinfo.setBackreason("");
            energyNewinfo.setApplydate(new Date());
            energyNewinfo.setBuildingArea(null);
            zgsReportEnergyExistinfoService.update(energyNewinfo, wrapper);
        }
    }

    @Override
    public void initAddZeroExistinfoNotInExistinfoTotal() {
        List<ZgsReportEnergyExistinfoTotal> list = this.baseMapper.selectExistInfoTotalNotInExistInfoList();
        if (list != null && list.size() > 0) {
            for (ZgsReportEnergyExistinfoTotal zgsReportEnergyExistinfoTotal : list) {
                String filltm = zgsReportEnergyExistinfoTotal.getFilltm();
                String createpersonaccount = zgsReportEnergyExistinfoTotal.getCreatepersonaccount();
                String createpersonname = zgsReportEnergyExistinfoTotal.getCreatepersonname();
                String areacode = zgsReportEnergyExistinfoTotal.getAreacode();
                String areaname = zgsReportEnergyExistinfoTotal.getAreaname();
                zgsReportEnergyExistinfoService.addItermInitTask(filltm, createpersonaccount, createpersonname, areacode, areaname);
            }
        }
    }

    private void energyExistinfoTotalChildAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
        if (type == 0) {
            totalInfo.setApplydate(new Date());
            totalInfo.setBackreason("");
        }
        if (applyState != null) {
            totalInfo.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyExistinfoTotal totalYearInfo = this.baseMapper.totalMonthExistinfoDataYear(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyExistinfoTotal totalMonthInfo = this.baseMapper.totalMonthExistinfoData(yearMonth, areacode);
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
        totalInfo.setMonthHouseNumber(totalMonthInfo.getMonthHouseNumber());
        totalInfo.setMonthHouseArea(totalMonthInfo.getMonthHouseArea());
        totalInfo.setMonthPublicNumber(totalMonthInfo.getMonthPublicNumber());
        totalInfo.setMonthPublicArea(totalMonthInfo.getMonthPublicArea());
        //
        totalInfo.setYearBuildNumber(totalYearInfo.getYearBuildNumber());
        totalInfo.setYearBuildArea(totalYearInfo.getYearBuildArea());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNull("area_type");
        ZgsReportEnergyExistinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            if (sysUser != null) {
                totalInfo.setModifypersonaccount(sysUser.getUsername());
                totalInfo.setModifypersonname(sysUser.getRealname());
            }
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                //新增
                totalInfo.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    totalInfo.setCreatepersonaccount(sysUser.getUsername());
                    totalInfo.setCreatepersonname(sysUser.getRealname());
                }
                totalInfo.setCreatetime(new Date());
                totalInfo.setAreacode(areacode);
                totalInfo.setAreaname(areaname);
                totalInfo.setFilltm(yearMonth);
                totalInfo.setQuarter(new BigDecimal(quarter));
                totalInfo.setYear(new BigDecimal(year));
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }


//    @Override
//    public void initEnergyExistinfoTotal(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        //计算当月上报合计
//        //三、既有建筑节能情况
//        energyExistinfoTotal(sysUser, year, quarter, yearMonth);
//        //上报完修改上报状态
//        QueryWrapper<ZgsReportEnergyExistinfo> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", yearMonth);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        wrapper.isNotNull("end_date");
//        ZgsReportEnergyExistinfo energyNewinfo = new ZgsReportEnergyExistinfo();
//        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        energyNewinfo.setBackreason("");
//        energyNewinfo.setApplydate(new Date());
//        energyNewinfo.setBuildingArea(null);
//        zgsReportEnergyExistinfoService.update(energyNewinfo, wrapper);
//    }

//    private void energyExistinfoTotal(LoginUser sysUser, String year, int quarter, String yearMonth) {
//        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
//        totalInfo.setApplydate(new Date());
//        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        totalInfo.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyExistinfoTotal totalYearInfo = this.baseMapper.totalMonthExistinfoDataYear(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyExistinfoTotal totalMonthInfo = this.baseMapper.totalMonthExistinfoData(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
//        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
//        totalInfo.setMonthHouseNumber(totalMonthInfo.getMonthHouseNumber());
//        totalInfo.setMonthHouseArea(totalMonthInfo.getMonthHouseArea());
//        totalInfo.setMonthPublicNumber(totalMonthInfo.getMonthPublicNumber());
//        totalInfo.setMonthPublicArea(totalMonthInfo.getMonthPublicArea());
//        //
//        totalInfo.setYearBuildNumber(totalYearInfo.getYearBuildNumber());
//        totalInfo.setYearBuildArea(totalYearInfo.getYearBuildArea());
//        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
//        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
//        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
//        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNull("area_type");
//        ZgsReportEnergyExistinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            totalInfo.setId(energyInfo.getId());
//            totalInfo.setModifypersonaccount(sysUser.getUsername());
//            totalInfo.setModifypersonname(sysUser.getRealname());
//            totalInfo.setModifytime(new Date());
//            this.baseMapper.updateById(totalInfo);
//        } else {
//            //新增
//            totalInfo.setId(UUID.randomUUID().toString());
//            totalInfo.setCreatepersonaccount(sysUser.getUsername());
//            totalInfo.setCreatepersonname(sysUser.getRealname());
//            totalInfo.setCreatetime(new Date());
//            totalInfo.setAreacode(sysUser.getAreacode());
//            totalInfo.setAreaname(sysUser.getAreaname());
//            totalInfo.setFilltm(yearMonth);
//            totalInfo.setQuarter(new BigDecimal(quarter));
//            totalInfo.setYear(new BigDecimal(year));
//            this.baseMapper.insert(totalInfo);
//        }
//    }

//    @Override
//    public void initEnergyExistinfoTotalCity(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
//        totalInfo.setApplydate(new Date());
//        totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        totalInfo.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyExistinfoTotal totalYearInfo = this.baseMapper.totalMonthExistinfoDataYearCity(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyExistinfoTotal totalMonthInfo = this.baseMapper.totalMonthExistinfoDataCity(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
//        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
//        totalInfo.setMonthHouseNumber(totalMonthInfo.getMonthHouseNumber());
//        totalInfo.setMonthHouseArea(totalMonthInfo.getMonthHouseArea());
//        totalInfo.setMonthPublicNumber(totalMonthInfo.getMonthPublicNumber());
//        totalInfo.setMonthPublicArea(totalMonthInfo.getMonthPublicArea());
//        //
//        totalInfo.setYearBuildNumber(totalYearInfo.getYearBuildNumber());
//        totalInfo.setYearBuildArea(totalYearInfo.getYearBuildArea());
//        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
//        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
//        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
//        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNotNull("area_type");
//        ZgsReportEnergyExistinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            totalInfo.setId(energyInfo.getId());
//            totalInfo.setModifypersonaccount(sysUser.getUsername());
//            totalInfo.setModifypersonname(sysUser.getRealname());
//            totalInfo.setModifytime(new Date());
//            this.baseMapper.updateById(totalInfo);
//        } else {
//            //新增
//            totalInfo.setId(UUID.randomUUID().toString());
//            totalInfo.setCreatepersonaccount(sysUser.getUsername());
//            totalInfo.setCreatepersonname(sysUser.getRealname());
//            totalInfo.setCreatetime(new Date());
//            totalInfo.setAreacode(sysUser.getAreacode());
//            totalInfo.setAreaname(sysUser.getAreaname());
//            totalInfo.setFilltm(yearMonth);
//            totalInfo.setQuarter(new BigDecimal(quarter));
//            totalInfo.setYear(new BigDecimal(year));
//            totalInfo.setAreaType("1");
//            this.baseMapper.insert(totalInfo);
//        }
//    }

    @Override
    public void initEnergyExistinfoTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState) {
        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
        if (type == 0) {
            //人工手动上报
            totalInfo.setApplydate(new Date());
            totalInfo.setBackreason("");
        }
        if (applyState != null) {
            totalInfo.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyExistinfoTotal totalYearInfo = this.baseMapper.totalMonthExistinfoDataYearCity(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyExistinfoTotal totalMonthInfo = this.baseMapper.totalMonthExistinfoDataCity(yearMonth, areacode);
        //以上方案取消，市区计算县区合计值，只计算通过的数据合计值，待审核和已退回不计入合计值内
//        ZgsReportEnergyExistinfoTotal cityTotal = lastExistTotalDataCity(yearMonth, areacode, 2);
        //3、本月记录+当年最新一条上报记录
        totalInfo.setMonthNumber(totalMonthInfo.getMonthNumber());
        totalInfo.setMonthArea(totalMonthInfo.getMonthArea());
        totalInfo.setMonthHouseNumber(totalMonthInfo.getMonthHouseNumber());
        totalInfo.setMonthHouseArea(totalMonthInfo.getMonthHouseArea());
        totalInfo.setMonthPublicNumber(totalMonthInfo.getMonthPublicNumber());
        totalInfo.setMonthPublicArea(totalMonthInfo.getMonthPublicArea());
        //
        totalInfo.setYearBuildNumber(totalYearInfo.getYearBuildNumber());
        totalInfo.setYearBuildArea(totalYearInfo.getYearBuildArea());
        totalInfo.setYearBuildhouseNumber(totalYearInfo.getYearBuildhouseNumber());
        totalInfo.setYearBuildhouseArea(totalYearInfo.getYearBuildhouseArea());
        totalInfo.setYearBuildpublicNumber(totalYearInfo.getYearBuildpublicNumber());
        totalInfo.setYearBuildpublicArea(totalYearInfo.getYearBuildpublicArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyExistinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNotNull("area_type");
        ZgsReportEnergyExistinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            totalInfo.setId(energyInfo.getId());
            if (sysUser != null) {
                totalInfo.setModifypersonaccount(sysUser.getUsername());
                totalInfo.setModifypersonname(sysUser.getRealname());
            }
            totalInfo.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                totalInfo.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    totalInfo.setCreatepersonaccount(sysUser.getUsername());
                    totalInfo.setCreatepersonname(sysUser.getRealname());
                }
                totalInfo.setCreatetime(new Date());
                totalInfo.setAreacode(areacode);
                totalInfo.setAreaname(areaname);
                totalInfo.setFilltm(yearMonth);
                totalInfo.setQuarter(new BigDecimal(quarter));
                totalInfo.setYear(new BigDecimal(year));
                totalInfo.setAreaType("1");
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(totalInfo, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }

    @Override
    public void spProject(String id, Integer spStatus, String backreason) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ZgsReportEnergyExistinfoTotal totalInfo = new ZgsReportEnergyExistinfoTotal();
        totalInfo.setId(id);
        totalInfo.setMonthNumber(null);
        totalInfo.setMonthArea(null);
        totalInfo.setMonthHouseNumber(null);
        totalInfo.setMonthHouseArea(null);
        totalInfo.setMonthPublicNumber(null);
        totalInfo.setMonthPublicArea(null);
        totalInfo.setYearBuildNumber(null);
        totalInfo.setYearBuildArea(null);
        totalInfo.setYearBuildhouseNumber(null);
        totalInfo.setYearBuildhouseArea(null);
        totalInfo.setYearBuildpublicNumber(null);
        totalInfo.setYearBuildpublicArea(null);
        //修改项目清单的状态
        ZgsReportEnergyExistinfoTotal newinfoTotal = this.baseMapper.selectById(id);
        ZgsReportEnergyExistinfo energyNewinfo = new ZgsReportEnergyExistinfo();
        energyNewinfo.setBuildingArea(null);
        if (spStatus == 0) {
            //驳回
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            energyNewinfo.setBackreason(backreason);
            totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            totalInfo.setBackreason(backreason);
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                //省厅账号退回后，市州汇总表子项状态改为待审批
                ZgsReportEnergyExistinfoTotal info = new ZgsReportEnergyExistinfoTotal();
                BeanUtils.copyProperties(totalInfo, info);
                info.setId(null);
                info.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                info.setBackreason("");
                QueryWrapper<ZgsReportEnergyExistinfoTotal> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.likeRight("areacode", newinfoTotal.getAreacode());
                wrapper.isNull("area_type");
                this.baseMapper.update(info, wrapper);
                //省退市，删掉该条汇总数据-不留痕(先注释为后面新增撤回按钮做准备)
//                this.baseMapper.deleteById(id);
            }
        } else {
            //通过
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            energyNewinfo.setBackreason("");
            totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            totalInfo.setBackreason("");
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市州账号审批
            QueryWrapper<ZgsReportEnergyExistinfo> wrapper = new QueryWrapper();
            wrapper.eq("filltm", newinfoTotal.getFilltm());
            wrapper.eq("areacode", newinfoTotal.getAreacode());
            wrapper.isNotNull("end_date");
            zgsReportEnergyExistinfoService.update(energyNewinfo, wrapper);
            //以下注释，暂不使用
            //市退县，删掉该条汇总数据-不留痕
//            if (spStatus == 0) {
//                this.baseMapper.deleteById(id);
//            }
            //每次市州审批完更新合计值
//            String filltm = newinfoTotal.getFilltm();
//            String year = filltm.substring(0, 4);
//            String month = filltm.substring(5, 7);
//            int intMonth = Integer.parseInt(month);
//            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//            initEnergyExistinfoTotalCityAll(year, quarter, filltm, newinfoTotal.getAreacode(), newinfoTotal.getAreaname(), sysUser, 1);
        }
        this.baseMapper.updateById(totalInfo);
    }

    @Override
    public ZgsReportEnergyExistinfoTotal lastExistTotalDataProvince(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastExistTotalDataProvince(filltm, areacode, applystate);
    }

    @Override
    public ZgsReportEnergyExistinfoTotal lastExistTotalDataCity(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastExistTotalDataCity(filltm, areacode, applystate);
    }
}
