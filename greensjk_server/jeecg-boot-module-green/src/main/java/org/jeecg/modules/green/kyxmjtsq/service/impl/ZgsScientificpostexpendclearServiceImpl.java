package org.jeecg.modules.green.kyxmjtsq.service.impl;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostexpendclearMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostexpendclearService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题经费决算表
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostexpendclearServiceImpl extends ServiceImpl<ZgsScientificpostexpendclearMapper, ZgsScientificpostexpendclear> implements IZgsScientificpostexpendclearService {

}
