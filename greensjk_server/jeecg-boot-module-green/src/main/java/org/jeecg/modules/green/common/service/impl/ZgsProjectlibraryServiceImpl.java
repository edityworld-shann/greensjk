package org.jeecg.modules.green.common.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itextpdf.text.pdf.parser.XObjectDoHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.mapper.ZgsProjectlibraryMapper;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Slf4j
@Service
public class ZgsProjectlibraryServiceImpl extends ServiceImpl<ZgsProjectlibraryMapper, ZgsProjectlibrary> implements IZgsProjectlibraryService {

    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;
    @Autowired
    private IZgsTaskbuildfundbudgetService zgsTaskbuildfundbudgetService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    //
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsExamplecertificatebaseService zgsExamplecertificatebaseService;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;
    @Autowired
    private IZgsPerformancebaseService zgsPerformancebaseService;
    @Autowired
    private IZgsSpostcertificatebaseService zgsSpostcertificatebaseService;
    @Autowired
    private IZgsSacceptcertificatebaseService zgsSacceptcertificatebaseService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsCommonService zgsCommonService;
    @Autowired
    private IZgsBuildjointunitService zgsBuildjointunitService;
    @Autowired
    private IZgsBuildfundbudgetService zgsBuildfundbudgetService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsPlannedprojectchangedetailService iZgsPlannedprojectchangedetailService;

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo1(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo1(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        /*if (listInfo.size() > 0) {
            for (ZgsProjectlibraryListInfo entity:listInfo
                 ) {
                String bid = entity.getBid();
                if (StringUtils.isNotBlank(bid)) {
                    // 获取数据所处最新阶段的状态进行更新
                    initStageAndStatus(bid);
                    entity.setProjectstage(redisUtil.get(bid).toString());
                    entity.setProjectstagestatus(redisUtil.get(GlobalConstants.STAGE_STATUS + bid).toString());
                }
            }
        }*/
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo2(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo2(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo3(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo3(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo4(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo4(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo5(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo5(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfo6(Wrapper<ZgsProjectlibrary> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjectlibraryListInfo6(pageMode, queryWrapper);
        List<ZgsProjectlibraryListInfo> listInfo = listInfoPage.getRecords();
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

    @Override
    public int verificationUnitAndProjectLeader(String projectlibraryId, String unitName, String projectLeader,Date startdate,int addEdit) {
        String projectLeader1 = Sm4Util.encryptEcb(projectLeader);
        //addEdit==1新增、addEdit==2编辑
        //默认为-1
        //0、负责人不符合此次申报条件：每年限申报1项建设科技计划项目
        //1、负责人不符合此次申报条件：有两项及以上建设科技计划项目未验收（结题）
        //2、负责人不符合此次申报条件：两年内有终止执行的项目
        //3、负责人不符合此次申报条件：有计划项目逾期但没提交终止执行申请及相关材料
        int status = -1;
        //先查下今年是否已上报
        //示范
        QueryWrapper<ZgsProjectlibrary> queryWrapperYear1 = new QueryWrapper<>();
        // queryWrapperYear1.eq("l.applyunitprojectleader", projectLeader);
        queryWrapperYear1 .and((wrapper) -> {
            wrapper.eq("l.applyunitprojectleader", projectLeader1).or().eq("l.applyunitprojectleader", projectLeader);
        });
        // queryWrapperYear1.eq("l.applyunitprojectleader", projectLeader1).or().eq("l.applyunitprojectleader", projectLeader);
        queryWrapperYear1.ne("l.id",projectlibraryId);
        //queryWrapperYear1.eq("l.year_num", zgsCommonService.queryApplyYear());
        queryWrapperYear1.notInSql("m.status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
        Page<ZgsProjectlibraryListInfo> pageModeY1 = new Page<>(1, 10);
        IPage<ZgsProjectlibraryListInfo> pageListY1 = this.baseMapper.listVerificationUnitAndProjectLeader(pageModeY1, queryWrapperYear1);
        if (pageListY1 != null && pageListY1.getRecords().size() > 0) {
            for (ZgsProjectlibraryListInfo record : pageListY1.getRecords()) {
                String year=new SimpleDateFormat("yyyy").format(record.getStartdate());
                String startDate=new SimpleDateFormat("yyyy").format(startdate);
                if (year.equals(startDate)){
                    return 0;
                }
            }
        }
        //科技
        QueryWrapper<ZgsSciencetechfeasible> queryWrapperYear2 = new QueryWrapper();
        queryWrapperYear2.eq("projectleader", projectLeader);
        //queryWrapperYear2.eq("year_num", zgsCommonService.queryApplyYear());
        queryWrapperYear2.notInSql("status", GlobalConstants.PROJECT_LIST_WORKTABLE_0_3);
        List<ZgsSciencetechfeasible> listY2 = zgsSciencetechfeasibleService.list(queryWrapperYear2);
        if (listY2.size() > 0) {//有项目
            for (ZgsSciencetechfeasible v : listY2) {
                String year=new SimpleDateFormat("yyyy").format(v.getStartdate());
                String startDate=new SimpleDateFormat("yyyy").format(startdate);
                if (year.equals(startDate)){
                    return 0;
                }
            }
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, -2);
        int count = 0;
        //1-1、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("l.applyunitprojectleader", projectLeader);
        queryWrapper1.isNotNull("m.status");
//        queryWrapper1.ge("m.applydate", calendar.getTime());
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(1, 10);
        IPage<ZgsProjectlibraryListInfo> pageList = this.baseMapper.listVerificationUnitAndProjectLeader(pageMode, queryWrapper1);
        if (pageList != null && pageList.getRecords().size() > 0) {
            for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : pageList.getRecords()) {
                if (GlobalConstants.PROJECT_LIST1439_12.contains(zgsProjectlibraryListInfo.getStatus())) {
                    if (addEdit == 1) {
                        ++count;
                    }
                } else {
//                    if (zgsProjectlibraryListInfo.getIsdealoverdue() != null && (zgsProjectlibraryListInfo.getIsdealoverdue().compareTo(new BigDecimal(2)) == 0)) {
//                        return 2;
//                    }
                    //3、验收
                    QueryWrapper<ZgsDemoprojectacceptance> queryWrapper13 = new QueryWrapper();
                    queryWrapper13.eq("projectlibraryguid", zgsProjectlibraryListInfo.getId());
                    queryWrapper13.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper13);
                    count += zgsDemoprojectacceptanceList.size();
                    //4、验收证书
                    QueryWrapper<ZgsExamplecertificatebase> queryWrapper14 = new QueryWrapper();
                    queryWrapper14.eq("projectlibraryguid", zgsProjectlibraryListInfo.getId());
                    queryWrapper14.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper14);
                    count += zgsExamplecertificatebaseList.size();
                    if (count >= 2) {
                        return 1;
                    }
                    //1、任务书
                    QueryWrapper<ZgsProjecttask> queryWrapper11 = new QueryWrapper();
                    queryWrapper11.eq("projectguid", zgsProjectlibraryListInfo.getId());
                    queryWrapper11.and(qwd -> {
                        qwd.eq("dealtype", "0").or().eq("dealtype", "1");
                    });
                    List<ZgsProjecttask> zgsProjecttaskList = zgsProjecttaskService.list(queryWrapper11);
                    if (zgsProjecttaskList.size() > 0) {
                        return 3;
                    }
                }
            }
        }
        //2、科技
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.eq("projectleader", projectLeader);
        queryWrapper2.isNotNull("status");
//        queryWrapper2.ge("applydate", calendar.getTime());
        List<ZgsSciencetechfeasible> list2 = zgsSciencetechfeasibleService.list(queryWrapper2);
        if (list2.size() > 0) {
            for (ZgsSciencetechfeasible zgsSciencetechfeasible : list2) {
                if (GlobalConstants.PROJECT_LIST1439_12.contains(zgsSciencetechfeasible.getStatus())) {
                    if (addEdit == 1) {
                        ++count;
                    }
                } else {
//                    if (zgsSciencetechfeasible.getIsdealoverdue() != null && (zgsSciencetechfeasible.getIsdealoverdue().compareTo(new BigDecimal(2)) == 0)) {
//                        return 2;
//                    }
                    //2、验收
                    QueryWrapper<ZgsScientificbase> queryWrapper22 = new QueryWrapper();
                    queryWrapper22.eq("projectlibraryguid", zgsSciencetechfeasible.getId());
                    queryWrapper22.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper22);
                    count += zgsScientificbaseList.size();
                    //3、结题
                    QueryWrapper<ZgsScientificpostbase> queryWrapper23 = new QueryWrapper();
                    queryWrapper23.eq("scientificbaseguid", zgsSciencetechfeasible.getId());
                    queryWrapper23.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper23);
                    count += zgsScientificpostbaseList.size();
                    //6、验收证书
                    QueryWrapper<ZgsSacceptcertificatebase> queryWrapper26 = new QueryWrapper();
                    queryWrapper26.eq("projectlibraryguid", zgsSciencetechfeasible.getId());
                    queryWrapper26.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsSacceptcertificatebase> zgsSacceptcertificatebaseList = zgsSacceptcertificatebaseService.list(queryWrapper26);
                    count += zgsSacceptcertificatebaseList.size();
                    //7、结题证书
                    QueryWrapper<ZgsSpostcertificatebase> queryWrapper27 = new QueryWrapper();
                    queryWrapper27.eq("projectlibraryguid", zgsSciencetechfeasible.getId());
                    queryWrapper27.inSql("status", GlobalConstants.PROJECT_LIST1439_12);
                    List<ZgsSpostcertificatebase> zgsSpostcertificatebaseList = zgsSpostcertificatebaseService.list(queryWrapper27);
                    count += zgsSpostcertificatebaseList.size();
                    if (count >= 2) {
                        return 1;
                    }
                    //1、任务书
                    QueryWrapper<ZgsSciencetechtask> queryWrapper21 = new QueryWrapper();
                    queryWrapper21.eq("sciencetechguid", zgsSciencetechfeasible.getId());
                    queryWrapper21.and(qwd -> {
                        qwd.eq("dealtype", "0").or().eq("dealtype", "1");
                    });
                    List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper21);
                    if (zgsSciencetechtaskList.size() > 0) {
                        return 3;
                    }
                }
            }
        }
        //1-0、示范-两年内终止执行的项目负责人
        QueryWrapper<ZgsProjectlibrary> queryWrapper1_0 = new QueryWrapper<>();
        queryWrapper1_0.eq("l.applyunitprojectleader", projectLeader);
        queryWrapper1_0.ge("m.applydate", calendar.getTime());
        Page<ZgsProjectlibraryListInfo> pageMode_0 = new Page<>(1, 10);
        IPage<ZgsProjectlibraryListInfo> pageList_0 = this.baseMapper.listVerificationUnitAndProjectLeader(pageMode_0, queryWrapper1_0);
        if (pageList_0 != null && pageList_0.getRecords().size() > 0) {
            for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : pageList_0.getRecords()) {
                if (zgsProjectlibraryListInfo.getIsdealoverdue() != null && (zgsProjectlibraryListInfo.getIsdealoverdue().compareTo(new BigDecimal(2)) == 0)) {
                    return 2;
                }
            }
        }
        //2-0、科技-两年内终止执行的项目负责人
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper2_0 = new QueryWrapper<>();
        queryWrapper2_0.eq("projectleader", projectLeader);
        queryWrapper2_0.ge("applydate", calendar.getTime());
        List<ZgsSciencetechfeasible> list2_0 = zgsSciencetechfeasibleService.list(queryWrapper2_0);
        if (list2_0.size() > 0) {
            for (ZgsSciencetechfeasible zgsSciencetechfeasible : list2_0) {
                if (zgsSciencetechfeasible.getIsdealoverdue() != null && (zgsSciencetechfeasible.getIsdealoverdue().compareTo(new BigDecimal(2)) == 0)) {
                    return 2;
                }
            }
        }
        //
        return status;
    }

    @Override
    public List<ZgsProjectParentInfo> listZgsProjectParentInfo(Wrapper<ZgsProjectParentInfo> queryWrapper, Integer accessType) {
        List<ZgsProjectParentInfo> list = this.baseMapper.listZgsProjectParentInfo(queryWrapper);
        if (accessType != null) {
            if (accessType == 2 && list != null && list.size() > 0) {
                //项目变更过虑
                String projectlibraryguid = null;
                String tasktype = null;
                ZgsProjectParentInfo zgsProjectParentInfo = null;
                List<ZgsProjectParentInfo> listTemp = new ArrayList<>();
                QueryWrapper<ZgsPlannedprojectchange> bgw = null;
                List<ZgsPlannedprojectchange> bgwList = null;
                QueryWrapper<ZgsProjecttask> task1 = null;
                List<ZgsProjecttask> list1 = null;
                QueryWrapper<ZgsSciencetechtask> task2 = null;
                List<ZgsSciencetechtask> list2 = null;
                QueryWrapper<ZgsMidterminspection> p1 = null;
                QueryWrapper<ZgsDemoprojectacceptance> p1_1 = null;
                List<ZgsMidterminspection> list3 = null;
                List<ZgsDemoprojectacceptance> list3_1 = null;
                QueryWrapper<ZgsScientificbase> p2 = null;
                List<ZgsScientificbase> list4 = null;
                for (int i = 0; i < list.size(); i++) {
                    zgsProjectParentInfo = list.get(i);
                    tasktype = zgsProjectParentInfo.getTasktype();
                    projectlibraryguid = zgsProjectParentInfo.getId();
                    if (StringUtils.isNotEmpty(projectlibraryguid)) {
                        //过虑已经有变更通过的项目
                        bgw = new QueryWrapper<>();
                        bgw.ne("isdelete", 1);
                        bgw.ne("status", GlobalConstants.SHENHE_STATUS0);
                        bgw.eq("projectlibraryguid", projectlibraryguid);
                        bgwList = zgsPlannedprojectchangeService.list(bgw);
                        if ("1".equals(tasktype)) {
                            task1 = new QueryWrapper<>();
                            task1.ne("isdelete", 1);
                            task1.eq("status", GlobalConstants.SHENHE_STATUS8);
                            task1.eq("projectguid", projectlibraryguid);
                            p1 = new QueryWrapper<>();
                            p1.ne("isdelete", 1);
                            p1.in("status", GlobalConstants.PROJECT_LIST1439);
                            p1.eq("projectlibraryguid", projectlibraryguid);
                            p1_1 = new QueryWrapper<>();
                            p1_1.ne("isdelete", 1);
                            p1_1.ne("status", GlobalConstants.SHENHE_STATUS0);
                            p1_1.eq("projectlibraryguid", projectlibraryguid);
                            //示范任务书是否具备变更条件，任务书通过且中期查验申报终审或未上报状态，项目验收申报未开始
                            list1 = zgsProjecttaskService.list(task1);
                            list3 = zgsMidterminspectionService.list(p1);
                            list3_1 = zgsDemoprojectacceptanceService.list(p1_1);
                            if (list1.size() > 0 && list3.size() == 0 && bgwList.size() == 0 && list3_1.size() == 0) {
                                listTemp.add(zgsProjectParentInfo);
                            }
                        } else {
                            //科技任务书是否具备变更条件，任务书通过且未开始项目验收申报
                            task2 = new QueryWrapper<>();
                            task2.ne("isdelete", 1);
                            task2.eq("status", GlobalConstants.SHENHE_STATUS8);
                            task2.eq("sciencetechguid", projectlibraryguid);
                            p2 = new QueryWrapper<>();
                            p2.ne("isdelete", 1);
                            p2.eq("projectlibraryguid", projectlibraryguid);
                            list2 = zgsSciencetechtaskService.list(task2);
                            list4 = zgsScientificbaseService.list(p2);
                            if (list2.size() > 0 && list4.size() == 0 && bgwList.size() == 0) {
                                listTemp.add(zgsProjectParentInfo);
                            }
                        }
                    }
                }
                return ValidateEncryptEntityUtil.validateDecryptList(listTemp, ValidateEncryptEntityUtil.isDecrypt);
            }
            return ValidateEncryptEntityUtil.validateDecryptList(list, ValidateEncryptEntityUtil.isDecrypt);
        }
        //注释：最早修改是，新增绩效自评时候，过滤掉省拨款资金为0的项目，06-14开始，不进行过滤，所有项目都可以新增绩效自评
        //数据新增是否位支持省级科技资金状态
        //以下代码不再执行
        List<ZgsProjectParentInfo> listAll = new ArrayList<>();
        if (list != null && list.size() > 0) {
            ZgsProjectParentInfo zgsProjectParentInfo = null;
            for (int i = 0; i < list.size(); i++) {
                zgsProjectParentInfo = list.get(i);
                String projecttype = zgsProjectParentInfo.getProjecttype();
                if (StringUtils.isNotEmpty(projecttype)) {
                    //示范
                    ZgsProjecttask zgsProjecttask = (ZgsProjecttask) zgsProjecttaskService.queryByProjectGuid(zgsProjectParentInfo.getId(), null);
                    if (zgsProjecttask != null && zgsProjecttask.getZgsTaskbuildfundbudgetList() != null && zgsProjecttask.getZgsTaskbuildfundbudgetList().size() > 0) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsProjecttask.getZgsTaskbuildfundbudgetList().get(0);
                        if (zgsTaskbuildfundbudget != null) {
                            if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                                listAll.add(zgsProjectParentInfo);
                            }
                        }
                    }
                } else {
                    //科技
                    ZgsSciencetechtask zgsSciencetechtask = (ZgsSciencetechtask) zgsProjecttaskService.queryByProjectGuid(zgsProjectParentInfo.getId(), null);
                    if (zgsSciencetechtask != null && zgsSciencetechtask.getZgsTaskbuildfundbudgetList() != null && zgsSciencetechtask.getZgsTaskbuildfundbudgetList().size() > 0) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsSciencetechtask.getZgsTaskbuildfundbudgetList().get(0);
                        if (zgsTaskbuildfundbudget != null) {
                            if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                                listAll.add(zgsProjectParentInfo);
                            }
                        }
                    }
                }
            }
        }
        return ValidateEncryptEntityUtil.validateDecryptList(listAll, ValidateEncryptEntityUtil.isDecrypt);
    }

    @Override
    public List<ZgsProjectParentInfo> listZgsProjectParentInfoByAccessType(Integer accessType) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        List<ZgsProjectParentInfo> listAll = new ArrayList<>();
        QueryWrapper<ZgsProjectParentInfo> queryWrapper = new QueryWrapper<>();
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
            queryWrapper.eq("m.enterpriseguid", sysUser.getEnterpriseguid());
            queryWrapper.and(qwd -> {
                qwd.isNull("m.ishistory").or().eq("m.ishistory", "0");
            });
            //终止的项目直接过滤
            queryWrapper.and(qwd -> {
                qwd.isNull("m.isdealoverdue").or().eq("m.isdealoverdue", GlobalConstants.IS_DEAL_OVERDUE_1);
            });
            //accessType(1示范项目任务书、2中期查验、3变更、4示范项目验收、5验收证书、6年度绩效、7结题绩效、8项目成果)
            //accessType(9科研项目任务书、10科研项目验收、11结题项目验收、12验收证书、13结题证书)
            switch (accessType) {
                case 1:
                    //1示范项目任务书（形审通过且立项，再排除已创建任务书的项目）
                    queryWrapper.isNotNull("m.provincialfundtotal");
                    List<ZgsProjectParentInfo> list1 = this.baseMapper.listZgsProjectParentInfoJoin1(queryWrapper);
                    ZgsProjectParentInfo info1 = null;
                    for (int i = 0; i < list1.size(); i++) {
                        info1 = list1.get(i);
                        //查任务书是否已被创建
                        QueryWrapper<ZgsProjecttask> qTask = new QueryWrapper();
                        qTask.eq("projectguid", info1.getId());
                        if (zgsProjecttaskService.getOne(qTask) == null) {
                            listAll.add(info1);
                        }
                    }
                    break;
                case 2:
                    //2示范中期查验（任务书形审通过）
                    Map<String,Object> params = new HashMap<>();
                    List<ZgsProjectParentInfo> list2 = this.baseMapper.listZgsProjectParentInfoJoin2(queryWrapper);
                    ZgsProjectParentInfo info2 = null;
                    for (int i = 0; i < list2.size(); i++) {
                        info2 = list2.get(i);
                        params.put(info2.getId(),i);
                        //查中期查验是否已被创建
                        QueryWrapper<ZgsMidterminspection> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info2.getId());
                        if (zgsMidterminspectionService.getOne(qTask) == null && getBgIsFlag(info2.getId())) {
                            listAll.add(info2);
                        }
                    }
                    // 如果中期查验  推荐单位：初审驳回；  省厅：形审驳回  且二次变更省厅已形审通过，则可在当前阶段从新进行添加项目；
                    QueryWrapper<ZgsMidterminspection> midterminspectionQueryWrapper = new QueryWrapper<>();
                    midterminspectionQueryWrapper.eq("enterpriseguid",sysUser.getEnterpriseguid());
                    midterminspectionQueryWrapper.ne("isdelete",1);
                    midterminspectionQueryWrapper.orderByDesc("firstdate");
                    List<ZgsMidterminspection>  zgsMidterminspections = zgsMidterminspectionService.list(midterminspectionQueryWrapper);
                    if (zgsMidterminspections.size() > 0) {
                        for (ZgsMidterminspection entity:zgsMidterminspections
                             ) {
                            String states = entity.getStatus();  // 初审、形审驳回状态
                            String pjlbgd = entity.getProjectlibraryguid();
                            // 查询当前项目是否存在二次变更
                            if (StrUtil.isNotBlank(pjlbgd)) {
                                QueryWrapper<ZgsPlannedprojectchange> plannedprojectchangeQueryWrapper = new QueryWrapper<>();
                                plannedprojectchangeQueryWrapper.eq("projectlibraryguid",pjlbgd);
                                plannedprojectchangeQueryWrapper.eq("enterpriseguid",sysUser.getEnterpriseguid());
                                plannedprojectchangeQueryWrapper.eq("intype","2");
                                plannedprojectchangeQueryWrapper.ne("isdelete",1);
                                plannedprojectchangeQueryWrapper.orderByDesc("firstdate");
                                ZgsPlannedprojectchange zgsPlannedprojectchange = zgsPlannedprojectchangeService.getOne(plannedprojectchangeQueryWrapper);
                                if (ObjectUtil.isNotNull(zgsPlannedprojectchange)) {
                                    String secondStatus = zgsPlannedprojectchange.getStatus();  // 二次变更状态
                                    String id = zgsPlannedprojectchange.getId();
                                    // 当状态为 初审驳回或形审驳回  且二次变更通过后进行项目添加
                                    if ((states.equals(GlobalConstants.SHENHE_STATUS13) || states.equals(GlobalConstants.SHENHE_STATUS14)) && secondStatus.equals(GlobalConstants.SHENHE_STATUS8)) {

                                        listAll.add(list2.get(Integer.parseInt(params.get(pjlbgd).toString())));

                                        /*// 获取二次变更后的最新数据进行更新
                                        QueryWrapper<ZgsPlannedprojectchangedetail> plannedprojectchangedetailQueryWrapper = new QueryWrapper<>();
                                        plannedprojectchangedetailQueryWrapper.eq("baseguid",id);
                                        plannedprojectchangedetailQueryWrapper.ne("isdelete",1);
                                        plannedprojectchangedetailQueryWrapper.orderByDesc("ordernum");
                                        // 获取所有变更内容
                                        List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList = iZgsPlannedprojectchangedetailService.list(plannedprojectchangedetailQueryWrapper);
                                        if (plannedprojectchangedetailList.size() > 0) {
                                            ZgsSciencetechtask zgsSciencetechtask = new ZgsSciencetechtask();
                                            String baseguid = plannedprojectchangedetailList.get(0).getBaseguid();
                                            for (ZgsPlannedprojectchangedetail plannedprojectchangedetail:plannedprojectchangedetailList
                                                 ) {

                                                // 因变更时 项目组成员通过单独接口进行保存，故此处不再做保存操作；
                                                // 获取变更字段及变更内容进行数据更新

                                                // 项目负责人：projectleader
                                                // 项目名称：projectname
                                                // 终止时间：projectenddata
                                                // 手机号：headmobile
                                                // 所在单位：headunit
                                                // 单位通讯地址：unitaddress
                                                String changeColumn = plannedprojectchangedetail.getChangeColumn();
                                                String changContent = plannedprojectchangedetail.getChangecontent();
                                                // 根据变更字段。更新所在不同表的字段信息
                                                switch (changeColumn) {
                                                    case "projectleader":
                                                        zgsSciencetechtask.setProjectleader(changContent);
                                                        break;
                                                    case "projectname":
                                                        zgsSciencetechtask.setProjectname(changContent);
                                                        break;
                                                    case "projectenddata":
                                                        try {
                                                            zgsSciencetechtask.setEnddate(new SimpleDateFormat("yyyy-MM-dd").parse(changContent));
                                                        } catch (ParseException e) {
                                                            e.printStackTrace();
                                                        }
                                                        break;
                                                    case "headmobile":
                                                        zgsSciencetechtask.setHeadmobile(changContent);
                                                        break;
                                                    case "unitaddress":
                                                        zgsSciencetechtask.setUnitaddress(changContent);
                                                        break;
                                                    default:
                                                        break;
                                                }
                                            }
                                            // 获取任务书阶段要变更的项目信息
                                            Map<String,Object> map = baseMapper.getProjectInfo(baseguid);
                                            if (ObjectUtil.isNotNull(map) && StrUtil.isNotBlank(map.get("taskId").toString())) {
                                                zgsSciencetechtask.setId(map.get("taskId").toString());
                                                // 对变更内容进行修改操作
                                                zgsSciencetechtaskService.updateById(zgsSciencetechtask);
                                            }
                                        }*/
                                    }
                                }
                            }
                        }
                    }
                    break;
                case 3:
                    //3示范变更-共用（需过滤掉的：示范中期查验进行中的比如：待审核、初审通过；验收阶段及以下阶段均不可变更）
                    List<ZgsProjectParentInfo> list3_1 = this.baseMapper.listZgsProjectParentInfoJoin3_1(queryWrapper);
                    ZgsProjectParentInfo info3_1 = null;
                    for (int i = 0; i < list3_1.size(); i++) {
                        info3_1 = list3_1.get(i);
                        //查示范变更是否已被创建
                        QueryWrapper<ZgsPlannedprojectchange> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info3_1.getId());
                        int bg_count = 0;
                        if (info3_1.getBgcount() != null) {
                            bg_count = info3_1.getBgcount();
                        }
                        int size_bg = zgsPlannedprojectchangeService.list(qTask).size();
                        if (size_bg == 0 || size_bg < bg_count) {
                            //变更次数为空可变更
                            //示范中期查验进行中的比如：待审核、初审通过过滤掉
                            QueryWrapper<ZgsMidterminspection> wqd1 = new QueryWrapper();
                            wqd1.eq("projectlibraryguid", info3_1.getId());
                            wqd1.ne("status", GlobalConstants.SHENHE_STATUS0);
                            wqd1.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);
                            boolean sfbg = false;
                            if (zgsMidterminspectionService.list(wqd1).size() == 0) {
                                sfbg = true;
                            }
                            if (sfbg) {
                                //验收阶段过滤掉
                                QueryWrapper<ZgsDemoprojectacceptance> wqd2 = new QueryWrapper();
                                wqd2.eq("projectlibraryguid", info3_1.getId());
                                wqd2.ne("status", GlobalConstants.SHENHE_STATUS0);
                                // wqd2.inSql("status", GlobalConstants.PROJECT_LIST_SB_W_1_4);  // 验收阶段如果存在正在进行审批或待审批的项目则不能进行项目变更  rxl 20230811
                                ZgsDemoprojectacceptance zgsDemoprojectacceptance = zgsDemoprojectacceptanceService.getOne(wqd2);
                                if (zgsDemoprojectacceptance == null) {
                                    //如果验收申请数据为空可显示
                                    sfbg = true;
                                } else {
                                    if (size_bg == 0 || size_bg < bg_count) {
                                        //如果验收申请数据新增了未上报可显示
                                        sfbg = true;
                                    } else {
                                        sfbg = false;
                                    }
                                }
                            }
                            if (sfbg) {
                                //验收证书阶段过滤掉
                                QueryWrapper<ZgsExamplecertificatebase> wqd3 = new QueryWrapper();
                                wqd3.eq("projectlibraryguid", info3_1.getId());
                                wqd3.ne("status", GlobalConstants.SHENHE_STATUS0);
                                ZgsExamplecertificatebase zgsExamplecertificatebase = zgsExamplecertificatebaseService.getOne(wqd3);
                                if (zgsExamplecertificatebase == null) {
                                    //如果验收证书数据为空可显示
                                    sfbg = true;
                                } else {
                                    if (size_bg == 0 || size_bg < bg_count) {
                                        //如果验收证书数据新增了未上报可显示
                                        sfbg = true;
                                    } else {
                                        sfbg = false;
                                    }
                                }
                            }
                            if (sfbg) {
                                listAll.add(info3_1);
                            }
                        }
                    }
                    //科研变更
                    List<ZgsProjectParentInfo> list3_2 = this.baseMapper.listZgsProjectParentInfoJoin3_2(queryWrapper);
                    ZgsProjectParentInfo info3_2 = null;
                    for (int i = 0; i < list3_2.size(); i++) {
                        info3_2 = list3_2.get(i);
                        //查科研变更是否已被创建
                        QueryWrapper<ZgsPlannedprojectchange> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info3_2.getId());
                        int bg_count = 0;
                        if (info3_2.getBgcount() != null) {
                            bg_count = info3_2.getBgcount();
                        }
                        int size_bg = zgsPlannedprojectchangeService.list(qTask).size();
                        if (size_bg == 0 || size_bg < bg_count) {
                            //变更次数为空可变更
                            boolean sfbg = false;
                            //科研验收阶段过滤掉
                            QueryWrapper<ZgsScientificbase> wqd1 = new QueryWrapper();
                            wqd1.eq("projectlibraryguid", info3_2.getId());
                            wqd1.ne("status", GlobalConstants.SHENHE_STATUS0);
                            ZgsScientificbase zgsScientificbase = zgsScientificbaseService.getOne(wqd1);
                            if (zgsScientificbase == null) {
                                //如果验收申请数据为空可显示
                                sfbg = true;
                            } else {
//                                if (GlobalConstants.SHENHE_STATUS0.equals(zgsScientificbase.getStatus())) {
                                if (size_bg == 0 || size_bg < bg_count) {
                                    //如果验收申请数据新增了未上报可显示
                                    sfbg = true;
                                } else {
                                    sfbg = false;
                                }
                            }
                            if (sfbg) {
                                //科研验收证书阶段过滤掉
                                QueryWrapper<ZgsSacceptcertificatebase> wqd2 = new QueryWrapper();
                                wqd2.eq("projectlibraryguid", info3_2.getId());
                                wqd2.ne("status", GlobalConstants.SHENHE_STATUS0);
                                ZgsSacceptcertificatebase zgsSacceptcertificatebase = zgsSacceptcertificatebaseService.getOne(wqd2);
                                if (zgsSacceptcertificatebase == null) {
                                    //如果验收证书数据为空可显示
                                    sfbg = true;
                                } else {
//                                    if (GlobalConstants.SHENHE_STATUS0.equals(zgsSacceptcertificatebase.getStatus())) {
                                    if (size_bg == 0 || size_bg < bg_count) {
                                        //如果验收证书数据新增了未上报可显示
                                        sfbg = true;
                                    } else {
                                        sfbg = false;
                                    }
                                }
                            }
                            if (sfbg) {
                                //结题验收阶段过滤掉
                                QueryWrapper<ZgsScientificpostbase> wqd3 = new QueryWrapper();
                                wqd3.eq("scientificbaseguid", info3_2.getId());
                                wqd3.ne("status", GlobalConstants.SHENHE_STATUS0);
                                ZgsScientificpostbase zgsScientificpostbase = zgsScientificpostbaseService.getOne(wqd3);
                                if (zgsScientificpostbase == null) {
                                    //如果验收申请数据为空可显示
                                    sfbg = true;
                                } else {
//                                    if (GlobalConstants.SHENHE_STATUS0.equals(zgsScientificpostbase.getStatus())) {
                                    if (size_bg == 0 || size_bg < bg_count) {
                                        //如果验收申请数据新增了未上报可显示
                                        sfbg = true;
                                    } else {
                                        sfbg = false;
                                    }
                                }
                            }
                            if (sfbg) {
                                //结题验收证书阶段过滤掉
                                QueryWrapper<ZgsSpostcertificatebase> wqd4 = new QueryWrapper();
                                wqd4.eq("projectlibraryguid", info3_2.getId());
                                wqd4.ne("status", GlobalConstants.SHENHE_STATUS0);
                                ZgsSpostcertificatebase zgsSacceptcertificatebase = zgsSpostcertificatebaseService.getOne(wqd4);
                                if (zgsSacceptcertificatebase == null) {
                                    //如果验收证书数据为空可显示
                                    sfbg = true;
                                } else {
//                                    if (GlobalConstants.SHENHE_STATUS0.equals(zgsSacceptcertificatebase.getStatus())) {
                                    if (size_bg == 0 || size_bg < bg_count) {
                                        //如果验收证书数据新增了未上报可显示
                                        sfbg = true;
                                    } else {
                                        sfbg = false;
                                    }
                                }
                            }
                            if (sfbg) {
                                listAll.add(info3_2);
                            }
                        }
                    }
                    break;
                case 4:
                    //4示范项目验收（中期查验形审通过）

                    List<ZgsProjectParentInfo> list4 = this.baseMapper.listZgsProjectParentInfoJoin4(queryWrapper);
                    ZgsProjectParentInfo info4 = null;
                    for (int i = 0; i < list4.size(); i++) {
                        info4 = list4.get(i);
                        //查项目验收是否已被创建
                        QueryWrapper<ZgsDemoprojectacceptance> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info4.getId());
                        ZgsDemoprojectacceptance zgsDemoprojectacceptance = zgsDemoprojectacceptanceService.getOne(qTask);
                        boolean bol = getBgIsFlag(info4.getId());
                        if (zgsDemoprojectacceptanceService.getOne(qTask) == null && getBgIsFlag(info4.getId())) {
                            listAll.add(info4);
                        }
                    }
                    break;
                case 5:
                    //5示范验收证书（示范项目验收形审通过）
                    List<ZgsProjectParentInfo> list5 = this.baseMapper.listZgsProjectParentInfoJoin5(queryWrapper);
                    ZgsProjectParentInfo info5 = null;
                    for (int i = 0; i < list5.size(); i++) {
                        info5 = list5.get(i);
                        //查示范项目验收是否已被创建
                        QueryWrapper<ZgsExamplecertificatebase> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info5.getId());
                        if (zgsExamplecertificatebaseService.getOne(qTask) == null) {
                            listAll.add(info5);
                        }
                    }
                    break;
                case 6:
                    List<ZgsProjectParentInfo> infoList = new ArrayList<>();
                    //6示范科研年度绩效-共用（无限制）
                    List<ZgsProjectParentInfo> list6_1 = this.baseMapper.listZgsProjectParentInfoJoin3_1(queryWrapper);
                    List<ZgsProjectParentInfo> list6_2 = this.baseMapper.listZgsProjectParentInfoJoin3_2(queryWrapper);
                    listAll.addAll(list6_1);
                    listAll.addAll(list6_2);
                    if (listAll.size() > 0) {
                        for (ZgsProjectParentInfo entity:listAll
                             ) {
                            QueryWrapper<ZgsPerformancebase> queryWrapperForPerformancebase = new QueryWrapper<>();
                            queryWrapperForPerformancebase.eq("p.intype","1");
                            queryWrapperForPerformancebase.eq("p.enterpriseguid",sysUser.getEnterpriseguid());
                            queryWrapperForPerformancebase.ne("p.isdelete",1);
                            queryWrapperForPerformancebase.eq("p.baseguid",entity.getGlid());
                            List<ZgsPerformancebase> listZgsPerformancebaseForJxzp = zgsPerformancebaseService.listZgsPerformancebaseForJxzp(queryWrapperForPerformancebase);
                                if (listZgsPerformancebaseForJxzp.size() == 0) {
                                    infoList.add(entity);
                                }
                        }
                        listAll.clear();
                        listAll.addAll(infoList);
                        /*if (listZgsPerformancebaseForJxzp.size() > 0) {
                            for (ZgsProjectParentInfo projectParentInfoEntity:listAll
                                 ) {
                                for (ZgsPerformancebase entity:listZgsPerformancebaseForJxzp
                                    ) {
                                    if (projectParentInfoEntity.getGlid().equals(entity.getBaseguid())) {
                                        listAll.remove(projectParentInfoEntity);
                                    }
                                }
                            }
                        }*/
                    }
                    break;
                case 7:
                    //7示范科研结题绩效-共用（示范项目筛掉、科研验收证书终审通过或科研结题证书通过）
                    List<ZgsProjectParentInfo> list7_1 = this.baseMapper.listZgsProjectParentInfoJoin3_2(queryWrapper);
                    ZgsProjectParentInfo info7 = null;
                    for (int i = 0; i < list7_1.size(); i++) {
                        info7 = list7_1.get(i);
                        //查结题绩效是否已被创建
                        QueryWrapper<ZgsPerformancebase> qTask0 = new QueryWrapper();
                        qTask0.eq("baseguid", info7.getId());
                        qTask0.eq("intype", "0");
                        if (zgsPerformancebaseService.getOne(qTask0) == null) {
                            //查科研验收证书
                            QueryWrapper<ZgsSacceptcertificatebase> qTask1 = new QueryWrapper();
                            qTask1.eq("projectlibraryguid", info7.getId());
                            qTask1.eq("status", GlobalConstants.SHENHE_STATUS2);
                            if (zgsSacceptcertificatebaseService.getOne(qTask1) != null) {
                                listAll.add(info7);
                            } else {
                                //查结题证书
                                QueryWrapper<ZgsSpostcertificatebase> qTask2 = new QueryWrapper();
                                qTask2.eq("projectlibraryguid", info7.getId());
                                qTask2.eq("status", GlobalConstants.SHENHE_STATUS2);
                                if (zgsSpostcertificatebaseService.getOne(qTask2) != null) {
                                    listAll.add(info7);
                                }
                            }
                        }
                    }
                    List<ZgsProjectParentInfo> list7_2 = this.baseMapper.listZgsProjectParentInfoJoin3_1(queryWrapper);
                    ZgsProjectParentInfo info7_2 = null;
                    for (int i = 0; i < list7_2.size(); i++) {
                        info7_2 = list7_2.get(i);
                        //查结题绩效是否已被创建
                        QueryWrapper<ZgsPerformancebase> qTask0 = new QueryWrapper();
                        qTask0.eq("baseguid", info7_2.getId());
                        qTask0.eq("intype", "0");
                        if (zgsPerformancebaseService.getOne(qTask0) == null) {
                            //查科研验收证书
                            QueryWrapper<ZgsExamplecertificatebase> qTask1 = new QueryWrapper();
                            qTask1.eq("projectlibraryguid", info7_2.getId());
                            qTask1.eq("status", GlobalConstants.SHENHE_STATUS2);
                            if (zgsExamplecertificatebaseService.getOne(qTask1) != null) {
                                listAll.add(info7_2);
                            }
                        }
                    }
                    break;
                case 8:
                    //8示范科研项目成果-共用（示范或科研证书终审通过）
                    List<ZgsProjectParentInfo> list8_1 = this.baseMapper.listZgsProjectParentInfoJoin3_1(queryWrapper);
                    ZgsProjectParentInfo info8_1 = null;
                    for (int i = 0; i < list8_1.size(); i++) {
                        info8_1 = list8_1.get(i);
                        //查成果是否已被创建
                        QueryWrapper<ZgsPlanresultbase> qTask0 = new QueryWrapper();
                        qTask0.eq("projectlibraryguid", info8_1.getId());
                        if (zgsPlanresultbaseService.getOne(qTask0) == null) {
                            //查结题绩效是否形审通过
                            QueryWrapper<ZgsPerformancebase> qTask1 = new QueryWrapper();
                            qTask1.eq("baseguid", info8_1.getId());
                            qTask1.eq("intype", "0");
                            qTask1.and(qw -> {
                                qw.eq("status", GlobalConstants.SHENHE_STATUS8).or().eq("status", GlobalConstants.SHENHE_STATUS4);
                            });
                            if (zgsPerformancebaseService.getOne(qTask1) != null) {
                                listAll.add(info8_1);
                            }
                        }
                    }
                    List<ZgsProjectParentInfo> list8_2 = this.baseMapper.listZgsProjectParentInfoJoin3_2(queryWrapper);
                    ZgsProjectParentInfo info8_2 = null;
                    for (int i = 0; i < list8_2.size(); i++) {
                        info8_2 = list8_2.get(i);
                        //查成果是否已被创建
                        QueryWrapper<ZgsPlanresultbase> qTask0 = new QueryWrapper();
                        qTask0.eq("projectlibraryguid", info8_2.getId());
                        if (zgsPlanresultbaseService.getOne(qTask0) == null) {
                            //查结题绩效是否形审通过
                            QueryWrapper<ZgsPerformancebase> qTask1 = new QueryWrapper();
                            qTask1.eq("baseguid", info8_2.getId());
                            qTask1.eq("intype", "0");
                            qTask1.and(qw -> {
                                qw.eq("status", GlobalConstants.SHENHE_STATUS8).or().eq("status", GlobalConstants.SHENHE_STATUS4);
                            });
                            if (zgsPerformancebaseService.getOne(qTask1) != null) {
                                listAll.add(info8_2);
                            }
                        }
                    }
                    break;
                case 9:
                    //9科研项目任务书（形审通过且立项）
                    queryWrapper.isNotNull("m.provincialfundtotal");
                    List<ZgsProjectParentInfo> list9 = this.baseMapper.listZgsProjectParentInfoJoin9(queryWrapper);
                    ZgsProjectParentInfo info9 = null;
                    for (int i = 0; i < list9.size(); i++) {
                        info9 = list9.get(i);
                        //查科研任务书是否已被创建
                        QueryWrapper<ZgsSciencetechtask> qTask = new QueryWrapper();
                        qTask.eq("sciencetechguid", info9.getId());
                        if (zgsSciencetechtaskService.getOne(qTask) == null) {
                            listAll.add(info9);
                        }
                    }
                    break;
                case 10:
                    // 获取科研结题验收项目 8
                    List<String>  addIdList = new ArrayList<>();
                    List<ZgsProjectParentInfo> scientificbaseList = this.baseMapper.listZgsProjectParentInfoJoin11(queryWrapper);
                    if (scientificbaseList.size() > 0) {
                        for (ZgsProjectParentInfo zgsProjectParentInfo:scientificbaseList
                        ) {
                            addIdList.add(zgsProjectParentInfo.getId());
                        }
                    }
                    //10科研项目验收（任务书形审通过）7
                    List<ZgsProjectParentInfo> list10 = this.baseMapper.listZgsProjectParentInfoJoin10(queryWrapper);
                    ZgsProjectParentInfo info10 = null;
                    for (int i = 0; i < list10.size(); i++) {
                        info10 = list10.get(i);
                        //查科研项目验收是否已被创建
                        QueryWrapper<ZgsScientificbase> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info10.getId());
                        qTask.orderByAsc("id");

                        int length = zgsScientificbaseService.list(qTask).size();
                        boolean bol = getBgIsFlag(info10.getId());

                        if (zgsScientificbaseService.list(qTask).size() == 0 && getBgIsFlag(info10.getId())) {
                            listAll.add(info10);
                        }
                    }
                    break;
                case 11:
                    //11科研结题项目验收（任务书形审通过）
                    queryWrapper.and((wrapper)->{wrapper.eq("m.provincialfundtotal", 0).or().eq("m.provincialfundtotal", "''");});
                    List<ZgsProjectParentInfo> list11 = this.baseMapper.listZgsProjectParentInfoJoin11(queryWrapper);
                    ZgsProjectParentInfo info11 = null;
                    for (int i = 0; i < list11.size(); i++) {
                        info11 = list11.get(i);
                        //查科研结题验收是否已被创建
                        QueryWrapper<ZgsScientificpostbase> qTask = new QueryWrapper();
                        qTask.eq("scientificbaseguid", info11.getId());
                        if (zgsScientificpostbaseService.getOne(qTask) == null && getBgIsFlag(info11.getId())) {
                            listAll.add(info11);
                        }
                    }
                    break;
                case 12:
                    //12科研验收证书（项目验收形审通过）
                    List<ZgsProjectParentInfo> list12 = this.baseMapper.listZgsProjectParentInfoJoin12(queryWrapper);
                    ZgsProjectParentInfo info12 = null;
                    for (int i = 0; i < list12.size(); i++) {
                        info12 = list12.get(i);
                        //查科研项证书是否已被创建
                        QueryWrapper<ZgsSacceptcertificatebase> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info12.getId());
                        if (zgsSacceptcertificatebaseService.getOne(qTask) == null) {
                            listAll.add(info12);
                        }
                    }
                    break;
                case 13:
                    //13科研结题证书（结题验收形审通过）
                    List<ZgsProjectParentInfo> list13 = this.baseMapper.listZgsProjectParentInfoJoin13(queryWrapper);
                    ZgsProjectParentInfo info13 = null;
                    for (int i = 0; i < list13.size(); i++) {
                        info13 = list13.get(i);
                        //查结题证书是否已被创建
                        QueryWrapper<ZgsSpostcertificatebase> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", info13.getId());
                        if (zgsSpostcertificatebaseService.getOne(qTask) == null) {
                            listAll.add(info13);
                        }
                    }
                    break;
            }
        }
        return ValidateEncryptEntityUtil.validateDecryptList(listAll, ValidateEncryptEntityUtil.isDecrypt);
    }

    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectCommonLibrary(Wrapper<ZgsProjectlibrary> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.listZgsProjectCommonLibrary(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }

    /**
     * 判断该项目变更状态是否可以新增中期查验、示范申请、验收申请、结题申请
     *
     * @param bid
     * @return
     */
    private boolean getBgIsFlag(String bid) {
        boolean flag = false;
        QueryWrapper<ZgsPlannedprojectchange> qTask = new QueryWrapper();
        qTask.eq("projectlibraryguid", bid);
        qTask.inSql("status", GlobalConstants.PROJECT_LIST1439);
        if (zgsPlannedprojectchangeService.getOne(qTask) == null) {
            flag = true;
        }
        return flag;
    }

    @Override
    public void updateAgreeProject(String pid) {
        ZgsProjectlibrary zgsProjectlibrary = this.baseMapper.selectById(pid);
        if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
            if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                QueryWrapper<ZgsGreenbuildproject> queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("projectlibraryguid", pid);
                queryWrapper1.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                zgsGreenbuildproject.setAgreeproject(new BigDecimal(2));
                zgsGreenbuildprojectService.update(zgsGreenbuildproject, queryWrapper1);
            } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                QueryWrapper<ZgsBuildproject> queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("projectlibraryguid", pid);
                queryWrapper2.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                zgsBuildproject.setAgreeproject(new BigDecimal(2));
                zgsBuildprojectService.update(zgsBuildproject, queryWrapper2);
            } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                QueryWrapper<ZgsAssembleproject> queryWrapper3 = new QueryWrapper();
                queryWrapper3.eq("projectlibraryguid", pid);
                queryWrapper3.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                zgsAssembleproject.setAgreeproject(new BigDecimal(2));
                zgsAssembleprojectService.update(zgsAssembleproject, queryWrapper3);
            } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                QueryWrapper<ZgsEnergybuildproject> queryWrapper4 = new QueryWrapper();
                queryWrapper4.eq("projectlibraryguid", pid);
                queryWrapper4.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                zgsEnergybuildproject.setAgreeproject(new BigDecimal(2));
                zgsEnergybuildprojectService.update(zgsEnergybuildproject, queryWrapper4);
            } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                QueryWrapper<ZgsSamplebuildproject> queryWrapper5 = new QueryWrapper();
                queryWrapper5.eq("projectlibraryguid", pid);
                queryWrapper5.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                zgsSamplebuildproject.setAgreeproject(new BigDecimal(2));
                zgsSamplebuildprojectService.update(zgsSamplebuildproject, queryWrapper5);
            }
        }
    }

    @Override
    public int updateAddMoney(ZgsProjectlibraryListInfo zgsProjectlibrary) {
        int updateNum = 0;
        ZgsProjectlibrary projectlibrary = this.baseMapper.selectById(zgsProjectlibrary.getId());
        //1项目库
        String sfxmstatus = null;
        if (StringUtils.isNotEmpty(projectlibrary.getSfxmtype()) && "1".equals(projectlibrary.getSfxmtype())) {
            sfxmstatus = "2";
        }
        return this.baseMapper.updateAddMoney(zgsProjectlibrary.getId(), zgsProjectlibrary.getProvincialfundtotal(), zgsProjectlibrary.getProjectnum(), sfxmstatus,zgsProjectlibrary.getFundRemark());
    }

    @Override
    public int rollBackSp(String id) {
        return this.baseMapper.rollBackSp(id);
    }

    @Override
    public void updateFeeSb(ZgsProjecttask zgsProjecttask) {
        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getDemonstratetypenum())) {
            if (GlobalConstants.GreenBuildType.equals(zgsProjecttask.getDemonstratetypenum())) {
                QueryWrapper<ZgsGreenbuildproject> queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
                queryWrapper1.eq("enterpriseguid", zgsProjecttask.getEnterpriseguid());
                ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                BeanUtils.copyProperties(zgsProjecttask.getZgsCommonFee(), zgsGreenbuildproject);
                zgsGreenbuildproject.setAgreeproject(new BigDecimal(2));
                zgsGreenbuildprojectService.update(zgsGreenbuildproject, queryWrapper1);
            } else if (GlobalConstants.ConstructBuidType.equals(zgsProjecttask.getDemonstratetypenum())) {
                QueryWrapper<ZgsBuildproject> queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
                queryWrapper2.eq("enterpriseguid", zgsProjecttask.getEnterpriseguid());
                ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                BeanUtils.copyProperties(zgsProjecttask.getZgsCommonFee(), zgsBuildproject);
                zgsBuildproject.setAgreeproject(new BigDecimal(2));
                zgsBuildprojectService.update(zgsBuildproject, queryWrapper2);
            } else if (GlobalConstants.AssemBuidType.equals(zgsProjecttask.getDemonstratetypenum())) {
                QueryWrapper<ZgsAssembleproject> queryWrapper3 = new QueryWrapper();
                queryWrapper3.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
                queryWrapper3.eq("enterpriseguid", zgsProjecttask.getEnterpriseguid());
                ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                BeanUtils.copyProperties(zgsProjecttask.getZgsCommonFee(), zgsAssembleproject);
                zgsAssembleproject.setAgreeproject(new BigDecimal(2));
                zgsAssembleprojectService.update(zgsAssembleproject, queryWrapper3);
            } else if (GlobalConstants.EnergyBuildType.equals(zgsProjecttask.getDemonstratetypenum())) {
                QueryWrapper<ZgsEnergybuildproject> queryWrapper4 = new QueryWrapper();
                queryWrapper4.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
                queryWrapper4.eq("enterpriseguid", zgsProjecttask.getEnterpriseguid());
                ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                BeanUtils.copyProperties(zgsProjecttask.getZgsCommonFee(), zgsEnergybuildproject);
                zgsEnergybuildproject.setAgreeproject(new BigDecimal(2));
                zgsEnergybuildprojectService.update(zgsEnergybuildproject, queryWrapper4);
            } else if (GlobalConstants.SampleBuidType.equals(zgsProjecttask.getDemonstratetypenum())) {
                QueryWrapper<ZgsSamplebuildproject> queryWrapper5 = new QueryWrapper();
                queryWrapper5.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
                queryWrapper5.eq("enterpriseguid", zgsProjecttask.getEnterpriseguid());
                ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                BeanUtils.copyProperties(zgsProjecttask.getZgsCommonFee(), zgsSamplebuildproject);
                zgsSamplebuildproject.setAgreeproject(new BigDecimal(2));
                zgsSamplebuildprojectService.update(zgsSamplebuildproject, queryWrapper5);
            }
        }
    }

    @Override
    public String getProjectNumCurrent() {
        String projectNum = this.baseMapper.getProjectNumCurrent(zgsCommonService.queryApplyYear());
        if (StringUtils.isNotEmpty(projectNum)) {
            if (projectNum.contains("-")) {
                String num[] = projectNum.split("-");
                int currentNum = Integer.parseInt(num[1]) + 1;
                String numT = String.valueOf(currentNum);
                if (currentNum < 10) {
                    numT = "0" + numT;
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
//                int year = calendar.get(Calendar.YEAR);
                projectNum = "JK" + zgsCommonService.queryApplyYear() + "-" + numT;
                return projectNum;
            }
        } else {
            return "JK" + zgsCommonService.queryApplyYear() + "-01";
        }
        return null;
    }

    @Override
    public void deleteByItemId(String ids) {
        if (StringUtils.isNotEmpty(ids)) {
            String strIds[] = ids.split(",");
            String bid = null;
            ZgsProjectlibrary zgsProjectlibrary = null;
            for (int i = 0; i < strIds.length; i++) {
                bid = strIds[i];
                if (StringUtils.isNotEmpty(bid)) {
                    zgsProjectlibrary = this.baseMapper.selectById(bid);
                    if (zgsProjectlibrary != null) {
                        if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsGreenbuildproject> queryWrapper1 = new QueryWrapper();
                            queryWrapper1.eq("projectlibraryguid", bid);
                            zgsGreenbuildprojectService.remove(queryWrapper1);
                        } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsBuildproject> queryWrapper2 = new QueryWrapper();
                            queryWrapper2.eq("projectlibraryguid", bid);
                            zgsBuildprojectService.remove(queryWrapper2);
                        } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsAssembleproject> queryWrapper3 = new QueryWrapper();
                            queryWrapper3.eq("projectlibraryguid", bid);
                            zgsAssembleprojectService.remove(queryWrapper3);
                        } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsEnergybuildproject> queryWrapper4 = new QueryWrapper();
                            queryWrapper4.eq("projectlibraryguid", bid);
                            zgsEnergybuildprojectService.remove(queryWrapper4);
                        } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsSamplebuildproject> queryWrapper5 = new QueryWrapper();
                            queryWrapper5.eq("projectlibraryguid", bid);
                            zgsSamplebuildprojectService.remove(queryWrapper5);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void initStageAndStatus(String projectlibraryguid) {
        log.info("每日定时更新项目当前阶段状态----------start");
        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper1 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper1.eq("id", projectlibraryguid);
        }
        queryWrapper1.ne("isdelete", 1);
        queryWrapper1.select("id");
        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper1);
        if (sciencetechfeasibleList != null && sciencetechfeasibleList.size() > 0) {
            String bid = null;
            for (ZgsSciencetechfeasible zgsSciencetechfeasible : sciencetechfeasibleList) {
                bid = zgsSciencetechfeasible.getId();
                redisUtil.set(bid, "申报阶段");
                redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSciencetechfeasible.getStatus());

                //任务书
                QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
                queryWrapper3.eq("sciencetechguid", bid);
                queryWrapper3.ne("isdelete", 1);
                queryWrapper3.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper3.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
                if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
                    redisUtil.set(bid, "任务书阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSciencetechtaskList.get(0).getStatus());
                }
                //项目变更
                QueryWrapper<ZgsPlannedprojectchange> queryWrapper6 = new QueryWrapper();
                queryWrapper6.eq("projectlibraryguid", bid);
                queryWrapper6.ne("isdelete", 1);
//                queryWrapper6.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
//                List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper6);
//                if (zgsPlannedprojectchangeList != null && zgsPlannedprojectchangeList.size() > 0) {
//                    redisUtil.set(bid, "变更阶段");
//                }
                //项目验收
                QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
                queryWrapper7.eq("projectlibraryguid", bid);
                queryWrapper7.ne("isdelete", 1);
                queryWrapper7.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper7.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper7);
                if (zgsScientificbaseList != null && zgsScientificbaseList.size() > 0) {
                    redisUtil.set(bid, "科研项目验收");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsScientificbaseList.get(0).getStatus());
                } else {
                    // 因项目验收或结题只能走其中一个阶段，所以当验收阶段满足时优先走验收阶段流程，当验收未查询到数据时再判断结题流程是否满足   rxl 20230628
                    //科研项目结题
                    QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
                    queryWrapper4.eq("scientificbaseguid", bid);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper4.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                    List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper4);
                    if (zgsScientificpostbaseList != null && zgsScientificpostbaseList.size() > 0) {
                        redisUtil.set(bid, "科研项目结题");
                        redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsScientificpostbaseList.get(0).getStatus());
                    }
                }
                //项目验收证书
                QueryWrapper<ZgsSacceptcertificatebase> queryWrapper11 = new QueryWrapper();
                queryWrapper11.eq("projectlibraryguid", bid);
                queryWrapper11.ne("isdelete", 1);
                queryWrapper11.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper11.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSacceptcertificatebase> zgsSacceptcertificatebaseList = zgsSacceptcertificatebaseService.list(queryWrapper11);
                if (zgsSacceptcertificatebaseList != null && zgsSacceptcertificatebaseList.size() > 0) {
                    redisUtil.set(bid, "科研项目验收证书");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSacceptcertificatebaseList.get(0).getStatus());
                }
                //科研项目结题证书
                QueryWrapper<ZgsSpostcertificatebase> queryWrapper12 = new QueryWrapper();
                queryWrapper12.eq("projectlibraryguid", bid);
                queryWrapper12.ne("isdelete", 1);
                queryWrapper12.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper12.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSpostcertificatebase> zgsSpostcertificatebaseList = zgsSpostcertificatebaseService.list(queryWrapper12);
                if (zgsSpostcertificatebaseList != null && zgsSpostcertificatebaseList.size() > 0) {
                    // 先判断当前项目是否为驳回项目，如果为驳回项目则判断驳回后的项目是否进行新的阶段，如果进行新的阶段则更新驳回状态为最新阶段状态，如果驳回后项目未进行新的阶段，则项目状态仍为驳回时的状态  rxl 20230705
                    if (zgsSpostcertificatebaseList.get(0).getStatus().equals(GlobalConstants.SHENHE_STATUS13) || zgsSpostcertificatebaseList.get(0).getStatus().equals(GlobalConstants.SHENHE_STATUS14)) {
                       //  if (zgsScientificbaseList.size() > 0) {
                            if (zgsSacceptcertificatebaseList.size() > 0) {
                                String state = zgsSacceptcertificatebaseList.get(0).getStatus();  // 新状态
                                // 更新redis为最新状态
                                redisUtil.set(bid, "科研项目验收证书");
                                redisUtil.set(GlobalConstants.STAGE_STATUS + bid, state);
                            } else {
                                // 修改原有数据状态为最新数据状态，更新redis状态
                                String state = zgsSpostcertificatebaseList.get(0).getStatus();  // 新状态
                                // zgsSpostcertificatebaseList.get(0).setStatus(state);
                                // zgsSpostcertificatebaseService.updateById(zgsSpostcertificatebaseList.get(0));  // 修改原有数据状态
                                // 更新redis为最新状态
                                redisUtil.set(bid, "科研项目结题证书");
                                redisUtil.set(GlobalConstants.STAGE_STATUS + bid, state);
                            }
                        /*} else {  // 当前驳回项目还未进行验收流程，故项目状态仍为原有状态
                            redisUtil.set(bid, "科研项目结题证书");
                            redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSpostcertificatebaseList.get(0).getStatus());
                        }*/
                    } else {
                        redisUtil.set(bid, "科研项目结题证书");
                        redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSpostcertificatebaseList.get(0).getStatus());
                    }
                }
                //绩效
                QueryWrapper<ZgsPerformancebase> queryWrapperJx = new QueryWrapper();
                queryWrapperJx.eq("baseguid", bid);
                queryWrapperJx.eq("intype", "0");
                queryWrapperJx.ne("isdelete", 1);
                queryWrapperJx.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapperJx.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPerformancebase> zgsPerformancebaseList = zgsPerformancebaseService.list(queryWrapperJx);
                if (zgsPerformancebaseList != null && zgsPerformancebaseList.size() > 0) {
                    redisUtil.set(bid, "结题验收绩效自评阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsPerformancebaseList.get(0).getStatus());
                }
                //成果简介
                QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
                queryWrapper10.eq("projectlibraryguid", bid);
                queryWrapper10.ne("isdelete", 1);
                queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper10.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
                if (zgsPlanresultbaseList != null && zgsPlanresultbaseList.size() > 0) {
                    redisUtil.set(bid, "成果简介阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsPlanresultbaseList.get(0).getStatus());
                }
            }
        }
        //2、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper2 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper2.eq("l.id", projectlibraryguid);
        }
        queryWrapper2.ne("l.isdelete", 1);
        List<ZgsProjectlibraryListInfo> zgsProjectlibraryList = listZgsProjectCommonLibrary(queryWrapper2);
        if (zgsProjectlibraryList != null && zgsProjectlibraryList.size() > 0) {
            String pid = null;
            for (ZgsProjectlibraryListInfo zgsProjectlibrary : zgsProjectlibraryList) {
                pid = zgsProjectlibrary.getId();
                redisUtil.set(pid, "申报阶段");
                redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsProjectlibrary.getStatus());
                //项目任务书
                QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
                queryWrapper32.eq("t.projectguid", pid);
                queryWrapper32.ne("t.isdelete", 1);
                queryWrapper32.ne("t.status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper32.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsProjectlibraryListInfo> zgsProjectlibraryListInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
                if (zgsProjectlibraryListInfoList != null && zgsProjectlibraryListInfoList.size() > 0) {
                    redisUtil.set(pid, "任务书阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsProjectlibraryListInfoList.get(0).getStatus());
                }
                //示范项目中期审查
                QueryWrapper<ZgsMidterminspection> queryWrapper5 = new QueryWrapper();
                queryWrapper5.eq("projectlibraryguid", pid);
                queryWrapper5.ne("isdelete", 1);
                queryWrapper5.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper5.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsMidterminspection> zgsMidterminspectionList = zgsMidterminspectionService.list(queryWrapper5);
                if (zgsMidterminspectionList != null && zgsMidterminspectionList.size() > 0) {
                    redisUtil.set(pid, "中期查验阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsMidterminspectionList.get(0).getStatus());
                }
                //项目变更
                QueryWrapper<ZgsPlannedprojectchange> queryWrapper13 = new QueryWrapper();
                queryWrapper13.eq("projectlibraryguid", pid);
                queryWrapper13.ne("isdelete", 1);
//                queryWrapper13.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
//                List<ZgsPlannedprojectchange> plannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper13);
//                if (plannedprojectchangeList != null && plannedprojectchangeList.size() > 0) {
//                    redisUtil.set(pid, "变更阶段");
//                }
                //项目验收
                QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
                queryWrapper72.eq("projectlibraryguid", pid);
                queryWrapper72.ne("isdelete", 1);
                queryWrapper72.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper72.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper72);
                if (zgsDemoprojectacceptanceList != null && zgsDemoprojectacceptanceList.size() > 0) {
                    redisUtil.set(pid, "示范项目验收");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsDemoprojectacceptanceList.get(0).getStatus());
                }
                //项目验收证书
                QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
                queryWrapper82.eq("projectlibraryguid", pid);
                queryWrapper82.ne("isdelete", 1);
                queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper82.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
                if (zgsExamplecertificatebaseList != null && zgsExamplecertificatebaseList.size() > 0) {
                    redisUtil.set(pid, "示范项目验收证书");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsExamplecertificatebaseList.get(0).getStatus());
                }
                //绩效
                QueryWrapper<ZgsPerformancebase> queryWrapperJx2 = new QueryWrapper();
                queryWrapperJx2.eq("baseguid", pid);
                queryWrapperJx2.eq("intype", 0);
                queryWrapperJx2.ne("isdelete", 1);
                queryWrapperJx2.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapperJx2.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPerformancebase> zgsPerformancebaseList2 = zgsPerformancebaseService.list(queryWrapperJx2);
                if (zgsPerformancebaseList2 != null && zgsPerformancebaseList2.size() > 0) {
                    redisUtil.set(pid, "结题验收绩效自评阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsPerformancebaseList2.get(0).getStatus());
                }
                //成果简介
                QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
                queryWrapper10.eq("projectlibraryguid", pid);
                queryWrapper10.ne("isdelete", 1);
                queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper10.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
                if (zgsPlanresultbaseList != null && zgsPlanresultbaseList.size() > 0) {
                    redisUtil.set(pid, "成果简介阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsPlanresultbaseList.get(0).getStatus());
                }
            }
        }
        updateProjectStage(projectlibraryguid);
        log.info("每日定时更新项目当前阶段状态----------end");
    }


    // 因更新状态方法多处调用，故此在验收证书阶段形审通过后 将原来的成果发布状态 更新为 验收证书终审通过状态，进而省厅可以进行撤回操作   rxl 20230726
    @Override
    public void initStageAndStatusForYszs(String projectlibraryguid) {
        log.info("每日定时更新项目当前阶段状态----------start");
        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper1 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper1.eq("id", projectlibraryguid);
        }
        queryWrapper1.ne("isdelete", 1);
        queryWrapper1.select("id");
        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper1);
        if (sciencetechfeasibleList != null && sciencetechfeasibleList.size() > 0) {
            String bid = null;
            for (ZgsSciencetechfeasible zgsSciencetechfeasible : sciencetechfeasibleList) {
                bid = zgsSciencetechfeasible.getId();
                redisUtil.set(bid, "申报阶段");
                redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSciencetechfeasible.getStatus());

                //任务书
                QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
                queryWrapper3.eq("sciencetechguid", bid);
                queryWrapper3.ne("isdelete", 1);
                queryWrapper3.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper3.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
                if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
                    redisUtil.set(bid, "任务书阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSciencetechtaskList.get(0).getStatus());
                }
                //项目变更
                QueryWrapper<ZgsPlannedprojectchange> queryWrapper6 = new QueryWrapper();
                queryWrapper6.eq("projectlibraryguid", bid);
                queryWrapper6.ne("isdelete", 1);
//                queryWrapper6.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
//                List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper6);
//                if (zgsPlannedprojectchangeList != null && zgsPlannedprojectchangeList.size() > 0) {
//                    redisUtil.set(bid, "变更阶段");
//                }
                //项目验收
                QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
                queryWrapper7.eq("projectlibraryguid", bid);
                queryWrapper7.ne("isdelete", 1);
                queryWrapper7.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper7.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper7);
                if (zgsScientificbaseList != null && zgsScientificbaseList.size() > 0) {
                    redisUtil.set(bid, "科研项目验收");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsScientificbaseList.get(0).getStatus());
                } else {
                    // 因项目验收或结题只能走其中一个阶段，所以当验收阶段满足时优先走验收阶段流程，当验收未查询到数据时再判断结题流程是否满足   rxl 20230628
                    //科研项目结题
                    QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
                    queryWrapper4.eq("scientificbaseguid", bid);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper4.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                    List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper4);
                    if (zgsScientificpostbaseList != null && zgsScientificpostbaseList.size() > 0) {
                        redisUtil.set(bid, "科研项目结题");
                        redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsScientificpostbaseList.get(0).getStatus());
                    }
                }
                //项目验收证书
                QueryWrapper<ZgsSacceptcertificatebase> queryWrapper11 = new QueryWrapper();
                queryWrapper11.eq("projectlibraryguid", bid);
                queryWrapper11.ne("isdelete", 1);
                queryWrapper11.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper11.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSacceptcertificatebase> zgsSacceptcertificatebaseList = zgsSacceptcertificatebaseService.list(queryWrapper11);
                if (zgsSacceptcertificatebaseList != null && zgsSacceptcertificatebaseList.size() > 0) {
                    redisUtil.set(bid, "科研项目验收证书");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSacceptcertificatebaseList.get(0).getStatus());
                }
                //科研项目结题证书
                QueryWrapper<ZgsSpostcertificatebase> queryWrapper12 = new QueryWrapper();
                queryWrapper12.eq("projectlibraryguid", bid);
                queryWrapper12.ne("isdelete", 1);
                queryWrapper12.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper12.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsSpostcertificatebase> zgsSpostcertificatebaseList = zgsSpostcertificatebaseService.list(queryWrapper12);
                if (zgsSpostcertificatebaseList != null && zgsSpostcertificatebaseList.size() > 0) {
                    // 先判断当前项目是否为驳回项目，如果为驳回项目则判断驳回后的项目是否进行新的阶段，如果进行新的阶段则更新驳回状态为最新阶段状态，如果驳回后项目未进行新的阶段，则项目状态仍为驳回时的状态  rxl 20230705
                    if (zgsSpostcertificatebaseList.get(0).getStatus().equals(GlobalConstants.SHENHE_STATUS13) || zgsSpostcertificatebaseList.get(0).getStatus().equals(GlobalConstants.SHENHE_STATUS14)) {
                        //  if (zgsScientificbaseList.size() > 0) {
                        if (zgsSacceptcertificatebaseList.size() > 0) {
                            String state = zgsSacceptcertificatebaseList.get(0).getStatus();  // 新状态
                            // 更新redis为最新状态
                            redisUtil.set(bid, "科研项目验收证书");
                            redisUtil.set(GlobalConstants.STAGE_STATUS + bid, state);
                        } else {
                            // 修改原有数据状态为最新数据状态，更新redis状态
                            String state = zgsSpostcertificatebaseList.get(0).getStatus();  // 新状态
                            // zgsSpostcertificatebaseList.get(0).setStatus(state);
                            // zgsSpostcertificatebaseService.updateById(zgsSpostcertificatebaseList.get(0));  // 修改原有数据状态
                            // 更新redis为最新状态
                            redisUtil.set(bid, "科研项目结题证书");
                            redisUtil.set(GlobalConstants.STAGE_STATUS + bid, state);
                        }
                        /*} else {  // 当前驳回项目还未进行验收流程，故项目状态仍为原有状态
                            redisUtil.set(bid, "科研项目结题证书");
                            redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSpostcertificatebaseList.get(0).getStatus());
                        }*/
                    } else {
                        redisUtil.set(bid, "科研项目结题证书");
                        redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsSpostcertificatebaseList.get(0).getStatus());
                    }
                }
                //绩效
                QueryWrapper<ZgsPerformancebase> queryWrapperJx = new QueryWrapper();
                queryWrapperJx.eq("baseguid", bid);
                queryWrapperJx.eq("intype", "0");
                queryWrapperJx.ne("isdelete", 1);
                queryWrapperJx.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapperJx.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPerformancebase> zgsPerformancebaseList = zgsPerformancebaseService.list(queryWrapperJx);
                if (zgsPerformancebaseList != null && zgsPerformancebaseList.size() > 0) {
                    redisUtil.set(bid, "结题验收绩效自评阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsPerformancebaseList.get(0).getStatus());
                }
                //成果简介
                /*QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
                queryWrapper10.eq("projectlibraryguid", bid);
                queryWrapper10.ne("isdelete", 1);
                queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper10.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
                if (zgsPlanresultbaseList != null && zgsPlanresultbaseList.size() > 0) {
                    redisUtil.set(bid, "成果简介阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + bid, zgsPlanresultbaseList.get(0).getStatus());
                }*/
            }
        }
        //2、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper2 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper2.eq("l.id", projectlibraryguid);
        }
        queryWrapper2.ne("l.isdelete", 1);
        List<ZgsProjectlibraryListInfo> zgsProjectlibraryList = listZgsProjectCommonLibrary(queryWrapper2);
        if (zgsProjectlibraryList != null && zgsProjectlibraryList.size() > 0) {
            String pid = null;
            for (ZgsProjectlibraryListInfo zgsProjectlibrary : zgsProjectlibraryList) {
                pid = zgsProjectlibrary.getId();
                redisUtil.set(pid, "申报阶段");
                redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsProjectlibrary.getStatus());
                //项目任务书
                QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
                queryWrapper32.eq("t.projectguid", pid);
                queryWrapper32.ne("t.isdelete", 1);
                queryWrapper32.ne("t.status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper32.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsProjectlibraryListInfo> zgsProjectlibraryListInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
                if (zgsProjectlibraryListInfoList != null && zgsProjectlibraryListInfoList.size() > 0) {
                    redisUtil.set(pid, "任务书阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsProjectlibraryListInfoList.get(0).getStatus());
                }
                //示范项目中期审查
                QueryWrapper<ZgsMidterminspection> queryWrapper5 = new QueryWrapper();
                queryWrapper5.eq("projectlibraryguid", pid);
                queryWrapper5.ne("isdelete", 1);
                queryWrapper5.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper5.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsMidterminspection> zgsMidterminspectionList = zgsMidterminspectionService.list(queryWrapper5);
                if (zgsMidterminspectionList != null && zgsMidterminspectionList.size() > 0) {
                    redisUtil.set(pid, "中期查验阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsMidterminspectionList.get(0).getStatus());
                }
                //项目变更
                QueryWrapper<ZgsPlannedprojectchange> queryWrapper13 = new QueryWrapper();
                queryWrapper13.eq("projectlibraryguid", pid);
                queryWrapper13.ne("isdelete", 1);
//                queryWrapper13.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
//                List<ZgsPlannedprojectchange> plannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper13);
//                if (plannedprojectchangeList != null && plannedprojectchangeList.size() > 0) {
//                    redisUtil.set(pid, "变更阶段");
//                }
                //项目验收
                QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
                queryWrapper72.eq("projectlibraryguid", pid);
                queryWrapper72.ne("isdelete", 1);
                queryWrapper72.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper72.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper72);
                if (zgsDemoprojectacceptanceList != null && zgsDemoprojectacceptanceList.size() > 0) {
                    redisUtil.set(pid, "示范项目验收");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsDemoprojectacceptanceList.get(0).getStatus());
                }
                //项目验收证书
                QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
                queryWrapper82.eq("projectlibraryguid", pid);
                queryWrapper82.ne("isdelete", 1);
                queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper82.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
                if (zgsExamplecertificatebaseList != null && zgsExamplecertificatebaseList.size() > 0) {
                    redisUtil.set(pid, "示范项目验收证书");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsExamplecertificatebaseList.get(0).getStatus());
                }
                //绩效
                QueryWrapper<ZgsPerformancebase> queryWrapperJx2 = new QueryWrapper();
                queryWrapperJx2.eq("baseguid", pid);
                queryWrapperJx2.eq("intype", 0);
                queryWrapperJx2.ne("isdelete", 1);
                queryWrapperJx2.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapperJx2.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPerformancebase> zgsPerformancebaseList2 = zgsPerformancebaseService.list(queryWrapperJx2);
                if (zgsPerformancebaseList2 != null && zgsPerformancebaseList2.size() > 0) {
                    redisUtil.set(pid, "结题验收绩效自评阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsPerformancebaseList2.get(0).getStatus());
                }
                //成果简介
                /*QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
                queryWrapper10.eq("projectlibraryguid", pid);
                queryWrapper10.ne("isdelete", 1);
                queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS0);
//                queryWrapper10.and(qwp2 -> {
//                    qwp2.isNotNull("firstdate");
//                    qwp2.or(f2 -> {
//                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1);
//                    });
//                });
                List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
                if (zgsPlanresultbaseList != null && zgsPlanresultbaseList.size() > 0) {
                    redisUtil.set(pid, "成果简介阶段");
                    redisUtil.set(GlobalConstants.STAGE_STATUS + pid, zgsPlanresultbaseList.get(0).getStatus());
                }*/
            }
        }
        updateProjectStage(projectlibraryguid);
        log.info("每日定时更新项目当前阶段状态----------end");
    }




    @Override
    public Map<String, String> getExportXlsData(String id, String bid, Map<String, String> map) {
        ZgsProjectlibrary zgsProjectlibrary = ValidateEncryptEntityUtil.validateDecryptObject(this.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        map.put("projectname", zgsProjectlibrary.getProjectname());
        if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
            map.put("startenddate", DateUtils.formatDate(zgsProjectlibrary.getProjectstartdate()) + "\n" + DateUtils.formatDate(zgsProjectlibrary.getProjectenddate()));
            ZgsAssembleproject assembleproject = zgsAssembleprojectService.getById(bid);
            if (assembleproject != null) {
                map.put("content", assembleproject.getProjectsurvey());
            }
        } else {
            map.put("startenddate", DateUtils.formatDate(zgsProjectlibrary.getStartdate()) + "\n" + DateUtils.formatDate(zgsProjectlibrary.getCompletedate()));
            map.put("content", zgsProjectlibrary.getSimplesummary());
        }
        QueryWrapper<ZgsBuildjointunit> queryWrapper = new QueryWrapper();
        queryWrapper.eq("buildguid", bid);
        queryWrapper.isNull("tag");
        queryWrapper.ne("isdelete", 1);
        List<ZgsBuildjointunit> buildjointunitList = zgsBuildjointunitService.list(queryWrapper);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < buildjointunitList.size(); i++) {
            if (StringUtils.isNotEmpty(buildjointunitList.get(i).getApplyunit())) {
                sb.append(Sm4Util.decryptEcb(buildjointunitList.get(i).getApplyunit()));
            }
            sb.append("\n");
        }
        if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
            //联合申报单位
            map.put("unit", zgsProjectlibrary.getApplyunit());
        } else {
            map.put("unit", sb.toString());
        }
        QueryWrapper<ZgsBuildfundbudget> queryWrapper6 = new QueryWrapper<>();
        queryWrapper6.eq("buildguid", bid);
        queryWrapper6.ne("isdelete", 1);
        queryWrapper6.select(
                "IFNULL(SUM(provincialfund),0) provincialfund",
                "IFNULL(SUM(unitselffund),0) unitselffund"
        );
        List<ZgsBuildfundbudget> zgsBuildfundbudgetList = zgsBuildfundbudgetService.list(queryWrapper6);
        if (zgsBuildfundbudgetList != null && zgsBuildfundbudgetList.size() == 1) {
            map.put("bk", zgsBuildfundbudgetList.get(0).getProvincialfund().toString());
            map.put("zc", zgsBuildfundbudgetList.get(0).getUnitselffund().toString());
        } else {
            map.put("bk", "0");
            map.put("zc", "0");
        }
        return null;
    }

    public void updateProjectStage(String projectlibraryguid) {
        log.info("各个表每日定时更新项目当前阶段状态----------start");
        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper1 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper1.eq("id", projectlibraryguid);
        }
        queryWrapper1.ne("isdelete", 1);
//        queryWrapper1.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper1.ne("status", GlobalConstants.SHENHE_STATUS1);
        queryWrapper1.select("id");
        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper1);
        Object bc1 = null;
        for (int a1 = 0; a1 < sciencetechfeasibleList.size(); a1++) {
            bc1 = redisUtil.get(sciencetechfeasibleList.get(a1).getId());
            if (bc1 != null) {
                Object status = redisUtil.get(GlobalConstants.STAGE_STATUS + sciencetechfeasibleList.get(a1).getId());
                Object id = sciencetechfeasibleList.get(a1).getId();
                zgsSciencetechtaskService.updateStageByBid2(bc1.toString(), sciencetechfeasibleList.get(a1).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + sciencetechfeasibleList.get(a1).getId()));
            } else {
                zgsSciencetechtaskService.updateStageByBid2("申报阶段", sciencetechfeasibleList.get(a1).getId(), null);
            }
        }
        //任务书
        QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper3.eq("sciencetechguid", projectlibraryguid);
        }
        queryWrapper3.ne("isdelete", 1);
//        queryWrapper3.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper3.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
        Object bc2 = null;
        for (int a2 = 0; a2 < zgsSciencetechtaskList.size(); a2++) {
            bc2 = redisUtil.get(zgsSciencetechtaskList.get(a2).getSciencetechguid());
            if (bc2 != null) {
                Object status = redisUtil.get(GlobalConstants.STAGE_STATUS + zgsSciencetechtaskList.get(a2).getSciencetechguid());
                Object id = zgsSciencetechtaskList.get(a2).getId();
                zgsSciencetechtaskService.updateStageByBid4(bc2.toString(), zgsSciencetechtaskList.get(a2).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsSciencetechtaskList.get(a2).getSciencetechguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid4("任务书阶段", zgsSciencetechtaskList.get(a2).getId(), null);
            }
        }
        //项目变更
        QueryWrapper<ZgsPlannedprojectchange> queryWrapper6 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper6.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper6.ne("isdelete", 1);
//        queryWrapper6.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper6.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper6);
        if (zgsPlannedprojectchangeList != null && zgsPlannedprojectchangeList.size() > 0) {
//                    redisUtil.set(bid, "变更阶段");
        }
        //项目验收
        QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper7.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper7.ne("isdelete", 1);
//        queryWrapper7.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper7.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper7);
        Object bc3 = null;
        for (int a3 = 0; a3 < zgsScientificbaseList.size(); a3++) {
            bc3 = redisUtil.get(zgsScientificbaseList.get(a3).getProjectlibraryguid());
            if (bc3 != null) {
                Object status = redisUtil.get(GlobalConstants.STAGE_STATUS + zgsScientificbaseList.get(a3).getProjectlibraryguid());
                Object id = zgsScientificbaseList.get(a3).getId();
                zgsSciencetechtaskService.updateStageByBid7(bc3.toString(), zgsScientificbaseList.get(a3).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsScientificbaseList.get(a3).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid7("科研项目验收", zgsScientificbaseList.get(a3).getId(), null);
            }
        }
        //科研项目结题
        QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper4.eq("scientificbaseguid", projectlibraryguid);
        }
        queryWrapper4.ne("isdelete", 1);
//        queryWrapper4.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper4.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper4);
        Object bc4 = null;
        for (int a4 = 0; a4 < zgsScientificpostbaseList.size(); a4++) {
            bc4 = redisUtil.get(zgsScientificpostbaseList.get(a4).getScientificbaseguid());
            if (bc4 != null) {
                Object status = redisUtil.get(GlobalConstants.STAGE_STATUS + zgsScientificpostbaseList.get(a4).getScientificbaseguid());
                Object id = zgsScientificpostbaseList.get(a4).getId();
                zgsSciencetechtaskService.updateStageByBid8(bc4.toString(), zgsScientificpostbaseList.get(a4).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsScientificpostbaseList.get(a4).getScientificbaseguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid8("科研项目结题", zgsScientificpostbaseList.get(a4).getId(), null);
            }
        }
        //项目验收证书
        QueryWrapper<ZgsSacceptcertificatebase> queryWrapper11 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper11.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper11.ne("isdelete", 1);
//        queryWrapper11.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper11.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsSacceptcertificatebase> zgsSacceptcertificatebaseList = zgsSacceptcertificatebaseService.list(queryWrapper11);
        Object bc5 = null;
        for (int a5 = 0; a5 < zgsSacceptcertificatebaseList.size(); a5++) {
            bc5 = redisUtil.get(zgsSacceptcertificatebaseList.get(a5).getProjectlibraryguid());
            if (bc5 != null) {
                zgsSciencetechtaskService.updateStageByBid9(bc5.toString(), zgsSacceptcertificatebaseList.get(a5).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsSacceptcertificatebaseList.get(a5).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid9("科研项目验收证书", zgsSacceptcertificatebaseList.get(a5).getId(), null);
            }
        }
        //科研项目结题证书
        QueryWrapper<ZgsSpostcertificatebase> queryWrapper12 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper12.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper12.ne("isdelete", 1);
//        queryWrapper12.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper12.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsSpostcertificatebase> zgsSpostcertificatebaseList = zgsSpostcertificatebaseService.list(queryWrapper12);
        Object bc6 = null;
        for (int a6 = 0; a6 < zgsSpostcertificatebaseList.size(); a6++) {
            bc6 = redisUtil.get(zgsSpostcertificatebaseList.get(a6).getProjectlibraryguid());
            if (bc6 != null) {
                zgsSciencetechtaskService.updateStageByBid10(bc6.toString(), zgsSpostcertificatebaseList.get(a6).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsSpostcertificatebaseList.get(a6).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid10("科研项目结题证书", zgsSpostcertificatebaseList.get(a6).getId(), null);
            }
        }
        //绩效
        QueryWrapper<ZgsPerformancebase> queryWrapperJx = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapperJx.eq("baseguid", projectlibraryguid);
        }
        queryWrapperJx.ne("isdelete", 1);
//        queryWrapperJx.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapperJx.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsPerformancebase> zgsPerformancebaseList = zgsPerformancebaseService.list(queryWrapperJx);
        Object bc7 = null;
        ZgsPerformancebase zgsPerformancebase = null;
        for (int a7 = 0; a7 < zgsPerformancebaseList.size(); a7++) {
            zgsPerformancebase = zgsPerformancebaseList.get(a7);
            bc7 = redisUtil.get(zgsPerformancebase.getBaseguid());
            if (bc7 != null) {
                zgsSciencetechtaskService.updateStageByBid12(bc7.toString(), zgsPerformancebaseList.get(a7).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsPerformancebase.getBaseguid()));
                //0结题1年度
                if ("0".equals(zgsPerformancebase.getIntype())) {

                }
            } else {
                zgsSciencetechtaskService.updateStageByBid12("结题验收绩效自评阶段", zgsPerformancebaseList.get(a7).getId(), null);
                //0结题1年度
                if ("0".equals(zgsPerformancebase.getIntype())) {

                }
            }
        }
        //成果简介
        QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapperJx.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper10.ne("isdelete", 1);
//        queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper10.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
        Object bc8 = null;
        for (int a8 = 0; a8 < zgsPlanresultbaseList.size(); a8++) {
            bc8 = redisUtil.get(zgsPlanresultbaseList.get(a8).getProjectlibraryguid());
            if (bc8 != null) {
                zgsSciencetechtaskService.updateStageByBid13(bc8.toString(), zgsPlanresultbaseList.get(a8).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsPlanresultbaseList.get(a8).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid13("成果简介阶段", zgsPlanresultbaseList.get(a8).getId(), null);
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        //2、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper2 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper2.eq("id", projectlibraryguid);
        }
        queryWrapper2.ne("isdelete", 1);
        List<ZgsProjectlibrary> zgsProjectlibraryList = list(queryWrapper2);
        Object bc9 = null;
        for (int a9 = 0; a9 < zgsProjectlibraryList.size(); a9++) {
            bc9 = redisUtil.get(zgsProjectlibraryList.get(a9).getId());
            if (bc9 != null) {
                zgsSciencetechtaskService.updateStageByBid1(bc9.toString(), zgsProjectlibraryList.get(a9).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsProjectlibraryList.get(a9).getId()));
            } else {
                zgsSciencetechtaskService.updateStageByBid1("申报阶段", zgsProjectlibraryList.get(a9).getId(), null);
            }
        }
        //项目任务书
        QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper32.eq("projectguid", projectlibraryguid);
        }
        queryWrapper32.ne("isdelete", 1);
//        queryWrapper32.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper32.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsProjecttask> zgsProjecttaskList = zgsProjecttaskService.list(queryWrapper32);
        Object bc10 = null;
        for (int a10 = 0; a10 < zgsProjecttaskList.size(); a10++) {
            bc10 = redisUtil.get(zgsProjecttaskList.get(a10).getProjectguid());
            if (bc10 != null) {
                zgsSciencetechtaskService.updateStageByBid3(bc10.toString(), zgsProjecttaskList.get(a10).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsProjecttaskList.get(a10).getProjectguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid3("任务书阶段", zgsProjecttaskList.get(a10).getId(), null);
            }
        }
        //示范项目中期审查
        QueryWrapper<ZgsMidterminspection> queryWrapper5 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper5.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper5.ne("isdelete", 1);
//        queryWrapper5.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper5.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsMidterminspection> zgsMidterminspectionList = zgsMidterminspectionService.list(queryWrapper5);
        Object bc11 = null;
        for (int a11 = 0; a11 < zgsMidterminspectionList.size(); a11++) {
            bc11 = redisUtil.get(zgsMidterminspectionList.get(a11).getProjectlibraryguid());
            if (bc11 != null) {
                zgsSciencetechtaskService.updateStageByBid5(bc11.toString(), zgsMidterminspectionList.get(a11).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsMidterminspectionList.get(a11).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid5("中期查验阶段", zgsMidterminspectionList.get(a11).getId(), null);
            }
        }
        //项目验收
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper72.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper72.ne("isdelete", 1);
//        queryWrapper72.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper72.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper72);
        Object bc12 = null;
        for (int a12 = 0; a12 < zgsDemoprojectacceptanceList.size(); a12++) {
            bc12 = redisUtil.get(zgsDemoprojectacceptanceList.get(a12).getProjectlibraryguid());
            if (bc12 != null) {
                zgsSciencetechtaskService.updateStageByBid6(bc12.toString(), zgsDemoprojectacceptanceList.get(a12).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsDemoprojectacceptanceList.get(a12).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid6("示范项目验收", zgsDemoprojectacceptanceList.get(a12).getId(), null);
            }
        }
        //项目验收证书
        QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
        if (StringUtils.isNotEmpty(projectlibraryguid)) {
            queryWrapper82.eq("projectlibraryguid", projectlibraryguid);
        }
        queryWrapper82.ne("isdelete", 1);
//        queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS0);
//        queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS1);
        List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
        Object bc13 = null;
        for (int a13 = 0; a13 < zgsExamplecertificatebaseList.size(); a13++) {
            bc13 = redisUtil.get(zgsExamplecertificatebaseList.get(a13).getProjectlibraryguid());
            if (bc13 != null) {
                zgsSciencetechtaskService.updateStageByBid11(bc13.toString(), zgsExamplecertificatebaseList.get(a13).getId(), redisUtil.get(GlobalConstants.STAGE_STATUS + zgsExamplecertificatebaseList.get(a13).getProjectlibraryguid()));
            } else {
                zgsSciencetechtaskService.updateStageByBid11("示范项目验收证书", zgsExamplecertificatebaseList.get(a13).getId(), null);
            }
        }
        log.info("各个表每日定时更新项目当前阶段状态----------end");
    }


    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOne(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoOne(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoOneForExport(String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoOneForExport(fieldStr,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }


    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwo(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoTwo(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoTwoForExport(String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoTwoForExport(fieldStr,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }


    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThree(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoThree(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoThreeForExport(String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoThreeForExport(fieldStr,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }


    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFour(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoFour(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFourForExport(String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoFourForExport(fieldStr,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }


    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFive(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoFive(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjectlibraryListInfoFiveForExport(String fieldStr,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.listZgsProjectlibraryListInfoFiveForExport(fieldStr,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }


    @Override
    public List<ZgsProjectlibraryListInfo> selectAllData(Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.selectAllData(queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }
    @Override
    public List<ZgsProjectlibraryListInfo> selectAllDataForExport(String fieldStrOfAllData,Wrapper<ZgsProjectlibrary> queryWrapper) {
        List<ZgsProjectlibraryListInfo> listInfo = this.baseMapper.selectAllDataForExport(fieldStrOfAllData,queryWrapper);
        return ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
    }



}
