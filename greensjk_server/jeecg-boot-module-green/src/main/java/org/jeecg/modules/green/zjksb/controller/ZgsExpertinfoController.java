package org.jeecg.modules.green.zjksb.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.po.ZgsExpertinfoPo;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertprojectfinish;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.green.zjksb.entity.*;
import org.jeecg.modules.green.zjksb.service.*;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 专家库表
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Api(tags = "专家库表")
@RestController
@RequestMapping("/zjksb/zgsExpertinfo")
@Slf4j
public class ZgsExpertinfoController extends JeecgController<ZgsExpertinfo, IZgsExpertinfoService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsExpertworkresumeService zgsExpertworkresumeService;
    @Autowired
    private IZgsExpertprojectbearService zgsExpertprojectbearService;
    @Autowired
    private IZgsExpertpatentService zgsExpertpatentService;
    @Autowired
    private IZgsExpertpaperService zgsExpertpaperService;
    @Autowired
    private IZgsExpertmonographService zgsExpertmonographService;
    @Autowired
    private IZgsExpertawardService zgsExpertawardService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;

    /**
     * 分页列表查询
     *
     * @param zgsExpertinfo
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "专家库表-分页列表查询")
    @ApiOperation(value = "专家库表-分页列表查询", notes = "专家库表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsExpertinfo zgsExpertinfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "pId", required = false) String pId,
                                   @RequestParam(name = "typeLx", required = false) Integer typeLx,
                                   @RequestParam(name = "nameList", required = false) List<String> nameList,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsExpertinfo> queryWrapper = new QueryWrapper();
        final String username = zgsExpertinfo.getUsername();
        if (zgsExpertinfo != null) {
            zgsExpertinfo = ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertinfo, ValidateEncryptEntityUtil.isEncrypt);
            if (StringUtils.isNotEmpty(zgsExpertinfo.getUsername())) {
                    //  queryWrapper.like("username", zgsExpertinfo.getUsername()).or().like("unit", zgsExpertinfo.getUsername());
                queryWrapper.and(pqw -> {
                    // pqw.like("username", Sm4Util.encryptEcb(username)).or().like("unit", Sm4Util.encryptEcb(username));
                    pqw.like("username", Sm4Util.encryptEcb(username)).or().like("username", username);
                });
            }
            if (StringUtils.isNotEmpty(zgsExpertinfo.getStudydirection())) {
                queryWrapper.like("studydirection", zgsExpertinfo.getStudydirection());
            }
            if (StringUtils.isNotEmpty(zgsExpertinfo.getExpertlibcategory())) {
                queryWrapper.like("expertlibcategory", zgsExpertinfo.getExpertlibcategory());
            }
            // 所学专业
            if (StringUtils.isNotEmpty(zgsExpertinfo.getMajorstudy())) {
                queryWrapper.like("majorstudy", zgsExpertinfo.getMajorstudy());
            }
            /*if (StringUtils.isNotEmpty(zgsExpertinfo.getStatus())) {
                if (!"-1".equals(zgsExpertinfo.getStatus())) {
                    queryWrapper.eq("status", zgsExpertinfo.getStatus());
                }
            }*/
            //TODO status有null
            String status = zgsExpertinfo.getStatus();
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) { // 省厅
                if (StringUtils.isNotEmpty(status)) {
                    if ("3".equals(status)) {
                        queryWrapper.notInSql("status", "1,8");
                    } else {
                        queryWrapper.eq("status", status);
                    }
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {  // 市州
                    queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS8);

            } else {
                if (StringUtils.isNotEmpty(status)) {
                    queryWrapper.eq("status", status);
                }
            }
        }
        if (StringUtils.isNotEmpty(pId)) {
            if (typeLx == 1) {
                //示范项目抽取
                ZgsProjectlibrary zgsProjectlibrary = zgsProjectlibraryService.getById(pId);
                if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getApplyunit())) {
                    queryWrapper.notLike("unit", zgsProjectlibrary.getApplyunit());
                }
            } else if (typeLx == 0) {
                //科研项目抽取
                ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(pId);
                if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getCommitmentunit())) {
                    queryWrapper.notLike("unit", zgsSciencetechfeasible.getCommitmentunit());
                }
            }
            //专家库自动屏蔽本单位参加人员
            if (nameList != null) {
                if (nameList.size() > 0) {
                    queryWrapper.and(w1 -> {
                        for (String str : nameList) {
                            w1.ne("username", Sm4Util.encryptEcb(str));
                        }
                    });
                }
            }
            //只抽取形审通过的专家
            queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS8);
        }
        if (sysUser.getLoginUserType() != 3 && sysUser.getLoginUserType() != 7) {
            String unitmemberid = sysUser.getUnitmemberid();
            if (sysUser.getLoginUserType() == 2) {
                //专家
                queryWrapper.eq("userid", unitmemberid);
            } else {
                queryWrapper.eq("userid", null);
            }
        }
        // 增加单位模糊搜索功能
        if (StringUtils.isNotEmpty(zgsExpertinfo.getUnit())) {
            String unitName = zgsExpertinfo.getUnit();
            queryWrapper.and(pqunit -> {
                pqunit.like("unit", Sm4Util.decryptEcb(unitName)).or().like("unit", unitName);
            });
        }

        queryWrapper.ne("isdelete", 1);
        queryWrapper.orderByDesc("applydate,createdate");
        Page<ZgsExpertinfo> page = new Page<ZgsExpertinfo>(pageNo, pageSize);
        IPage<ZgsExpertinfo> pageList = zgsExpertinfoService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 专家抽取分页列表查询
     *
     * @param zgsExpertChouQu
     * @param req
     * @return
     */
    @AutoLog(value = "专家库表-专家抽取分页列表查询")
    @ApiOperation(value = "专家库表-专家抽取分页列表查询", notes = "专家库表-专家抽取分页列表查询")
    @GetMapping(value = "/chouQulist")
    public Result<?> queryPageChouQuList(ZgsExpertChouQu zgsExpertChouQu, HttpServletRequest req) {
        QueryWrapper<ZgsExpertinfo> queryWrapper = new QueryWrapper();
        int num = 3;
        if (zgsExpertChouQu != null) {
            if (StringUtils.isNotEmpty(zgsExpertChouQu.getId())) {
                if (zgsExpertChouQu.getTypeLx() == 1) {
                    //示范项目抽取
                    ZgsProjectlibrary zgsProjectlibrary = zgsProjectlibraryService.getById(zgsExpertChouQu.getId());
                    if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getApplyunit())) {
                        queryWrapper.notLike("unit", zgsProjectlibrary.getApplyunit());
                    }
                } else if (zgsExpertChouQu.getTypeLx() == 0) {
                    //科研项目抽取
                    ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getById(zgsExpertChouQu.getId());
                    if (zgsSciencetechfeasible != null && StringUtils.isNotEmpty(zgsSciencetechfeasible.getCommitmentunit())) {
                        queryWrapper.notLike("unit", zgsSciencetechfeasible.getCommitmentunit());
                    }
                }
            }
            if (zgsExpertChouQu.getNum() != null) {
                num = zgsExpertChouQu.getNum();
            }
            if (StringUtils.isNotEmpty(zgsExpertChouQu.getType())) {
                if (zgsExpertChouQu.getType().contains(",")) {
                    String typeStr[] = zgsExpertChouQu.getType().split(",");
                    queryWrapper.and(w1 -> {
                        for (String str : typeStr) {
                            w1.or(w2 -> {
                                w2.like("expertlibcategorynum", str);
                            });
                        }
                    });
                } else {
                    queryWrapper.in("expertlibcategorynum", zgsExpertChouQu.getType());
                }
            }
            if (zgsExpertChouQu.getNameList() != null) {
                if (zgsExpertChouQu.getNameList().size() > 0) {
                    queryWrapper.and(w1 -> {
                        for (String str : zgsExpertChouQu.getNameList()) {
                            w1.ne("username", Sm4Util.encryptEcb(str));
                        }
                    });
                }
            }
        }
        queryWrapper.eq("status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper.ne("isdelete", 1);
        // 总记录数
        int count = zgsExpertinfoService.count(queryWrapper);
        // 随机数起始位置
        int randomCount = (int) (Math.random() * count);
        // 保证能展示几个数据
        if (randomCount > count - num) {
            randomCount = count - num;
        }
        if (randomCount < 0) {
            randomCount = count;
        }
        queryWrapper.last("limit " + randomCount + ", " + num);
        //
        List<ZgsExpertinfo> list = zgsExpertinfoService.list(queryWrapper);
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptList(list, ValidateEncryptEntityUtil.isDecrypt));
    }

    /**
     * 添加
     *
     * @param zgsExpertinfo
     * @return
     */
    @AutoLog(value = "专家库表-添加")
    @ApiOperation(value = "专家库表-添加", notes = "专家库表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsExpertinfo zgsExpertinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsExpertinfo.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsExpertinfo.getStatus())) {
            //保存并上报
            zgsExpertinfo.setApplydate(new Date());
        } else {
            zgsExpertinfo.setApplydate(null);
        }
        zgsExpertinfo.setUserid(sysUser.getUnitmemberid());
        zgsExpertinfo.setCreateaccountname(sysUser.getUsername());
        zgsExpertinfo.setCreateusername(sysUser.getRealname());
        zgsExpertinfo.setCreatedate(new Date());
        //查询下是否已新增
        QueryWrapper<ZgsExpertinfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("userid", sysUser.getUnitmemberid());
        List<ZgsExpertinfo> list = zgsExpertinfoService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("不可重复添加！");
        }
        //
        zgsExpertinfoService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertinfo, ValidateEncryptEntityUtil.isEncrypt));
        //工作简历
        List<ZgsExpertworkresume> zgsExpertworkresumeList = zgsExpertinfo.getZgsExpertworkresumeList();
        if (zgsExpertworkresumeList != null && zgsExpertworkresumeList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertworkresumeList.size(); a1++) {
                ZgsExpertworkresume zgsExpertworkresume = zgsExpertworkresumeList.get(a1);
                zgsExpertworkresume.setId(UUID.randomUUID().toString());
                zgsExpertworkresume.setExpertguid(id);
                zgsExpertworkresume.setCreateaccountname(sysUser.getUsername());
                zgsExpertworkresume.setCreateusername(sysUser.getRealname());
                zgsExpertworkresume.setCreatedate(new Date());
                zgsExpertworkresumeService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertworkresume, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //获奖情况
        List<ZgsExpertaward> zgsExpertawardList = zgsExpertinfo.getZgsExpertawardList();
        if (zgsExpertawardList != null && zgsExpertawardList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertawardList.size(); a1++) {
                ZgsExpertaward zgsExpertaward = zgsExpertawardList.get(a1);
                zgsExpertaward.setId(UUID.randomUUID().toString());
                zgsExpertaward.setExpertguid(id);
                zgsExpertaward.setCreateaccountname(sysUser.getUsername());
                zgsExpertaward.setCreateusername(sysUser.getRealname());
                zgsExpertaward.setCreatedate(new Date());
                zgsExpertawardService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertaward, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //论文
        List<ZgsExpertpaper> zgsExpertpaperList = zgsExpertinfo.getZgsExpertpaperList();
        if (zgsExpertpaperList != null && zgsExpertpaperList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertpaperList.size(); a1++) {
                ZgsExpertpaper zgsExpertpaper = zgsExpertpaperList.get(a1);
                zgsExpertpaper.setId(UUID.randomUUID().toString());
                zgsExpertpaper.setExpertguid(id);
                zgsExpertpaper.setCreateaccountname(sysUser.getUsername());
                zgsExpertpaper.setCreateusername(sysUser.getRealname());
                zgsExpertpaper.setCreatedate(new Date());
                zgsExpertpaperService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertpaper, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //专著
        List<ZgsExpertmonograph> zgsExpertmonographList = zgsExpertinfo.getZgsExpertmonographList();
        if (zgsExpertmonographList != null && zgsExpertmonographList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertmonographList.size(); a1++) {
                ZgsExpertmonograph zgsExpertmonograph = zgsExpertmonographList.get(a1);
                zgsExpertmonograph.setId(UUID.randomUUID().toString());
                zgsExpertmonograph.setExpertguid(id);
                zgsExpertmonograph.setCreateaccountname(sysUser.getUsername());
                zgsExpertmonograph.setCreateusername(sysUser.getRealname());
                zgsExpertmonograph.setCreatedate(new Date());
                zgsExpertmonographService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertmonograph, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsExpertinfo.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.ExpertLibray);
                        zgsAttachappendix.setAppendixsubtype("ExpertLibray");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }
        //专利
        List<ZgsExpertpatent> zgsExpertpatentList = zgsExpertinfo.getZgsExpertpatentList();
        if (zgsExpertpatentList != null && zgsExpertpatentList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertpatentList.size(); a1++) {
                ZgsExpertpatent zgsExpertpatent = zgsExpertpatentList.get(a1);
                zgsExpertpatent.setId(UUID.randomUUID().toString());
                zgsExpertpatent.setExpertguid(id);
                zgsExpertpatent.setCreateaccountname(sysUser.getUsername());
                zgsExpertpatent.setCreateusername(sysUser.getRealname());
                zgsExpertpatent.setCreatedate(new Date());
                zgsExpertpatentService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertpatent, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //项目承担情况
        List<ZgsExpertprojectbear> zgsExpertprojectbearList = zgsExpertinfo.getZgsExpertprojectbearList();
        if (zgsExpertprojectbearList != null && zgsExpertprojectbearList.size() > 0) {
            for (int a1 = 0; a1 < zgsExpertprojectbearList.size(); a1++) {
                ZgsExpertprojectbear zgsExpertprojectbear = zgsExpertprojectbearList.get(a1);
                zgsExpertprojectbear.setId(UUID.randomUUID().toString());
                zgsExpertprojectbear.setExpertguid(id);
                zgsExpertprojectbear.setCreateaccountname(sysUser.getUsername());
                zgsExpertprojectbear.setCreateusername(sysUser.getRealname());
                zgsExpertprojectbear.setCreatedate(new Date());
                zgsExpertprojectbearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertprojectbear, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsExpertinfo
     * @return
     */
    @AutoLog(value = "专家库表-编辑")
    @ApiOperation(value = "专家库表-编辑", notes = "专家库表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsExpertinfo zgsExpertinfo) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
            String id = zgsExpertinfo.getId();
            if (GlobalConstants.handleSubmit.equals(zgsExpertinfo.getStatus())) {
                //保存并上报
                zgsExpertinfo.setApplydate(new Date());
                zgsExpertinfo.setAuditopinion(null);
            } else {
                zgsExpertinfo.setApplydate(null);
            }
            zgsExpertinfo.setModifyaccountname(sysUser.getUsername());
            zgsExpertinfo.setModifyusername(sysUser.getRealname());
            zgsExpertinfo.setModifydate(new Date());
            zgsExpertinfoService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertinfo, ValidateEncryptEntityUtil.isEncrypt));
            //工作简历
            List<ZgsExpertworkresume> zgsExpertworkresumeList = zgsExpertinfo.getZgsExpertworkresumeList();
            if (zgsExpertworkresumeList != null && zgsExpertworkresumeList.size() > 0) {
                QueryWrapper<ZgsExpertworkresume> queryWrapper0 = new QueryWrapper<>();
                queryWrapper0.eq("expertguid", id);
                queryWrapper0.ne("isdelete", 1);
                zgsExpertworkresumeService.remove(queryWrapper0);
                for (int a1 = 0; a1 < zgsExpertworkresumeList.size(); a1++) {
                    ZgsExpertworkresume zgsExpertworkresume = zgsExpertworkresumeList.get(a1);
                    zgsExpertworkresume.setId(UUID.randomUUID().toString());
                    zgsExpertworkresume.setExpertguid(id);
                    zgsExpertworkresume.setCreateaccountname(sysUser.getUsername());
                    zgsExpertworkresume.setCreateusername(sysUser.getRealname());
                    zgsExpertworkresume.setCreatedate(new Date());
                    zgsExpertworkresumeService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertworkresume, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //获奖情况
            List<ZgsExpertaward> zgsExpertawardList = zgsExpertinfo.getZgsExpertawardList();
            if (zgsExpertawardList != null && zgsExpertawardList.size() > 0) {
                QueryWrapper<ZgsExpertaward> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("expertguid", id);
                queryWrapper1.ne("isdelete", 1);
                zgsExpertawardService.remove(queryWrapper1);
                for (int a1 = 0; a1 < zgsExpertawardList.size(); a1++) {
                    ZgsExpertaward zgsExpertaward = zgsExpertawardList.get(a1);
                    zgsExpertaward.setId(UUID.randomUUID().toString());
                    zgsExpertaward.setExpertguid(id);
                    zgsExpertaward.setCreateaccountname(sysUser.getUsername());
                    zgsExpertaward.setCreateusername(sysUser.getRealname());
                    zgsExpertaward.setCreatedate(new Date());
                    zgsExpertawardService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertaward, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //论文
            List<ZgsExpertpaper> zgsExpertpaperList = zgsExpertinfo.getZgsExpertpaperList();
            if (zgsExpertpaperList != null && zgsExpertpaperList.size() > 0) {
                QueryWrapper<ZgsExpertpaper> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("expertguid", id);
                queryWrapper2.ne("isdelete", 1);
                zgsExpertpaperService.remove(queryWrapper2);
                for (int a1 = 0; a1 < zgsExpertpaperList.size(); a1++) {
                    ZgsExpertpaper zgsExpertpaper = zgsExpertpaperList.get(a1);
                    zgsExpertpaper.setId(UUID.randomUUID().toString());
                    zgsExpertpaper.setExpertguid(id);
                    zgsExpertpaper.setCreateaccountname(sysUser.getUsername());
                    zgsExpertpaper.setCreateusername(sysUser.getRealname());
                    zgsExpertpaper.setCreatedate(new Date());
                    zgsExpertpaperService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertpaper, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //专著
            List<ZgsExpertmonograph> zgsExpertmonographList = zgsExpertinfo.getZgsExpertmonographList();
            if (zgsExpertmonographList != null && zgsExpertmonographList.size() > 0) {
                QueryWrapper<ZgsExpertmonograph> queryWrapper3 = new QueryWrapper<>();
                queryWrapper3.eq("expertguid", id);
                queryWrapper3.ne("isdelete", 1);
                zgsExpertmonographService.remove(queryWrapper3);
                for (int a1 = 0; a1 < zgsExpertmonographList.size(); a1++) {
                    ZgsExpertmonograph zgsExpertmonograph = zgsExpertmonographList.get(a1);
                    zgsExpertmonograph.setId(UUID.randomUUID().toString());
                    zgsExpertmonograph.setExpertguid(id);
                    zgsExpertmonograph.setCreateaccountname(sysUser.getUsername());
                    zgsExpertmonograph.setCreateusername(sysUser.getRealname());
                    zgsExpertmonograph.setCreatedate(new Date());
                    zgsExpertmonographService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertmonograph, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsExpertinfo.getZgsMattermaterialList();
            String mattermaterialId;
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (StringUtils.isEmpty(mattermaterialId)) {
                        mattermaterialId = UUID.randomUUID().toString();
                        zgsMattermaterial.setId(mattermaterialId);
                        zgsMattermaterial.setBuildguid(id);
                        zgsMattermaterialService.save(zgsMattermaterial);
                    }
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.ExpertLibray);
                                zgsAttachappendix.setAppendixsubtype("ExpertLibray");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
            //专利
            List<ZgsExpertpatent> zgsExpertpatentList = zgsExpertinfo.getZgsExpertpatentList();
            if (zgsExpertpatentList != null && zgsExpertpatentList.size() > 0) {
                QueryWrapper<ZgsExpertpatent> queryWrapper5 = new QueryWrapper<>();
                queryWrapper5.eq("expertguid", id);
                queryWrapper5.ne("isdelete", 1);
                zgsExpertpatentService.remove(queryWrapper5);
                for (int a1 = 0; a1 < zgsExpertpatentList.size(); a1++) {
                    ZgsExpertpatent zgsExpertpatent = zgsExpertpatentList.get(a1);
                    zgsExpertpatent.setId(UUID.randomUUID().toString());
                    zgsExpertpatent.setExpertguid(id);
                    zgsExpertpatent.setCreateaccountname(sysUser.getUsername());
                    zgsExpertpatent.setCreateusername(sysUser.getRealname());
                    zgsExpertpatent.setCreatedate(new Date());
                    zgsExpertpatentService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertpatent, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //项目承担情况
            List<ZgsExpertprojectbear> zgsExpertprojectbearList = zgsExpertinfo.getZgsExpertprojectbearList();
            if (zgsExpertprojectbearList != null && zgsExpertprojectbearList.size() > 0) {
                QueryWrapper<ZgsExpertprojectbear> queryWrapper6 = new QueryWrapper<>();
                queryWrapper6.eq("expertguid", id);
                queryWrapper6.ne("isdelete", 1);
                zgsExpertprojectbearService.remove(queryWrapper6);
                for (int a1 = 0; a1 < zgsExpertprojectbearList.size(); a1++) {
                    ZgsExpertprojectbear zgsExpertprojectbear = zgsExpertprojectbearList.get(a1);
                    zgsExpertprojectbear.setId(UUID.randomUUID().toString());
                    zgsExpertprojectbear.setExpertguid(id);
                    zgsExpertprojectbear.setCreateaccountname(sysUser.getUsername());
                    zgsExpertprojectbear.setCreateusername(sysUser.getRealname());
                    zgsExpertprojectbear.setCreatedate(new Date());
                    zgsExpertprojectbearService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertprojectbear, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "专家库表-通过id删除")
    @ApiOperation(value = "专家库表-通过id删除", notes = "专家库表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsExpertinfoService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "专家库表-批量删除")
    @ApiOperation(value = "专家库表-批量删除", notes = "专家库表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsExpertinfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "专家库表-通过id查询")
    @ApiOperation(value = "专家库表-通过id查询", notes = "专家库表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsExpertinfo zgsExpertinfo = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsExpertinfo = ValidateEncryptEntityUtil.validateDecryptObject(zgsExpertinfoService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsExpertinfo == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsExpertinfo);
            }
        } else {
            //新增时查出附件
            zgsExpertinfo = new ZgsExpertinfo();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.ExpertLibray);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsExpertinfo.setZgsMattermaterialList(zgsMattermaterialList);
        }
        return Result.OK(zgsExpertinfo);
    }

    //TODO 9-20 修改专家库
    @AutoLog(value = "根据类型人数进行排序")
    @ApiOperation(value = "专家库表-根据类型人数进行排序", notes = "专家库表-根据类型人数进行排序")
    @GetMapping(value = "/getTheNumberOfExpertsFor")
    public Result<?> getTheNumberOfExpertsFor(@RequestParam(name = "type") Integer type) {
        List<ZgsExpertinfoPo> zgsExpertinfoPo = null;
        zgsExpertinfoPo = zgsMattermaterialService.getTheNumberOfExpertsFor();
        for (ZgsExpertinfoPo zgExpertinfo : zgsExpertinfoPo) {
            int countUser = zgsMattermaterialService.getUserCount(zgExpertinfo.getItemValue(), type);
            zgExpertinfo.setCountUser(countUser);
        }
        zgsExpertinfoPo.sort(Comparator.comparing(ZgsExpertinfoPo::getCountUser).reversed());
        return Result.OK(zgsExpertinfoPo);
    }

    private void initProjectSelectById(ZgsExpertinfo zgsExpertinfo) {
        String id = zgsExpertinfo.getId();
        if (StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(id)) {
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.ExpertLibray);
            map.put("buildguid", id);
            //工作简历
            QueryWrapper<ZgsExpertworkresume> queryWrapper0 = new QueryWrapper<>();
            queryWrapper0.eq("expertguid", id);
            queryWrapper0.ne("isdelete", 1);
            queryWrapper0.orderByAsc("starttime");
            List<ZgsExpertworkresume> zgsExpertworkresumeList = zgsExpertworkresumeService.list(queryWrapper0);
            zgsExpertinfo.setZgsExpertworkresumeList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertworkresumeList, ValidateEncryptEntityUtil.isDecrypt));
            //获奖情况
            QueryWrapper<ZgsExpertaward> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("expertguid", id);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("years");
            List<ZgsExpertaward> zgsExpertawardList = zgsExpertawardService.list(queryWrapper1);
            zgsExpertinfo.setZgsExpertawardList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertawardList, ValidateEncryptEntityUtil.isDecrypt));
            //论文
            QueryWrapper<ZgsExpertpaper> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("expertguid", id);
            queryWrapper2.ne("isdelete", 1);
            queryWrapper1.orderByAsc("publishdate");
            List<ZgsExpertpaper> zgsExpertpaperList = zgsExpertpaperService.list(queryWrapper2);
            zgsExpertinfo.setZgsExpertpaperList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertpaperList, ValidateEncryptEntityUtil.isDecrypt));
            //专著
            QueryWrapper<ZgsExpertmonograph> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("expertguid", id);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper1.orderByAsc("publishdate");
            List<ZgsExpertmonograph> zgsExpertmonographList = zgsExpertmonographService.list(queryWrapper3);
            zgsExpertinfo.setZgsExpertmonographList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertmonographList, ValidateEncryptEntityUtil.isDecrypt));
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper4 = new QueryWrapper<>();
                    queryWrapper4.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper4.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper4), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);

                }
                zgsExpertinfo.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //专利
            QueryWrapper<ZgsExpertpatent> queryWrapper5 = new QueryWrapper<>();
            queryWrapper5.eq("expertguid", id);
            queryWrapper5.ne("isdelete", 1);
            queryWrapper5.orderByAsc("authorizedate");
            List<ZgsExpertpatent> zgsExpertpatentList = zgsExpertpatentService.list(queryWrapper5);
            zgsExpertinfo.setZgsExpertpatentList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertpatentList, ValidateEncryptEntityUtil.isDecrypt));
            //项目承担情况
            QueryWrapper<ZgsExpertprojectbear> queryWrapper6 = new QueryWrapper<>();
            queryWrapper6.eq("expertguid", id);
            queryWrapper6.ne("isdelete", 1);
            queryWrapper6.orderByAsc("ranking");
            List<ZgsExpertprojectbear> zgsExpertprojectbearList = zgsExpertprojectbearService.list(queryWrapper6);
            zgsExpertinfo.setZgsExpertprojectbearList(ValidateEncryptEntityUtil.validateDecryptList(zgsExpertprojectbearList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsExpertinfo
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsExpertinfo zgsExpertinfo,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "pId", required = false) String pId,
                                  @RequestParam(name = "typeLx", required = false) Integer typeLx,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsExpertinfo, 1, 9999, pId, typeLx, null, req);
        IPage<ZgsExpertinfo> pageList = (IPage<ZgsExpertinfo>) result.getResult();
        List<ZgsExpertinfo> list = pageList.getRecords();
        List<ZgsExpertinfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "专家信息";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsExpertinfo.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        return mv;
    }

    private String getId(ZgsExpertinfo item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsExpertinfo.class);
    }

    /**
     * 审批
     *
     * @param zgsExpertinfo
     * @return
     */
    @AutoLog(value = "专家-审批")
    @ApiOperation(value = "专家-审批", notes = "专家-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsExpertinfo zgsExpertinfo) {
        zgsExpertinfoService.spProject(zgsExpertinfo);
        return Result.OK("审批成功！");
    }


    /**
     * @describe: 获取已规避的专家列表信息
     * @author: renxiaoliang
     * @date: 2023/6/15 15:41
     */
    @AutoLog(value = "专家库表-获取已规避的专家列表信息")
    @ApiOperation(value = "专家库表-获取已规避的专家列表信息", notes = "专家库表-获取已规避的专家列表信息")
    @GetMapping(value = "/queryExpertListByType")
    public Result<?> queryExpertListByType(ZgsExpertinfo zgsExpertinfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "typeLx", required = false) String typeLx,
                                   @RequestParam(name = "projectLeaderName", required = true) String projectLeaderName,
                                   @RequestParam(name = "projectId", required = false) String projectId,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsExpertinfo> queryWrapper = new QueryWrapper();
        final String username = zgsExpertinfo.getUsername();
        if (zgsExpertinfo != null) {
            zgsExpertinfo = ValidateEncryptEntityUtil.validateEncryptObject(zgsExpertinfo, ValidateEncryptEntityUtil.isEncrypt);
            if (StringUtils.isNotEmpty(zgsExpertinfo.getUsername())) {
                queryWrapper.and(pqw -> {
                    pqw.like("username", Sm4Util.encryptEcb(username)).or().like("username", username);
                });
            }
            if (StringUtils.isNotEmpty(zgsExpertinfo.getStudydirection())) {
                queryWrapper.like("studydirection", zgsExpertinfo.getStudydirection());
            }
            if (StringUtils.isNotBlank(typeLx)) {
                queryWrapper.like("expertlibcategory", typeLx);
            }
            if (StringUtils.isNotEmpty(zgsExpertinfo.getStatus())) {
                if (!"-1".equals(zgsExpertinfo.getStatus())) {
                    queryWrapper.eq("status", zgsExpertinfo.getStatus());
                }
            }
        }

        // 判断个人/申报单位添加专家信息时  规避专家为当前项目负责人的信息以及申报阶段、中期查验阶段已经分配过的专家信息
        // 1、规避掉项目负责人为专家的信息
        if (StringUtils.isNotBlank(projectLeaderName)) {  // 项目负责人姓名
            /*queryWrapper.and(wq -> {
                    wq.ne("username", Sm4Util.encryptEcb(projectLeaderName)).or().ne("username", projectLeaderName);
            });*/

            if (projectLeaderName.split(",").length > 0) {
                String nameStr = "";
                queryWrapper.and(w1 -> {
                    for (String leaderName : projectLeaderName.split(",")) {
                        w1.ne("username", Sm4Util.encryptEcb(leaderName)).or().ne("username", leaderName);
                    }
                });
            }
        }

        // 2、规避掉在当前项目已经被分配过的专家信息
        if (StringUtils.isNotBlank(projectId)) {
            QueryWrapper qWrapper = new QueryWrapper();
            qWrapper.eq("baseguid",projectId);
            List<ZgsSacceptcertexpert> zgsSacceptcertexperts = zgsSacceptcertexpertService.list(qWrapper);  // 根据项目id查询已经被分配的所有专家信息
            if (zgsSacceptcertexperts.size() > 0) {
                String nameStr = "";
                queryWrapper.and(w2 -> {
                    for (ZgsSacceptcertexpert zgsSacceptcertexpert : zgsSacceptcertexperts) {
                        w2.ne("username", Sm4Util.encryptEcb(zgsSacceptcertexpert.getName())).or().ne("username", zgsSacceptcertexpert.getName());
                    }
                });
            }
        }
        queryWrapper.ne("isdelete", 1);
        queryWrapper.orderByDesc("applydate,createdate");
        Page<ZgsExpertinfo> page = new Page<ZgsExpertinfo>(pageNo, pageSize);
        IPage<ZgsExpertinfo> pageList = zgsExpertinfoService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * @describe: 专家信息保存验证
     * @author: renxiaoliang
     * @date: 2023/6/15 16:24
     */
    @AutoLog(value = "验收专家信息提交验证")
    @ApiOperation(value = "验收专家信息提交验证", notes = "验收专家信息提交验证")
    @GetMapping(value = "/expertInfoValidation")
    public Result<?> validateExpertInfo(@RequestParam(name = "projectId", required = false) String projectId,
                                        @RequestParam(name = "expertNameStr", required = false) String expertNameStr,
                                        @RequestParam(name = "projectReposibleNameStr", required = true) String projectReposibleNameStr,
                                        HttpServletRequest req) {
        Result result = new Result();
        // 1、自定义添加的专家信息与历史专家信息重复验证
        // 2、专家库添加的专家信息与历史专家信息重复验证(添加专家时已处理)
        // 3、当前列表上自定义添加的专家信息与从专家库添加的专家信息进行重复验证
        // 4、自定义添加的专家信息与项目负责人信息验证

        List<ZgsSacceptcertexpert> zgsSacceptcertexperts = null;
        // 查询当前项目id已关联分配的所有专家信息
        if (StringUtils.isNotBlank(projectId)) {
            QueryWrapper qWrapper = new QueryWrapper();
            qWrapper.eq("baseguid",projectId);
            zgsSacceptcertexperts = zgsSacceptcertexpertService.list(qWrapper);
        }

        // 判断当前提交的专家列表是否存在重复信息（手动添加与专家库添加可能存在重复信息）
        if (StringUtils.isNotBlank(expertNameStr)) {
            for (String expertName:expertNameStr.split(",")
                 ) {
                if (expertName.indexOf(expertNameStr) > 0) {
                    result.setCode(-1);
                    result.setMessage("专家信息重复添加");
                    result.setSuccess(false);
                    return result;
                }
                // 判断当前提交的专家信息是否为项目负责人
                /*if (StringUtils.isNotBlank(projectReposibleNameStr) && expertName.equals(projectReposibleNameStr)) {
                    result.setCode(-1);
                    result.setMessage("项目负责人不能添加为审批专家");
                    result.setSuccess(false);
                    return result;
                }*/
                // 专家信息规避（专家信息不能为单位负责人）
                if (StringUtils.isNotBlank(projectReposibleNameStr)) {
                    for (String reposibleName:projectReposibleNameStr.split(",")) {
                        if (expertName.equals(reposibleName)) {
                            result.setCode(-1);
                            result.setMessage("专家信息不能为项目负责人");
                            result.setSuccess(false);
                            return result;
                        }
                    }
                }
                // 判断是否与当前项目的历史专家信息重复
                if (zgsSacceptcertexperts.size() > 0) {
                    for (ZgsSacceptcertexpert entity:zgsSacceptcertexperts
                    ) {
                        if (expertName.equals(entity.getName())) {
                            result.setCode(-1);
                            result.setMessage("项目负责人不能添加为审批专家");
                            result.setSuccess(false);
                            return result;
                        }
                    }
                }
            }
        }
        result.setCode(1);
        result.setMessage("验证成功");
        result.setSuccess(true);
        return result;
    }







}
