package org.jeecg.modules.green.kyxmjtsq.service;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研项目申报结项基本信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificpostbaseService extends IService<ZgsScientificpostbase> {
    int spProject(ZgsScientificpostbase zgsScientificpostbase);
}
