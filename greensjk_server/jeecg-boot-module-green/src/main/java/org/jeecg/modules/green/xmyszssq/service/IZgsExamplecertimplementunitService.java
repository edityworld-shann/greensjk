package org.jeecg.modules.green.xmyszssq.service;

import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertimplementunit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 示范验收证书项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsExamplecertimplementunitService extends IService<ZgsExamplecertimplementunit> {

}
