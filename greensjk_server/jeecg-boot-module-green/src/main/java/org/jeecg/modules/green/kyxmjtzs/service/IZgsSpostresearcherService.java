package org.jeecg.modules.green.kyxmjtzs.service;

import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsSpostresearcherService extends IService<ZgsSpostresearcher> {

}
