package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompany;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 装配式建筑-单位库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaCityCompanyService extends IService<ZgsReportMonthfabrAreaCityCompany> {


    public void addIterm(String filltm);
}
