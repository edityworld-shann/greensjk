package org.jeecg.modules.green.report.service;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.annotations.Param;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaproject;

/**
 * @Description: 装配式建筑-已竣工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
public interface IZgsReportMonthfabrAreaCityCompletedService extends IService<ZgsReportMonthfabrAreaCityCompleted> {

    ZgsReportMonthfabrAreaproject queryProjectByReportTime(String reporttm);

    void initMonthfabrAreaTotalALL(int type, String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea, String areacode, String areaname, Integer applyState, LoginUser sysUser);

//    void initMonthfabrAreaTotal(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea);

//    void initMonthfabrCityTotal(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea);

    void initMonthfabrCityTotalAll(int type, Integer applyState, String year, int quarter, String yearMonth, String projecttype, String areacode, String areaname, LoginUser sysUser);

    void initMonthfabrAreaTotalZero(String year, int quarter, String yearMonth, String projecttype, double newConstructionArea, double completedArea);

    void spProject(String id, Integer spStatus, String backreason);

    ZgsReportMonthfabrAreaCityCompleted lastTotalDataProvince(String filltm, String projecttype, Integer applystate);

    ZgsReportMonthfabrAreaCityCompleted lastTotalDataCity(String filltm, String areacode, String projecttype, Integer applystate);

}
