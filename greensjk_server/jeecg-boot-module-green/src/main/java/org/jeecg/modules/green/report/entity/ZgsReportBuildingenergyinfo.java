package org.jeecg.modules.green.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: zgs_report_buildingenergyinfo
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_buildingenergyinfo")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_buildingenergyinfo对象", description = "zgs_report_buildingenergyinfo")
public class ZgsReportBuildingenergyinfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * 新增建筑面积小计
     */
    @Excel(name = "新增建筑面积小计", width = 15)
    @ApiModelProperty(value = "新增建筑面积小计")
    private java.math.BigDecimal addbulidtotal;
    /**
     * 新增建筑面积居住面积
     */
    @Excel(name = "新增建筑面积居住面积", width = 15)
    @ApiModelProperty(value = "新增建筑面积居住面积")
    private java.math.BigDecimal addbulidlive;
    /**
     * 新增建筑面积公共建筑面积
     */
    @Excel(name = "新增建筑面积公共建筑面积", width = 15)
    @ApiModelProperty(value = "新增建筑面积公共建筑面积")
    private java.math.BigDecimal addbulidpublic;
    /**
     * 标准比例
     */
    @Excel(name = "标准比例", width = 15)
    @ApiModelProperty(value = "标准比例")
    private java.math.BigDecimal standardrate;
    /**
     * 大型公共建筑栋数
     */
    @Excel(name = "大型公共建筑栋数", width = 15)
    @ApiModelProperty(value = "大型公共建筑栋数")
    private java.math.BigDecimal bigpublicbuildnum;
    /**
     * 大型公共建筑面积
     */
    @Excel(name = "大型公共建筑面积", width = 15)
    @ApiModelProperty(value = "大型公共建筑面积")
    private java.math.BigDecimal bigpulicebulid;
    /**
     * 太阳能光热应用面积
     */
    @Excel(name = "太阳能光热应用面积", width = 15)
    @ApiModelProperty(value = "太阳能光热应用面积")
    private java.math.BigDecimal sunlightuser;
    /**
     * 太阳能光电应用面积
     */
    @Excel(name = "太阳能光电应用面积", width = 15)
    @ApiModelProperty(value = "太阳能光电应用面积")
    private java.math.BigDecimal sunenergyuser;
    /**
     * 浅层应用面积
     */
    @Excel(name = "浅层应用面积", width = 15)
    @ApiModelProperty(value = "浅层应用面积")
    private java.math.BigDecimal shallowuser;
    /**
     * 填报人
     */
    @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String fillpersontel;
    /**
     * 填报时间
     */
    @Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 填报单位
     */
    @Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    private java.lang.String fillunit;
    /**
     * 审核人
     */
    @Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    @Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 删除标志
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "createpersonname")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    private java.lang.String modifypersonaccount;
    /**
     * 修改人名称
     */
    @Excel(name = "修改人名称", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * 行政区域编码
     */
    @Excel(name = "行政区域编码", width = 15)
    @ApiModelProperty(value = "areacode")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    @Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    @Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    @Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;


}
