package org.jeecg.modules.green.kyxmjtzs.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科研结题证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_spostresearcher")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_spostresearcher对象", description = "科研结题证书主要研究人员名单")
public class ZgsSpostresearcher implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 甘肃省建设科技科研项目结题证书主键
     */
    @Excel(name = "甘肃省建设科技科研项目结题证书主键", width = 15)
    @ApiModelProperty(value = "甘肃省建设科技科研项目结题证书主键")
    private java.lang.String baseguid;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String name;
    /**
     * 性别
     */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String sex;
    /**
     * 身份证号码
     */
    @Excel(name = "身份证号码", width = 15)
    @ApiModelProperty(value = "身份证号码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String idcard;
    /**
     * 技术职称
     */
    @Excel(name = "技术职称", width = 15)
    @ApiModelProperty(value = "技术职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String technicaltitle;
    /**
     * 工作单位
     */
    @Excel(name = "工作单位", width = 15)
    @ApiModelProperty(value = "工作单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String workunit;
    /**
     * 对成果创造性贡献
     */
    @Excel(name = "对成果创造性贡献", width = 15)
    @ApiModelProperty(value = "对成果创造性贡献")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String contribution;
    /**
     * createaccountname
     */
    @Excel(name = "createaccountname", width = 15)
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
    @Excel(name = "modifyaccountname", width = 15)
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
    @Excel(name = "modifyusername", width = 15)
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    private java.lang.String status;
    /**
     * 文化程度
     */
    @Excel(name = "文化程度", width = 15)
    @ApiModelProperty(value = "文化程度")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String education;
    /**
     * ordernum
     */
    @Excel(name = "ordernum", width = 15)
    @ApiModelProperty(value = "ordernum")
    private java.math.BigDecimal ordernum;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
