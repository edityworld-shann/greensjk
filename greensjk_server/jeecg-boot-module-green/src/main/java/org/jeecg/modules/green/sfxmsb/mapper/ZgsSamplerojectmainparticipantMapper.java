package org.jeecg.modules.green.sfxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sfxmsb.entity.ZgsSamplerojectmainparticipant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 市政公用建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsSamplerojectmainparticipantMapper extends BaseMapper<ZgsSamplerojectmainparticipant> {

}
