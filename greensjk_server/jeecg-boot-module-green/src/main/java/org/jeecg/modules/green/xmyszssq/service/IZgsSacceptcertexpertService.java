package org.jeecg.modules.green.xmyszssq.service;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研验收证书验收专家名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsSacceptcertexpertService extends IService<ZgsSacceptcertexpert> {

}
