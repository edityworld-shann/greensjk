package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemprojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsAssemprojectmainparticipantMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsAssemprojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 装配式示范工程工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsAssemprojectmainparticipantServiceImpl extends ServiceImpl<ZgsAssemprojectmainparticipantMapper, ZgsAssemprojectmainparticipant> implements IZgsAssemprojectmainparticipantService {

}
