package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsBuildjointunit;
import org.jeecg.modules.green.common.mapper.ZgsBuildjointunitMapper;
import org.jeecg.modules.green.common.service.IZgsBuildjointunitService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 示范工程联合申报单位表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsBuildjointunitServiceImpl extends ServiceImpl<ZgsBuildjointunitMapper, ZgsBuildjointunit> implements IZgsBuildjointunitService {

}
