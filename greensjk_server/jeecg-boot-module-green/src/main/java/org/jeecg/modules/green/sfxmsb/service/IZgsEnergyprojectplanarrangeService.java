package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergyprojectplanarrange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 节能建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsEnergyprojectplanarrangeService extends IService<ZgsEnergyprojectplanarrange> {

}
