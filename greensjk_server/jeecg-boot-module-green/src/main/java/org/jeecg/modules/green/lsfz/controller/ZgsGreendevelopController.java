package org.jeecg.modules.green.lsfz.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.GreenUtilSelf;
import org.jeecg.common.util.VerificationUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.IZgsAttachappendixService;
import org.jeecg.modules.green.common.service.IZgsExpertsetService;
import org.jeecg.modules.green.common.service.IZgsMattermaterialService;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelop;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelopDetail;
import org.jeecg.modules.green.lsfz.service.ZgsGreendevelopDetailService;
import org.jeecg.modules.green.lsfz.service.ZgsGreendevelopService;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Struct;
import java.time.LocalDate;
import java.util.*;

/**
 * @describe:绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/14 11:24
 */
@Api(tags = "绿色发展项目")
@RestController
@RequestMapping("/lsfz/zgsGreenDevelop")
@Slf4j
public class ZgsGreendevelopController extends JeecgController<ZgsGreendevelop, ZgsGreendevelopService> {


    @Autowired
    private ZgsGreendevelopService zgsGreendevelopService;
    @Autowired
    private ZgsGreendevelopDetailService zgsGreendevelopDetailService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;



    @AutoLog(value = "绿色发展项目库-分页列表查询")
    @ApiOperation(value = "绿色发展项目库-分页列表查询", notes = "绿色发展项目库-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsGreendevelop zgsGreendevelop,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        QueryWrapper<ZgsGreendevelop> queryWrapper = new QueryWrapper<>();
        // 填报单位  市州/金融单位/终审单位具备此查询条件，申报单位不具备
        if (StringUtils.isNotEmpty(zgsGreendevelop.getFillUnit())) {
            queryWrapper.like("fill_unit", zgsGreendevelop.getFillUnit());
        }
        // 项目名称
        if (StringUtils.isNotEmpty(zgsGreendevelop.getProjectName())) {
            queryWrapper.like("project_name", zgsGreendevelop.getProjectName());
        }
        // 金融审批状态
        if (StringUtils.isNotEmpty(zgsGreendevelop.getFinanceState())) {
            queryWrapper.eq("finance_state", zgsGreendevelop.getFinanceState());
        }
        // 申报年度
        if (ObjectUtil.isNotEmpty(zgsGreendevelop.getYear()) && ObjectUtil.isNotNull(zgsGreendevelop.getYear())) {
            queryWrapper.eq("year", zgsGreendevelop.getYear());
        }
        // 查询对应菜单所关联的数据列表
        if (ObjectUtil.isNotEmpty(zgsGreendevelop.getBuildSecondcategory())) {
            queryWrapper.eq("build_secondcategory", zgsGreendevelop.getBuildSecondcategory());
        }


        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {

            // 住建厅
            // if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_8) {  // 登录角色为 推荐单位（市州）或 金融审批单位
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_8) {  // 登录角色为市州  或 金融审批单位
                String areacode = sysUser.getAreacode();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.like("areacode", areacode);

                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {  // 市州单位
                        queryWrapper.ne("projectstage_state", GlobalConstants.SHENHE_STATUS0);
                    } else {  // 金融审批单位
                        queryWrapper.and(qwp2 -> {
                            // qwp2.isNotNull("finishdate");
                            qwp2.or(f2 -> {
                                f2.ne("projectstage_state", GlobalConstants.SHENHE_STATUS0).ne("projectstage_state", GlobalConstants.SHENHE_STATUS1).ne("projectstage_state", GlobalConstants.SHENHE_STATUS3)
                                        .ne("projectstage_state", GlobalConstants.SHENHE_STATUS13);
                            });
                        });
                    }
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {   // 区县
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            }
           /*else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {   // 单位/个人
               queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
           }*/
            /*if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_8) {  // 登录角色为市州  或 金融审批单位
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectunitmember, ValidateEncryptEntityUtil.isDecrypt);
                String enterprisename = zgsProjectunitmember.getEnterprisename();  // 排查发现注册时在ZgsProjectunitmember 表中存储的单位名称字段是：setHigherupunit，故此处查询将原有的zgsProjectunitmember.getEnterprisename() 替换为 zgsProjectunitmember.getHigherupunit()   rxl 20231221
                String higherupunit = zgsProjectunitmember.getHigherupunit();

                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                if (StrUtil.isNotBlank(enterprisename)) {
                    wrapper.eq("higherupunit", enterprisename);
                } else {
                    wrapper.eq("higherupunit", higherupunit);
                }

                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {  // 推荐单位
                        queryWrapper.ne("projectstage_state", GlobalConstants.SHENHE_STATUS0);
                    } else {  // 金融审批单位
                        queryWrapper.and(qwp2 -> {
                            // qwp2.isNotNull("finishdate");
                            qwp2.or(f2 -> {
                                f2.ne("projectstage_state", GlobalConstants.SHENHE_STATUS0).ne("projectstage_state", GlobalConstants.SHENHE_STATUS1).ne("projectstage_state", GlobalConstants.SHENHE_STATUS3)
                                        .ne("projectstage_state", GlobalConstants.SHENHE_STATUS13);
                            });
                        });
                    }
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {   // 单位/个人
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            }*/
        } else {
            queryWrapper.isNotNull("projectstage_state");
            if (StringUtils.isEmpty(zgsGreendevelop.getProjectstageState())) {
                queryWrapper.and(qwp2 -> {
                    // qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("projectstage_state", GlobalConstants.SHENHE_STATUS0).ne("projectstage_state", GlobalConstants.SHENHE_STATUS1).ne("projectstage_state", GlobalConstants.SHENHE_STATUS3).ne("projectstage_state", GlobalConstants.SHENHE_STATUS4)
                                .ne("projectstage_state", GlobalConstants.SHENHE_STATUS13).ne("projectstage_state", GlobalConstants.SHENHE_STATUS19);
                    });
                });
            }
        }

        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        queryWrapper.last("ORDER BY createdate DESC");

        IPage<ZgsGreendevelop> pageList = null;
        Page<ZgsGreendevelop> page = new Page<ZgsGreendevelop>(pageNo, pageSize);

         pageList = zgsGreendevelopService.page(page, queryWrapper);
        // 查询子表 zgs_greendevelop_detail数据
        if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
            for (int i = 0; i < pageList.getRecords().size(); i++) {
                ZgsGreendevelop zgsGreendevelopEntity = pageList.getRecords().get(i);
                pageList.getRecords().set(i, ValidateEncryptEntityUtil.validateDecryptObject(zgsGreendevelopEntity, ValidateEncryptEntityUtil.isDecrypt));

                QueryWrapper<ZgsGreendevelopDetail> detailQueryWrapper = new QueryWrapper<>();
                detailQueryWrapper.eq("greenprojectkey", zgsGreendevelopEntity.getId());
                List<ZgsGreendevelopDetail> greendevelopDetail = zgsGreendevelopDetailService.list(detailQueryWrapper);
                if (greendevelopDetail.size() > 0) {
                    greendevelopDetail.set(0,ValidateEncryptEntityUtil.validateDecryptObject(greendevelopDetail.get(0), ValidateEncryptEntityUtil.isDecrypt));
                    zgsGreendevelopEntity.setZgsGreendevelopDetails(greendevelopDetail);
                }

            }

        }
        return Result.OK(pageList);
    }


    /**
     * @describe: 项目进度及项目进度详情转化
     * @author: renxiaoliang
     * @date: 2023/12/25 10:38
     */
    private void projectStageConvert(ZgsGreendevelop greendevelopEntity) {
        String projectStage = greendevelopEntity.getProjectstage();  // 项目进度   designStage:设计咨询阶段,  constructStage:施工阶段,  completeStage:竣工运营阶段
        String projectStageDetail = greendevelopEntity.getProjectstageDetails();  // 项目进度详情 1:立项备案  2:可行性研究  3:可研批复 4:建设用地规划许可  5:建设工程规划许可
        // 6:初步设计阶段  7:初步设计批复  8:施工图设计  9:施工图审查合格  10:施工许可  11:开工建设  12:地基基础阶段  13:主体结构阶段  14:装饰装修阶段  15:设备安装阶段  16:设备调试阶段  17:竣工验收  18:运营阶段
        if (StrUtil.isNotBlank(projectStage) && projectStage.equals("designStage")) {
            // greendevelopEntity.setProjectstage("设计咨询阶段");
            if (StrUtil.isNotBlank(projectStageDetail)) {
                switch (projectStageDetail) {
                    case "1":
                        greendevelopEntity.setProjectstageDetails("立项备案");
                        break;
                    case "2":
                        greendevelopEntity.setProjectstageDetails("可行性研究");
                        break;
                    case "3":
                        greendevelopEntity.setProjectstageDetails("可研批复");
                        break;
                    case "4":
                        greendevelopEntity.setProjectstageDetails("建设用地规划许可");
                        break;
                    case "5":
                        greendevelopEntity.setProjectstageDetails("建设工程规划许可");
                        break;
                    case "6":
                        greendevelopEntity.setProjectstageDetails("初步设计阶段");
                        break;
                    case "7":
                        greendevelopEntity.setProjectstageDetails("初步设计批复");
                        break;
                    case "8":
                        greendevelopEntity.setProjectstageDetails("施工图设计");
                        break;
                    case "9":
                        greendevelopEntity.setProjectstageDetails("施工图审查合格");
                        break;
                    default:
                        break;
                }
            }
        } else if (StrUtil.isNotBlank(projectStage) && projectStage.equals("constructStage")) {
            // greendevelopEntity.setProjectstage("施工阶段");
            if (StrUtil.isNotBlank(projectStageDetail)) {
                switch (projectStageDetail) {
                    case "10":
                        greendevelopEntity.setProjectstageDetails("施工许可");
                        break;
                    case "11":
                        greendevelopEntity.setProjectstageDetails("开工建设");
                        break;
                    case "12":
                        greendevelopEntity.setProjectstageDetails("地基基础阶段");
                        break;
                    case "13":
                        greendevelopEntity.setProjectstageDetails("主体结构阶段");
                        break;
                    case "14":
                        greendevelopEntity.setProjectstageDetails("装饰装修阶段");
                        break;
                    case "15":
                        greendevelopEntity.setProjectstageDetails("设备安装阶段");
                        break;
                    case "16":
                        greendevelopEntity.setProjectstageDetails("设备调试阶段");
                        break;
                    default:
                        break;
                }
            }
        } else if (StrUtil.isNotBlank(projectStage) && projectStage.equals("completeStage")) {
            // greendevelopEntity.setProjectstage("竣工运营阶段");
            if (StrUtil.isNotBlank(projectStageDetail)) {
                switch (projectStageDetail) {
                    case "17":
                        greendevelopEntity.setProjectstageDetails("竣工验收");
                        break;
                    case "18":
                        greendevelopEntity.setProjectstageDetails("运营阶段");
                        break;
                    default:
                        break;
                }
            }
        }
    }





    /**
     * @describe: 绿色发展项目新增
     * @author: renxiaoliang
     * @date: 2023/12/14 11:24
     */
    @AutoLog(value = "绿色发展项目库-添加")
    @ApiOperation(value = "绿色发展项目库-添加", notes = "绿色发展项目库-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsGreendevelop zgsGreendevelop) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
         String id = UUID.randomUUID().toString();
        zgsGreendevelop.setId(id);
        zgsGreendevelop.setEnterpriseguid(sysUser.getEnterpriseguid());
        if (GlobalConstants.handleSubmit.equals(zgsGreendevelop.getStatus())) {  // 上报状态：0 保存， 1 保存并上报
            //保存并上报
            zgsGreendevelop.setApplydate(new Date());
            zgsGreendevelop.setProjectstageState(GlobalConstants.SHENHE_STATUS1);  //待审核

        } else {
            zgsGreendevelop.setProjectstageState(GlobalConstants.SHENHE_STATUS0);  // 未上报
        }
        String secondBuildType = zgsGreendevelop.getBuildSecondcategory();
        if (StrUtil.isNotBlank(secondBuildType) && secondBuildType.equals("energyAndgreenTransform")) {
            String[] buildtypeArray = zgsGreendevelop.getBuildtypeArray();
            if (buildtypeArray.length > 0) {
                StringBuilder sb = new StringBuilder();
                if (buildtypeArray.length == 2) {
                    sb.append(buildtypeArray[0]).append(",").append(buildtypeArray[1]);
                } else {
                    sb.append(buildtypeArray[0]);
                }
                zgsGreendevelop.setBuildtype(sb.toString());
            }
        }
        zgsGreendevelop.setYear(Integer.toString(LocalDate.now().getYear()));
        zgsGreendevelop.setCreateaccountname(sysUser.getUsername());
        zgsGreendevelop.setCreateusername(sysUser.getRealname());
        zgsGreendevelop.setCreatedate(new Date());
        zgsGreendevelopService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsGreendevelop, ValidateEncryptEntityUtil.isEncrypt));

        // 绿色发展项目详情信息
        List<ZgsGreendevelopDetail> greendevelopDetails = zgsGreendevelop.getZgsGreendevelopDetails();
        if (greendevelopDetails != null && greendevelopDetails.size() > 0) {

            ZgsGreendevelopDetail zgsGreendevelopDetail = greendevelopDetails.get(0);
            zgsGreendevelopDetail.setDetailId(UUID.randomUUID().toString());
            zgsGreendevelopDetail.setGreenprojectkey(id);   // 关联绿色发展业务基础信息表外键
            zgsGreendevelopDetail.setCreateaccountname(sysUser.getUsername());
            zgsGreendevelopDetail.setCreateusername(sysUser.getRealname());
            zgsGreendevelopDetail.setCreatedate(new Date());
            zgsGreendevelopDetailService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsGreendevelopDetail, ValidateEncryptEntityUtil.isEncrypt));
        }
        /*ZgsGreendevelopDetail zgsGreendevelopDetail = new ZgsGreendevelopDetail();
        zgsGreendevelopDetail.setId(UUID.randomUUID().toString());
        zgsGreendevelopDetail.setGreenprojectkey(id);
        zgsGreendevelopDetail.setConstructionScale(zgsGreendevelop.getConstructionScale());  // 建筑规模
        zgsGreendevelopDetail.setInvestMoney(zgsGreendevelop.getInvestMoney());  // 项目投资金额
        zgsGreendevelopDetail.setApplyFinance(zgsGreendevelop.getApplyFinance());  // 申请绿色金融产品
        zgsGreendevelopDetail.setApplyGreenfinanceMoney(zgsGreendevelop.getApplyGreenfinanceMoney()); // 申请绿色金融数额
        zgsGreendevelopDetail.setApplyGreenfinanceUnit(zgsGreendevelop.getApplyGreenfinanceUnit()); // 申请绿色金融主体单位
        zgsGreendevelopDetail.setApplyGreenfinanceLeader(zgsGreendevelop.getApplyGreenfinanceLeader());  // 项目负责人
        zgsGreendevelopDetail.setApplyGreenfinancePhone(zgsGreendevelop.getApplyGreenfinancePhone());  // 联系电话

        zgsGreendevelopDetail.setConstructUnit(zgsGreendevelop.getConstructUnit());  // 建设单位
        zgsGreendevelopDetail.setConstructUnitLeader(zgsGreendevelop.getConstructUnitLeader());  // 项目负责人
        zgsGreendevelopDetail.setConstructUnitPhone(zgsGreendevelop.getConstructUnitPhone());  // 联系电话

        zgsGreendevelopDetail.setDesignUnit(zgsGreendevelop.getDesignUnit());  // 设计单位
        zgsGreendevelopDetail.setDesignUnitLeader(zgsGreendevelop.getDesignUnitLeader());  // 设计项目负责人
        zgsGreendevelopDetail.setDesignUnitPhone(zgsGreendevelop.getDesignUnitPhone());  // 设计单位联系电话

        zgsGreendevelopDetail.setAskUnit(zgsGreendevelop.getAskUnit());  // 咨询单位
        zgsGreendevelopDetail.setAskUnitLeader(zgsGreendevelop.getAskUnitLeader());  // 咨询项目负责人
        zgsGreendevelopDetail.setAskUnitPhone(zgsGreendevelop.getAskUnitPhone());  // 咨询单位联系电话

        zgsGreendevelopDetail.setBuildUnit(zgsGreendevelop.getAskUnit());  // 咨询单位
        zgsGreendevelopDetail.setBuildUnitLeader(zgsGreendevelop.getAskUnitLeader());  // 咨询项目负责人
        zgsGreendevelopDetail.setBuildUnitPhone(zgsGreendevelop.getAskUnitPhone());  // 咨询单位联系电话

        zgsGreendevelopDetail.setOperateUnit(zgsGreendevelop.getAskUnit());  // 咨询单位
        zgsGreendevelopDetail.setOperateUnitLeader(zgsGreendevelop.getOperateUnitLeader());  // 咨询项目负责人
        zgsGreendevelopDetail.setOperateUnitPhone(zgsGreendevelop.getOperateUnitPhone());  // 咨询单位联系电话

        zgsGreendevelopDetail.setServiceUnit(zgsGreendevelop.getServiceUnit());  // 施工单位
        zgsGreendevelopDetail.setServiceUnitLeader(zgsGreendevelop.getServiceUnitLeader());  // 施工项目负责人
        zgsGreendevelopDetail.setServiceUnitPhone(zgsGreendevelop.getServiceUnitPhone());  // 施工单位联系电话

        zgsGreendevelopDetail.setCreateaccountname(sysUser.getUsername());
        zgsGreendevelopDetail.setCreateusername(sysUser.getRealname());
        zgsGreendevelopDetail.setCreatedate(new Date());
        zgsGreendevelopDetailService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsGreendevelopDetail, ValidateEncryptEntityUtil.isEncrypt));*/

        //相关附件
        List<ZgsAttachappendix> attachappendices = zgsGreendevelop.getZgsAttachappendixList();
        if (attachappendices != null && attachappendices.size() > 0) {
            String mattermaterialId = UUID.randomUUID().toString();
            ZgsMattermaterial zgsMattermaterial = new ZgsMattermaterial();
            zgsMattermaterial.setId(mattermaterialId);
            zgsMattermaterial.setBuildguid(id);
            zgsMattermaterial.setMaterialname("绿色发展文件");
            zgsMattermaterial.setIsrequired(BigDecimal.valueOf(1));
            zgsMattermaterial.setMaterialtype("复印件");
            zgsMattermaterial.setOrdernum(BigDecimal.valueOf(1));

            zgsMattermaterialService.save(zgsMattermaterial);
            for (int a4_0 = 0; a4_0 < attachappendices.size(); a4_0++) {
                ZgsAttachappendix zgsAttachappendix = attachappendices.get(a4_0);
                zgsAttachappendix.setId(UUID.randomUUID().toString());                zgsAttachappendix.setRelationid(mattermaterialId);
                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                zgsAttachappendix.setCreatetime(new Date());
                zgsAttachappendix.setAppendixtype(zgsGreendevelop.getBuildSecondcategory());
                zgsAttachappendix.setAppendixsubtype("greenDevelopMaterial");
                zgsAttachappendix.setAppendixname(attachappendices.get(a4_0).getAppendixname());
                zgsAttachappendix.setFilename(attachappendices.get(a4_0).getFilename());
                zgsAttachappendix.setAppendixextension(attachappendices.get(a4_0).getAppendixextension());
                zgsAttachappendix.setAppendixpath(attachappendices.get(a4_0).getAppendixpath());
                zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        return Result.OK("添加成功！");
    }


    /**
     * @describe: 通过id删除
     * @author: renxiaoliang
     * @date: 2023/12/14 16:24
     */
    @AutoLog(value = "绿色发展项目库-通过id删除")
    @ApiOperation(value = "绿色发展项目库-通过id删除", notes = "绿色发展项目库-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        Result result = new Result();
        QueryWrapper<ZgsMattermaterial> queryWrapper = new QueryWrapper<>();
        UpdateWrapper<ZgsAttachappendix> updateWrapperOfdix = new UpdateWrapper();
        UpdateWrapper<ZgsGreendevelopDetail> updateWrapperOfDetail = new UpdateWrapper();
        ZgsGreendevelop zgsGreendevelop = new ZgsGreendevelop();

        updateWrapperOfDetail.eq("greenprojectkey", id);
        updateWrapperOfDetail.set("isdelete","1");
        boolean delDetailBol = zgsGreendevelopDetailService.update(updateWrapperOfDetail);
        //if (delDetailBol) {
            zgsGreendevelop.setId(id);
            zgsGreendevelop.setIsdelete("1");
            zgsGreendevelopService.updateById(zgsGreendevelop);
            // 删除关联附件信息
            queryWrapper.eq("buildguid", id);
            List<ZgsMattermaterial> mattermaterials = zgsMattermaterialService.list(queryWrapper);
            if (ObjectUtil.isNotNull(mattermaterials) && mattermaterials.size() > 0) {  // 若存在附件则删除
                ZgsMattermaterial mattermaterial = mattermaterials.get(0);
                // 删除附件从表信息
                updateWrapperOfdix.eq("relationid", mattermaterial.getId());
                updateWrapperOfdix.set("isdelete", "1");  // 1删除，  0未删除
                boolean bol = zgsAttachappendixService.update(updateWrapperOfdix);
                // if (bol) {
                    zgsMattermaterialService.removeById(id);  // 因原表没有删除标识字段，故此将附件信息从表中直接删除
                /*} else {
                    result.setMessage("删除失败");
                    result.setSuccess(false);
                    return result;
                }*/
            }
            result.setMessage("删除成功");
            result.setSuccess(true);
        /*} else {
            result.setMessage("删除失败");
            result.setSuccess(false);
            return  result;
        }*/

        return result;
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技攻关项目库-批量删除")
    @ApiOperation(value = "建设科技攻关项目库-批量删除", notes = "建设科技攻关项目库-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Result result = new Result();
        if (StrUtil.isNotBlank(ids)) {
            String[] idsArray = ids.split(",");
            for (int i = 0; i < idsArray.length; i++) {
                UpdateWrapper<ZgsGreendevelopDetail> updateWrapperOfDetail = new UpdateWrapper();
                UpdateWrapper<ZgsGreendevelop> updateWrapperOfDevelop = new UpdateWrapper();
                updateWrapperOfDetail.eq("greenprojectkey",idsArray[i]);
                updateWrapperOfDetail.set("isdelete", "1");
                boolean delDetailBol = zgsGreendevelopDetailService.update(updateWrapperOfDetail);  // 删除从表信息
                // if (delDetailBol) {
                    updateWrapperOfDevelop.eq("id",idsArray[i]);
                    updateWrapperOfDevelop.set("isdelete","1");
                    zgsGreendevelopService.update(updateWrapperOfDevelop);

                    // 删除关联附件信息
                    QueryWrapper<ZgsMattermaterial> queryWrapper = new QueryWrapper<>();
                    queryWrapper.eq("buildguid", idsArray[i]);
                    List<ZgsMattermaterial> mattermaterials = zgsMattermaterialService.list(queryWrapper);
                    if (ObjectUtil.isNotNull(mattermaterials) && mattermaterials.size() > 0) {  // 若存在附件则删除
                        ZgsMattermaterial mattermaterial = mattermaterials.get(0);
                        // 删除附件从表信息
                        UpdateWrapper<ZgsAttachappendix> updateWrapperOfdix = new UpdateWrapper();
                        updateWrapperOfdix.eq("relationid", mattermaterial.getId());
                        updateWrapperOfdix.set("isdelete", "1");  // 1删除，  0未删除
                        boolean bol = zgsAttachappendixService.update(updateWrapperOfdix);
                        // if (bol) {
                            zgsMattermaterialService.removeById(mattermaterial.getId());  // 因原表没有删除标识字段，故此将附件信息从表中直接删除
                        /*} else {
                            result.setMessage("删除失败");
                            result.setSuccess(false);
                            return result;
                        }*/
                    }
                /*} else {
                    result.setMessage("删除失败");
                    result.setSuccess(false);
                    return result;
                }*/
            }
            result.setMessage("批量删除成功!");
            result.setSuccess(true);
        }
        return result;
    }


    /**
     * @describe: 绿色发展查询详情
     * @author: renxiaoliang
     * @date: 2023/12/14 16:53
     */
    @AutoLog(value = "绿色发展项目库-通过id查询")
    @ApiOperation(value = "绿色发展项目库-通过id查询", notes = "绿色发展项目库-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsGreendevelop zgsGreendevelop = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsGreendevelop = zgsGreendevelopService.getById(id);
            if (zgsGreendevelop == null) {
                return Result.error("未找到对应数据");
            } else {
                String buildType = zgsGreendevelop.getBuildtype();
                if (StrUtil.isNotBlank(buildType) && zgsGreendevelop.getBuildSecondcategory().equals("energyAndgreenTransform")) {
                    String[] buildTypeArray = buildType.split(",");
                    zgsGreendevelop.setBuildtypeArray(buildTypeArray);
                }
                initProjectSelectById(zgsGreendevelop);
            }
        }
        return Result.OK(zgsGreendevelop);
    }

    /**
     * @describe: 查询绿色发展项目详情信息
     * @author: renxiaoliang
     * @date: 2023/12/14 17:01
     */
    private void initProjectSelectById(ZgsGreendevelop zgsGreendevelop) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = zgsGreendevelop.getEnterpriseguid();
        String greendevelopId = zgsGreendevelop.getId();
        if (StringUtils.isNotEmpty(greendevelopId)) {
            // 项目进度及项目进度详情转化
            projectStageConvert(zgsGreendevelop);
            // 查询绿色发展项目详情信息
            QueryWrapper<ZgsGreendevelopDetail> queryWrapperdetail = new QueryWrapper<>();
            queryWrapperdetail.eq("greenprojectkey", zgsGreendevelop.getId());
            queryWrapperdetail.ne("isdelete", 1);
            List<ZgsGreendevelopDetail> zgsGreendevelopDetailList = zgsGreendevelopDetailService.list(queryWrapperdetail);
            if (ObjectUtil.isNotNull(zgsGreendevelopDetailList) && zgsGreendevelopDetailList.size() > 0) {

                zgsGreendevelop.setZgsGreendevelopDetails(ValidateEncryptEntityUtil.validateDecryptList(zgsGreendevelopDetailList, ValidateEncryptEntityUtil.isDecrypt));
            }
            // 查询项目附件
            QueryWrapper<ZgsMattermaterial> mattermaterialQueryWrapper = new QueryWrapper<>();
            mattermaterialQueryWrapper.eq("buildguid", greendevelopId);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.list(mattermaterialQueryWrapper);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                ZgsMattermaterial mattermaterial = zgsMattermaterialList.get(0);
                if (ObjectUtil.isNotNull(mattermaterial)) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("relationid", mattermaterial.getId());
                    queryWrapper5.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper5), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsGreendevelop.setZgsAttachappendixList(zgsAttachappendixList);
                }
            }
        }
    }


    /**
     * @describe: 绿色发展项目信息编辑保存
     * @author: renxiaoliang
     * @date: 2023/12/15 13:54
     */
    @AutoLog(value = "绿色发展项目库-编辑")
    @ApiOperation(value = "绿色发展项目库-编辑", notes = "绿色发展项目库-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsGreendevelop zgsGreendevelop) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (ObjectUtil.isNotNull(zgsGreendevelop)) {
            if (GlobalConstants.handleSubmit.equals(zgsGreendevelop.getStatus())) {
                //保存并上报
                zgsGreendevelop.setProjectstageState(GlobalConstants.SHENHE_STATUS1); // 待审核
            } else {
                zgsGreendevelop.setProjectstageState(GlobalConstants.SHENHE_STATUS0); //未上报
            }

            String secondBuildType = zgsGreendevelop.getBuildSecondcategory();
            if (StrUtil.isNotBlank(secondBuildType) && secondBuildType.equals("energyAndgreenTransform")) {
                String[] buildtypeArray = zgsGreendevelop.getBuildtypeArray();
                if (buildtypeArray.length > 0) {
                    StringBuilder sb = new StringBuilder();
                    if (buildtypeArray.length == 2) {
                        sb.append(buildtypeArray[0]).append(",").append(buildtypeArray[1]);
                    } else {
                        sb.append(buildtypeArray[0]);
                    }
                    zgsGreendevelop.setBuildtype(sb.toString());
                }
            }
            zgsGreendevelop.setModifyaccountname(sysUser.getUsername());
            zgsGreendevelop.setModifyusername(sysUser.getRealname());
            zgsGreendevelop.setModifydate(new Date());
            zgsGreendevelop.setCreatedate(null);
            zgsGreendevelop.setCreateaccountname(null);
            zgsGreendevelop.setCreateusername(null);
            zgsGreendevelopService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsGreendevelop, ValidateEncryptEntityUtil.isEncrypt));
            // 绿色发展详情
            List<ZgsGreendevelopDetail> zgsGreendevelopDetailList = zgsGreendevelop.getZgsGreendevelopDetails();
            if (zgsGreendevelopDetailList != null && zgsGreendevelopDetailList.size() > 0) {
                for (int a0 = 0; a0 < zgsGreendevelopDetailList.size(); a0++) {
                    ZgsGreendevelopDetail greendevelopDetail = zgsGreendevelopDetailList.get(a0);
                    if (ObjectUtil.isNotNull(greendevelopDetail)) {
                        //编辑
                        greendevelopDetail.setModifyaccountname(sysUser.getUsername());
                        greendevelopDetail.setModifyusername(sysUser.getRealname());
                        greendevelopDetail.setModifydate(new Date());
                        zgsGreendevelopDetailService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(greendevelopDetail, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
            //相关附件
            List<ZgsAttachappendix> zgsAttachappendixList = zgsGreendevelop.getZgsAttachappendixList();
            if (ObjectUtil.isNotNull(zgsAttachappendixList) && zgsAttachappendixList.size() > 0) {
                QueryWrapper<ZgsMattermaterial> mattermaterialQueryWrapper = new QueryWrapper<>();
                mattermaterialQueryWrapper.eq("buildguid",zgsGreendevelop.getId());
                List<ZgsMattermaterial>  mattermaterialList = zgsMattermaterialService.list(mattermaterialQueryWrapper);
                if (ObjectUtil.isNotNull(mattermaterialList) && mattermaterialList.size()  > 0) {
                    ZgsMattermaterial mattermaterial = mattermaterialList.get(0);

                    String mattermaterialId = mattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);

                    for (int i = 0; i < zgsAttachappendixList.size(); i++) {
                        ZgsAttachappendix zgsAttachappendix = new ZgsAttachappendix();

                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(zgsGreendevelop.getBuildSecondcategory());
                        zgsAttachappendix.setAppendixsubtype("greenDevelopMaterial");
                        zgsAttachappendix.setAppendixname(zgsAttachappendixList.get(i).getAppendixname());
                        zgsAttachappendix.setFilename(zgsAttachappendixList.get(i).getFilename());
                        zgsAttachappendix.setAppendixextension(zgsAttachappendixList.get(i).getAppendixextension());
                        zgsAttachappendix.setAppendixpath(zgsAttachappendixList.get(i).getAppendixpath());
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }

            /*List<ZgsAttachappendix> zgsAttachappendixList = zgsGreendevelop.getZgsAttachappendixList();
            if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                for (int a4 = 0; a4 < zgsAttachappendixList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    if (StrUtil.isNotBlank(zgsMattermaterial.getId())) {
                        // 因为只有一种附件类型，故此ZgsMattermaterial表不修改，只对附件内容进行信息再编辑
                        List<ZgsAttachappendix> zgsAttachappendixList = zgsMattermaterial.getZgsAttachappendixList();
                        if (ObjectUtil.isNotNull(zgsAttachappendixList) && zgsAttachappendixList.size() > 0) {
                            for (int i = 0; i < zgsAttachappendixList.size(); i++) {
                                ZgsAttachappendix attachappendix = zgsAttachappendixList.get(i);
                                if (StrUtil.isNotBlank(zgsAttachappendixList.get(i).getId())) {
                                    // 若存在原有附件，则进行更新
                                    attachappendix.setModifypersonaccount(sysUser.getUsername());
                                    attachappendix.setModifypersonname(sysUser.getRealname());
                                    attachappendix.setModifytime(new Date());
                                    zgsAttachappendixService.updateById(attachappendix);
                                } else {  // 否则进行附件新增
                                    attachappendix.setId(UUID.randomUUID().toString());
                                    attachappendix.setRelationid(zgsMattermaterial.getId());
                                    attachappendix.setCreatepersonaccount(sysUser.getUsername());
                                    attachappendix.setCreatepersonname(sysUser.getRealname());
                                    attachappendix.setCreatetime(new Date());
                                    attachappendix.setAppendixsubtype("ProofMaterial");


                                }

                            }
                        }

                    } else {  // 针对未上报且未上传附件时，若再次编辑上传附件时 走新增操作
                        String mattermaterialId = UUID.randomUUID().toString();
                        zgsMattermaterial.setId(mattermaterialId);
                        zgsMattermaterial.setBuildguid(zgsGreendevelop.getId());
                        zgsMattermaterialService.save(zgsMattermaterial);
                        if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                            for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                                ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(zgsGreendevelop.getBuildSecondcategory());
                                zgsAttachappendix.setAppendixsubtype("greenDevelopMaterial");
                                zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                            }
                        }
                    }
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                                zgsAttachappendixService.updateById(zgsAttachappendix);
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(zgsGreendevelop.getBuildSecondcategory());
                                zgsAttachappendix.setAppendixsubtype("greenDevelopMaterial");
                                zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                            }
                        }
                    }
                }
            }*/
        }
        return Result.OK("编辑成功!");
    }




    @AutoLog(value = "绿色发展项目初审-审批")
    @ApiOperation(value = "绿色发展项目初审-审批", notes = "绿色发展项目初审-审批")
    @PostMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsGreendevelop zgsGreendevelop) {
        UpdateWrapper<ZgsGreendevelop> updateWrapper = new UpdateWrapper();
        Result result = new Result();
        if (StrUtil.isNotBlank(zgsGreendevelop.getSpStatus()) && zgsGreendevelop.getSpStatus().equals("1")) {
            updateWrapper.eq("id",zgsGreendevelop.getId());
            updateWrapper.set("projectstage_state",GlobalConstants.SHENHE_STATUS4); // 初审通过
            updateWrapper.set("auditopinion",zgsGreendevelop.getAuditopinion()); // 初审审批意见

        } else if (StrUtil.isNotBlank(zgsGreendevelop.getSpStatus()) && zgsGreendevelop.getSpStatus().equals("0")){
            updateWrapper.eq("id",zgsGreendevelop.getId());
            updateWrapper.set("projectstage_state",GlobalConstants.SHENHE_STATUS3); // 初审通过
            updateWrapper.set("auditopinion",zgsGreendevelop.getAuditopinion()); // 初审审批意见

        } else if (StrUtil.isNotBlank(zgsGreendevelop.getFinanceState()) && zgsGreendevelop.getFinanceState().equals("1")){
            updateWrapper.eq("id",zgsGreendevelop.getId());
            updateWrapper.set("finance_state","1");
            updateWrapper.set("projectstage_state",GlobalConstants.SHENHE_STATUS18); // 金融审批通过
            updateWrapper.set("finance_opinion",zgsGreendevelop.getAuditopinion()); // 金融审批意见

        } else if (StrUtil.isNotBlank(zgsGreendevelop.getFinanceState()) && zgsGreendevelop.getFinanceState().equals("0")){
            updateWrapper.eq("id",zgsGreendevelop.getId());
            updateWrapper.set("finance_state","0");
            updateWrapper.set("projectstage_state",GlobalConstants.SHENHE_STATUS19); // 金融审批退回
            updateWrapper.set("finance_opinion",zgsGreendevelop.getAuditopinion()); // 金融审批意见

        }
        if (zgsGreendevelopService.update(updateWrapper)) {
            result.setSuccess(true);
            result.setMessage("审批成功");
        } else {
            result.setSuccess(false);
            result.setMessage("审批失败");
        }

        return result;
    }




}
