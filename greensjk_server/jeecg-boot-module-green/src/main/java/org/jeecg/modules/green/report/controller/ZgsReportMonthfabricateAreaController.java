package org.jeecg.modules.green.report.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.common.service.IZgsCommonService;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: zgs_report_monthfabricate_area
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Api(tags = "装配式(区县)")
@RestController
@RequestMapping("/report/zgsReportMonthfabricateArea")
@Slf4j
public class ZgsReportMonthfabricateAreaController extends JeecgController<ZgsReportMonthfabricateArea, IZgsReportMonthfabricateAreaService> {
    @Autowired
    private IZgsReportMonthfabricateAreaService zgsReportMonthfabricateAreaService;

    @Autowired
    private IZgsReportMonthfabrAreadetailService zgsReportMonthfabrAreadetailService;

    @Autowired
    private IZgsReportMonthfabrAreaprojectService zgsReportMonthfabrAreaprojectService;

    @Autowired
    private IZgsReportMonthfabrAreascdetailService zgsReportMonthfabrAreascdetailService;

  @Autowired
  private IZgsReportMonthfabrAreacompanyService zgsReportMonthfabrAreacompanyService;

    @Autowired
    private IZgsCommonService zgsCommonService;


    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabricateArea
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式(区县)-分页列表查询")
    @ApiOperation(value = "装配式(区县)-分页列表查询", notes = "装配式(区县)-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabricateArea zgsReportMonthfabricateArea,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "prossType", required = false) Integer prossType,
                                   @RequestParam(name = "applyState", required = false) String applyState,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabricateArea> qW = new QueryWrapper<>();
        if (zgsReportMonthfabricateArea != null) {
            if (zgsReportMonthfabricateArea.getApplystate() != null) {
                //有问题，BigDecimal applystate 需要判空？
                BigDecimal applystate = zgsReportMonthfabricateArea.getApplystate();
                qW.eq("applystate", applystate);
            }
            Integer userType = sysUser.getLoginUserType();
            //6县区，7市区
            if(userType==6){
                qW.eq("areacode",sysUser.getAreacode());
            }else if(userType==7){
                qW.like("areacode",sysUser.getAreacode());
            }
            if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
                String reporttm = zgsReportMonthfabricateArea.getReporttm();
                qW.eq("reporttm", reporttm);
            }
        }
        //  参数列表
        List list = new ArrayList();
        String[] splitArray = null;
        if(applyState != null){
          splitArray = applyState.split(",");
        }
        if (splitArray != null){
          if (splitArray[0] != "" && splitArray[0] != null){
            for (int i = 0;i < splitArray.length;i++){
              list.add(splitArray[i]);
            }
            qW.in("applystate", list);
          }
        }
        qW.ne("isdelete", 1);
        qW.orderByDesc("createtime");

        Page<ZgsReportMonthfabricateArea> page = new Page<ZgsReportMonthfabricateArea>(pageNo, pageSize);
        IPage<ZgsReportMonthfabricateArea> pageList = zgsReportMonthfabricateAreaService.page(page, qW);
        //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
        for (int i = 0;i < pageList.getRecords().size();i++) {
              ZgsReportMonthfabrAreadetail currentDetail = findAreaDetail(pageList.getRecords().get(i), 1);
              if (currentDetail != null) {
                  pageList.getRecords().get(i).setDt1(currentDetail);
              }
              ZgsReportMonthfabrAreadetail currentDetail2 = findAreaDetail(pageList.getRecords().get(i), 2);
              if (currentDetail2 != null) {
                  pageList.getRecords().get(i).setDt2(currentDetail2);
              }
              //  查询产能
              QueryWrapper<ZgsReportMonthfabrAreascdetail> qWdetail = new QueryWrapper<>();
              qWdetail.eq("monthmianguid", pageList.getRecords().get(i).getId());
              qWdetail.ne("isdelete", 1);
              List<ZgsReportMonthfabrAreascdetail> scdetail = zgsReportMonthfabrAreascdetailService.list(qWdetail);
              if (scdetail.size() > 0) {
                pageList.getRecords().get(i).setAreascdetail(scdetail.get(0));
              }
        }
        return Result.OK(pageList);
    }



    /**
     * 添加
     *
     * @param zgsReportMonthfabricateArea
     * @return
     */
    @AutoLog(value = "装配式(区县)-添加")
    @ApiOperation(value = "装配式(区县)-添加", notes = "装配式(区县)-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabricateArea zgsReportMonthfabricateArea) {
//		zgsReportMonthfabricateAreaService.save(zgsReportMonthfabricateArea);
//		return Result.OK("添加成功！");
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //生成主表ID
        String id = UUID.randomUUID().toString();
        zgsReportMonthfabricateArea.setId(id);
        //申报状态  0未上报
        zgsReportMonthfabricateArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
        //行政区域
        zgsReportMonthfabricateArea.setAreacode(sysUser.getAreacode());
        zgsReportMonthfabricateArea.setAreaname(sysUser.getAreaname());
        zgsReportMonthfabricateArea.setIsdelete(new BigDecimal(0));
        zgsReportMonthfabricateArea.setCreatepersonaccount(sysUser.getUsername());
        zgsReportMonthfabricateArea.setCreatepersonname(sysUser.getRealname());
        zgsReportMonthfabricateArea.setCreatetime(new Date());
        zgsReportMonthfabricateAreaService.save(zgsReportMonthfabricateArea);
        String prosstype = "0";
        //保存各种状态下的明细数据
        //1:开工建设2:已竣工验收
        ZgsReportMonthfabrAreadetail dt1 = zgsReportMonthfabricateArea.getDt1();
        if(dt1!=null){
          prosstype = "1";
          saveAreaDetail(dt1,sysUser,id,prosstype);
        }
        ZgsReportMonthfabrAreadetail dt2 = zgsReportMonthfabricateArea.getDt2();
        if(dt2!=null){
          prosstype = "2";
          saveAreaDetail(dt2,sysUser,id,prosstype);
        }

        return Result.OK("添加成功！");
    }



  //	================== 项目库 申报   begin ==================================================================================================
  /**
   * 分页列表查询
   *
   * @param zgsReportMonthfabricateArea
   * @param pageNo
   * @param pageSize
   * @param req
   * @return
   */
  @AutoLog(value = "装配式(区县)-项目库-分页")
  @ApiOperation(value = "装配式(区县)-项目库-分页", notes = "装配式(区县)-项目库-分页")
  @GetMapping(value = "/queryPageProjectList")
  public Result<?> queryPageProjectList(ZgsReportMonthfabricateArea zgsReportMonthfabricateArea,
                                 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(name = "prossType", required = false) Integer prossType,
                                 @RequestParam(name = "applyState", required = false) String applyState,
                                 HttpServletRequest req) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    QueryWrapper<ZgsReportMonthfabricateArea> qW = new QueryWrapper<>();
    if (zgsReportMonthfabricateArea != null) {
      if (zgsReportMonthfabricateArea.getApplystate() != null) {
        //有问题，BigDecimal applystate 需要判空？
        BigDecimal applystate = zgsReportMonthfabricateArea.getApplystate();
        qW.eq("applystate", applystate);
      }
      Integer userType = sysUser.getLoginUserType();
      //6县区，7市区
      if(userType==6){
        qW.eq("areacode",sysUser.getAreacode());
      }else if(userType==7){
        qW.like("areacode",sysUser.getAreacode());
      }
      if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
        String reporttm = zgsReportMonthfabricateArea.getReporttm();
        qW.eq("reporttm", reporttm);
      }
    }
    //  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
    if(applyState != null){
      splitArray = applyState.split(",");
    }
    if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
    qW.ne("isdelete", 1);
    qW.orderByDesc("createtime");
    Page<ZgsReportMonthfabricateArea> page = new Page<ZgsReportMonthfabricateArea>(pageNo, pageSize);
    IPage<ZgsReportMonthfabricateArea> pageList = zgsReportMonthfabricateAreaService.page(page, qW);
    for (int i = 0;i < pageList.getRecords().size();i++) {
            ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(pageList.getRecords().get(i), 1);
            if (currentProjectDetail != null) {
              pageList.getRecords().get(i).setDtproject1(currentProjectDetail);
            }
            ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(pageList.getRecords().get(i), 2);
            if (currentProjectDetail2 != null) {
              pageList.getRecords().get(i).setDtproject2(currentProjectDetail2);
            }
            //  查询产能
            QueryWrapper<ZgsReportMonthfabrAreacompany> qWdetail = new QueryWrapper<>();
            qWdetail.eq("monthmianguid", pageList.getRecords().get(i).getId());
            qWdetail.ne("isdelete", 1);
            List<ZgsReportMonthfabrAreacompany> scdetail = zgsReportMonthfabrAreacompanyService.list(qWdetail);
            if (scdetail.size() > 0) {
              pageList.getRecords().get(i).setDtcompany(scdetail.get(0));
            }
    }
    return Result.OK(pageList);
  }


  /**
   * 通过id查询
   *
   * @param id
   * @return
   */
  @AutoLog(value = "装配式(市州)-项目库-单位库-通过id查询")
  @ApiOperation(value = "装配式(市州)-项目库-单位库-通过id查询", notes = "装配式(市州)-项目库-单位库-通过id查询")
  @GetMapping(value = "/queryProjectById")
  public Result<?> queryProjectById(@RequestParam(name = "id", required = true) String id) {
    ZgsReportMonthfabricateArea zgsReportMonthfabricateArea = zgsReportMonthfabricateAreaService.getById(id);
    if (zgsReportMonthfabricateArea == null) {
      return Result.error("未找到对应数据");
    } else {
      ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(zgsReportMonthfabricateArea, 1);
      if (currentProjectDetail != null) {
        zgsReportMonthfabricateArea.setDtproject1(currentProjectDetail);
      }
      ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(zgsReportMonthfabricateArea, 2);
      if (currentProjectDetail2 != null) {
        zgsReportMonthfabricateArea.setDtproject2(currentProjectDetail2);
      }
      //  查询产能
      QueryWrapper<ZgsReportMonthfabrAreacompany> qWdetail = new QueryWrapper<>();
      qWdetail.eq("monthmianguid", zgsReportMonthfabricateArea.getId());
      qWdetail.ne("isdelete", 1);
      List<ZgsReportMonthfabrAreacompany> scdetail = zgsReportMonthfabrAreacompanyService.list(qWdetail);
      if (scdetail.size() > 0) {
        zgsReportMonthfabricateArea.setDtcompany(scdetail.get(0));
      }
    }
    return Result.OK(zgsReportMonthfabricateArea);
  }


  /**
   * 数据汇总
   *
   * @param zgsReportMonthfabricateArea
   * @param pageNo
   * @param pageSize
   * @param req
   * @return sysCategoryService queryCategoryByCode
   */
  @AutoLog(value = "装配式（区县）- 项目库")
  @ApiOperation(value = "装配式（区县）- 项目库", notes = "装配式（区县）- 项目库")
  @GetMapping(value = "/queryProjectList")
  public Result<?> queryProjectList(ZgsReportMonthfabricateArea zgsReportMonthfabricateArea,
                                 @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                 @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                 @RequestParam(name = "prossType", required = false) Integer prossType,
                                 @RequestParam(name = "applyState", required = false) String applyState,
                                 HttpServletRequest req) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    QueryWrapper<ZgsReportMonthfabricateArea> qW = new QueryWrapper<>();
    if (zgsReportMonthfabricateArea != null) {
      if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
        String reporttm = zgsReportMonthfabricateArea.getReporttm();
        qW.eq("reporttm", reporttm);
      }
    }

    qW.ne("isdelete", 1);
    qW.orderByDesc("createtime");

    //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
    //  1、查询本月所有 地区的主表信息；2、根据主表信息，查询出项目库中的信息，3、将所有项目库中的信息进行累计求和；4、计算所有从1月份到本月的所有数据和

    QueryWrapper<ZgsReportMonthfabrAreaproject> qWdetail = new QueryWrapper<>();
    if (zgsReportMonthfabricateArea != null) {
      if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
        String reporttm = zgsReportMonthfabricateArea.getReporttm();
        qWdetail.eq("reporttm", reporttm);
      }
    }
    qWdetail.eq("projecttype", 1);
    List<ZgsReportMonthfabrAreaproject> areaprojectsList = zgsReportMonthfabrAreaprojectService.list(qWdetail);

//    Map<String, List<ZgsReportMonthfabrAreaproject>> collect = areaprojectsList.stream().collect(
//          Collectors.groupingBy(ZgsReportMonthfabrAreaproject::getAreacode));
//    for(String key:collect.keySet()){
//        System.out.println(key+":" +collect.get(key).size());
//        System.out.println(collect.get(key));
//    }

    if (areaprojectsList.size() > 0) {
      for (int i = 0;i < areaprojectsList.size();i++) {
        if (i > 0){
          if (areaprojectsList.get(i).getJngzjzmj() != null && areaprojectsList.get(i-1).getJngzjzmj() != null){
            areaprojectsList.get(i).setJngzjzmj(areaprojectsList.get(i).getJngzjzmj().add(areaprojectsList.get(i-1).getJngzjzmj()));
          }
          if (areaprojectsList.get(i).getQzxzzmj() != null  && areaprojectsList.get(i-1).getQzxzzmj() != null){
            areaprojectsList.get(i).setQzxzzmj(areaprojectsList.get(i).getQzxzzmj().add(areaprojectsList.get(i-1).getQzxzzmj()));
          }
          if (areaprojectsList.get(i).getZpszxzzjzmj() != null  && areaprojectsList.get(i-1).getZpszxzzjzmj() != null){
            areaprojectsList.get(i).setZpszxzzjzmj(areaprojectsList.get(i).getZpszxzzjzmj().add(areaprojectsList.get(i-1).getZpszxzzjzmj()));
          }
        }
        if (areaprojectsList.get(i).getAreacode() != null){
          String name = zgsCommonService.queryAreaTextByCode(areaprojectsList.get(i).getAreacode()); //  区域名
          areaprojectsList.get(i).setAreacode(name);
        }
      }
    }


//    Page<ZgsReportMonthfabricateArea> page = new Page<ZgsReportMonthfabricateArea>(pageNo, pageSize);
//    IPage<ZgsReportMonthfabricateArea> pageList = zgsReportMonthfabricateAreaService.page(page, qW);
//    for (int i = 0;i < pageList.getRecords().size();i++) {
//            ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(pageList.getRecords().get(i), 1);
//            if (currentProjectDetail != null) {
//              pageList.getRecords().get(i).setDtproject1(currentProjectDetail);
//              ZgsReportMonthfabrAreaproject temporaryProject1 = pageList.getRecords().get(i).getDtproject1();
//              pageList.getRecords().get(i).getDtproject1().setJngzjzmj(temporaryProject1.getJngzjzmj().add(currentProjectDetail.getJngzjzmj()));
//              pageList.getRecords().get(i).getDtproject1().setQzxzzmj(temporaryProject1.getQzxzzmj().add(currentProjectDetail.getQzxzzmj()));
//              pageList.getRecords().get(i).getDtproject1().setZpszxzzjzmj(temporaryProject1.getZpszxzzjzmj().add(currentProjectDetail.getZpszxzzjzmj()));
//            }
//            ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(pageList.getRecords().get(i), 2);
//            if (currentProjectDetail2 != null) {
//              pageList.getRecords().get(i).setDtproject2(currentProjectDetail2);
//              ZgsReportMonthfabrAreaproject temporaryProject2 = pageList.getRecords().get(i).getDtproject2();
//              pageList.getRecords().get(i).getDtproject2().setJngzjzmj(temporaryProject2.getJngzjzmj().add(currentProjectDetail.getJngzjzmj()));
//              pageList.getRecords().get(i).getDtproject2().setQzxzzmj(temporaryProject2.getQzxzzmj().add(currentProjectDetail.getQzxzzmj()));
//              pageList.getRecords().get(i).getDtproject2().setZpszxzzjzmj(temporaryProject2.getZpszxzzjzmj().add(currentProjectDetail.getZpszxzzjzmj()));
//            }
//
//    }
    return Result.OK(areaprojectsList);
  }


  //查找明细数据
  public ZgsReportMonthfabrAreaproject findAreaProjectDetail(ZgsReportMonthfabricateArea zgsReportMonthfabricateArea, Integer projecttype) {
    QueryWrapper<ZgsReportMonthfabrAreaproject> qWdetail = new QueryWrapper<>();
    qWdetail.eq("monthfabrareadetailguid", zgsReportMonthfabricateArea.getId());
    qWdetail.eq("projecttype", projecttype);
    List<ZgsReportMonthfabrAreaproject> areadetail = zgsReportMonthfabrAreaprojectService.list(qWdetail);
    if (areadetail.size() > 0){
      return areadetail.get(0);
    }
    return null;
  }



  /**
   * 添加
   *
   * @param zgsReportMonthfabricateArea
   * @return
   */
  @AutoLog(value = "装配式(区县)-项目库-添加")
  @ApiOperation(value = "装配式(区县)-项目库-添加", notes = "装配式(区县)-项目库-添加")
  @PostMapping(value = "/addProject")
  public Result<?> addProject(@RequestBody ZgsReportMonthfabricateArea zgsReportMonthfabricateArea) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    //生成主表ID
    String id = UUID.randomUUID().toString();
    zgsReportMonthfabricateArea.setId(id);
    //申报状态  0未上报
    zgsReportMonthfabricateArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
    //行政区域
    zgsReportMonthfabricateArea.setAreacode(sysUser.getAreacode());
    zgsReportMonthfabricateArea.setAreaname(sysUser.getAreaname());
    zgsReportMonthfabricateArea.setIsdelete(new BigDecimal(0));
    zgsReportMonthfabricateArea.setCreatepersonaccount(sysUser.getUsername());
    zgsReportMonthfabricateArea.setCreatepersonname(sysUser.getRealname());
    zgsReportMonthfabricateArea.setCreatetime(new Date());
    zgsReportMonthfabricateAreaService.save(zgsReportMonthfabricateArea);
    String prosstype = "0";
    //保存各种状态下的明细数据
    //1:开工建设2:已竣工验收
    ZgsReportMonthfabrAreaproject dtproject1 = zgsReportMonthfabricateArea.getDtproject1();
    if(dtproject1!=null){
      prosstype = "1";
      dtproject1.setReporttm(zgsReportMonthfabricateArea.getReporttm());
      saveAreaProjectDetail(dtproject1,sysUser,id,prosstype);
    }
    ZgsReportMonthfabrAreaproject dtproject2 = zgsReportMonthfabricateArea.getDtproject2();
    if(dtproject2!=null){
      prosstype = "2";
      dtproject2.setReporttm(zgsReportMonthfabricateArea.getReporttm());
      saveAreaProjectDetail(dtproject2,sysUser,id,prosstype);
    }
    return Result.OK("添加成功！");
  }


  /**
   * 添加
   *
   * @param zgsReportMonthfabricateArea
   * @return
   */
  @AutoLog(value = "装配式(区县)-单位库 - 添加")
  @ApiOperation(value = "装配式(区县)-单位库 -添加", notes = "装配式(区县)-单位库 - 添加")
  @PostMapping(value = "/addCompany")
  public Result<?> addCompany(@RequestBody ZgsReportMonthfabricateArea zgsReportMonthfabricateArea) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    //生成主表ID
    String id = UUID.randomUUID().toString();
    zgsReportMonthfabricateArea.setId(id);
    //申报状态  0未上报
    zgsReportMonthfabricateArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
    //行政区域
    zgsReportMonthfabricateArea.setAreacode(sysUser.getAreacode());
    zgsReportMonthfabricateArea.setAreaname(sysUser.getAreaname());
    zgsReportMonthfabricateArea.setIsdelete(new BigDecimal(0));
    zgsReportMonthfabricateArea.setCreatepersonaccount(sysUser.getUsername());
    zgsReportMonthfabricateArea.setCreatepersonname(sysUser.getRealname());
    zgsReportMonthfabricateArea.setCreatetime(new Date());
    zgsReportMonthfabricateAreaService.save(zgsReportMonthfabricateArea);
    String prosstype = "0";
    //保存各种状态下的明细数据
    ZgsReportMonthfabrAreacompany dtproject1 = zgsReportMonthfabricateArea.getDtcompany();
    if(dtproject1!=null){
      saveAreaCompanyDetail(dtproject1,sysUser,id,prosstype);
    }
    return Result.OK("添加成功！");
  }



  //保存 项目库 明细
  public void saveAreaProjectDetail(ZgsReportMonthfabrAreaproject areaDetail,LoginUser sysUser,String id, String prosstype){
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthfabrareadetailguid(id);
    /**1:开工建设2:已竣工验收*/
    areaDetail.setProjecttype(prosstype);
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabrAreaprojectService.save(areaDetail);
  }


  //保存 项目库（生产节能） 明细
  public void saveAreaCompanyDetail(ZgsReportMonthfabrAreacompany areaDetail,LoginUser sysUser,String id, String prosstype){
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthmianguid(id);
//    areaDetail.setProjecttype(prosstype);
    areaDetail.setCountyorcity("1");    //  （1、区县；2、市州）
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabrAreacompanyService.save(areaDetail);
  }
//	================== 项目库 申报   end ==================================================================================================






  //保存 生产节能 单位库 明细
  public void saveAreaProjectScDetail(ZgsReportMonthfabrAreascdetail scdetail, LoginUser sysUser, String id) {
    if (StringUtils.isNotEmpty(scdetail.getId())) {
      //编辑
      scdetail.setModifypersonaccount(sysUser.getUsername());
      scdetail.setModifypersonname(sysUser.getRealname());
      scdetail.setModifytime(new Date());
      zgsReportMonthfabrAreascdetailService.updateById(scdetail);
    } else {
      //新增
      saveScDetail(scdetail, sysUser, id);
    }
  }



    //保存明细
    public void saveAreaDetail(ZgsReportMonthfabrAreadetail areaDetail,LoginUser sysUser,String id, String prosstype){
        //生成明细表ID
        String id2 = UUID.randomUUID().toString();
        areaDetail.setId(id2);
        //关联主表ID
        areaDetail.setMonthmianguid(id);
        /**0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行*/
        areaDetail.setProsstype(new BigDecimal(prosstype));
        areaDetail.setIsdelete(new BigDecimal(0));
        areaDetail.setCreatepersonaccount(sysUser.getUsername());
        areaDetail.setCreatepersonname(sysUser.getRealname());
        areaDetail.setCreatetime(new Date());
        zgsReportMonthfabrAreadetailService.save(areaDetail);
    }


    //保存明细
    public void saveAreaDetail(ZgsReportMonthfabrAreadetail areaDetail, LoginUser sysUser, String id) {
        //生成明细表ID
        String id2 = UUID.randomUUID().toString();
        areaDetail.setId(id2);
        //关联主表ID
        areaDetail.setMonthmianguid(id);
        areaDetail.setCreatepersonaccount(sysUser.getUsername());
        areaDetail.setCreatepersonname(sysUser.getRealname());
        areaDetail.setCreatetime(new Date());
        zgsReportMonthfabrAreadetailService.save(areaDetail);

    }

    //保存产能明细
    public void saveScDetail(ZgsReportMonthfabrAreascdetail areascdetail, LoginUser sysUser, String id) {
        //生成明细表ID
        String id2 = UUID.randomUUID().toString();
        areascdetail.setId(id2);
        //关联主表ID
        areascdetail.setMonthmianguid(id);
        areascdetail.setCreatepersonaccount(sysUser.getUsername());
        areascdetail.setCreatepersonname(sysUser.getRealname());
        areascdetail.setCreatetime(new Date());
        zgsReportMonthfabrAreascdetailService.save(areascdetail);

    }

    /**
     * 编辑
     *
     * @param zgsReportMonthfabricateArea
     * @return
     */
        @AutoLog(value = "装配式(区县)-编辑")
    @ApiOperation(value = "装配式(区县)-编辑", notes = "装配式(区县)-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabricateArea zgsReportMonthfabricateArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsReportMonthfabricateArea != null && StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getId())) {
            String id = zgsReportMonthfabricateArea.getId();
            if (GlobalConstants.apply_state1.equals(zgsReportMonthfabricateArea.getApplystate())) {
                //保存并上报
                zgsReportMonthfabricateArea.setApplydate(new Date());
            }
            if (GlobalConstants.apply_state3.equals(zgsReportMonthfabricateArea.getApplystate())) {
                //退回
                zgsReportMonthfabricateArea.setBackrdate(new Date());
            }
            zgsReportMonthfabricateArea.setModifypersonaccount(sysUser.getUsername());
            zgsReportMonthfabricateArea.setModifypersonname(sysUser.getRealname());
            zgsReportMonthfabricateArea.setModifytime(new Date());
            zgsReportMonthfabricateAreaService.updateById(zgsReportMonthfabricateArea);

            //更新明细表数据
            ZgsReportMonthfabrAreadetail dt1 = zgsReportMonthfabricateArea.getDt1();
            ZgsReportMonthfabrAreadetail dt2 = zgsReportMonthfabricateArea.getDt2();

            if (dt1 != null) {
                updateDetail(dt1, sysUser, id);
            } else {
                deleteDetail(zgsReportMonthfabricateArea, GlobalConstants.prosstype1);
            }
            if (dt2 != null) {
                updateDetail(dt2, sysUser, id);
            } else {
                deleteDetail(zgsReportMonthfabricateArea, GlobalConstants.prosstype2);
            }
            ZgsReportMonthfabrAreascdetail scdetail = zgsReportMonthfabricateArea.getAreascdetail();
            if (scdetail != null) {
                updateScDetail(scdetail, sysUser, id);
            } else {
                deleteScDetail(zgsReportMonthfabricateArea);
            }
        }
        return Result.OK("编辑成功!");
    }

    public void updateDetail(ZgsReportMonthfabrAreadetail areaDetail, LoginUser sysUser, String id) {
        if (StringUtils.isNotEmpty(areaDetail.getId())) {
            //编辑
            areaDetail.setModifypersonaccount(sysUser.getUsername());
            areaDetail.setModifypersonname(sysUser.getRealname());
            areaDetail.setModifytime(new Date());
            zgsReportMonthfabrAreadetailService.updateById(areaDetail);
        } else {
            //新增
            saveAreaDetail(areaDetail, sysUser, id);
        }
    }

    public void updateScDetail(ZgsReportMonthfabrAreascdetail scdetail, LoginUser sysUser, String id) {
        if (StringUtils.isNotEmpty(scdetail.getId())) {
            //编辑
            scdetail.setModifypersonaccount(sysUser.getUsername());
            scdetail.setModifypersonname(sysUser.getRealname());
            scdetail.setModifytime(new Date());
            zgsReportMonthfabrAreascdetailService.updateById(scdetail);
        } else {
            //新增
            saveScDetail(scdetail, sysUser, id);
        }
    }

    public void deleteDetail(ZgsReportMonthfabricateArea areaDetail, Integer prosstype) {
        QueryWrapper<ZgsReportMonthfabrAreadetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", areaDetail.getId());
        qWdetail.ne("isdelete", 1);
        qWdetail.eq("prosstype", prosstype);
        zgsReportMonthfabrAreadetailService.remove(qWdetail);
    }

    public void deleteScDetail(ZgsReportMonthfabricateArea scdetail) {
        QueryWrapper<ZgsReportMonthfabrAreascdetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", scdetail.getId());
        qWdetail.ne("isdelete", 1);
        zgsReportMonthfabrAreascdetailService.remove(qWdetail);
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式(区县)-通过id删除")
    @ApiOperation(value = "装配式(区县)-通过id删除", notes = "装配式(区县)-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportMonthfabricateAreaService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式(区县)-批量删除")
    @ApiOperation(value = "装配式(区县)-批量删除", notes = "装配式(区县)-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabricateAreaService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }


    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式(区县)-通过id查询")
    @ApiOperation(value = "装配式(区县)-通过id查询", notes = "装配式(区县)-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabricateArea zgsReportMonthfabricateArea = zgsReportMonthfabricateAreaService.getById(id);
        if (zgsReportMonthfabricateArea == null) {
            return Result.error("未找到对应数据");
        } else {
          ZgsReportMonthfabrAreadetail currentDetail = findAreaDetail(zgsReportMonthfabricateArea, 1);
          if (currentDetail != null) {
            zgsReportMonthfabricateArea.setDt1(currentDetail);
          }
          ZgsReportMonthfabrAreadetail currentDetail2 = findAreaDetail(zgsReportMonthfabricateArea, 2);
          if (currentDetail2 != null) {
            zgsReportMonthfabricateArea.setDt2(currentDetail2);
          }
          //  查询产能
          QueryWrapper<ZgsReportMonthfabrAreascdetail> qWdetail = new QueryWrapper<>();
          qWdetail.eq("monthmianguid", zgsReportMonthfabricateArea.getId());
          qWdetail.ne("isdelete", 1);
          List<ZgsReportMonthfabrAreascdetail> scdetail = zgsReportMonthfabrAreascdetailService.list(qWdetail);
          if (scdetail.size() > 0) {
            zgsReportMonthfabricateArea.setAreascdetail(scdetail.get(0));
          }
        }
        return Result.OK(zgsReportMonthfabricateArea);
    }


    //查找明细数据
    public List<ZgsReportMonthfabrAreadetail> findAreaDetailList(ZgsReportMonthfabricateArea zgsReportMonthgreenbuildArea){
        QueryWrapper<ZgsReportMonthfabrAreadetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid",zgsReportMonthgreenbuildArea.getId());
        qWdetail.ne("isdelete",1);
        //		 qWdetail.eq("prosstype",prosstype);
        List<ZgsReportMonthfabrAreadetail> list = zgsReportMonthfabrAreadetailService.list(qWdetail);
        return list;
    }


    //查找明细数据
    public ZgsReportMonthfabrAreadetail findAreaDetail(ZgsReportMonthfabricateArea zgsReportMonthfabricateArea, Integer prosstype) {
        QueryWrapper<ZgsReportMonthfabrAreadetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", zgsReportMonthfabricateArea.getId());
        qWdetail.ne("isdelete", 1);
        qWdetail.eq("prosstype", prosstype);
        List<ZgsReportMonthfabrAreadetail> areadetail = zgsReportMonthfabrAreadetailService.list(qWdetail);
        if (areadetail.size() > 0){
          return areadetail.get(0);
        }
        return null;
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsReportMonthfabricateArea
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabricateArea zgsReportMonthfabricateArea) {
        return super.exportXls(request, zgsReportMonthfabricateArea, ZgsReportMonthfabricateArea.class, "zgs_report_monthfabricate_area");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabricateArea.class);
    }

}
