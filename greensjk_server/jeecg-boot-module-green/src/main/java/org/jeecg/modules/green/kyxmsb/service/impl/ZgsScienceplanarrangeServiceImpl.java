package org.jeecg.modules.green.kyxmsb.service.impl;

import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.mapper.ZgsScienceplanarrangeMapper;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceplanarrangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Service
public class ZgsScienceplanarrangeServiceImpl extends ServiceImpl<ZgsScienceplanarrangeMapper, ZgsScienceplanarrange> implements IZgsScienceplanarrangeService {

}
