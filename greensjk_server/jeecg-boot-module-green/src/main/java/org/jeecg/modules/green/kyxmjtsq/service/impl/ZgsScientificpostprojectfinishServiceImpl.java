package org.jeecg.modules.green.kyxmjtsq.service.impl;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostprojectfinish;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostprojectfinishMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostprojectfinishService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostprojectfinishServiceImpl extends ServiceImpl<ZgsScientificpostprojectfinishMapper, ZgsScientificpostprojectfinish> implements IZgsScientificpostprojectfinishService {

}
