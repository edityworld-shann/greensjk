package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertprojectbear;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertprojectbearMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertprojectbearService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库项目承担情况
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertprojectbearServiceImpl extends ServiceImpl<ZgsExpertprojectbearMapper, ZgsExpertprojectbear> implements IZgsExpertprojectbearService {

}
