package org.jeecg.modules.green.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 装配式建筑-新开工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_area_city_completed")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "装配式建筑-新开工-汇总表", description = "装配式建筑-新开工-汇总表")
public class ZgsReportMonthfabrAreaCityCompletedNewPo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
    /**
     * 填报人
     */
    //@Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    private String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private String filltm;
    /**
     * 报表时间
     */
    // @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private String reporttm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private BigDecimal applystate;
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    private String fillunit;
    /**
     * 创建人帐号
     */
    //@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    private String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    private String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    private String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    private String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private String areaname;
    /**
     * 退回原因
     */
    //@Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private Date backrdate;
    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private BigDecimal quarter;
    /**
     * 默认为空：市州内上报，1市州向省厅上报
     */
    //@Excel(name = "默认为空：市州内上报，1市州向省厅上报", width = 15)
    @ApiModelProperty(value = "默认为空：市州内上报，1市州向省厅上报")
    private String areaType;

    /**
     * 本月-新开工验收建筑-总面积
     */
    @Excel(name = "本月-新开工验收建筑-总面积", width = 15)
    @ApiModelProperty(value = "本月-新开工验收建筑-总面积")
    private BigDecimal monthArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-新开工验收装配式建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-新开工验收装配式建筑-建筑面积")
    private BigDecimal monthFarArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-新开工验收装配式建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-新开工验收装配式建筑-项数")
    private BigDecimal monthFarNumber = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    //@Excel(name = "本月-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它")
    private String monthBaildType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    // @Excel(name = "本月-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它")
    private String monthStructureType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-新开工验收全装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本月-新开工验收全装修住宅建筑面积")
    private BigDecimal monthFarZpszxzzjzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-新开工验收装配化装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本月-新开工验收装配化装修住宅建筑面积")
    private BigDecimal monthFarQzxzzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本月-新开工验收装配式建筑面积在新开工验收建筑总面积的占比（本月-装配率（%））", width = 15)
    @ApiModelProperty(value = "本月-新开工验收装配式建筑面积在新开工验收建筑总面积的占比（本月-装配率（%））")
    private BigDecimal monthFarAssemblyrate = new BigDecimal(0);

    /**
     * 本年-新开工验收建筑-总面积
     */
    @Excel(name = "本年-新开工验收建筑-总面积", width = 15)
    @ApiModelProperty(value = "本年-新开工验收建筑-总面积")
    private BigDecimal yearArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-新开工验收装配式建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-新开工验收装配式建筑-建筑面积")
    private BigDecimal yearFarArea = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-新开工验收装配式建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-新开工验收装配式建筑-项数")
    private BigDecimal yearFarNumber = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它")
    private String yearBaildType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它")
    private String yearStructureType;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-新开工验收全装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本年-新开工验收全装修住宅建筑面积")
    private BigDecimal yearFarZpszxzzjzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-新开工验收装配化装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本年-新开工验收装配化装修住宅建筑面积")
    private BigDecimal yearFarQzxzzmj = new BigDecimal(0);
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年-新开工验收装配式建筑面积在新开工验收建筑总面积的占比（本月-装配率（%））", width = 15)
    @ApiModelProperty(value = "本年-新开工验收装配式建筑面积在新开工验收建筑总面积的占比（本月-装配率（%））")
    private BigDecimal yearFarAssemblyrate = new BigDecimal(0);


    /**
     * 本月-按建筑类型分类-保障性住房-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-保障性住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-保障性住房-建筑面积")
    private BigDecimal monthFarArea1 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-保障性住房-项数
     */
    @Excel(name = "本月-按建筑类型分类-保障性住房-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-保障性住房-项数")
    private BigDecimal monthFarNumber1 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-商品住房-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-商品住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-商品住房-建筑面积")
    private BigDecimal monthFarArea2 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-商品住房-项数
     */
    @Excel(name = "本月-按建筑类型分类-商品住房-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-商品住房-项数")
    private BigDecimal monthFarNumber2 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-公共建筑-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-公共建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-公共建筑-建筑面积")
    private BigDecimal monthFarArea3 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-公共建筑-项数
     */
    @Excel(name = "本月-按建筑类型分类-公共建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-公共建筑-项数")
    private BigDecimal monthFarNumber3 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-农村及旅游景观项目-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-农村及旅游景观项目-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-农村及旅游景观项目-建筑面积")
    private BigDecimal monthFarArea4 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-农村及旅游景观项目-项数
     */
    @Excel(name = "本月-按建筑类型分类-农村及旅游景观项目-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-农村及旅游景观项目-项数")
    private BigDecimal monthFarNumber4 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-其他-建筑面积
     */
    @Excel(name = "本月-按建筑类型分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-其他-建筑面积")
    private BigDecimal monthFarArea5 = new BigDecimal(0);
    /**
     * 本月-按建筑类型分类-其他-项数
     */
    @Excel(name = "本月-按建筑类型分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本月-按建筑类型分类-其他-项数")
    private BigDecimal monthFarNumber5 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式混凝土结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式混凝土结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式混凝土结构建筑-建筑面积")
    private BigDecimal monthFarArea6 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式混凝土结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式混凝土结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式混凝土结构建筑-项数")
    private BigDecimal monthFarNumber6 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式钢结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式钢结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式钢结构建筑-建筑面积")
    private BigDecimal monthFarArea7 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式钢结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式钢结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式钢结构建筑-项数")
    private BigDecimal monthFarNumber7 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其中，钢结构装配式住宅-建筑面积")
    private BigDecimal monthFarArea8 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其中，钢结构装配式住宅-项数
     */
    @Excel(name = "本月-按结构形式分类-其中，钢结构装配式住宅-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其中，钢结构装配式住宅-项数")
    private BigDecimal monthFarNumber8 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式木结构建筑-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-装配式木结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式木结构建筑-建筑面积")
    private BigDecimal monthFarArea9 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-装配式木结构建筑-项数
     */
    @Excel(name = "本月-按结构形式分类-装配式木结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-装配式木结构建筑-项数")
    private BigDecimal monthFarNumber9 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其他-建筑面积
     */
    @Excel(name = "本月-按结构形式分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其他-建筑面积")
    private BigDecimal monthFarArea10 = new BigDecimal(0);
    /**
     * 本月-按结构形式分类-其他-项数
     */
    @Excel(name = "本月-按结构形式分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本月-按结构形式分类-其他-项数")
    private BigDecimal monthFarNumber10 = new BigDecimal(0);


    /**
     * 本年-按建筑类型分类-保障性住房-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-保障性住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-保障性住房-建筑面积")
    private BigDecimal yearFarArea1 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-保障性住房-项数
     */
    @Excel(name = "本年-按建筑类型分类-保障性住房-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-保障性住房-项数")
    private BigDecimal yearFarNumber1 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-商品住房-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-商品住房-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-商品住房-建筑面积")
    private BigDecimal yearFarArea2 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-商品住房-项数
     */
    @Excel(name = "本年-按建筑类型分类-商品住房-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-商品住房-项数")
    private BigDecimal yearFarNumber2 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-公共建筑-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-公共建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-公共建筑-建筑面积")
    private BigDecimal yearFarArea3 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-公共建筑-项数
     */
    @Excel(name = "本年-按建筑类型分类-公共建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-公共建筑-项数")
    private BigDecimal yearFarNumber3 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-农村及旅游景观项目-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-农村及旅游景观项目-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-农村及旅游景观项目-建筑面积")
    private BigDecimal yearFarArea4 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-农村及旅游景观项目-项数
     */
    @Excel(name = "本年-按建筑类型分类-农村及旅游景观项目-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-农村及旅游景观项目-项数")
    private BigDecimal yearFarNumber4 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-其他-建筑面积
     */
    @Excel(name = "本年-按建筑类型分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-其他-建筑面积")
    private BigDecimal yearFarArea5 = new BigDecimal(0);
    /**
     * 本年-按建筑类型分类-其他-项数
     */
    @Excel(name = "本年-按建筑类型分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本年-按建筑类型分类-其他-项数")
    private BigDecimal yearFarNumber5 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式混凝土结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式混凝土结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式混凝土结构建筑-建筑面积")
    private BigDecimal yearFarArea6 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式混凝土结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式混凝土结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式混凝土结构建筑-项数")
    private BigDecimal yearFarNumber6 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式钢结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式钢结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式钢结构建筑-建筑面积")
    private BigDecimal yearFarArea7 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式钢结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式钢结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式钢结构建筑-项数")
    private BigDecimal yearFarNumber7 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其中，钢结构装配式住宅-建筑面积")
    private BigDecimal yearFarArea8 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其中，钢结构装配式住宅-项数
     */
    @Excel(name = "本年-按结构形式分类-其中，钢结构装配式住宅-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其中，钢结构装配式住宅-项数")
    private BigDecimal yearFarNumber8 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式木结构建筑-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-装配式木结构建筑-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式木结构建筑-建筑面积")
    private BigDecimal yearFarArea9 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-装配式木结构建筑-项数
     */
    @Excel(name = "本年-按结构形式分类-装配式木结构建筑-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-装配式木结构建筑-项数")
    private BigDecimal yearFarNumber9 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其他-建筑面积
     */
    @Excel(name = "本年-按结构形式分类-其他-建筑面积", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其他-建筑面积")
    private BigDecimal yearFarArea10 = new BigDecimal(0);
    /**
     * 本年-按结构形式分类-其他-项数
     */
    @Excel(name = "本年-按结构形式分类-其他-项数", width = 15)
    @ApiModelProperty(value = "本年-按结构形式分类-其他-项数")
    private BigDecimal yearFarNumber10 = new BigDecimal(0);

    /**
     * 1:新开工2:新开工验收
     */
    //@Excel(name = "1:新开工2:新开工验收", width = 3)
    @ApiModelProperty(value = "1:新开工2:新开工验收")
    private String projecttype;

    @TableField(exist = false)
    @ApiModelProperty(value = "本月新开工建筑总面积")
    private double newConstructionArea = 0.0;
    @TableField(exist = false)
    @ApiModelProperty(value = "本月新开工验收建筑总面积")
    private double completedArea = 0.0;

}
