package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsScientificprojectfinishService extends IService<ZgsScientificprojectfinish> {

}
