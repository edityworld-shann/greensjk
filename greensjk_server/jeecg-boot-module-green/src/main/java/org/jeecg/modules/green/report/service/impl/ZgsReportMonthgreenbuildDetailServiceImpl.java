package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthgreenbuildDetail;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthgreenbuildDetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildDetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthgreenbuild_detail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthgreenbuildDetailServiceImpl extends ServiceImpl<ZgsReportMonthgreenbuildDetailMapper, ZgsReportMonthgreenbuildDetail> implements IZgsReportMonthgreenbuildDetailService {

}
