package org.jeecg.modules.green.zjksb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 专家库项目承担情况
 * @Author: jeecg-boot
 * @Date: 2022-02-18
 * @Version: V1.0
 */
@Data
@TableName("zgs_expertprojectbear")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_expertprojectbear对象", description = "专家库项目承担情况")
public class ZgsExpertprojectbear implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 登录人员ID
     */
    @Excel(name = "登录人员ID", width = 15)
    @ApiModelProperty(value = "登录人员ID")
    private java.lang.String userid;
    /**
     * 审核状态：0未上报，1待审核，3退回，4初审通过，5退回初审，2终审通过
     */
    @Excel(name = "审核状态：0未上报，1待审核，3退回，4初审通过，5退回初审，2终审通过", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，3退回，4初审通过，5退回初审，2终审通过")
    private java.lang.String status;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 项目级别
     */
    @Excel(name = "项目级别", width = 15)
    @ApiModelProperty(value = "项目级别")
    private java.lang.String projectlevel;
    /**
     * 项目级别编码，字典值：ProjectBearLevel
     */
    @Excel(name = "项目级别编码，字典值：ProjectBearLevel", width = 15)
    @ApiModelProperty(value = "项目级别编码，字典值：ProjectBearLevel")
    private java.lang.String projectlevelnum;
    /**
     * 项目级别为其他时填写
     */
    @Excel(name = "项目级别为其他时填写", width = 15)
    @ApiModelProperty(value = "项目级别为其他时填写")
    private java.lang.String projectlevelother;
    /**
     * 本人排名
     */
    @Excel(name = "本人排名", width = 15)
    @ApiModelProperty(value = "本人排名")
    private java.lang.String ranking;
    /**
     * 开始日期
     */
    @Excel(name = "开始日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始日期")
    private java.util.Date startdate;
    /**
     * 结束日期
     */
    @Excel(name = "结束日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "结束日期")
    private java.util.Date enddate;
    /**
     * 项目摘要
     */
    @Excel(name = "项目摘要", width = 15)
    @ApiModelProperty(value = "项目摘要")
    private java.lang.String projectsummary;
    /**
     * 关键词，用分号或逗号隔开
     */
    @Excel(name = "关键词，用分号或逗号隔开", width = 15)
    @ApiModelProperty(value = "关键词，用分号或逗号隔开")
    private java.lang.String keyword;
    /**
     * 承担单位
     */
    @Excel(name = "承担单位", width = 15)
    @ApiModelProperty(value = "承担单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String undertakingunit;
    /**
     * 参与模式
     */
    @Excel(name = "参与模式", width = 15)
    @ApiModelProperty(value = "参与模式")
    private java.lang.String parttype;
    /**
     * 参与模式编码，字典值：PartInType
     */
    @Excel(name = "参与模式编码，字典值：PartInType", width = 15)
    @ApiModelProperty(value = "参与模式编码，字典值：PartInType")
    private java.lang.String parttypenum;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15)
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 项目年度
     */
    @Excel(name = "项目年度", width = 15)
    @ApiModelProperty(value = "项目年度")
    private java.lang.String projectyear;
    /**
     * 资助机构
     */
    @Excel(name = "资助机构", width = 15)
    @ApiModelProperty(value = "资助机构")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String financialbody;
    /**
     * 资助类别
     */
    @Excel(name = "资助类别", width = 15)
    @ApiModelProperty(value = "资助类别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String financialtype;
    /**
     * 资金总额，万元
     */
    @Excel(name = "资金总额，万元", width = 15)
    @ApiModelProperty(value = "资金总额，万元")
    private java.math.BigDecimal amount;
    /**
     * 项目状态
     */
    @Excel(name = "项目状态", width = 15)
    @ApiModelProperty(value = "项目状态")
    private java.lang.String projectstatus;
    /**
     * 项目状态编号，字典值：ProjectStatus
     */
    @Excel(name = "项目状态编号，字典值：ProjectStatus", width = 15)
    @ApiModelProperty(value = "项目状态编号，字典值：ProjectStatus")
    private java.lang.String projectstatusnum;
    /**
     * 项目状态为其他时填写
     */
    @Excel(name = "项目状态为其他时填写", width = 15)
    @ApiModelProperty(value = "项目状态为其他时填写")
    private java.lang.String projectstatusother;
    /**
     * 备注
     */
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 上报时间
     */
    @Excel(name = "上报时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报时间")
    private java.util.Date applydate;
    /**
     * 对应业务表T_EXPERTINFO主键
     */
    @Excel(name = "对应业务表T_EXPERTINFO主键", width = 15)
    @ApiModelProperty(value = "对应业务表T_EXPERTINFO主键")
    private java.lang.String expertguid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
