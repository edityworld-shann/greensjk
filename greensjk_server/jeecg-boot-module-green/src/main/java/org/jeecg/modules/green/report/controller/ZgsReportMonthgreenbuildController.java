package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildDetailService;
import org.jeecg.modules.green.report.service.IZgsReportMonthgreenbuildService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthgreenbuild
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="绿色建筑（市州）")
@RestController
@RequestMapping("/report/zgsReportMonthgreenbuild")
@Slf4j
public class ZgsReportMonthgreenbuildController extends JeecgController<ZgsReportMonthgreenbuild, IZgsReportMonthgreenbuildService> {
	@Autowired
	private IZgsReportMonthgreenbuildService zgsReportMonthgreenbuildService;

	 @Autowired
	 private IZgsReportMonthgreenbuildDetailService zgsReportMonthgreenbuildDetailService;


	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthgreenbuild
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "绿色建筑（市州）-分页列表查询")
	@ApiOperation(value="绿色建筑（市州）-分页列表查询", notes="绿色建筑（市州）-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   @RequestParam(name = "prossType", required = false) Integer prossType,
                   @RequestParam(name = "applyState", required = false) String applyState,
								   HttpServletRequest req) {

		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		QueryWrapper<ZgsReportMonthgreenbuild> qW = new QueryWrapper<>();
		if(zgsReportMonthgreenbuild!=null){
			BigDecimal applystate  = zgsReportMonthgreenbuild.getApplystate();
			if(applystate!=null){
				//有问题，BigDecimal applystate 需要判空？
				qW.eq("applystate",applystate);
			}
			Integer userType = sysUser.getLoginUserType();
			//6县区，7市区
			if(userType==7){
				qW.eq("areacode",sysUser.getAreacode());
			}
			String reporttm  = zgsReportMonthgreenbuild.getReporttm();
			if(StringUtils.isNotEmpty(reporttm)){
				qW.eq("reporttm",reporttm);
			}
		}
    //  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
    if(applyState != null){
      splitArray = applyState.split(",");
    }
    if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
		qW.ne("isdelete",1);
		qW.orderByDesc("createtime");

//		QueryWrapper<ZgsReportMonthgreenbuild> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthgreenbuild, req.getParameterMap());
		Page<ZgsReportMonthgreenbuild> page = new Page<ZgsReportMonthgreenbuild>(pageNo, pageSize);
		IPage<ZgsReportMonthgreenbuild> pageList = zgsReportMonthgreenbuildService.page(page, qW);
		//根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
    for(ZgsReportMonthgreenbuild greenbuildArea : pageList.getRecords()){
      List<ZgsReportMonthgreenbuildDetail> areaDetailList = findAreaDetailList(greenbuildArea);
      if(areaDetailList != null){
        for (int i = 0;i < areaDetailList.size();i++){
          if ("0".equals(areaDetailList.get(i).getProsstype().toString())){
            greenbuildArea.setDt0(areaDetailList.get(i));
          }else if ("1".equals(areaDetailList.get(i).getProsstype().toString())){
            greenbuildArea.setDt1(areaDetailList.get(i));
          }else if ("2".equals(areaDetailList.get(i).getProsstype().toString())){
            greenbuildArea.setDt2(areaDetailList.get(i));
          }else if ("3".equals(areaDetailList.get(i).getProsstype().toString())){
            greenbuildArea.setDt3(areaDetailList.get(i));
          }
        }
      }
    }
		return Result.OK(pageList);
	}


//   //查找明细数据
   public List<ZgsReportMonthgreenbuildDetail> findAreaDetailList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuildArea){
     QueryWrapper<ZgsReportMonthgreenbuildDetail> qWdetail = new QueryWrapper<>();
     qWdetail.eq("monthmianguid",zgsReportMonthgreenbuildArea.getId());
     qWdetail.ne("isdelete",1);
     List<ZgsReportMonthgreenbuildDetail> list = zgsReportMonthgreenbuildDetailService.list(qWdetail);
     return list;
   }


	/**
	 *   添加
	 *
	 * @param zgsReportMonthgreenbuild
	 * @return
	 */
	@AutoLog(value = "绿色建筑（市州）-添加")
	@ApiOperation(value="绿色建筑（市州）-添加", notes="绿色建筑（市州）-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthgreenbuild zgsReportMonthgreenbuild) {
//		zgsReportMonthgreenbuildService.save(zgsReportMonthgreenbuild);
//		return Result.OK("添加成功！");

		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		//生成主表ID
		String id = UUID.randomUUID().toString();
		zgsReportMonthgreenbuild.setId(id);
		//申报状态  0未上报
		zgsReportMonthgreenbuild.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
		//行政区域
    zgsReportMonthgreenbuild.setAreacode(sysUser.getAreacode());
    zgsReportMonthgreenbuild.setAreaname(sysUser.getAreaname());
    zgsReportMonthgreenbuild.setIsdelete(new BigDecimal(0));
    zgsReportMonthgreenbuild.setCreatepersonaccount(sysUser.getUsername());
    zgsReportMonthgreenbuild.setCreatepersonname(sysUser.getRealname());
    zgsReportMonthgreenbuild.setCreatetime(new Date());
    zgsReportMonthgreenbuildService.save(zgsReportMonthgreenbuild);
    String prosstype = "0";
    //保存各种状态下的明细数据
    //根据工程进度状态命名0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行
    ZgsReportMonthgreenbuildDetail dt0 = zgsReportMonthgreenbuild.getDt0();
    if(dt0!=null){
      prosstype = "0";
      saveAreaDetail(dt0,sysUser,id,prosstype);
    }
    ZgsReportMonthgreenbuildDetail dt1 = zgsReportMonthgreenbuild.getDt1();
    if(dt1!=null){
      prosstype = "1";
      saveAreaDetail(dt1,sysUser,id,prosstype);
    }
    ZgsReportMonthgreenbuildDetail dt2 = zgsReportMonthgreenbuild.getDt2();
    if(dt2!=null){
      prosstype = "2";
      saveAreaDetail(dt2,sysUser,id,prosstype);
    }
    ZgsReportMonthgreenbuildDetail dt3 = zgsReportMonthgreenbuild.getDt3();
    if(dt3!=null){
      prosstype = "3";
      saveAreaDetail(dt3,sysUser,id,prosstype);
    }
    return Result.OK("添加成功！");
  }


   //保存明细
   public void saveAreaDetail(ZgsReportMonthgreenbuildDetail areaDetail,LoginUser sysUser,String id, String prosstype){
     //生成明细表ID
     String id2 = UUID.randomUUID().toString();
     areaDetail.setId(id2);
     //关联主表ID
     areaDetail.setMonthmianguid(id);
     /**0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行*/
     areaDetail.setProsstype(new BigDecimal(prosstype));
     areaDetail.setIsdelete(new BigDecimal(0));
     areaDetail.setCreatepersonaccount(sysUser.getUsername());
     areaDetail.setCreatepersonname(sysUser.getRealname());
     areaDetail.setCreatetime(new Date());
     zgsReportMonthgreenbuildDetailService.save(areaDetail);
   }

	 //保存明细
	 public void saveAreaDetail(ZgsReportMonthgreenbuildDetail areaDetail,LoginUser sysUser,String id){
		 //生成明细表ID
		 String id2 = UUID.randomUUID().toString();
		 areaDetail.setId(id2);
		 //关联主表ID
		 areaDetail.setMonthmianguid(id);
		 areaDetail.setCreatepersonaccount(sysUser.getUsername());
		 areaDetail.setCreatepersonname(sysUser.getRealname());
		 areaDetail.setCreatetime(new Date());
		 zgsReportMonthgreenbuildDetailService.save(areaDetail);

	 }
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthgreenbuild
	 * @return
	 */
	@AutoLog(value = "绿色建筑（市州）-编辑")
	@ApiOperation(value="绿色建筑（市州）-编辑", notes="绿色建筑（市州）-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthgreenbuild zgsReportMonthgreenbuild) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (zgsReportMonthgreenbuild != null && StringUtils.isNotEmpty(zgsReportMonthgreenbuild.getId())) {
			String id = zgsReportMonthgreenbuild.getId();
			if (GlobalConstants.apply_state1.equals(zgsReportMonthgreenbuild.getApplystate())) {
				//保存并上报
				zgsReportMonthgreenbuild.setApplydate(new Date());
			}
			if (GlobalConstants.apply_state3.equals(zgsReportMonthgreenbuild.getApplystate())) {
				//退回
				zgsReportMonthgreenbuild.setBackrdate(new Date());
			}
			zgsReportMonthgreenbuild.setModifypersonaccount(sysUser.getUsername());
			zgsReportMonthgreenbuild.setModifypersonname(sysUser.getRealname());
			zgsReportMonthgreenbuild.setModifytime(new Date());
			zgsReportMonthgreenbuildService.updateById(zgsReportMonthgreenbuild);

			//更新明细表数据
			ZgsReportMonthgreenbuildDetail dt0 = zgsReportMonthgreenbuild.getDt0();
			ZgsReportMonthgreenbuildDetail dt1 = zgsReportMonthgreenbuild.getDt1();
			ZgsReportMonthgreenbuildDetail dt2 = zgsReportMonthgreenbuild.getDt2();
			ZgsReportMonthgreenbuildDetail dt3 = zgsReportMonthgreenbuild.getDt3();
			if(dt0!=null){
				updateDetail(dt0,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype0);
			}
			if(dt1!=null){
				updateDetail(dt1,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype1);
			}
			if(dt2!=null){
				updateDetail(dt2,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype2);
			}
			if(dt3!=null){
				updateDetail(dt3,sysUser,id);
			}else{
				deleteDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype3);
			}

		}

		return Result.OK("编辑成功!");
	}

	public void deleteDetail(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild,Integer prosstype){
		QueryWrapper<ZgsReportMonthgreenbuildDetail> qWdetail = new QueryWrapper<>();
		qWdetail.eq("monthmianguid",zgsReportMonthgreenbuild.getId());
		qWdetail.ne("isdelete",1);
		qWdetail.eq("prosstype",prosstype);
		zgsReportMonthgreenbuildDetailService.remove(qWdetail);
	}


	public void updateDetail(ZgsReportMonthgreenbuildDetail areaDetail,LoginUser sysUser,String id){
		if (StringUtils.isNotEmpty(areaDetail.getId())) {
			//编辑
			areaDetail.setModifypersonaccount(sysUser.getUsername());
			areaDetail.setModifypersonname(sysUser.getRealname());
			areaDetail.setModifytime(new Date());
			zgsReportMonthgreenbuildDetailService.updateById(areaDetail);
		} else {
			//新增
			saveAreaDetail(areaDetail, sysUser, id);
		}
	}

	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreenbuild-通过id删除")
	@ApiOperation(value="zgs_report_monthgreenbuild-通过id删除", notes="zgs_report_monthgreenbuild-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthgreenbuildService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthgreenbuild-批量删除")
	@ApiOperation(value="zgs_report_monthgreenbuild-批量删除", notes="zgs_report_monthgreenbuild-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthgreenbuildService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "绿色建筑（市州）-通过id查询")
	@ApiOperation(value="绿色建筑（市州）-通过id查询", notes="绿色建筑（市州）-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthgreenbuild zgsReportMonthgreenbuild = zgsReportMonthgreenbuildService.getById(id);
		if(zgsReportMonthgreenbuild==null) {
			return Result.error("未找到对应数据");
		}else{
			//查询各种状态明细数据
			ZgsReportMonthgreenbuildDetail dt0= findAreaDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype0);
			if(dt0!=null){
				zgsReportMonthgreenbuild.setDt0(dt0);
			}
			ZgsReportMonthgreenbuildDetail dt1= findAreaDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype1);
			if(dt1!=null){
				zgsReportMonthgreenbuild.setDt1(dt1);
			}
			ZgsReportMonthgreenbuildDetail dt2= findAreaDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype2);
			if(dt2!=null){
				zgsReportMonthgreenbuild.setDt2(dt2);
			}
			ZgsReportMonthgreenbuildDetail dt3= findAreaDetail(zgsReportMonthgreenbuild,GlobalConstants.prosstype3);
			if(dt3!=null){
				zgsReportMonthgreenbuild.setDt3(dt3);
			}
		}
		return Result.OK(zgsReportMonthgreenbuild);
	}
	 //查找明细数据
	 public ZgsReportMonthgreenbuildDetail findAreaDetail(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, Integer prosstype){
		 QueryWrapper<ZgsReportMonthgreenbuildDetail> qWdetail = new QueryWrapper<>();
		 qWdetail.eq("monthmianguid",zgsReportMonthgreenbuild.getId());
		 qWdetail.ne("isdelete",1);
		 qWdetail.eq("prosstype",prosstype);
		 ZgsReportMonthgreenbuildDetail areadetail = zgsReportMonthgreenbuildDetailService.getOne(qWdetail);
		 return areadetail;

	 }

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthgreenbuild
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthgreenbuild zgsReportMonthgreenbuild) {
        return super.exportXls(request, zgsReportMonthgreenbuild, ZgsReportMonthgreenbuild.class, "zgs_report_monthgreenbuild");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthgreenbuild.class);
    }

	 /**
	  * 大屏绿色建筑查询
	  *
	  * @param zgsReportMonthgreenbuild
	  * @param req
	  * @return Map<String ,Object>
	  */
	 @AutoLog(value = "大屏绿色建筑列表查询")
	 @ApiOperation(value="大屏绿色建筑列表查询", notes="大屏绿色建筑列表查询")
	 @GetMapping(value = "/screanGreenList")
	 public Result<Map<String ,Object>> screanGreenQueryList(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 HashMap<String,Object> resultMap = new HashMap<String,Object>();

		 /**大屏项目总数：“未开工”+“已开工”+“已竣工”=“项目总数” 自年初绿色建筑累计情况 项数相加*/
		 BigDecimal screanTotalxmzs=new BigDecimal(0);
		 /**大屏绿色建筑总数：“未开工绿色建筑项数”+“已开工绿色建筑项数 ”+“已竣工绿色建筑项数”=“绿色建筑”  只加年初绿色建筑一项*/
		 BigDecimal screanTotallsjz =new BigDecimal(0);
		 /**大屏强制推广类总数 ：“未开工强制推广项目数”+“已开工强制推广项目数”+“已竣工强制推广项目数”=“强制推广类” 强制推广类年初项数和（保障性住房+10万平方米以上商业开发住宅小区+大型公共建筑+中小型公共建筑+其中政府投资类公共建筑）*/
		 BigDecimal screanTotalqztgl =new BigDecimal(0);
		 /**大屏未开工情况数量：“未开工所有项目数”=“未开工情况数量” 这个只统计未开工状态 */
		 BigDecimal screanTotalwkgqksl =new BigDecimal(0);

		 QueryWrapper<ZgsReportMonthgreenbuild> qW = new QueryWrapper<>();
		 String reporttm  = DateUtils.formatDate(new Date(),"YYYY-MM");
		 if(zgsReportMonthgreenbuild!=null){
		 	if(zgsReportMonthgreenbuild.getReporttm()!=null){
				reporttm = zgsReportMonthgreenbuild.getReporttm();
			}
			 if(zgsReportMonthgreenbuild.getAreacode()!=null){
				 qW.eq("areacode",zgsReportMonthgreenbuild.getAreacode());
			 }
		 }

		 qW.eq("reporttm", reporttm);
		 qW.ne("isdelete",1);
		 qW.orderByDesc("applydate");

		 List<ZgsReportMonthgreenbuild> greenList = zgsReportMonthgreenbuildService.list(qW);
		 //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
		 //查询明细数据
		 for(ZgsReportMonthgreenbuild b : greenList){
			 ZgsReportMonthgreenbuildDetail dt0= findAreaDetail(b,GlobalConstants.prosstype0);
			 if(dt0!=null){
				 b.setDt0(dt0);
				 //大屏顶部数据 大屏项目总数
				 screanTotalxmzs(b,dt0,screanTotalxmzs);
				 //大屏顶部数据 未开工情况数量
				 screanTotallsjz(b,dt0,screanTotallsjz);
				 screanTotalqztgl(b,dt0,screanTotalqztgl);

			 }
			 ZgsReportMonthgreenbuildDetail dt1= findAreaDetail(b,GlobalConstants.prosstype1);
			 if(dt1!=null){
				 b.setDt1(dt1);
				 //大屏顶部数据 大屏项目总数
				 screanTotalxmzs(b,dt0,screanTotalxmzs);
				 screanTotallsjz(b,dt0,screanTotallsjz);
				 screanTotalqztgl(b,dt0,screanTotalqztgl);
			 }
			 ZgsReportMonthgreenbuildDetail dt2= findAreaDetail(b,GlobalConstants.prosstype2);
			 if(dt2!=null){
				 b.setDt2(dt2);
				 //大屏顶部数据 大屏项目总数
				 screanTotalxmzs(b,dt0,screanTotalxmzs);
				 screanTotallsjz(b,dt0,screanTotallsjz);
				 screanTotalqztgl(b,dt0,screanTotalqztgl);

			 }
			 ZgsReportMonthgreenbuildDetail dt3= findAreaDetail(b,GlobalConstants.prosstype3);
			 if(dt3!=null){
				 b.setDt3(dt3);
			 }

		 }
		 //大屏列表数据
		 resultMap.put("greenList",greenList);
		 //大屏顶部数据 大屏项目总数
		 resultMap.put("screanTotalxmzs",screanTotalxmzs);
		 //大屏顶部数据 大屏绿色建筑总数
		 resultMap.put("screanTotallsjz",screanTotallsjz);
		 //大屏顶部数据 大屏强制推广类总数
		 resultMap.put("screanTotalqztgl",screanTotalqztgl);
		 //大屏顶部数据 未开工情况数量
		 resultMap.put("screanTotalwkgqksl",screanTotalwkgqksl);

		 return Result.OK(resultMap);
	 }

	 //大屏顶部数据 大屏项目总数 ;未开工情况数量
	 public void screanTotalxmzs(ZgsReportMonthgreenbuild record,ZgsReportMonthgreenbuildDetail c, BigDecimal sum ){
	 	sum =sum.add(c.getNcxs());
	 	sum = sum.add(c.getNcdxggjzxs());
	 	sum = sum.add(c.getNcbzxxs());
	 	sum = sum.add(c.getNcgyjzxs());
	 	sum = sum.add(c.getNcsyzzxqxs());
	 	sum = sum.add(c.getNczftzlggjzxs());
	 	sum = sum.add(c.getNczxsggjzxs());
	 	sum = sum.add(c.getNczzjzxs());
	 	sum = sum.add(c.getSjeggxs());
	 	sum = sum.add(c.getSjegyxs());
	 	sum = sum.add(c.getSjezzxs());
	 	sum = sum.add(c.getSjsggxs());
	 	sum = sum.add(c.getSjsgyxs());
	 	sum = sum.add(c.getSjszzxs());
	 	sum = sum.add(c.getSjyggxs());
	 	sum = sum.add(c.getSjygyxs());
	 	sum = sum.add(c.getSjyzzxs());
	 	sum = sum.add(c.getYxeggxs());
	 	sum = sum.add(c.getYxegyxs());
	 	sum = sum.add(c.getYxezzxs());
	 	sum = sum.add(c.getYxsggxs());
	 	sum = sum.add(c.getYxsgyxs());
	 	sum = sum.add(c.getYxszzxs());
	 	sum = sum.add(c.getYxyggxs());
	 	sum = sum.add(c.getYxygyxs());
	 	sum = sum.add(c.getYxyzzxs());

	 	int xmzs = record.getScreanTotalxmzs();
	 	xmzs +=sum.intValue();
	 	record.setScreanTotalxmzs(xmzs);
		 //大屏顶部数据 未开工情况数量
	 	if(c.getProsstype().equals(GlobalConstants.prosstype0)){
			int wkgqksl = record.getScreanTotalwkgqksl();
			wkgqksl +=sum.intValue();
			record.setScreanTotalwkgqksl(wkgqksl);
		}

	 }
	 //大屏顶部数据 大屏绿色建筑总数
	 public void screanTotallsjz(ZgsReportMonthgreenbuild record,ZgsReportMonthgreenbuildDetail c, BigDecimal sum ){
	 	sum=sum.add(c.getNcxs());
	 	int lsjz = record.getScreanTotallsjz();
	 	lsjz +=sum.intValue();
	 	record.setScreanTotallsjz(lsjz);
	 }
	 //大屏顶部数据 大屏强制推广类总数
	 public void screanTotalqztgl(ZgsReportMonthgreenbuild record,ZgsReportMonthgreenbuildDetail c, BigDecimal sum ){
		sum=sum.add(c.getNcbzxxs());
		sum=sum.add(c.getNcdxggjzxs());
		sum=sum.add(c.getNczxsggjzxs());
		sum=sum.add(c.getNczftzlggjzxs());
		sum=sum.add(c.getNcsyzzxqxs());
		int qztgl = record.getScreanTotalqztgl();
		qztgl +=sum.intValue();
		record.setScreanTotalqztgl(qztgl);
	 }

	 /**
	  * 大屏绿色建筑查询
	  *
	  * @param zgsReportMonthgreenbuild
	  * @param req
	  * @return Map<String ,Object>
	  */
	 @AutoLog(value = "大屏首页甘肃省批复统计查询")
	 @ApiOperation(value="大屏首页甘肃省批复统计查询", notes="大屏首页甘肃省批复统计查询")
	 @GetMapping(value = "/pifutongji")
	 public Result<Map<String ,Object>> screanpifutongji(ZgsReportMonthgreenbuild zgsReportMonthgreenbuild, HttpServletRequest req) {
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 HashMap<String,Object> resultMap = new HashMap<String,Object>();
//		 省级权限，已开工，自年初累计里的绿色面积，需要求和，是甘肃省批复统计。红框左下的绿色建筑数据是在省级权限，已竣工，自年初累计里的绿色面积，需要求和。
		 /**绿色建筑批复面积,已开工，自年初累计里的绿色面积，需要求和，是甘肃省批复统计*/
		 BigDecimal lsjzpfmj=new BigDecimal(0);
		 /**绿色建筑 已竣工，自年初累计里的绿色面积，需要求和*/
		 BigDecimal lsjzmj =new BigDecimal(0);
		 /**装配式建筑 装配式建筑：省级权限，“自年初已竣工验收累计情况”里的“已竣工验收装配式建筑”里的建筑面积*/
		 BigDecimal zpsjzmj =new BigDecimal(0);
		 /**建筑节能 建筑节能：省级权限，“自年初完成既有建筑节能改造累计情况”里的小计面积*/
		 BigDecimal jzjnmj =new BigDecimal(0);

		 QueryWrapper<ZgsReportMonthgreenbuild> qW = new QueryWrapper<>();
		 String reporttm  = DateUtils.formatDate(new Date(),"YYYY-MM");
		 if(zgsReportMonthgreenbuild!=null){
			 if(zgsReportMonthgreenbuild.getReporttm()!=null){
				 reporttm = zgsReportMonthgreenbuild.getReporttm();
			 }
		 }

		 qW.eq("reporttm", reporttm);
		 qW.ne("isdelete",1);
		 qW.orderByDesc("applydate");

		 List<ZgsReportMonthgreenbuild> greenList = zgsReportMonthgreenbuildService.list(qW);
		 //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
		 //查询明细数据
		 for(ZgsReportMonthgreenbuild b : greenList){
			 ZgsReportMonthgreenbuildDetail dt1= findAreaDetail(b,GlobalConstants.prosstype1);
			 if(dt1!=null){
//				 b.setDt1(dt1);
//				 //大屏顶部数据 大屏项目总数
//				 screanTotalxmzs(b,dt0,screanTotalxmzs);
//				 screanTotallsjz(b,dt0,screanTotallsjz);
//				 screanTotalqztgl(b,dt0,screanTotalqztgl);
			 }
			 ZgsReportMonthgreenbuildDetail dt2= findAreaDetail(b,GlobalConstants.prosstype2);
			 if(dt2!=null){
//				 b.setDt2(dt2);
//				 //大屏顶部数据 大屏项目总数
//				 screanTotalxmzs(b,dt0,screanTotalxmzs);
//				 screanTotallsjz(b,dt0,screanTotallsjz);
//				 screanTotalqztgl(b,dt0,screanTotalqztgl);

			 }

		 }



		 /**绿色建筑批复面积,已开工，自年初累计里的绿色面积，需要求和，是甘肃省批复统计*/
		 resultMap.put("lsjzpfmj",lsjzpfmj);
		 /**绿色建筑 已竣工，自年初累计里的绿色面积，需要求和*/
		 resultMap.put("lsjzmj",lsjzmj);
		 /**装配式建筑 装配式建筑：省级权限，“自年初已竣工验收累计情况”里的“已竣工验收装配式建筑”里的建筑面积*/
		 resultMap.put("zpsjzmj",zpsjzmj);
		 /**建筑节能 建筑节能：省级权限，“自年初完成既有建筑节能改造累计情况”里的小计面积*/
		 resultMap.put("jzjnmj",jzjnmj);


		 return Result.OK(resultMap);
	 }




 }
