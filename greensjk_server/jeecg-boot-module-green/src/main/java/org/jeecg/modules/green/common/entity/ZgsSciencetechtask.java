package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.sfxmsb.entity.ZgsCommonFee;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificexpenditureclear;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 科技项目任务书申报
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Data
@TableName("zgs_sciencetechtask")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_sciencetechtask对象", description = "科技项目任务书申报")
public class ZgsSciencetechtask implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
//    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15)
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;

    @ApiModelProperty(value = "省科技经费备注")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String fundRemark;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15)
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15)
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15)
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15)
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 承担单位
     */
    @Excel(name = "承担单位", width = 15, orderNum = "1")
    @ApiModelProperty(value = "承担单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String commitmentunit;
    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 15, orderNum = "0")
    @ApiModelProperty(value = "工程项目名称")
    private java.lang.String projectname;
    /**
     * 开始时间
     */
    @Excel(name = "开始时间", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private java.util.Date startdate;
    /**
     * 截止时间
     */
    @Excel(name = "截止时间", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "截止时间")
    private java.util.Date enddate;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleader;
    /**
     * 项目负责人联系电话
     */
    @Excel(name = "项目负责人联系电话", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleadertel;
    /**
     * 购置设备费
     */
    @Excel(name = "购置设备费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "购置设备费")
    private java.math.BigDecimal equipmentcost;
    /**
     * 设备改造与租赁费
     */
    @Excel(name = "设备改造与租赁费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "设备改造与租赁费")
    private java.math.BigDecimal equipmentchange;
    /**
     * 材料费
     */
    @Excel(name = "材料费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "材料费")
    private java.math.BigDecimal materialfee;
    /**
     * 出版/文献/信息传播/知识产权事务费
     */
    @Excel(name = "出版/文献/信息传播/知识产权事务费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "出版/文献/信息传播/知识产权事务费")
    private java.math.BigDecimal publicationfee;
    /**
     * 专家咨询评审费
     */
    @Excel(name = "专家咨询评审费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "专家咨询评审费")
    private java.math.BigDecimal assessmentfee;
    /**
     * 其他（含培训费）
     */
    @Excel(name = "其他（含培训费）", width = 15, orderNum = "4")
    @ApiModelProperty(value = "其他（含培训费）")
    private java.math.BigDecimal trainingfee;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 组织机构代码
     */
//    @Excel(name = "组织机构代码", width = 15)
    @ApiModelProperty(value = "组织机构代码")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String organizationcode;
    /**
     * 单位通讯地址
     */
//    @Excel(name = "单位通讯地址", width = 15)
    @ApiModelProperty(value = "单位通讯地址")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unitaddress;
    /**
     * 邮编
     */
    //@Excel(name = "邮编", width = 15, orderNum = "2")
    @ApiModelProperty(value = "邮编")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String zipcode;
    /**
     * 单位性质
     */
    //@Excel(name = "单位性质", width = 15, orderNum = "2")
    @ApiModelProperty(value = "单位性质")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unitnature;
    /**
     * 上级行政主管部门
     */
//    @Excel(name = "上级行政主管部门", width = 15)
    @ApiModelProperty(value = "上级行政主管部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String department;
    /**
     * 项目负责人性别
     */
    //@Excel(name = "项目负责人性别", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人性别")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headsex;
    /**
     * 项目负责人学位
     */
    //@Excel(name = "项目负责人学位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人学位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String degree;
    /**
     * 项目负责人职称
     */
    //@Excel(name = "项目负责人职称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人职称")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String technical;
    /**
     * 专业
     */
    //@Excel(name = "专业", width = 15, orderNum = "1")
    @ApiModelProperty(value = "专业")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String major;
    /**
     * 项目负责人所在单位
     */
    //@Excel(name = "项目负责人所在单位", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人所在单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headunit;
    /**
     * 身份证号
     */
    //@Excel(name = "身份证号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "身份证号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String identitycard;
    /**
     * 项目负责人手机号
     */
    @Excel(name = "项目负责人手机号", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人手机号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String headmobile;
    /**
     * 项目负责人email
     */
    //@Excel(name = "项目负责人email", width = 15, orderNum = "1")
    @ApiModelProperty(value = "项目负责人email")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String heademail;
    /**
     * 参加课题人数总人数
     */
    //@Excel(name = "参加课题人数总人数", width = 15, orderNum = "2")
    @ApiModelProperty(value = "参加课题人数总人数")
    private java.math.BigDecimal cjktrstotal;
    /**
     * 高级
     */
    //@Excel(name = "高级", width = 15, orderNum = "2")
    @ApiModelProperty(value = "高级")
    private java.math.BigDecimal techgj;
    /**
     * 中级
     */
    //@Excel(name = "中级", width = 15, orderNum = "2")
    @ApiModelProperty(value = "中级")
    private java.math.BigDecimal techzj;
    /**
     * 初级
     */
    // @Excel(name = "初级", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初级")
    private java.math.BigDecimal techcj;
    /**
     * 其他
     */
    //@Excel(name = "其他", width = 15, orderNum = "2")
    @ApiModelProperty(value = "其他")
    private java.math.BigDecimal techqt;
    /**
     * 博士
     */
    // @Excel(name = "博士", width = 15, orderNum = "2")
    @ApiModelProperty(value = "博士")
    private java.math.BigDecimal degreebs;
    /**
     * 硕士
     */
    //@Excel(name = "硕士", width = 15, orderNum = "2")
    @ApiModelProperty(value = "硕士")
    private java.math.BigDecimal degreess;
    /**
     * 学士
     */
    // @Excel(name = "学士", width = 15, orderNum = "2")
    @ApiModelProperty(value = "学士")
    private java.math.BigDecimal degreexs;
    /**
     * 其他
     */
    // @Excel(name = "其他", width = 15, orderNum = "2")
    @ApiModelProperty(value = "其他")
    private java.math.BigDecimal degreeqt;
    /**
     * 所属技术领域
     */
    // @Excel(name = "所属技术领域", width = 15, orderNum = "3")
    @ApiModelProperty(value = "所属技术领域")
    private java.lang.String technicalfield;
    /**
     * 所属技术领域(其他备注)
     */
    //@Excel(name = "所属技术领域(其他备注)", width = 15, orderNum = "3")
    @ApiModelProperty(value = "所属技术领域(其他备注)")
    private java.lang.String technicalfieldremark;
    /**
     * 创新类型
     */
    @Excel(name = "创新类型", width = 15, orderNum = "3")
    @ApiModelProperty(value = "创新类型")
    private java.lang.String innovatetype;
    /**
     * 预期成果
     */
    @Excel(name = "预期成果", width = 15, orderNum = "4")
    @ApiModelProperty(value = "预期成果")
    private java.lang.String expectresult;
    /**
     * 预期成果(其他备注)
     */
    //@Excel(name = "预期成果(其他备注)", width = 15, orderNum = "4")
    @ApiModelProperty(value = "预期成果(其他备注)")
    private java.lang.String expectresultremark;
    /**
     * 获得国外发明专利
     */
    //@Excel(name = "获得国外发明专利", width = 15, orderNum = "4")
    @ApiModelProperty(value = "获得国外发明专利")
    private java.math.BigDecimal abroad;
    /**
     * 国内发明专利
     */
    //@Excel(name = "国内发明专利", width = 15, orderNum = "4")
    @ApiModelProperty(value = "国内发明专利")
    private java.math.BigDecimal internal;
    /**
     * 其他专利
     */
    //@Excel(name = "其他专利", width = 15, orderNum = "4")
    @ApiModelProperty(value = "其他专利")
    private java.math.BigDecimal otherpatent;
    /**
     * 技术标准
     */
    // @Excel(name = "技术标准", width = 15, orderNum = "4")
    @ApiModelProperty(value = "技术标准")
    private java.lang.String techstandard;
    /**
     * 年度研究内容及考核目标
     */
    @Excel(name = "年度研究内容及考核目标", width = 15, orderNum = "4")
    @ApiModelProperty(value = "年度研究内容及考核目标")
    private java.lang.String annualdemoncontent;
    /**
     * 项目主要研究内容及考核目标
     */
    @Excel(name = "项目主要研究内容及考核目标", width = 15, orderNum = "4")
    @ApiModelProperty(value = "项目主要研究内容及考核目标")
    private java.lang.String prodemoncontent;
    /**
     * T_SCIENCETECHFEASIBLE.guid
     */
//    @Excel(name = "T_SCIENCETECHFEASIBLE.guid", width = 15)
    @ApiModelProperty(value = "T_SCIENCETECHFEASIBLE.guid")
    private java.lang.String sciencetechguid;
    /**
     * 试制设备费
     */
    @Excel(name = "试制设备费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "试制设备费")
    private java.math.BigDecimal equipmenttrial;
    /**
     * 测试化验加工费
     */
    @Excel(name = "测试化验加工费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "测试化验加工费")
    private java.math.BigDecimal processfee;
    /**
     * 燃料动力费
     */
    @Excel(name = "燃料动力费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "燃料动力费")
    private java.math.BigDecimal fuelpowerfee;
    /**
     * 差旅费
     */
    @Excel(name = "差旅费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "差旅费")
    private java.math.BigDecimal travelfee;
    /**
     * 会议费
     */
    @Excel(name = "会议费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "会议费")
    private java.math.BigDecimal meetingfee;
    /**
     * 合作与交流费
     */
    @Excel(name = "合作与交流费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "合作与交流费")
    private java.math.BigDecimal exchangefee;
    /**
     * 劳务费
     */
    @Excel(name = "劳务费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "劳务费")
    private java.math.BigDecimal labourfee;
    /**
     * 管理费
     */
    @Excel(name = "管理费", width = 15, orderNum = "4")
    @ApiModelProperty(value = "管理费")
    private java.math.BigDecimal managefee;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype = new BigDecimal(0);
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15)
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15)
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    /**
     * 国内实用新型专利
     */
    @Excel(name = "国内实用新型专利", width = 15, orderNum = "4")
    @ApiModelProperty(value = "国内实用新型专利")
    private java.math.BigDecimal internalnew;
    /**
     * 1：终止日期延期2:闭合-终止
     */
//    @Excel(name = "1：终止日期延期2:闭合-终止", width = 15)
    @ApiModelProperty(value = "1：终止日期延期2:闭合-终止")
    private java.math.BigDecimal isdealoverdue;
    /**
     * 过期处理时间,2019.10.10 add
     */
//    @Excel(name = "过期处理时间,2019.10.10 add", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "过期处理时间,2019.10.10 add")
    private java.util.Date dealdate;
    /**
     * 过期处理说明,2019.10.10 add
     */
//    @Excel(name = "过期处理说明,2019.10.10 add", width = 15)
    @ApiModelProperty(value = "过期处理说明,2019.10.10 add")
    private java.lang.String dealmark;
    /**
     * word导出文件路径
     */
//    @Excel(name = "word导出文件路径", width = 15)
    @ApiModelProperty(value = "word导出文件路径")
    private String pdfUrl;
    @ApiModelProperty(value = "是否可强制编辑:默认为空，1为可编辑")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private String editStatus;
    @TableField(exist = false)
    private List<ZgsSciencejointunit> zgsSciencejointunitList;
    @TableField(exist = false)
    private List<ZgsTaskscienceparticipant> zgsTaskparticipantList;
    //各不同人员对象集合
    //项目验收管理-科研项目验收申请人员
    @TableField(exist = false)
    private List<ZgsScientificresearcher> zgsScientificresearcherList;
    //项目验收管理-科研项目结题申请人员
    @TableField(exist = false)
    private List<ZgsScientificpostresearcher> zgsScientificpostresearcherList;
    //项目验收结题证书-科研项目验收证书人员
    @TableField(exist = false)
    private List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList;
    //项目验收结题证书-科研项目结题证书人员
    @TableField(exist = false)
    private List<ZgsSpostresearcher> zgsSpostresearcherList;
    //科研项目申报验收经费决算表
    @TableField(exist = false)
    private ZgsScientificexpenditureclear zgsScientificexpenditureclear;
    @TableField(exist = false)
    //科研项目结题经费决算表
    private ZgsScientificpostexpendclear zgsScientificpostexpendclear;
    @TableField(exist = false)
    private List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String bid;
    @TableField(exist = false)
    private String tasknum;
    @TableField(exist = false)
    private String feeStatus;
    /**
     * 0省级科技资金未支持项目,1省级科技资金支持项目
     */
    @ApiModelProperty(value = "0省级科技资金未支持项目,1省级科技资金支持项目")
    private String fundType;
    @ApiModelProperty(value = "逾期类型：默认为空，0逾期半年，1逾期半年以上")
    private String dealtype;
    @TableField(exist = false)
    private ZgsCommonFee zgsCommonFee;
    @TableField(exist = false)
    private Object cstatus;
    //区分是示范项目还是科技项目1、科技项目；0、示范项目
    @TableField(exist = false)
    private String projectType;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    //项目可以变更次数
    private Integer bgcount;
    @TableField(exist = false)
    private Integer isYuQ;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    @TableField(exist = false)
    private String sciencetype;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;
    // 项目完成单位
    @TableField(exist = false)
    private List<ZgsScientificprojectfinish> zgsScientificprojectfinishList;

    @TableField(exist = false)
    private String sbnf;  // 申报年份

}
