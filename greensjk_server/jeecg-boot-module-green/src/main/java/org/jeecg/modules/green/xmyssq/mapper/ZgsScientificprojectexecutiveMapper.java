package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectexecutive;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目验收申请项目执行情况评价
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface ZgsScientificprojectexecutiveMapper extends BaseMapper<ZgsScientificprojectexecutive> {

}
