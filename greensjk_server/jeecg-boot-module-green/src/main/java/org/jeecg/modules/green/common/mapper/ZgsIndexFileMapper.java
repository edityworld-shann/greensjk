package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsIndexFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 登录页文件下载表
 * @Author: jeecg-boot
 * @Date:   2022-03-05
 * @Version: V1.0
 */
public interface ZgsIndexFileMapper extends BaseMapper<ZgsIndexFile> {

}
