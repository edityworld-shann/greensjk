package org.jeecg.modules.green.jhxmbgsq.service.impl;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsTaskchange;
import org.jeecg.modules.green.jhxmbgsq.mapper.ZgsTaskchangeMapper;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsTaskchangeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目变更记录
 * @Author: jeecg-boot
 * @Date:   2022-03-15
 * @Version: V1.0
 */
@Service
public class ZgsTaskchangeServiceImpl extends ServiceImpl<ZgsTaskchangeMapper, ZgsTaskchange> implements IZgsTaskchangeService {

}
