package org.jeecg.modules.green.xmyssq.service;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研项目申报验收基本信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface IZgsScientificbaseService extends IService<ZgsScientificbase> {

}
