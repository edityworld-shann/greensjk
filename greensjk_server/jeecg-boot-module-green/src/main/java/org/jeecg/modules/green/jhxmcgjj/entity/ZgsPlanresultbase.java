package org.jeecg.modules.green.jhxmcgjj.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Column;

/**
 * @Description: 项目成果简介
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_planresultbase")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_planresultbase对象", description = "项目成果简介")
public class ZgsPlanresultbase implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 科研项目申报验收基本信息表ID
     */
//    @Excel(name = "科研项目申报验收基本信息表ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "科研项目申报验收基本信息表ID")
    private java.lang.String projectlibraryguid;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15, orderNum = "2")
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 成果名称
     */
    @Excel(name = "成果名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "成果名称")
    private java.lang.String resultname;
    /**
     * 登记号
     */
    //@Excel(name = "登记号", width = 15, orderNum = "0")
    @ApiModelProperty(value = "登记号")
    private java.lang.String registerno;
    /**
     * 完成单位
     */
    @Excel(name = "完成单位", width = 15, orderNum = "2")
    @ApiModelProperty(value = "完成单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishenterprise;
    /**
     * 项目负责人
     */
    @Excel(name = "项目负责人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleader;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15, orderNum = "2")
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linktel;
    /**
     * 研究开始时间
     */
    @Excel(name = "研究开始时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "研究开始时间")
    private java.util.Date startdate;
    /**
     * 研究结束时间
     */
    @Excel(name = "研究结束时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "研究结束时间")
    private java.util.Date enddate;
    /**
     * 验收时间
     */
    @Excel(name = "验收时间", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "验收时间")
    private java.util.Date receptiontime;
    /**
     * 是否涉密
     */
    @Excel(name = "是否涉密", width = 15, dicCode = "Secret", orderNum = "2")
    @ApiModelProperty(value = "是否涉密")
    private java.lang.String issecurity;
    /**
     * 适用范围
     */
    @Excel(name = "适用范围", width = 15, orderNum = "2")
    @ApiModelProperty(value = "适用范围")
    private java.lang.String applyrange;
    /**
     * 拟推广方向
     */
    @Excel(name = "拟推广方向", width = 15, orderNum = "2")
    @ApiModelProperty(value = "拟推广方向")
    private java.lang.String promotiondirection;
    /**
     * 成果简介
     */
    @Excel(name = "成果简介", width = 15, orderNum = "2")
    @ApiModelProperty(value = "成果简介")
    private java.lang.String resultintro;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "1")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
//    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 审核退回意见
     */
//    @Excel(name = "审核退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * createaccountname
     */
//    @Excel(name = "createaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
//    @Excel(name = "createusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
//    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
//    @Excel(name = "modifyaccountname", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
//    @Excel(name = "modifyusername", width = 15, orderNum = "2")
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
//    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15, orderNum = "2")
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
//    @Excel(name = "初审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15, orderNum = "2")
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 上级推荐部门
     */
//    @Excel(name = "上级推荐部门", width = 15, orderNum = "2")
    @ApiModelProperty(value = "上级推荐部门")
    private java.lang.String higherupunitnum;
    /**
     * 终审退回意见
     */
//    @Excel(name = "终审退回意见", width = 15, orderNum = "2")
    @ApiModelProperty(value = "终审退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String finishauditopinion;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15, orderNum = "2")
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    /**
     * 发布状态（0未发布，1已发布，2已撤销）
     */
    @Excel(name = "发布状态", width = 15, dicCode = "send_status", orderNum = "2")
    @Dict(dicCode = "send_status")
    private java.lang.String sendStatus;
    /**
     * 发布时间
     */
    @Excel(name = "发布时间", width = 15, format = "yyyy-MM-dd HH:mm:ss", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date sendTime;

    @ApiModelProperty(value = "成果展示  1展示   2不展示")
    @Column(name="result_show")
    private java.lang.String resultShow;

    @ApiModelProperty(value = "验收id")
    @Column(name="ysid")
    private java.lang.String ysid;
    /**
     * 撤销时间
     */
//    @Excel(name = "撤销时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date cancelTime;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Integer spStatus;
    @TableField(exist = false)
    private String pdfUrl;
    @TableField(exist = false)
    private Object cstatus;
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    @TableField(exist = false)
    private String appendixpath;
    @TableField(exist = false)
    private String appendixextension;
    @TableField(exist = false)
    private String filename;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;
    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;
    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;

}
