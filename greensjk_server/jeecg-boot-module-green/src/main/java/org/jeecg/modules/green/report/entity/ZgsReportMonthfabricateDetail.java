package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: zgs_report_monthfabricate_detail
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabricate_detail")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_monthfabricate_detail对象", description = "zgs_report_monthfabricate_detail")
public class ZgsReportMonthfabricateDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
    /**
     * REPORT_MONTHFABRICATE_Area.guid
     */
    @Excel(name = "REPORT_MONTHFABRICATE_Area.guid", width = 15)
    @ApiModelProperty(value = "REPORT_MONTHFABRICATE_Area.guid")
    private java.lang.String monthmianguid;
    /**
     * 1:新开工2:已竣工验收
     */
    @Excel(name = "1:新开工2:已竣工验收", width = 15)
    @ApiModelProperty(value = "1:新开工2:已竣工验收")
    private java.math.BigDecimal prosstype;
    /**
     * 删除标志
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "isdelete")
    private java.math.BigDecimal isdelete;
    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "createpersonaccount")
    private java.lang.String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "createpersonname")
    private java.lang.String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createtime")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "modifypersonaccount")
    private java.lang.String modifypersonaccount;
    /**
     * 修改人名称
     */
    @Excel(name = "修改人名称", width = 15)
    @ApiModelProperty(value = "modifypersonname")
    private java.lang.String modifypersonname;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifytime")
    private java.util.Date modifytime;
    /**
     * 本月新开工全装修住宅建筑面积
     */
    @Excel(name = "本月新开工全装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本月新开工全装修住宅建筑面积")
    private java.math.BigDecimal byqzxzzmj;
    /**
     * 本月新开工装配化装修住宅建筑面积
     */
    @Excel(name = "本月新开工装配化装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本月新开工装配化装修住宅建筑面积")
    private java.math.BigDecimal byzphzzmj;
    /**
     * 本月新开工装配式建筑面积在新开工建筑总面积的占比
     */
    @Excel(name = "本月新开工装配式建筑面积在新开工建筑总面积的占比", width = 15)
    @ApiModelProperty(value = "本月新开工装配式建筑面积在新开工建筑总面积的占比")
    private java.math.BigDecimal byzpsmjzb;
    /**
     * 本月新开工建筑总面积
     */
    @Excel(name = "本月新开工建筑总面积", width = 15)
    @ApiModelProperty(value = "本月新开工建筑总面积")
    private java.math.BigDecimal byjzzmj;
    /**
     * 本月新开工装配式建筑项数
     */
    @Excel(name = "本月新开工装配式建筑项数", width = 15)
    @ApiModelProperty(value = "本月新开工装配式建筑项数")
    private java.math.BigDecimal byzpsjzxs;
    /**
     * 本月新开工装配式建筑建筑面积
     */
    @Excel(name = "本月新开工装配式建筑建筑面积", width = 15)
    @ApiModelProperty(value = "本月新开工装配式建筑建筑面积")
    private java.math.BigDecimal byzpsjzmj;
    /**
     * 本月保障性住房项数
     */
    @Excel(name = "本月保障性住房项数", width = 15)
    @ApiModelProperty(value = "本月保障性住房项数")
    private java.math.BigDecimal bybzxzfxs;
    /**
     * 本月保障性住房建筑面积
     */
    @Excel(name = "本月保障性住房建筑面积", width = 15)
    @ApiModelProperty(value = "本月保障性住房建筑面积")
    private java.math.BigDecimal bybzxzfmj;
    /**
     * 本月商品住房项数
     */
    @Excel(name = "本月商品住房项数", width = 15)
    @ApiModelProperty(value = "本月商品住房项数")
    private java.math.BigDecimal byspzfxs;
    /**
     * 本月商品住房建筑面积
     */
    @Excel(name = "本月商品住房建筑面积", width = 15)
    @ApiModelProperty(value = "本月商品住房建筑面积")
    private java.math.BigDecimal byspzfmj;
    /**
     * 本月公共建筑项数
     */
    @Excel(name = "本月公共建筑项数", width = 15)
    @ApiModelProperty(value = "本月公共建筑项数")
    private java.math.BigDecimal byggjzxs;
    /**
     * 本月公共建筑建筑面积
     */
    @Excel(name = "本月公共建筑建筑面积", width = 15)
    @ApiModelProperty(value = "本月公共建筑建筑面积")
    private java.math.BigDecimal byggjzmj;
    /**
     * 本月农村及旅游景观项目项数
     */
    @Excel(name = "本月农村及旅游景观项目项数", width = 15)
    @ApiModelProperty(value = "本月农村及旅游景观项目项数")
    private java.math.BigDecimal bynclyxs;
    /**
     * 本月农村及旅游景观项目建筑面积
     */
    @Excel(name = "本月农村及旅游景观项目建筑面积", width = 15)
    @ApiModelProperty(value = "本月农村及旅游景观项目建筑面积")
    private java.math.BigDecimal bynclymj;
    /**
     * 本月其他建筑类型项数
     */
    @Excel(name = "本月其他建筑类型项数", width = 15)
    @ApiModelProperty(value = "本月其他建筑类型项数")
    private java.math.BigDecimal byqtjzlxxs;
    /**
     * 本月其他建筑类型建筑面积
     */
    @Excel(name = "本月其他建筑类型建筑面积", width = 15)
    @ApiModelProperty(value = "本月其他建筑类型建筑面积")
    private java.math.BigDecimal byqtjzlxmj;
    /**
     * 本月装配式混凝土结构建筑项数
     */
    @Excel(name = "本月装配式混凝土结构建筑项数", width = 15)
    @ApiModelProperty(value = "本月装配式混凝土结构建筑项数")
    private java.math.BigDecimal byzpshntxs;
    /**
     * 本月装配式混凝土结构建筑建筑面积
     */
    @Excel(name = "本月装配式混凝土结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "本月装配式混凝土结构建筑建筑面积")
    private java.math.BigDecimal byzpshntmj;
    /**
     * 本月装配式钢结构建筑项数
     */
    @Excel(name = "本月装配式钢结构建筑项数", width = 15)
    @ApiModelProperty(value = "本月装配式钢结构建筑项数")
    private java.math.BigDecimal byzpsgjgxs;
    /**
     * 本月装配式钢结构建筑建筑面积
     */
    @Excel(name = "本月装配式钢结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "本月装配式钢结构建筑建筑面积")
    private java.math.BigDecimal byzpsgjgmj;
    /**
     * 本月钢结构装配式住宅项数
     */
    @Excel(name = "本月钢结构装配式住宅项数", width = 15)
    @ApiModelProperty(value = "本月钢结构装配式住宅项数")
    private java.math.BigDecimal bygjgzpszzxs;
    /**
     * 本月钢结构装配式住宅建筑面积
     */
    @Excel(name = "本月钢结构装配式住宅建筑面积", width = 15)
    @ApiModelProperty(value = "本月钢结构装配式住宅建筑面积")
    private java.math.BigDecimal bygjgzpszzmj;
    /**
     * 本月装配式木结构建筑项数
     */
    @Excel(name = "本月装配式木结构建筑项数", width = 15)
    @ApiModelProperty(value = "本月装配式木结构建筑项数")
    private java.math.BigDecimal byzpsmjgxs;
    /**
     * 本月装配式木结构建筑建筑面积
     */
    @Excel(name = "本月装配式木结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "本月装配式木结构建筑建筑面积")
    private java.math.BigDecimal byzpsmjgmj;
    /**
     * 本月其他结构形式项数
     */
    @Excel(name = "本月其他结构形式项数", width = 15)
    @ApiModelProperty(value = "本月其他结构形式项数")
    private java.math.BigDecimal byqtjgxsxs;
    /**
     * 本月其他结构形式建筑面积
     */
    @Excel(name = "本月其他结构形式建筑面积", width = 15)
    @ApiModelProperty(value = "本月其他结构形式建筑面积")
    private java.math.BigDecimal byqtjgxsmj;
    /**
     * 自年初累计新开工全装修住宅建筑面积
     */
    @Excel(name = "自年初累计新开工全装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计新开工全装修住宅建筑面积")
    private java.math.BigDecimal ncqzxzzmj;
    /**
     * 自年初累计新开工装配化装修住宅建筑面积
     */
    @Excel(name = "自年初累计新开工装配化装修住宅建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计新开工装配化装修住宅建筑面积")
    private java.math.BigDecimal nczphzzmj;
    /**
     * 自年初累计新开工装配式建筑面积在新开工建筑总面积的占比
     */
    @Excel(name = "自年初累计新开工装配式建筑面积在新开工建筑总面积的占比", width = 15)
    @ApiModelProperty(value = "自年初累计新开工装配式建筑面积在新开工建筑总面积的占比")
    private java.math.BigDecimal nczpsmjzb;
    /**
     * 自年初累计新开工建筑总面积
     */
    @Excel(name = "自年初累计新开工建筑总面积", width = 15)
    @ApiModelProperty(value = "自年初累计新开工建筑总面积")
    private java.math.BigDecimal ncjzzmj;
    /**
     * 自年初累计新开工装配式建筑项数
     */
    @Excel(name = "自年初累计新开工装配式建筑项数", width = 15)
    @ApiModelProperty(value = "自年初累计新开工装配式建筑项数")
    private java.math.BigDecimal nczpsjzxs;
    /**
     * 自年初累计新开工装配式建筑建筑面积
     */
    @Excel(name = "自年初累计新开工装配式建筑建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计新开工装配式建筑建筑面积")
    private java.math.BigDecimal nczpsjzmj;
    /**
     * 自年初累计保障性住房项数
     */
    @Excel(name = "自年初累计保障性住房项数", width = 15)
    @ApiModelProperty(value = "自年初累计保障性住房项数")
    private java.math.BigDecimal ncbzxzfxs;
    /**
     * 自年初累计保障性住房建筑面积
     */
    @Excel(name = "自年初累计保障性住房建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计保障性住房建筑面积")
    private java.math.BigDecimal ncbzxzfmj;
    /**
     * 自年初累计商品住房项数
     */
    @Excel(name = "自年初累计商品住房项数", width = 15)
    @ApiModelProperty(value = "自年初累计商品住房项数")
    private java.math.BigDecimal ncspzfxs;
    /**
     * 自年初累计商品住房建筑面积
     */
    @Excel(name = "自年初累计商品住房建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计商品住房建筑面积")
    private java.math.BigDecimal ncspzfmj;
    /**
     * 自年初累计公共建筑项数
     */
    @Excel(name = "自年初累计公共建筑项数", width = 15)
    @ApiModelProperty(value = "自年初累计公共建筑项数")
    private java.math.BigDecimal ncggjzxs;
    /**
     * 自年初累计公共建筑建筑面积
     */
    @Excel(name = "自年初累计公共建筑建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计公共建筑建筑面积")
    private java.math.BigDecimal ncggjzmj;
    /**
     * 自年初累计农村及旅游景观项目项数
     */
    @Excel(name = "自年初累计农村及旅游景观项目项数", width = 15)
    @ApiModelProperty(value = "自年初累计农村及旅游景观项目项数")
    private java.math.BigDecimal ncnclyxs;
    /**
     * 自年初累计农村及旅游景观项目建筑面积
     */
    @Excel(name = "自年初累计农村及旅游景观项目建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计农村及旅游景观项目建筑面积")
    private java.math.BigDecimal ncnclymj;
    /**
     * 自年初累计其他建筑类型项数
     */
    @Excel(name = "自年初累计其他建筑类型项数", width = 15)
    @ApiModelProperty(value = "自年初累计其他建筑类型项数")
    private java.math.BigDecimal ncqtjzlxxs;
    /**
     * 自年初累计其他建筑类型建筑面积
     */
    @Excel(name = "自年初累计其他建筑类型建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计其他建筑类型建筑面积")
    private java.math.BigDecimal ncqtjzlxmj;
    /**
     * 自年初累计装配式混凝土结构建筑项数
     */
    @Excel(name = "自年初累计装配式混凝土结构建筑项数", width = 15)
    @ApiModelProperty(value = "自年初累计装配式混凝土结构建筑项数")
    private java.math.BigDecimal nczpshntxs;
    /**
     * 自年初累计装配式混凝土结构建筑建筑面积
     */
    @Excel(name = "自年初累计装配式混凝土结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计装配式混凝土结构建筑建筑面积")
    private java.math.BigDecimal nczpshntmj;
    /**
     * 自年初累计装配式钢结构建筑项数
     */
    @Excel(name = "自年初累计装配式钢结构建筑项数", width = 15)
    @ApiModelProperty(value = "自年初累计装配式钢结构建筑项数")
    private java.math.BigDecimal nczpsgjgxs;
    /**
     * 自年初累计装配式钢结构建筑建筑面积
     */
    @Excel(name = "自年初累计装配式钢结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计装配式钢结构建筑建筑面积")
    private java.math.BigDecimal nczpsgjgmj;
    /**
     * 自年初累计钢结构装配式住宅项数
     */
    @Excel(name = "自年初累计钢结构装配式住宅项数", width = 15)
    @ApiModelProperty(value = "自年初累计钢结构装配式住宅项数")
    private java.math.BigDecimal ncgjgzpszzxs;
    /**
     * 自年初累计钢结构装配式住宅建筑面积
     */
    @Excel(name = "自年初累计钢结构装配式住宅建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计钢结构装配式住宅建筑面积")
    private java.math.BigDecimal ncgjgzpszzmj;
    /**
     * 自年初累计装配式木结构建筑项数
     */
    @Excel(name = "自年初累计装配式木结构建筑项数", width = 15)
    @ApiModelProperty(value = "自年初累计装配式木结构建筑项数")
    private java.math.BigDecimal nczpsmjgxs;
    /**
     * 自年初累计装配式木结构建筑建筑面积
     */
    @Excel(name = "自年初累计装配式木结构建筑建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计装配式木结构建筑建筑面积")
    private java.math.BigDecimal nczpsmjgmj;
    /**
     * 自年初累计其他结构形式项数
     */
    @Excel(name = "自年初累计其他结构形式项数", width = 15)
    @ApiModelProperty(value = "自年初累计其他结构形式项数")
    private java.math.BigDecimal ncqtjgxsxs;
    /**
     * 自年初累计其他结构形式建筑面积
     */
    @Excel(name = "自年初累计其他结构形式建筑面积", width = 15)
    @ApiModelProperty(value = "自年初累计其他结构形式建筑面积")
    private java.math.BigDecimal ncqtjgxsmj;

    /**
     * 行政区域
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "行政区域")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
}
