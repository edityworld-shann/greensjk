package org.jeecg.modules.green.task.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 任务书项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-11
 * @Version: V1.0
 */
public interface ZgsTaskprojectmainparticipantMapper extends BaseMapper<ZgsTaskprojectmainparticipant> {

}
