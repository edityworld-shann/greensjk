package org.jeecg.modules.green.report.service.impl;

import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyExistinfo;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyExistinfoMapper;
import org.jeecg.modules.green.report.service.IZgsReportEnergyExistinfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Description: 既有建筑-节能改造项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Service
public class ZgsReportEnergyExistinfoServiceImpl extends ServiceImpl<ZgsReportEnergyExistinfoMapper, ZgsReportEnergyExistinfo> implements IZgsReportEnergyExistinfoService {


    /**
     * 添加
     *
     * @return
     */
    @Override
    public void addIterm(String filltm) {
        try {
            ZgsReportEnergyExistinfo zgsReportEnergyExistinfo = new ZgsReportEnergyExistinfo();
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            zgsReportEnergyExistinfo.setCreatepersonaccount(sysUser.getUsername());
            zgsReportEnergyExistinfo.setCreatepersonname(sysUser.getRealname());
            zgsReportEnergyExistinfo.setCreatetime(new Date());
            zgsReportEnergyExistinfo.setAreacode(sysUser.getAreacode());
            zgsReportEnergyExistinfo.setAreaname(sysUser.getAreaname());
            //计算月份、年、季度
            String year = filltm.substring(0, 4);
            String month = filltm.substring(5, 7);
            int intMonth = Integer.parseInt(month);
            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
            zgsReportEnergyExistinfo.setFilltm(filltm);
            zgsReportEnergyExistinfo.setQuarter(new BigDecimal(quarter));
            zgsReportEnergyExistinfo.setYear(new BigDecimal(year));
            zgsReportEnergyExistinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
            //计算月份、年、季度
            Date format = new Date();
            Date currentTime = new Date(format.getTime());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String tempDate = filltm + "-01 06:00:00";
            Date initDate = simpleDateFormat.parse(tempDate);
            zgsReportEnergyExistinfo.setStartDate(initDate);
            zgsReportEnergyExistinfo.setEndDate(initDate);
            zgsReportEnergyExistinfo.setApplydate(currentTime);
            zgsReportEnergyExistinfo.setBuildingArea(new BigDecimal(0));
            zgsReportEnergyExistinfo.setBuildType("1");
            zgsReportEnergyExistinfo.setContent("无");
            zgsReportEnergyExistinfo.setFillpersonname("无");
            zgsReportEnergyExistinfo.setFillpersontel("无");
            zgsReportEnergyExistinfo.setFillunit("无");
            zgsReportEnergyExistinfo.setProjectname("无");
            this.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfo, ValidateEncryptEntityUtil.isEncrypt));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 添加
     *
     * @return
     */
    @Override
    public void addItermInitTask(String filltm, String createpersonaccount, String createpersonname, String areacode, String areaname) {
        try {
            ZgsReportEnergyExistinfo zgsReportEnergyExistinfo = new ZgsReportEnergyExistinfo();
            zgsReportEnergyExistinfo.setCreatepersonaccount(createpersonaccount);
            zgsReportEnergyExistinfo.setCreatepersonname(createpersonname);
            zgsReportEnergyExistinfo.setCreatetime(new Date());
            zgsReportEnergyExistinfo.setAreacode(areacode);
            zgsReportEnergyExistinfo.setAreaname(areaname);
            //计算月份、年、季度
            String year = filltm.substring(0, 4);
            String month = filltm.substring(5, 7);
            int intMonth = Integer.parseInt(month);
            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
            zgsReportEnergyExistinfo.setFilltm(filltm);
            zgsReportEnergyExistinfo.setQuarter(new BigDecimal(quarter));
            zgsReportEnergyExistinfo.setYear(new BigDecimal(year));
            zgsReportEnergyExistinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            //计算月份、年、季度
            Date format = new Date();
            Date currentTime = new Date(format.getTime());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String tempDate = filltm + "-01 06:00:00";
            Date initDate = simpleDateFormat.parse(tempDate);
            zgsReportEnergyExistinfo.setStartDate(initDate);
            zgsReportEnergyExistinfo.setEndDate(initDate);
            zgsReportEnergyExistinfo.setApplydate(currentTime);
            zgsReportEnergyExistinfo.setBuildingArea(new BigDecimal(0));
            zgsReportEnergyExistinfo.setBuildType("1");
            zgsReportEnergyExistinfo.setContent("无");
            zgsReportEnergyExistinfo.setFillpersonname("无");
            zgsReportEnergyExistinfo.setFillpersontel("无");
            zgsReportEnergyExistinfo.setFillunit("无");
            zgsReportEnergyExistinfo.setProjectname("无");
            this.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyExistinfo, ValidateEncryptEntityUtil.isEncrypt));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
