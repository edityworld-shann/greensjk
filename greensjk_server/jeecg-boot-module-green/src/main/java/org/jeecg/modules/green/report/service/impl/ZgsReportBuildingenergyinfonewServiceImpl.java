package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonew;
import org.jeecg.modules.green.report.mapper.ZgsReportBuildingenergyinfonewMapper;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_buildingenergyinfonew
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportBuildingenergyinfonewServiceImpl extends ServiceImpl<ZgsReportBuildingenergyinfonewMapper, ZgsReportBuildingenergyinfonew> implements IZgsReportBuildingenergyinfonewService {

}
