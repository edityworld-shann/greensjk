package org.jeecg.modules.green.jxzpsq.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancescoreService;
import org.jeecg.modules.green.xmyssq.entity.*;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 绩效自评申请
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "绩效自评申请")
@RestController
@RequestMapping("/jxzpsq/zgsPerformancebase")
@Slf4j
public class ZgsPerformancebaseController extends JeecgController<ZgsPerformancebase, IZgsPerformancebaseService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsPerformancebaseService zgsPerformancebaseService;
    @Autowired
    private IZgsPerformancescoreService zgsPerformancescoreService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    /**
     * 分页列表查询
     *
     * @param zgsPerformancebase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "绩效自评申请-分页列表查询")
    @ApiOperation(value = "绩效自评申请-分页列表查询", notes = "绩效自评申请-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsPerformancebase zgsPerformancebase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsPerformancebase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsPerformancebase.getProjectname())) {
            queryWrapper.like("p.projectname", zgsPerformancebase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsPerformancebase.getProjectnum())) {
            queryWrapper.like("p.projectnum", zgsPerformancebase.getProjectnum());
        }
        // 根据fundType判断是否为省科技经费支持项目
        if (StringUtils.isNotEmpty(zgsPerformancebase.getFundType())) {
            String fundType = zgsPerformancebase.getFundType();   // 0 省科技经费未支持   1 省科技经费支持
            if (fundType.equals("0")) {  // 省科技资金未支持
                queryWrapper.and(ft -> {
                    ft.eq("p.fund_type", 0).or().eq("p.fund_type",null);
                });
            } else { // 省科技资金支持，资金大于0
                queryWrapper.gt("p.fund_type", 0);
            }
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(p.projectnum,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsPerformancebase.getProjectstage())) {
            if (!"-1".equals(zgsPerformancebase.getProjectstage())) {
                queryWrapper.eq("p.projectstage", QueryWrapperUtil.getProjectstage(zgsPerformancebase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                if ("0".equals(zgsPerformancebase.getIntype())) {
                    queryWrapper.and(qrt -> {
                        qrt.eq("p.projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6)).or().isNull("p.projectstage");
                    });
                }
            }
        }
        queryWrapper.eq("p.intype", zgsPerformancebase.getIntype());
//      QueryWrapperUtil.initPerformancebaseSelect(queryWrapper, zgsPerformancebase.getStatus(), sysUser);
        /*if (StringUtils.isNotBlank(zgsPerformancebase.getStatus()) && zgsPerformancebase.getStatus().equals("0")) {
            QueryWrapperUtil.initCommonQueryForJxzp(queryWrapper, zgsPerformancebase.getStatus(), sysUser, 1);
        } else {
            QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, zgsPerformancebase.getStatus(), sysUser, 1);
        }*/

        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("p.status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("p.enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("p.enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("p.enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("p.status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsPerformancebase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("p.finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("p.status", GlobalConstants.SHENHE_STATUS0).ne("p.status", GlobalConstants.SHENHE_STATUS1).ne("p.status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("p.isdelete", 1);
        //TODO 修改根据初审日期进行降序 2022-09-21
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.applydate),0,1), p.applydate DESC");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.applydate),0,1), p.applydate DESC");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.applydate),0,1), p.applydate DESC");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.applydate),0,1), p.applydate DESC");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.firstdate),0,1), p.firstdate DESC");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.last("ORDER BY IF(isnull(p.firstdate),0,1), p.firstdate DESC");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(p.firstdate),0,1), p.firstdate DESC");
            // queryWrapper.last("ORDER BY IF(isnull(p.applydate),0,1), p.applydate DESC");
        }
        Page<ZgsPerformancebase> page = new Page<ZgsPerformancebase>(pageNo, pageSize);
        // Page<ZgsPerformancebase> pageList = zgsPerformancebaseService.page(page, queryWrapper);
        // IPage<ZgsPerformancebase> pageList = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsPerformancebase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsPerformancebase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsPerformancebase> pageList = null;

        // 项目成果简介
        redisUtil.expire("ndjxzp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("ndjxzpOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("ndjxzpOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsPerformancebase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsPerformancebase.getDspForTjdw())) {
            if (zgsPerformancebase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("ndjxzp")) {
                    QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);
                    redisUtil.set("ndjxzp",pageListForProjectCount.getTotal());
                }
            }
            if (zgsPerformancebase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("ndjxzpOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);
                    redisUtil.set("ndjxzpOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsPerformancebase.getDspForTjdw()) && StringUtils.isBlank(zgsPerformancebase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isBlank(zgsPerformancebase.getDspForTjdw()) && StringUtils.isNotBlank(zgsPerformancebase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, "", sysUser, 1);
            pageList = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isNotBlank(zgsPerformancebase.getDspForSt()) && zgsPerformancebase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);
            redisUtil.set("ndjxzpOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            if (StringUtils.isNotBlank(zgsPerformancebase.getStatus()) && zgsPerformancebase.getStatus().equals("0")) {
                QueryWrapperUtil.initCommonQueryForJxzp(queryWrapper, zgsPerformancebase.getStatus(), sysUser, 1);
            } else {
                QueryWrapperUtil.initCommonQueryForDsp(queryWrapper, zgsPerformancebase.getStatus(), sysUser, 1);
            }
            pageList = zgsPerformancebaseService.listZgsPerformancebase(queryWrapper, pageNo, pageSize);
        }


        /*// 年度绩效自评
        redisUtil.expire("ndjxzp",60);
        if (!redisUtil.hasKey("ndjxzp")) {
            if (StringUtils.isNotBlank(zgsPerformancebase.getFlagByWorkTable()) && zgsPerformancebase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("ndjxzp",pageList.getTotal());
            }
        }
        redisUtil.expire("ndjxzpOfDsp",60);
        if (!redisUtil.hasKey("ndjxzpOfDsp")) {
            if (StringUtils.isNotBlank(zgsPerformancebase.getDspForSt()) && zgsPerformancebase.getDspForSt().equals("2")) {
                redisUtil.set("ndjxzpOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("ndjxzpOfTjdw",60);
        if (!redisUtil.hasKey("ndjxzpOfTjdw")) {
            if (StringUtils.isNotBlank(zgsPerformancebase.getDspForTjdw()) && zgsPerformancebase.getDspForTjdw().equals("3")) {
                redisUtil.set("ndjxzpOfTjdw",pageList.getTotal());
            }
        }*/

        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPerformancebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
//                    if (StringUtils.isNotEmpty(zgsInfo.getBaseguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getBaseguid()));
//                    } else {
//                        if ("1".equals(zgsPerformancebase.getIntype())) {
//                            pageList.getRecords().get(m).setCstatus("年度绩效自评阶段");
//                        } else {
//                            pageList.getRecords().get(m).setCstatus("结题验收绩效自评阶段");
//                        }
//                    }
//                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
//                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
//                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                    initData(pageList, m, zgsInfo);
                }
            }
        } else {
            if (pageList.getRecords() != null && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPerformancebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
//                    if (StringUtils.isNotEmpty(zgsInfo.getBaseguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getBaseguid()));
//                    } else {
//                        if ("1".equals(zgsPerformancebase.getIntype())) {
//                            pageList.getRecords().get(m).setCstatus("年度绩效自评阶段");
//                        } else {
//                            pageList.getRecords().get(m).setCstatus("结题验收绩效自评阶段");
//                        }
//                    }
//                    if(StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())){
//                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
//                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    initData(pageList, m, zgsInfo);
                }
            }
        }
        return Result.OK(pageList);
    }

    private void initData(IPage<ZgsPerformancebase> pageList, int m, ZgsPerformancebase zgsInfo) {
        //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
        QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
        queryWrapper0.eq("sciencetechguid", zgsInfo.getBaseguid());
        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper0.ne("isdelete", 1);
        ZgsSciencetechtask zgsSciencetechtask = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechtaskService.getOne(queryWrapper0), ValidateEncryptEntityUtil.isDecrypt);
        //查询项目负责任
        String applyunitprojectleader = zgsPerformancebaseService.zgsInfoOne(zgsInfo.getBaseguid());
        pageList.getRecords().get(m).setProjectleader(applyunitprojectleader);
        if (zgsSciencetechtask != null) {
            if (StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
            }
            pageList.getRecords().get(m).setProjectleader(zgsSciencetechtask.getProjectleader());
            pageList.getRecords().get(m).setHeadmobile(zgsSciencetechtask.getHeadmobile());
        }
        if (StringUtils.isNotEmpty(zgsInfo.getBaseguid()) && StringUtils.isNotEmpty(zgsInfo.getProjectname())) {
            QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<ZgsProjecttask>();
            queryWrapper1.eq("projectguid", zgsInfo.getBaseguid());
            queryWrapper1.eq("status", GlobalConstants.SHENHE_STATUS8);
            queryWrapper1.ne("isdelete", 1);
            ZgsProjecttask zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getOne(queryWrapper1), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjecttask != null) {
                if (StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                }
                if (StringUtils.isNotEmpty(zgsProjecttask.getProjectleader())) {
                    pageList.getRecords().get(m).setProjectleader(zgsProjecttask.getProjectleader());
                } else {
                    if (StringUtils.isNotEmpty(applyunitprojectleader)) {
                        pageList.getRecords().get(m).setProjectleader(applyunitprojectleader);
                    }
                }
                pageList.getRecords().get(m).setHeadmobile(zgsProjecttask.getHeadmobile());
            }
        }
    }

    /**
     * 撤回
     *
     * @param zgsPerformancebase
     * @return
     */
    @AutoLog(value = "绩效自评申请-撤回")
    @ApiOperation(value = "绩效自评申请-撤回", notes = "绩效自评申请-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsPerformancebase zgsPerformancebase) {
        if (zgsPerformancebase != null) {
            String id = zgsPerformancebase.getId();
            String bid = zgsPerformancebase.getBaseguid();
            String status = zgsPerformancebase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status)
                        && (QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6).equals(zgsPerformancebase.getProjectstage()) || "1".equals(zgsPerformancebase.getIntype()))) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsPerformancebase info = new ZgsPerformancebase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    if ("0".equals(zgsPerformancebase.getIntype())) {
                        info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
                        info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    }
                    zgsPerformancebaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsPerformancebase
     * @return
     */
    @AutoLog(value = "绩效自评申请-审批")
    @ApiOperation(value = "绩效自评申请-审批", notes = "绩效自评申请-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsPerformancebase zgsPerformancebase) {
        zgsPerformancebaseService.spProject(zgsPerformancebase);
        String projectlibraryguid = zgsPerformancebaseService.getById(zgsPerformancebase.getId()).getBaseguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsPerformancebase
     * @return
     */
    @AutoLog(value = "绩效自评申请-添加")
    @ApiOperation(value = "绩效自评申请-添加", notes = "绩效自评申请-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsPerformancebase zgsPerformancebase) {

        //        1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsPerformancebase.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsPerformancebase.getStatus())) {
            //保存并上报
            zgsPerformancebase.setApplydate(new Date());
        } else {
            zgsPerformancebase.setApplydate(null);
        }
        zgsPerformancebase.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsPerformancebase.setCreateaccountname(sysUser.getUsername());
        zgsPerformancebase.setCreateusername(sysUser.getRealname());
        zgsPerformancebase.setCreatedate(new Date());
        zgsPerformancebase.setFundType(zgsProjecttaskService.initFundType(zgsPerformancebase.getBaseguid()));
        //0结题验收绩效自评,1年度绩效自评
        if ("0".equals(zgsPerformancebase.getIntype())) {
            zgsPerformancebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
            zgsPerformancebase.setProjectstagestatus(zgsPerformancebase.getStatus());
        } else {
            Object stage = redisUtil.get(zgsPerformancebase.getBaseguid());
            Object stageStatus = redisUtil.get(GlobalConstants.STAGE_STATUS + zgsPerformancebase.getBaseguid());
            if (stage != null) {
                zgsPerformancebase.setProjectstage(stage.toString());
            }
            if (stageStatus != null) {
                zgsPerformancebase.setProjectstagestatus(stageStatus.toString());
            }
        }
        zgsPerformancebaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPerformancebase, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsPerformancebase.getStatus()) && StringUtils.isNotEmpty(zgsPerformancebase.getBaseguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsPerformancebase.getBaseguid());
        }
        //计划项目执行情况绩效自评
        ZgsPerformancescore zgsPerformancescore = zgsPerformancebase.getZgsPerformancescore();
        zgsPerformancescore.setId(UUID.randomUUID().toString());
        zgsPerformancescore.setBaseguid(zgsPerformancebase.getId());
        zgsPerformancescore.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsPerformancescore.setCreateaccountname(sysUser.getUsername());
        zgsPerformancescore.setCreateusername(sysUser.getRealname());
        zgsPerformancescore.setCreatedate(new Date());
        zgsPerformancescoreService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPerformancescore, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsPerformancebase
     * @return
     */
    @AutoLog(value = "绩效自评申请-编辑")
    @ApiOperation(value = "绩效自评申请-编辑", notes = "绩效自评申请-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsPerformancebase zgsPerformancebase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPerformancebase != null && StringUtils.isNotEmpty(zgsPerformancebase.getId())) {
            String id = zgsPerformancebase.getId();
            if (GlobalConstants.handleSubmit.equals(zgsPerformancebase.getStatus())) {
                //保存并上报
                zgsPerformancebase.setApplydate(new Date());
                zgsPerformancebase.setAuditopinion(null);
//                zgsPerformancebase.setReturntype(null);
            } else {
                zgsPerformancebase.setApplydate(null);
                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                ZgsPerformancebase projectTask = zgsPerformancebaseService.getById(id);
                if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                    zgsPerformancebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                }
            }
            zgsPerformancebase.setModifyaccountname(sysUser.getUsername());
            zgsPerformancebase.setModifyusername(sysUser.getRealname());
            zgsPerformancebase.setModifydate(new Date());
            zgsPerformancebase.setCreatedate(null);
            zgsPerformancebase.setFinishdate(null);
            zgsPerformancebase.setFirstdate(null);
            //0结题验收绩效自评,1年度绩效自评
            if ("0".equals(zgsPerformancebase.getIntype())) {
                zgsPerformancebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_6));
                zgsPerformancebase.setProjectstagestatus(zgsPerformancebase.getStatus());
            } else {
                Object stage = redisUtil.get(zgsPerformancebase.getBaseguid());
                Object stageStatus = redisUtil.get(GlobalConstants.STAGE_STATUS + zgsPerformancebase.getBaseguid());
                if (stage != null) {
                    zgsPerformancebase.setProjectstage(stage.toString());
                }
                if (stageStatus != null) {
                    zgsPerformancebase.setProjectstagestatus(stageStatus.toString());
                }
            }
            zgsPerformancebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPerformancebase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsPerformancebase.getStatus()) && StringUtils.isNotEmpty(zgsPerformancebase.getBaseguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsPerformancebase.getBaseguid());
            }
            String enterpriseguid = zgsPerformancebase.getEnterpriseguid();
            //计划项目执行情况绩效自评
            //科研项目验收申请项目执行情况评价实体类
            ZgsPerformancescore zgsPerformancescore = zgsPerformancebase.getZgsPerformancescore();
            if (StringUtils.isNotEmpty(zgsPerformancescore.getId())) {
                //编辑
                zgsPerformancescore.setModifyaccountname(sysUser.getUsername());
                zgsPerformancescore.setModifyusername(sysUser.getRealname());
                zgsPerformancescore.setModifydate(new Date());
                zgsPerformancescoreService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPerformancescore, ValidateEncryptEntityUtil.isEncrypt));
            } else {
                //新增
                zgsPerformancescore.setId(UUID.randomUUID().toString());
                zgsPerformancescore.setBaseguid(id);
                zgsPerformancescore.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsPerformancescore.setCreateaccountname(sysUser.getUsername());
                zgsPerformancescore.setCreateusername(sysUser.getRealname());
                zgsPerformancescore.setCreatedate(new Date());
                zgsPerformancescoreService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPerformancescore, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绩效自评申请-通过id删除")
    @ApiOperation(value = "绩效自评申请-通过id删除", notes = "绩效自评申请-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsPerformancebaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "绩效自评申请-批量删除")
    @ApiOperation(value = "绩效自评申请-批量删除", notes = "绩效自评申请-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsPerformancebaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绩效自评申请-通过id查询")
    @ApiOperation(value = "绩效自评申请-通过id查询", notes = "绩效自评申请-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsPerformancebase zgsPerformancebase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsPerformancebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsPerformancebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsPerformancebase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsPerformancebase);
            }
            if ((zgsPerformancebase.getFirstdate() != null || zgsPerformancebase.getFinishdate() != null) && zgsPerformancebase.getApplydate() != null) {
                zgsPerformancebase.setSpLogStatus(1);
            } else {
                zgsPerformancebase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsPerformancebase = new ZgsPerformancebase();
            zgsPerformancebase.setSpLogStatus(0);
        }
        return Result.OK(zgsPerformancebase);
    }

    private void initProjectSelectById(ZgsPerformancebase zgsPerformancebase) {
        String enterpriseguid = zgsPerformancebase.getEnterpriseguid();
        String buildguid = zgsPerformancebase.getId();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //计划项目执行情况绩效自评
            QueryWrapper<ZgsPerformancescore> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByDesc("baseguid").last("limit 1");
            ZgsPerformancescore zgsPerformancescore = zgsPerformancescoreService.getOne(queryWrapper1);
            zgsPerformancebase.setZgsPerformancescore(ValidateEncryptEntityUtil.validateDecryptObject(zgsPerformancescore, ValidateEncryptEntityUtil.isDecrypt));
            //
            ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getBySciencetechguid(zgsPerformancebase.getBaseguid(), 7);
            if (zgsSciencetechtask != null) {
                zgsPerformancebase.setZgsCommonFee(zgsSciencetechtask.getZgsCommonFee());
                zgsPerformancebase.setZgsTaskbuildfundbudgetList(zgsSciencetechtask.getZgsTaskbuildfundbudgetList());
            } else {
                ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getByProjectGuid(zgsPerformancebase.getBaseguid(), 7);
                if (zgsProjecttask != null) {
                    zgsPerformancebase.setZgsCommonFee(zgsProjecttask.getZgsCommonFee());
                    zgsPerformancebase.setZgsTaskbuildfundbudgetList(zgsProjecttask.getZgsTaskbuildfundbudgetList());
                }
            }
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsPerformancebase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsPerformancebase zgsPerformancebase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsPerformancebase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsPerformancebase> pageList = (IPage<ZgsPerformancebase>) result.getResult();
        List<ZgsPerformancebase> list = pageList.getRecords();
        List<ZgsPerformancebase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "绩效自评申请";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsPerformancebase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsPerformancebase, ZgsPerformancebase.class, "绩效自评申请");
        return mv;

    }

    private String getId(ZgsPerformancebase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsPerformancebase.class);
    }


    /**
     * 通过id查询导出word
     *
     * @param id
     * @return
     */
    @AutoLog(value = "绩效自评申请-通过id查询导出word")
    @ApiOperation(value = "绩效自评申请-通过id查询导出word", notes = "绩效自评申请-通过id查询导出word")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsPerformancebase zgsPerformancebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsPerformancebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsPerformancebase != null) {
            initProjectSelectById(zgsPerformancebase);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "19甘肃省建设绩效自评报告申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsPerformancebase);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

}
