package org.jeecg.modules.green.sfxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenprojectplanarrange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 绿色建筑工程计划进度与安排
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsGreenprojectplanarrangeMapper extends BaseMapper<ZgsGreenprojectplanarrange> {

}
