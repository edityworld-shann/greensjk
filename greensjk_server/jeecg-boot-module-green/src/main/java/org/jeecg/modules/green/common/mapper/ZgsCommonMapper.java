package org.jeecg.modules.green.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.jeecg.common.model.TreeSelectModel;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 共用 Mapper 接口
 * </p>
 *
 * @Author lizhi
 * @since 2022-04-12
 */
public interface ZgsCommonMapper extends BaseMapper<Object> {

    public String queryDictTextByKey(@Param("code") String code, @Param("key") String key);

    public String queryAreaTextByCode(@Param("code") String code);

    /**
     * 根据父级ID查询树节点数据
     *
     * @param pid
     * @return
     */
    public List<TreeSelectModel> queryListByPid(@Param("pid") String pid, @Param("query") Map<String, String> query);

    public String queryApplyYear();
}
