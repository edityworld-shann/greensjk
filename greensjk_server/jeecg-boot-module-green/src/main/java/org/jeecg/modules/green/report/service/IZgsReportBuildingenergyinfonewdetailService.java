package org.jeecg.modules.green.report.service;

import org.jeecg.modules.green.report.entity.ZgsReportBuildingenergyinfonewdetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: zgs_report_buildingenergyinfonewdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
public interface IZgsReportBuildingenergyinfonewdetailService extends IService<ZgsReportBuildingenergyinfonewdetail> {

}
