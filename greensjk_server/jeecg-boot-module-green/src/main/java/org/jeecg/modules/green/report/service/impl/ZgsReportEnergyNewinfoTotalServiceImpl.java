package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfo;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoTotal;
import org.jeecg.modules.green.report.mapper.ZgsReportEnergyNewinfoTotalMapper;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoGreentotalService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoService;
import org.jeecg.modules.green.report.service.IZgsReportEnergyNewinfoTotalService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 新建建筑节能情况汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Service
public class ZgsReportEnergyNewinfoTotalServiceImpl extends ServiceImpl<ZgsReportEnergyNewinfoTotalMapper, ZgsReportEnergyNewinfoTotal> implements IZgsReportEnergyNewinfoTotalService {

    @Autowired
    private IZgsReportEnergyNewinfoService zgsReportEnergyNewinfoService;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService zgsReportEnergyNewinfoGreentotalService;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService zgsReportEnergyNewinfoTotalService;

    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
    @Override
    public void initEnergyNewinfoTotalZero(String year, int quarter, String yearMonth) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //计算当月上报合计
        //一、建筑节能情况
        energyNewinfoTotalZero(sysUser, year, quarter, yearMonth);
    }

    private void energyNewinfoTotalZero(LoginUser sysUser, String year, int quarter, String yearMonth) {
        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
        zgsReportEnergyNewinfoTotal.setApplydate(new Date());
        zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
        zgsReportEnergyNewinfoTotal.setBackreason("");
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoTotal energyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataYear(yearMonth, sysUser.getAreacode(), year);
        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoTotal reportEnergyNewinfoTotal = this.baseMapper.totalMonthNewinfoData(yearMonth, sysUser.getAreacode());
        //3、本月记录+当年最新一条上报记录
        zgsReportEnergyNewinfoTotal.setMonthNumber(0);
        zgsReportEnergyNewinfoTotal.setMonthArea(new BigDecimal(0));
        //
        zgsReportEnergyNewinfoTotal.setYearBuildNumber(energyNewinfoTotal.getYearBuildNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildArea(energyNewinfoTotal.getYearBuildArea());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(energyNewinfoTotal.getYearBuildhouseNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(energyNewinfoTotal.getYearBuildhouseArea());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(energyNewinfoTotal.getYearBuildpublicNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(energyNewinfoTotal.getYearBuildpublicArea());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(energyNewinfoTotal.getYearBuildindustryNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(energyNewinfoTotal.getYearBuildindustryArea());
        zgsReportEnergyNewinfoTotal.setYearLowNumber(energyNewinfoTotal.getYearLowNumber());
        zgsReportEnergyNewinfoTotal.setYearLowArea(energyNewinfoTotal.getYearLowArea());
        zgsReportEnergyNewinfoTotal.setYearZeroNumber(energyNewinfoTotal.getYearZeroNumber());
        zgsReportEnergyNewinfoTotal.setYearZeroArea(energyNewinfoTotal.getYearZeroArea());
        zgsReportEnergyNewinfoTotal.setYearSolarNumber(energyNewinfoTotal.getYearSolarNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarArea(energyNewinfoTotal.getYearSolarArea());
        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(energyNewinfoTotal.getYearSolarHighNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(energyNewinfoTotal.getYearSolarHighArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(energyNewinfoTotal.getYearGeothermalNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(energyNewinfoTotal.getYearGeothermalArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(energyNewinfoTotal.getYearGeothermalHighNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(energyNewinfoTotal.getYearGeothermalHighArea());
        zgsReportEnergyNewinfoTotal.setYearOtherNumber(energyNewinfoTotal.getYearOtherNumber());
        zgsReportEnergyNewinfoTotal.setYearOtherArea(energyNewinfoTotal.getYearOtherArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", sysUser.getAreacode());
        queryW.isNull("area_type");
        ZgsReportEnergyNewinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportEnergyNewinfoTotal.setId(energyInfo.getId());
            zgsReportEnergyNewinfoTotal.setModifypersonaccount(sysUser.getUsername());
            zgsReportEnergyNewinfoTotal.setModifypersonname(sysUser.getRealname());
            zgsReportEnergyNewinfoTotal.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            zgsReportEnergyNewinfoTotal.setId(UUID.randomUUID().toString());
            zgsReportEnergyNewinfoTotal.setCreatepersonaccount(sysUser.getUsername());
            zgsReportEnergyNewinfoTotal.setCreatepersonname(sysUser.getRealname());
            zgsReportEnergyNewinfoTotal.setCreatetime(new Date());
            zgsReportEnergyNewinfoTotal.setAreacode(sysUser.getAreacode());
            zgsReportEnergyNewinfoTotal.setAreaname(sysUser.getAreaname());
            zgsReportEnergyNewinfoTotal.setFilltm(yearMonth);
            zgsReportEnergyNewinfoTotal.setQuarter(new BigDecimal(quarter));
            zgsReportEnergyNewinfoTotal.setYear(new BigDecimal(year));
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        }
    }
    // ====--====--====--====--====-- 0 上报操作 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--


    @Override
    public void energyNewinfoTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        //计算当月上报合计
        //一、建筑节能情况
        energyNewinfoTotalChildAll(type, year, quarter, yearMonth, areacode, areaname, applyState, sysUser);
        if (type == 0) {
            //只有手动上报才可以新增,数据同步时无需新增
            //上报完修改上报状态（包含绿色建筑）
            UpdateWrapper<ZgsReportEnergyNewinfo> wrapper = new UpdateWrapper();
            wrapper.eq("filltm", yearMonth);
            wrapper.eq("areacode", areacode);
            wrapper.isNotNull("end_date");
            // 原有方法上报时将开工时间、竣工时间默认进行了置空，故此修改 更新方法为UpdateWrapper   rxl 20230725
            wrapper.set("applystate", new BigDecimal(applyState));
            // wrapper.set("backreason", "");
            wrapper.set("applydate", new Date());
            /*wrapper.set("building_area", null);
            wrapper.set("low_energy", null);
            wrapper.set("zero_energy", null);
            wrapper.set("geothermal_energy", null);
            wrapper.set("geothermal_energy_high", null);
            wrapper.set("solar_energy", null);
            wrapper.set("solar_energy_high", null);
            wrapper.set("other_energy", null);*/

            // ZgsReportEnergyNewinfo energyNewinfo = new ZgsReportEnergyNewinfo();
            // energyNewinfo.setApplystate(new BigDecimal(applyState));
            // energyNewinfo.setBackreason("");
            // energyNewinfo.setApplydate(new Date());
            // energyNewinfo.setBuildingArea(null);
            // energyNewinfo.setLowEnergy(null);
            // energyNewinfo.setZeroEnergy(null);
            // energyNewinfo.setGeothermalEnergy(null);
            // energyNewinfo.setGeothermalEnergyHigh(null);
            // energyNewinfo.setSolarEnergy(null);
            // energyNewinfo.setSolarEnergyHigh(null);
            // energyNewinfo.setOtherEnergy(null);
            zgsReportEnergyNewinfoService.update(null, wrapper);
        }
    }

    private void energyNewinfoTotalChildAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser) {
        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
        if (type == 0) {
            zgsReportEnergyNewinfoTotal.setApplydate(new Date());
            zgsReportEnergyNewinfoTotal.setBackreason("");
        }
        if (applyState != null) {
            zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoTotal energyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataYear(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyNewinfoTotal reportEnergyNewinfoTotal = this.baseMapper.totalMonthNewinfoData(yearMonth, areacode);
        //3、本月记录+当年最新一条上报记录
        zgsReportEnergyNewinfoTotal.setMonthNumber(reportEnergyNewinfoTotal.getMonthNumber());
        zgsReportEnergyNewinfoTotal.setMonthArea(reportEnergyNewinfoTotal.getMonthArea());
        //
        zgsReportEnergyNewinfoTotal.setYearBuildNumber(energyNewinfoTotal.getYearBuildNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildArea(energyNewinfoTotal.getYearBuildArea());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(energyNewinfoTotal.getYearBuildhouseNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(energyNewinfoTotal.getYearBuildhouseArea());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(energyNewinfoTotal.getYearBuildpublicNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(energyNewinfoTotal.getYearBuildpublicArea());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(energyNewinfoTotal.getYearBuildindustryNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(energyNewinfoTotal.getYearBuildindustryArea());
        zgsReportEnergyNewinfoTotal.setYearLowNumber(energyNewinfoTotal.getYearLowNumber());
        zgsReportEnergyNewinfoTotal.setYearLowArea(energyNewinfoTotal.getYearLowArea());
        zgsReportEnergyNewinfoTotal.setYearZeroNumber(energyNewinfoTotal.getYearZeroNumber());
        zgsReportEnergyNewinfoTotal.setYearZeroArea(energyNewinfoTotal.getYearZeroArea());
        zgsReportEnergyNewinfoTotal.setYearSolarNumber(energyNewinfoTotal.getYearSolarNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarArea(energyNewinfoTotal.getYearSolarArea());
        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(energyNewinfoTotal.getYearSolarHighNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(energyNewinfoTotal.getYearSolarHighArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(energyNewinfoTotal.getYearGeothermalNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(energyNewinfoTotal.getYearGeothermalArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(energyNewinfoTotal.getYearGeothermalHighNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(energyNewinfoTotal.getYearGeothermalHighArea());
        zgsReportEnergyNewinfoTotal.setYearOtherNumber(energyNewinfoTotal.getYearOtherNumber());
        zgsReportEnergyNewinfoTotal.setYearOtherArea(energyNewinfoTotal.getYearOtherArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNull("area_type");
        ZgsReportEnergyNewinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportEnergyNewinfoTotal.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportEnergyNewinfoTotal.setModifypersonaccount(sysUser.getUsername());
                zgsReportEnergyNewinfoTotal.setModifypersonname(sysUser.getRealname());
            }
            zgsReportEnergyNewinfoTotal.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            if (type == 0) {
                //只有手动上报才可以新增,数据同步时无需新增
                //新增
                zgsReportEnergyNewinfoTotal.setId(UUID.randomUUID().toString());
                if (sysUser != null) {
                    zgsReportEnergyNewinfoTotal.setCreatepersonaccount(sysUser.getUsername());
                    zgsReportEnergyNewinfoTotal.setCreatepersonname(sysUser.getRealname());
                }
                zgsReportEnergyNewinfoTotal.setCreatetime(new Date());
                zgsReportEnergyNewinfoTotal.setAreacode(areacode);
                zgsReportEnergyNewinfoTotal.setAreaname(areaname);
                zgsReportEnergyNewinfoTotal.setFilltm(yearMonth);
                zgsReportEnergyNewinfoTotal.setQuarter(new BigDecimal(quarter));
                zgsReportEnergyNewinfoTotal.setYear(new BigDecimal(year));
                this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
    }


//    @Override
//    public void initEnergyNewinfoTotal(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        //计算当月上报合计
//        //一、建筑节能情况
//        energyNewinfoTotal(sysUser, year, quarter, yearMonth);
//        //上报完修改上报状态（包含绿色建筑）
//        QueryWrapper<ZgsReportEnergyNewinfo> wrapper = new QueryWrapper();
//        wrapper.eq("filltm", yearMonth);
//        wrapper.eq("areacode", sysUser.getAreacode());
//        wrapper.isNotNull("end_date");
//        ZgsReportEnergyNewinfo energyNewinfo = new ZgsReportEnergyNewinfo();
//        energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        energyNewinfo.setBackreason("");
//        energyNewinfo.setApplydate(new Date());
//        energyNewinfo.setBuildingArea(null);
//        energyNewinfo.setLowEnergy(null);
//        energyNewinfo.setZeroEnergy(null);
//        energyNewinfo.setGeothermalEnergy(null);
//        energyNewinfo.setGeothermalEnergyHigh(null);
//        energyNewinfo.setSolarEnergy(null);
//        energyNewinfo.setSolarEnergyHigh(null);
//        energyNewinfo.setOtherEnergy(null);
//        zgsReportEnergyNewinfoService.update(energyNewinfo, wrapper);
//    }

//    private void energyNewinfoTotal(LoginUser sysUser, String year, int quarter, String yearMonth) {
//        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
//        zgsReportEnergyNewinfoTotal.setApplydate(new Date());
//        zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportEnergyNewinfoTotal.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyNewinfoTotal energyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataYear(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoTotal reportEnergyNewinfoTotal = this.baseMapper.totalMonthNewinfoData(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        zgsReportEnergyNewinfoTotal.setMonthNumber(reportEnergyNewinfoTotal.getMonthNumber());
//        zgsReportEnergyNewinfoTotal.setMonthArea(reportEnergyNewinfoTotal.getMonthArea());
//        //
//        zgsReportEnergyNewinfoTotal.setYearBuildNumber(energyNewinfoTotal.getYearBuildNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildArea(energyNewinfoTotal.getYearBuildArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(energyNewinfoTotal.getYearBuildhouseNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(energyNewinfoTotal.getYearBuildhouseArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(energyNewinfoTotal.getYearBuildpublicNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(energyNewinfoTotal.getYearBuildpublicArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(energyNewinfoTotal.getYearBuildindustryNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(energyNewinfoTotal.getYearBuildindustryArea());
//        zgsReportEnergyNewinfoTotal.setYearLowNumber(energyNewinfoTotal.getYearLowNumber());
//        zgsReportEnergyNewinfoTotal.setYearLowArea(energyNewinfoTotal.getYearLowArea());
//        zgsReportEnergyNewinfoTotal.setYearZeroNumber(energyNewinfoTotal.getYearZeroNumber());
//        zgsReportEnergyNewinfoTotal.setYearZeroArea(energyNewinfoTotal.getYearZeroArea());
//        zgsReportEnergyNewinfoTotal.setYearSolarNumber(energyNewinfoTotal.getYearSolarNumber());
//        zgsReportEnergyNewinfoTotal.setYearSolarArea(energyNewinfoTotal.getYearSolarArea());
//        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(energyNewinfoTotal.getYearSolarHighNumber());
//        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(energyNewinfoTotal.getYearSolarHighArea());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(energyNewinfoTotal.getYearGeothermalNumber());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(energyNewinfoTotal.getYearGeothermalArea());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(energyNewinfoTotal.getYearGeothermalHighNumber());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(energyNewinfoTotal.getYearGeothermalHighArea());
//        zgsReportEnergyNewinfoTotal.setYearOtherNumber(energyNewinfoTotal.getYearOtherNumber());
//        zgsReportEnergyNewinfoTotal.setYearOtherArea(energyNewinfoTotal.getYearOtherArea());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNull("area_type");
//        ZgsReportEnergyNewinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportEnergyNewinfoTotal.setId(energyInfo.getId());
//            zgsReportEnergyNewinfoTotal.setModifypersonaccount(sysUser.getUsername());
//            zgsReportEnergyNewinfoTotal.setModifypersonname(sysUser.getRealname());
//            zgsReportEnergyNewinfoTotal.setModifytime(new Date());
//            this.baseMapper.updateById(zgsReportEnergyNewinfoTotal);
//        } else {
//            //新增
//            zgsReportEnergyNewinfoTotal.setId(UUID.randomUUID().toString());
//            zgsReportEnergyNewinfoTotal.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportEnergyNewinfoTotal.setCreatepersonname(sysUser.getRealname());
//            zgsReportEnergyNewinfoTotal.setCreatetime(new Date());
//            zgsReportEnergyNewinfoTotal.setAreacode(sysUser.getAreacode());
//            zgsReportEnergyNewinfoTotal.setAreaname(sysUser.getAreaname());
//            zgsReportEnergyNewinfoTotal.setFilltm(yearMonth);
//            zgsReportEnergyNewinfoTotal.setQuarter(new BigDecimal(quarter));
//            zgsReportEnergyNewinfoTotal.setYear(new BigDecimal(year));
//            this.baseMapper.insert(zgsReportEnergyNewinfoTotal);
//        }
//    }

//    @Override
//    public void initEnergyNewinfoTotalCity(String year, int quarter, String yearMonth) {
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
//        zgsReportEnergyNewinfoTotal.setApplydate(new Date());
//        zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
//        zgsReportEnergyNewinfoTotal.setBackreason("");
//        //1、新方案：每次上报重新计算年累计值
//        ZgsReportEnergyNewinfoTotal energyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataYearCity(yearMonth, sysUser.getAreacode(), year);
//        //2、计算本月所有汇总记录
//        ZgsReportEnergyNewinfoTotal reportEnergyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataCity(yearMonth, sysUser.getAreacode());
//        //3、本月记录+当年最新一条上报记录
//        zgsReportEnergyNewinfoTotal.setMonthNumber(reportEnergyNewinfoTotal.getMonthNumber());
//        zgsReportEnergyNewinfoTotal.setMonthArea(reportEnergyNewinfoTotal.getMonthArea());
//        //
//        zgsReportEnergyNewinfoTotal.setYearBuildNumber(energyNewinfoTotal.getYearBuildNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildArea(energyNewinfoTotal.getYearBuildArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(energyNewinfoTotal.getYearBuildhouseNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(energyNewinfoTotal.getYearBuildhouseArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(energyNewinfoTotal.getYearBuildpublicNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(energyNewinfoTotal.getYearBuildpublicArea());
//        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(energyNewinfoTotal.getYearBuildindustryNumber());
//        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(energyNewinfoTotal.getYearBuildindustryArea());
//        zgsReportEnergyNewinfoTotal.setYearLowNumber(energyNewinfoTotal.getYearLowNumber());
//        zgsReportEnergyNewinfoTotal.setYearLowArea(energyNewinfoTotal.getYearLowArea());
//        zgsReportEnergyNewinfoTotal.setYearZeroNumber(energyNewinfoTotal.getYearZeroNumber());
//        zgsReportEnergyNewinfoTotal.setYearZeroArea(energyNewinfoTotal.getYearZeroArea());
//        zgsReportEnergyNewinfoTotal.setYearSolarNumber(energyNewinfoTotal.getYearSolarNumber());
//        zgsReportEnergyNewinfoTotal.setYearSolarArea(energyNewinfoTotal.getYearSolarArea());
//        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(energyNewinfoTotal.getYearSolarHighNumber());
//        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(energyNewinfoTotal.getYearSolarHighArea());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(energyNewinfoTotal.getYearGeothermalNumber());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(energyNewinfoTotal.getYearGeothermalArea());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(energyNewinfoTotal.getYearGeothermalHighNumber());
//        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(energyNewinfoTotal.getYearGeothermalHighArea());
//        zgsReportEnergyNewinfoTotal.setYearOtherNumber(energyNewinfoTotal.getYearOtherNumber());
//        zgsReportEnergyNewinfoTotal.setYearOtherArea(energyNewinfoTotal.getYearOtherArea());
//        //判断是编辑还是新增
//        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryW = new QueryWrapper();
//        queryW.eq("filltm", yearMonth);
//        queryW.eq("areacode", sysUser.getAreacode());
//        queryW.isNotNull("area_type");
//        ZgsReportEnergyNewinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
//        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
//            //编辑
//            zgsReportEnergyNewinfoTotal.setId(energyInfo.getId());
//            zgsReportEnergyNewinfoTotal.setModifypersonaccount(sysUser.getUsername());
//            zgsReportEnergyNewinfoTotal.setModifypersonname(sysUser.getRealname());
//            zgsReportEnergyNewinfoTotal.setModifytime(new Date());
//            this.baseMapper.updateById(zgsReportEnergyNewinfoTotal);
//        } else {
//            //新增
//            zgsReportEnergyNewinfoTotal.setId(UUID.randomUUID().toString());
//            zgsReportEnergyNewinfoTotal.setCreatepersonaccount(sysUser.getUsername());
//            zgsReportEnergyNewinfoTotal.setCreatepersonname(sysUser.getRealname());
//            zgsReportEnergyNewinfoTotal.setCreatetime(new Date());
//            zgsReportEnergyNewinfoTotal.setAreacode(sysUser.getAreacode());
//            zgsReportEnergyNewinfoTotal.setAreaname(sysUser.getAreaname());
//            zgsReportEnergyNewinfoTotal.setFilltm(yearMonth);
//            zgsReportEnergyNewinfoTotal.setQuarter(new BigDecimal(quarter));
//            zgsReportEnergyNewinfoTotal.setYear(new BigDecimal(year));
//            zgsReportEnergyNewinfoTotal.setAreaType("1");
//            this.baseMapper.insert(zgsReportEnergyNewinfoTotal);
//        }
//    }

    @Override
    public void initEnergyNewinfoTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState) {
        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
        if (type == 0) {
            zgsReportEnergyNewinfoTotal.setApplydate(new Date());
            zgsReportEnergyNewinfoTotal.setBackreason("");
        }
        if (applyState != null) {
            zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(applyState));
        }
        //1、新方案：每次上报重新计算年累计值
        ZgsReportEnergyNewinfoTotal energyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataYearCity(yearMonth, areacode, year);
        //2、计算本月所有汇总记录
        ZgsReportEnergyNewinfoTotal reportEnergyNewinfoTotal = this.baseMapper.totalMonthNewinfoDataCity(yearMonth, areacode);
        //以上方案取消，市区计算县区合计值，只计算通过的数据合计值，待审核和已退回不计入合计值内
//        ZgsReportEnergyNewinfoTotal cityTotal = lastTotalDataCity(yearMonth, areacode, 2);
        //3、本月记录+当年最新一条上报记录
        zgsReportEnergyNewinfoTotal.setMonthNumber(reportEnergyNewinfoTotal.getMonthNumber());
        zgsReportEnergyNewinfoTotal.setMonthArea(reportEnergyNewinfoTotal.getMonthArea());
        //
        zgsReportEnergyNewinfoTotal.setYearBuildNumber(energyNewinfoTotal.getYearBuildNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildArea(energyNewinfoTotal.getYearBuildArea());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(energyNewinfoTotal.getYearBuildhouseNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(energyNewinfoTotal.getYearBuildhouseArea());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(energyNewinfoTotal.getYearBuildpublicNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(energyNewinfoTotal.getYearBuildpublicArea());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(energyNewinfoTotal.getYearBuildindustryNumber());
        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(energyNewinfoTotal.getYearBuildindustryArea());
        zgsReportEnergyNewinfoTotal.setYearLowNumber(energyNewinfoTotal.getYearLowNumber());
        zgsReportEnergyNewinfoTotal.setYearLowArea(energyNewinfoTotal.getYearLowArea());
        zgsReportEnergyNewinfoTotal.setYearZeroNumber(energyNewinfoTotal.getYearZeroNumber());
        zgsReportEnergyNewinfoTotal.setYearZeroArea(energyNewinfoTotal.getYearZeroArea());
        zgsReportEnergyNewinfoTotal.setYearSolarNumber(energyNewinfoTotal.getYearSolarNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarArea(energyNewinfoTotal.getYearSolarArea());
        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(energyNewinfoTotal.getYearSolarHighNumber());
        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(energyNewinfoTotal.getYearSolarHighArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(energyNewinfoTotal.getYearGeothermalNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(energyNewinfoTotal.getYearGeothermalArea());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(energyNewinfoTotal.getYearGeothermalHighNumber());
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(energyNewinfoTotal.getYearGeothermalHighArea());
        zgsReportEnergyNewinfoTotal.setYearOtherNumber(energyNewinfoTotal.getYearOtherNumber());
        zgsReportEnergyNewinfoTotal.setYearOtherArea(energyNewinfoTotal.getYearOtherArea());
        //判断是编辑还是新增
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryW = new QueryWrapper();
        queryW.eq("filltm", yearMonth);
        queryW.eq("areacode", areacode);
        queryW.isNotNull("area_type");
        ZgsReportEnergyNewinfoTotal energyInfo = this.baseMapper.selectOne(queryW);
        if (energyInfo != null && StringUtils.isNotEmpty(energyInfo.getId())) {
            //编辑
            zgsReportEnergyNewinfoTotal.setId(energyInfo.getId());
            if (sysUser != null) {
                zgsReportEnergyNewinfoTotal.setModifypersonaccount(sysUser.getUsername());
                zgsReportEnergyNewinfoTotal.setModifypersonname(sysUser.getRealname());
            }
            zgsReportEnergyNewinfoTotal.setModifytime(new Date());
            this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            //新增
            zgsReportEnergyNewinfoTotal.setId(UUID.randomUUID().toString());
            if (sysUser != null) {
                zgsReportEnergyNewinfoTotal.setCreatepersonaccount(sysUser.getUsername());
                zgsReportEnergyNewinfoTotal.setCreatepersonname(sysUser.getRealname());
            }
            zgsReportEnergyNewinfoTotal.setCreatetime(new Date());
            zgsReportEnergyNewinfoTotal.setAreacode(areacode);
            zgsReportEnergyNewinfoTotal.setAreaname(areaname);
            zgsReportEnergyNewinfoTotal.setFilltm(yearMonth);
            zgsReportEnergyNewinfoTotal.setQuarter(new BigDecimal(quarter));
            zgsReportEnergyNewinfoTotal.setYear(new BigDecimal(year));
            zgsReportEnergyNewinfoTotal.setAreaType("1");
            this.baseMapper.insert(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportEnergyNewinfoTotal, ValidateEncryptEntityUtil.isEncrypt));
        }
    }

    @Override
    public void spProject(String id, Integer spStatus, String backreason) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();


        // 绿色建筑汇总  rxl 20230727
        UpdateWrapper<ZgsReportEnergyNewinfoGreentotal> zgsReportEnergyNewinfoGreentotalUpdateWrapper = new UpdateWrapper();

        UpdateWrapper<ZgsReportEnergyNewinfoGreentotal> zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx = new UpdateWrapper();

        // 新建建筑汇总  rxl 20230727
        UpdateWrapper<ZgsReportEnergyNewinfoTotal> zgsReportEnergyNewinfoTotalUpdateWrapper = new UpdateWrapper();

        // 清单(新建建筑+绿色建筑同一张表) 修改方法  rxl 20230727
        UpdateWrapper<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfoUpdateWrapper = new UpdateWrapper();
        boolean spBol = false;



        //市州及县区绿色建筑汇总全部被退回
        ZgsReportEnergyNewinfoGreentotal zgsReportEnergyNewinfoGreentotal = new ZgsReportEnergyNewinfoGreentotal();
        zgsReportEnergyNewinfoGreentotal.setMonthNumber(null);
        zgsReportEnergyNewinfoGreentotal.setMonthArea(null);
        zgsReportEnergyNewinfoGreentotal.setMonthNewEndArea(null);
        zgsReportEnergyNewinfoGreentotal.setMonthZb(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildhouseNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildhouseArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildpublicArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildpublicNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildindustryArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearBuildindustryNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearNewEndArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearZb(null);
        zgsReportEnergyNewinfoGreentotal.setYearGreenNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearGreenArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber1(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea1(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber2(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea2(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankNumber3(null);
        zgsReportEnergyNewinfoGreentotal.setYearRankArea3(null);
        zgsReportEnergyNewinfoGreentotal.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
        zgsReportEnergyNewinfoGreentotal.setBackreason(backreason);

        // 新建建筑汇总
        ZgsReportEnergyNewinfoTotal zgsReportEnergyNewinfoTotal = new ZgsReportEnergyNewinfoTotal();
        zgsReportEnergyNewinfoTotal.setId(id);
        zgsReportEnergyNewinfoTotal.setMonthNumber(null);
        zgsReportEnergyNewinfoTotal.setMonthArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildhouseNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildhouseArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildpublicArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildpublicNumber(null);
        zgsReportEnergyNewinfoTotal.setYearBuildindustryArea(null);
        zgsReportEnergyNewinfoTotal.setYearBuildindustryNumber(null);
        zgsReportEnergyNewinfoTotal.setYearLowArea(null);
        zgsReportEnergyNewinfoTotal.setYearLowNumber(null);
        zgsReportEnergyNewinfoTotal.setYearZeroArea(null);
        zgsReportEnergyNewinfoTotal.setYearZeroNumber(null);
        zgsReportEnergyNewinfoTotal.setYearSolarArea(null);
        zgsReportEnergyNewinfoTotal.setYearSolarNumber(null);
        zgsReportEnergyNewinfoTotal.setYearSolarHighArea(null);
        zgsReportEnergyNewinfoTotal.setYearSolarHighNumber(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalArea(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalNumber(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighArea(null);
        zgsReportEnergyNewinfoTotal.setYearGeothermalHighNumber(null);
        zgsReportEnergyNewinfoTotal.setYearOtherArea(null);
        zgsReportEnergyNewinfoTotal.setYearOtherNumber(null);

        //修改项目清单的状态
        ZgsReportEnergyNewinfoTotal newinfoTotal = this.baseMapper.selectById(id);

        ZgsReportEnergyNewinfo energyNewinfo = new ZgsReportEnergyNewinfo();
        energyNewinfo.setBuildingArea(null);
        energyNewinfo.setLowEnergy(null);
        energyNewinfo.setZeroEnergy(null);
        energyNewinfo.setGeothermalEnergy(null);
        energyNewinfo.setGeothermalEnergyHigh(null);
        energyNewinfo.setSolarEnergy(null);
        energyNewinfo.setSolarEnergyHigh(null);
        energyNewinfo.setOtherEnergy(null);
        if (spStatus == 0) {

            //驳回
            // 修改清单状态（绿色清单+新建建筑清单）
            zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state1));
            zgsReportEnergyNewinfoUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoTotal.setBackreason(backreason);


            // 修改新建建筑汇总
            zgsReportEnergyNewinfoTotalUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoTotalUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoTotalUpdateWrapper.eq("filltm", newinfoTotal.getFilltm());
            zgsReportEnergyNewinfoTotalUpdateWrapper.eq("areacode", newinfoTotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                zgsReportEnergyNewinfoTotalUpdateWrapper.isNotNull("area_type");
            } else {
                zgsReportEnergyNewinfoTotalUpdateWrapper.isNull("area_type");
            }
            zgsReportEnergyNewinfoTotalService.update(null, zgsReportEnergyNewinfoTotalUpdateWrapper);


            // 修改绿色建筑汇总
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.set("backreason",backreason);

            zgsReportEnergyNewinfoGreentotalUpdateWrapper.eq("filltm", newinfoTotal.getFilltm());
            zgsReportEnergyNewinfoGreentotalUpdateWrapper.eq("areacode", newinfoTotal.getAreacode());
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                zgsReportEnergyNewinfoGreentotalUpdateWrapper.isNotNull("area_type");
            } else {
                zgsReportEnergyNewinfoGreentotalUpdateWrapper.isNull("area_type");
            }
            zgsReportEnergyNewinfoGreentotalService.update(null, zgsReportEnergyNewinfoGreentotalUpdateWrapper);

            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                //省厅账号退回后，市州汇总表子项状态改为待审批
                ZgsReportEnergyNewinfoTotal info = new ZgsReportEnergyNewinfoTotal();
                BeanUtils.copyProperties(zgsReportEnergyNewinfoTotal, info);
                info.setId(null);
                info.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                info.setBackreason("");
                QueryWrapper<ZgsReportEnergyNewinfoTotal> wrapper = new QueryWrapper();
                wrapper.eq("filltm", newinfoTotal.getFilltm());
                wrapper.likeRight("areacode", newinfoTotal.getAreacode());
                wrapper.isNull("area_type");
                this.baseMapper.update(info, wrapper);

                //省退市，删掉该条汇总数据-不留痕
//                this.baseMapper.deleteById(id);
                //子项修改状态
                // totalInfo.setApplystate(new BigDecimal(GlobalConstants.apply_state1));
                zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx.set("applystate",new BigDecimal(GlobalConstants.apply_state1));
                //省退市各个县区默认不填退回原因
                // totalInfo.setBackreason("");
                zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx.set("backreason","");
                /*QueryWrapper<ZgsReportEnergyNewinfoGreentotal> wrapperAllSon = new QueryWrapper();
                wrapperAllSon.eq("filltm", newinfoTotal.getFilltm());
                wrapperAllSon.likeRight("areacode", newinfoTotal.getAreacode());
                wrapperAllSon.isNull("area_type");*/
                zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx.eq("filltm", newinfoTotal.getFilltm());
                zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx.likeRight("areacode", newinfoTotal.getAreacode());
                zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx.isNull("area_type");
                zgsReportEnergyNewinfoGreentotalService.update(null, zgsReportEnergyNewinfoGreentotalUpdateWrapperForZx);


                // 修改清单状态
                zgsReportEnergyNewinfoUpdateWrapper.eq("filltm", newinfoTotal.getFilltm());
                zgsReportEnergyNewinfoUpdateWrapper.likeRight("areacode", newinfoTotal.getAreacode());
                this.zgsReportEnergyNewinfoService.update(null, zgsReportEnergyNewinfoUpdateWrapper);

            }
        } else {
            spBol =true;
            //通过
            energyNewinfo.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            energyNewinfo.setBackreason("");

            // 将新建建筑汇总表最后一行的合计值状态修改为
            zgsReportEnergyNewinfoTotal.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportEnergyNewinfoTotal.setBackreason("");

            zgsReportEnergyNewinfoGreentotal.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
            zgsReportEnergyNewinfoGreentotal.setBackreason("");

            /*zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state2));
            zgsReportEnergyNewinfoUpdateWrapper.set("backreason","");*/

        }

        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市州账号审批
            // 修改清单（新建建筑清单+绿色建筑清单，为同一张表）   原有更新存在竣工时间置空问题  rxl 20230726
            if (spBol) {
                zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state2));
                zgsReportEnergyNewinfoUpdateWrapper.set("backreason",backreason);
            } else {
                zgsReportEnergyNewinfoUpdateWrapper.set("applystate",new BigDecimal(GlobalConstants.apply_state3));
                zgsReportEnergyNewinfoUpdateWrapper.set("backreason",backreason);
            }

            zgsReportEnergyNewinfoUpdateWrapper.eq("filltm", newinfoTotal.getFilltm());
            zgsReportEnergyNewinfoUpdateWrapper.eq("areacode", newinfoTotal.getAreacode());
            zgsReportEnergyNewinfoUpdateWrapper.isNotNull("end_date");
            zgsReportEnergyNewinfoService.update(null, zgsReportEnergyNewinfoUpdateWrapper);

            //市退县，删掉该条汇总数据-不留痕
//            if (spStatus == 0) {
//                this.baseMapper.deleteById(id);
//            }
            //每次市州审批完更新合计值
//            String filltm = newinfoTotal.getFilltm();
//            String year = filltm.substring(0, 4);
//            String month = filltm.substring(5, 7);
//            int intMonth = Integer.parseInt(month);
//            int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//            initEnergyNewinfoTotalCityAll(year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser, 1);
        }

        // 修改新建建筑汇总表中最后一行合计值的状态
        // this.baseMapper.updateById(zgsReportEnergyNewinfoTotal);
        // 修改绿色建筑汇总表中最后一行合计值的状态
        // this.zgsReportEnergyNewinfoGreentotalService.updateById(zgsReportEnergyNewinfoGreentotal);
        // 修改清单数据状态（绿色建筑清单+新建建筑清单）
        this.baseMapper.updateById(zgsReportEnergyNewinfoTotal);
    }

    @Override
    public ZgsReportEnergyNewinfoTotal lastTotalDataProvince(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastTotalDataProvince(filltm, areacode, applystate);
    }


    @Override
    public ZgsReportEnergyNewinfoTotal lastTotalDataProvinceByCityCode(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastTotalDataProvinceByCityCode(filltm, areacode, applystate);
    }



    @Override
    public ZgsReportEnergyNewinfoTotal lastTotalDataCity(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.lastTotalDataCity(filltm, areacode, applystate);
    }

    @Override
    public ZgsReportEnergyNewinfoTotal getYearNewBuildArea(String filltm, String areacode) {
        return this.baseMapper.getYearNewBuildArea(filltm, areacode);
    }

    @Override
    public ZgsReportEnergyNewinfoTotal getAreadByCityCode(String filltm, String areacode, Integer applystate) {
        return this.baseMapper.getAreadByCityCode(filltm, areacode, applystate);
    }


    @Override
    public ZgsReportEnergyNewinfoTotal queryEnergNewInfoList(String areaCode, String fillTime) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportEnergyNewinfoTotal> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (StringUtils.isNotBlank(fillTime) && fillTime != null) {
            queryWrapper.eq("filltm", fillTime);
        }

        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }

        if (StringUtils.isNotEmpty(areaCode)) {
            queryWrapper.eq("areacode", areaCode);
            queryWrapper.orderByAsc("area_type");
            queryWrapper.eq("area_type",1);
        }
        Page<ZgsReportEnergyNewinfoTotal> page = new Page<ZgsReportEnergyNewinfoTotal>(1, 99);
        IPage<ZgsReportEnergyNewinfoTotal> pageList = zgsReportEnergyNewinfoTotalService.page(page, queryWrapper);
        ZgsReportEnergyNewinfoTotal entity = null;
        if(pageList.getRecords().size() > 0) {
            // 获取当前区域编码下的 能源建筑面积信息
            entity= pageList.getRecords().get(0);
        }
        return entity;
    }
}
