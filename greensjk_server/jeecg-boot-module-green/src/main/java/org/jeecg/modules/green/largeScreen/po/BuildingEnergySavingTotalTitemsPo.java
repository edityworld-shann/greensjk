package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class BuildingEnergySavingTotalTitemsPo implements Serializable {
    /**
     * 本月竣工验收项目数
     */
    private String monthNumber;
    /**
     * 本月竣工验收建筑面积
     */
    private String monthArea;
    /**
     * 本年度新建建筑竣工项目数
     */
    private String yearBuildNumber;
    /**
     * 本年度新建建筑竣工建筑面积
     */
    private String yearBuildArea;
}
