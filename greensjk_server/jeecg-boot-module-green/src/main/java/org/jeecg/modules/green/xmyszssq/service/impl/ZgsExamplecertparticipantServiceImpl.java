package org.jeecg.modules.green.xmyszssq.service.impl;

import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsExamplecertparticipantMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 示范验收证书主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsExamplecertparticipantServiceImpl extends ServiceImpl<ZgsExamplecertparticipantMapper, ZgsExamplecertparticipant> implements IZgsExamplecertparticipantService {

}
