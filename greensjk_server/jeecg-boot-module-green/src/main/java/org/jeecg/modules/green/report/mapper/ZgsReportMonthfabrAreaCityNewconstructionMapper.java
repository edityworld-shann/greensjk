package org.jeecg.modules.green.report.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityNewconstruction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 装配式建筑-新开工-汇总表
 * @Author: jeecg-boot
 * @Date:   2022-05-20
 * @Version: V1.0
 */
public interface ZgsReportMonthfabrAreaCityNewconstructionMapper extends BaseMapper<ZgsReportMonthfabrAreaCityNewconstruction> {

}
