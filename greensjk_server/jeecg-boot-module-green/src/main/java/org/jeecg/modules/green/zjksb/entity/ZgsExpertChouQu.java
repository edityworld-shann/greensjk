package org.jeecg.modules.green.zjksb.entity;

import lombok.Data;

import java.util.List;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/2/2116:07
 */
@Data
public class ZgsExpertChouQu {
    private String id;
    private String status;
    private String projectname;
    private String set;
    private Integer num;
    private String way;
    private String type;
    private Integer typeLx;
    private List<String> nameList;
}
