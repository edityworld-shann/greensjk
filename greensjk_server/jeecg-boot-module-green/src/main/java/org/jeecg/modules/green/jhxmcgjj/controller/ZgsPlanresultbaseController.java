package org.jeecg.modules.green.jhxmcgjj.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.itextpdf.text.pdf.qrcode.QRCode;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 项目成果简介
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "项目成果简介")
@RestController
@RequestMapping("/jhxmcgjj/zgsPlanresultbase")
@Slf4j
public class ZgsPlanresultbaseController extends JeecgController<ZgsPlanresultbase, IZgsPlanresultbaseService> {
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsSacceptcertificatebaseService zgsSacceptcertificatebaseService;


    /**
     * 分页列表查询
     *
     * @param zgsPlanresultbase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "项目成果简介-分页列表查询")
    @ApiOperation(value = "项目成果简介-分页列表查询", notes = "项目成果简介-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsPlanresultbase zgsPlanresultbase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "assType", required = false) Integer assType,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper();
        if (zgsPlanresultbase != null) {
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getResultname())) {
                queryWrapper.like("p.resultname", zgsPlanresultbase.getResultname());
            }
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getSendStatus())) {
                if ("0".equals(zgsPlanresultbase.getSendStatus())) {
                    queryWrapper.isNull("p.send_status");
                } else {
                    queryWrapper.eq("p.send_status", zgsPlanresultbase.getSendStatus());
                }
            }
        }
        if (assType != null) {
            // queryWrapper.eq("p.status", GlobalConstants.SHENHE_STATUS8).or().eq("p.result_show","1");

            queryWrapper.and(wrapper -> wrapper.lambda().eq(ZgsPlanresultbase::getStatus, GlobalConstants.SHENHE_STATUS8)
                            .or().eq(ZgsPlanresultbase::getResultShow,"1")
            );
            if (assType == 1) {
                //终审通过全量查询

            }
            if (assType == 2) {
                //首页发布查询
                queryWrapper.eq("p.send_status", "1");
            }
        }
        queryWrapper.ne("p.isdelete", 1);
        queryWrapper.groupBy("id");
        if (assType != null && assType == 2) {
            queryWrapper.orderByDesc("p.send_time");
        } else {
            queryWrapper.orderByDesc("p.createdate");
        }
        // Page<ZgsPlanresultbase> page = new Page<ZgsPlanresultbase>(pageNo, pageSize);
        // IPage<ZgsPlanresultbase> pageList = zgsPlanresultbaseService.page(page, queryWrapper);
        IPage<ZgsPlanresultbase> pageList = zgsPlanresultbaseService.getResultInfo(queryWrapper,pageNo, pageSize);



        // pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    @AutoLog(value = "项目成果简介-分页列表查询Sys")
    @ApiOperation(value = "项目成果简介-分页列表查询Sys", notes = "项目成果简介-分页列表查询Sys")
    @GetMapping(value = "/listSys")
    public Result<?> queryPageListSys(ZgsPlanresultbase zgsPlanresultbase,
                                      @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                      @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                      @RequestParam(name = "year", required = false) String year,
                                      @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                      @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                      @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                      HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsPlanresultbase.getResultname())) {
            queryWrapper.like("b.resultname", zgsPlanresultbase.getResultname());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(l.projectnum,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsPlanresultbase.getProjectstage())) {
            if (!"-1".equals(zgsPlanresultbase.getProjectstage())) {
                queryWrapper.eq("b.projectstage", QueryWrapperUtil.getProjectstage(zgsPlanresultbase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("b.projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7)).or().isNull("b.projectstage");
                });
            }
        }
//        QueryWrapperUtil.initPlanresultSelect(queryWrapper, zgsPlanresultbase.getStatus(), sysUser);
       // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsPlanresultbase.getStatus(), sysUser, 1);
        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("b.status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("b.enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("b.enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("b.enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("b.status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsPlanresultbase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("b.finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("b.status", GlobalConstants.SHENHE_STATUS0).ne("b.status", GlobalConstants.SHENHE_STATUS1).ne("b.status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("b.isdelete", 1);
        //TODO 修改根据初审日期进行降序 2022-09-21
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("b.applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("b.applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("b.enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("b.enddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("b.firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("b.firstdate");
            }
        }
        if (flag) {
            /*queryWrapper.orderByDesc("createdate");
            queryWrapper.orderByDesc("applydate");*/
            queryWrapper.orderByDesc("b.firstdate");
            queryWrapper.orderByDesc("b.applydate");
        }
        // IPage<ZgsPlanresultbase> pageList = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);

        // 推荐单位查看项目统计，状态为空
        IPage<ZgsPlanresultbase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsPlanresultbase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsPlanresultbase> pageList = null;

        // 项目成果简介
        redisUtil.expire("xmcgjj",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmcgjjOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmcgjjOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsPlanresultbase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsPlanresultbase.getDspForTjdw())) {
            if (zgsPlanresultbase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("xmcgjj")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);
                    redisUtil.set("xmcgjj",pageListForProjectCount.getTotal());
                }
            }
            if (zgsPlanresultbase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("xmcgjjOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);
                    redisUtil.set("xmcgjjOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsPlanresultbase.getDspForTjdw()) && StringUtils.isBlank(zgsPlanresultbase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isBlank(zgsPlanresultbase.getDspForTjdw()) && StringUtils.isNotBlank(zgsPlanresultbase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);

        } else if (StringUtils.isNotBlank(zgsPlanresultbase.getDspForSt()) && zgsPlanresultbase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);
            redisUtil.set("xmcgjjOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsPlanresultbase.getStatus(), sysUser, 1);
            pageList = zgsPlanresultbaseService.listZgsPlanResultBaseListInfo(queryWrapper, pageNo, pageSize);
        }


        /*// 项目成果简介
        redisUtil.expire("xmcgjj",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmcgjj")) {
            if (StringUtils.isNotBlank(zgsPlanresultbase.getFlagByWorkTable()) && zgsPlanresultbase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("xmcgjj",pageList.getTotal());
            }
        }
        redisUtil.expire("xmcgjjOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmcgjjOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsPlanresultbase.getDspForSt()) && zgsPlanresultbase.getDspForSt().equals("2")) {
                redisUtil.set("xmcgjjOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("xmcgjjOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmcgjjOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsPlanresultbase.getDspForTjdw()) && zgsPlanresultbase.getDspForTjdw().equals("3")) {
                redisUtil.set("xmcgjjOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPlanresultbase zgsInfo = pageList.getRecords().get(m);
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("成果简介阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPlanresultbase zgsInfo = pageList.getRecords().get(m);
                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("成果简介阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                    queryWrapper0.eq("sciencetechguid", zgsInfo.getProjectlibraryguid());
                    queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                    queryWrapper0.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                    if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                    }
                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid()) && StringUtils.isNotEmpty(zgsInfo.getResultname())) {
                        QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<ZgsProjecttask>();
                        queryWrapper1.eq("projectguid", zgsInfo.getProjectlibraryguid());
                        queryWrapper1.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper1.ne("isdelete", 1);
                        ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper1);
                        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                        }
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsPlanresultbase
     * @return
     */
    @AutoLog(value = "项目成果简介-撤回")
    @ApiOperation(value = "项目成果简介-撤回", notes = "项目成果简介-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsPlanresultbase zgsPlanresultbase) {
        if (zgsPlanresultbase != null) {
            String id = zgsPlanresultbase.getId();
            String bid = zgsPlanresultbase.getProjectlibraryguid();
            String status = zgsPlanresultbase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7).equals(zgsPlanresultbase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsPlanresultbase info = new ZgsPlanresultbase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsPlanresultbaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsPlanresultbase
     * @return
     */
    @AutoLog(value = "项目成果简介-审批")
    @ApiOperation(value = "项目成果简介-审批", notes = "项目成果简介-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsPlanresultbase zgsPlanresultbase) {
        zgsPlanresultbaseService.spProject(zgsPlanresultbase);
        String projectlibraryguid = zgsPlanresultbaseService.getById(zgsPlanresultbase.getId()).getProjectlibraryguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsPlanresultbase
     * @return
     */
    @AutoLog(value = "项目成果简介-添加")
    @ApiOperation(value = "项目成果简介-添加", notes = "项目成果简介-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsPlanresultbase zgsPlanresultbase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsPlanresultbase.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsPlanresultbase.getStatus())) {
            //保存并上报
            zgsPlanresultbase.setApplydate(new Date());
        } else {
            zgsPlanresultbase.setApplydate(null);
        }
        zgsPlanresultbase.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsPlanresultbase.setCreateaccountname(sysUser.getUsername());
        zgsPlanresultbase.setCreateusername(sysUser.getRealname());
        zgsPlanresultbase.setCreatedate(new Date());
        //科研项目-判断是否添加验收项目，否则给予提醒，需先添加验收项目
        QueryWrapper<ZgsScientificbase> queryWrapper = new QueryWrapper();
        queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getResultname())) {
                qwp1.like("projectname", zgsPlanresultbase.getResultname());
            }
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getProjectlibraryguid())) {
                qwp1.or().eq("projectlibraryguid", zgsPlanresultbase.getProjectlibraryguid());
            }
        });
//        List<ZgsScientificbase> list = zgsScientificbaseService.list(queryWrapper);
        //示范项目-判断是否添加验收项目，否则给予提醒，需先添加验收项目
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper1.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper1.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getResultname())) {
                qwp1.like("projectname", zgsPlanresultbase.getResultname());
            }
            if (StringUtils.isNotEmpty(zgsPlanresultbase.getProjectlibraryguid())) {
                qwp1.or().eq("projectlibraryguid", zgsPlanresultbase.getProjectlibraryguid());
            }
        });
//        List<ZgsDemoprojectacceptance> list1 = zgsDemoprojectacceptanceService.list(queryWrapper1);
//        if (list.size() == 0 && list1.size() == 0) {
//            return Result.error("请您先添加验收管理项目，形审通过后方可添加此项！");
//        }
        //
        zgsPlanresultbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
        zgsPlanresultbase.setProjectstagestatus(zgsPlanresultbase.getStatus());


        QueryWrapper<ZgsPlanresultbase> qr = new QueryWrapper<ZgsPlanresultbase>();
        qr.eq("projectlibraryguid",zgsPlanresultbase.getProjectlibraryguid());
        List<ZgsPlanresultbase> list = zgsPlanresultbaseService.list(qr);
        if (list.size() > 0) {
            zgsPlanresultbase.setId(list.get(0).getId());
            zgsPlanresultbaseService.updateById(ValidateEncryptEntityUtil.validateDecryptObject(zgsPlanresultbase, ValidateEncryptEntityUtil.isEncrypt));
        } else {
            zgsPlanresultbaseService.save(ValidateEncryptEntityUtil.validateDecryptObject(zgsPlanresultbase, ValidateEncryptEntityUtil.isEncrypt));
        }
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsPlanresultbase.getStatus()) && StringUtils.isNotEmpty(zgsPlanresultbase.getProjectlibraryguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsPlanresultbase.getProjectlibraryguid());
        }
//        redisUtil.set(zgsPlanresultbase.getProjectlibraryguid(), "成果简介阶段");
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsPlanresultbase
     * @return
     */
    @AutoLog(value = "项目成果简介-编辑")
    @ApiOperation(value = "项目成果简介-编辑", notes = "项目成果简介-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsPlanresultbase zgsPlanresultbase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPlanresultbase != null && StringUtils.isNotEmpty(zgsPlanresultbase.getId())) {
            String id = zgsPlanresultbase.getId();
            if (GlobalConstants.handleSubmit.equals(zgsPlanresultbase.getStatus())) {
                //保存并上报
                zgsPlanresultbase.setApplydate(new Date());
                zgsPlanresultbase.setAuditopinion(null);
//                zgsPlanresultbase.setReturntype(null);
            } else {
                zgsPlanresultbase.setApplydate(null);
                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                ZgsPlanresultbase projectTask = zgsPlanresultbaseService.getById(id);
                if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                    zgsPlanresultbase.setStatus(GlobalConstants.SHENHE_STATUS3);
                }
            }
            zgsPlanresultbase.setModifyaccountname(sysUser.getUsername());
            zgsPlanresultbase.setModifyusername(sysUser.getRealname());
            zgsPlanresultbase.setModifydate(new Date());
            zgsPlanresultbase.setCreatedate(null);
            zgsPlanresultbase.setFinishdate(null);
            zgsPlanresultbase.setFirstdate(null);
            zgsPlanresultbase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_7));
            zgsPlanresultbase.setProjectstagestatus(zgsPlanresultbase.getStatus());
            zgsPlanresultbaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlanresultbase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsPlanresultbase.getStatus()) && StringUtils.isNotEmpty(zgsPlanresultbase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsPlanresultbase.getProjectlibraryguid());
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 发布或撤销
     *
     * @param zgsPlanresultbase
     * @return
     */
    @AutoLog(value = "项目成果简介-发布或撤销")
    @ApiOperation(value = "项目成果简介-发布或撤销", notes = "项目成果简介-发布或撤销")
    @PutMapping(value = "/updateStatusById")
    public Result<?> updateStatusById(@RequestBody ZgsPlanresultbase zgsPlanresultbase) {
        String result = "";
        if (zgsPlanresultbase != null && StringUtils.isNotEmpty(zgsPlanresultbase.getId())) {
            if ("0".equals(zgsPlanresultbase.getSendStatus())) {
                //撤销
                zgsPlanresultbase.setCancelTime(new Date());
                result = "撤销";
            } else {
                //发布
                zgsPlanresultbase.setSendTime(new Date());
                result = "发布";
            }
            zgsPlanresultbaseService.updateById(zgsPlanresultbase);
        }

        return Result.OK(result + "成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目成果简介-通过id删除")
    @ApiOperation(value = "项目成果简介-通过id删除", notes = "项目成果简介-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsPlanresultbaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "项目成果简介-批量删除")
    @ApiOperation(value = "项目成果简介-批量删除", notes = "项目成果简介-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsPlanresultbaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目成果简介-通过id查询")
    @ApiOperation(value = "项目成果简介-通过id查询", notes = "项目成果简介-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsPlanresultbase zgsPlanresultbase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsPlanresultbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsPlanresultbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsPlanresultbase == null) {
                return Result.error("未找到对应数据");
            }
            if ((zgsPlanresultbase.getFirstdate() != null || zgsPlanresultbase.getFinishdate() != null) && zgsPlanresultbase.getApplydate() != null) {
                zgsPlanresultbase.setSpLogStatus(1);
            } else {
                zgsPlanresultbase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsPlanresultbase = new ZgsPlanresultbase();
            zgsPlanresultbase.setSpLogStatus(0);
        }
        return Result.OK(zgsPlanresultbase);
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsPlanresultbase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsPlanresultbase zgsPlanresultbase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageListSys(zgsPlanresultbase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsPlanresultbase> pageList = (IPage<ZgsPlanresultbase>) result.getResult();
        List<ZgsPlanresultbase> list = pageList.getRecords();
        List<ZgsPlanresultbase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "项目成果简介";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsPlanresultbase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsPlanresultbase, ZgsPlanresultbase.class, "项目成果简介");
        return mv;

    }

    private String getId(ZgsPlanresultbase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsPlanresultbase.class);
    }

    /**
     * 通过id查询导出Word
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目成果简介-通过id查询导出Word")
    @ApiOperation(value = "项目成果简介-通过id查询导出Word", notes = "项目成果简介-通过id查询导出Word")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsPlanresultbase zgsPlanresultbase = ValidateEncryptEntityUtil.validateDecryptObject(zgsPlanresultbaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "18甘肃省建设计划项目成果简介.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsPlanresultbase);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

}
