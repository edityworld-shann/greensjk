package org.jeecg.modules.green.sfxmsb.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: 示范工程费用使用明细公共提取实体类
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Data
public class ZgsCommonFee implements Serializable {
    private static final long serialVersionUID = 1L;
    private BigDecimal equipmentcost;
    private BigDecimal equipmenttrial;
    private BigDecimal equipmentchange;
    private BigDecimal materialfee;
    private BigDecimal processfee;
    private BigDecimal fuelpowerfee;
    private BigDecimal travelfee;
    private BigDecimal meetingfee;
    private BigDecimal exchangefee;
    private BigDecimal publicationfee;
    private BigDecimal labourfee;
    private BigDecimal assessmentfee;
    private BigDecimal managefee;
    private BigDecimal trainingfee;
}
