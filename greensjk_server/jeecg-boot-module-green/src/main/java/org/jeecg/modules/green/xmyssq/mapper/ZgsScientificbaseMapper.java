package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目申报验收基本信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
public interface ZgsScientificbaseMapper extends BaseMapper<ZgsScientificbase> {

}
