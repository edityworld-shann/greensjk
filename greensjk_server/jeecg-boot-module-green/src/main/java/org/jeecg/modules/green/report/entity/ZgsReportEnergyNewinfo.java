package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 新建绿色建筑-节能项目清单
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_energy_newinfo")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_energy_newinfo对象", description = "新建绿色建筑-节能项目清单")
public class ZgsReportEnergyNewinfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    // @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String filltm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate = new BigDecimal(0);
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 创建人帐号
     */
    // @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;


    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;
    /**
     * 项目名称
     */
    @Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String projectname;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 工改系统代码
     */
    //@Excel(name = "工改系统代码", width = 15)
    @ApiModelProperty(value = "工改系统代码")
    private java.lang.String sysCode;
    /**
     * 建筑面积
     */
    @Excel(name = "建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "建筑面积")
    private java.math.BigDecimal buildingArea = new BigDecimal(0);
    /**
     * 1、住宅建筑面积2、公共建筑面积3、工业建筑面积
     */
    @Excel(name = "建筑类型", width = 15)
    @ApiModelProperty(value = "1、住宅建筑面积2、公共建筑面积3、工业建筑面积")
    private java.lang.String buildType;
    /**
     * 1是，0否（是否绿色建筑）
     */
    //@Excel(name = "1是，0否（是否绿色建筑）", width = 15)
    @ApiModelProperty(value = "1是，0否（是否绿色建筑）")
    private java.lang.String isNo;
    /**
     * 一星、二星、三星
     */
    @Excel(name = "等级", width = 15)
    @ApiModelProperty(value = "一星、二星、三星")
    private java.lang.String greenRank;
    /**
     * 超低能耗建筑（万㎡）
     */
    @Excel(name = "超低能耗建筑（万㎡）", width = 15)
    @ApiModelProperty(value = "超低能耗建筑（万㎡）")
    private java.math.BigDecimal lowEnergy = new BigDecimal(0);
    /**
     * 近零能耗建筑（万㎡）
     */
    @Excel(name = "近零能耗建筑（万㎡）", width = 15)
    @ApiModelProperty(value = "近零能耗建筑（万㎡）")
    private java.math.BigDecimal zeroEnergy = new BigDecimal(0);
    /**
     * 建筑太阳能光伏装机容量（KW）
     */
    @Excel(name = "建筑太阳能光伏装机容量（KW）", width = 15)
    @ApiModelProperty(value = "建筑太阳能光伏装机容量（KW）")
    private java.math.BigDecimal solarEnergy = new BigDecimal(0);
    /**
     * 太阳能光热应用建筑面积（万㎡）
     */
    @Excel(name = "太阳能光热应用建筑面积（万㎡）", width = 15)
    @ApiModelProperty(value = "太阳能光热应用建筑面积（万㎡）")
    private java.math.BigDecimal solarEnergyHigh = new BigDecimal(0);
    /**
     * 浅层地热能应用面积（万㎡）
     */
    @Excel(name = "浅层地热能应用面积（万㎡）", width = 15)
    @ApiModelProperty(value = "浅层地热能应用面积（万㎡）")
    private java.math.BigDecimal geothermalEnergy = new BigDecimal(0);
    /**
     * 中深层地热能应用面积（万㎡）
     */
    @Excel(name = "中深层地热能应用面积（万㎡）", width = 15)
    @ApiModelProperty(value = "中深层地热能应用面积（万㎡）")
    private java.math.BigDecimal geothermalEnergyHigh = new BigDecimal(0);
    /**
     * 其他类型技术应用面积（万㎡）
     */
    @Excel(name = "其他类型技术应用面积（万㎡）", width = 15)
    @ApiModelProperty(value = "其他类型技术应用面积（万㎡）")
    private java.math.BigDecimal otherEnergy = new BigDecimal(0);
    /**
     * 开工时间
     */
    @Excel(name = "开工时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开工时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.util.Date startDate;
    /**
     * 竣工验收时间
     */
    @Excel(name = "竣工验收时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "竣工验收时间")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.util.Date endDate;
    /**
     * 退回原因
     */
    // @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 开始运行监测时间
     */
    //@Excel(name = "开始运行监测时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始运行监测时间")
    private java.util.Date runDate;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    // 1 模糊查；  2 精准查询
    @TableField(exist = false)
    private String qybm;
}
