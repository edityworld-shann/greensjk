package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergyprojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsEnergyprojectmainparticipantMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsEnergyprojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 节能建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsEnergyprojectmainparticipantServiceImpl extends ServiceImpl<ZgsEnergyprojectmainparticipantMapper, ZgsEnergyprojectmainparticipant> implements IZgsEnergyprojectmainparticipantService {

}
