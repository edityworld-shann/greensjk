package org.jeecg.modules.green.largeScreen.vo;

import lombok.Data;

@Data
public class PrefabricaeZbVo {
    private double monthFarAssemblyrate;
    private double yearFarAssemblyrate;
}
