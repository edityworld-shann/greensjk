package org.jeecg.modules.green.sfxmsb.service;

import org.jeecg.modules.green.sfxmsb.entity.ZgsEnergybuildproject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 节能建筑工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface IZgsEnergybuildprojectService extends IService<ZgsEnergybuildproject> {

}
