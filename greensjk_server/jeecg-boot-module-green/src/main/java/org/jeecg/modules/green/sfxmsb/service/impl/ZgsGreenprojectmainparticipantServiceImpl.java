package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsGreenprojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsGreenprojectmainparticipantMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsGreenprojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 绿色建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsGreenprojectmainparticipantServiceImpl extends ServiceImpl<ZgsGreenprojectmainparticipantMapper, ZgsGreenprojectmainparticipant> implements IZgsGreenprojectmainparticipantService {

}
