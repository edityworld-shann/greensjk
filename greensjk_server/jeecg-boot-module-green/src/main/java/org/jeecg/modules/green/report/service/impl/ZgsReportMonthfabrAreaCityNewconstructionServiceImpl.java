package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.model.TreeSelectModel;
import org.jeecg.modules.green.common.mapper.ZgsCommonMapper;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.mapper.*;
import org.jeecg.modules.green.report.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Description: 现用来同步数据使用
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Slf4j
@Service
public class ZgsReportMonthfabrAreaCityNewconstructionServiceImpl extends ServiceImpl<ZgsReportMonthfabrAreaCityNewconstructionMapper, ZgsReportMonthfabrAreaCityNewconstruction> implements IZgsReportMonthfabrAreaCityNewconstructionService {

    @Autowired
    private ZgsCommonMapper zgsCommonMapper;
    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService zgsReportMonthfabrAreaCityProductioncapacityService;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;
    @Autowired
    private ZgsReportMonthfabrAreaCityProjectMapper zgsReportMonthfabrAreaCityProjectMapper;
    @Autowired
    private ZgsReportMonthfabrAreaCityCompanyMapper zgsReportMonthfabrAreaCityCompanyMapper;
    @Autowired
    private IZgsReportEnergyExistinfoTotalService zgsReportEnergyExistinfoTotalService;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService zgsReportEnergyNewinfoGreentotalService;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService zgsReportEnergyNewinfoTotalService;
    @Autowired
    private ZgsReportEnergyExistinfoMapper zgsReportEnergyExistinfoMapper;
    @Autowired
    private ZgsReportEnergyNewinfoMapper zgsReportEnergyNewinfoMapper;
    @Autowired
    private IZgsReportAreacodeValuesService zgsReportAreacodeValuesService;

    /**
     * 计算两个时间中所有的月份
     *
     * @param date1 开始时间
     * @param date2 结束时间
     * @return
     * @throws ParseException
     */
    public static List<String> getMonths(String date1, String date2) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date parse = sdf.parse(date1);
//        Date parse2 = sdf.parse(date2);
//        List<String> dateList = new ArrayList<>();
//        Calendar c1 = Calendar.getInstance();
//        c1.setTime(parse);
//        //转为周一
//        int year = c1.get(Calendar.YEAR);
//        int month = c1.get(Calendar.MONTH);
//        c1.set(year, month, 1, 0, 0, 0);
//        Calendar c2 = Calendar.getInstance();
//        c2.setTime(parse2);
//        int weekYear2 = c2.get(Calendar.YEAR);
//        int weekOfYear2 = c2.get(Calendar.WEEK_OF_YEAR);
//        c2.setWeekDate(weekYear2, weekOfYear2, Calendar.SUNDAY);
//        while (true) {
//            int tempMonth = c1.get(Calendar.MONTH);
//            String date = c1.getWeekYear() + "-" + ((tempMonth + 1) <= 9 ? "0" + (tempMonth + 1) : tempMonth + 1);
//            System.out.println(date);
//            dateList.add(date);
//            //下一个月<结束日期
//            c1.set(Calendar.MONTH, tempMonth + 1);
//            if (c1.getTimeInMillis() >= c2.getTimeInMillis()) {
//                break;
//            }
//        }
        List<String> dateList = new ArrayList<>();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date parse = sdf.parse(date1);
            Date parse2 = sdf.parse(date2);
            Calendar c1 = Calendar.getInstance();
            c1.setTime(parse);
            //转为周一
            int year = c1.get(Calendar.YEAR);
            int month = 1;
            Calendar c2 = Calendar.getInstance();
            c2.setTime(parse2);
            int month2 = c2.get(Calendar.MONTH);
            int dateT = c2.get(Calendar.DATE);
            //限制每月25号才能同步当月数据
            if (dateT > 25) {
                month2 += 1;
            }
            for (int i = month; i <= month2; i++) {
                String date = year + "-" + (i <= 9 ? "0" + i : i);
                dateList.add(date);
                System.out.println(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateList;
    }

    /**
     * 递归求子节点 同步加载用到
     */
    private void loadAllCategoryChildren(List<TreeSelectModel> ls) {
        for (TreeSelectModel tsm : ls) {
            List<TreeSelectModel> temp = zgsCommonMapper.queryListByPid(tsm.getKey(), null);
            if (temp != null && temp.size() > 0) {
                tsm.setChildren(temp);
                loadAllCategoryChildren(temp);
            }
        }
    }

    @Override
    public void dataInitByAreaCode(String areaCode, Integer type) {
        reportAreaApply(areaCode, type);
    }

    private static List<String> getMonthStartAndEnd() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String yearMonth = simpleDateFormat.format(new Date());
        List<String> list = new ArrayList<>();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(new Date());
        //转为周一
        int year = c1.get(Calendar.YEAR);
        if (Integer.parseInt(yearMonth.substring(5, 7)) == 1) {
            year = year - 1;
            yearMonth = (year + "-12-31");
        }
        //
        list.add(year + "-01-01");
        list.add(yearMonth);
        return list;
    }

    public static void main(String[] args) {
        List<String> list = getMonthStartAndEnd();
        getMonths(list.get(0), list.get(1));
    }

    private void reportAreaApply(String areaCode, Integer type) {
        log.info("自动上报 县区 清单----------start");
        //  区县上报：获取所有区县的地区码，循环遍历，
        //  市州上报：获取所有区县的地区码，循环遍历，
        List<TreeSelectModel> ls = zgsCommonMapper.queryListByPid("f6254f48af4fb3af13b9e8bf167fg5210", null);
        loadAllCategoryChildren(ls);
        List<String> monthsList = new ArrayList<>();
        List<String> listMon = getMonthStartAndEnd();
        monthsList = getMonths(listMon.get(0), listMon.get(1));
        //  执行 自动上报 所有地区的 县区 清单
        //  装配式
        for (int i = 0; i < ls.size(); i++) {     //  地区码（市州）
            if (ls.get(i).getChildren() == null) {
//                log.info("------中止本次循环，继续下次循环-------");
                continue;
            }
            if (StringUtils.isNotEmpty(areaCode) && !areaCode.equals(ls.get(i).getCode())) {
                continue;
            }
            for (int k = 0; k < ls.get(i).getChildren().size(); k++) {     //  地区码（区县）
                for (int m = 0; m < monthsList.size(); m++) {   //月份（-1 是为了除去当月的，当月的手动上报）
                    String yearMonth = monthsList.get(m);
                    String year = yearMonth.substring(0, 4);
                    String month = yearMonth.substring(5, 7);
                    int intMonth = Integer.parseInt(month);
                    int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
                    String areacode = ls.get(i).getChildren().get(k).getCode();
                    String areaname = ls.get(i).getChildren().get(k).getTitle();
//                    //只先执行酒泉市经济技术开发区
//                    if (!"620930".equals(areacode)) {
//                        continue;
//                    }
                    Double monthNewconstructArea = 0.0;    //  本月所有新开工建筑的总面积（㎡）
                    Double monthCompletedArea = 0.0;       //  本月所有已竣工验收建筑总面积（㎡）
                    QueryWrapper<ZgsReportAreacodeValues> areacodeValuesQueryWrapper = new QueryWrapper();
                    areacodeValuesQueryWrapper.eq("month", monthsList.get(m));
                    areacodeValuesQueryWrapper.eq("areacode", areacode);
                    List<ZgsReportAreacodeValues> areacodeValuesList = zgsReportAreacodeValuesService.list(areacodeValuesQueryWrapper);
                    if (areacodeValuesList.size() > 0) {
                        if (areacodeValuesList.get(0).getMonthNewconstructArea() != null && areacodeValuesList.get(0).getMonthCompletedArea() != null) {
                            if (Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString()) > 0) {
                                monthNewconstructArea = Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString());
                            }
                            if (Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString()) > 0) {
                                monthCompletedArea = Double.parseDouble(areacodeValuesList.get(0).getMonthCompletedArea().toString());
                            }
                        }
                    }
                    //  装配式   Begin   ==================================================================================================================================
                    //  新开工 判断，并上报
//                    QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapperXkg = new QueryWrapper();
//                    wrapperXkg.eq("reporttm", monthsList.get(m));
//                    wrapperXkg.eq("areacode", areacode);
//                    wrapperXkg.eq("projecttype", "1");
//                    List<ZgsReportMonthfabrAreaCityProject> listTotalXkg = zgsReportMonthfabrAreaCityProjectMapper.selectList(wrapperXkg);
//                    if (listTotalXkg.size() > 0) {
//                    log.info("error----------------------1");
                    //***默认状态status传null,同步数据不修改之前的状态
                    //GlobalConstants.apply_state2
                    zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, "1", monthNewconstructArea, monthCompletedArea, areacode, areaname, null, null);
//                    }
                    //  已竣工 判断，并上报
//                    QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapperYjg = new QueryWrapper();
//                    wrapperYjg.eq("reporttm", monthsList.get(m));
//                    wrapperYjg.eq("areacode", areacode);
//                    wrapperYjg.eq("projecttype", "2");
//                    List<ZgsReportMonthfabrAreaCityProject> listTotalYjg = zgsReportMonthfabrAreaCityProjectMapper.selectList(wrapperYjg);
//                    if (listTotalYjg.size() > 0) {
//                    log.info("error----------------------2");
                    //***默认状态status传null,同步数据不修改之前的状态
                    zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, "2", monthNewconstructArea, monthCompletedArea, areacode, areaname, null, null);
//                    }
                    //  生产产能 判断，并上报
//                    QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
//                    wrapper.eq("reporttm", monthsList.get(m));
//                    wrapper.eq("areacode", areacode);
//                    List<ZgsReportMonthfabrAreaCityCompany> listTotal = zgsReportMonthfabrAreaCityCompanyMapper.selectList(wrapper);
//                    if (listTotal.size() > 0) {
//                    log.info("error----------------------3");
                    //***默认状态status传null,同步数据不修改之前的状态
                    zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                    }
                    //  装配式   End   =================================================================================================================================
                    //  非装配式   Begin   ==============================================================================================================================
                    //  新建建筑节能情况 判断，并上报
//                    QueryWrapper<ZgsReportEnergyNewinfo> wrapperEnergyNewinfo1 = new QueryWrapper();
//                    wrapperEnergyNewinfo1.eq("filltm", monthsList.get(m));
//                    wrapperEnergyNewinfo1.eq("areacode", areacode);
//                    List<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfosList1 = zgsReportEnergyNewinfoMapper.selectList(wrapperEnergyNewinfo1);
//                    if (zgsReportEnergyNewinfosList1.size() > 0) {
//                    log.info("error----------------------4");
                    //***默认状态status传null,同步数据不修改之前的状态
                    zgsReportEnergyNewinfoTotalService.energyNewinfoTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                    }
                    //  绿色 判断，并上报
//                    QueryWrapper<ZgsReportEnergyNewinfo> wrapperEnergyNewinfo2 = new QueryWrapper();
//                    wrapperEnergyNewinfo2.eq("filltm", monthsList.get(m));
//                    wrapperEnergyNewinfo2.eq("areacode", areacode);
//                    List<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfosList2 = zgsReportEnergyNewinfoMapper.selectList(wrapperEnergyNewinfo2);
//                    if (zgsReportEnergyNewinfosList2.size() > 0) {
//                    log.info("error----------------------5");
                    //***默认状态status传null,同步数据不修改之前的状态
                    zgsReportEnergyNewinfoGreentotalService.energyNewinfoGreenTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                    }
                    //  既有建筑节能改造完成情况 判断，并上报
//                    QueryWrapper<ZgsReportEnergyExistinfo> wrapperEnergyExistinfo = new QueryWrapper();
//                    wrapperEnergyExistinfo.eq("filltm", monthsList.get(m));
//                    wrapperEnergyExistinfo.eq("areacode", areacode);
//                    List<ZgsReportEnergyExistinfo> zgsReportEnergyExistinfo = zgsReportEnergyExistinfoMapper.selectList(wrapperEnergyExistinfo);
//                    if (zgsReportEnergyExistinfo.size() > 0) {
//                    log.info("error----------------------6");
                    //***默认状态status传null,同步数据不修改之前的状态
                    zgsReportEnergyExistinfoTotalService.energyExistinfoTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                    }
                    //  非装配式   End   ===============================================================================================================================
                }
            }
        }
        log.info("自动上报 县区 清单----------end");
        //继续执行一下任务
        reportCityApply(areaCode, type);
    }

    /**
     * 市州清单上报及市州汇总上报
     * 批量初始化 统计报表的清单一键上报
     */
    private void reportCityApply(String areaCode, Integer type) {
        log.info("自动上报 市州 清单----------start");
        //  区县上报：获取所有区县的地区码，循环遍历，
        //  市州上报：获取所有区县的地区码，循环遍历，
        List<TreeSelectModel> ls = zgsCommonMapper.queryListByPid("f6254f48af4fb3af13b9e8bf167fg5210", null);
        loadAllCategoryChildren(ls);
        List<String> monthsList = new ArrayList<>();
        List<String> listMon = getMonthStartAndEnd();
        monthsList = getMonths(listMon.get(0), listMon.get(1));
        //  执行 自动上报 所有地区的 县区 清单
        //  装配式
        for (int i = 0; i < ls.size(); i++) {   //  地区码（市州）
            if (StringUtils.isNotEmpty(areaCode) && !areaCode.equals(ls.get(i).getCode())) {
                continue;
            }
            for (int m = 0; m < monthsList.size(); m++) {   //月份（-1 是为了除去当月的，当月的手动上报）
                String yearMonth = monthsList.get(m);
                String year = yearMonth.substring(0, 4);
                String month = yearMonth.substring(5, 7);
                int intMonth = Integer.parseInt(month);
                int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
                String areacode = ls.get(i).getCode();
                String areaname = ls.get(i).getTitle();
                Double monthNewconstructArea = 0.0;    //  本月所有新开工建筑的总面积（㎡）
                Double monthCompletedArea = 0.0;       //  本月所有已竣工验收建筑总面积（㎡）
                QueryWrapper<ZgsReportAreacodeValues> areacodeValuesQueryWrapper = new QueryWrapper();
                areacodeValuesQueryWrapper.eq("month", monthsList.get(m));
                areacodeValuesQueryWrapper.eq("areacode", areacode);
                List<ZgsReportAreacodeValues> areacodeValuesList = zgsReportAreacodeValuesService.list(areacodeValuesQueryWrapper);
                if (areacodeValuesList.size() > 0) {
                    if (areacodeValuesList.get(0).getMonthNewconstructArea() != null && areacodeValuesList.get(0).getMonthCompletedArea() != null) {
                        if (Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString()) > 0) {
                            monthNewconstructArea = Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString());
                        }
                        if (Double.parseDouble(areacodeValuesList.get(0).getMonthNewconstructArea().toString()) > 0) {
                            monthCompletedArea = Double.parseDouble(areacodeValuesList.get(0).getMonthCompletedArea().toString());
                        }
                    }
                }
                //  装配式   Begin   ==================================================================================================================================
                //  1.1、新开工 判断，市州清单上报
//                QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapperXkg = new QueryWrapper();
//                wrapperXkg.eq("reporttm", monthsList.get(m));
//                wrapperXkg.eq("areacode", areacode);
//                wrapperXkg.eq("projecttype", "1");
//                List<ZgsReportMonthfabrAreaCityProject> listTotalXkg = zgsReportMonthfabrAreaCityProjectMapper.selectList(wrapperXkg);
//                if (listTotalXkg.size() > 0) {
//                log.info("error----------------------11");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, "1", monthNewconstructArea, monthCompletedArea, areacode, areaname, null, null);
//                }
//                log.info("error----------------------12");
                //  1.2、新开工 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(1, null, year, quarter, yearMonth, "1", areacode, areaname, null);
                //  1.3已竣工 判断，市州清单上报
//                QueryWrapper<ZgsReportMonthfabrAreaCityProject> wrapperYjg = new QueryWrapper();
//                wrapperYjg.eq("reporttm", monthsList.get(m));
//                wrapperYjg.eq("areacode", areacode);
//                wrapperYjg.eq("projecttype", "2");
//                List<ZgsReportMonthfabrAreaCityProject> listTotalYjg = zgsReportMonthfabrAreaCityProjectMapper.selectList(wrapperYjg);
//                if (listTotalYjg.size() > 0) {
//                log.info("error----------------------13");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, "2", monthNewconstructArea, monthCompletedArea, areacode, areaname, null, null);
//                }
//                log.info("error----------------------14");
                //  1.4、新开工 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(1, null, year, quarter, yearMonth, "2", areacode, areaname, null);
                //  1.5生产产能 判断，市州清单上报
//                QueryWrapper<ZgsReportMonthfabrAreaCityCompany> wrapper = new QueryWrapper();
//                wrapper.eq("reporttm", monthsList.get(m));
//                wrapper.eq("areacode", areacode);
//                List<ZgsReportMonthfabrAreaCityCompany> listTotal = zgsReportMonthfabrAreaCityCompanyMapper.selectList(wrapper);
//                if (listTotal.size() > 0) {
//                log.info("error----------------------15");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrAreaTotalALL(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                }
//                log.info("error----------------------16");
                //  1.6生产产能 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrCityTotalALL(1, null, year, quarter, yearMonth, areacode, areaname, null);
                //  装配式   End   =================================================================================================================================

                //  非装配式   Begin   ==============================================================================================================================
                //  2.1新建建筑节能情况 判断，市州清单上报
//                QueryWrapper<ZgsReportEnergyNewinfo> wrapperEnergyNewinfo1 = new QueryWrapper();
//                wrapperEnergyNewinfo1.eq("filltm", monthsList.get(m));
//                wrapperEnergyNewinfo1.eq("areacode", areacode);
//                List<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfosList1 = zgsReportEnergyNewinfoMapper.selectList(wrapperEnergyNewinfo1);
//                if (zgsReportEnergyNewinfosList1.size() > 0) {
//                log.info("error----------------------17");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyNewinfoTotalService.energyNewinfoTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                }
//                log.info("error----------------------18");
                //  2.2新建建筑节能情况 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyNewinfoTotalService.initEnergyNewinfoTotalCityAll(year, quarter, yearMonth, areacode, areaname, null, 1, null);
                //  2.3绿色 判断，市州清单上报
//                QueryWrapper<ZgsReportEnergyNewinfo> wrapperEnergyNewinfo2 = new QueryWrapper();
//                wrapperEnergyNewinfo2.eq("filltm", monthsList.get(m));
//                wrapperEnergyNewinfo2.eq("areacode", areacode);
//                List<ZgsReportEnergyNewinfo> zgsReportEnergyNewinfosList2 = zgsReportEnergyNewinfoMapper.selectList(wrapperEnergyNewinfo2);
//                if (zgsReportEnergyNewinfosList2.size() > 0) {
//                log.info("error----------------------19");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyNewinfoGreentotalService.energyNewinfoGreenTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                }
//                log.info("error----------------------20");
                //  2.4绿色 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyNewinfoGreentotalService.initEnergyNewinfoGreenTotalCityAll(year, quarter, yearMonth, areacode, areaname, null, 1, null);
                //  2.5既有建筑节能改造完成情况 判断，市州清单上报
//                QueryWrapper<ZgsReportEnergyExistinfo> wrapperEnergyExistinfo = new QueryWrapper();
//                wrapperEnergyExistinfo.eq("filltm", monthsList.get(m));
//                wrapperEnergyExistinfo.eq("areacode", areacode);
//                List<ZgsReportEnergyExistinfo> zgsReportEnergyExistinfo = zgsReportEnergyExistinfoMapper.selectList(wrapperEnergyExistinfo);
//                if (zgsReportEnergyExistinfo.size() > 0) {
//                log.info("error----------------------21");
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyExistinfoTotalService.energyExistinfoTotalAll(1, year, quarter, yearMonth, areacode, areaname, null, null);
//                }
//                log.info("error----------------------22");
                //  2.6既有建筑节能改造完成情况 判断，市州汇总上报省厅
                //***默认状态status传null,同步数据不修改之前的状态
                zgsReportEnergyExistinfoTotalService.initEnergyExistinfoTotalCityAll(year, quarter, yearMonth, areacode, areaname, null, 1, null);
                //  非装配式   End   ===============================================================================================================================
            }
        }
        log.info("自动上报 市州 清单----------end");
    }
}
