package org.jeecg.modules.green.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: zgs_report_monthfabr_areacompany
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_areacompany")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "单位库-对象", description = "单位库-对象")
public class ZgsReportMonthfabrAreacompany implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "id")
    private String id;
    /**
     * REPORT_MONTHFABR_AREADETAIL的guid
     */
    @Excel(name = "REPORT_MONTHFABR_AREADETAIL的guid", width = 15)
    @ApiModelProperty(value = "REPORT_MONTHFABR_AREADETAIL的guid")
    private String monthmianguid;
    /**
     * projectname
     */
    @Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    private String companyname;

    /**
     * 主要技术体系住宅
     */
    @Excel(name = "主要技术体系住宅", width = 15)
    @ApiModelProperty(value = "主要技术体系住宅")
    private String zyjstxzz;

    /**
     * 主要技术体系住宅-装配式木结构
     */
    @Excel(name = "主要技术体系住宅-装配式木结构", width = 15)
    @ApiModelProperty(value = "主要技术体系住宅-装配式木结构")
    private String zyjstxzzzpsmjg;

    /**
     * 主要技术体系住宅-装配式混凝土结构
     */
    @Excel(name = "主要技术体系住宅-装配式混凝土结构", width = 15)
    @ApiModelProperty(value = "主要技术体系住宅-装配式混凝土结构")
    private String zyjstxzzzpshntjg;

    /**
     * 主要技术体系住宅-装配式钢结构
     */
    @Excel(name = "主要技术体系住宅-装配式钢结构", width = 15)
    @ApiModelProperty(value = "主要技术体系住宅-装配式钢结构")
    private String zyjstxzzzpsgjg;

    /**
     * 主要技术体系公共建筑
     */
    @Excel(name = "主要技术体系公共建筑", width = 15)
    @ApiModelProperty(value = "主要技术体系公共建筑")
    private String zyjstxjz;


    /**
     * 主要技术体系公共建筑-装配式木结构
     */
    @Excel(name = "主要技术体系公共建筑-装配式木结构", width = 15)
    @ApiModelProperty(value = "主要技术体系公共建筑-装配式木结构")
    private String zyjstxzjzpsmjg;

    /**
     * 主要技术体系公共建筑-装配式混凝土结构
     */
    @Excel(name = "主要技术体系公共建筑-装配式混凝土结构", width = 15)
    @ApiModelProperty(value = "主要技术体系公共建筑-装配式混凝土结构")
    private String zyjstxzjzpshntjg;

    /**
     * 主要技术体系公共建筑-装配式钢结构
     */
    @Excel(name = "主要技术体系公共建筑-装配式钢结构", width = 15)
    @ApiModelProperty(value = "主要技术体系公共建筑-装配式钢结构")
    private String zyjstxzjzpsgjg;

    /**
     * 装配式混凝土预制构配件生产线（条）
     */
    @Excel(name = "装配式混凝土预制构配件生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件生产线（条）")
    private String zpshntscx;

    /**
     * 装配式混凝土预制构配件设计产能（万立方米）
     */
    @Excel(name = "装配式混凝土预制构配件设计产能（万立方米）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件设计产能（万立方米）")
    private String zpshntshejcn;

    /**
     * 装配式混凝土预制构配件设计产能（万立方米）
     */
    @Excel(name = "装配式混凝土预制构配件实际产能（万立方米）", width = 15)
    @ApiModelProperty(value = "装配式混凝土预制构配件实际产能（万立方米）")
    private String zpshntsjcn;

    /**
     * 装配式钢结构构件生产线（条）
     */
    @Excel(name = "装配式钢结构构件生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产线（条）")
    private String zpsgjgscx;

    /**
     * 装配式钢结构构件生产能力（万吨）
     */
    @Excel(name = "装配式钢结构构件生产能力（万吨）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产能力（万吨）")
    private String zpsgjgscnl;

    /**
     * 装配式钢结构构件应用（万平方米）
     */
    @Excel(name = "装配式钢结构构件应用（万平方米）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件应用（万平方米）")
    private String zpsgjgyy;

    /**
     * 装配式钢结构构件生产线（条）
     */
    @Excel(name = "装配式钢结构构件生产线（条）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产线（条）")
    private String zpsmjgscx;

    /**
     * 装配式钢结构构件生产能力（万吨）
     */
    @Excel(name = "装配式钢结构构件生产能力（万吨）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件生产能力（万吨）")
    private String zpsmjgscnl;

    /**
     * 装配式钢结构构件应用（万平方米）
     */
    @Excel(name = "装配式钢结构构件应用（万平方米）", width = 15)
    @ApiModelProperty(value = "装配式钢结构构件应用（万平方米）")
    private String zpsmjgyy;

    /**
     * 装配式木结构构件生产线（条）
     */
    @Excel(name = "厂址", width = 15)
    @ApiModelProperty(value = "厂址")
    private String companyaddress;

    /**
     * 联系人
     */
    @Excel(name = "联系人", width = 15)
    @ApiModelProperty(value = "联系人")
    private String contacts;

    /**
     * 联系人电话
     */
    @Excel(name = "联系人电话", width = 15)
    @ApiModelProperty(value = "联系人电话")
    private String contactstelephone;

    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private String isdelete;

    /**
     * 区县/市州（1、区县；2、市州）
     */
    @Excel(name = "区县/市州（1、区县；2、市州）", width = 3)
    @ApiModelProperty(value = "区县/市州（1、区县；2、市州）")
    private String countyorcity;

    /**
     * 创建人账号
     */
    @Excel(name = "创建人账号", width = 15)
    @ApiModelProperty(value = "创建人账号")
    private String createpersonaccount;
    /**
     * 创建人名称
     */
    @Excel(name = "创建人名称", width = 15)
    @ApiModelProperty(value = "创建人名称")
    private String createpersonname;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private Date createtime;
}
