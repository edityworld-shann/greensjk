package org.jeecg.modules.green.sms.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 短信记录表
 * @Author: jeecg-boot
 * @Date: 2022-06-07
 * @Version: V1.0
 */
@Data
@TableName("zgs_sms_sending_record_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_sms_sending_record_log对象", description = "短信记录表")
public class ZgsSmsSendingRecordLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private String id;
    /**
     * 短信发送内容
     */
    @Excel(name = "短信发送内容", width = 15)
    @ApiModelProperty(value = "短信发送内容")
    private String sendingrecord;
    /**
     * 已发送验证码
     */
    @Excel(name = "已发送验证码", width = 15)
    @ApiModelProperty(value = "已发送验证码")
    private String verificationcode;
    /**
     * 接收方用户ID
     */
    @Excel(name = "接收方用户ID", width = 15)
    @ApiModelProperty(value = "接收方用户ID")
    @ValidateEncryptEntity(isEncrypt = true)
    private String username;
    /**
     * 接收方手机号
     */
    @Excel(name = "接收方手机号", width = 15)
    @ApiModelProperty(value = "接收方手机号")
    @ValidateEncryptEntity(isEncrypt = true)
    private String phonenumber;
    /**
     * 发送时间
     */
    @Excel(name = "发送时间", width = 15)
    @ApiModelProperty(value = "发送时间")
    private String createtime;
    /**
     * 发送账号
     */
    @Excel(name = "发送账号", width = 15)
    @ApiModelProperty(value = "发送账号")
    private String sendingAccount;
    /**
     * 发送状态（0：未成功、1：已发送）
     */
    @Excel(name = "发送状态（0：未成功、1：已发送）", width = 15)
    @ApiModelProperty(value = "发送状态（0：未成功、1：已发送）")
    private String status;
    /**
     * 接收单位
     */
    @Excel(name = "接收单位", width = 3000)
    @ApiModelProperty(value = "接收单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private String receiveCompany;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
