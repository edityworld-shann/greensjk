package org.jeecg.modules.green.common.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;

/**
 * @Description: 示范项目任务书
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
public interface ZgsProjecttaskMapper extends BaseMapper<ZgsProjecttask> {
    Page<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfo(Page<ZgsProjectlibraryListInfo> page, @Param(Constants.WRAPPER) Wrapper<ZgsProjecttask> queryWrapper);

    List<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfoTask(@Param(Constants.WRAPPER) Wrapper<ZgsProjecttask> queryWrapper);


    List<ZgsProjecttask> zgsProjectTaskListForYq(@Param(Constants.WRAPPER) Wrapper<ZgsProjecttask> queryWrapper);
}
