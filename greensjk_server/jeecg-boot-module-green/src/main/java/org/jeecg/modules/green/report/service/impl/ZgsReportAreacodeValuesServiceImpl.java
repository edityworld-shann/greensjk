package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportAreacodeValues;
import org.jeecg.modules.green.report.mapper.ZgsReportAreacodeValuesMapper;
import org.jeecg.modules.green.report.service.IZgsReportAreacodeValuesService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 装配式新开工和已竣工建筑总面积表
 * @Author: jeecg-boot
 * @Date:   2022-07-29
 * @Version: V1.0
 */
@Service
public class ZgsReportAreacodeValuesServiceImpl extends ServiceImpl<ZgsReportAreacodeValuesMapper, ZgsReportAreacodeValues> implements IZgsReportAreacodeValuesService {

}
