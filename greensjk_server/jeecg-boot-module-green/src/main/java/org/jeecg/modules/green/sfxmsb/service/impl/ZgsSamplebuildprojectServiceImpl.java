package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsSamplebuildproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsSamplebuildprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsSamplebuildprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 市政公用工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsSamplebuildprojectServiceImpl extends ServiceImpl<ZgsSamplebuildprojectMapper, ZgsSamplebuildproject> implements IZgsSamplebuildprojectService {

}
