package org.jeecg.modules.green.common.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.mapper.ZgsSciencetechtaskMapper;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostexpendclear;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.sfxmsb.entity.ZgsCommonFee;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificexpenditureclear;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertresearcher;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 科技项目任务书申报
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Service
@Slf4j
public class ZgsSciencetechtaskServiceImpl extends ServiceImpl<ZgsSciencetechtaskMapper, ZgsSciencetechtask> implements IZgsSciencetechtaskService {

    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsTaskscienceparticipantService zgsTaskscienceparticipantService;
    @Autowired
    private IZgsTaskbuildfundbudgetService zgsTaskbuildfundbudgetService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private AsyncScienceService asyncScienceService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsPlannedchangeparticipantService zgsPlannedchangeparticipantService;
    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    @Autowired
    private IZgsScientificprojectfinishService zgsScientificprojectfinishService;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ZgsSciencetechtask getBySciencetechguid(String sciencetechguid, Integer accessType) {
        ZgsSciencetechtask zgsSciencetechtask = new ZgsSciencetechtask();
        try {
            ZgsSciencetechfeasible zgsSciencetechfeasible = zgsSciencetechfeasibleService.getBySciencetechguid(sciencetechguid);
            if (zgsSciencetechfeasible != null) {
                //查询任务书数据
                QueryWrapper<ZgsSciencetechtask> queryWrapper = new QueryWrapper();
                queryWrapper.eq("sciencetechguid", sciencetechguid);
                queryWrapper.ne("isdelete", 1);
                zgsSciencetechtask = ValidateEncryptEntityUtil.validateDecryptObject(this.baseMapper.selectOne(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
                /* if (accessType != null && accessType == 11) {
                    //变更新增选中项目后，判断是否逾期，逾期前一个月可操作变更
                    Date dateYq = zgsSciencetechfeasible.getEnddate();
                    //判断是否有延期申请变更
                    QueryWrapper<ZgsPlannedprojectchange> bglog1 = new QueryWrapper();
                    bglog1.and(bg -> {
                        bg.eq("projectlibraryguid", zgsSciencetechfeasible.getId());
                    });
                    bglog1.eq("status", GlobalConstants.SHENHE_STATUS8);
                    bglog1.ne("isdelete", 1);
                    bglog1.orderByDesc("firstdate");
                    List<ZgsPlannedprojectchange> bgLogList1 = zgsPlannedprojectchangeService.list(bglog1);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail1 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList1 = null;
                    if (bgLogList1.size() > 0) {
                        for (int m = 0; m < bgLogList1.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog11 = new QueryWrapper();
                            bglog11.eq("baseguid", bgLogList1.get(m).getId());
                            bglog11.eq("item_key", 7);
                            bglog11.ne("isdelete", 1);
                            bglog11.orderByDesc("createdate");
                            plannedprojectchangedetailList1 = zgsPlannedprojectchangedetailService.list(bglog11);
                            if (plannedprojectchangedetailList1 != null && plannedprojectchangedetailList1.size() > 0) {
                                zgsPlannedprojectchangedetail1 = plannedprojectchangedetailList1.get(0);
                            }
                        }
                    }
                    if (zgsPlannedprojectchangedetail1 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail1.getChangecontent())) {
                        //获取到该项目变更日期
                        dateYq = dateFormat.parse(zgsPlannedprojectchangedetail1.getChangecontent());
                    }
                    //时间对比=天
                    Long time = dateYq.getTime();
                    Long time2 = new Date().getTime();
                    Long a= time - time2;
                    long dateTemp = (dateYq.getTime() - new Date().getTime()) / (24 * 60 * 60 * 1000);
                    if (dateTemp < 0) {
                        //判断是否人为新增变更次数
                        QueryWrapper<ZgsPlannedprojectchange> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", sciencetechguid);
                        int bg_count = 0;
                        if (zgsSciencetechtask.getBgcount() != null) {
                            bg_count = zgsSciencetechtask.getBgcount();
                        }
                        int size_bg = zgsPlannedprojectchangeService.list(qTask).size();
                        if (!(size_bg < bg_count)) {
                            //该项目已逾期，不允许变更
                            zgsSciencetechtask = new ZgsSciencetechtask();
                            zgsSciencetechtask.setIsYuQ(1);
                            return zgsSciencetechtask;
                        }
                    }
                }*/
                //
                boolean isFlag = false;
                if (accessType != null && accessType == 1) {
                    //新增任务书时的关联查询accessType == 1
                    isFlag = true;
                    zgsSciencetechtask = new ZgsSciencetechtask();
                    //基本信息
                    zgsSciencetechtask.setSciencetechguid(zgsSciencetechfeasible.getId());
                    zgsSciencetechtask.setProjectname(zgsSciencetechfeasible.getProjectname());
                    zgsSciencetechtask.setCommitmentunit(zgsSciencetechfeasible.getCommitmentunit());
                    zgsSciencetechtask.setProjectleader(zgsSciencetechfeasible.getProjectleader());
                    zgsSciencetechtask.setProjectnum(zgsSciencetechfeasible.getProjectnum());
                    zgsSciencetechtask.setStartdate(zgsSciencetechfeasible.getStartdate());
                    zgsSciencetechtask.setEnddate(zgsSciencetechfeasible.getEnddate());
                    zgsSciencetechtask.setProdemoncontent("示范内容：" + zgsSciencetechfeasible.getResearchcontent() + "\n\n考核目标：" + zgsSciencetechfeasible.getResearchresult());
                } else {
                    if (zgsSciencetechtask == null) {
                        return null;
                    }
                }
                //
                zgsSciencetechtask.setBid(sciencetechguid);
                //费用编辑状态0可编辑，1不可编辑，2明细可编辑
                if (StringUtils.isEmpty(zgsSciencetechfeasible.getFeeStatus())) {
                    zgsSciencetechtask.setFeeStatus("1");
                } else {
                    zgsSciencetechtask.setFeeStatus(zgsSciencetechfeasible.getFeeStatus());
                }
                //
                if (isFlag) {
                    zgsSciencetechtask.setEquipmentcost(zgsSciencetechfeasible.getEquipmentcost());
                    zgsSciencetechtask.setEquipmentchange(zgsSciencetechfeasible.getEquipmentchange());
                    zgsSciencetechtask.setMaterialfee(zgsSciencetechfeasible.getMaterialfee());
                    zgsSciencetechtask.setPublicationfee(zgsSciencetechfeasible.getPublicationfee());
                    zgsSciencetechtask.setAssessmentfee(zgsSciencetechfeasible.getAssessmentfee());
                    zgsSciencetechtask.setTrainingfee(zgsSciencetechfeasible.getTrainingfee());
                    zgsSciencetechtask.setEquipmenttrial(zgsSciencetechfeasible.getEquipmenttrial());
                    zgsSciencetechtask.setProcessfee(zgsSciencetechfeasible.getProcessfee());
                    zgsSciencetechtask.setFuelpowerfee(zgsSciencetechfeasible.getFuelpowerfee());
                    zgsSciencetechtask.setTravelfee(zgsSciencetechfeasible.getTravelfee());
                    zgsSciencetechtask.setMeetingfee(zgsSciencetechfeasible.getMeetingfee());
                    zgsSciencetechtask.setExchangefee(zgsSciencetechfeasible.getExchangefee());
                    zgsSciencetechtask.setLabourfee(zgsSciencetechfeasible.getLabourfee());
                    zgsSciencetechtask.setManagefee(zgsSciencetechfeasible.getManagefee());
                    //项目组成员
                    List<ZgsScienceparticipant> zgsScienceparticipantList0 = zgsSciencetechfeasible.getZgsScienceparticipantList0();
                    if (zgsScienceparticipantList0 != null && zgsScienceparticipantList0.size() > 0) {
                        ZgsTaskscienceparticipant zgsTaskscienceparticipant = null;
                        ZgsScienceparticipant zgsScienceparticipant = null;
                        List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList = new ArrayList<>();
                        for (int i = 0; i < zgsScienceparticipantList0.size(); i++) {
                            int orderNum = i + 1;
                            zgsScienceparticipant = zgsScienceparticipantList0.get(i);
                            zgsTaskscienceparticipant = new ZgsTaskscienceparticipant();
                            zgsTaskscienceparticipant.setOrdernum(new BigDecimal(orderNum));
                            zgsTaskscienceparticipant.setPersonname(zgsScienceparticipant.getPersonname());
                            zgsTaskscienceparticipant.setPersonpost(zgsScienceparticipant.getPersonpost());
                            zgsTaskscienceparticipant.setUnit(zgsScienceparticipant.getUnit());
                            zgsTaskscienceparticipant.setWorkcontent(zgsScienceparticipant.getWorkcontent());
                            zgsTaskscienceparticipant.setProfessional(zgsScienceparticipant.getSpecialty());
                            zgsTaskscienceparticipant.setSex(zgsScienceparticipant.getSex());
                            if (StringUtils.isNotEmpty(zgsScienceparticipant.getIdcard())) {
                                zgsTaskscienceparticipant.setIdcard(zgsScienceparticipant.getIdcard());
                                zgsTaskscienceparticipant.setIdcard(zgsScienceparticipant.getIdcard());
                            }
                            zgsTaskscienceparticipant.setStandardOfCulture(zgsScienceparticipant.getStandardOfCulture());
                            zgsTaskscienceparticipantList.add(zgsTaskscienceparticipant);
                        }
                        zgsSciencetechtask.setZgsTaskparticipantList(zgsTaskscienceparticipantList);
                    }
                    //联合申报单位
                    zgsSciencetechtask.setZgsSciencejointunitList(zgsSciencetechfeasible.getZgsSciencejointunitList());
                    //经费预算
                    List<ZgsSciencefundbudget> zgsSciencefundbudgetList = zgsSciencetechfeasible.getZgsSciencefundbudgetList();
                    ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = new ZgsTaskbuildfundbudget();
                    List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = new ArrayList<>();
                    BigDecimal provincialfund = new BigDecimal(0);
                    BigDecimal bankloans = new BigDecimal(0);
                    BigDecimal unitselffund = new BigDecimal(0);
                    BigDecimal nationalinvest = new BigDecimal(0);
                    BigDecimal totalfund = new BigDecimal(0);
                    if (zgsSciencefundbudgetList != null && zgsSciencefundbudgetList.size() > 0) {
                        ZgsSciencefundbudget zgsSciencefundbudget = null;
                        for (int i = 0; i < zgsSciencefundbudgetList.size(); i++) {
                            //计算累计
                            zgsSciencefundbudget = zgsSciencefundbudgetList.get(i);
                            if (zgsSciencefundbudget.getProvincialfund() != null) {
                                provincialfund = provincialfund.add(zgsSciencefundbudget.getProvincialfund());
                            }
                            if (zgsSciencefundbudget.getBankloans() != null) {
                                bankloans = bankloans.add(zgsSciencefundbudget.getBankloans());
                            }
                            if (zgsSciencefundbudget.getUnitselffund() != null) {
                                unitselffund = unitselffund.add(zgsSciencefundbudget.getUnitselffund());
                            }
                            if (zgsSciencefundbudget.getNationalinvest() != null) {
                                nationalinvest = nationalinvest.add(zgsSciencefundbudget.getNationalinvest());
                            }
                            if (zgsSciencefundbudget.getTotalfund() != null) {
                                totalfund = totalfund.add(zgsSciencefundbudget.getTotalfund());
                            }
                        }
                    }
//                    zgsTaskbuildfundbudget.setProvincialfund(provincialfund);
                    zgsTaskbuildfundbudget.setProvincialfund(zgsSciencetechfeasible.getProvincialfundtotal());
                    if (StringUtils.isNotEmpty(zgsSciencetechtask.getFeeStatus())) {
                        if ("1".equals(zgsSciencetechtask.getFeeStatus()) || "2".equals(zgsSciencetechtask.getFeeStatus())) {
                            zgsTaskbuildfundbudget.setProvincialfund(new BigDecimal(0));
                        }
                    }
                    zgsTaskbuildfundbudget.setBankloans(new BigDecimal(0));
                    zgsTaskbuildfundbudget.setUnitselffund(unitselffund);
                    zgsTaskbuildfundbudget.setNationalinvest(new BigDecimal(0));
                    zgsTaskbuildfundbudget.setTotalfund(totalfund);
//                    zgsTaskbuildfundbudget.setProvincialfundtotal(zgsSciencetechfeasible.getProvincialfundtotal());
                    zgsTaskbuildfundbudgetList.add(zgsTaskbuildfundbudget);
                    zgsSciencetechtask.setZgsTaskbuildfundbudgetList(zgsTaskbuildfundbudgetList);
                } else {
                    //任务书以下菜单全部信息，如人员、费用、单位都从任务书关联查询
                    //项目组成员
                    QueryWrapper<ZgsTaskscienceparticipant> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("taskguid", zgsSciencetechtask.getId());
                    queryWrapper1.ne("isdelete", 1);
                    queryWrapper1.orderByAsc("ordernum");
                    queryWrapper1.last("limit 15");
                    List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsTaskscienceparticipantService.list(queryWrapper1), ValidateEncryptEntityUtil.isDecrypt);
                    //先查该项目所有变更记录为下面做筛选条件使用
                    QueryWrapper<ZgsPlannedprojectchange> mqwd = new QueryWrapper();
                    mqwd.eq("projectlibraryguid", sciencetechguid);
                    mqwd.eq("status", GlobalConstants.SHENHE_STATUS8);
                    mqwd.ne("isdelete", 1);
                    List<ZgsPlannedprojectchange> listBg = zgsPlannedprojectchangeService.list(mqwd);
                    if (listBg.size() > 0) {
                        //查变更历史记录（多次变更按照创建日期升序循环遍历）
                        QueryWrapper<ZgsPlannedprojectchangedetail> mqueryWrapper = new QueryWrapper();
                        mqueryWrapper.ne("isdelete", 1);
                        StringBuilder stringBuilder0_1 = new StringBuilder();
                        for (int i = 0; i < listBg.size(); i++) {
                            stringBuilder0_1.append("'");
                            stringBuilder0_1.append(listBg.get(i).getId());
                            stringBuilder0_1.append("'");
                            if (i < listBg.size() - 1) {
                                stringBuilder0_1.append(",");
                            }
                        }
                        mqueryWrapper.inSql("baseguid", stringBuilder0_1.toString());
                        mqueryWrapper.orderByAsc("createdate");
                        List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList = zgsPlannedprojectchangedetailService.list(mqueryWrapper);
//                        未使用（0项目名称、1立项编号、2项目承担单位、3任务书编号、9电子邮箱、10邮编）
//                        已使用（4项目责任人、5工作单位、6通信地址、7终止时间、8联系电话、11项目组成员）
                        if (zgsPlannedprojectchangedetailList.size() > 0) {
                            for (ZgsPlannedprojectchangedetail plannedprojectchangedetail : zgsPlannedprojectchangedetailList) {
                                String itemKey = plannedprojectchangedetail.getItemKey();
                                if ("4".equals(itemKey)) {
                                    //项目负责人
                                    zgsSciencetechtask.setProjectleader(plannedprojectchangedetail.getChangecontent());
                                } else if ("5".equals(itemKey)) {
                                    //工作单位
                                    zgsSciencetechtask.setCommitmentunit(plannedprojectchangedetail.getChangecontent());
                                } else if ("6".equals(itemKey)) {
                                    //通信地址
                                    zgsSciencetechtask.setUnitaddress(plannedprojectchangedetail.getChangecontent());
                                } else if ("7".equals(itemKey)) {
                                    //终止时间
                                    try {
                                        zgsSciencetechtask.setEnddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } else if ("8".equals(itemKey)) {
                                    //联系电话
                                    zgsSciencetechtask.setHeadmobile(plannedprojectchangedetail.getChangecontent());
                                }
                            }
                        }
                    }
                    //
                    zgsSciencetechtask.setZgsTaskparticipantList(zgsTaskscienceparticipantList);
                    //经费预算
                    QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("taskguid", zgsSciencetechtask.getId());
                    queryWrapper3.ne("isdelete", 1);
                    queryWrapper3.orderByAsc("year");
                    List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsTaskbuildfundbudgetService.list(queryWrapper3);
                    if (zgsTaskbuildfundbudgetList.size() > 0) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(0);
                        if (zgsTaskbuildfundbudget != null) {
                            if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                                zgsSciencetechtask.setFundType("1");
                            }
                        }
                    }
                    zgsSciencetechtask.setZgsTaskbuildfundbudgetList(zgsTaskbuildfundbudgetList);
                    //费用明细
                    ZgsCommonFee zgsCommonFee = new ZgsCommonFee();
                    zgsCommonFee.setEquipmentcost(zgsSciencetechtask.getEquipmentcost());
                    zgsCommonFee.setEquipmentchange(zgsSciencetechtask.getEquipmentchange());
                    zgsCommonFee.setMaterialfee(zgsSciencetechtask.getMaterialfee());
                    zgsCommonFee.setPublicationfee(zgsSciencetechtask.getPublicationfee());
                    zgsCommonFee.setAssessmentfee(zgsSciencetechtask.getAssessmentfee());
                    zgsCommonFee.setTrainingfee(zgsSciencetechtask.getTrainingfee());
                    zgsCommonFee.setEquipmenttrial(zgsSciencetechtask.getEquipmenttrial());
                    zgsCommonFee.setProcessfee(zgsSciencetechtask.getProcessfee());
                    zgsCommonFee.setFuelpowerfee(zgsSciencetechtask.getFuelpowerfee());
                    zgsCommonFee.setTravelfee(zgsSciencetechtask.getTravelfee());
                    zgsCommonFee.setMeetingfee(zgsSciencetechtask.getMeetingfee());
                    zgsCommonFee.setExchangefee(zgsSciencetechtask.getExchangefee());
                    zgsCommonFee.setLabourfee(zgsSciencetechtask.getLabourfee());
                    zgsCommonFee.setManagefee(zgsSciencetechtask.getManagefee());
                    zgsSciencetechtask.setZgsCommonFee(zgsCommonFee);

                    //联合申报单位
                    QueryWrapper<ZgsSciencejointunit> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("scienceguid", sciencetechguid);
                    queryWrapper2.eq("tag", "1");
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper2);
                    if (zgsSciencejointunitList.size() == 0) {
                        QueryWrapper<ZgsSciencejointunit> queryWrapper2_1 = new QueryWrapper<>();
                        queryWrapper2_1.eq("scienceguid", sciencetechguid);
                        queryWrapper2_1.ne("isdelete", 1);
                        zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper2_1);
                    }


                    // 申报阶段_科技攻关项目_申报单位信息
                    QueryWrapper<ZgsSciencejointunit> queryWrapper8 = new QueryWrapper<>();
                    // queryWrapper8.eq("enterpriseguid", sysUser);
                    queryWrapper8.eq("scienceguid", sciencetechguid);
                    queryWrapper8.isNull("tag");
                    queryWrapper8.ne("isdelete", 1);
                    List<ZgsSciencejointunit> zgsSciencejointunitListForKjgg = zgsSciencejointunitService.list(queryWrapper8);

                    // 申报阶段_软科学_申报单位信息
                    QueryWrapper<ZgsSciencejointunit> queryWrapper9 = new QueryWrapper<>();
                    // queryWrapper9.eq("enterpriseguid", enterpriseguid);
                    queryWrapper9.eq("scienceguid", sciencetechguid);
                    queryWrapper9.isNull("tag");
                    queryWrapper9.ne("isdelete", 1);
                    List<ZgsSciencejointunit> zgsSciencejointunitListForRkx = zgsSciencejointunitService.list(queryWrapper9);

                    // 如果原有项目完成单位信息不存在，则直接获取申报阶段填写的单位信息  rxl 20230729
                    if (zgsSciencejointunitList.size() > 0) {
                        zgsSciencetechtask.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitList, ValidateEncryptEntityUtil.isDecrypt));
                    } else if (zgsSciencejointunitListForKjgg.size() > 0) { // 申报阶段_科技攻关的完成单位信息
                        zgsSciencetechtask.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForKjgg, ValidateEncryptEntityUtil.isDecrypt));
                    } else if (zgsSciencejointunitListForRkx.size() > 0) {
                        zgsSciencetechtask.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitListForRkx, ValidateEncryptEntityUtil.isDecrypt));
                    }

                    //项目完成单位情况
                    QueryWrapper<ZgsScientificprojectfinish> queryWrapper4 = new QueryWrapper<>();
                    queryWrapper4.eq("enterpriseguid", zgsSciencetechfeasible.getEnterpriseguid());
                    queryWrapper4.eq("baseguid", sciencetechguid);
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.orderByAsc("ordernum");
                    queryWrapper4.last("limit 5");
                    List<ZgsScientificprojectfinish> zgsScientificprojectfinishList = zgsScientificprojectfinishService.list(queryWrapper4);
                    zgsSciencetechtask.setZgsScientificprojectfinishList(ValidateEncryptEntityUtil.validateDecryptList(zgsScientificprojectfinishList, ValidateEncryptEntityUtil.isDecrypt));


                    List<ZgsPlannedchangeparticipant> pants = null;
                    //人员问题单独再针对每个菜单对应的实体类转换一下，否则初次新增时因对象不同，不能回显
                    //判断是否有项目组成员变更，如果有从变更查出替代
                    if (accessType != null && zgsTaskscienceparticipantList.size() > 0) {
                        if (StringUtils.isNotEmpty(zgsSciencetechtask.getId()) && listBg.size() > 0) {
                            QueryWrapper<ZgsPlannedchangeparticipant> xmzcyBg = new QueryWrapper();
                            xmzcyBg.eq("taskguid", zgsSciencetechtask.getId());
                            xmzcyBg.orderByAsc("ordernum");
                            pants = ValidateEncryptEntityUtil.validateDecryptList(zgsPlannedchangeparticipantService.list(xmzcyBg), ValidateEncryptEntityUtil.isDecrypt);
                            if (pants.size() > 0) {
                                zgsTaskscienceparticipantList = copy(pants, ZgsTaskscienceparticipant.class);
                            }
                        }
                        //accessType=1(任务书新增)、accessType=2(项目验收管理-科研项目验收申请)、accessType=3(项目验收管理-科研项目结题申请)
                        //accessType=4(项目验收结题证书-科研项目验收证书人员)、accessType=5(项目验收结题证书-示范项目验收证书人员)、accessType=6(项目验收结题证书-科研项目结题证书人员)
                        switch (accessType) {
                            case 2:
                                //科研项目申报验收人员
                                List<ZgsScientificresearcher> zgsScientificresearcherList = new ArrayList<>();
                                ZgsScientificresearcher searcher2 = null;
                                for (ZgsTaskscienceparticipant pant : zgsTaskscienceparticipantList) {
                                    searcher2 = new ZgsScientificresearcher();
                                    searcher2.setName(pant.getPersonname());
                                    searcher2.setSex(pant.getSex());
                                    searcher2.setIdcard(pant.getIdcard());
                                    searcher2.setTechnicaltitle(pant.getPersonpost());
                                    searcher2.setWorkunit(pant.getUnit());
                                    searcher2.setContribution(pant.getWorkcontent());
                                    searcher2.setOrdernum(pant.getOrdernum());
                                    zgsScientificresearcherList.add(searcher2);
                                }
                                zgsSciencetechtask.setZgsScientificresearcherList(zgsScientificresearcherList);
                                //经费
                                ZgsScientificexpenditureclear zgsScientificexpenditureclear = new ZgsScientificexpenditureclear();
                                zgsScientificexpenditureclear.setEquipment(zgsSciencetechtask.getEquipmentcost());
                                zgsScientificexpenditureclear.setMaterial(zgsSciencetechtask.getMaterialfee());
                                zgsScientificexpenditureclear.setProcesscost(zgsSciencetechtask.getProcessfee());
                                zgsScientificexpenditureclear.setFuelcost(zgsSciencetechtask.getFuelpowerfee());
                                zgsScientificexpenditureclear.setTravelexpense(zgsSciencetechtask.getTravelfee());
                                zgsScientificexpenditureclear.setCoferemce(zgsSciencetechtask.getMeetingfee());
                                zgsScientificexpenditureclear.setExchange(zgsSciencetechtask.getExchangefee());
                                zgsScientificexpenditureclear.setAppear(zgsSciencetechtask.getPublicationfee());
                                zgsScientificexpenditureclear.setServicefee(zgsSciencetechtask.getLabourfee());
                                zgsScientificexpenditureclear.setConsult(zgsSciencetechtask.getAssessmentfee());
                                zgsScientificexpenditureclear.setOtherexpense(zgsSciencetechtask.getTrainingfee());
                                if (zgsTaskbuildfundbudgetList.size() > 0) {
                                    zgsScientificexpenditureclear.setTotalincome(zgsTaskbuildfundbudgetList.get(0).getTotalfund());
                                    zgsScientificexpenditureclear.setProvinceappropriation(zgsTaskbuildfundbudgetList.get(0).getProvincialfund());
                                    zgsScientificexpenditureclear.setUnitraised(zgsTaskbuildfundbudgetList.get(0).getUnitselffund());
                                }
                                zgsSciencetechtask.setZgsScientificexpenditureclear(zgsScientificexpenditureclear);
                                break;
                            case 3:
                                //科研项目申报结项人员
                                List<ZgsScientificpostresearcher> zgsScientificpostresearcherList = new ArrayList<>();
                                ZgsScientificpostresearcher searcher3 = null;
                                for (ZgsTaskscienceparticipant pant : zgsTaskscienceparticipantList) {
                                    searcher3 = new ZgsScientificpostresearcher();
                                    searcher3.setName(pant.getPersonname());
                                    searcher3.setSex(pant.getSex());
                                    searcher3.setTechnicaltitle(pant.getPersonpost());
                                    searcher3.setWorkunit(pant.getUnit());
                                    searcher3.setContribution(pant.getWorkcontent());
                                    searcher3.setOrdernum(pant.getOrdernum());
                                    searcher3.setIdcard(pant.getIdcard());
                                    zgsScientificpostresearcherList.add(searcher3);
                                }
                                zgsSciencetechtask.setZgsScientificpostresearcherList(zgsScientificpostresearcherList);
                                //经费
                                ZgsScientificpostexpendclear zgsScientificpostexpendclear = new ZgsScientificpostexpendclear();
                                zgsScientificpostexpendclear.setEquipment(zgsSciencetechtask.getEquipmentcost());
                                zgsScientificpostexpendclear.setMaterial(zgsSciencetechtask.getMaterialfee());
                                zgsScientificpostexpendclear.setProcesscost(zgsSciencetechtask.getProcessfee());
                                zgsScientificpostexpendclear.setFuelcost(zgsSciencetechtask.getFuelpowerfee());
                                zgsScientificpostexpendclear.setTravelexpense(zgsSciencetechtask.getTravelfee());
                                zgsScientificpostexpendclear.setCoferemce(zgsSciencetechtask.getMeetingfee());
                                zgsScientificpostexpendclear.setExchange(zgsSciencetechtask.getExchangefee());
                                zgsScientificpostexpendclear.setAppear(zgsSciencetechtask.getPublicationfee());
                                zgsScientificpostexpendclear.setServicefee(zgsSciencetechtask.getLabourfee());
                                zgsScientificpostexpendclear.setConsult(zgsSciencetechtask.getAssessmentfee());
                                zgsScientificpostexpendclear.setOtherexpense(zgsSciencetechtask.getTrainingfee());
                                if (zgsTaskbuildfundbudgetList.size() > 0) {
                                    zgsScientificpostexpendclear.setTotalincome(zgsTaskbuildfundbudgetList.get(0).getTotalfund());
                                    zgsScientificpostexpendclear.setProvinceappropriation(zgsTaskbuildfundbudgetList.get(0).getProvincialfund());
                                    zgsScientificpostexpendclear.setUnitraised(zgsTaskbuildfundbudgetList.get(0).getUnitselffund());
                                }
                                zgsSciencetechtask.setZgsScientificpostexpendclear(zgsScientificpostexpendclear);
                                break;
                            case 4:
                                //科研项目验收证书人员
                                List<ZgsSacceptcertresearcher> zgsSacceptcertresearcherList = new ArrayList<>();
                                ZgsSacceptcertresearcher searcher4 = null;
                                for (ZgsTaskscienceparticipant pant : zgsTaskscienceparticipantList) {
                                    searcher4 = new ZgsSacceptcertresearcher();
                                    searcher4.setName(pant.getPersonname());
                                    searcher4.setSex(pant.getSex());
                                    searcher4.setIdcard(pant.getIdcard());
                                    searcher4.setTechnicaltitle(pant.getPersonpost());
                                    searcher4.setWorkunit(pant.getUnit());
                                    searcher4.setContribution(pant.getWorkcontent());
                                    searcher4.setOrdernum(pant.getOrdernum());
                                    searcher4.setEducation(pant.getStandardOfCulture());
                                    searcher4.setIdcard(pant.getIdcard());
                                    zgsSacceptcertresearcherList.add(searcher4);
                                }
                                zgsSciencetechtask.setZgsSacceptcertresearcherList(zgsSacceptcertresearcherList);
                                break;
                            case 6:
                                //科研项目结题证书人员
                                List<ZgsSpostresearcher> zgsSpostresearcherList = new ArrayList<>();
                                ZgsSpostresearcher searcher6 = null;
                                for (ZgsTaskscienceparticipant pant : zgsTaskscienceparticipantList) {
                                    searcher6 = new ZgsSpostresearcher();
                                    searcher6.setName(pant.getPersonname());
                                    searcher6.setSex(pant.getSex());
                                    searcher6.setIdcard(pant.getIdcard());
                                    searcher6.setTechnicaltitle(pant.getPersonpost());
                                    searcher6.setWorkunit(pant.getUnit());
                                    searcher6.setContribution(pant.getWorkcontent());
                                    searcher6.setOrdernum(pant.getOrdernum());
                                    searcher6.setEducation(pant.getStandardOfCulture());
                                    zgsSpostresearcherList.add(searcher6);
                                }
                                zgsSciencetechtask.setZgsSpostresearcherList(zgsSpostresearcherList);
                                break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zgsSciencetechtask;
    }

    @Override
    public Page<ZgsSciencetechtask> YqLqlist(Page<ZgsSciencetechtask> page, Wrapper<ZgsSciencetechtask> wrapper,String enterpriseguid) {
        Page<ZgsSciencetechtask> listInfoPage = this.baseMapper.YqLqlist(page, wrapper,enterpriseguid);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }


    @Override
    public List<ZgsSciencetechtask> listZgsScienceTechTaskListInfoTask(Wrapper<ZgsSciencetechtask> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.listZgsScienceTechTaskListInfoTask(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }





    public <T> List<T> copy(List<?> zgsTaskparticipantList, Class<T> temp) {
        String oldOb = JSON.toJSONString(zgsTaskparticipantList);
        return JSON.parseArray(oldOb, temp);
    }

    @Override
    public int spProject(ZgsSciencetechtask zgsSciencetechtask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getId())) {
            ZgsSciencetechtask sciencetechtask = new ZgsSciencetechtask();
            sciencetechtask.setId(zgsSciencetechtask.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(sciencetechtask.getId());
            zgsReturnrecord.setReturnreason(zgsSciencetechtask.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE8);
            ZgsSciencetechtask task = getById(zgsSciencetechtask.getId());
            if (task != null) {
                zgsReturnrecord.setEnterpriseguid(task.getEnterpriseguid());
                zgsReturnrecord.setApplydate(task.getApplydate());
                zgsReturnrecord.setProjectname(task.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", task.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsSciencetechtask.getSpStatus() == 1) {
                //通过记录状态==0
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                sciencetechtask.setAuditopinion(null);
                sciencetechtask.setFundRemark(zgsSciencetechtask.getFundRemark());
                //通过
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSciencetechtask.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsSciencetechtask.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsSciencetechtask.getStatus())) {
                    sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS4);
                    sciencetechtask.setFirstdate(new Date());
                    sciencetechtask.setFirstperson(sysUser.getRealname());
                    sciencetechtask.setFirstdepartment(sysUser.getEnterprisename());
                    sciencetechtask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    //初审通过后修改上报日期
                    sciencetechtask.setApplydate(new Date());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    sciencetechtask.setFundType(zgsProjecttaskService.initFundType(task.getSciencetechguid()));
                    sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS8);
                    sciencetechtask.setFinishperson(sysUser.getRealname());
                    sciencetechtask.setFinishdate(new Date());
                    sciencetechtask.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审通过异步生成任务书pdf
                    asyncScienceService.initScienceTechtaskPdfUrl(zgsSciencetechtask.getId());
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                sciencetechtask.setAuditopinion(zgsSciencetechtask.getAuditopinion());
                sciencetechtask.setFundRemark(zgsSciencetechtask.getFundRemark());
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSciencetechtask.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsSciencetechtask.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsSciencetechtask.getStatus())) {
                    if (zgsSciencetechtask.getSpStatus() == 2) {
                        sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    sciencetechtask.setFirstdate(new Date());
                    sciencetechtask.setFirstperson(sysUser.getRealname());
                    sciencetechtask.setAuditopinion(zgsSciencetechtask.getAuditopinion());
                    sciencetechtask.setFirstdepartment(sysUser.getEnterprisename());
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsSciencetechtask.getSpStatus() == 2) {
                        sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        sciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    sciencetechtask.setFinishperson(sysUser.getRealname());
                    sciencetechtask.setFinishdate(new Date());
                    sciencetechtask.setAuditopinion(zgsSciencetechtask.getAuditopinion());
                    sciencetechtask.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
                if (zgsSciencetechtask.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsSciencetechtask.getAuditopinion())) {
                        zgsSciencetechtask.setAuditopinion(" ");
                    }
                    sciencetechtask.setAuditopinion(zgsSciencetechtask.getAuditopinion());
                    sciencetechtask.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else {
                    //驳回
                    sciencetechtask.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(task.getSciencetechguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(sciencetechtask, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }

    @Override
    public int changeBg(ZgsSciencetechtask zgsSciencetechtask) {
        if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getId()) && StringUtils.isNotEmpty(zgsSciencetechtask.getSciencetechguid())) {
            QueryWrapper<ZgsPlannedprojectchange> queryWrapper = new QueryWrapper();
            queryWrapper.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid());
            queryWrapper.ne("isdelete", 1);
            List<ZgsPlannedprojectchange> listBg = zgsPlannedprojectchangeService.list(queryWrapper);
            int size = listBg.size();
            //首次新增变更不需要进行二次变更操作
//            if (size == 0) {
//                return 0;
//            } else {
            ZgsSciencetechtask sciencetechtask = new ZgsSciencetechtask();
            sciencetechtask.setBgcount(++size);
            sciencetechtask.setId(zgsSciencetechtask.getId());
            return this.baseMapper.updateById(sciencetechtask);
//            }
        }
        return 0;
    }

    @Override
    public void initProjectSelectById(ZgsSciencetechtask zgsSciencetechtask) {
        String taskid = zgsSciencetechtask.getId();
        String enterpriseguid = zgsSciencetechtask.getEnterpriseguid();
        String sciencetechguid = zgsSciencetechtask.getSciencetechguid();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(sciencetechguid)) {
            //
            ZgsSciencetechfeasible zgsSciencetechfeasible = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechfeasibleService.getById(sciencetechguid), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsSciencetechfeasible != null) {
                //费用编辑状态0可编辑，1不可编辑，2明细可编辑
                if (StringUtils.isEmpty(zgsSciencetechfeasible.getFeeStatus())) {
                    zgsSciencetechtask.setFeeStatus("1");
                } else {
                    zgsSciencetechtask.setFeeStatus(zgsSciencetechfeasible.getFeeStatus());
                }
            }
            //项目组成员
            QueryWrapper<ZgsTaskscienceparticipant> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("taskguid", taskid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            queryWrapper1.last("limit 15");
            List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsTaskscienceparticipantService.list(queryWrapper1), ValidateEncryptEntityUtil.isDecrypt);
            zgsSciencetechtask.setZgsTaskparticipantList(zgsTaskscienceparticipantList);
            //项目其他主要参加单位
            QueryWrapper<ZgsSciencejointunit> queryWrapper2 = new QueryWrapper<>();
//            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("scienceguid", sciencetechguid);
            queryWrapper2.eq("tag", "1");
            queryWrapper2.ne("isdelete", 1);
            queryWrapper2.orderByAsc("createdate");
            List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper2);
            if (zgsSciencejointunitList.size() == 0) {
                QueryWrapper<ZgsSciencejointunit> queryWrapper2_1 = new QueryWrapper<>();
//                queryWrapper2_1.eq("enterpriseguid", enterpriseguid);
                queryWrapper2_1.eq("scienceguid", sciencetechguid);
                queryWrapper2_1.ne("isdelete", 1);
                queryWrapper2_1.orderByAsc("createdate");
                zgsSciencejointunitList = zgsSciencejointunitService.list(queryWrapper2_1);
            }
            zgsSciencetechtask.setZgsSciencejointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsSciencejointunitList, ValidateEncryptEntityUtil.isDecrypt));
            //经费预算
            QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("taskguid", taskid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("year");
            List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsTaskbuildfundbudgetService.list(queryWrapper3);
            if (zgsTaskbuildfundbudgetList.size() > 0) {
                for (int i = 0; i < zgsTaskbuildfundbudgetList.size(); i++) {
                    zgsTaskbuildfundbudgetList.get(i).setProvinceFundRemark(zgsSciencetechfeasible.getFundRemark());
                }
            }
            zgsSciencetechtask.setZgsTaskbuildfundbudgetList(ValidateEncryptEntityUtil.validateDecryptList(zgsTaskbuildfundbudgetList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    @Override
    public boolean selectSciencetechExist(String sciencetechguid) {
        QueryWrapper<ZgsSciencetechtask> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        queryWrapper.eq("sciencetechguid", sciencetechguid);
        List<ZgsSciencetechtask> list = this.baseMapper.selectList(queryWrapper);
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public int updateDealTypeIsNull() {
        this.baseMapper.updateDealType1IsNull();
        this.baseMapper.updateDealType2IsNull();
        return 1;
    }

    @Override
    public int updateStageByBid1(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid1(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid2(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid2(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid3(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid3(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid4(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid4(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid5(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid5(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid6(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid6(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid7(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid7(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid8(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid8(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid9(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid9(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid10(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid10(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid11(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid11(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid12(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid12(projectstage, bid, initStatus(status));
    }

    @Override
    public int updateStageByBid13(String projectstage, String bid, Object status) {
        return this.baseMapper.updateStageByBid13(projectstage, bid, initStatus(status));
    }

    private String initStatus(Object status) {
        String statusTemp = null;
        if (status != null) {
            statusTemp = status.toString();
        }
        return statusTemp;
    }

    @Override
    public Page<ZgsSciencetechtask> listZgsSciencetechtask(Wrapper<ZgsSciencetechtask> wrapper,Integer pageNo,Integer pageSize) {
        Page<ZgsSciencetechtask> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsSciencetechtask> listInfoPage = this.baseMapper.getZgsSciencetechtask(pageMode,wrapper);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }

    /**
     * @describe:查询往期 逾期项目
     * @author: renxiaoliang
     * @date: 2023/10/28 14:42
     */
    @Override
    public List<ZgsSciencetechtask> zgsScienceTechTaskListForYq(Wrapper<ZgsSciencetechtask> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.zgsScienceTechTaskListForYq(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }
}
