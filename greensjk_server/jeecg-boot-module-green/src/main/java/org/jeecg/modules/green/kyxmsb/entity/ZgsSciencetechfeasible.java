package org.jeecg.modules.green.kyxmsb.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建设科技攻关项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-10
 * @Version: V1.0
 */
@Data
@TableName("zgs_sciencetechfeasible")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_sciencetechfeasible对象", description = "建设科技攻关项目库")
public class ZgsSciencetechfeasible implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
//    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 必要性（包括经济的、社会的、科学技术自身的要求）
     */
    @Excel(name = "必要性（包括经济的、社会的、科学技术自身的要求）", width = 15, orderNum = "8")
    @ApiModelProperty(value = "必要性（包括经济的、社会的、科学技术自身的要求）")
    private java.lang.String necessity;
    /**
     * 国内外现状和发展趋势及本课题研究成果的科学价值
     */
    @Excel(name = "国内外现状和发展趋势及本课题研究成果的科学价值", width = 15, orderNum = "9")
    @ApiModelProperty(value = "国内外现状和发展趋势及本课题研究成果的科学价值")
    private java.lang.String scientificvalue;
    /**
     * 社会价值
     */
    @Excel(name = "社会价值", width = 15, orderNum = "10")
    @ApiModelProperty(value = "社会价值")
    private java.lang.String socialvalue;
    /**
     * 研究内容及提交成果
     */
    @Excel(name = "研究内容及提交成果", width = 15, orderNum = "11")
    @ApiModelProperty(value = "研究内容及提交成果")
    private java.lang.String researchresult;
    /**
     * 资源利用及市场预测
     */
    @Excel(name = "资源利用及市场预测", width = 15, orderNum = "12")
    @ApiModelProperty(value = "资源利用及市场预测")
    private java.lang.String marketforecast;
    /**
     * 经济上的合理性
     */
    @Excel(name = "经济上的合理性", width = 15, orderNum = "13")
    @ApiModelProperty(value = "经济上的合理性")
    private java.lang.String economicrationality;
    /**
     * 技术上的合理性
     */
    @Excel(name = "技术上的合理性", width = 15, orderNum = "14")
    @ApiModelProperty(value = "技术上的合理性")
    private java.lang.String technicalrationality;
    /**
     * 计划进度
     */
    //@Excel(name = "计划进度", width = 15, orderNum = "15")
    @ApiModelProperty(value = "计划进度")
    private java.lang.String scheduleprogress;
    /**
     * 现有工作基础、装备水平与实验测试能力
     */
    //@Excel(name = "现有工作基础、装备水平与实验测试能力", width = 15, orderNum = "16")
    @ApiModelProperty(value = "现有工作基础、装备水平与实验测试能力")
    private java.lang.String testability;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd", orderNum = "2")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3初审退回，2终审通过，5终审退回给初审
     */
    // @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3初审退回，2终审通过，5终审退回给初审", width = 15)
    @Excel(name = "项目状态", width = 15, dicCode = "sfproject_status", orderNum = "7")
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3初审退回，2终审通过，5终审退回给初审")
    @Dict(dicCode = "sfproject_status")
    private java.lang.String status;
    /**
     * 立项补证状态（默认为空，1表示被补证修改过）
     */
    private String lxbzStatus;
    /**
     * 审核退回意见
     */
    @Excel(name = "退回原因", width = 15,orderNum = "10")
    @ApiModelProperty(value = "审核退回意见")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.lang.String auditopinion;
    /**
     * 初审时间
     */
//    @Excel(name = "初审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "初审时间")
    private java.util.Date firstdate;
    /**
     * 初审人
     */
//    @Excel(name = "初审人", width = 15)
    @ApiModelProperty(value = "初审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstperson;
    /**
     * 终审时间
     */
//    @Excel(name = "终审时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "终审时间")
    private java.util.Date finishdate;
    /**
     * 终审人
     */
//    @Excel(name = "终审人", width = 15)
    @ApiModelProperty(value = "终审人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishperson;
    /**
     * 创建人帐号
     */
//    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
//    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
//    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
//    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
//    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
//    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
//    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 初审部门
     */
    @Excel(name = "初审单位", width = 15, orderNum = "4")
    @ApiModelProperty(value = "初审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String firstdepartment;
    /**
     * 终审部门
     */
//    @Excel(name = "终审部门", width = 15)
    @ApiModelProperty(value = "终审部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String finishdepartment;
    /**
     * 科技投资效益率--投入产出比
     */
    @Excel(name = "科技投资效益率--投入产出比", width = 15, orderNum = "18")
    @ApiModelProperty(value = "科技投资效益率--投入产出比")
    private java.lang.String inoutputratio;
    /**
     * 科技投资效益率--效益比
     */
    @Excel(name = "科技投资效益率--效益比", width = 15, orderNum = "19")
    @ApiModelProperty(value = "科技投资效益率--效益比")
    private java.lang.String efficiencyratio;
    /**
     * 承担单位
     */
    @Excel(name = "申报单位", width = 15, orderNum = "3")
    @ApiModelProperty(value = "承担单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String commitmentunit;
    /**
     * 工程项目名称
     */
    @Excel(name = "工程项目名称", width = 15, orderNum = "1")
    @ApiModelProperty(value = "工程项目名称")
    private java.lang.String projectname;
    /**
     * 开始时间
     */
    @Excel(name = "开始时间", width = 15, format = "yyyy-MM-dd", orderNum = "3")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private java.util.Date startdate;
    /**
     * 截止时间
     */
    @Excel(name = "上报时间", width = 15, format = "yyyy-MM-dd", orderNum = "11")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "截止时间")
    private java.util.Date enddate;
    /**
     * 项目负责人
     */
    @Excel(name = "负责人", width = 15, orderNum = "5")
    @ApiModelProperty(value = "项目负责人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleader;
    /**
     * 项目负责人联系电话
     */
    @Excel(name = "项目负责人联系电话", width = 15, orderNum = "6")
    @ApiModelProperty(value = "项目负责人联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String projectleadertel;
    /**
     * 购置设备费
     */
    @Excel(name = "购置设备费", width = 15, orderNum = "20")
    @ApiModelProperty(value = "购置设备费")
    private java.math.BigDecimal equipmentcost;
    /**
     * 试制设备费
     */
    @Excel(name = "试制设备费", width = 15, orderNum = "21")
    @ApiModelProperty(value = "试制设备费")
    private java.math.BigDecimal equipmenttrial;
    /**
     * 设备改造与租赁费
     */
    @Excel(name = "设备改造与租赁费", width = 15, orderNum = "22")
    @ApiModelProperty(value = "设备改造与租赁费")
    private java.math.BigDecimal equipmentchange;
    /**
     * 材料费
     */
    @Excel(name = "材料费", width = 15, orderNum = "23")
    @ApiModelProperty(value = "材料费")
    private java.math.BigDecimal materialfee;
    /**
     * 测试化验加工费
     */
    //@Excel(name = "测试化验加工费", width = 15, orderNum = "24")
    @ApiModelProperty(value = "测试化验加工费")
    private java.math.BigDecimal processfee;
    /**
     * 燃料动力费
     */
    // @Excel(name = "燃料动力费", width = 15, orderNum = "25")
    @ApiModelProperty(value = "燃料动力费")
    private java.math.BigDecimal fuelpowerfee;
    /**
     * 差旅费
     */
    //@Excel(name = "差旅费", width = 15, orderNum = "26")
    @ApiModelProperty(value = "差旅费")
    private java.math.BigDecimal travelfee;
    /**
     * 会议费
     */
    //@Excel(name = "会议费", width = 15, orderNum = "27")
    @ApiModelProperty(value = "会议费")
    private java.math.BigDecimal meetingfee;
    /**
     * 合作与交流费
     */
    //@Excel(name = "合作与交流费", width = 15, orderNum = "28")
    @ApiModelProperty(value = "合作与交流费")
    private java.math.BigDecimal exchangefee;
    /**
     * 出版/文献/信息传播/知识产权事务费
     */
    //@Excel(name = "出版/文献/信息传播/知识产权事务费", width = 15, orderNum = "29")
    @ApiModelProperty(value = "出版/文献/信息传播/知识产权事务费")
    private java.math.BigDecimal publicationfee;
    /**
     * 劳务费
     */
    @Excel(name = "劳务费", width = 15, orderNum = "30")
    @ApiModelProperty(value = "劳务费")
    private java.math.BigDecimal labourfee;
    /**
     * 专家咨询评审费
     */
    //@Excel(name = "专家咨询评审费", width = 15, orderNum = "31")
    @ApiModelProperty(value = "专家咨询评审费")
    private java.math.BigDecimal assessmentfee;
    /**
     * 管理费
     */
    //@Excel(name = "管理费", width = 15, orderNum = "32")
    @ApiModelProperty(value = "管理费")
    private java.math.BigDecimal managefee;
    /**
     * 其他（含培训费）
     */
    //@Excel(name = "其他（含培训费）", width = 15, orderNum = "33")
    @ApiModelProperty(value = "其他（含培训费）")
    private java.math.BigDecimal trainingfee;
    /**
     * 软科学ScienceSoft，科技攻关 ScienceTech
     */
//    @Excel(name = "软科学ScienceSoft，科技攻关 ScienceTech", width = 15)
    @ApiModelProperty(value = "软科学ScienceSoft，科技攻关 ScienceTech")
    @Dict(dicCode = "ScienceType")
    private java.lang.String sciencetype;
    /**
     * 上级推荐单位（市州建设局或省直单位）编码
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）编码", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）编码")
    private java.lang.String higherupunitnum;
    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
//    @Excel(name = "上级推荐单位（市州建设局或省直单位）", width = 15)
    @ApiModelProperty(value = "上级推荐单位（市州建设局或省直单位）")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;
    /**
     * 研究内容
     */
    @Excel(name = "研究内容", width = 15, orderNum = "7")
    @ApiModelProperty(value = "研究内容")
    private java.lang.String researchcontent;
    /**
     * 现有工作基础
     */
    @Excel(name = "现有工作基础", width = 15, orderNum = "34")
    @ApiModelProperty(value = "现有工作基础")
    private java.lang.String workbasics;
    /**
     * 装备水平
     */
    @Excel(name = "装备水平", width = 15, orderNum = "35")
    @ApiModelProperty(value = "装备水平")
    private java.lang.String equipmentlevel;
    /**
     * 主要研究内容及预期成果简介
     */
    @Excel(name = "主要研究内容及预期成果简介", width = 15, orderNum = "36")
    @ApiModelProperty(value = "主要研究内容及预期成果简介")
    private java.lang.String simpleresearch;
    /**
     * 退回类型，1：不通过；2：补正修改；
     */
//    @Excel(name = "退回类型，1：不通过；2：补正修改；", width = 15)
    @ApiModelProperty(value = "退回类型，1：不通过；2：补正修改；")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal returntype;
    /**
     * 专家评审结果 1 ：不同意立项 ，2：同意立项
     */
    // @Excel(name = "是否立项", width = 15, orderNum = "9")
    @ApiModelProperty(value = "专家评审结果 1 ：不同意立项 ，2：同意立项")
    @Dict(dicCode = "agreeproject")
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal agreeproject;

    @Excel(name = "是否立项", width = 15, orderNum = "9")
    @ApiModelProperty(value = "专家评审结果 1 ：不同意立项 ，2：同意立项")
    @TableField(exist = false)
    private String agreeprojectStr;

    /**
     * 项目编号
     */
    @Excel(name = "项目编号", width = 15, orderNum = "2")
    @ApiModelProperty(value = "项目编号")
    private java.lang.String projectnum;
    /**
     * 不同意立项原因
     */
//    @Excel(name = "不同意立项原因", width = 15)
    @ApiModelProperty(value = "不同意立项原因")
    private java.lang.String notagreereason;
    /**
     * 0不是历史数据，1历史数据
     */
//    @Excel(name = "0不是历史数据，1历史数据", width = 15)
    @ApiModelProperty(value = "0不是历史数据，1历史数据")
    private java.lang.String ishistory;
    /**
     * 项目负责人年龄
     */
    //@Excel(name = "项目负责人年龄", width = 15, orderNum = "5")
    @ApiModelProperty(value = "项目负责人年龄")
    private java.math.BigDecimal age;
    /**
     * 费用状态
     */
    @ApiModelProperty(value = "费用状态")
    private String feeStatus = "0";
    //    @ExcelCollection(name = "负责单位及人员")
    @TableField(exist = false)
    private List<ZgsScienceparticipant> zgsScienceparticipantList0;
    //    @ExcelCollection(name = "参加单位及人员")
    @TableField(exist = false)
    private List<ZgsScienceparticipant> zgsScienceparticipantList1;
    //    @ExcelCollection(name = "工程计划进度与安排")
    @TableField(exist = false)
    private List<ZgsScienceplanarrange> zgsScienceplanarrangeList;
    //    @ExcelCollection(name = "联合申报单位表")
    @TableField(exist = false)
    private List<ZgsSciencejointunit> zgsSciencejointunitList;
    //    @ExcelCollection(name = "各类项目工程申报材料")
    @TableField(exist = false)
    private List<ZgsMattermaterial> zgsMattermaterialList;
    //    @ExcelCollection(name = "科技攻关项目经费预算")
    @TableField(exist = false)
    private List<ZgsSciencefundbudget> zgsSciencefundbudgetList;
    @TableField(exist = false)
    private Integer spStatus;
    //    @ExcelCollection(name = "科技类审批记录")
    @TableField(exist = false)
    private List<ZgsSofttechexpert> zgsSofttechexpertList;
    //新增和首次编辑的时候隐藏审批记录
    @TableField(exist = false)
    private Integer spLogStatus;
    @TableField(exist = false)
    private Object cstatus;
    @TableField(exist = false)
    private String applyStatus = "1";

    @Excel(name = "项目阶段", width = 15, orderNum = "6")
    @ApiModelProperty(value = "项目阶段1-7")
    private String projectstage;

    // @Excel(name = "项目状态", width = 15, orderNum = "7")
    @ApiModelProperty(value = "项目阶段状态")
    @Dict(dicCode = "sfproject_status")
    private String projectstagestatus;

    /**
     * 1：终止日期延期2:闭合-终止
     */
    @ApiModelProperty(value = "1：终止日期延期2:闭合-终止")
    private BigDecimal isdealoverdue;
    @ApiModelProperty(value = "申报年度编码")
    private java.lang.String yearNum;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
    /**
     * 是否会审
     */
    @Dict(dicCode = "yn")
    @Excel(name = "是否会审", width = 15, orderNum = "8")
    private String sfhs;


    @ApiModelProperty(value = "省科技经费总额")
//    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private java.math.BigDecimal provincialfundtotal;
    @TableField(exist = false)
    private Integer moneyStatus;

    @TableField(exist = false)
    private List<ZgsSciencetechfeasible> ZgsSciencetechfeasibleList;

    // 添加专家信息
    @TableField(exist = false)
    private List<ZgsSacceptcertexpert> zgsSacceptcertexpertList;
    // 1 表示从工作台进来
    @TableField(exist = false)
    private String flagByWorkTable;

    // 2 工作台 查看省厅待审批项目
    @TableField(exist = false)
    private String dspForSt;

    // 3 工作台 查看推荐单位待审批项目
    @TableField(exist = false)
    private String dspForTjdw;

    // 项目完成单位
    @TableField(exist = false)
    private List<ZgsScientificprojectfinish> zgsScientificprojectfinishList;

    // 往期逾期项目
    @TableField(exist = false)
    private List<ZgsSciencetechtask> yqProjectList;

    // 项目逾期标识  1逾期   2不逾期
    @TableField(exist = false)
    private String yqFlag;

    @ApiModelProperty(value = "授权编辑 1同意编辑  2取消编辑")
    private String sqbj;

    @ApiModelProperty(value = "编辑保存 1修改未提交  2修改已提交")
    private String updateSave;

    // 1 表示申报单位可编辑    2 表示取消编辑，编辑按钮不存在
    @TableField(exist = false)
    private String updateFlag;

    // 编辑状态  1 修改未提交   2修改已提交
    @TableField(exist = false)
    private String bjzt;

    @TableField(exist = false)
    private boolean sqbjBol = true;

    @ApiModelProperty(value = "经费备注")
    private java.lang.String fundRemark;



}
