package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsAssembleproject;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsAssembleprojectMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsAssembleprojectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 装配式建筑示范工程业务主表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsAssembleprojectServiceImpl extends ServiceImpl<ZgsAssembleprojectMapper, ZgsAssembleproject> implements IZgsAssembleprojectService {

}
