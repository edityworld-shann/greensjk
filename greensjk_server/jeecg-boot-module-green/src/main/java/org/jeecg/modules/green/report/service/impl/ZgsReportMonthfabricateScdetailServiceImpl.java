package org.jeecg.modules.green.report.service.impl;

import org.jeecg.modules.green.report.entity.ZgsReportMonthfabricateScdetail;
import org.jeecg.modules.green.report.mapper.ZgsReportMonthfabricateScdetailMapper;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabricateScdetailService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: zgs_report_monthfabricate_scdetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Service
public class ZgsReportMonthfabricateScdetailServiceImpl extends ServiceImpl<ZgsReportMonthfabricateScdetailMapper, ZgsReportMonthfabricateScdetail> implements IZgsReportMonthfabricateScdetailService {

}
