package org.jeecg.modules.green.jhxmbgsq.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificpexecutivelist;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 建设科技计划项目变更申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "建设科技计划项目变更申请业务主表")
@RestController
@RequestMapping("/jhxmbgsq/zgsPlannedprojectchange")
@Slf4j
public class ZgsPlannedprojectchangeController extends JeecgController<ZgsPlannedprojectchange, IZgsPlannedprojectchangeService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsPlannedchangeparticipantService zgsPlannedchangeparticipantService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;

    /**
     * 分页列表查询
     *
     * @param zgsPlannedprojectchange
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-分页列表查询")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-分页列表查询", notes = "建设科技计划项目变更申请业务主表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsPlannedprojectchange zgsPlannedprojectchange,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsPlannedprojectchange> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsPlannedprojectchange.getProjectname())) {
            queryWrapper.like("projectname", zgsPlannedprojectchange.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsPlannedprojectchange.getTasknum())) {
            queryWrapper.and(qwd -> {
                qwd.like("tasknum", zgsPlannedprojectchange.getTasknum()).or().like("setupnum", zgsPlannedprojectchange.getTasknum());
            });
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(setupnum,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsPlannedprojectchange.getIntype())) {
            queryWrapper.eq("intype", zgsPlannedprojectchange.getIntype());
        }
//        QueryWrapperUtil.initPlannedSelect(queryWrapper, zgsPlannedprojectchange.getStatus(), sysUser);
        // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsPlannedprojectchange.getStatus(), sysUser, 1);

        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsPlannedprojectchange.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("isdelete", 1);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("researchenddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("researchenddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            /*queryWrapper.orderByDesc("firstdate");
            queryWrapper.orderByDesc("applydate");*/
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        }
        Page<ZgsPlannedprojectchange> page = new Page<ZgsPlannedprojectchange>(pageNo, pageSize);
        // IPage<ZgsPlannedprojectchange> pageList = zgsPlannedprojectchangeService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsPlannedprojectchange> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsPlannedprojectchange> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsPlannedprojectchange> pageList = null;

        // 项目变更
        redisUtil.expire("xmbg",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmbgOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmbgOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        redisUtil.expire("xmzz",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmzzOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("xmzzOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）


        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsPlannedprojectchange.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsPlannedprojectchange.getDspForTjdw())) {
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageListForProjectCount = zgsPlannedprojectchangeService.page(page, queryWrapper);
            if (zgsPlannedprojectchange.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (zgsPlannedprojectchange.getIntype().equals("1")) {  // inttype = 1 查询终止项目；   inttype = 2 查询变更项目
                    if (!redisUtil.hasKey("xmzz")) {
                        redisUtil.set("xmzz",pageListForProjectCount.getTotal());
                    }

                } else if (zgsPlannedprojectchange.getIntype().equals("2")) {
                    if (!redisUtil.hasKey("xmbg")) {
                        redisUtil.set("xmbg",pageListForProjectCount.getTotal());
                    }
                }
            }
            if (zgsPlannedprojectchange.getDspForTjdw().equals("3")) {
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                pageListForDsp = zgsPlannedprojectchangeService.page(page, queryWrapper);
                if (zgsPlannedprojectchange.getIntype().equals("1")) {
                    if (!redisUtil.hasKey("xmzzOfTjdw")) {  // for 推荐单位待审批项目
                        redisUtil.set("xmzzOfTjdw", pageListForDsp.getTotal());
                    }
                } else if (zgsPlannedprojectchange.getIntype().equals("2")) {
                    if (!redisUtil.hasKey("xmbgOfTjdw")) {  // for 推荐单位待审批项目
                        redisUtil.set("xmbgOfTjdw", pageListForDsp.getTotal());
                    }
                }
            }

        } else if (StringUtils.isNotBlank(zgsPlannedprojectchange.getDspForTjdw()) && StringUtils.isBlank(zgsPlannedprojectchange.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsPlannedprojectchangeService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsPlannedprojectchange.getDspForTjdw()) && StringUtils.isNotBlank(zgsPlannedprojectchange.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsPlannedprojectchangeService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsPlannedprojectchange.getDspForSt()) && zgsPlannedprojectchange.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsPlannedprojectchangeService.page(page, queryWrapper);

            if (zgsPlannedprojectchange.getIntype().equals("1")) {  // inttype = 1 查询终止项目；   inttype = 2 查询变更项目
                redisUtil.set("xmzzOfDsp",pageList.getTotal());
            } else {
                redisUtil.set("xmbgOfDsp",pageList.getTotal());
            }
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsPlannedprojectchange.getStatus(), sysUser, 1);
            pageList = zgsPlannedprojectchangeService.page(page, queryWrapper);
        }


        /*// 项目变更
        redisUtil.expire("xmbg",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmbg")) {
            if (StringUtils.isNotBlank(zgsPlannedprojectchange.getFlagByWorkTable()) && zgsPlannedprojectchange.getFlagByWorkTable().equals("1")) {
                redisUtil.set("xmbg",pageList.getTotal());
            }
        }
        redisUtil.expire("xmbgOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmbgOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsPlannedprojectchange.getDspForSt()) && zgsPlannedprojectchange.getDspForSt().equals("2")) {
                redisUtil.set("xmbgOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("xmbgOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("xmbgOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsPlannedprojectchange.getDspForTjdw()) && zgsPlannedprojectchange.getDspForTjdw().equals("3")) {
                redisUtil.set("xmbgOfTjdw",pageList.getTotal());
            }
        }*/
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPlannedprojectchange zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsPlannedprojectchange zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    // 将待审核、初审通过、终审通过、形审通过、专家通过的所有项目的退回原因置空
                    if (zgsInfo.getStatus().equals("1") || zgsInfo.getStatus().equals("4") || zgsInfo.getStatus().equals("8") ||
                            zgsInfo.getStatus().equals("2") || zgsInfo.getStatus().equals("10")) {
                        zgsInfo.setAuditopinion("");
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
                    queryWrapper0.eq("sciencetechguid", zgsInfo.getProjectlibraryguid());
                    queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                    queryWrapper0.ne("isdelete", 1);
                    ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper0);
                    if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getPdfUrl())) {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsSciencetechtask.getPdfUrl());
                    }
                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid()) && StringUtils.isNotEmpty(zgsInfo.getProjectname())) {
                        QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<ZgsProjecttask>();
                        queryWrapper1.eq("projectguid", zgsInfo.getProjectlibraryguid());
                        queryWrapper1.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper1.ne("isdelete", 1);
                        ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper1);
                        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                        }
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 审批
     *
     * @param zgsPlannedprojectchange
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-审批")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-审批", notes = "建设科技计划项目变更申请业务主表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsPlannedprojectchange zgsPlannedprojectchange) {
        zgsPlannedprojectchangeService.spProject(zgsPlannedprojectchange);
//        String projectlibraryguid = zgsPlannedprojectchangeService.getById(zgsPlannedprojectchange.getId()).getProjectlibraryguid();
//        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsPlannedprojectchange
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-添加")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-添加", notes = "建设科技计划项目变更申请业务主表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsPlannedprojectchange zgsPlannedprojectchange) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsPlannedprojectchange.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsPlannedprojectchange.getStatus())) {
            //保存并上报
            zgsPlannedprojectchange.setApplydate(new Date());
        } else {
            zgsPlannedprojectchange.setApplydate(null);
        }
        if ("1".equals(zgsPlannedprojectchange.getIntype())) {
            //项目终止并设置终止类型：1申请终止，2省厅终止
            zgsPlannedprojectchange.setClosetype(GlobalConstants.CLOSE_TYPE_1);
        }
        //判断一个项目只允许提交一次变更
        QueryWrapper<ZgsPlannedprojectchange> wrapper = new QueryWrapper<ZgsPlannedprojectchange>();
        wrapper.and(wrapperO -> {
            wrapperO.eq("projectname", zgsPlannedprojectchange.getProjectname()).or().eq("projectlibraryguid", zgsPlannedprojectchange.getProjectlibraryguid());
        });
        wrapper.ne("isdelete", 1);
//        List<ZgsPlannedprojectchange> listOnlyOne = zgsPlannedprojectchangeService.list(wrapper);
//        if (listOnlyOne != null && listOnlyOne.size() > 0) {
//            return Result.error("一个项目目前只允许提交一次变更！");
//        }
        //
        zgsPlannedprojectchange.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsPlannedprojectchange.setCreateaccountname(sysUser.getUsername());
        zgsPlannedprojectchange.setCreateusername(sysUser.getRealname());
        zgsPlannedprojectchange.setCreatedate(new Date());
        zgsPlannedprojectchangeService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedprojectchange, ValidateEncryptEntityUtil.isEncrypt));
//        redisUtil.set(zgsPlannedprojectchange.getProjectlibraryguid(), "变更阶段");
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsPlannedprojectchange.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(id);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(GlobalConstants.PlannedProjectChange);
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                    }
                }
            }
        }

        //建设科技计划项目变更内容记录表
        List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList = zgsPlannedprojectchange.getZgsPlannedprojectchangedetailList();
        if (zgsPlannedprojectchangedetailList != null && zgsPlannedprojectchangedetailList.size() > 0) {
            for (int a0 = 0; a0 < zgsPlannedprojectchangedetailList.size(); a0++) {
                ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail = zgsPlannedprojectchangedetailList.get(a0);
                initPlanned(zgsPlannedprojectchangedetail, zgsPlannedprojectchange);
                zgsPlannedprojectchangedetail.setId(UUID.randomUUID().toString());
                zgsPlannedprojectchangedetail.setBaseguid(id);
                zgsPlannedprojectchangedetail.setCreateaccountname(sysUser.getUsername());
                zgsPlannedprojectchangedetail.setCreateusername(sysUser.getRealname());
                zgsPlannedprojectchangedetail.setCreatedate(new Date());
                zgsPlannedprojectchangedetailService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedprojectchangedetail, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    //0项目名称、1立项编号、2项目承担单位、3任务书编号、4项目责任人、5工作单位、6通信地址、7终止时间、8联系电话、9电子邮箱、10邮编、11项目组成员
    private void initPlanned(ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail, ZgsPlannedprojectchange zgsPlannedprojectchange) {
        ValidateEncryptEntityUtil.validateDecryptObject(zgsPlannedprojectchange, ValidateEncryptEntityUtil.isDecrypt);
        //获取并拼装变更日志
        StringBuilder sb = new StringBuilder();
//        sb.append("本次变更内容，");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //项目终止
        if ("1".equals(zgsPlannedprojectchange.getIntype())) {
            sb.append(zgsPlannedprojectchange.getProjectname());
            sb.append("，");
            sb.append(sdf.format(new Date()));
            sb.append("申请终止。");
        } else {
            //项目变更
            String itemKey = zgsPlannedprojectchangedetail.getItemKey();
            if ("11".equals(itemKey)) {
                //11项目组成员
                sb.append("项目组成员变更：");
                sb.append(zgsPlannedprojectchangedetail.getChangecontent());
            } else {
                if ("0".equals(itemKey)) {
                    //0项目名称
                    sb.append("变更字段：项目名称，变更前内容：" + zgsPlannedprojectchange.getProjectname());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("1".equals(itemKey)) {
                    //1立项编号
                    sb.append("变更字段：立项编号，变更前内容：" + zgsPlannedprojectchange.getSetupnum());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("2".equals(itemKey)) {
                    //2项目承担单位
                    sb.append("变更字段：项目承担单位，变更前内容：" + zgsPlannedprojectchange.getProjectbearunit());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("3".equals(itemKey)) {
                    //3任务书编号
                    sb.append("变更字段：任务书编号，变更前内容：" + zgsPlannedprojectchange.getTasknum());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("4".equals(itemKey)) {
                    //4项目责任人
                    sb.append("变更字段：项目责任人，变更前内容：" + zgsPlannedprojectchange.getProjectleader());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("5".equals(itemKey)) {
                    //5工作单位
                    sb.append("变更字段：工作单位，变更前内容：" + zgsPlannedprojectchange.getWorkunit());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("6".equals(itemKey)) {
                    //6通信地址
                    sb.append("变更字段：通信地址，变更前内容：" + zgsPlannedprojectchange.getAddress());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("7".equals(itemKey)) {
                    //7终止时间
                    sb.append("变更字段：终止时间，变更前内容：" + sdf.format(zgsPlannedprojectchange.getResearchenddate()));
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("8".equals(itemKey)) {
                    //8联系电话
                    sb.append("变更字段：联系电话，变更前内容：" + zgsPlannedprojectchange.getLinkphone());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("9".equals(itemKey)) {
                    //9电子邮箱
                    sb.append("变更字段：电子邮箱，变更前内容：" + zgsPlannedprojectchange.getEmail());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                } else if ("10".equals(itemKey)) {
                    //10邮编
                    sb.append("变更字段：邮编，变更前内容：" + zgsPlannedprojectchange.getZipcode());
                    sb.append(",变更后内容：" + zgsPlannedprojectchangedetail.getChangecontent());
                    sb.append("。");
                }
            }
        }
        zgsPlannedprojectchangedetail.setChangelog(sb.toString());
    }

    /**
     * 编辑
     *
     * @param zgsPlannedprojectchange
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-编辑")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-编辑", notes = "建设科技计划项目变更申请业务主表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsPlannedprojectchange zgsPlannedprojectchange) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsPlannedprojectchange != null && StringUtils.isNotEmpty(zgsPlannedprojectchange.getId())) {
            String id = zgsPlannedprojectchange.getId();
            if (GlobalConstants.handleSubmit.equals(zgsPlannedprojectchange.getStatus())) {
                //保存并上报
                zgsPlannedprojectchange.setApplydate(new Date());
                zgsPlannedprojectchange.setAuditopinion(null);
//                zgsPlannedprojectchange.setReturntype(null);
            } else {
                zgsPlannedprojectchange.setApplydate(null);
            }
            zgsPlannedprojectchange.setModifyaccountname(sysUser.getUsername());
            zgsPlannedprojectchange.setModifyusername(sysUser.getRealname());
            zgsPlannedprojectchange.setModifydate(new Date());
            zgsPlannedprojectchangeService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedprojectchange, ValidateEncryptEntityUtil.isEncrypt));

            //建设科技计划项目变更内容记录表
            List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList0 = zgsPlannedprojectchange.getZgsPlannedprojectchangedetailList();
            QueryWrapper<ZgsPlannedprojectchangedetail> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("baseguid", id);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList0_1 = zgsPlannedprojectchangedetailService.list(queryWrapper1);
            if (zgsPlannedprojectchangedetailList0 != null && zgsPlannedprojectchangedetailList0.size() > 0) {
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsPlannedprojectchangedetailList0.size(); a0++) {
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail = zgsPlannedprojectchangedetailList0.get(a0);
                    initPlanned(zgsPlannedprojectchangedetail, zgsPlannedprojectchange);
                    if (StringUtils.isNotEmpty(zgsPlannedprojectchangedetail.getId())) {
                        //编辑
                        zgsPlannedprojectchangedetail.setModifyaccountname(sysUser.getUsername());
                        zgsPlannedprojectchangedetail.setModifyusername(sysUser.getRealname());
                        zgsPlannedprojectchangedetail.setModifydate(new Date());
                        zgsPlannedprojectchangedetailService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedprojectchangedetail, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsPlannedprojectchangedetail.setId(UUID.randomUUID().toString());
                        zgsPlannedprojectchangedetail.setBaseguid(id);
                        zgsPlannedprojectchangedetail.setCreateaccountname(sysUser.getUsername());
                        zgsPlannedprojectchangedetail.setCreateusername(sysUser.getRealname());
                        zgsPlannedprojectchangedetail.setCreatedate(new Date());
                        zgsPlannedprojectchangedetailService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedprojectchangedetail, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail1 : zgsPlannedprojectchangedetailList0_1) {
                        if (!zgsPlannedprojectchangedetail1.getId().equals(zgsPlannedprojectchangedetail.getId())) {
                            map0.put(zgsPlannedprojectchangedetail1.getId(), zgsPlannedprojectchangedetail1.getId());
                        } else {
                            list0.add(zgsPlannedprojectchangedetail1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsPlannedprojectchangedetailService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsPlannedprojectchangedetailList0_1 != null && zgsPlannedprojectchangedetailList0_1.size() > 0) {
                    for (ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail : zgsPlannedprojectchangedetailList0_1) {
                        zgsPlannedprojectchangedetailService.removeById(zgsPlannedprojectchangedetail.getId());
                    }
                }
            }

            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsPlannedprojectchange.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.PlannedProjectChange);
                                zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-通过id删除")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-通过id删除", notes = "建设科技计划项目变更申请业务主表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsPlannedprojectchangeService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-批量删除")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-批量删除", notes = "建设科技计划项目变更申请业务主表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsPlannedprojectchangeService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-通过id查询")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-通过id查询", notes = "建设科技计划项目变更申请业务主表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsPlannedprojectchange zgsPlannedprojectchange = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsPlannedprojectchange = ValidateEncryptEntityUtil.validateDecryptObject(zgsPlannedprojectchangeService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsPlannedprojectchange == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsPlannedprojectchange);
            }
            if ((zgsPlannedprojectchange.getFirstdate() != null || zgsPlannedprojectchange.getFinishdate() != null) && zgsPlannedprojectchange.getApplydate() != null) {
                zgsPlannedprojectchange.setSpLogStatus(1);
            } else {
                zgsPlannedprojectchange.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsPlannedprojectchange = new ZgsPlannedprojectchange();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.PlannedProjectChange);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsPlannedprojectchange.setZgsMattermaterialList(zgsMattermaterialList);
            zgsPlannedprojectchange.setSpLogStatus(0);
        }
        return Result.OK(zgsPlannedprojectchange);
    }

    private void initProjectSelectById(ZgsPlannedprojectchange zgsPlannedprojectchange) {
        String enterpriseguid = zgsPlannedprojectchange.getEnterpriseguid();
        String buildguid = zgsPlannedprojectchange.getId();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //建设科技计划项目变更内容记录表
            QueryWrapper<ZgsPlannedprojectchangedetail> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList = zgsPlannedprojectchangedetailService.list(queryWrapper1);
            zgsPlannedprojectchange.setZgsPlannedprojectchangedetailList(zgsPlannedprojectchangedetailList);
            //相关附件
            map.put("buildguid", buildguid);
            //后期查下PlannedProjectChange出现在哪张表里
            map.put("projecttypenum", GlobalConstants.PlannedProjectChange);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsPlannedprojectchange.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //项目组成员查询
            if (StringUtils.isNotEmpty(zgsPlannedprojectchange.getProjectlibraryguid())) {
                String projectlibraryguid = zgsPlannedprojectchange.getProjectlibraryguid();
                QueryWrapper<ZgsPlannedchangeparticipant> queryWrapper = new QueryWrapper();
                queryWrapper.eq("taskguid", zgsProjecttaskService.queryByProjectGuid(projectlibraryguid));
                queryWrapper.ne("isdelete", 1);
                queryWrapper.orderByAsc("ordernum");
                List<ZgsPlannedchangeparticipant> list = ValidateEncryptEntityUtil.validateDecryptList(zgsPlannedchangeparticipantService.list(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
                if (list != null && list.size() > 0) {
                    ZgsTaskscienceparticipant zgsTaskscienceparticipant = null;
                    List<ZgsTaskscienceparticipant> zgsTaskparticipantList = new ArrayList<>();
                    for (int i = 0; i < list.size(); i++) {
                        int orderNum = i + 1;
                        ZgsPlannedchangeparticipant zgsScienceparticipant = list.get(i);
                        zgsTaskscienceparticipant = new ZgsTaskscienceparticipant();
                        zgsTaskscienceparticipant.setId(zgsScienceparticipant.getId());
                        zgsTaskscienceparticipant.setTaskguid(zgsScienceparticipant.getTaskguid());
                        zgsTaskscienceparticipant.setOrdernum(new BigDecimal(orderNum));
                        zgsTaskscienceparticipant.setPersonname(zgsScienceparticipant.getPersonname());
                        zgsTaskscienceparticipant.setPersonpost(zgsScienceparticipant.getPersonpost());
                        zgsTaskscienceparticipant.setUnit(zgsScienceparticipant.getUnit());
                        zgsTaskscienceparticipant.setWorkcontent(zgsScienceparticipant.getWorkcontent());
                        zgsTaskscienceparticipant.setSex(zgsScienceparticipant.getSex());
                        zgsTaskscienceparticipant.setProfessional(zgsScienceparticipant.getProfessional());
                        zgsTaskscienceparticipant.setIdcard(zgsScienceparticipant.getIdcard());
                        zgsTaskscienceparticipant.setStandardOfCulture(zgsScienceparticipant.getStandardOfCulture());
                        zgsTaskparticipantList.add(zgsTaskscienceparticipant);
                    }
                    zgsPlannedprojectchange.setZgsTaskparticipantList(zgsTaskparticipantList);
                }
            }
        }
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsPlannedprojectchange
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsPlannedprojectchange zgsPlannedprojectchange,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsPlannedprojectchange, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsPlannedprojectchange> pageList = (IPage<ZgsPlannedprojectchange>) result.getResult();
        List<ZgsPlannedprojectchange> list = pageList.getRecords();
        List<ZgsPlannedprojectchange> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "建设科技计划项目变更申请";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsPlannedprojectchange.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsPlannedprojectchange, ZgsPlannedprojectchange.class, "建设科技计划项目变更申请业务主表");
        return mv;

    }

    private String getId(ZgsPlannedprojectchange item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsPlannedprojectchange.class);
    }


    /**
     * 通过id查询导出word表
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建设科技计划项目变更申请业务主表-通过id查询导出word表")
    @ApiOperation(value = "建设科技计划项目变更申请业务主表-通过id查询导出word表", notes = "建设科技计划项目变更申请业务主表-通过id查询导出word表")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsPlannedprojectchange zgsPlannedprojectchange = ValidateEncryptEntityUtil.validateDecryptObject(zgsPlannedprojectchangeService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsPlannedprojectchange != null) {
            initProjectSelectById(zgsPlannedprojectchange);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "14甘肃省建设科技计划项目变更申请表.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsPlannedprojectchange);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);

        return mv;
    }

}
