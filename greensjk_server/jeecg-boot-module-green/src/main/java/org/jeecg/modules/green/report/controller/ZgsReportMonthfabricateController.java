package org.jeecg.modules.green.report.controller;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.green.common.service.IZgsCommonService;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;

/**
 * @Description: zgs_report_monthfabricate
 * @Author: jeecg-boot
 * @Date: 2022-03-16
 * @Version: V1.0
 */
@Api(tags = "装配式(市州)")
@RestController
@RequestMapping("/report/zgsReportMonthfabricate")
@Slf4j
public class ZgsReportMonthfabricateController extends JeecgController<ZgsReportMonthfabricate, IZgsReportMonthfabricateService> {
    @Autowired
    private IZgsReportMonthfabricateService zgsReportMonthfabricateService;

    @Autowired
    private IZgsReportMonthfabricateDetailService zgsReportMonthfabricateDetailService;

    @Autowired
    private IZgsReportMonthfabricateScdetailService zgsReportMonthfabricateScdetailService;

    @Autowired
    private IZgsReportMonthfabrAreaprojectService zgsReportMonthfabrAreaprojectService;

    @Autowired
    private IZgsReportMonthfabrAreacompanyService zgsReportMonthfabrAreacompanyService;

    @Autowired
    private IZgsCommonService zgsCommonService;


    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabricate
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式(市州)-分页列表查询")
    @ApiOperation(value = "装配式(市州)-分页列表查询", notes = "装配式(市州)-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabricate zgsReportMonthfabricate,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "prossType", required = false) Integer prossType,
                                   @RequestParam(name = "applyState", required = false) String applyState,
                                   HttpServletRequest req) {
//		QueryWrapper<ZgsReportMonthfabricate> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabricate, req.getParameterMap());
//		Page<ZgsReportMonthfabricate> page = new Page<ZgsReportMonthfabricate>(pageNo, pageSize);
//		IPage<ZgsReportMonthfabricate> pageList = zgsReportMonthfabricateService.page(page, queryWrapper);
//		return Result.OK(pageList);

        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabricate> qW = new QueryWrapper<>();
        if (zgsReportMonthfabricate != null) {
            if (zgsReportMonthfabricate.getApplystate() != null) {
                //有问题，BigDecimal applystate 需要判空？
                BigDecimal applystate = zgsReportMonthfabricate.getApplystate();
                qW.eq("applystate", applystate);
            }
            Integer userType = sysUser.getLoginUserType();
            //6县区，7市区
            if(userType==7){
                qW.eq("areacode",sysUser.getAreacode());
            }
            if (StringUtils.isNotEmpty(zgsReportMonthfabricate.getReporttm())) {
                String reporttm = zgsReportMonthfabricate.getReporttm();
                qW.eq("reporttm", reporttm);
            }
        }
        //  参数列表
        List list = new ArrayList();
        String[] splitArray = null;
        if(applyState != null){
          splitArray = applyState.split(",");
        }
        if (splitArray != null){
          if (splitArray[0] != "" && splitArray[0] != null){
            for (int i = 0;i < splitArray.length;i++){
              list.add(splitArray[i]);
            }
            qW.in("applystate", list);
          }
        }
        qW.ne("isdelete", 1);
        qW.orderByDesc("createtime");

//		QueryWrapper<ZgsReportMonthfabricate> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabricate, req.getParameterMap());
        Page<ZgsReportMonthfabricate> page = new Page<ZgsReportMonthfabricate>(pageNo, pageSize);
        IPage<ZgsReportMonthfabricate> pageList = zgsReportMonthfabricateService.page(page, qW);
        //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行

      for (int i = 0;i < pageList.getRecords().size();i++) {
        ZgsReportMonthfabricateDetail currentDetail = findAreaDetail(pageList.getRecords().get(i), 1);
        if (currentDetail != null) {
          pageList.getRecords().get(i).setDt1(currentDetail);
        }
        ZgsReportMonthfabricateDetail currentDetail2 = findAreaDetail(pageList.getRecords().get(i), 2);
        if (currentDetail2 != null) {
          pageList.getRecords().get(i).setDt2(currentDetail2);
        }
        //  查询产能
        QueryWrapper<ZgsReportMonthfabricateScdetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", pageList.getRecords().get(i).getId());
        qWdetail.ne("isdelete", 1);
        List<ZgsReportMonthfabricateScdetail> scdetail = zgsReportMonthfabricateScdetailService.list(qWdetail);
        if (scdetail.size() > 0) {
          pageList.getRecords().get(i).setScdetail(scdetail.get(0));
        }
      }
      return Result.OK(pageList);
    }




  //	================== 项目库 申报   begin ==================================================================================================0
  //	================== 项目库 申报   begin ==================================================================================================
  /**
   * 分页列表查询
   *
   * @param zgsReportMonthfabricate
   * @param pageNo
   * @param pageSize
   * @param req
   * @return
   */
  @AutoLog(value = "装配式(市州)-项目库-分页")
  @ApiOperation(value = "装配式(市州)-项目库-分页", notes = "装配式(市州)-项目库-分页")
  @GetMapping(value = "/projectPageList")
  public Result<?> queryPageProjectList(ZgsReportMonthfabricate zgsReportMonthfabricate,
                                        @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                        @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                        @RequestParam(name = "prossType", required = false) Integer prossType,
                                        @RequestParam(name = "applyState", required = false) String applyState,
                                        HttpServletRequest req) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    QueryWrapper<ZgsReportMonthfabricate> qW = new QueryWrapper<>();
    if (zgsReportMonthfabricate != null) {
      if (zgsReportMonthfabricate.getApplystate() != null) {
        //有问题，BigDecimal applystate 需要判空？
        BigDecimal applystate = zgsReportMonthfabricate.getApplystate();
        qW.eq("applystate", applystate);
      }
      Integer userType = sysUser.getLoginUserType();
      //6县区，7市区
      if(userType==7){
        qW.eq("areacode",sysUser.getAreacode());
      }
      if (StringUtils.isNotEmpty(zgsReportMonthfabricate.getReporttm())) {
        String reporttm = zgsReportMonthfabricate.getReporttm();
        qW.eq("reporttm", reporttm);
      }
    }
    //  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
    if(applyState != null){
      splitArray = applyState.split(",");
    }
    if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
    qW.ne("isdelete", 1);
    qW.orderByDesc("createtime");
    Page<ZgsReportMonthfabricate> page = new Page<ZgsReportMonthfabricate>(pageNo, pageSize);
    IPage<ZgsReportMonthfabricate> pageList = zgsReportMonthfabricateService.page(page, qW);
    for (int i = 0;i < pageList.getRecords().size();i++) {
      ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(pageList.getRecords().get(i), 1);
      if (currentProjectDetail != null) {
        pageList.getRecords().get(i).setDtproject1(currentProjectDetail);
      }
      ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(pageList.getRecords().get(i), 2);
      if (currentProjectDetail2 != null) {
        pageList.getRecords().get(i).setDtproject2(currentProjectDetail2);
      }
      //  查询产能
      QueryWrapper<ZgsReportMonthfabrAreacompany> qWdetail = new QueryWrapper<>();
      qWdetail.eq("monthmianguid", pageList.getRecords().get(i).getId());
      qWdetail.ne("isdelete", 1);
      List<ZgsReportMonthfabrAreacompany> scdetail = zgsReportMonthfabrAreacompanyService.list(qWdetail);
      if (scdetail.size() > 0) {
        pageList.getRecords().get(i).setDtcompany(scdetail.get(0));
      }
    }
    return Result.OK(pageList);
  }


  /**
   * 通过id查询
   *
   * @param id
   * @return
   */
  @AutoLog(value = "装配式(市州)-项目库-单位库-通过id查询")
  @ApiOperation(value = "装配式(市州)-项目库-单位库-通过id查询", notes = "装配式(市州)-项目库-单位库-通过id查询")
  @GetMapping(value = "/queryProjectById")
  public Result<?> queryProjectById(@RequestParam(name = "id", required = true) String id) {
    ZgsReportMonthfabricate zgsReportMonthfabricate = zgsReportMonthfabricateService.getById(id);
    if (zgsReportMonthfabricate == null) {
      return Result.error("未找到对应数据");
    } else {
      ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(zgsReportMonthfabricate, 1);
      if (currentProjectDetail != null) {
        zgsReportMonthfabricate.setDtproject1(currentProjectDetail);
      }
      ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(zgsReportMonthfabricate, 2);
      if (currentProjectDetail2 != null) {
        zgsReportMonthfabricate.setDtproject2(currentProjectDetail2);
      }
      //  查询产能
      QueryWrapper<ZgsReportMonthfabrAreacompany> qWdetail = new QueryWrapper<>();
      qWdetail.eq("monthmianguid", zgsReportMonthfabricate.getId());
      qWdetail.ne("isdelete", 1);
      List<ZgsReportMonthfabrAreacompany> scdetail = zgsReportMonthfabrAreacompanyService.list(qWdetail);
      if (scdetail.size() > 0) {
        zgsReportMonthfabricate.setDtcompany(scdetail.get(0));
      }
    }
    return Result.OK(zgsReportMonthfabricate);
  }


  /**
   * 数据汇总
   *
   * @param zgsReportMonthfabricateArea
   * @param pageNo
   * @param pageSize
   * @param req
   * @return sysCategoryService queryCategoryByCode
   */
  @AutoLog(value = "装配式(市州)-项目库")
  @ApiOperation(value = "装配式(市州)-项目库", notes = "装配式(市州)-项目库")
  @GetMapping(value = "/projectList")
  public Result<?> queryProjectList(ZgsReportMonthfabricate zgsReportMonthfabricateArea,
                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                    @RequestParam(name = "prossType", required = false) Integer prossType,
                                    @RequestParam(name = "applyState", required = false) String applyState,
                                    HttpServletRequest req) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    QueryWrapper<ZgsReportMonthfabricate> qW = new QueryWrapper<>();
    if (zgsReportMonthfabricateArea != null) {
      if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
        String reporttm = zgsReportMonthfabricateArea.getReporttm();
        qW.eq("reporttm", reporttm);
      }
    }

    qW.ne("isdelete", 1);
    qW.orderByDesc("createtime");

    //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
    //  1、查询本月所有 地区的主表信息；2、根据主表信息，查询出项目库中的信息，3、将所有项目库中的信息进行累计求和；4、计算所有从1月份到本月的所有数据和

    QueryWrapper<ZgsReportMonthfabrAreaproject> qWdetail = new QueryWrapper<>();
    if (zgsReportMonthfabricateArea != null) {
      if (StringUtils.isNotEmpty(zgsReportMonthfabricateArea.getReporttm())) {
        String reporttm = zgsReportMonthfabricateArea.getReporttm();
        qWdetail.eq("reporttm", reporttm);
      }
    }
    qWdetail.eq("projecttype", 1);
    List<ZgsReportMonthfabrAreaproject> areaprojectsList = zgsReportMonthfabrAreaprojectService.list(qWdetail);

//    Map<String, List<ZgsReportMonthfabrAreaproject>> collect = areaprojectsList.stream().collect(
//          Collectors.groupingBy(ZgsReportMonthfabrAreaproject::getAreacode));
//    for(String key:collect.keySet()){
//        System.out.println(key+":" +collect.get(key).size());
//        System.out.println(collect.get(key));
//    }

    if (areaprojectsList.size() > 0) {
      for (int i = 0;i < areaprojectsList.size();i++) {
        if (i > 0){
          if (areaprojectsList.get(i).getJngzjzmj() != null && areaprojectsList.get(i-1).getJngzjzmj() != null){
            areaprojectsList.get(i).setJngzjzmj(areaprojectsList.get(i).getJngzjzmj().add(areaprojectsList.get(i-1).getJngzjzmj()));
          }
          if (areaprojectsList.get(i).getQzxzzmj() != null  && areaprojectsList.get(i-1).getQzxzzmj() != null){
            areaprojectsList.get(i).setQzxzzmj(areaprojectsList.get(i).getQzxzzmj().add(areaprojectsList.get(i-1).getQzxzzmj()));
          }
          if (areaprojectsList.get(i).getZpszxzzjzmj() != null  && areaprojectsList.get(i-1).getZpszxzzjzmj() != null){
            areaprojectsList.get(i).setZpszxzzjzmj(areaprojectsList.get(i).getZpszxzzjzmj().add(areaprojectsList.get(i-1).getZpszxzzjzmj()));
          }
        }
        if (areaprojectsList.get(i).getAreacode() != null){
          String name = zgsCommonService.queryAreaTextByCode(areaprojectsList.get(i).getAreacode()); //  区域名
          areaprojectsList.get(i).setAreacode(name);
        }
      }
    }


//    Page<ZgsReportMonthfabricateArea> page = new Page<ZgsReportMonthfabricateArea>(pageNo, pageSize);
//    IPage<ZgsReportMonthfabricateArea> pageList = zgsReportMonthfabricateAreaService.page(page, qW);
//    for (int i = 0;i < pageList.getRecords().size();i++) {
//            ZgsReportMonthfabrAreaproject currentProjectDetail = findAreaProjectDetail(pageList.getRecords().get(i), 1);
//            if (currentProjectDetail != null) {
//              pageList.getRecords().get(i).setDtproject1(currentProjectDetail);
//              ZgsReportMonthfabrAreaproject temporaryProject1 = pageList.getRecords().get(i).getDtproject1();
//              pageList.getRecords().get(i).getDtproject1().setJngzjzmj(temporaryProject1.getJngzjzmj().add(currentProjectDetail.getJngzjzmj()));
//              pageList.getRecords().get(i).getDtproject1().setQzxzzmj(temporaryProject1.getQzxzzmj().add(currentProjectDetail.getQzxzzmj()));
//              pageList.getRecords().get(i).getDtproject1().setZpszxzzjzmj(temporaryProject1.getZpszxzzjzmj().add(currentProjectDetail.getZpszxzzjzmj()));
//            }
//            ZgsReportMonthfabrAreaproject currentProjectDetail2 = findAreaProjectDetail(pageList.getRecords().get(i), 2);
//            if (currentProjectDetail2 != null) {
//              pageList.getRecords().get(i).setDtproject2(currentProjectDetail2);
//              ZgsReportMonthfabrAreaproject temporaryProject2 = pageList.getRecords().get(i).getDtproject2();
//              pageList.getRecords().get(i).getDtproject2().setJngzjzmj(temporaryProject2.getJngzjzmj().add(currentProjectDetail.getJngzjzmj()));
//              pageList.getRecords().get(i).getDtproject2().setQzxzzmj(temporaryProject2.getQzxzzmj().add(currentProjectDetail.getQzxzzmj()));
//              pageList.getRecords().get(i).getDtproject2().setZpszxzzjzmj(temporaryProject2.getZpszxzzjzmj().add(currentProjectDetail.getZpszxzzjzmj()));
//            }
//
//    }
    return Result.OK(areaprojectsList);
  }


  //查找明细数据
  public ZgsReportMonthfabrAreaproject findAreaProjectDetail(ZgsReportMonthfabricate zgsReportMonthfabricateArea, Integer projecttype) {
    QueryWrapper<ZgsReportMonthfabrAreaproject> qWdetail = new QueryWrapper<>();
    qWdetail.eq("monthfabrareadetailguid", zgsReportMonthfabricateArea.getId());
    qWdetail.eq("projecttype", projecttype);
    List<ZgsReportMonthfabrAreaproject> areadetail = zgsReportMonthfabrAreaprojectService.list(qWdetail);
    if (areadetail.size() > 0){
      return areadetail.get(0);
    }
    return null;
  }



  /**
   * 添加
   *
   * @param zgsReportMonthfabricateArea
   * @return
   */
  @AutoLog(value = "装配式(市州)-项目库-添加")
  @ApiOperation(value = "装配式(市州)-项目库-添加", notes = "装配式(市州)-项目库-添加")
  @PostMapping(value = "/addProject")
  public Result<?> addProject(@RequestBody ZgsReportMonthfabricate zgsReportMonthfabricateArea) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    //生成主表ID
    String id = UUID.randomUUID().toString();
    zgsReportMonthfabricateArea.setId(id);
    //申报状态  0未上报
    zgsReportMonthfabricateArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
    //行政区域
    zgsReportMonthfabricateArea.setAreacode(sysUser.getAreacode());
    zgsReportMonthfabricateArea.setAreaname(sysUser.getAreaname());
    zgsReportMonthfabricateArea.setIsdelete(new BigDecimal(0));
    zgsReportMonthfabricateArea.setCreatepersonaccount(sysUser.getUsername());
    zgsReportMonthfabricateArea.setCreatepersonname(sysUser.getRealname());
    zgsReportMonthfabricateArea.setCreatetime(new Date());
    zgsReportMonthfabricateService.save(zgsReportMonthfabricateArea);
    String prosstype = "0";
    //保存各种状态下的明细数据
    //1:开工建设2:已竣工验收
    ZgsReportMonthfabrAreaproject dtproject1 = zgsReportMonthfabricateArea.getDtproject1();
    if(dtproject1!=null){
      prosstype = "1";
      dtproject1.setReporttm(zgsReportMonthfabricateArea.getReporttm());
      saveAreaProjectDetail(dtproject1,sysUser,id,prosstype);
    }
    ZgsReportMonthfabrAreaproject dtproject2 = zgsReportMonthfabricateArea.getDtproject2();
    if(dtproject2!=null){
      prosstype = "2";
      dtproject1.setReporttm(zgsReportMonthfabricateArea.getReporttm());
      saveAreaProjectDetail(dtproject2,sysUser,id,prosstype);
    }
    return Result.OK("添加成功！");
  }


  /**
   * 添加
   *
   * @param zgsReportMonthfabricateArea
   * @return
   */
  @AutoLog(value = "装配式(市州)-单位库-添加")
  @ApiOperation(value = "装配式(市州)-单位库-添加", notes = "装配式(市州)-单位库-添加")
  @PostMapping(value = "/addCompany")
  public Result<?> addCompany(@RequestBody ZgsReportMonthfabricate zgsReportMonthfabricateArea) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    //生成主表ID
    String id = UUID.randomUUID().toString();
    zgsReportMonthfabricateArea.setId(id);
    //申报状态  0未上报
    zgsReportMonthfabricateArea.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
    //行政区域
    zgsReportMonthfabricateArea.setAreacode(sysUser.getAreacode());
    zgsReportMonthfabricateArea.setAreaname(sysUser.getAreaname());
    zgsReportMonthfabricateArea.setIsdelete(new BigDecimal(0));
    zgsReportMonthfabricateArea.setCreatepersonaccount(sysUser.getUsername());
    zgsReportMonthfabricateArea.setCreatepersonname(sysUser.getRealname());
    zgsReportMonthfabricateArea.setCreatetime(new Date());
    zgsReportMonthfabricateService.save(zgsReportMonthfabricateArea);
    String prosstype = "0";
    ZgsReportMonthfabrAreacompany dtproject1 = zgsReportMonthfabricateArea.getDtcompany();
    if(dtproject1!=null){
      saveAreaCompanyDetail(dtproject1,sysUser,id,prosstype);
    }
    return Result.OK("添加成功！");
  }


  //保存 项目库 明细
  public void saveAreaProjectDetail(ZgsReportMonthfabrAreaproject areaDetail,LoginUser sysUser,String id, String prosstype){
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthfabrareadetailguid(id);
    /**1:开工建设2:已竣工验收*/
    areaDetail.setProjecttype(prosstype);
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabrAreaprojectService.save(areaDetail);
  }

  //保存 项目库（生产节能） 明细
  public void saveAreaCompanyDetail(ZgsReportMonthfabrAreacompany areaDetail,LoginUser sysUser,String id, String prosstype){
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthmianguid(id);
    areaDetail.setCountyorcity("2");    //  （1、区县；2、市州）
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabrAreacompanyService.save(areaDetail);
  }


  /**
   * 编辑
   *
   * @param zgsReportMonthfabricate
   * @return
   */
  @AutoLog(value = "装配式(市州)-编辑")
  @ApiOperation(value = "装配式(市州)-编辑", notes = "装配式(市州)-编辑")
  @PutMapping(value = "/editProject")
  public Result<?> editProject(@RequestBody ZgsReportMonthfabricate zgsReportMonthfabricate) {
    LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
    if (zgsReportMonthfabricate != null && StringUtils.isNotEmpty(zgsReportMonthfabricate.getId())) {
      String id = zgsReportMonthfabricate.getId();
      if (GlobalConstants.apply_state1.equals(zgsReportMonthfabricate.getApplystate())) {
        //保存并上报
        zgsReportMonthfabricate.setApplydate(new Date());
      }
      if (GlobalConstants.apply_state3.equals(zgsReportMonthfabricate.getApplystate())) {
        //退回
        zgsReportMonthfabricate.setBackrdate(new Date());
      }
      zgsReportMonthfabricate.setModifypersonaccount(sysUser.getUsername());
      zgsReportMonthfabricate.setModifypersonname(sysUser.getRealname());
      zgsReportMonthfabricate.setModifytime(new Date());
      zgsReportMonthfabricateService.updateById(zgsReportMonthfabricate);

      //更新明细表数据
      ZgsReportMonthfabricateDetail dt1 = zgsReportMonthfabricate.getDt1();
      ZgsReportMonthfabricateDetail dt2 = zgsReportMonthfabricate.getDt2();

      ZgsReportMonthfabrAreaproject project1 = zgsReportMonthfabricate.getDtproject1();
      ZgsReportMonthfabrAreaproject project2 = zgsReportMonthfabricate.getDtproject2();

      if (project1 != null) {
        updateProjectDetail(project1, sysUser, id);
      } else {
        deleteDetail(zgsReportMonthfabricate, GlobalConstants.prosstype1);
      }
      if (project2 != null) {
        updateProjectDetail(project2, sysUser, id);
      } else {
        deleteDetail(zgsReportMonthfabricate, GlobalConstants.prosstype1);
      }
      ZgsReportMonthfabrAreacompany scdetail = zgsReportMonthfabricate.getDtcompany();
      if (scdetail != null) {
        updateCompanyDetail(scdetail, sysUser, id);
      } else {
        deleteScDetail(zgsReportMonthfabricate, GlobalConstants.prosstype1);
      }
    }
    return Result.OK("编辑成功!");
  }

  public void updateProjectDetail(ZgsReportMonthfabrAreaproject areaDetail, LoginUser sysUser, String id) {
    if (StringUtils.isNotEmpty(areaDetail.getId())) {
        //编辑
        zgsReportMonthfabrAreaprojectService.updateById(areaDetail);
    } else {
        //新增
        String prosstype = "2";
        saveAreaProjectDetail(areaDetail,sysUser,id,prosstype);
    }
  }

  public void updateCompanyDetail(ZgsReportMonthfabrAreacompany areaDetail, LoginUser sysUser, String id) {
    if (StringUtils.isNotEmpty(areaDetail.getId())) {
      //编辑
      zgsReportMonthfabrAreacompanyService.updateById(areaDetail);
    } else {
      //新增
      String prosstype = "2";
      saveAreaCompanyDetail(areaDetail,sysUser,id,prosstype);
    }
  }
//	================== 项目库 申报   end ==================================================================================================

  //	================== 项目库 申报   end ==================================================================================================





  /**
     * 添加
     *
     * @param zgsReportMonthfabricate
     * @return
     */
    @AutoLog(value = "装配式(市州)-添加")
    @ApiOperation(value = "装配式(市州)-添加", notes = "装配式(市州)-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabricate zgsReportMonthfabricate) {
//		zgsReportMonthfabricateService.save(zgsReportMonthfabricate);
//		return Result.OK("添加成功！");

        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //生成主表ID
        String id = UUID.randomUUID().toString();
        zgsReportMonthfabricate.setId(id);
        //申报状态  0未上报
        zgsReportMonthfabricate.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
        //行政区域
        zgsReportMonthfabricate.setAreacode(sysUser.getAreacode());
        zgsReportMonthfabricate.setAreaname(sysUser.getAreaname());
        zgsReportMonthfabricate.setIsdelete(new BigDecimal(0));
        zgsReportMonthfabricate.setCreatepersonaccount(sysUser.getUsername());
        zgsReportMonthfabricate.setCreatepersonname(sysUser.getRealname());
        zgsReportMonthfabricate.setCreatetime(new Date());
        zgsReportMonthfabricateService.save(zgsReportMonthfabricate);
        ZgsReportMonthfabricateDetail dt1 = zgsReportMonthfabricate.getDt1();
        if (dt1 != null) {
            saveAreaDetail(dt1, sysUser, id, "1");
        }
        ZgsReportMonthfabricateDetail dt2 = zgsReportMonthfabricate.getDt2();
        if (dt2 != null) {
            saveAreaDetail(dt2, sysUser, id, "2");
        }
        //保存产能
        ZgsReportMonthfabricateScdetail scdetail = zgsReportMonthfabricate.getScdetail();
        if (scdetail != null) {
            saveScDetail(scdetail, sysUser, id);
        }
      return Result.OK("添加成功！");
    }


  public void saveAreaDetail(ZgsReportMonthfabricateDetail areaDetail, LoginUser sysUser, String id, String prosstype) {
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthmianguid(id);
    areaDetail.setIsdelete(new BigDecimal(0));
    areaDetail.setProsstype(new BigDecimal(prosstype));
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabricateDetailService.save(areaDetail);
  }

  public void saveAreaDetail(ZgsReportMonthfabricateDetail areaDetail, LoginUser sysUser, String id) {
    //生成明细表ID
    String id2 = UUID.randomUUID().toString();
    areaDetail.setId(id2);
    //关联主表ID
    areaDetail.setMonthmianguid(id);
    areaDetail.setIsdelete(new BigDecimal(0));
    areaDetail.setProsstype(new BigDecimal(0));
    areaDetail.setCreatepersonaccount(sysUser.getUsername());
    areaDetail.setCreatepersonname(sysUser.getRealname());
    areaDetail.setCreatetime(new Date());
    zgsReportMonthfabricateDetailService.save(areaDetail);

  }

    //保存产能明细
    public void saveScDetail(ZgsReportMonthfabricateScdetail scdetail, LoginUser sysUser, String id) {
        //生成明细表ID
        String id2 = UUID.randomUUID().toString();
        scdetail.setId(id2);
        //关联主表ID
        scdetail.setMonthmianguid(id);
        scdetail.setIsdelete(new BigDecimal(0));
        scdetail.setCreatepersonaccount(sysUser.getUsername());
        scdetail.setCreatepersonname(sysUser.getRealname());
        scdetail.setCreatetime(new Date());
        zgsReportMonthfabricateScdetailService.save(scdetail);
    }


    /**
     * 编辑
     *
     * @param zgsReportMonthfabricate
     * @return
     */
    @AutoLog(value = "装配式(市州)-编辑")
    @ApiOperation(value = "装配式(市州)-编辑", notes = "装配式(市州)-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabricate zgsReportMonthfabricate) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsReportMonthfabricate != null && StringUtils.isNotEmpty(zgsReportMonthfabricate.getId())) {
            String id = zgsReportMonthfabricate.getId();
            if (GlobalConstants.apply_state1.equals(zgsReportMonthfabricate.getApplystate())) {
                //保存并上报
                zgsReportMonthfabricate.setApplydate(new Date());
            }
            if (GlobalConstants.apply_state3.equals(zgsReportMonthfabricate.getApplystate())) {
                //退回
                zgsReportMonthfabricate.setBackrdate(new Date());
            }
            zgsReportMonthfabricate.setModifypersonaccount(sysUser.getUsername());
            zgsReportMonthfabricate.setModifypersonname(sysUser.getRealname());
            zgsReportMonthfabricate.setModifytime(new Date());
            zgsReportMonthfabricateService.updateById(zgsReportMonthfabricate);

            //更新明细表数据
            ZgsReportMonthfabricateDetail dt1 = zgsReportMonthfabricate.getDt1();
            ZgsReportMonthfabricateDetail dt2 = zgsReportMonthfabricate.getDt2();

            if (dt1 != null) {
                updateDetail(dt1, sysUser, id);
            } else {
                deleteDetail(zgsReportMonthfabricate, GlobalConstants.prosstype1);
            }
            if (dt2 != null) {
                updateDetail(dt2, sysUser, id);
            } else {
                deleteDetail(zgsReportMonthfabricate, GlobalConstants.prosstype2);
            }
            ZgsReportMonthfabricateScdetail scdetail = zgsReportMonthfabricate.getScdetail();
            if (scdetail != null) {
                updateScDetail(scdetail, sysUser, id);
            } else {
                deleteScDetail(zgsReportMonthfabricate, GlobalConstants.prosstype1);
            }
        }
        return Result.OK("编辑成功!");
    }

    public void updateDetail(ZgsReportMonthfabricateDetail areaDetail, LoginUser sysUser, String id) {
        if (StringUtils.isNotEmpty(areaDetail.getId())) {
            //编辑
            areaDetail.setModifypersonaccount(sysUser.getUsername());
            areaDetail.setModifypersonname(sysUser.getRealname());
            areaDetail.setModifytime(new Date());
            zgsReportMonthfabricateDetailService.updateById(areaDetail);
        } else {
            //新增
            saveAreaDetail(areaDetail, sysUser, id);
        }
    }

    public void updateScDetail(ZgsReportMonthfabricateScdetail scdetail, LoginUser sysUser, String id) {
        if (StringUtils.isNotEmpty(scdetail.getId())) {
            //编辑
            scdetail.setModifypersonaccount(sysUser.getUsername());
            scdetail.setModifypersonname(sysUser.getRealname());
            scdetail.setModifytime(new Date());
            zgsReportMonthfabricateScdetailService.updateById(scdetail);
        } else {
            //新增
            saveScDetail(scdetail, sysUser, id);
        }
    }

    public void deleteDetail(ZgsReportMonthfabricate areaDetail, Integer prosstype) {
        QueryWrapper<ZgsReportMonthfabricateDetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", areaDetail.getId());
        qWdetail.ne("isdelete", 1);
        qWdetail.eq("prosstype", prosstype);
        zgsReportMonthfabricateDetailService.remove(qWdetail);
    }

    public void deleteScDetail(ZgsReportMonthfabricate scdetail, Integer prosstype) {
        QueryWrapper<ZgsReportMonthfabricateScdetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", scdetail.getId());
        qWdetail.ne("isdelete", 1);
        zgsReportMonthfabricateScdetailService.remove(qWdetail);
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式(市州)-通过id删除")
    @ApiOperation(value = "装配式(市州)-通过id删除", notes = "装配式(市州)-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportMonthfabricateService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式(市州)-批量删除")
    @ApiOperation(value = "装配式(市州)-批量删除", notes = "装配式(市州)-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabricateService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式(市州)-通过id查询")
    @ApiOperation(value = "装配式(市州)-通过id查询", notes = "装配式(市州)-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabricate zgsReportMonthfabricate = zgsReportMonthfabricateService.getById(id);
        if (zgsReportMonthfabricate == null) {
            return Result.error("未找到对应数据");
        } else {
          ZgsReportMonthfabricateDetail currentDetail = findAreaDetail(zgsReportMonthfabricate, 1);
          if (currentDetail != null) {
            zgsReportMonthfabricate.setDt1(currentDetail);
          }
          ZgsReportMonthfabricateDetail currentDetail2 = findAreaDetail(zgsReportMonthfabricate, 2);
          if (currentDetail2 != null) {
            zgsReportMonthfabricate.setDt2(currentDetail2);
          }
          //  查询产能
          QueryWrapper<ZgsReportMonthfabricateScdetail> qWdetail = new QueryWrapper<>();
          qWdetail.eq("monthmianguid", zgsReportMonthfabricate.getId());
          qWdetail.ne("isdelete", 1);
          List<ZgsReportMonthfabricateScdetail> scdetail = zgsReportMonthfabricateScdetailService.list(qWdetail);
          if (scdetail.size() > 0) {
            zgsReportMonthfabricate.setScdetail(scdetail.get(0));
          }

        }
        return Result.OK(zgsReportMonthfabricate);
    }


    //查找明细数据
    public ZgsReportMonthfabricateDetail findAreaDetail(ZgsReportMonthfabricate zgsReportMonthfabricate, Integer prosstype) {
        QueryWrapper<ZgsReportMonthfabricateDetail> qWdetail = new QueryWrapper<>();
        qWdetail.eq("monthmianguid", zgsReportMonthfabricate.getId());
        qWdetail.ne("isdelete", 1);
        qWdetail.eq("prosstype", prosstype);
        List<ZgsReportMonthfabricateDetail> areadetail = zgsReportMonthfabricateDetailService.list(qWdetail);
        if (areadetail.size() > 0){
          return areadetail.get(0);
        }
        return null;
    }


    /**
     * 导出excel
     *
     * @param request
     * @param zgsReportMonthfabricate
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabricate zgsReportMonthfabricate) {
        return super.exportXls(request, zgsReportMonthfabricate, ZgsReportMonthfabricate.class, "zgs_report_monthfabricate");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabricate.class);
    }

    /**
     * 大屏装配式建筑查询
     *
     * @param zgsReportMonthfabricate
     * @param req
     * @return
     */
    @AutoLog(value = "大屏装配式建筑查询")
    @ApiOperation(value = "大屏装配式建筑查询", notes = "大屏装配式建筑查询")
    @GetMapping(value = "/screanQueryList")
    public Result<?> screanQueryList(ZgsReportMonthfabricate zgsReportMonthfabricate,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabricate> qW = new QueryWrapper<>();
        String reporttm = DateUtils.formatDate(new Date(),"YYYY-MM");
        //默认不传参数，如果需要传参数可以通过zgsReportMonthfabricate来实现。
        if(zgsReportMonthfabricate!=null){
            if(zgsReportMonthfabricate.getReporttm()!=null){
                reporttm = zgsReportMonthfabricate.getReporttm();
            }
            if(zgsReportMonthfabricate.getAreacode()!=null){
                qW.eq("areacode",zgsReportMonthfabricate.getAreacode());
            }
        }
        qW.eq("reporttm", reporttm);
        qW.ne("isdelete", 1);
        qW.orderByDesc("applydate");

        List<ZgsReportMonthfabricate> fabricateList = zgsReportMonthfabricateService.list(qW);
        //根据prossType查询明细数据。 request.getParameter("prossType");点击菜单时查询，菜单查询结果：0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3竣工后运行
        for (ZgsReportMonthfabricate b : fabricateList) {
            //查询未开工
            ZgsReportMonthfabricateDetail dt1 = findAreaDetail(b, GlobalConstants.prosstype1);
            if (dt1 != null) {
                zgsReportMonthfabricate.setDt1(dt1);
            }
            //查询已竣工
            ZgsReportMonthfabricateDetail dt2 = findAreaDetail(b, GlobalConstants.prosstype2);
            if (dt2 != null) {
                zgsReportMonthfabricate.setDt2(dt2);
            }
            //查询产能情况
            QueryWrapper<ZgsReportMonthfabricateScdetail> qWdetail = new QueryWrapper<>();
            qWdetail.eq("monthmianguid", zgsReportMonthfabricate.getId());
            qWdetail.ne("isdelete", 1);
            ZgsReportMonthfabricateScdetail scdetail = zgsReportMonthfabricateScdetailService.getOne(qWdetail);
            if (scdetail != null) {
                zgsReportMonthfabricate.setScdetail(scdetail);
            }

        }

        return Result.OK(fabricateList);
    }


}
