package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsThreepersonfinal;
import org.jeecg.modules.green.common.mapper.ZgsThreepersonfinalMapper;
import org.jeecg.modules.green.common.service.IZgsThreepersonfinalService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 三类人员实时信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
@Service
public class ZgsThreepersonfinalServiceImpl extends ServiceImpl<ZgsThreepersonfinalMapper, ZgsThreepersonfinal> implements IZgsThreepersonfinalService {

}
