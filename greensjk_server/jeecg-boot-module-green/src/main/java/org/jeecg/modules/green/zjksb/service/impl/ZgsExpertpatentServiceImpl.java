package org.jeecg.modules.green.zjksb.service.impl;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertpatent;
import org.jeecg.modules.green.zjksb.mapper.ZgsExpertpatentMapper;
import org.jeecg.modules.green.zjksb.service.IZgsExpertpatentService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 专家库专家专利信息
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
@Service
public class ZgsExpertpatentServiceImpl extends ServiceImpl<ZgsExpertpatentMapper, ZgsExpertpatent> implements IZgsExpertpatentService {

}
