package org.jeecg.modules.green.sms.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.RedisUtil;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/10/2012:17
 */
public interface SMSTokenUtilService {

    String getTokenForSMS(String client_id, String client_secret);

    public Result<?> sendSMSService(String smsParams);

    public Result<?> sendSMSBatchService(String smsParams);

    public Result<?> getSMSVerificationCode(@RequestBody String smsParams);

    public Result<?> getSMSVerificationCodePost(@RequestBody String smsParams);
}
