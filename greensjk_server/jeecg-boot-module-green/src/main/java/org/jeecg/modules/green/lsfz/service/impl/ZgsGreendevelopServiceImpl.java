package org.jeecg.modules.green.lsfz.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelop;
import org.jeecg.modules.green.lsfz.mapper.ZgsGreendevelopMapper;
import org.jeecg.modules.green.lsfz.service.ZgsGreendevelopService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @describe: 绿色发展项目
 * @author: renxiaoliang
 * @date: 2023/12/13 14:31
 */
@Service
public class ZgsGreendevelopServiceImpl extends ServiceImpl<ZgsGreendevelopMapper, ZgsGreendevelop> implements ZgsGreendevelopService {

    @Override
    public Page<ZgsGreendevelop> greenDevelopList(Wrapper<ZgsGreendevelop> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsGreendevelop> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsGreendevelop> listInfoPage = this.baseMapper.greenDevelopList(pageMode, queryWrapper);
        List<ZgsGreendevelop> listInfo = listInfoPage.getRecords();
        /*if (listInfo.size() > 0) {
            for (ZgsProjectlibraryListInfo entity:listInfo
                 ) {
                String bid = entity.getBid();
                if (StringUtils.isNotBlank(bid)) {
                    // 获取数据所处最新阶段的状态进行更新
                    initStageAndStatus(bid);
                    entity.setProjectstage(redisUtil.get(bid).toString());
                    entity.setProjectstagestatus(redisUtil.get(GlobalConstants.STAGE_STATUS + bid).toString());
                }
            }
        }*/
        listInfo = ValidateEncryptEntityUtil.validateDecryptList(listInfo, ValidateEncryptEntityUtil.isDecrypt);
        listInfoPage.setRecords(listInfo);
        return listInfoPage;
    }

}
