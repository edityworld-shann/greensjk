package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsThreepersoncategoryfinal;
import org.jeecg.modules.green.common.mapper.ZgsThreepersoncategoryfinalMapper;
import org.jeecg.modules.green.common.service.IZgsThreepersoncategoryfinalService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 三类人员类别实时表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
@Service
public class ZgsThreepersoncategoryfinalServiceImpl extends ServiceImpl<ZgsThreepersoncategoryfinalMapper, ZgsThreepersoncategoryfinal> implements IZgsThreepersoncategoryfinalService {

}
