package org.jeecg.modules.green.xmyssq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 建设科技示范项目验收应用情况信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-13
 * @Version: V1.0
 */
@Data
@TableName("zgs_demoprojectapplication")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_demoprojectapplication对象", description = "建设科技示范项目验收应用情况信息表")
public class ZgsDemoprojectapplication implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 业务主表ID
     */
    @Excel(name = "业务主表ID", width = 15)
    @ApiModelProperty(value = "业务主表ID")
    private java.lang.String baseguid;
    /**
     * 应用新技术名称
     */
    @Excel(name = "应用新技术名称", width = 15)
    @ApiModelProperty(value = "应用新技术名称")
    private java.lang.String technicalname;
    /**
     * 部位
     */
    @Excel(name = "部位", width = 15)
    @ApiModelProperty(value = "部位")
    private java.lang.String position;
    /**
     * 数量
     */
    @Excel(name = "数量", width = 15)
    @ApiModelProperty(value = "数量")
    private java.lang.String amount;
    /**
     * 比重
     */
    @Excel(name = "比重", width = 15)
    @ApiModelProperty(value = "比重")
    private java.lang.String proportion;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * 创建时间
     */
    @Excel(name = "创建时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdate;
    /**
     * 修改人帐号
     */
    @Excel(name = "修改人帐号", width = 15)
    @ApiModelProperty(value = "修改人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * 修改时间
     */
    @Excel(name = "修改时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改时间")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 排序号
     */
    @Excel(name = "排序号", width = 15)
    @ApiModelProperty(value = "排序号")
    private java.math.BigDecimal ordernum;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
