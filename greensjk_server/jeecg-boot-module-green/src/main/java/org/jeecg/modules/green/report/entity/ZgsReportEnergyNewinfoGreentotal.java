package org.jeecg.modules.green.report.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 绿色建筑汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_energy_newinfo_greentotal")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_report_energy_newinfo_greentotal对象", description = "绿色建筑汇总表")
public class ZgsReportEnergyNewinfoGreentotal implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    //@Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    //@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    //@Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String filltm;
    /**
     * 上报日期
     */
    //@Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    //@Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 负责人
     */
    //@Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    //@Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 创建人帐号
     */
    //@Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    //@Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    //@Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    //@Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    //@Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    //@Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    //@Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    //@Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 行政区划代码
     */
    //@Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    //@Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    //@Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    //@Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    //@Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;
    /**
     * 本月绿色建筑竣工情况-建筑项数总合计
     */
    @Excel(name = "本月绿色建筑竣工情况-建筑项数总合计", width = 15)
    @ApiModelProperty(value = "本月绿色建筑竣工情况-建筑项数总合计")
    private java.lang.Integer monthNumber = 0;
    /**
     * 本月绿色建筑竣工情况-建筑面积总合计
     */
    @Excel(name = "本月绿色建筑竣工情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本月绿色建筑竣工情况-建筑面积总合计")
    private java.math.BigDecimal monthArea = new BigDecimal(0);
    /**
     * 本月新建建筑竣工情况-建筑面积总合计
     */
    @Excel(name = "本月新建建筑竣工情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本月新建建筑竣工情况-建筑面积总合计")
    private java.math.BigDecimal monthNewEndArea = new BigDecimal(0);
    /**
     * 本月绿色建筑竣工面积占新建建筑竣工面积比重（%）
     */
    @Excel(name = "本月绿色建筑竣工面积占新建建筑竣工面积比重（%）", width = 15)
    @ApiModelProperty(value = "本月绿色建筑竣工面积占新建建筑竣工面积比重（%）")
    private java.math.BigDecimal monthZb = new BigDecimal(0);

    // add by liur for 本月绿色建筑竣工面积占新建建筑竣工面积比重 重算 --->

    

    // <--- end 


    /**
     * 本年度累计绿色建筑竣工情况-建筑项数总合计
     */
    @Excel(name = "本年度累计绿色建筑竣工情况-建筑项数总合计", width = 15)
    @ApiModelProperty(value = "本年度累计绿色建筑竣工情况-建筑项数总合计")
    private java.lang.Integer yearNumber = 0;
    /**
     * 本年度累计绿色建筑竣工情况-建筑面积总合计
     */
    @Excel(name = "本年度累计绿色建筑竣工情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本年度累计绿色建筑竣工情况-建筑面积总合计")
    private java.math.BigDecimal yearArea = new BigDecimal(0);
    /**
     * 本年度累计新建建筑竣工情况-建筑面积总合计
     */
    @Excel(name = "本年度累计新建建筑竣工情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本年度累计新建建筑竣工情况-建筑面积总合计")
    private java.math.BigDecimal yearNewEndArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工面积占新建建筑竣工面积比重（%）
     */
    @Excel(name = "本年度绿色建筑竣工面积占新建建筑竣工面积比重（%）", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工面积占新建建筑竣工面积比重（%）")
    private java.math.BigDecimal yearZb = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-住宅建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-住宅建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-住宅建筑项数合计")
    private java.lang.Integer yearBuildhouseNumber = 0;
    /**
     * 本年度绿色建筑竣工累计情况-住宅建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-住宅建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-住宅建筑面积合计")
    private java.math.BigDecimal yearBuildhouseArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-公共建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-公共建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-公共建筑项数合计")
    private java.lang.Integer yearBuildpublicNumber = 0;
    /**
     * 本年度绿色建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-公共建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-公共建筑面积合计")
    private java.math.BigDecimal yearBuildpublicArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-工业建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-工业建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-工业建筑项数合计")
    private java.lang.Integer yearBuildindustryNumber = 0;
    /**
     * 本年度绿色建筑竣工累计情况-工业建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-工业建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-工业建筑面积合计")
    private java.math.BigDecimal yearBuildindustryArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-绿色建筑等级项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-绿色建筑等级项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-绿色建筑等级项数合计")
    private java.lang.Integer yearGreenNumber = 0;
    /**
     * 本年度绿色建筑竣工累计情况-绿色建筑等级面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-绿色建筑等级面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-绿色建筑等级面积合计")
    private java.math.BigDecimal yearGreenArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-基本级建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-基本级建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-基本级建筑项数合计")
    private java.lang.Integer yearRankNumber = 0;
    /**
     * 本年度绿色建筑竣工累计情况-基本级建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-基本级建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-基本级建筑面积合计")
    private java.math.BigDecimal yearRankArea = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-一星建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-一星建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-一星建筑项数合计")
    private java.lang.Integer yearRankNumber1 = 0;
    /**
     * 本年度绿色建筑竣工累计情况-一星建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-一星建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-一星建筑面积合计")
    private java.math.BigDecimal yearRankArea1 = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-二星建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-二星建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-二星建筑项数合计")
    private java.lang.Integer yearRankNumber2 = 0;
    /**
     * 本年度绿色建筑竣工累计情况-二星建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-二星建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-二星建筑面积合计")
    private java.math.BigDecimal yearRankArea2 = new BigDecimal(0);
    /**
     * 本年度绿色建筑竣工累计情况-三星建筑项数合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-三星建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-三星建筑项数合计")
    private java.lang.Integer yearRankNumber3 = 0;
    /**
     * 本年度绿色建筑竣工累计情况-三星建筑面积合计
     */
    @Excel(name = "本年度绿色建筑竣工累计情况-三星建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度绿色建筑竣工累计情况-三星建筑面积合计")
    private java.math.BigDecimal yearRankArea3 = new BigDecimal(0);
    /**
     * 默认为空：市州内上报，1市州向省厅上报
     */
    //@Excel(name = "默认为空：市州内上报，1市州向省厅上报", width = 15)
    @ApiModelProperty(value = "默认为空：市州内上报，1市州向省厅上报")
    private java.lang.String areaType;
    /**
     * 默认为空：不显示合计，1显示合计
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "默认为空：不显示合计，1显示合计")
    private java.lang.String totalTag;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
