package org.jeecg.modules.green.jhxmcgjj.service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjectlibraryListInfo;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 项目成果简介
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsPlanresultbaseService extends IService<ZgsPlanresultbase> {
    int spProject(ZgsPlanresultbase zgsPlanresultbase);

    Page<ZgsPlanresultbase> listZgsPlanResultBaseListInfo(Wrapper<ZgsPlanresultbase> queryWrapper, Integer pageNo, Integer pageSize);

    Page<ZgsPlanresultbase> getResultInfo(Wrapper<ZgsPlanresultbase> queryWrapper, Integer pageNo, Integer pageSize);



}
