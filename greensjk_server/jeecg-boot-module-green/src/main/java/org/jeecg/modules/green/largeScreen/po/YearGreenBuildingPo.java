package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

/**
 * @Name ：YearGreenBuildingPo
 * @Description ：<按照年进行返回的>
 * @Author ：Haozhiyang
 * @Date ：2022/8/22 14:47
 * @Version ：1.0
 * @History ：<修改代码时说明>
 */
@Data
public class YearGreenBuildingPo implements Serializable {
}
