package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.common.mapper.ZgsAttachappendixMapper;
import org.jeecg.modules.green.common.service.IZgsAttachappendixService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 多附件信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsAttachappendixServiceImpl extends ServiceImpl<ZgsAttachappendixMapper, ZgsAttachappendix> implements IZgsAttachappendixService {

}
