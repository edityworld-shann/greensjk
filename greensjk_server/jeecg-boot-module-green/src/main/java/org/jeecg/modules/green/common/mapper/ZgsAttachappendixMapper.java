package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 多附件信息表
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsAttachappendixMapper extends BaseMapper<ZgsAttachappendix> {

}
