package org.jeecg.modules.green.xmyszssq.service;

import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 示范项目验收证书基础表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsExamplecertificatebaseService extends IService<ZgsExamplecertificatebase> {
    int spProject(ZgsExamplecertificatebase zgsExamplecertificatebase);

    public String number();
}
