package org.jeecg.modules.green.worktable.entity;

import lombok.Data;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/2217:06
 */
@Data
public class ZgsWorkTable {
    private String menuNameParent;
    private String menuName;
    private Integer count;
    private String menuUrl;
}
