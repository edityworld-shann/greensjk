package org.jeecg.modules.green.worktable.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.entity.ZgsAttachappendix;
import org.jeecg.modules.green.worktable.entity.CityAndStateWorkbenchPo;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTable;
import org.jeecg.modules.green.worktable.entity.ZgsWorkTableTjPro;

import java.util.List;
import java.util.Map;

/**
 * @Description: 工作台
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface IZgsWorkTableService extends IService<ZgsWorkTable> {
    Map<String, Object> getPersonalWorkTableMap();

    Map<String, Object> getUnitWorkTableMap();

    Map<String, Object> getGovWorkTableMap(Integer year);

    Map<String, Object> getExpertWorkTableMap();

    List<ZgsWorkTableTjPro> getGovWorkTableCenterList(Integer year);

    CityAndStateWorkbenchPo energySavingGreenBuildingProjectData(String year, String areacode);

    List<ZgsWorkTable> listOfDistrictsAndCounties(String year, String areacode);
}
