package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsBuildfundbudget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
public interface ZgsBuildfundbudgetMapper extends BaseMapper<ZgsBuildfundbudget> {

}
