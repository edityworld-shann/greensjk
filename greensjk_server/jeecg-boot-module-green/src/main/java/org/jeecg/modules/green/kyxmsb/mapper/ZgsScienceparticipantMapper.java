package org.jeecg.modules.green.kyxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 参加单位及人员的基本情况
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface ZgsScienceparticipantMapper extends BaseMapper<ZgsScienceparticipant> {

}
