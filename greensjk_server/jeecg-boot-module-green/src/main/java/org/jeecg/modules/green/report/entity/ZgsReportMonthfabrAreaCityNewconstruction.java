package org.jeecg.modules.green.report.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 装配式建筑-新开工-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Data
@TableName("zgs_report_monthfabr_area_city_newconstruction")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "已经不用 - 废弃", description = "已经不用 - 废弃")
public class ZgsReportMonthfabrAreaCityNewconstruction implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
    /**
     * 填报人
     */
    @Excel(name = "填报人", width = 15)
    @ApiModelProperty(value = "填报人")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersonname;
    /**
     * 联系电话
     */
    @Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillpersontel;
    /**
     * 报表时间
     */
    @Excel(name = "填报时间", width = 15)
    @ApiModelProperty(value = "填报时间")
    private java.lang.String filltm;
    /**
     * 报表时间
     */
    @Excel(name = "报表时间", width = 15)
    @ApiModelProperty(value = "报表时间")
    private java.lang.String reporttm;
    /**
     * 上报日期
     */
    @Excel(name = "上报日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "上报日期")
    private java.util.Date applydate;
    /**
     * 申报状态  0未上报 1 已上报 3 退回
     */
    @Excel(name = "申报状态  0未上报 1 已上报 3 退回", width = 15)
    @ApiModelProperty(value = "申报状态  0未上报 1 已上报 3 退回")
    private java.math.BigDecimal applystate;
    /**
     * 负责人
     */
    @Excel(name = "负责人", width = 15)
    @ApiModelProperty(value = "负责人")
    private java.lang.String fzr;
    /**
     * 填报单位
     */
    @Excel(name = "填报单位", width = 15)
    @ApiModelProperty(value = "填报单位")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fillunit;
    /**
     * 创建人帐号
     */
    @Excel(name = "创建人帐号", width = 15)
    @ApiModelProperty(value = "创建人帐号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonaccount;
    /**
     * 创建人姓名
     */
    @Excel(name = "创建人姓名", width = 15)
    @ApiModelProperty(value = "创建人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createpersonname;
    /**
     * 创建日期
     */
    @Excel(name = "创建日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createtime;
    /**
     * 修改人账号
     */
    @Excel(name = "修改人账号", width = 15)
    @ApiModelProperty(value = "修改人账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonaccount;
    /**
     * 修改人姓名
     */
    @Excel(name = "修改人姓名", width = 15)
    @ApiModelProperty(value = "修改人姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifypersonname;
    /**
     * 修改日期
     */
    @Excel(name = "修改日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "修改日期")
    private java.util.Date modifytime;
    /**
     * 审核人
     */
    @Excel(name = "审核人", width = 15)
    @ApiModelProperty(value = "审核人")
    private java.lang.String auditer;
    /**
     * 删除标志
     */
    @Excel(name = "删除标志", width = 15)
    @ApiModelProperty(value = "删除标志")
    private java.math.BigDecimal isdelete;
    /**
     * 行政区划代码
     */
    @Excel(name = "行政区划代码", width = 15)
    @ApiModelProperty(value = "行政区划代码")
    private java.lang.String areacode;
    /**
     * 行政区域名称
     */
    @Excel(name = "行政区域名称", width = 15)
    @ApiModelProperty(value = "行政区域名称")
    private java.lang.String areaname;
    /**
     * 退回原因
     */
    @Excel(name = "退回原因", width = 15)
    @ApiModelProperty(value = "退回原因")
    private java.lang.String backreason;
    /**
     * 退回时间
     */
    @Excel(name = "退回时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "退回时间")
    private java.util.Date backrdate;
    /**
     * 年份
     */
    @Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    private java.math.BigDecimal year;
    /**
     * 季度
     */
    @Excel(name = "季度", width = 15)
    @ApiModelProperty(value = "季度")
    private java.math.BigDecimal quarter;
    /**
     * 本月新建建筑竣工情况-建筑项数总合计
     */
    @Excel(name = "本月新建建筑竣工情况-建筑项数总合计", width = 15)
    @ApiModelProperty(value = "本月新建建筑竣工情况-建筑项数总合计")
    private java.lang.Integer monthNumber;
    /**
     * 本月新建建筑竣工情况-建筑面积总合计
     */
    @Excel(name = "本月新建建筑竣工情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本月新建建筑竣工情况-建筑面积总合计")
    private java.math.BigDecimal monthArea;
    /**
     * 本年度新建建筑竣工累计情况-建筑项数总合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-建筑项数总合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-建筑项数总合计")
    private java.lang.Integer yearBuildNumber;
    /**
     * 本年度新建建筑竣工累计情况-建筑面积总合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-建筑面积总合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-建筑面积总合计")
    private java.math.BigDecimal yearBuildArea;
    /**
     * 本年度新建建筑竣工累计情况-住宅建筑项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-住宅建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-住宅建筑项数合计")
    private java.lang.Integer yearBuildhouseNumber;
    /**
     * 本年度新建建筑竣工累计情况-住宅建筑面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-住宅建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-住宅建筑面积合计")
    private java.math.BigDecimal yearBuildhouseArea;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-公共建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-公共建筑项数合计")
    private java.lang.Integer yearBuildpublicNumber;
    /**
     * 本年度新建建筑竣工累计情况-公共建筑面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-公共建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-公共建筑面积合计")
    private java.math.BigDecimal yearBuildpublicArea;
    /**
     * 本年度新建建筑竣工累计情况-工业建筑项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-工业建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-工业建筑项数合计")
    private java.lang.Integer yearBuildindustryNumber;
    /**
     * 本年度新建建筑竣工累计情况-工业建筑面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-工业建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-工业建筑面积合计")
    private java.math.BigDecimal yearBuildindustryArea;
    /**
     * 本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑项数合计")
    private java.lang.Integer yearLowNumber;
    /**
     * 本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-超低能耗、近零能耗建筑面积合计")
    private java.math.BigDecimal yearLowArea;
    /**
     * 本年度新建建筑竣工累计情况-太阳能建筑应用项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-太阳能建筑应用项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-太阳能建筑应用项数合计")
    private java.lang.Integer yearSolarNumber;
    /**
     * 本年度新建建筑竣工累计情况-太阳能建筑应用容量合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-太阳能建筑应用容量合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-太阳能建筑应用容量合计")
    private java.math.BigDecimal yearSolarArea;
    /**
     * 本年度新建建筑竣工累计情况-地热能应用项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-地热能应用项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-地热能应用项数合计")
    private java.lang.Integer yearGeothermalNumber;
    /**
     * 本年度新建建筑竣工累计情况-地热能应用面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-地热能应用面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-地热能应用面积合计")
    private java.math.BigDecimal yearGeothermalArea;
    /**
     * 本年度新建建筑竣工累计情况-应用其他类型技术项数合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-应用其他类型技术项数合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-应用其他类型技术项数合计")
    private java.lang.Integer yearOtherNumber;
    /**
     * 本年度新建建筑竣工累计情况-应用其他类型技术面积合计
     */
    @Excel(name = "本年度新建建筑竣工累计情况-应用其他类型技术面积合计", width = 15)
    @ApiModelProperty(value = "本年度新建建筑竣工累计情况-应用其他类型技术面积合计")
    private java.math.BigDecimal yearOtherArea;
    /**
     * 默认为空：市州内上报，1市州向省厅上报
     */
    @Excel(name = "默认为空：市州内上报，1市州向省厅上报", width = 15)
    @ApiModelProperty(value = "默认为空：市州内上报，1市州向省厅上报")
    private java.lang.String areaType;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
