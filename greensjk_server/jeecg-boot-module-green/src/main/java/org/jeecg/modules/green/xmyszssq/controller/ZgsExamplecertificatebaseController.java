package org.jeecg.modules.green.xmyszssq.controller;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.afterturn.easypoi.entity.ImageEntity;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectapplication;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificresearcher;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyszssq.entity.*;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificateexpertService;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertimplementunitService;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertparticipantService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 示范项目验收证书基础表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Api(tags = "示范项目验收证书基础表")
@RestController
@RequestMapping("/xmyszssq/zgsExamplecertificatebase")
@Slf4j
public class ZgsExamplecertificatebaseController extends JeecgController<ZgsExamplecertificatebase, IZgsExamplecertificatebaseService> {
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.viewloadUrl}")
    private String viewloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsExamplecertificatebaseService zgsExamplecertificatebaseService;
    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsExamplecertificateexpertService zgsExamplecertificateexpertService;
    @Autowired
    private IZgsExamplecertimplementunitService zgsExamplecertimplementunitService;
    @Autowired
    private IZgsExamplecertparticipantService zgsExamplecertparticipantService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlanresultbaseService planresultbaseService;

    /**
     * 分页列表查询
     *
     * @param zgsExamplecertificatebase
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-分页列表查询")
    @ApiOperation(value = "示范项目验收证书基础表-分页列表查询", notes = "示范项目验收证书基础表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsExamplecertificatebase zgsExamplecertificatebase,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsExamplecertificatebase> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectname())) {
            queryWrapper.like("projectname", zgsExamplecertificatebase.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectnumber())) {
            queryWrapper.like("projectnumber", zgsExamplecertificatebase.getProjectnumber());
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(projectnumber,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectstage())) {
            if (!"-1".equals(zgsExamplecertificatebase.getProjectstage())) {
                queryWrapper.eq("projectstage", QueryWrapperUtil.getProjectstage(zgsExamplecertificatebase.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51)).or().isNull("projectstage");
                });
            }
        }
//        QueryWrapperUtil.initExamplecertificatebaseSelect(queryWrapper, zgsExamplecertificatebase.getStatus(), sysUser);
       // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsExamplecertificatebase.getStatus(), sysUser, 1);
        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsExamplecertificatebase.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("status", GlobalConstants.SHENHE_STATUS0).ne("status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("isdelete", 1);
        //TODO 修改根据初审日期进行降序 2022-09-21
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("enddate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(firstdate),0,1), firstdate DESC");
        }
        Page<ZgsExamplecertificatebase> page = new Page<ZgsExamplecertificatebase>(pageNo, pageSize);
        // IPage<ZgsExamplecertificatebase> pageList = zgsExamplecertificatebaseService.page(page, queryWrapper);


        // 推荐单位查看项目统计，状态为空
        IPage<ZgsExamplecertificatebase> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsExamplecertificatebase> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsExamplecertificatebase> pageList = null;

        // 科研项目验收证书
        redisUtil.expire("sfxmyszs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmyszsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("sfxmyszsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsExamplecertificatebase.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsExamplecertificatebase.getDspForTjdw())) {
            if (zgsExamplecertificatebase.getFlagByWorkTable().equals("1")) { // 推荐单位下的 项目统计
                if (!redisUtil.hasKey("sfxmyszs")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsExamplecertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("sfxmyszs",pageListForProjectCount.getTotal());
                }
            }
            if (zgsExamplecertificatebase.getDspForTjdw().equals("3")) {
                if (!redisUtil.hasKey("sfxmyszsOfTjdw")) {  // for 推荐单位待审批项目
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsExamplecertificatebaseService.page(page, queryWrapper);
                    redisUtil.set("sfxmyszsOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsExamplecertificatebase.getDspForTjdw()) && StringUtils.isBlank(zgsExamplecertificatebase.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsExamplecertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isBlank(zgsExamplecertificatebase.getDspForTjdw()) && StringUtils.isNotBlank(zgsExamplecertificatebase.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsExamplecertificatebaseService.page(page, queryWrapper);

        } else if (StringUtils.isNotBlank(zgsExamplecertificatebase.getDspForSt()) && zgsExamplecertificatebase.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis判断查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsExamplecertificatebaseService.page(page, queryWrapper);
            redisUtil.set("sfxmyszsOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsExamplecertificatebase.getStatus(), sysUser, 1);
            pageList = zgsExamplecertificatebaseService.page(page, queryWrapper);
        }



        /*// 示范项目验收证书
        redisUtil.expire("sfxmyszs",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmyszs")) {
            if (StringUtils.isNotBlank(zgsExamplecertificatebase.getFlagByWorkTable()) && zgsExamplecertificatebase.getFlagByWorkTable().equals("1")) {
                redisUtil.set("sfxmyszs",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmyszsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmyszsOfDsp")) {  // for 省厅待审批项目
            if (StringUtils.isNotBlank(zgsExamplecertificatebase.getDspForSt()) && zgsExamplecertificatebase.getDspForSt().equals("2")) {
                redisUtil.set("sfxmyszsOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("sfxmyszsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("sfxmyszsOfTjdw")) {  // for 推荐单位待审批项目
            if (StringUtils.isNotBlank(zgsExamplecertificatebase.getDspForTjdw()) && zgsExamplecertificatebase.getDspForTjdw().equals("3")) {
                redisUtil.set("sfxmyszsOfTjdw",pageList.getTotal());
            }
        }*/

        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsExamplecertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
                    }
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsExamplecertificatebase zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    //省厅账号显示初审上报日期
//                    pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
//                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid())) {
//                        pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getProjectlibraryguid()));
//                    } else {
//                        pageList.getRecords().get(m).setCstatus("验收结题证书阶段");
//                    }
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //终审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                    //查询当前与任务书是否有关联，没有直接给个默认任务书提示未生成任务书
                    if (StringUtils.isNotEmpty(zgsInfo.getProjectlibraryguid()) && StringUtils.isNotEmpty(zgsInfo.getProjectname())) {
                        QueryWrapper<ZgsProjecttask> queryWrapper0 = new QueryWrapper<ZgsProjecttask>();
                        queryWrapper0.eq("projectguid", zgsInfo.getProjectlibraryguid());
                        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
                        queryWrapper0.ne("isdelete", 1);
                        ZgsProjecttask zgsProjecttask = zgsProjecttaskService.getOne(queryWrapper0);
                        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getPdfUrl())) {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsProjecttask.getPdfUrl());
                        } else {
                            pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                        }
                    } else {
                        pageList.getRecords().get(m).setPdfUrl(viewloadUrl + "green_task/nocreate.pdf");
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-撤回")
    @ApiOperation(value = "示范项目验收证书基础表-撤回", notes = "示范项目验收证书基础表-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsExamplecertificatebase zgsExamplecertificatebase) {
        if (zgsExamplecertificatebase != null) {
            String id = zgsExamplecertificatebase.getId();
            String bid = zgsExamplecertificatebase.getProjectlibraryguid();
            String status = zgsExamplecertificatebase.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51).equals(zgsExamplecertificatebase.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsExamplecertificatebase info = new ZgsExamplecertificatebase();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsExamplecertificatebaseService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsExamplecertificatebase
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-审批")
    @ApiOperation(value = "示范项目验收证书基础表-审批", notes = "示范项目验收证书基础表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsExamplecertificatebase zgsExamplecertificatebase) {
        zgsExamplecertificatebaseService.spProject(zgsExamplecertificatebase);
        String projectlibraryguid = zgsExamplecertificatebaseService.getById(zgsExamplecertificatebase.getId()).getProjectlibraryguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        zgsProjectlibraryService.initStageAndStatusForYszs(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 添加
     *
     * @param zgsExamplecertificatebase
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-添加")
    @ApiOperation(value = "示范项目验收证书基础表-添加", notes = "示范项目验收证书基础表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsExamplecertificatebase zgsExamplecertificatebase) {
        // 1.项目基本概况
        Result result = new Result();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsExamplecertificatebase.setId(id);
        zgsExamplecertificatebase.setEnterpriseguid(sysUser.getEnterpriseguid());
        if (GlobalConstants.handleSubmit.equals(zgsExamplecertificatebase.getStatus())) {
            //保存并上报
            zgsExamplecertificatebase.setApplydate(new Date());
        } else {
            zgsExamplecertificatebase.setApplydate(null);
        }
        zgsExamplecertificatebase.setCreateaccountname(sysUser.getUsername());
        zgsExamplecertificatebase.setCreateusername(sysUser.getRealname());
        zgsExamplecertificatebase.setCreatedate(new Date());
        //判断是否添加验收项目，否则给予提醒，需先添加验收项目
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper = new QueryWrapper();
        queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
        queryWrapper.and(qwp -> {
            qwp.eq("status", GlobalConstants.SHENHE_STATUS2).or().eq("status", GlobalConstants.SHENHE_STATUS8).or()
                    .eq("status", GlobalConstants.SHENHE_STATUS10).or().eq("status", GlobalConstants.SHENHE_STATUS11).or();
        });
        queryWrapper.and(qwp1 -> {
            if (StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectname())) {
                qwp1.like("projectname", zgsExamplecertificatebase.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectlibraryguid())) {
                qwp1.or().eq("projectlibraryguid", zgsExamplecertificatebase.getProjectlibraryguid());
            }
        });
//        List<ZgsDemoprojectacceptance> list = zgsDemoprojectacceptanceService.list(queryWrapper);
//        if (list.size() == 0) {
//            return Result.error("请项目验收管理流程结束后，方可进行此阶段申请。！");
//        }
        //判断项目终止
//        QueryWrapper<ZgsProjecttask> queryHist = new QueryWrapper();
//        queryHist.ne("isdelete", 1);
//        queryHist.eq("projectguid", zgsExamplecertificatebase.getProjectlibraryguid());
//        queryHist.eq("ishistory", "1");
//        List<ZgsProjecttask> listHist = zgsProjecttaskService.list(queryHist);
//        if (listHist.size() > 0) {
//            return Result.error("项目已终止！");
//        }
        zgsExamplecertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
        zgsExamplecertificatebase.setProjectstagestatus(zgsExamplecertificatebase.getStatus());
        if (zgsExamplecertificatebaseService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificatebase, ValidateEncryptEntityUtil.isEncrypt))) {

            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsExamplecertificatebase.getStatus()) && StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsExamplecertificatebase.getProjectlibraryguid());
            }
//        redisUtil.set(zgsExamplecertificatebase.getProjectlibraryguid(), "验收结题证书阶段");
            //主要参加人员名单
            List<ZgsExamplecertparticipant> zgsExamplecertparticipantList = zgsExamplecertificatebase.getZgsExamplecertparticipantList();
            if (zgsExamplecertparticipantList != null && zgsExamplecertparticipantList.size() > 0) {
                if (zgsExamplecertparticipantList.size() > 15) {
                    return Result.error("人员名单数量不得超过15人！");
                }
                for (int a1 = 0; a1 < zgsExamplecertparticipantList.size(); a1++) {
                    ZgsExamplecertparticipant zgsExamplecertparticipant = zgsExamplecertparticipantList.get(a1);
                    zgsExamplecertparticipant.setId(UUID.randomUUID().toString());
                    zgsExamplecertparticipant.setBaseguid(id);
                    zgsExamplecertparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                    zgsExamplecertparticipant.setCreateaccountname(sysUser.getUsername());
                    zgsExamplecertparticipant.setCreateusername(sysUser.getRealname());
                    zgsExamplecertparticipant.setCreatedate(new Date());
                    zgsExamplecertparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertparticipant, ValidateEncryptEntityUtil.isEncrypt));
                }
            }

            //实施单位情况
            List<ZgsExamplecertimplementunit> zgsExamplecertimplementunitList = zgsExamplecertificatebase.getZgsExamplecertimplementunitList();
            if (zgsExamplecertimplementunitList != null && zgsExamplecertimplementunitList.size() > 0) {
                if (zgsExamplecertimplementunitList.size() > 5) {
                    return Result.error("项目完成单位情况不得超过5个！");
                }
                for (int a1 = 0; a1 < zgsExamplecertimplementunitList.size(); a1++) {
                    ZgsExamplecertimplementunit zgsExamplecertimplementunit = zgsExamplecertimplementunitList.get(a1);
                    zgsExamplecertimplementunit.setId(UUID.randomUUID().toString());
                    zgsExamplecertimplementunit.setBaseguid(id);
                    zgsExamplecertimplementunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                    zgsExamplecertimplementunit.setCreateaccountname(sysUser.getUsername());
                    zgsExamplecertimplementunit.setCreateusername(sysUser.getRealname());
                    zgsExamplecertimplementunit.setCreatedate(new Date());
                    zgsExamplecertimplementunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertimplementunit, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            //验收专家名单
            List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList = zgsExamplecertificatebase.getZgsExamplecertificateexpertList();
            if (zgsExamplecertificateexpertList != null && zgsExamplecertificateexpertList.size() > 0) {
                String baseguid = zgsExamplecertificatebase.getProjectlibraryguid();
                for (int a1 = 0; a1 < zgsExamplecertificateexpertList.size(); a1++) {
                    ZgsExamplecertificateexpert zgsExamplecertificateexpert = zgsExamplecertificateexpertList.get(a1);
                    zgsExamplecertificateexpert.setId(UUID.randomUUID().toString());
                    // zgsExamplecertificateexpert.setBaseguid(id);
                    zgsExamplecertificateexpert.setBaseguid(baseguid);
                    zgsExamplecertificateexpert.setProjectStage("ysjd"); // 验收阶段
                    zgsExamplecertificateexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                    zgsExamplecertificateexpert.setCreateaccountname(sysUser.getUsername());
                    zgsExamplecertificateexpert.setCreateusername(sysUser.getRealname());
                    zgsExamplecertificateexpert.setCreatedate(new Date());
                    zgsExamplecertificateexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificateexpert, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            List<ZgsMattermaterial> zgsMattermaterialList = zgsExamplecertificatebase.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    String mattermaterialId = UUID.randomUUID().toString();
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    zgsMattermaterial.setId(mattermaterialId);
                    zgsMattermaterial.setBuildguid(id);
                    zgsMattermaterialService.save(zgsMattermaterial);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            zgsAttachappendix.setId(UUID.randomUUID().toString());
                            zgsAttachappendix.setRelationid(mattermaterialId);
                            zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                            zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                            zgsAttachappendix.setCreatetime(new Date());
                            zgsAttachappendix.setAppendixtype(GlobalConstants.ExampleCertificate);
                            zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
            result.setResult(id);
            result.setSuccess(true);
            result.setMessage("添加成功");
        } else {
            result.setResult("");
            result.setSuccess(false);
            result.setMessage("添加失败");
        }
        return result;
    }

    /**
     * 编辑
     *
     * @param zgsExamplecertificatebase
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-编辑")
    @ApiOperation(value = "示范项目验收证书基础表-编辑", notes = "示范项目验收证书基础表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsExamplecertificatebase zgsExamplecertificatebase) {
        //1.项目基本概况
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsExamplecertificatebase != null && StringUtils.isNotEmpty(zgsExamplecertificatebase.getId())) {
            String id = zgsExamplecertificatebase.getId();
            if (GlobalConstants.handleSubmit.equals(zgsExamplecertificatebase.getStatus())) {
                //保存并上报
                zgsExamplecertificatebase.setApplydate(new Date());
                zgsExamplecertificatebase.setAuditopinion(null);
//                zgsExamplecertificatebase.setReturntype(null);
            } else {
                zgsExamplecertificatebase.setApplydate(null);
                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                ZgsExamplecertificatebase projectTask = zgsExamplecertificatebaseService.getById(id);
                if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                    zgsExamplecertificatebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                }
            }
            zgsExamplecertificatebase.setModifyaccountname(sysUser.getUsername());
            zgsExamplecertificatebase.setModifyusername(sysUser.getRealname());
            zgsExamplecertificatebase.setModifydate(new Date());
            zgsExamplecertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_51));
            zgsExamplecertificatebase.setProjectstagestatus(zgsExamplecertificatebase.getStatus());
            zgsExamplecertificatebaseService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificatebase, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsExamplecertificatebase.getStatus()) && StringUtils.isNotEmpty(zgsExamplecertificatebase.getProjectlibraryguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsExamplecertificatebase.getProjectlibraryguid());
            }
            String enterpriseguid = zgsExamplecertificatebase.getEnterpriseguid();
            //主要参加人员名单
            List<ZgsExamplecertparticipant> zgsExamplecertparticipantList0 = zgsExamplecertificatebase.getZgsExamplecertparticipantList();
            QueryWrapper<ZgsExamplecertparticipant> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", id);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            List<ZgsExamplecertparticipant> zgsExamplecertparticipantList0_1 = zgsExamplecertparticipantService.list(queryWrapper1);
            if (zgsExamplecertparticipantList0 != null && zgsExamplecertparticipantList0.size() > 0) {
                if (zgsExamplecertparticipantList0.size() > 15) {
                    return Result.error("人员名单数量不得超过15人！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsExamplecertparticipantList0.size(); a0++) {
                    ZgsExamplecertparticipant zgsExamplecertparticipant = zgsExamplecertparticipantList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsExamplecertparticipant.getId())) {
                        //编辑
                        zgsExamplecertparticipant.setModifyaccountname(sysUser.getUsername());
                        zgsExamplecertparticipant.setModifyusername(sysUser.getRealname());
                        zgsExamplecertparticipant.setModifydate(new Date());
                        zgsExamplecertparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertparticipant, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsExamplecertparticipant.setId(UUID.randomUUID().toString());
                        zgsExamplecertparticipant.setBaseguid(id);
                        zgsExamplecertparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsExamplecertparticipant.setCreateaccountname(sysUser.getUsername());
                        zgsExamplecertparticipant.setCreateusername(sysUser.getRealname());
                        zgsExamplecertparticipant.setCreatedate(new Date());
                        zgsExamplecertparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertparticipant, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsExamplecertparticipant zgsExamplecertparticipant1 : zgsExamplecertparticipantList0_1) {
                        if (!zgsExamplecertparticipant1.getId().equals(zgsExamplecertparticipant.getId())) {
                            map0.put(zgsExamplecertparticipant1.getId(), zgsExamplecertparticipant1.getId());
                        } else {
                            list0.add(zgsExamplecertparticipant1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsExamplecertparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsExamplecertparticipantList0_1 != null && zgsExamplecertparticipantList0_1.size() > 0) {
                    for (ZgsExamplecertparticipant zgsExamplecertparticipant : zgsExamplecertparticipantList0_1) {
                        zgsExamplecertparticipantService.removeById(zgsExamplecertparticipant.getId());
                    }
                }
            }

            //实施单位情况
            List<ZgsExamplecertimplementunit> zgsExamplecertimplementunitList0 = zgsExamplecertificatebase.getZgsExamplecertimplementunitList();
            QueryWrapper<ZgsExamplecertimplementunit> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", id);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            List<ZgsExamplecertimplementunit> zgsExamplecertimplementunitList0_1 = zgsExamplecertimplementunitService.list(queryWrapper3);
            if (zgsExamplecertimplementunitList0 != null && zgsExamplecertimplementunitList0.size() > 0) {
                if (zgsExamplecertimplementunitList0.size() > 5) {
                    return Result.error("项目完成单位情况不得超过5个！");
                }
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsExamplecertimplementunitList0.size(); a0++) {
                    ZgsExamplecertimplementunit zgsExamplecertimplementunit = zgsExamplecertimplementunitList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsExamplecertimplementunit.getId())) {
                        //编辑
                        zgsExamplecertimplementunit.setModifyaccountname(sysUser.getUsername());
                        zgsExamplecertimplementunit.setModifyusername(sysUser.getRealname());
                        zgsExamplecertimplementunit.setModifydate(new Date());
                        zgsExamplecertimplementunitService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertimplementunit, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsExamplecertimplementunit.setId(UUID.randomUUID().toString());
                        zgsExamplecertimplementunit.setBaseguid(id);
                        zgsExamplecertimplementunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsExamplecertimplementunit.setCreateaccountname(sysUser.getUsername());
                        zgsExamplecertimplementunit.setCreateusername(sysUser.getRealname());
                        zgsExamplecertimplementunit.setCreatedate(new Date());
                        zgsExamplecertimplementunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertimplementunit, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsExamplecertimplementunit zgsExamplecertimplementunit1 : zgsExamplecertimplementunitList0_1) {
                        if (!zgsExamplecertimplementunit1.getId().equals(zgsExamplecertimplementunit.getId())) {
                            map0.put(zgsExamplecertimplementunit1.getId(), zgsExamplecertimplementunit1.getId());
                        } else {
                            list0.add(zgsExamplecertimplementunit1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsExamplecertimplementunitService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsExamplecertimplementunitList0_1 != null && zgsExamplecertimplementunitList0_1.size() > 0) {
                    for (ZgsExamplecertimplementunit zgsExamplecertimplementunit : zgsExamplecertimplementunitList0_1) {
                        zgsExamplecertimplementunitService.removeById(zgsExamplecertimplementunit.getId());
                    }
                }
            }

            //验收专家名单（原有专家信息修改逻辑）
            /*List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList0 = zgsExamplecertificatebase.getZgsExamplecertificateexpertList();
            QueryWrapper<ZgsExamplecertificateexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", id);
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList0_1 = zgsExamplecertificateexpertService.list(queryWrapper4);
            if (zgsExamplecertificateexpertList0 != null && zgsExamplecertificateexpertList0.size() > 0) {
                Map<String, String> map0 = new HashMap<>();
                List<String> list0 = new ArrayList<>();
                for (int a0 = 0; a0 < zgsExamplecertificateexpertList0.size(); a0++) {
                    ZgsExamplecertificateexpert zgsExamplecertificateexpert = zgsExamplecertificateexpertList0.get(a0);
                    if (StringUtils.isNotEmpty(zgsExamplecertificateexpert.getId())) {
                        //编辑
                        zgsExamplecertificateexpert.setModifyaccountname(sysUser.getUsername());
                        zgsExamplecertificateexpert.setModifyusername(sysUser.getRealname());
                        zgsExamplecertificateexpert.setModifydate(new Date());
                        zgsExamplecertificateexpertService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificateexpert, ValidateEncryptEntityUtil.isEncrypt));
                    } else {
                        //新增
                        zgsExamplecertificateexpert.setId(UUID.randomUUID().toString());
                        zgsExamplecertificateexpert.setBaseguid(id);
                        zgsExamplecertificateexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsExamplecertificateexpert.setCreateaccountname(sysUser.getUsername());
                        zgsExamplecertificateexpert.setCreateusername(sysUser.getRealname());
                        zgsExamplecertificateexpert.setCreatedate(new Date());
                        zgsExamplecertificateexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificateexpert, ValidateEncryptEntityUtil.isEncrypt));
                    }
                    for (ZgsExamplecertificateexpert zgsExamplecertificateexpert1 : zgsExamplecertificateexpertList0_1) {
                        if (!zgsExamplecertificateexpert1.getId().equals(zgsExamplecertificateexpert.getId())) {
                            map0.put(zgsExamplecertificateexpert1.getId(), zgsExamplecertificateexpert1.getId());
                        } else {
                            list0.add(zgsExamplecertificateexpert1.getId());
                        }
                    }
                }
                map0.keySet().removeAll(list0);
                zgsExamplecertificateexpertService.removeByIds(new ArrayList<>(map0.keySet()));
            } else {
                if (zgsExamplecertificateexpertList0_1 != null && zgsExamplecertificateexpertList0_1.size() > 0) {
                    for (ZgsExamplecertificateexpert zgsExamplecertificateexpert : zgsExamplecertificateexpertList0_1) {
                        zgsExamplecertificateexpertService.removeById(zgsExamplecertificateexpert.getId());
                    }
                }
            }*/

            // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
            QueryWrapper<ZgsExamplecertificateexpert> expertQueryWrapper = new QueryWrapper<>();
            expertQueryWrapper.eq("baseguid",zgsExamplecertificatebase.getProjectlibraryguid());
            expertQueryWrapper.eq("project_stage","ysjd");
            expertQueryWrapper.ne("isdelete",1);
            List<ZgsExamplecertificateexpert> ZgsSacceptcertexpertList = zgsExamplecertificateexpertService.list(expertQueryWrapper);
            if (ZgsSacceptcertexpertList.size() > 0) {
                // 删除历史专家信息
                QueryWrapper<ZgsExamplecertificateexpert> expertDeleteWrapper = new QueryWrapper<>();
                expertDeleteWrapper.eq("baseguid",zgsExamplecertificatebase.getProjectlibraryguid());
                expertDeleteWrapper.eq("project_stage","ysjd");
                expertDeleteWrapper.ne("isdelete",1);
                boolean bolResult = zgsExamplecertificateexpertService.remove(expertDeleteWrapper);
                // 删除成功后，保存修改后的专家信息  rxl  20230620
                if (bolResult) {
                    List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList = zgsExamplecertificatebase.getZgsExamplecertificateexpertList();
                    if (zgsExamplecertificateexpertList != null && zgsExamplecertificateexpertList.size() > 0) {
                        String baseguid = zgsExamplecertificatebase.getProjectlibraryguid();
                        for (int a1 = 0; a1 < zgsExamplecertificateexpertList.size(); a1++) {
                            ZgsExamplecertificateexpert zgsExamplecertificateexpert = zgsExamplecertificateexpertList.get(a1);
                            zgsExamplecertificateexpert.setId(UUID.randomUUID().toString());
                            // zgsExamplecertificateexpert.setBaseguid(id);
                            zgsExamplecertificateexpert.setBaseguid(baseguid);
                            zgsExamplecertificateexpert.setProjectStage("ysjd"); // 验收阶段
                            zgsExamplecertificateexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsExamplecertificateexpert.setCreateaccountname(sysUser.getUsername());
                            zgsExamplecertificateexpert.setCreateusername(sysUser.getRealname());
                            zgsExamplecertificateexpert.setCreatedate(new Date());
                            zgsExamplecertificateexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsExamplecertificateexpert, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }

            //相关附件
            List<ZgsMattermaterial> zgsMattermaterialList = zgsExamplecertificatebase.getZgsMattermaterialList();
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int a4 = 0; a4 < zgsMattermaterialList.size(); a4++) {
                    ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a4);
                    String mattermaterialId = zgsMattermaterial.getId();
                    UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                    wrapper.eq("relationid", mattermaterialId);
                    //先删除原来的再重新添加
                    zgsAttachappendixService.remove(wrapper);
                    if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                        for (int a4_0 = 0; a4_0 < zgsMattermaterial.getZgsAttachappendixList().size(); a4_0++) {
                            ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a4_0);
                            if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                zgsAttachappendix.setModifytime(new Date());
                            } else {
                                zgsAttachappendix.setId(UUID.randomUUID().toString());
                                zgsAttachappendix.setRelationid(mattermaterialId);
                                zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                zgsAttachappendix.setCreatetime(new Date());
                                zgsAttachappendix.setAppendixtype(GlobalConstants.ExampleCertificate);
                                zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                            }
                            zgsAttachappendixService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt));
                        }
                    }
                }
            }
        }

        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-通过id删除")
    @ApiOperation(value = "示范项目验收证书基础表-通过id删除", notes = "示范项目验收证书基础表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsExamplecertificatebaseService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-批量删除")
    @ApiOperation(value = "示范项目验收证书基础表-批量删除", notes = "示范项目验收证书基础表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsExamplecertificatebaseService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-通过id查询")
    @ApiOperation(value = "示范项目验收证书基础表-通过id查询", notes = "示范项目验收证书基础表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id,@RequestParam(name = "ysid", required = false) String ysid) {
        ZgsExamplecertificatebase zgsExamplecertificatebase = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsExamplecertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsExamplecertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsExamplecertificatebase == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsExamplecertificatebase,ysid);
            }
            if ((zgsExamplecertificatebase.getFirstdate() != null || zgsExamplecertificatebase.getFinishdate() != null) && zgsExamplecertificatebase.getApplydate() != null) {
                zgsExamplecertificatebase.setSpLogStatus(1);
            } else {
                zgsExamplecertificatebase.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsExamplecertificatebase = new ZgsExamplecertificatebase();
            Map<String, Object> map = new HashMap<>();
            map.put("projecttypenum", GlobalConstants.ExampleCertificate);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsExamplecertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            zgsExamplecertificatebase.setSpLogStatus(0);
        }
        return Result.OK(zgsExamplecertificatebase);
    }

    private void initProjectSelectById(ZgsExamplecertificatebase zgsExamplecertificatebase,String ysid) {
        String enterpriseguid = zgsExamplecertificatebase.getEnterpriseguid();
        String buildguid = zgsExamplecertificatebase.getId();
        String baseguid = zgsExamplecertificatebase.getProjectlibraryguid();
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {
            //主要参加人员名单
            QueryWrapper<ZgsExamplecertparticipant> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("enterpriseguid", enterpriseguid);
            queryWrapper1.eq("baseguid", buildguid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            queryWrapper1.last("limit 15");
            List<ZgsExamplecertparticipant> zgsExamplecertparticipantList = zgsExamplecertparticipantService.list(queryWrapper1);
            zgsExamplecertificatebase.setZgsExamplecertparticipantList(ValidateEncryptEntityUtil.validateDecryptList(zgsExamplecertparticipantList, ValidateEncryptEntityUtil.isDecrypt));
            //相关附件
            map.put("buildguid", buildguid);
            //后期查下ExampleCertificate出现在哪张表里
            map.put("projecttypenum", "ExampleCertificate");
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper2), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsExamplecertificatebase.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //实施单位情况
            QueryWrapper<ZgsExamplecertimplementunit> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("enterpriseguid", enterpriseguid);
            queryWrapper3.eq("baseguid", buildguid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("ordernum");
            queryWrapper3.last("limit 5");
            List<ZgsExamplecertimplementunit> zgsExamplecertimplementunitList = zgsExamplecertimplementunitService.list(queryWrapper3);
            zgsExamplecertificatebase.setZgsExamplecertimplementunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsExamplecertimplementunitList, ValidateEncryptEntityUtil.isDecrypt));
            //验收专家名单
            QueryWrapper<ZgsExamplecertificateexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            // queryWrapper4.eq("baseguid", buildguid);
            queryWrapper4.eq("baseguid", baseguid);
            queryWrapper4.eq("project_stage", "ysjd");
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsExamplecertificateexpert> zgsExamplecertificateexpertList = zgsExamplecertificateexpertService.list(queryWrapper4);

            // 验收证书阶段 查询成果简介 进行回显   rxl  20230629
            QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper<>();
            queryWrapper.select("applyrange,enddate,finishenterprise,issecurity,linktel,projectleader,promotiondirection,receptiontime,resultintro,resultname,startdate,status,result_show");
            queryWrapper.eq("projectlibraryguid",baseguid).or().eq("ysid",ysid);
            queryWrapper.eq("status",'8');

            List<ZgsPlanresultbase> listZgsPlanresultbase = planresultbaseService.list(queryWrapper);
            if (listZgsPlanresultbase.size() > 0) {
              zgsExamplecertificatebase.setZgsPlanresultbase(ValidateEncryptEntityUtil.validateDecryptObject(listZgsPlanresultbase.get(0), ValidateEncryptEntityUtil.isDecrypt));
            }
            zgsExamplecertificatebase.setZgsExamplecertificateexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsExamplecertificateexpertList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param zgsExamplecertificatebase
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsExamplecertificatebase zgsExamplecertificatebase,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsExamplecertificatebase, 1, 9999, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsExamplecertificatebase> pageList = (IPage<ZgsExamplecertificatebase>) result.getResult();
        List<ZgsExamplecertificatebase> list = pageList.getRecords();
        List<ZgsExamplecertificatebase> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "示范项目验收证书基础表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsExamplecertificatebase.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsExamplecertificatebase, ZgsExamplecertificatebase.class, "示范项目验收证书基础表");
        return mv;

    }

    private String getId(ZgsExamplecertificatebase item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsExamplecertificatebase.class);
    }


    /**
     * 通过id查询导出word
     *
     * @param id
     * @return
     */
    @AutoLog(value = "示范项目验收证书基础表-通过id查询导出word")
    @ApiOperation(value = "示范项目验收证书基础表-通过id查询导出word", notes = "示范项目验收证书基础表-通过id查询导出word")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsExamplecertificatebase zgsExamplecertificatebase = ValidateEncryptEntityUtil.validateDecryptObject(zgsExamplecertificatebaseService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsExamplecertificatebase != null) {
            initProjectSelectById(zgsExamplecertificatebase,"");
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "17甘肃省建设科技示范项目验收证书.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsExamplecertificatebase);
        //完成单位情况-start
        StringBuffer sb = new StringBuffer();
        if (templateMap != null && zgsExamplecertificatebase.getZgsExamplecertimplementunitList() != null) {
            if (zgsExamplecertificatebase.getZgsExamplecertimplementunitList().size() > 0) {
                for (int i = 0; i < zgsExamplecertificatebase.getZgsExamplecertimplementunitList().size(); i++) {
                    ZgsExamplecertimplementunit zgsSpostprojectfinish = zgsExamplecertificatebase.getZgsExamplecertimplementunitList().get(i);
                    sb.append(zgsSpostprojectfinish.getEnterprisename());
                    if (i < zgsExamplecertificatebase.getZgsExamplecertimplementunitList().size() - 1) {
                        sb.append("、");
                    }
                }
            }
            templateMap.put("enterprisenameList", sb.toString());
        }
        //完成单位情况-end
        //图片处理-start
        ImageEntity imageYiJian = new ImageEntity();
        ImageEntity imageYiJian1 = new ImageEntity();
        ImageEntity imageQianZi = new ImageEntity();
        ImageEntity imageQianZi1 = new ImageEntity();
        String filePath1 = "";
        String filePath2 = "";
        String filePath3 = "";
        String filePath4 = "";
        if (zgsExamplecertificatebase != null && zgsExamplecertificatebase.getZgsMattermaterialList().size() > 1) {
            List<ZgsMattermaterial> zgsMattermaterialList = zgsExamplecertificatebase.getZgsMattermaterialList();
            for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(i);
                if (i == 1 && zgsMattermaterial.getZgsAttachappendixList().size() > 2) {
                    for (int j = 0; j < zgsMattermaterial.getZgsAttachappendixList().size(); j++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(j);
                        if (!zgsAttachappendix.getAppendixname().contains("专家")) {
                            zgsMattermaterialList.get(i).getZgsAttachappendixList().remove(j);
                        }
                    }
                }
            }
            imageYiJian.setHeight(600);
            imageYiJian.setWidth(500);
            filePath1 = upload + File.separator + zgsMattermaterialList.get(0).getZgsAttachappendixList().get(0).getAppendixpath();
            filePath1 = LibreOfficeUtil.wordConverterToJpg(filePath1);
            log.info("filePath1=" + filePath1);
            File file1 = new File(filePath1);
            if (file1.exists()) {
                log.info("file1文件存在-yes");
            } else {
                log.info("file1文件不存在-no");
            }
            //url路径
//            imageYiJian.setUrl(filePath1);
            //字节流读取
            imageYiJian.setData(FileUtil.readBytes(file1));
            //设置读取图⽚⽅式必须
//            imageYiJian.setType(ImageEntity.URL);
            imageYiJian.setType(ImageEntity.Data);
            if (zgsMattermaterialList.get(0).getZgsAttachappendixList().size() > 1) {
                imageYiJian1.setHeight(600);
                imageYiJian1.setWidth(500);
                filePath2 = upload + File.separator + zgsMattermaterialList.get(0).getZgsAttachappendixList().get(1).getAppendixpath();
                filePath2 = LibreOfficeUtil.wordConverterToJpg(filePath2);
                log.info("filePath2=" + filePath2);
                File file2 = new File(filePath2);
                if (file2.exists()) {
                    log.info("file2文件存在-yes");
                } else {
                    log.info("file2文件不存在-no");
                }
//                imageYiJian1.setUrl(filePath2);
                imageYiJian1.setData(FileUtil.readBytes(file2));
                //设置读取图⽚⽅式必须
//                imageYiJian1.setType(ImageEntity.URL);
                imageYiJian1.setType(ImageEntity.Data);
            } else {
                imageYiJian1 = null;
            }
            imageQianZi.setHeight(280);
            imageQianZi.setWidth(580);
            filePath3 = upload + File.separator + zgsMattermaterialList.get(1).getZgsAttachappendixList().get(0).getAppendixpath();
            filePath3 = LibreOfficeUtil.wordConverterToJpg(filePath3);
            log.info("filePath3=" + filePath3);
            File file3 = new File(filePath3);
            if (file3.exists()) {
                log.info("file3文件存在-yes");
            } else {
                log.info("file3文件不存在-no");
            }
//            imageQianZi.setUrl(filePath3);
            imageQianZi.setData(FileUtil.readBytes(file3));
            //设置读取图⽚⽅式必须
//            imageQianZi.setType(ImageEntity.URL);
            imageQianZi.setType(ImageEntity.Data);
            if (zgsMattermaterialList.get(1).getZgsAttachappendixList().size() > 1) {
                imageQianZi1.setHeight(280);
                imageQianZi1.setWidth(580);
                filePath4 = upload + File.separator + zgsMattermaterialList.get(1).getZgsAttachappendixList().get(1).getAppendixpath();
                filePath4 = LibreOfficeUtil.wordConverterToJpg(filePath4);
                log.info("filePath4=" + filePath4);
                File file4 = new File(filePath4);
                if (file4.exists()) {
                    log.info("file4文件存在-yes");
                } else {
                    log.info("file4文件不存在-no");
                }
//                imageQianZi1.setUrl(filePath4);
                imageQianZi1.setData(FileUtil.readBytes(file4));
                //设置读取图⽚⽅式必须
//                imageQianZi1.setType(ImageEntity.URL);
                imageQianZi1.setType(ImageEntity.Data);
            } else {
                imageQianZi1 = null;
            }
        }
        templateMap.put("imageYiJian", imageYiJian);
        templateMap.put("imageYiJian1", imageYiJian1);
        templateMap.put("imageQianZi", imageQianZi);
        templateMap.put("imageQianZi1", imageQianZi1);
        //图片处理-end
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

    /**
     * @describe: 文件归档
     * @author: renxiaoliang
     * @date: 2024/1/12 17:03
     */
    @AutoLog(value = "项目文件归档")
    @ApiOperation(value = "项目文件归档", notes = "项目文件归档")
    @PostMapping(value = "/induce")
    public Result<?> fileInduce(@RequestBody Map<String,Object> params) {
        Result result = new Result();
        UpdateWrapper<ZgsExamplecertificatebase> updateWrapper = new UpdateWrapper<>();
        String id = params.get("id").toString();
        String fileState = params.get("fileState").toString();
        if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(fileState)) {
            updateWrapper.set("file_induce", fileState);
            updateWrapper.eq("id", id);
            boolean bol = zgsExamplecertificatebaseService.update(updateWrapper);
            if (fileState.equals("1") && bol) {
                result.setSuccess(true);
                result.setMessage("已归档");
            } else if (fileState.equals("2") && bol) {
                result.setSuccess(true);
                result.setMessage("取消归档");
            } else {
                result.setSuccess(false);
                result.setMessage("归档失败");
            }

        }

        return result;

    }


}
