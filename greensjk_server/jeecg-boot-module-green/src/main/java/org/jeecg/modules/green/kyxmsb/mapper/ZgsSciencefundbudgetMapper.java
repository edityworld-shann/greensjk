package org.jeecg.modules.green.kyxmsb.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科技攻关项目经费预算
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface ZgsSciencefundbudgetMapper extends BaseMapper<ZgsSciencefundbudget> {

}
