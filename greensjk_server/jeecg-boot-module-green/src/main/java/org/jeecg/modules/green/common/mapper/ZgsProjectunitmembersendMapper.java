package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 账号短信下发表
 * @Author: jeecg-boot
 * @Date:   2022-05-06
 * @Version: V1.0
 */
public interface ZgsProjectunitmembersendMapper extends BaseMapper<ZgsProjectunitmembersend> {

}
