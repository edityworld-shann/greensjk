package org.jeecg.modules.green.common.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.bean.BeanUtil;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 科技项目任务书申报
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Api(tags = "科技项目任务书申报")
@RestController
@RequestMapping("/common/zgsSciencetechtask")
@Slf4j
public class ZgsSciencetechtaskController extends JeecgController<ZgsSciencetechtask, IZgsSciencetechtaskService> {
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsSciencejointunitService zgsSciencejointunitService;
    @Autowired
    private IZgsTaskscienceparticipantService zgsTaskscienceparticipantService;
    @Autowired
    private IZgsTaskbuildfundbudgetService zgsTaskbuildfundbudgetService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    /**
     * 分页列表查询
     *
     * @param zgsSciencetechtask
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-分页列表查询")
    @ApiOperation(value = "科技项目任务书申报-分页列表查询", notes = "科技项目任务书申报-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsSciencetechtask zgsSciencetechtask,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "numArrow", required = false) Integer numArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsSciencetechtask> queryWrapper = new QueryWrapper<>();

        if (StringUtils.isNotEmpty(zgsSciencetechtask.getProjectname())) {
            queryWrapper.like("s.projectname", zgsSciencetechtask.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getProjectnum())) {
            queryWrapper.like("s.projectnum", zgsSciencetechtask.getProjectnum());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getFundType())) {
            String fundtype = zgsSciencetechtask.getFundType();
            // queryWrapper.eq("s.fund_type", fundtype);
            if (fundtype.equals("0")) {  // 省科技资金未支持
                queryWrapper.and(ft -> {
                    ft.eq("t.provincialfund", 0).or().eq("t.provincialfund",null);
                });
            } else { // 省科技资金支持，资金大于0
                queryWrapper.gt("t.provincialfund", 0);
            }
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getDealtype())) {
            if (!"-1".equals(zgsSciencetechtask.getDealtype())) {
                queryWrapper.eq("s.dealtype", zgsSciencetechtask.getDealtype());
            }
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(s.projectnum,3,4)", year);
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getProjectstage())) {
            if (!"-1".equals(zgsSciencetechtask.getProjectstage())) {
                queryWrapper.eq("s.projectstage", QueryWrapperUtil.getProjectstage(zgsSciencetechtask.getProjectstage()));
            }
        } else {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                queryWrapper.and(qrt -> {
                    qrt.eq("s.projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2)).or().isNull("s.projectstage");
                });
            }
        }
//        QueryWrapperUtil.initSciencetechfeasibleTaskSelect(queryWrapper, zgsSciencetechtask.getStatus(), sysUser);
        /*if (StringUtils.isBlank(zgsSciencetechtask.getStatus())) {
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsSciencetechtask.getStatus(), sysUser, 1);
        } else {
            QueryWrapperUtil.initCommonQueryWrapperForState(queryWrapper, zgsSciencetechtask.getStatus(), sysUser, 1);
        }*/


        //
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("s.status", GlobalConstants.SHENHE_STATUS0);
                    queryWrapper.and(qw -> {
                        qw.and(w1 -> {
                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
                                w1.or(w2 -> {
                                    w2.eq("s.enterpriseguid", projectunitmember.getEnterpriseguid());
                                });
                            }
                        });
                    });
                } else {
                    queryWrapper.eq("s.enterpriseguid", UUID.randomUUID().toString());
                }
//                else {
//                    queryWrapper.eq("enterpriseguid", "null");
//                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("s.enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //无专家审批
            }
        } else {
            queryWrapper.isNotNull("s.status");
//            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsSciencetechtask.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("s.finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("s.status", GlobalConstants.SHENHE_STATUS0).ne("s.status", GlobalConstants.SHENHE_STATUS1).ne("s.status", GlobalConstants.SHENHE_STATUS3);
                    });
                });
            }
        }
        //
        queryWrapper.ne("s.isdelete", 1);
        queryWrapper.isNotNull("s.sciencetechguid");
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("applydate");
            }
        } else {
            flag = false;
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("enddate");
            }
        } else {
            flag = false;
        }
        if (numArrow != null) {
            if (numArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("projectnum");
            } else if (numArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("projectnum");
            }
        } else {
            flag = false;
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("firstdate");
            }
        } else {
            flag = false;
        }
        if (flag) {

            queryWrapper.last("ORDER BY IF(isnull(s.createdate),0,1), createdate DESC");
            queryWrapper.last("ORDER BY IF(isnull(s.applydate),0,1), applydate DESC");
        } else {

            queryWrapper.last("ORDER BY IF(isnull(s.firstdate),0,1), firstdate DESC");
            queryWrapper.last("ORDER BY IF(isnull(s.applydate),0,1), applydate DESC");
        }
        // Page<ZgsSciencetechtask> page = new Page<ZgsSciencetechtask>(pageNo, pageSize);
        // IPage<ZgsSciencetechtask> pageList = zgsSciencetechtaskService.page(page, queryWrapper);
        // 关联 zgs_taskbuildfundbudget表  获取省科技经费金额字段信息，判断是否为省科技经费拨款项目

        // IPage<ZgsSciencetechtask> pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);

        // 推荐单位查看项目统计，状态为空
        IPage<ZgsSciencetechtask> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsSciencetechtask> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsSciencetechtask> pageList = null;

        // 科技项目任务书
        redisUtil.expire("kjxmrws",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kjxmrwsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        redisUtil.expire("kjxmrwsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

        // 默认初始进来 查询推荐单位待初审、项目统计数量
        if (StringUtils.isNotBlank(zgsSciencetechtask.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsSciencetechtask.getDspForTjdw())) {
            if (zgsSciencetechtask.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                if (!redisUtil.hasKey("kjxmrws")) {
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                    pageListForProjectCount = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);
                    redisUtil.set("kjxmrws",pageListForProjectCount.getTotal());
                }
            }
            if (zgsSciencetechtask.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                if (!redisUtil.hasKey("kjxmrwsOfTjdw")) {
                    QueryWrapperUtil.initCommonQueryWrapperForState(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
                    pageListForDsp = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);
                    redisUtil.set("kjxmrwsOfTjdw",pageListForDsp.getTotal());
                }
            }

        } else if (StringUtils.isNotBlank(zgsSciencetechtask.getDspForTjdw()) && StringUtils.isBlank(zgsSciencetechtask.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
            QueryWrapperUtil.initCommonQueryWrapperForState(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 1);
            pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);

        } else if (StringUtils.isBlank(zgsSciencetechtask.getDspForTjdw()) && StringUtils.isNotBlank(zgsSciencetechtask.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
            pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);

        } else if (StringUtils.isNotBlank(zgsSciencetechtask.getDspForSt()) && zgsSciencetechtask.getDspForSt().equals("2")) { // for 省厅待审批项目
            // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
            // 省厅查看待审批项目信息
            QueryWrapperUtil.initCommonQueryWrapperForState(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 1);
            pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);
            redisUtil.set("kjxmrwsOfDsp",pageList.getTotal());
            // }
        } else { // 正常菜单进入
            if (StringUtils.isBlank(zgsSciencetechtask.getStatus())) {
                QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 1);
                pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper,pageNo,pageSize);
            } else {
                QueryWrapperUtil.initCommonQueryWrapperForState(queryWrapper, zgsSciencetechtask.getStatus(), sysUser, 1);
                pageList = zgsSciencetechtaskService.listZgsSciencetechtask(queryWrapper, pageNo, pageSize);
            }
        }


        /*// 科技项目任务书
        redisUtil.expire("kjxmrws",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kjxmrws")) {
            if (StringUtils.isNotBlank(zgsSciencetechtask.getFlagByWorkTable()) && zgsSciencetechtask.getFlagByWorkTable().equals("1")) {
                redisUtil.set("kjxmrws",pageList.getTotal());
            }
        }
        redisUtil.expire("kjxmrwsOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kjxmrwsOfDsp")) {
            if (StringUtils.isNotBlank(zgsSciencetechtask.getDspForSt()) && zgsSciencetechtask.getDspForSt().equals("2")) {
                redisUtil.set("kjxmrwsOfDsp",pageList.getTotal());
            }
        }
        redisUtil.expire("kjxmrwsOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
        if (!redisUtil.hasKey("kjxmrwsOfTjdw")) {
            if (StringUtils.isNotBlank(zgsSciencetechtask.getDspForTjdw()) && zgsSciencetechtask.getDspForTjdw().equals("3")) {
                redisUtil.set("kjxmrwsOfTjdw",pageList.getTotal());
            }
        }*/

        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (pageList.getRecords() != null && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSciencetechtask zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    }
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsInfo.getPdfUrl());
                    if (sysUser.getEnterpriseguid().equals(zgsInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(zgsInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(zgsInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //无专家审批

                    }
                }
            }
        } else {
            if (pageList.getRecords() != null && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsSciencetechtask zgsInfo = pageList.getRecords().get(m);
                    pageList.getRecords().set(m, ValidateEncryptEntityUtil.validateDecryptObject(zgsInfo, ValidateEncryptEntityUtil.isDecrypt));
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    }
                    pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsInfo.getPdfUrl());
                    if (GlobalConstants.SHENHE_STATUS4.equals(zgsInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    }
                }
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 逾期临期分页列表查询
     *
     * @param zgsSciencetechtask
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "逾期临期-分页列表查询")
    @ApiOperation(value = "逾期临期-分页列表查询", notes = "逾期临期-分页列表查询")
    @GetMapping(value = "/YqLqlist")
    public Result<?> queryPageYqLqList(ZgsSciencetechtask zgsSciencetechtask,
                                       @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                       @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                       @RequestParam(name = "year", required = false) String year,
                                       @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                       @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                       @RequestParam(name = "numArrow", required = false) Integer numArrow,
                                       @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                       HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsSciencetechtask> queryWrapper = new QueryWrapper<>();
        /*if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_0 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_1) {  // 登录信息为个人或单位时只查看个人或单位自己的逾期、临期项目
            queryWrapper.eq("m.enterpriseguid",sysUser.getEnterpriseguid());
        }*/
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getProjectname())) {
            queryWrapper.like("m.projectname", zgsSciencetechtask.getProjectname());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getProjectnum())) {
            queryWrapper.like("m.projectnum", zgsSciencetechtask.getProjectnum());
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getFundType())) {
            String fundType = zgsSciencetechtask.getFundType();   // 0 省科技经费未支持   1 省科技经费支持
            // queryWrapper.eq("m.fund_type", fundType);
            if (fundType.equals("0")) {  // 省科技资金未支持
                queryWrapper.and(ft -> {
                    ft.eq("m.provincialfund", 0).or().eq("m.provincialfund",null);
                });
            } else { // 省科技资金支持，资金大于0
                queryWrapper.gt("m.provincialfund", 0);
            }
        }
        if (StringUtils.isNotEmpty(zgsSciencetechtask.getDealtype())) {
            if (!"-1".equals(zgsSciencetechtask.getDealtype())) {
                queryWrapper.eq("m.dealtype", zgsSciencetechtask.getDealtype());
            } else {
                queryWrapper.and(qwd -> {
                    qwd.eq("m.dealtype", 0).or().eq("m.dealtype", 1);
                });
            }
        } else {
            queryWrapper.and(qwd -> {
                qwd.eq("m.dealtype", 0).or().eq("m.dealtype", 1);
            });
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(m.applydate,'%Y')={0}", year);
            queryWrapper.eq("substring(m.projectnum,3,4)", year);
        }
        QueryWrapperUtil.initSciencetechfeasibleTaskSelect(queryWrapper, zgsSciencetechtask.getStatus(), sysUser);
        boolean flag = true;
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("m.applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("m.applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("m.enddate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("m.enddate");
            }
        }
        if (numArrow != null) {
            if (numArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("m.projectnum");
            } else if (numArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("m.projectnum");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("m.firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("m.firstdate");
            }
        }
        if (flag) {
          /*  queryWrapper.orderByDesc("createdate");
            queryWrapper.orderByDesc("applydate");*/
            queryWrapper.orderByDesc("m.firstdate");
            queryWrapper.orderByDesc("m.applydate");
        }
        Page<ZgsSciencetechtask> page = new Page<ZgsSciencetechtask>(pageNo, pageSize);
        String  enterpriseguid= sysUser.getEnterpriseguid();
        IPage<ZgsSciencetechtask> pageList = null;
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            pageList = zgsSciencetechtaskService.YqLqlist(page, queryWrapper,null);
        } else {
            pageList = zgsSciencetechtaskService.YqLqlist(page, queryWrapper,sysUser.getEnterpriseguid());
        }



        if (pageList.getRecords() != null && pageList.getRecords().size() > 0) {
            for (int m = 0; m < pageList.getRecords().size(); m++) {
                ZgsSciencetechtask zgsInfo = pageList.getRecords().get(m);
                //注释项目阶段最早的取值方法
//                pageList.getRecords().get(m).setCstatus(redisUtil.get(zgsInfo.getSciencetechguid()));
//                if (redisUtil.get(zgsInfo.getSciencetechguid()) == null) {
//                    pageList.getRecords().get(m).setCstatus("任务书阶段");
//                }
                if (StringUtils.isEmpty(zgsInfo.getProjectstage())) {
                    pageList.getRecords().get(m).setCstatus("任务书阶段");
                } else {
                    if ("中期查验阶段".equals(zgsInfo.getProjectstage())) {
                        QueryWrapper<ZgsMidterminspection> qwd = new QueryWrapper();
                        qwd.eq("projectlibraryguid", zgsInfo.getSciencetechguid());
                        ZgsMidterminspection midterminspection = zgsMidterminspectionService.getOne(qwd);
                        pageList.getRecords().get(m).setApplydate(midterminspection.getApplydate());
                        pageList.getRecords().get(m).setFirstdate(midterminspection.getFirstdate());
                        pageList.getRecords().get(m).setEnddate(midterminspection.getProjectenddate());
                    }
                }
                pageList.getRecords().get(m).setPdfUrl(viewloadUrl + zgsInfo.getPdfUrl());
                //省厅账号显示初审上报日期
//                pageList.getRecords().get(m).setApplydate(zgsInfo.getFirstdate());
            }
        }
        return Result.OK(pageList);
    }

    /**
     * 撤回
     *
     * @param zgsSciencetechtask
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-撤回")
    @ApiOperation(value = "科技项目任务书申报-撤回", notes = "科技项目任务书申报-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsSciencetechtask zgsSciencetechtask) {
        if (zgsSciencetechtask != null) {
            String id = zgsSciencetechtask.getId();
            String bid = zgsSciencetechtask.getSciencetechguid();
            String status = zgsSciencetechtask.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2).equals(zgsSciencetechtask.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    //更新各阶段状态
                    ZgsSciencetechtask info = new ZgsSciencetechtask();
                    info.setId(id);
                    info.setStatus(GlobalConstants.SHENHE_STATUS4);
                    info.setReturntype(null);
                    info.setAuditopinion(null);
                    info.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    info.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                    zgsSciencetechtaskService.updateById(info);
                    zgsProjectlibraryService.initStageAndStatus(bid);
                    //删除审批记录
                    zgsReturnrecordService.deleteReturnRecordLastLog(id);
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsSciencetechtask
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-审批")
    @ApiOperation(value = "科技项目任务书申报-审批", notes = "科技项目任务书申报-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsSciencetechtask zgsSciencetechtask) {
        zgsSciencetechtaskService.spProject(zgsSciencetechtask);
        //实时更新阶段和状态
        String projectlibraryguid = zgsSciencetechtaskService.getById(zgsSciencetechtask.getId()).getSciencetechguid();
        zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
        return Result.OK("审批成功！");
    }

    /**
     * 项目变更次数+1
     *
     * @param zgsSciencetechtask
     * @return
     */
    @AutoLog(value = "项目变更次数+1")
    @ApiOperation(value = "项目变更次数+1", notes = "项目变更次数+1")
    @PutMapping(value = "/changeBg")
    public Result<?> changeBg(@RequestBody ZgsSciencetechtask zgsSciencetechtask) {
        int status = zgsSciencetechtaskService.changeBg(zgsSciencetechtask);
        if (status == 0) {
//            return Result.error("首次变更无需操作！");
        }
        return Result.OK("变更次数+1成功！");
    }

    /**
     * 添加
     *
     * @param zgsSciencetechtask
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-添加")
    @ApiOperation(value = "科技项目任务书申报-添加", notes = "科技项目任务书申报-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsSciencetechtask zgsSciencetechtask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        zgsSciencetechtask.setId(id);
        if (GlobalConstants.handleSubmit.equals(zgsSciencetechtask.getStatus())) {
            //保存并上报
            zgsSciencetechtask.setApplydate(new Date());
        } else {
            zgsSciencetechtask.setApplydate(null);
        }
        if (StringUtils.isEmpty(zgsSciencetechtask.getSciencetechguid())) {
            return Result.error("信息填写不完整，提交失败！");
        }
        //查下是否已存在关联项目的新增操作
        if (zgsSciencetechtaskService.selectSciencetechExist(zgsSciencetechtask.getSciencetechguid())) {
            return Result.error("不能重复添加关联项目任务书！");
        }
        zgsSciencetechtask.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsSciencetechtask.setCreateaccountname(sysUser.getUsername());
        zgsSciencetechtask.setCreateusername(sysUser.getRealname());
        zgsSciencetechtask.setCreatedate(new Date());
        zgsSciencetechtask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
        zgsSciencetechtask.setProjectstagestatus(zgsSciencetechtask.getStatus());
        zgsSciencetechtaskService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencetechtask, ValidateEncryptEntityUtil.isEncrypt));
        //实时更新阶段和状态
        if (GlobalConstants.handleSubmit.equals(zgsSciencetechtask.getStatus()) && StringUtils.isNotEmpty(zgsSciencetechtask.getSciencetechguid())) {
            zgsProjectlibraryService.initStageAndStatus(zgsSciencetechtask.getSciencetechguid());
        }
        //修改申报项目位已立项状态
        ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
        zgsSciencetechfeasible.setId(zgsSciencetechtask.getSciencetechguid());
        zgsSciencetechfeasible.setAgreeproject(new BigDecimal(2));
        zgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible);
//        redisUtil.set(zgsSciencetechtask.getSciencetechguid(), "任务书阶段");
        //项目组成员
        List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList = zgsSciencetechtask.getZgsTaskparticipantList();
        if (zgsTaskscienceparticipantList != null && zgsTaskscienceparticipantList.size() > 0) {
            if (zgsTaskscienceparticipantList.size() > 15) {
                return Result.error("人员名单数量不得超过15人！");
            }
            for (int a0 = 0; a0 < zgsTaskscienceparticipantList.size(); a0++) {
                ZgsTaskscienceparticipant zgsTaskscienceparticipant = zgsTaskscienceparticipantList.get(a0);
                zgsTaskscienceparticipant.setId(UUID.randomUUID().toString());
                zgsTaskscienceparticipant.setTaskguid(id);
                zgsTaskscienceparticipant.setCreateaccountname(sysUser.getUsername());
                zgsTaskscienceparticipant.setCreateusername(sysUser.getRealname());
                zgsTaskscienceparticipant.setCreatedate(new Date());
                zgsTaskscienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskscienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //项目其他主要参加单位（注：跟主表共用一个因此不做新增）
        List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencetechtask.getZgsSciencejointunitList();
        if (zgsSciencejointunitList != null && zgsSciencejointunitList.size() > 0) {
            QueryWrapper<ZgsSciencejointunit> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("scienceguid", zgsSciencetechtask.getSciencetechguid());
            queryWrapper.eq("tag", "1");
            queryWrapper.ne("isdelete", 1);
            List<ZgsSciencejointunit> zgsSciencejointunitList1 = zgsSciencejointunitService.list(queryWrapper);
            if (zgsSciencejointunitList1 != null && zgsSciencejointunitList1.size() > 0) {
                for (ZgsSciencejointunit zgsSciencejointunit : zgsSciencejointunitList1) {
                    zgsSciencejointunitService.removeById(zgsSciencejointunit.getId());
                }
            }
            for (int a0 = 0; a0 < zgsSciencejointunitList.size(); a0++) {
                ZgsSciencejointunit zgsSciencejointunit = zgsSciencejointunitList.get(a0);
                zgsSciencejointunit.setId(UUID.randomUUID().toString());
                zgsSciencejointunit.setScienceguid(zgsSciencetechtask.getSciencetechguid());
                zgsSciencejointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSciencejointunit.setCreateaccountname(sysUser.getUsername());
                zgsSciencejointunit.setCreateusername(sysUser.getRealname());
                zgsSciencejointunit.setCreatedate(new Date());
                zgsSciencejointunit.setTag("1");
                zgsSciencejointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        //经费预算
        List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsSciencetechtask.getZgsTaskbuildfundbudgetList();
        if (zgsTaskbuildfundbudgetList != null && zgsTaskbuildfundbudgetList.size() > 0) {
            for (int a0 = 0; a0 < zgsTaskbuildfundbudgetList.size(); a0++) {
                ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(a0);
                zgsTaskbuildfundbudget.setId(UUID.randomUUID().toString());
                zgsTaskbuildfundbudget.setTaskguid(id);
                zgsTaskbuildfundbudget.setCreateaccountname(sysUser.getUsername());
                zgsTaskbuildfundbudget.setCreateusername(sysUser.getRealname());
                zgsTaskbuildfundbudget.setCreatedate(new Date());
                zgsTaskbuildfundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
            }
        }
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsSciencetechtask
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-编辑")
    @ApiOperation(value = "科技项目任务书申报-编辑", notes = "科技项目任务书申报-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsSciencetechtask zgsSciencetechtask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSciencetechtask != null && StringUtils.isNotEmpty(zgsSciencetechtask.getId())) {
            String id = zgsSciencetechtask.getId();
            if (StringUtils.isNotEmpty(zgsSciencetechtask.getEditStatus())) {
                //强制编辑
                zgsSciencetechtask.setEditStatus(null);
                //修改申报单位项目名称或负责人啥的
                if (StringUtils.isNotEmpty(zgsSciencetechtask.getSciencetechguid())) {
                    ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
                    zgsSciencetechfeasible.setId(zgsSciencetechtask.getSciencetechguid());
                    zgsSciencetechfeasible.setProjectname(zgsSciencetechtask.getProjectname());
                    zgsSciencetechfeasible.setFeeStatus(null);
                    zgsSciencetechfeasible.setProjectleader(zgsSciencetechtask.getProjectleader());
                    zgsSciencetechfeasibleService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencetechfeasible, ValidateEncryptEntityUtil.isEncrypt));
                }
            } else {
                if (GlobalConstants.handleSubmit.equals(zgsSciencetechtask.getStatus())) {
                    //保存并上报
                    zgsSciencetechtask.setApplydate(new Date());
                    zgsSciencetechtask.setAuditopinion(null);
                    zgsSciencetechtask.setReturntype(null);
                } else {
                    zgsSciencetechtask.setApplydate(null);
                    //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                    ZgsSciencetechtask projectTask = zgsSciencetechtaskService.getById(id);
                    if (StringUtils.isNotEmpty(projectTask.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(projectTask.getStatus())) {
                        zgsSciencetechtask.setStatus(GlobalConstants.SHENHE_STATUS3);
                    }
                }
            }
            zgsSciencetechtask.setModifyaccountname(sysUser.getUsername());
            zgsSciencetechtask.setModifyusername(sysUser.getRealname());
            zgsSciencetechtask.setModifydate(new Date());
//            zgsSciencetechtask.setReturntype(new BigDecimal(0));
            zgsSciencetechtask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
            zgsSciencetechtask.setProjectstagestatus(zgsSciencetechtask.getStatus());
            zgsSciencetechtaskService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencetechtask, ValidateEncryptEntityUtil.isEncrypt));
            //实时更新阶段和状态
            if (GlobalConstants.handleSubmit.equals(zgsSciencetechtask.getStatus()) && StringUtils.isNotEmpty(zgsSciencetechtask.getSciencetechguid())) {
                zgsProjectlibraryService.initStageAndStatus(zgsSciencetechtask.getSciencetechguid());
            }
            //强制更新限制细节修改开启
            if (StringUtils.isEmpty(zgsSciencetechtask.getEditStatus())) {
                //项目组成员
                List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList = zgsSciencetechtask.getZgsTaskparticipantList();
                QueryWrapper<ZgsTaskscienceparticipant> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.eq("taskguid", id);
                queryWrapper1.ne("isdelete", 1);
                queryWrapper1.orderByAsc("ordernum");
                List<ZgsTaskscienceparticipant> zgsTaskscienceparticipantList1 = zgsTaskscienceparticipantService.list(queryWrapper1);
                if (zgsTaskscienceparticipantList != null && zgsTaskscienceparticipantList.size() > 0) {
                    if (zgsTaskscienceparticipantList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsTaskscienceparticipantList.size(); a0++) {
                        ZgsTaskscienceparticipant zgsTaskscienceparticipant = zgsTaskscienceparticipantList.get(a0);
                        if (StringUtils.isNotEmpty(zgsTaskscienceparticipant.getId())) {
                            //编辑
                            zgsTaskscienceparticipant.setModifyaccountname(sysUser.getUsername());
                            zgsTaskscienceparticipant.setModifyusername(sysUser.getRealname());
                            zgsTaskscienceparticipant.setModifydate(new Date());
                            zgsTaskscienceparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskscienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                        } else {
                            //新增
                            zgsTaskscienceparticipant.setId(UUID.randomUUID().toString());
                            zgsTaskscienceparticipant.setTaskguid(id);
                            zgsTaskscienceparticipant.setCreateaccountname(sysUser.getUsername());
                            zgsTaskscienceparticipant.setCreateusername(sysUser.getRealname());
                            zgsTaskscienceparticipant.setCreatedate(new Date());
                            zgsTaskscienceparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskscienceparticipant, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsTaskscienceparticipant zgsTaskscienceparticipant1 : zgsTaskscienceparticipantList1) {
                            if (!zgsTaskscienceparticipant1.getId().equals(zgsTaskscienceparticipant.getId())) {
                                map0.put(zgsTaskscienceparticipant1.getId(), zgsTaskscienceparticipant1.getId());
                            } else {
                                list0.add(zgsTaskscienceparticipant1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsTaskscienceparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsTaskscienceparticipantList1 != null && zgsTaskscienceparticipantList1.size() > 0) {
                        for (ZgsTaskscienceparticipant zgsTaskscienceparticipant : zgsTaskscienceparticipantList1) {
                            zgsTaskscienceparticipantService.removeById(zgsTaskscienceparticipant.getId());
                        }
                    }
                }
                //项目其他主要参加单位（注：跟主表共用一个因此不做修改）
                List<ZgsSciencejointunit> zgsSciencejointunitList = zgsSciencetechtask.getZgsSciencejointunitList();
                QueryWrapper<ZgsSciencejointunit> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("scienceguid", zgsSciencetechtask.getSciencetechguid());
                queryWrapper.eq("tag", "1");
                queryWrapper.ne("isdelete", 1);
                List<ZgsSciencejointunit> zgsSciencejointunitList1 = zgsSciencejointunitService.list(queryWrapper);
                if (zgsSciencejointunitList != null && zgsSciencejointunitList.size() > 0) {
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsSciencejointunitList.size(); a0++) {
                        ZgsSciencejointunit zgsSciencejointunit = zgsSciencejointunitList.get(a0);
                        if (StringUtils.isNotEmpty(zgsSciencejointunit.getId())) {
                            //先查询一下判断类型
                            ZgsSciencejointunit sciencejointunit = zgsSciencejointunitService.getById(zgsSciencejointunit.getId());
                            if (sciencejointunit != null && StringUtils.isNotEmpty(sciencejointunit.getTag())) {
                                //编辑
                                zgsSciencejointunit.setModifyaccountname(sysUser.getUsername());
                                zgsSciencejointunit.setModifyusername(sysUser.getRealname());
                                zgsSciencejointunit.setModifydate(new Date());
                                zgsSciencejointunitService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
                            } else {
                                //新增
                                zgsSciencejointunit.setId(UUID.randomUUID().toString());
                                zgsSciencejointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsSciencejointunit.setScienceguid(zgsSciencetechtask.getSciencetechguid());
                                zgsSciencejointunit.setCreateaccountname(sysUser.getUsername());
                                zgsSciencejointunit.setCreateusername(sysUser.getRealname());
                                zgsSciencejointunit.setCreatedate(new Date());
                                zgsSciencejointunit.setTag("1");
                                zgsSciencejointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
                            }
                        } else {
                            //新增
                            zgsSciencejointunit.setId(UUID.randomUUID().toString());
                            zgsSciencejointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                            zgsSciencejointunit.setScienceguid(zgsSciencetechtask.getSciencetechguid());
                            zgsSciencejointunit.setCreateaccountname(sysUser.getUsername());
                            zgsSciencejointunit.setCreateusername(sysUser.getRealname());
                            zgsSciencejointunit.setCreatedate(new Date());
                            zgsSciencejointunit.setTag("1");
                            zgsSciencejointunitService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSciencejointunit, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsSciencejointunit zgsSciencejointunit1 : zgsSciencejointunitList1) {
                            if (!zgsSciencejointunit1.getId().equals(zgsSciencejointunit.getId())) {
                                map0.put(zgsSciencejointunit1.getId(), zgsSciencejointunit1.getId());
                            } else {
                                list0.add(zgsSciencejointunit1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsSciencejointunitService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsSciencejointunitList1 != null && zgsSciencejointunitList1.size() > 0) {
                        for (ZgsSciencejointunit zgsSciencejointunit : zgsSciencejointunitList1) {
                            zgsSciencejointunitService.removeById(zgsSciencejointunit.getId());
                        }
                    }
                }
                //经费预算
                List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsSciencetechtask.getZgsTaskbuildfundbudgetList();
                QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.eq("taskguid", id);
                queryWrapper2.ne("isdelete", 1);
                queryWrapper2.orderByAsc("year");
                List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList1 = zgsTaskbuildfundbudgetService.list(queryWrapper2);
                if (zgsTaskbuildfundbudgetList != null && zgsTaskbuildfundbudgetList.size() > 0) {
                    Map<String, String> map0 = new HashMap<>();
                    List<String> list0 = new ArrayList<>();
                    for (int a0 = 0; a0 < zgsTaskbuildfundbudgetList.size(); a0++) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(a0);
                        if (StringUtils.isNotEmpty(zgsTaskbuildfundbudget.getId())) {
                            //编辑
                            zgsTaskbuildfundbudget.setModifyaccountname(sysUser.getUsername());
                            zgsTaskbuildfundbudget.setModifyusername(sysUser.getRealname());
                            zgsTaskbuildfundbudget.setModifydate(new Date());
                            zgsTaskbuildfundbudgetService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
                        } else {
                            //新增
                            zgsTaskbuildfundbudget.setId(UUID.randomUUID().toString());
                            zgsTaskbuildfundbudget.setTaskguid(id);
                            zgsTaskbuildfundbudget.setCreateaccountname(sysUser.getUsername());
                            zgsTaskbuildfundbudget.setCreateusername(sysUser.getRealname());
                            zgsTaskbuildfundbudget.setCreatedate(new Date());
                            zgsTaskbuildfundbudgetService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsTaskbuildfundbudget, ValidateEncryptEntityUtil.isEncrypt));
                        }
                        for (ZgsTaskbuildfundbudget zgsTaskbuildfundbudget1 : zgsTaskbuildfundbudgetList1) {
                            if (!zgsTaskbuildfundbudget1.getId().equals(zgsTaskbuildfundbudget.getId())) {
                                map0.put(zgsTaskbuildfundbudget1.getId(), zgsTaskbuildfundbudget1.getId());
                            } else {
                                list0.add(zgsTaskbuildfundbudget1.getId());
                            }
                        }
                    }
                    map0.keySet().removeAll(list0);
                    zgsTaskbuildfundbudgetService.removeByIds(new ArrayList<>(map0.keySet()));
                } else {
                    if (zgsTaskbuildfundbudgetList1 != null && zgsTaskbuildfundbudgetList1.size() > 0) {
                        for (ZgsTaskbuildfundbudget zgsTaskbuildfundbudget : zgsTaskbuildfundbudgetList1) {
                            zgsTaskbuildfundbudgetService.removeById(zgsTaskbuildfundbudget.getId());
                        }
                    }
                }
            }
        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-通过id删除")
    @ApiOperation(value = "科技项目任务书申报-通过id删除", notes = "科技项目任务书申报-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsSciencetechtaskService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-批量删除")
    @ApiOperation(value = "科技项目任务书申报-批量删除", notes = "科技项目任务书申报-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsSciencetechtaskService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-通过id查询")
    @ApiOperation(value = "科技项目任务书申报-通过id查询", notes = "科技项目任务书申报-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id) {
        ZgsSciencetechtask zgsSciencetechtask = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsSciencetechtask = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechtaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsSciencetechtask == null) {
                return Result.error("未找到对应数据");
            } else {
                zgsSciencetechtaskService.initProjectSelectById(zgsSciencetechtask);
            }
            if ((zgsSciencetechtask.getFirstdate() != null || zgsSciencetechtask.getFinishdate() != null) && zgsSciencetechtask.getApplydate() != null) {
                zgsSciencetechtask.setSpLogStatus(1);
            } else {
                zgsSciencetechtask.setSpLogStatus(0);
            }
        } else {
            //新增时查出附件
            zgsSciencetechtask = new ZgsSciencetechtask();
            zgsSciencetechtask.setSpLogStatus(0);
        }
        return Result.OK(zgsSciencetechtask);
    }

    /**
     * 通过sciencetechguid查询
     *
     * @param sciencetechguid
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-通过sciencetechguid关联查询")
    @ApiOperation(value = "科技项目任务书申报-通过sciencetechguid关联查询", notes = "科技项目任务书申报-通过sciencetechguid关联查询")
    @GetMapping(value = "/queryBySciencetechGuid")
    public Result<?> queryBySciencetechGuid(@RequestParam(name = "sciencetechguid", required = true) String sciencetechguid, @RequestParam(name = "accessType", required = false) Integer accessType) {
        ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getBySciencetechguid(sciencetechguid, accessType);
        if (zgsSciencetechtask == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsSciencetechtask);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsSciencetechtask
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsSciencetechtask zgsSciencetechtask,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "numArrow", required = false) Integer numArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsSciencetechtask, 1, 9999, year, sbArrow, wcArrow, csArrow, numArrow, req);
        IPage<ZgsSciencetechtask> pageList = (IPage<ZgsSciencetechtask>) result.getResult();
        List<ZgsSciencetechtask> list = pageList.getRecords();
        List<ZgsSciencetechtask> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        String title = "科技项目任务书";
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsSciencetechtask.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        return mv;
    }

    private String getId(ZgsSciencetechtask item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSciencetechtask.class);
    }


    /**
     * 通过id查询生成任务书
     *
     * @param id
     * @return
     */
    @AutoLog(value = "科技项目任务书申报-通过id查询生成任务书")
    @ApiOperation(value = "科技项目任务书申报-通过id查询生成任务书", notes = "科技项目任务书申报-通过id查询生成任务书")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsSciencetechtask zgsSciencetechtask = ValidateEncryptEntityUtil.validateDecryptObject(zgsSciencetechtaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsSciencetechtask != null) {
            zgsSciencetechtaskService.initProjectSelectById(zgsSciencetechtask);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "9科技项目任务书.docx";
        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsSciencetechtask);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }

}
