package org.jeecg.modules.green.common.service;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 共用 服务类接口
 * </p>
 *
 * @Author lizhi
 * @since 2022-04-12
 */
public interface IZgsCommonService extends IService<Object> {

    public String queryDictTextByKey(String code, String key);

    public String queryAreaTextByCode(String code);

    public String queryApplyYear();

}
