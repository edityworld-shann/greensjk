package org.jeecg.modules.green.jxzpsq.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 计划项目执行情况绩效自评表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Data
@TableName("zgs_performancescore")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_performancescore对象", description = "计划项目执行情况绩效自评表")
public class ZgsPerformancescore implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 绩效自评申请guid
     */
    @Excel(name = "绩效自评申请guid", width = 15)
    @ApiModelProperty(value = "绩效自评申请guid")
    private java.lang.String baseguid;
    /**
     * 企业ID
     */
    @Excel(name = "企业ID", width = 15)
    @ApiModelProperty(value = "企业ID")
    private java.lang.String enterpriseguid;
    /**
     * 项目设立依据分数
     */
    @Excel(name = "项目设立依据分数", width = 15)
    @ApiModelProperty(value = "项目设立依据分数")
    private java.math.BigDecimal gistscore;
    /**
     * 项目设立依据备注
     */
    @Excel(name = "项目设立依据备注", width = 15)
    @ApiModelProperty(value = "项目设立依据备注")
    private java.lang.String gistremark;
    /**
     * 项目预定目标分数
     */
    @Excel(name = "项目预定目标分数", width = 15)
    @ApiModelProperty(value = "项目预定目标分数")
    private java.math.BigDecimal targetscore;
    /**
     * 项目预定目标备注
     */
    @Excel(name = "项目预定目标备注", width = 15)
    @ApiModelProperty(value = "项目预定目标备注")
    private java.lang.String targetremark;
    /**
     * 组织管理制度分数
     */
    @Excel(name = "组织管理制度分数", width = 15)
    @ApiModelProperty(value = "组织管理制度分数")
    private java.math.BigDecimal managesystemscore;
    /**
     * 组织管理制度备注
     */
    @Excel(name = "组织管理制度备注", width = 15)
    @ApiModelProperty(value = "组织管理制度备注")
    private java.lang.String managesystemremark;
    /**
     * 项目管理分数
     */
    @Excel(name = "项目管理分数", width = 15)
    @ApiModelProperty(value = "项目管理分数")
    private java.math.BigDecimal proejctmanagescore;
    /**
     * 项目管理备注
     */
    @Excel(name = "项目管理备注", width = 15)
    @ApiModelProperty(value = "项目管理备注")
    private java.lang.String proejctmanageremark;
    /**
     * 项目实施分数
     */
    @Excel(name = "项目实施分数", width = 15)
    @ApiModelProperty(value = "项目实施分数")
    private java.math.BigDecimal proejctimplementscore;
    /**
     * 项目实施备注
     */
    @Excel(name = "项目实施备注", width = 15)
    @ApiModelProperty(value = "项目实施备注")
    private java.lang.String proejctimplementremark;
    /**
     * 资料是否齐全分数
     */
    @Excel(name = "资料是否齐全分数", width = 15)
    @ApiModelProperty(value = "资料是否齐全分数")
    private java.math.BigDecimal materialscore;
    /**
     * 资料是否齐全备注
     */
    @Excel(name = "资料是否齐全备注", width = 15)
    @ApiModelProperty(value = "资料是否齐全备注")
    private java.lang.String materialremark;
    /**
     * 项目成果分数
     */
    @Excel(name = "项目成果分数", width = 15)
    @ApiModelProperty(value = "项目成果分数")
    private java.math.BigDecimal projectresultscore;
    /**
     * 项目成果备注
     */
    @Excel(name = "项目成果备注", width = 15)
    @ApiModelProperty(value = "项目成果备注")
    private java.lang.String projectresultremark;
    /**
     * 制度的健全性分数
     */
    @Excel(name = "制度的健全性分数", width = 15)
    @ApiModelProperty(value = "制度的健全性分数")
    private java.math.BigDecimal regimesanescore;
    /**
     * 制度的健全性备注
     */
    @Excel(name = "制度的健全性备注", width = 15)
    @ApiModelProperty(value = "制度的健全性备注")
    private java.lang.String regimesaneremark;
    /**
     * 管理的规范性分数
     */
    @Excel(name = "管理的规范性分数", width = 15)
    @ApiModelProperty(value = "管理的规范性分数")
    private java.math.BigDecimal regimenormscore;
    /**
     * 管理的规范性备注
     */
    @Excel(name = "管理的规范性备注", width = 15)
    @ApiModelProperty(value = "管理的规范性备注")
    private java.lang.String regimenormremark;
    /**
     * 成本控制情况分数
     */
    @Excel(name = "成本控制情况分数", width = 15)
    @ApiModelProperty(value = "成本控制情况分数")
    private java.math.BigDecimal costcontrolscore;
    /**
     * 成本控制情况备注
     */
    @Excel(name = "成本控制情况备注", width = 15)
    @ApiModelProperty(value = "成本控制情况备注")
    private java.lang.String costcontrolremark;
    /**
     * 预算编制情况分数
     */
    @Excel(name = "预算编制情况分数", width = 15)
    @ApiModelProperty(value = "预算编制情况分数")
    private java.math.BigDecimal budgetscore;
    /**
     * 预算编制情况备注
     */
    @Excel(name = "预算编制情况备注", width = 15)
    @ApiModelProperty(value = "预算编制情况备注")
    private java.lang.String budgetremark;
    /**
     * 配套资金情况分数
     */
    @Excel(name = "配套资金情况分数", width = 15)
    @ApiModelProperty(value = "配套资金情况分数")
    private java.math.BigDecimal matchfundscore;
    /**
     * 配套资金情况备注
     */
    @Excel(name = "配套资金情况备注", width = 15)
    @ApiModelProperty(value = "配套资金情况备注")
    private java.lang.String matchfundremark;
    /**
     * 预算支出情况分数
     */
    @Excel(name = "预算支出情况分数", width = 15)
    @ApiModelProperty(value = "预算支出情况分数")
    private java.math.BigDecimal budgetoutscore;
    /**
     * 预算支出情况备注
     */
    @Excel(name = "预算支出情况备注", width = 15)
    @ApiModelProperty(value = "预算支出情况备注")
    private java.lang.String budgetoutremark;
    /**
     * 预算调整情况分数
     */
    @Excel(name = "预算调整情况分数", width = 15)
    @ApiModelProperty(value = "预算调整情况分数")
    private java.math.BigDecimal budgetadjustscore;
    /**
     * 预算调整情况备注
     */
    @Excel(name = "预算调整情况备注", width = 15)
    @ApiModelProperty(value = "预算调整情况备注")
    private java.lang.String budgetadjustremark;
    /**
     * 资金到位情况分数
     */
    @Excel(name = "资金到位情况分数", width = 15)
    @ApiModelProperty(value = "资金到位情况分数")
    private java.math.BigDecimal fullyfundscore;
    /**
     * 资金到位情况备注
     */
    @Excel(name = "资金到位情况备注", width = 15)
    @ApiModelProperty(value = "资金到位情况备注")
    private java.lang.String fullyfundremark;
    /**
     * 资金使用情况分数
     */
    @Excel(name = "资金使用情况分数", width = 15)
    @ApiModelProperty(value = "资金使用情况分数")
    private java.math.BigDecimal fundusescore;
    /**
     * 资金使用情况备注
     */
    @Excel(name = "资金使用情况备注", width = 15)
    @ApiModelProperty(value = "资金使用情况备注")
    private java.lang.String funduseremark;
    /**
     * 支出的合规性分数
     */
    @Excel(name = "支出的合规性分数", width = 15)
    @ApiModelProperty(value = "支出的合规性分数")
    private java.math.BigDecimal expendscore;
    /**
     * 支出的合规性备注
     */
    @Excel(name = "支出的合规性备注", width = 15)
    @ApiModelProperty(value = "支出的合规性备注")
    private java.lang.String expendremark;
    /**
     * createaccountname
     */
    @Excel(name = "createaccountname", width = 15)
    @ApiModelProperty(value = "createaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createaccountname;
    /**
     * createusername
     */
    @Excel(name = "createusername", width = 15)
    @ApiModelProperty(value = "createusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String createusername;
    /**
     * createdate
     */
    @Excel(name = "createdate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "createdate")
    private java.util.Date createdate;
    /**
     * modifyaccountname
     */
    @Excel(name = "modifyaccountname", width = 15)
    @ApiModelProperty(value = "modifyaccountname")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyaccountname;
    /**
     * modifyusername
     */
    @Excel(name = "modifyusername", width = 15)
    @ApiModelProperty(value = "modifyusername")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String modifyusername;
    /**
     * modifydate
     */
    @Excel(name = "modifydate", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @ApiModelProperty(value = "modifydate")
    private java.util.Date modifydate;
    /**
     * 是否删除,1删除
     */
    @Excel(name = "是否删除,1删除", width = 15)
    @ApiModelProperty(value = "是否删除,1删除")
    private java.math.BigDecimal isdelete = new BigDecimal(0);
    /**
     * 审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过
     */
    @Excel(name = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过", width = 15)
    @ApiModelProperty(value = "审核状态：0未上报，1待审核，4初审通过，3退回，2终审通过")
    private java.lang.String status;
    /**
     * 总分
     */
    @Excel(name = "总分", width = 15)
    @ApiModelProperty(value = "总分")
    private java.math.BigDecimal totalscore;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
