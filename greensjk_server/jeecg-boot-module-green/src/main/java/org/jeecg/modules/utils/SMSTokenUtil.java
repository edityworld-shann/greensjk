package org.jeecg.modules.utils;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.httputil.HttpHeader;
import org.jeecg.common.httputil.HttpParamers;
import org.jeecg.common.httputil.HttpService;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.sms.service.SMSTokenUtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * 专门为短信发送获取Token 工具类
 *
 * @Author Scott
 */
@Service
@Slf4j
public class SMSTokenUtil implements SMSTokenUtilService {

    @Value("${jeecg.smsBaseUrl}")
    private String smsBaseUrl;
    @Value("${jeecg.clientId}")
    private String client_id;
    @Value("${jeecg.clientSecret}")
    private String client_secret;
    @Autowired
    private IZgsSmsSendingRecordLogService smsLogsService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsSmsSendingRecordLogService smsLogsServiceParam;

    /**
     * 专门为短信发送获取Token
     *
     * @param client_id     应用唯一标识
     * @param client_secret 应用标识 对应秘钥
     * @return
     */
    public String getTokenForSMS(String client_id, String client_secret) {
        try {
            //	================== 模拟参数 begin ====================================================================
            Map<String, String> mapToken = new HashMap<String, String>();
            //	省略 client_id、client_secret 加密获取 token 的过程......
            String access_token = "bHNqbmp6OmI2ZjI0MDc3LTc3MjctNDAyMC1iZGM5LWFjNjhiOGRjMWM0Zg==";
            mapToken.put("isToken", "false");
            mapToken.put("Authorization", "Basic " + access_token);
            HttpHeader httpHeaderUser = new HttpHeader(mapToken);
            //	================== 模拟参数 end ======================================================================
            HttpParamers httpParamers = HttpParamers.httpPostParamers();
            String usl = "auth/oauth/token?scope=server&grant_type=client_credentials";
            HttpService httpServiceUser = new HttpService(smsBaseUrl);
            String resultUser = httpServiceUser.service(usl, httpParamers, httpHeaderUser);
            return resultUser;
        } catch (Exception e) {
            e.printStackTrace();
            return "获取token失败！";
        }
    }

    /**
     * 短信发送-获取验证码（单发、同步数据库记录）
     * <p>
     * smsParams Json参数
     * phoneNumber 手机号
     * smsType 短信类型
     * templateId 模板id
     * fillContent 短信填充内容
     * verifyCode 短信变量
     *
     * @return Result
     */
    public Result<?> sendSMSService(String smsParams) {
        try {
            //	================== 解析入参 begin ========================================================================
            log.info("smsParams>" + smsParams);
            JSONObject smsParamsObject = JSON.parseObject(smsParams);
            log.info("smsParams>parseObject");
            String phoneNumber = smsParamsObject.get("phoneNumber").toString();
            log.info("smsParams>phoneNumber");
            String smsType = smsParamsObject.get("smsType").toString();
            log.info("smsParams>smsType");
            Object oName = smsParamsObject.get("companyName");
            log.info("smsParams>companyName");
            String companyName = "系统管理员";
            if (oName != null) {
                companyName = oName.toString();
            }
            Object typeOb = smsParamsObject.get("type");
            log.info("smsParams>type");
            boolean flag = true;
            if (typeOb != null) {
                if ("0".equals(typeOb.toString())) {
//                    flag = false;
                }
            }
            String templateId = smsParamsObject.get("templateId").toString();
            log.info("smsParams>templateId");
            Map fillContent = (Map) smsParamsObject.get("fillContent");
            log.info("smsParams>fillContent");
            //	================== 解析入参 end ==========================================================================
            //	================== 根据模板类型，拆分业务代码 begin ==========================================================================
            String captcha = RandomUtil.randomNumbers(6);    //	随机数
            String fillContentJsonString = smsParamsObject.getJSONObject("fillContent").toString();
            log.info("smsParams>fillContent-1");
//            String verifyCode = "短信发送成功，（非验证码业务）...";
            String verifyCode = captcha;
            Object captchaOb = smsParamsObject.get("captcha");
            log.info("smsParams>captcha");
            if (captchaOb != null) {
                verifyCode = captchaOb.toString();
            }
            String operation = "";
            String sendingRecord = "";
            String month = "";
            String day = "";
            String projectName = "";
            String year = "";
            String time = "";
            String location = "";
            String locationName = "";
            String accountname = "";
            String initPassword = "";
            String QQNum = "";
            String username = "";
            String content = "";
            String content1 = "";
            String content2 = "";
            String content3 = "";
            Gson gson = new Gson();
            if ("9".equals(templateId) || "13".equals(templateId)) {
//                verifyCode = captcha;
                fillContent.put("verifyCode", verifyCode);
                sendingRecord = "您的短信验证码是" + verifyCode + "。您正在通过手机号重置登录密码，如非本人操作，请忽略该短信。";
            } else {
                if (flag) {
                    if ("10".equals(templateId) || "15".equals(templateId)) {
                        operation = gson.fromJson(fillContentJsonString, Map.class).get("operation").toString();
                        if ("10".equals(templateId)) {
                            sendingRecord = "您已被" + operation + "黑名单，请登录系统查看详情。";
                        } else if ("15".equals(templateId)) {
                            sendingRecord = "账号注册申请" + operation + "。";
                        }
                    } else if ("11".equals(templateId)) {
                        projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                        month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                        day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                        sendingRecord = projectName + "项目将于" + month + "月" + day + "日审批超时，请及时登录系统进行审批。";
                    } else if ("12".equals(templateId)) {
                        projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                        year = gson.fromJson(fillContentJsonString, Map.class).get("year").toString();
                        month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                        day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                        time = gson.fromJson(fillContentJsonString, Map.class).get("time").toString();
                        location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                        locationName = gson.fromJson(fillContentJsonString, Map.class).get("locationName").toString();
                        sendingRecord = "您被抽取为" + projectName + "项目验收评审，请于" + year + "年" + month + "月" + day + "日" + time + "时刻" + location +
                                "地参加线下项目验收评审会，地址" + locationName + "确认参加评审请回复“1”，不能参加评审请回复“2”。";
                    } else if ("14".equals(templateId)) {
                        projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                        sendingRecord = "您被抽取为" + projectName + "项目验收评审，请及时登录系统进行项目评审，确认评审请回复“1”，不能评审请回复“2”。";
                    } else if ("19".equals(templateId)) {
                        location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                        month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                        day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                        accountname = gson.fromJson(fillContentJsonString, Map.class).get("accountname").toString();
                        username = gson.fromJson(fillContentJsonString, Map.class).get("username").toString();
                        initPassword = gson.fromJson(fillContentJsonString, Map.class).get("initPassword").toString();
                        QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                        sendingRecord = "您好，现甘肃省住建厅科技项目申报系统全面升级改造已完成，根据《甘肃省住房和城乡建设厅关于印发2022年建设科技项目计划的通知》，请您尽快通过地址：" +
                                location + "，于" + month + "月" + day + "日之前进行任务书填报及后续工作，过期视为自动放弃。您的所属单位/个人账户名为：" + accountname + "（单位）/"
                                + username + "（姓名）”，初始密码为 “" + initPassword + "”。请您第一时间进行初始密码修改，并妥善保管密码（如密码遗失，须单位以正" +
                                "式文件形式向我厅申请并经同意后方可重置），避免对您的业务造成不必要的损失。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                    } else if ("20".equals(templateId)) {
                        content1 = gson.fromJson(fillContentJsonString, Map.class).get("content1").toString();
                        content2 = gson.fromJson(fillContentJsonString, Map.class).get("content2").toString();
                        content3 = gson.fromJson(fillContentJsonString, Map.class).get("content3").toString();
                        month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                        day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                        QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                        sendingRecord = "您好，" + content1 + "，请" + content2 + "，现通知到您，为避免对您的业务造成不必要的损失，请您于" + month + "月" + day + "日之前" + content3 + "。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                    } else if ("24".equals(templateId)) {
                        content = gson.fromJson(fillContentJsonString, Map.class).get("content").toString();
                        sendingRecord = "请" + content + "单位保留中期查验过程中所有影像资料。";
                    } else if ("26".equals(templateId)) {
                        log.info("smsParams>26");
                        projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                        String date = gson.fromJson(fillContentJsonString, Map.class).get("date").toString();
                        sendingRecord = "您好，项目名称：" + projectName + "，形审通过！按通知文件要求报送纸质版评审材料。EMS形式寄送。（截止时间" + date + "）";
                        log.info(sendingRecord);
                    } else if ("27".equals(templateId)) {
                        log.info("smsParams>27");
                        String title = gson.fromJson(fillContentJsonString, Map.class).get("title").toString();
                        String date = gson.fromJson(fillContentJsonString, Map.class).get("date").toString();
                        sendingRecord = "专家您好，请在" + date + "前登录“甘肃省建设科技与建筑节能信息系统”，按照" + title + "通知要求，完善您的专家库个人信息，谢谢！";
                        log.info(sendingRecord);
                    }
                }
            }
            //	================== 根据模板类型，拆分业务代码 end ==========================================================================
            //	================== 参数封装 begin ========================================================================
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            String access_token = "";    //	系统登录 token
            String tokenForSMS = "";    //	短信发送，专用 token
            Object checkCode = redisUtil.get(CommonConstant.SMS_TOKEN);        //	判断 redis 缓存中是否存在token，如果
//            if (checkCode == null) {
            tokenForSMS = this.getTokenForSMS(client_id, client_secret);
            JSONObject paramJson = JSON.parseObject(tokenForSMS);
            access_token = paramJson.get("access_token").toString();
            redisUtil.set(CommonConstant.SMS_TOKEN, tokenForSMS, CommonConstant.SMS_TOKEN_EXPIRE_TIME);
//            } else {
//                JSONObject paramJson = JSON.parseObject(checkCode.toString());
//                access_token = paramJson.get("access_token").toString();
//            }
            //	================== 参数封装 end ==========================================================================
            //	================== 调用短信发送接口 begin ==================================================================
            HashMap<String, Object> headers = new HashMap<>(10);
            HashMap<String, Object> params = new HashMap<>(10);
            headers.put("Authorization", "Bearer " + access_token);
            params.put("phoneNumber", phoneNumber);
            params.put("smsType", smsType);
            params.put("templateId", templateId);
            //	JSONObject json = new JSONObject(fillContent);	//	使用 fastjson 将 内层对象转为 json 格式
            params.put("fillContent", fillContent);
            String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(params));
            //	================== 调用短信发送接口 end ====================================================================
            //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 begin ========================================
            log.info("after-" + resultUser);
            JSONObject resultJson = JSON.parseObject(resultUser);
            Map resultMap = new HashMap();
            if ("00000".equals(resultJson.get("code"))) {
                resultMap.put("id", uuid);
                resultMap.put("verificationcode", verifyCode);
                resultMap.put("phoneNumber", phoneNumber);
                //	================== 同步信息发送记录表 begin ============================================================
                ZgsSmsSendingRecordLog smsLogs = new ZgsSmsSendingRecordLog();
                smsLogs.setId(uuid);
                smsLogs.setPhonenumber(phoneNumber);
                smsLogs.setSendingrecord(sendingRecord);
                LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
                if (user != null) {
                    smsLogs.setUsername(user.getRealname());
                    smsLogs.setSendingAccount(user.getUsername());
                }
                smsLogs.setVerificationcode(verifyCode);
                smsLogs.setReceiveCompany(companyName);
                smsLogs.setStatus("1");
                //	通过Date类来获取当前时间，通过SimpleDateFormat来设置时间格式
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date currentDate = new Date();
                String currentTime = dateFormat.format(currentDate);
                smsLogs.setCreatetime(currentTime);
                smsLogsServiceParam.save(ValidateEncryptEntityUtil.validateEncryptObject(smsLogs, ValidateEncryptEntityUtil.isEncrypt));
                return Result.OK(resultMap);
                //	================== 同步信息发送记录表 end ==============================================================
                //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 end =====================================
            } else if ("D0624".equals(resultJson.get("code"))) {
                return Result.error("同一号码相同内容发送次数太多（默认24小时内，验证码类发送5次或相同内容3次以上会报此错误。");
            } else {
                return Result.error(resultJson.get("msg").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("信息错误");
        }
    }

    /**
     * 短信发送-获取验证码（群发、不同步数据库记录【考虑数据库压力，群发，的通过日志看记录】、需要 手动 书写循环赋值逻辑 ）
     * <p>
     * smsParams Json参数
     * phoneNumber 手机号
     * smsType 短信类型
     * templateId 模板id
     * fillContent 短信填充内容
     * verifyCode 短信变量
     *
     * @return Result
     */
    public Result<?> sendSMSBatchService(String smsParams) {
        try {
            //	================== 解析入参 begin ========================================================================
            JSONObject smsParamsObject = JSON.parseObject(smsParams);
            String phoneNumber = smsParamsObject.get("phoneNumber").toString();
            String smsType = smsParamsObject.get("smsType").toString();
            String templateId = smsParamsObject.get("templateId").toString();
            Map fillContent = (Map) smsParamsObject.get("fillContent");
            //	================== 解析入参 end ==========================================================================
            //	================== 根据模板类型，拆分业务代码 begin ==========================================================================
            String captcha = RandomUtil.randomNumbers(6);    //	随机数
            String verifyCode = "短信发送成功，（非验证码业务）...";
            if ("9".equals(templateId) || "13".equals(templateId)) {
                verifyCode = captcha;
                fillContent.put("verifyCode", verifyCode);
            }
            //	================== 根据模板类型，拆分业务代码 end ==========================================================================
            //	================== 参数封装 begin ========================================================================
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            String access_token = "";    //	系统登录 token
            String tokenForSMS = "";    //	短信发送，专用 token
            System.out.println("redis: =========: " + redisUtil);
            Object checkCode = redisUtil.get(CommonConstant.SMS_TOKEN);        //	判断 redis 缓存中是否存在token，如果
            if (checkCode == null) {
                tokenForSMS = this.getTokenForSMS(client_id, client_secret);
                JSONObject paramJson = JSON.parseObject(tokenForSMS);
                access_token = paramJson.get("access_token").toString();
                redisUtil.set(CommonConstant.SMS_TOKEN, tokenForSMS);
                redisUtil.expire(CommonConstant.SMS_TOKEN, CommonConstant.SMS_TOKEN_EXPIRE_TIME);
            } else {
                JSONObject paramJson = JSON.parseObject(checkCode.toString());
                access_token = paramJson.get("access_token").toString();
            }
            //	================== 参数封装 end ==========================================================================
            //	================== 调用短信发送接口 begin ==================================================================
            HashMap<String, Object> headers = new HashMap<>(10);
            HashMap<String, Object> params = new HashMap<>(10);
            headers.put("Authorization", "Bearer " + access_token);
            params.put("phoneNumber", phoneNumber);
            params.put("smsType", smsType);
            params.put("templateId", templateId);
            //	JSONObject json = new JSONObject(fillContent);	//	使用 fastjson 将 内层对象转为 json 格式
            params.put("fillContent", fillContent);
            String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(params));
            //	================== 调用短信发送接口 end ====================================================================
            //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 begin ========================================
            JSONObject resultJson = JSON.parseObject(resultUser);
            Map resultMap = new HashMap();
            if ("00000".equals(resultJson.get("code"))) {
                resultMap.put("id", uuid);
                resultMap.put("verificationcode", verifyCode);
                resultMap.put("phoneNumber", phoneNumber);
                return Result.OK(resultMap);
                //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 end =====================================
            } else if ("D0624".equals(resultJson.get("code"))) {
                return Result.error("同一号码相同内容发送次数太多（默认24小时内，验证码类发送5次或相同内容3次以上会报此错误。");
            } else {
                return Result.error(resultJson.get("msg").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("信息错误");
        }
    }


    /**
     * 短信发送-获取验证码
     *
     * @param smsParams Json参数
     *                  //	@param phoneNumber 手机号
     *                  //	@param smsType 短信类型
     *                  //	@param templateId 模板id
     *                  //	@param fillContent 短信填充内容
     *                  //@param verifyCode 短信变量
     * @return
     */
    public Result<?> getSMSVerificationCode(@RequestBody String smsParams) {
        try {
            //	================== 解析入参 begin ========================================================================
            JSONObject smsParamsObject = JSON.parseObject(smsParams);
            String phoneNumber = smsParamsObject.get("phoneNumber").toString();
            String smsType = smsParamsObject.get("smsType").toString();
            String companyName = smsParamsObject.get("companyName").toString();
            String templateId = smsParamsObject.get("templateId").toString();
            Map fillContent = (Map) smsParamsObject.get("fillContent");
            //	================== 解析入参 end ==========================================================================
            //	================== 根据模板类型，拆分业务代码 begin ==========================================================================
            String captcha = RandomUtil.randomNumbers(6);    //	随机数
            String fillContentJsonString = smsParamsObject.getJSONObject("fillContent").toString();
            String verifyCode = "短信发送成功，（非验证码业务）...";
            String operation = "";
            String sendingRecord = "";
            String month = "";
            String day = "";
            String projectName = "";
            String year = "";
            String time = "";
            String location = "";
            String locationName = "";
            String accountname = "";
            String initPassword = "";
            String QQNum = "";
            String username = "";
            String content = "";
            String content1 = "";
            String content2 = "";
            String content3 = "";
            Gson gson = new Gson();
            if ("9".equals(templateId) || "13".equals(templateId)) {
                verifyCode = captcha;
                fillContent.put("verifyCode", verifyCode);
                sendingRecord = "您的短信验证码是" + verifyCode + "。您正在通过手机号重置登录密码，如非本人操作，请忽略该短信。";
            } else {
                if ("10".equals(templateId) || "15".equals(templateId)) {
                    operation = gson.fromJson(fillContentJsonString, Map.class).get("operation").toString();
                    if ("10".equals(templateId)) {
                        sendingRecord = "您已被" + operation + "黑名单，请登录系统查看详情。";
                    } else if ("15".equals(templateId)) {
                        sendingRecord = "账号注册申请" + operation + "。";
                    }
                } else if ("11".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    sendingRecord = projectName + "项目将于" + month + "月" + day + "日审批超时，请及时登录系统进行审批。";
                } else if ("12".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    year = gson.fromJson(fillContentJsonString, Map.class).get("year").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    time = gson.fromJson(fillContentJsonString, Map.class).get("time").toString();
                    location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                    locationName = gson.fromJson(fillContentJsonString, Map.class).get("locationName").toString();
                    sendingRecord = "您被抽取为" + projectName + "项目验收评审，请于" + year + "年" + month + "月" + day + "日" + time + "时刻" + location +
                            "地参加线下项目验收评审会，地址" + locationName + "确认参加评审请回复“1”，不能参加评审请回复“2”。";
                } else if ("14".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    sendingRecord = "您被抽取为" + projectName + "项目验收评审，请及时登录系统进行项目评审，确认评审请回复“1”，不能评审请回复“2”。";
                } else if ("19".equals(templateId)) {
                    location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    accountname = gson.fromJson(fillContentJsonString, Map.class).get("accountname").toString();
                    username = gson.fromJson(fillContentJsonString, Map.class).get("username").toString();
                    initPassword = gson.fromJson(fillContentJsonString, Map.class).get("initPassword").toString();
                    QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                    sendingRecord = "您好，现甘肃省住建厅科技项目申报系统全面升级改造已完成，根据《甘肃省住房和城乡建设厅关于印发2022年建设科技项目计划的通知》，请您尽快通过地址：" +
                            location + "，于" + month + "月" + day + "日之前进行任务书填报及后续工作，过期视为自动放弃。您的所属单位/个人账户名为：" + accountname + "（单位）/"
                            + username + "（姓名）”，初始密码为 “" + initPassword + "”。请您第一时间进行初始密码修改，并妥善保管密码（如密码遗失，须单位以正" +
                            "式文件形式向我厅申请并经同意后方可重置），避免对您的业务造成不必要的损失。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                } else if ("20".equals(templateId)) {
                    content1 = gson.fromJson(fillContentJsonString, Map.class).get("content1").toString();
                    content2 = gson.fromJson(fillContentJsonString, Map.class).get("content2").toString();
                    content3 = gson.fromJson(fillContentJsonString, Map.class).get("content3").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                    sendingRecord = "您好，" + content1 + "，请" + content2 + "，现通知到您，为避免对您的业务造成不必要的损失，请您于" + month + "月" + day + "日之前" + content3 + "。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                } else if ("24".equals(templateId)) {
                    content = gson.fromJson(fillContentJsonString, Map.class).get("content").toString();
                    sendingRecord = "请" + content + "单位保留中期查验过程中所有影像资料。";
                }
            }
            //	================== 根据模板类型，拆分业务代码 end ==========================================================================
            //	================== 参数封装 begin ========================================================================
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            String access_token = "";    //	系统登录 token
            String tokenForSMS = "";    //	短信发送，专用 token
            System.out.println("redis ----------------------: " + redisUtil);
            Object checkCode = redisUtil.get(CommonConstant.SMS_TOKEN);        //	判断 redis 缓存中是否存在token，如果
            if (checkCode == null) {
                tokenForSMS = this.getTokenForSMS(client_id, client_secret);
                JSONObject paramJson = JSON.parseObject(tokenForSMS);
                access_token = paramJson.get("access_token").toString();
                redisUtil.set(CommonConstant.SMS_TOKEN, tokenForSMS);
                redisUtil.expire(CommonConstant.SMS_TOKEN, CommonConstant.SMS_TOKEN_EXPIRE_TIME);
            } else {
                JSONObject paramJson = JSON.parseObject(checkCode.toString());
                access_token = paramJson.get("access_token").toString();
            }
            //	================== 参数封装 end ==========================================================================
            //	================== 调用短信发送接口 begin ==================================================================
            HashMap<String, Object> headers = new HashMap<>(10);
            HashMap<String, Object> params = new HashMap<>(10);
            headers.put("Authorization", "Bearer " + access_token);
            params.put("phoneNumber", phoneNumber);
            params.put("smsType", smsType);
            params.put("templateId", templateId);
            //	JSONObject json = new JSONObject(fillContent);	//	使用 fastjson 将 内层对象转为 json 格式
            params.put("fillContent", fillContent);
            String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(params));
            //	================== 调用短信发送接口 end ====================================================================
            //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 begin ========================================
            JSONObject resultJson = JSON.parseObject(resultUser);
            Map resultMap = new HashMap();
            if ("00000".equals(resultJson.get("code"))) {
                resultMap.put("id", uuid);
                resultMap.put("verificationcode", verifyCode);
                resultMap.put("phoneNumber", phoneNumber);
                //	================== 同步信息发送记录表 begin ============================================================
                ZgsSmsSendingRecordLog smsLogs = new ZgsSmsSendingRecordLog();
                smsLogs.setId(uuid);
                smsLogs.setPhonenumber(phoneNumber);
                smsLogs.setSendingrecord(sendingRecord);
                smsLogs.setUsername(((LoginUser) SecurityUtils.getSubject().getPrincipal()).getRealname());
                smsLogs.setVerificationcode(verifyCode);
                smsLogs.setSendingAccount(((LoginUser) SecurityUtils.getSubject().getPrincipal()).getUsername());
                smsLogs.setReceiveCompany(companyName);
                smsLogs.setStatus("1");
                //	通过Date类来获取当前时间，通过SimpleDateFormat来设置时间格式
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date currentDate = new Date();
                String currentTime = dateFormat.format(currentDate);
                smsLogs.setCreatetime(currentTime);
                smsLogsService.save(smsLogs);
                return Result.OK(resultMap);
                //	================== 同步信息发送记录表 end ==============================================================
                //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 end =====================================
            } else if ("D0624".equals(resultJson.get("code"))) {
                return Result.error("同一号码相同内容发送次数太多（默认24小时内，验证码类发送5次或相同内容3次以上会报此错误。");
            } else {
                return Result.error(resultJson.get("msg").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("信息错误");
        }
    }


    /**
     * 短信发送-获取验证码
     *
     * @param smsParams Json参数
     *                  //	@param phoneNumber 手机号
     *                  //	@param smsType 短信类型
     *                  //	@param templateId 模板id
     *                  //	@param fillContent 短信填充内容
     *                  //@param verifyCode 短信变量
     * @return
     */
    public Result<?> getSMSVerificationCodePost(@RequestBody String smsParams) {
        try {
            //	================== 解析入参 begin ========================================================================
            JSONObject smsParamsObject = JSON.parseObject(smsParams);
            String phoneNumber = smsParamsObject.get("phoneNumber").toString();
            String smsType = smsParamsObject.get("smsType").toString();
            String companyName = smsParamsObject.get("companyName").toString();
            String templateId = smsParamsObject.get("templateId").toString();
            Map fillContent = (Map) smsParamsObject.get("fillContent");
            //	================== 解析入参 end ==========================================================================
            //	================== 根据模板类型，拆分业务代码 begin ==========================================================================
            String captcha = RandomUtil.randomNumbers(6);    //	随机数
            String fillContentJsonString = smsParamsObject.getJSONObject("fillContent").toString();
            String verifyCode = "短信发送成功，（非验证码业务）...";
            String operation = "";
            String sendingRecord = "";
            String month = "";
            String day = "";
            String projectName = "";
            String year = "";
            String time = "";
            String location = "";
            String locationName = "";
            String accountname = "";
            String initPassword = "";
            String QQNum = "";
            String username = "";
            String content = "";
            String content1 = "";
            String content2 = "";
            String content3 = "";
            Gson gson = new Gson();
            if ("9".equals(templateId) || "13".equals(templateId)) {
                verifyCode = captcha;
                fillContent.put("verifyCode", verifyCode);
                sendingRecord = "您的短信验证码是" + verifyCode + "。您正在通过手机号重置登录密码，如非本人操作，请忽略该短信。";
            } else {
                if ("10".equals(templateId) || "15".equals(templateId)) {
                    operation = gson.fromJson(fillContentJsonString, Map.class).get("operation").toString();
                    if ("10".equals(templateId)) {
                        sendingRecord = "您已被" + operation + "黑名单，请登录系统查看详情。";
                    } else if ("15".equals(templateId)) {
                        sendingRecord = "账号注册申请" + operation + "。";
                    }
                } else if ("11".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    sendingRecord = projectName + "项目将于" + month + "月" + day + "日审批超时，请及时登录系统进行审批。";
                } else if ("12".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    year = gson.fromJson(fillContentJsonString, Map.class).get("year").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    time = gson.fromJson(fillContentJsonString, Map.class).get("time").toString();
                    location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                    locationName = gson.fromJson(fillContentJsonString, Map.class).get("locationName").toString();
                    sendingRecord = "您被抽取为" + projectName + "项目验收评审，请于" + year + "年" + month + "月" + day + "日" + time + "时刻" + location +
                            "地参加线下项目验收评审会，地址" + locationName + "确认参加评审请回复“1”，不能参加评审请回复“2”。";
                } else if ("14".equals(templateId)) {
                    projectName = gson.fromJson(fillContentJsonString, Map.class).get("projectName").toString();
                    sendingRecord = "您被抽取为" + projectName + "项目验收评审，请及时登录系统进行项目评审，确认评审请回复“1”，不能评审请回复“2”。";
                } else if ("19".equals(templateId)) {
                    location = gson.fromJson(fillContentJsonString, Map.class).get("location").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    accountname = gson.fromJson(fillContentJsonString, Map.class).get("accountname").toString();
                    username = gson.fromJson(fillContentJsonString, Map.class).get("username").toString();
                    initPassword = gson.fromJson(fillContentJsonString, Map.class).get("initPassword").toString();
                    QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                    sendingRecord = "您好，现甘肃省住建厅科技项目申报系统全面升级改造已完成，根据《甘肃省住房和城乡建设厅关于印发2022年建设科技项目计划的通知》，请您尽快通过地址：" +
                            location + "，于" + month + "月" + day + "日之前进行任务书填报及后续工作，过期视为自动放弃。您的所属单位/个人账户名为：" + accountname + "（单位）/"
                            + username + "（姓名）”，初始密码为 “" + initPassword + "”。请您第一时间进行初始密码修改，并妥善保管密码（如密码遗失，须单位以正" +
                            "式文件形式向我厅申请并经同意后方可重置），避免对您的业务造成不必要的损失。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                } else if ("20".equals(templateId)) {
                    content1 = gson.fromJson(fillContentJsonString, Map.class).get("content1").toString();
                    content2 = gson.fromJson(fillContentJsonString, Map.class).get("content2").toString();
                    content3 = gson.fromJson(fillContentJsonString, Map.class).get("content3").toString();
                    month = gson.fromJson(fillContentJsonString, Map.class).get("month").toString();
                    day = gson.fromJson(fillContentJsonString, Map.class).get("day").toString();
                    QQNum = gson.fromJson(fillContentJsonString, Map.class).get("QQNum").toString();
                    sendingRecord = "您好，" + content1 + "，请" + content2 + "，现通知到您，为避免对您的业务造成不必要的损失，请您于" + month + "月" + day + "日之前" + content3 + "。如有问题，加入QQ技术交流群：" + QQNum + "进行咨询。";
                } else if ("24".equals(templateId)) {
                    content = gson.fromJson(fillContentJsonString, Map.class).get("content").toString();
                    sendingRecord = "请" + content + "单位保留中期查验过程中所有影像资料。";
                }
            }
            //	================== 根据模板类型，拆分业务代码 end ==========================================================================
            //	================== 参数封装 begin ========================================================================
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            String access_token = "";    //	系统登录 token
            String tokenForSMS = "";    //	短信发送，专用 token
            Object checkCode = redisUtil.get(CommonConstant.SMS_TOKEN);        //	判断 redis 缓存中是否存在token，如果
            if (checkCode == null) {
                tokenForSMS = this.getTokenForSMS(client_id, client_secret);
                JSONObject paramJson = JSON.parseObject(tokenForSMS);
                access_token = paramJson.get("access_token").toString();
                redisUtil.set(CommonConstant.SMS_TOKEN, tokenForSMS);
                redisUtil.expire(CommonConstant.SMS_TOKEN, CommonConstant.SMS_TOKEN_EXPIRE_TIME);
            } else {
                JSONObject paramJson = JSON.parseObject(checkCode.toString());
                access_token = paramJson.get("access_token").toString();
            }
//      String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(params));
            Map<String, String> mapToken = new HashMap<String, String>();    //	Token （采用 Authorization 模式）
            mapToken.put("isToken", "false");
            mapToken.put("Authorization", "Bearer " + access_token);
            HttpHeader httpHeaderUser = new HttpHeader(mapToken);
            HttpParamers httpParamers = HttpParamers.httpPostParamers();//	短信参数
            httpParamers.addParam("phoneNumber", phoneNumber);
            httpParamers.addParam("smsType", smsType);
            httpParamers.addParam("templateId", templateId);
            //	JSONObject json = new JSONObject(fillContent);	//	使用 fastjson 将 内层对象转为 json 格式
            httpParamers.addParam("fillContent", fillContent);
            httpParamers.setJsonParamer();    //	将 httpParamers 参数复制，变为 json 格式
            //	================== 参数封装 end ==========================================================================

            //	================== 调用短信发送接口 begin ==================================================================

            HttpService httpServiceUser = new HttpService(smsBaseUrl);
            String resultUser = httpServiceUser.service(GlobalConstants.usl, httpParamers, httpHeaderUser);
            System.out.println("===============: " + resultUser);
            //	================== 调用短信发送接口 end ====================================================================
            //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 begin ========================================
            JSONObject resultJson = JSON.parseObject(resultUser);
            Map resultMap = new HashMap();
            if ("00000".equals(resultJson.get("code"))) {
                resultMap.put("id", uuid);
                resultMap.put("verificationcode", verifyCode);
                resultMap.put("phoneNumber", phoneNumber);
                //	================== 同步信息发送记录表 begin ============================================================
                ZgsSmsSendingRecordLog smsLogs = new ZgsSmsSendingRecordLog();
                smsLogs.setId(uuid);
                smsLogs.setPhonenumber(phoneNumber);
                smsLogs.setSendingrecord(sendingRecord);
                smsLogs.setUsername(((LoginUser) SecurityUtils.getSubject().getPrincipal()).getRealname());
                smsLogs.setVerificationcode(verifyCode);
                smsLogs.setSendingAccount(((LoginUser) SecurityUtils.getSubject().getPrincipal()).getUsername());
                smsLogs.setReceiveCompany(companyName);
                smsLogs.setStatus("1");
                //	通过Date类来获取当前时间，通过SimpleDateFormat来设置时间格式
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date currentDate = new Date();
                String currentTime = dateFormat.format(currentDate);
                smsLogs.setCreatetime(currentTime);
                smsLogsService.save(smsLogs);
                return Result.OK(resultMap);
                //	================== 同步信息发送记录表 end ==============================================================
                //	================== 处理短信发送接口 返回信息，并同步数据库，记录验证码，以供做比对 end =====================================
            } else if ("D0624".equals(resultJson.get("code"))) {
                return Result.error("同一号码相同内容发送次数太多（默认24小时内，验证码类发送5次或相同内容3次以上会报此错误。");
            } else {
                return Result.error(resultJson.get("msg").toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("信息错误");
        }
    }

}
