package org.jeecg.modules.green.report.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreadetail;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreadetailService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_monthfabr_areadetail
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="zgs_report_monthfabr_areadetail")
@RestController
@RequestMapping("/report/zgsReportMonthfabrAreadetail")
@Slf4j
public class ZgsReportMonthfabrAreadetailController extends JeecgController<ZgsReportMonthfabrAreadetail, IZgsReportMonthfabrAreadetailService> {
	@Autowired
	private IZgsReportMonthfabrAreadetailService zgsReportMonthfabrAreadetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportMonthfabrAreadetail
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-分页列表查询")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-分页列表查询", notes="zgs_report_monthfabr_areadetail-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportMonthfabrAreadetail zgsReportMonthfabrAreadetail,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsReportMonthfabrAreadetail> queryWrapper = QueryGenerator.initQueryWrapper(zgsReportMonthfabrAreadetail, req.getParameterMap());
		Page<ZgsReportMonthfabrAreadetail> page = new Page<ZgsReportMonthfabrAreadetail>(pageNo, pageSize);
		IPage<ZgsReportMonthfabrAreadetail> pageList = zgsReportMonthfabrAreadetailService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportMonthfabrAreadetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-添加")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-添加", notes="zgs_report_monthfabr_areadetail-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportMonthfabrAreadetail zgsReportMonthfabrAreadetail) {
		zgsReportMonthfabrAreadetailService.save(zgsReportMonthfabrAreadetail);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsReportMonthfabrAreadetail
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-编辑")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-编辑", notes="zgs_report_monthfabr_areadetail-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportMonthfabrAreadetail zgsReportMonthfabrAreadetail) {
		zgsReportMonthfabrAreadetailService.updateById(zgsReportMonthfabrAreadetail);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-通过id删除")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-通过id删除", notes="zgs_report_monthfabr_areadetail-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportMonthfabrAreadetailService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-批量删除")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-批量删除", notes="zgs_report_monthfabr_areadetail-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportMonthfabrAreadetailService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_monthfabr_areadetail-通过id查询")
	@ApiOperation(value="zgs_report_monthfabr_areadetail-通过id查询", notes="zgs_report_monthfabr_areadetail-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportMonthfabrAreadetail zgsReportMonthfabrAreadetail = zgsReportMonthfabrAreadetailService.getById(id);
		if(zgsReportMonthfabrAreadetail==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsReportMonthfabrAreadetail);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportMonthfabrAreadetail
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabrAreadetail zgsReportMonthfabrAreadetail) {
        return super.exportXls(request, zgsReportMonthfabrAreadetail, ZgsReportMonthfabrAreadetail.class, "zgs_report_monthfabr_areadetail");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabrAreadetail.class);
    }

}
