package org.jeecg.modules.green.report.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.largeScreen.po.*;
import org.jeecg.modules.green.largeScreen.vo.PrefabricaeZbVo;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description: 绿色建筑汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface ZgsReportEnergyNewinfoGreentotalMapper extends BaseMapper<ZgsReportEnergyNewinfoGreentotal> {
    ZgsReportEnergyNewinfoGreentotal totalMonthNewinfoGreenData(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyNewinfoGreentotal totalMonthNewinfoGreenDataYear(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    ZgsReportEnergyNewinfoGreentotal totalMonthNewinfoGreenDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode);

    ZgsReportEnergyNewinfoGreentotal totalMonthNewinfoGreenDataYearCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("year") String year);

    //省厅账号计算当前月份、当前行政区划合计值
    ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataProvince(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    //市州账号计算当前月份、当前行政区划合计值
    ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataCity(@Param("filltm") String filltm, @Param("areacode") String areacode, @Param("applystate") Integer applystate);

    /****************************TODO 大屏绿色建筑统计**************/
    //绿色建筑统计
    SpecificDataInformationPo specificDataInformation(@Param("year") String year, @Param("areacodel") String areacodel);

    //装配式数据统计
    AssemblyDataStatisticsPo assemblyDataStatistics(@Param("year") String year);

    //大屏项目总数和绿色项目总数
    TotalTitemsPo totalTitems(@Param("year") String year, @Param("areacodel") String areacodel);

    //本年度累计绿色建筑等级
    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYear(@Param("year") String year);

    //绿色建筑数据
    List<MoonGreenBuildingPo> greenBuildingData(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //新开工装配式数据统计
    PrefabricatedBuildingPo prefabricatedBuilding(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //新开工装配式总面积
    String newConstructiontoTalArea(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //竣工装配式建筑数据统计
    PrefabricatedBuildingPo fabricatedCompletion(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //竣工装配式总面积
    String totalBuildingArea(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //建筑节能数据
    List<BuildingEnergySavingPo> buildingEnergySavingData(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //建筑节能统计本月竣工验收项目
    BuildingEnergySavingTotalTitemsPo buildingEnergySavingTotalTitems(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //可再生能源利用情况
    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergy(@Param("year") String year);

    //建筑节能统计
    BuildingEnergyEfficiencyStatisticsPo buildingEnergyEfficiencyStatistics(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    //建筑节能数据既有
    List<BuildingEnergySavingPo> buildingEnergySavingExisting(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    List<City> getAllCities();

    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInThisYearNew(@Param("year") String year, @Param("areacodel") String code);

    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyNew(@Param("year") String year, @Param("areacodel") String code);

    List<MoonGreenBuildingPo> moonGreenBuildingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    SpecificDataInformationPo specificDataInformationNew(@Param("year") String year, @Param("areacodel") String areacodel);

    BuildingEnergySavingPo buildingEnergySavingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String code);

    BuildingEnergySavingPo buildingEnergySavingExistingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String code);

    List<AccumulatedGreenBuildingGradeInPo> accumulatedGreenBuildingGradeInPoList(@Param("year") String year, @Param("areacodel") String areacodel);

    List<UtilizationOfRenewableEnergyPo> utilizationOfRenewableEnergyList(@Param("year") String year, @Param("areacodel") String areacodel);

    String getCityOne(@Param("areacodel") String areacodel);

    MoonGreenBuildingPo monthMoonGreenBuildingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    List<MoonGreenBuildingPo> monthGreenBuildingData(@Param("year") String year, @Param("month") String month, @Param("areacodel") String areacodel);

    List<BuildingEnergySavingPo> monthBuildingEnergySavingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String code);

    List<BuildingEnergySavingPo>  monthBuildingEnergySavingExistingPo(@Param("year") String year, @Param("month") String month, @Param("areacodel") String code);

    PrefabricaeZbVo prefabricateBulidngZb(@Param("month") String month, @Param("areacodel") String areacodel, @Param("prdType") String prdType);

    List<MoonGreenBuildingPo> moonGreenBuildingPoTow(@Param("month") String month, @Param("areacodel") String areacodel);
}
