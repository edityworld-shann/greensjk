package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsThreepersoncategoryfinal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 三类人员类别实时表
 * @Author: jeecg-boot
 * @Date:   2022-02-21
 * @Version: V1.0
 */
public interface IZgsThreepersoncategoryfinalService extends IService<ZgsThreepersoncategoryfinal> {

}
