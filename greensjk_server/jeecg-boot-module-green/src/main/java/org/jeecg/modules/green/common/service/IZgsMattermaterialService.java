package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsMattermaterial;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.green.common.po.ZgsExpertinfoPo;

import java.util.List;
import java.util.Map;

/**
 * @Description: 业务库-各类项目工程申报材料
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
public interface IZgsMattermaterialService extends IService<ZgsMattermaterial> {
    List<ZgsMattermaterial> getZgsMattermaterialList(Map<String, Object> map);

    List<ZgsMattermaterial> getAddZgsMattermaterialList(Map<String, Object> map);

    List<ZgsExpertinfoPo> getTheNumberOfExpertsFor();

    int getUserCount(String itemValue, Integer type);
}
