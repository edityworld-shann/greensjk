package org.jeecg.modules.green.xmyszssq.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.service.IZgsProjectlibraryService;
import org.jeecg.modules.green.common.service.IZgsSciencetechfeasibleService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

 /**
 * @Description: 科研验收证书验收专家名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Api(tags="科研验收证书验收专家名单")
@RestController
@RequestMapping("/xmyszssq/zgsSacceptcertexpert")
@Slf4j
public class ZgsSacceptcertexpertController extends JeecgController<ZgsSacceptcertexpert, IZgsSacceptcertexpertService> {
	 @Autowired
	 private IZgsSacceptcertexpertService zgsSacceptcertexpertService;
	 @Autowired
	 private IZgsSciencetechfeasibleService iZgsSciencetechfeasibleService;
	 @Autowired
	 private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
	 @Autowired
	 private IZgsBuildprojectService zgsBuildprojectService;
	 @Autowired
	 private IZgsAssembleprojectService zgsAssembleprojectService;
	 @Autowired
	 private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
	 @Autowired
	 private IZgsSamplebuildprojectService zgsSamplebuildprojectService;

	 @Autowired
	 private IZgsProjectlibraryService zgsProjectlibraryService;
	/**
	 * 分页列表查询
	 *
	 * @param zgsSacceptcertexpert
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-分页列表查询")
	@ApiOperation(value="科研验收证书验收专家名单-分页列表查询", notes="科研验收证书验收专家名单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsSacceptcertexpert zgsSacceptcertexpert,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsSacceptcertexpert> queryWrapper = QueryGenerator.initQueryWrapper(zgsSacceptcertexpert, req.getParameterMap());
		Page<ZgsSacceptcertexpert> page = new Page<ZgsSacceptcertexpert>(pageNo, pageSize);
		IPage<ZgsSacceptcertexpert> pageList = zgsSacceptcertexpertService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsSacceptcertexpert
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-添加")
	@ApiOperation(value="科研验收证书验收专家名单-添加", notes="科研验收证书验收专家名单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsSacceptcertexpert zgsSacceptcertexpert) {
		zgsSacceptcertexpertService.save(zgsSacceptcertexpert);
		return Result.OK("添加成功！");
	}

	/**
	 * @describe: 省厅分配专家信息
	 * @author: renxiaoliang
	 * @date: 2023/7/3 18:00
	 */
	 @AutoLog(value = "科研验收证书验收专家名单-省厅分配专家")
	 @ApiOperation(value="科研验收证书验收专家名单-省厅分配专家", notes="科研验收证书验收专家名单-省厅分配专家")
	 @PostMapping(value = "/addExperts")
	 public Result<?> addExperts(@RequestBody ZgsSacceptcertexpert zgsSacceptcertexpert) {
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		 String projectlibraryguid = zgsSacceptcertexpert.getProjectlibraryguid();  // 科研项目申报验收基本信息表主键
		 String enterpriseguid = zgsSacceptcertexpert.getEnterpriseguid();  // 企业id
		 String projectStage = zgsSacceptcertexpert.getProjectStage();  // 项目阶段，各个阶段保存专家标识  申报阶段：sbjd   type: 1：示范类   2：科研类;     zqcy:中期查验;     验收阶段：ysjd;
		 String type = zgsSacceptcertexpert.getType();
		 // 如果进行专家信息更新，先删除历史专家信息，从新进行专家信息新增
		 QueryWrapper<ZgsSacceptcertexpert> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("baseguid", projectlibraryguid);
		 queryWrapper.eq("project_stage", projectStage);
		 queryWrapper.ne("isdelete", 1);
		 List<ZgsSacceptcertexpert> listExpert = zgsSacceptcertexpertService.list(queryWrapper);
		 if (listExpert.size() > 0) {
			 Boolean delBol = zgsSacceptcertexpertService.remove(queryWrapper);
			 if (delBol) {
				 // 获取专家信息集合
				 List<Map<String, Object>> expertsMap = zgsSacceptcertexpert.getExpertList();
				 if (expertsMap.size() > 0) {
					 for (Map<String, Object> expertmap : expertsMap) {
						 ZgsSacceptcertexpert entityExpert = new ZgsSacceptcertexpert();

						 entityExpert.setId(UUID.randomUUID().toString());
						 entityExpert.setBaseguid(projectlibraryguid);
						 entityExpert.setEnterpriseguid(enterpriseguid);
						 entityExpert.setName(expertmap.get("name").toString());
						 entityExpert.setTechnicaltitle(expertmap.get("technicaltitle").toString());  // 职务职称
						 entityExpert.setWorkunit(expertmap.get("workunit").toString());  // 工作单位
						 entityExpert.setCreateaccountname(sysUser.getUsername());  // 创建人账号
						 entityExpert.setCreateusername(sysUser.getRealname());  // 创建人姓名
						 entityExpert.setCreatedate(new Date()); // 创建日期
						 entityExpert.setStatus(GlobalConstants.SHENHE_STATUS4);  // 审核状态
						 entityExpert.setDuty(expertmap.get("duty").toString());  // 专家会职务
						 entityExpert.setSpecialty(expertmap.get("specialty").toString());  // 现从事专业
						 // entityExpert.setIsEncrypt(1);  // 是否加密  1 已加密
						 entityExpert.setExpertsSource(expertmap.get("expertsSource").toString());  // 专家来源
						 entityExpert.setProjectStage(projectStage);  // 项目阶段
						 zgsSacceptcertexpertService.save(entityExpert);
						 // zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(entityExpert, ValidateEncryptEntityUtil.isEncrypt));
					 }
					 if (type.equals("1")) {  // 专家分配后将项目状态变为待立项
					 	 if (updateState(projectlibraryguid)) {
							 // 更新项目当前所处阶段及状态信息
							 ZgsProjectlibrary zgsProjectlibrary = new ZgsProjectlibrary();
							 zgsProjectlibrary.setId(projectlibraryguid);
							 zgsProjectlibrary.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
							 zgsProjectlibraryService.updateById(zgsProjectlibrary);
						 };
					 } else if (type.equals("2")) {  // 专家分配后将项目状态变为待立项
						 ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
						 zgsSciencetechfeasible.setId(projectlibraryguid);
						 zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS12);
						 // 更新项目当前所处阶段及状态
						 zgsSciencetechfeasible.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
						 iZgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible);
					 }


					 return Result.OK("添加成功");
				 }
			 } else {
				 return Result.error("专家信息更新失败");
			 }
		 } else {
			 // 获取专家信息集合
			 List<Map<String, Object>> expertsMap = zgsSacceptcertexpert.getExpertList();

			 if (expertsMap.size() > 0) {
				 for (Map<String, Object> expertmap : expertsMap) {
					 ZgsSacceptcertexpert entityExpert = new ZgsSacceptcertexpert();

					 entityExpert.setId(UUID.randomUUID().toString());
					 entityExpert.setBaseguid(projectlibraryguid);
					 entityExpert.setEnterpriseguid(enterpriseguid);
					 entityExpert.setName(expertmap.get("name").toString());
					 entityExpert.setTechnicaltitle(expertmap.get("technicaltitle").toString());  // 职务职称
					 entityExpert.setWorkunit(expertmap.get("workunit").toString());  // 工作单位
					 entityExpert.setCreateaccountname(sysUser.getUsername());  // 创建人账号
					 entityExpert.setCreateusername(sysUser.getRealname());  // 创建人姓名
					 entityExpert.setCreatedate(new Date()); // 创建日期
					 entityExpert.setStatus(GlobalConstants.SHENHE_STATUS4);  // 审核状态
					 entityExpert.setDuty(expertmap.get("duty").toString());  // 专家会职务
					 entityExpert.setSpecialty(expertmap.get("specialty").toString());  // 现从事专业
					 // entityExpert.setIsEncrypt(1);  // 是否加密  1 已加密
					 entityExpert.setExpertsSource(expertmap.get("expertsSource").toString());  // 专家来源
					 entityExpert.setProjectStage(projectStage);  // 项目阶段

					 zgsSacceptcertexpertService.save(entityExpert);
					 // zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(entityExpert, ValidateEncryptEntityUtil.isEncrypt));
				 }
				 if (type.equals("1")) {  // 专家分配后将项目状态变为待立项
					 if (updateState(projectlibraryguid)) {
						 // 更新项目当前所处阶段及状态信息
						 ZgsProjectlibrary zgsProjectlibrary = new ZgsProjectlibrary();
						 zgsProjectlibrary.setId(projectlibraryguid);
						 zgsProjectlibrary.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
						 zgsProjectlibraryService.updateById(zgsProjectlibrary);
					 };
				 } else if (type.equals("2")) {  // 专家分配后将项目状态变为待立项
					 ZgsSciencetechfeasible zgsSciencetechfeasible = new ZgsSciencetechfeasible();
					 zgsSciencetechfeasible.setId(projectlibraryguid);
					 zgsSciencetechfeasible.setStatus(GlobalConstants.SHENHE_STATUS12);
					 // 更新项目当前所处阶段及状态
					 zgsSciencetechfeasible.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
					 iZgsSciencetechfeasibleService.updateById(zgsSciencetechfeasible);
				 }
			 }
			 return Result.OK("添加成功！");
		 }
		 return Result.OK("添加成功！");
	 }



	 private boolean updateState(String projectlibraryguid) {
	 	boolean stateBol = false;
	 	if (StringUtils.isNotBlank(projectlibraryguid)) {
			QueryWrapper<ZgsGreenbuildproject> greenbuildprojectQueryWrapper = new QueryWrapper();  // 绿色建筑
			greenbuildprojectQueryWrapper.eq("projectlibraryguid", projectlibraryguid);
			QueryWrapper<ZgsBuildproject> buildprojectQueryWrapper = new QueryWrapper();  // 建筑工程
			buildprojectQueryWrapper.eq("projectlibraryguid", projectlibraryguid);
			QueryWrapper<ZgsAssembleproject> assembleprojectQueryWrapper = new QueryWrapper();  // 装配式
			assembleprojectQueryWrapper.eq("projectlibraryguid", projectlibraryguid);
			QueryWrapper<ZgsEnergybuildproject> energybuildprojectQueryWrapper = new QueryWrapper();  // 建筑节能
			energybuildprojectQueryWrapper.eq("projectlibraryguid", projectlibraryguid);
			QueryWrapper<ZgsSamplebuildproject> samplebuildprojectQueryWrapper = new QueryWrapper();  // 市政工程
			samplebuildprojectQueryWrapper.eq("projectlibraryguid", projectlibraryguid);


			// 绿色建筑
			if (zgsGreenbuildprojectService.list(greenbuildprojectQueryWrapper).size() > 0) {
				ZgsGreenbuildproject zgsGreenbuildproject = zgsGreenbuildprojectService.list(greenbuildprojectQueryWrapper).get(0);
				ZgsGreenbuildproject entity = new ZgsGreenbuildproject();
				entity.setId(zgsGreenbuildproject.getId());
				entity.setStatus(GlobalConstants.SHENHE_STATUS12);
				stateBol = zgsGreenbuildprojectService.updateById(entity);

			} else if (zgsBuildprojectService.list(buildprojectQueryWrapper).size() > 0) {  // 建筑工程
				ZgsBuildproject zgsBuildproject = zgsBuildprojectService.list(buildprojectQueryWrapper).get(0);
				ZgsBuildproject entity = new ZgsBuildproject();
				entity.setId(zgsBuildproject.getId());
				entity.setStatus(GlobalConstants.SHENHE_STATUS12);
				stateBol = zgsBuildprojectService.updateById(entity);

			} else if (zgsAssembleprojectService.list(assembleprojectQueryWrapper).size() > 0) {  // 装配式
				ZgsAssembleproject zgsAssembleproject = zgsAssembleprojectService.list(assembleprojectQueryWrapper).get(0);
				ZgsAssembleproject entity = new ZgsAssembleproject();
				entity.setId(zgsAssembleproject.getId());
				entity.setStatus(GlobalConstants.SHENHE_STATUS12);
				stateBol = zgsAssembleprojectService.updateById(entity);

			} else if (zgsEnergybuildprojectService.list(energybuildprojectQueryWrapper).size() > 0) {  // 建筑节能
				ZgsEnergybuildproject zgsEnergybuildproject = zgsEnergybuildprojectService.list(energybuildprojectQueryWrapper).get(0);
				ZgsEnergybuildproject entity = new ZgsEnergybuildproject();
				entity.setId(zgsEnergybuildproject.getId());
				entity.setStatus(GlobalConstants.SHENHE_STATUS12);
				stateBol = zgsEnergybuildprojectService.updateById(entity);

			} else if (zgsSamplebuildprojectService.list(samplebuildprojectQueryWrapper).size() > 0) { // 市政工程
				ZgsSamplebuildproject zgsSamplebuildproject = zgsSamplebuildprojectService.list(samplebuildprojectQueryWrapper).get(0);
				ZgsSamplebuildproject entity = new ZgsSamplebuildproject();
				entity.setId(zgsSamplebuildproject.getId());
				entity.setStatus(GlobalConstants.SHENHE_STATUS12);
				stateBol = zgsSamplebuildprojectService.updateById(entity);

			}
		}
		 return stateBol;
	 }








	 @AutoLog(value = "查询省厅添加的专家信息")
	 @ApiOperation(value="科研验收证书验收专家名单-通过id查询", notes="科研验收证书验收专家名单-通过id查询")
	 @GetMapping(value = "/queryExpertById")
	 public Result<?> queryExpertById(@RequestParam(name="projectlibraryguid",required=false) String projectlibraryguid, @RequestParam(name="projectStage",required=false) String projectStage) {
		 QueryWrapper<ZgsSacceptcertexpert> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("baseguid",projectlibraryguid);
		 queryWrapper.eq("project_stage",projectStage);
		 queryWrapper.ne("isdelete",1);

		 List<ZgsSacceptcertexpert> zgsSacceptcertexperts = zgsSacceptcertexpertService.list(queryWrapper);

		 if(zgsSacceptcertexperts.size() > 0) {
			 return Result.OK(zgsSacceptcertexperts);
		 } else {
			 return Result.error("未找到对应数据");
		 }

	 }



	
	/**
	 *  编辑
	 *
	 * @param zgsSacceptcertexpert
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-编辑")
	@ApiOperation(value="科研验收证书验收专家名单-编辑", notes="科研验收证书验收专家名单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsSacceptcertexpert zgsSacceptcertexpert) {
		zgsSacceptcertexpertService.updateById(zgsSacceptcertexpert);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-通过id删除")
	@ApiOperation(value="科研验收证书验收专家名单-通过id删除", notes="科研验收证书验收专家名单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsSacceptcertexpertService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-批量删除")
	@ApiOperation(value="科研验收证书验收专家名单-批量删除", notes="科研验收证书验收专家名单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsSacceptcertexpertService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "科研验收证书验收专家名单-通过id查询")
	@ApiOperation(value="科研验收证书验收专家名单-通过id查询", notes="科研验收证书验收专家名单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertService.getById(id);
		if(zgsSacceptcertexpert==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsSacceptcertexpert);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsSacceptcertexpert
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsSacceptcertexpert zgsSacceptcertexpert) {
        return super.exportXls(request, zgsSacceptcertexpert, ZgsSacceptcertexpert.class, "科研验收证书验收专家名单");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsSacceptcertexpert.class);
    }

}
