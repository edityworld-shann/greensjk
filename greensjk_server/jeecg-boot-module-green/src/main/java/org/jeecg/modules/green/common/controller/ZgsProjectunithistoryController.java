package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import org.jeecg.modules.green.common.service.IZgsProjectunithistoryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 黑名单历史记录
 * @Author: jeecg-boot
 * @Date: 2022-03-06
 * @Version: V1.0
 */
@Api(tags = "黑名单历史记录")
@RestController
@RequestMapping("/common/zgsProjectunithistory")
@Slf4j
public class ZgsProjectunithistoryController extends JeecgController<ZgsProjectunithistory, IZgsProjectunithistoryService> {
    @Autowired
    private IZgsProjectunithistoryService zgsProjectunithistoryService;

    /**
     * 分页列表查询
     *
     * @param zgsProjectunithistory
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "黑名单历史记录-分页列表查询")
    @ApiOperation(value = "黑名单历史记录-分页列表查询", notes = "黑名单历史记录-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjectunithistory zgsProjectunithistory,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsProjectunithistory> queryWrapper = QueryGenerator.initQueryWrapper(zgsProjectunithistory, req.getParameterMap());
        Page<ZgsProjectunithistory> page = new Page<ZgsProjectunithistory>(pageNo, pageSize);
        IPage<ZgsProjectunithistory> pageList = zgsProjectunithistoryService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsProjectunithistory
     * @return
     */
    @AutoLog(value = "黑名单历史记录-添加")
    @ApiOperation(value = "黑名单历史记录-添加", notes = "黑名单历史记录-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjectunithistory zgsProjectunithistory) {
        zgsProjectunithistoryService.save(zgsProjectunithistory);
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsProjectunithistory
     * @return
     */
    @AutoLog(value = "黑名单历史记录-编辑")
    @ApiOperation(value = "黑名单历史记录-编辑", notes = "黑名单历史记录-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjectunithistory zgsProjectunithistory) {
        zgsProjectunithistoryService.updateById(zgsProjectunithistory);
        return Result.OK("编辑成功!");
    }

    /**
     * 用户加入黑名单
     *
     * @param zgsProjectunithistory
     * @return
     */
    @AutoLog(value = "用户加入黑名单")
    @ApiOperation(value = "用户加入黑名单", notes = "用户加入黑名单")
    @PutMapping(value = "/blackUser")
    public Result<?> blackUser(@RequestBody ZgsProjectunithistory zgsProjectunithistory) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectunithistory != null && zgsProjectunithistory.getStatus() != null && StringUtils.isNotEmpty(zgsProjectunithistory.getUserid())) {
            if (zgsProjectunithistory.getStatus() == 2) {
                zgsProjectunithistory.setCreateTime(new Date());
                zgsProjectunithistory.setCreateBy(sysUser.getRealname());
                zgsProjectunithistoryService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsProjectunithistory, ValidateEncryptEntityUtil.isEncrypt));
                zgsProjectunithistoryService.updateUserStatus(zgsProjectunithistory.getUserid());
            }
        }
        return Result.OK("加入黑名单成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "黑名单历史记录-通过id删除")
    @ApiOperation(value = "黑名单历史记录-通过id删除", notes = "黑名单历史记录-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjectunithistoryService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "黑名单历史记录-批量删除")
    @ApiOperation(value = "黑名单历史记录-批量删除", notes = "黑名单历史记录-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsProjectunithistoryService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "黑名单历史记录-通过id查询")
    @ApiOperation(value = "黑名单历史记录-通过id查询", notes = "黑名单历史记录-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsProjectunithistory zgsProjectunithistory = zgsProjectunithistoryService.getById(id);
        if (zgsProjectunithistory == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsProjectunithistory);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsProjectunithistory
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsProjectunithistory zgsProjectunithistory) {
        return super.exportXls(request, zgsProjectunithistory, ZgsProjectunithistory.class, "黑名单历史记录");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjectunithistory.class);
    }

}
