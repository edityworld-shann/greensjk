package org.jeecg.modules.green.largeScreen.po;

import lombok.Data;

import java.io.Serializable;

@Data
public class UtilizationOfRenewableEnergyPo implements Serializable {
    private String areaname;
    /**
     * 本年度新建建筑竣工累计情况-中深层地热能应用项数合计
     */
    private String yearGeothermalHighNumber;
    /**
     * 本年度新建建筑竣工累计情况-中深层地热能应用面积合计
     */
    private String yearGeothermalHighArea;
    /**
     * 本年度新建建筑竣工累计情况-浅层地热能应用项数合计
     */
    private String yearGeothermalNumber;
    /**
     * 本年度新建建筑竣工累计情况-浅层地热能应用面积合计
     */
    private String yearGeothermalArea;
    /**
     * 本年度新建建筑竣工累计情况-太阳能光热应用建筑应用项数合计
     */
    private String yearSolarHighNumber;
    /**
     * 本年度新建建筑竣工累计情况-太阳能光热应用建筑应用容量合计
     */
    private String yearSolarHighArea;
    /**
     * 本年度新建建筑竣工累计情况-太阳能建筑应用项数合计
     */
    private String yearSolarNumber;
    /**
     * 本年度新建建筑竣工累计情况-太阳能建筑应用容量合计
     */
    private String yearSolarArea;
    /**
     * 本年度新建建筑竣工累计情况-应用其他类型技术项数合计
     */
    private String yearOtherNumber;
    /**
     * 本年度新建建筑竣工累计情况-应用其他类型技术面积合计
     */
    private String yearOtherArea;
}
