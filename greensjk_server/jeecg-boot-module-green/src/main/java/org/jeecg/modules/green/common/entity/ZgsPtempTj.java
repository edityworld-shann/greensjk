package org.jeecg.modules.green.common.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 统计报表区县市州用户添加账号匹配表
 * @Author: jeecg-boot
 * @Date: 2022-06-01
 * @Version: V1.0
 */
@Data
@TableName("zgs_ptemp_tj")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "zgs_ptemp_tj对象", description = "统计报表区县市州用户添加账号匹配表")
public class ZgsPtempTj implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键ID")
    private java.lang.String id;
    /**
     * 市州区域
     */
    @Excel(name = "市州区域", width = 15)
    @ApiModelProperty(value = "市州区域")
    private java.lang.String pareaname;
    /**
     * 县区区域
     */
    @Excel(name = "县区区域", width = 15)
    @ApiModelProperty(value = "县区区域")
    private java.lang.String areaname;
    /**
     * 区域代码
     */
    @Excel(name = "区域代码", width = 15)
    @ApiModelProperty(value = "区域代码")
    private java.lang.String areacode;
    /**
     * 人员
     */
    @Excel(name = "人员", width = 15)
    @ApiModelProperty(value = "人员")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String ry;
    /**
     * 部门
     */
    @Excel(name = "部门", width = 15)
    @ApiModelProperty(value = "部门")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String bm;
    /**
     * 姓名
     */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String name;
    /**
     * 职务
     */
    @Excel(name = "职务", width = 15)
    @ApiModelProperty(value = "职务")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String zw;
    /**
     * 固话
     */
    @Excel(name = "固话", width = 15)
    @ApiModelProperty(value = "固话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String tel;
    /**
     * 移动电话
     */
    @Excel(name = "移动电话", width = 15)
    @ApiModelProperty(value = "移动电话")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String phone;
    /**
     * 邮箱
     */
    @Excel(name = "邮箱", width = 15)
    @ApiModelProperty(value = "邮箱")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String email;
    /**
     * QQ
     */
    @Excel(name = "QQ", width = 15)
    @ApiModelProperty(value = "QQ")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String qq;
    /**
     * 是否
     */
    @Excel(name = "是否", width = 15)
    @ApiModelProperty(value = "是否")
    private java.lang.String isNo;
    /**
     * 编码锁
     */
    @Excel(name = "编码锁", width = 15)
    @ApiModelProperty(value = "编码锁")
    private java.lang.String numSuo;
    /**
     * 账号
     */
    @Excel(name = "账号", width = 15)
    @ApiModelProperty(value = "账号")
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String username;
    /**
     * 填充状态
     */
    @Excel(name = "填充状态", width = 15)
    @ApiModelProperty(value = "填充状态")
    private java.lang.String isFlag;
    /**
     * 市=1，县=0
     */
    @Excel(name = "市=1，县=0", width = 15)
    @ApiModelProperty(value = "市=1，县=0")
    private java.lang.String areatype;
    /**
     * 用户表主键
     */
    @Excel(name = "用户表主键", width = 15)
    @ApiModelProperty(value = "用户表主键")
    private java.lang.String userid;
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    private Integer isEncrypt = 1;
}
