package org.jeecg.modules.green.xmyszssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificateexpert;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 示范验收证书验收专家名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsExamplecertificateexpertMapper extends BaseMapper<ZgsExamplecertificateexpert> {

}
