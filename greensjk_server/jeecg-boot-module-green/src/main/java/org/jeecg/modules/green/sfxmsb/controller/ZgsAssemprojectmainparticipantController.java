package org.jeecg.modules.green.sfxmsb.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.sfxmsb.entity.ZgsAssemprojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.service.IZgsAssemprojectmainparticipantService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 装配式示范工程工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Api(tags="装配式示范工程工程项目主要参加人员")
@RestController
@RequestMapping("/sfxmsb/zgsAssemprojectmainparticipant")
@Slf4j
public class ZgsAssemprojectmainparticipantController extends JeecgController<ZgsAssemprojectmainparticipant, IZgsAssemprojectmainparticipantService> {
	@Autowired
	private IZgsAssemprojectmainparticipantService zgsAssemprojectmainparticipantService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsAssemprojectmainparticipant
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-分页列表查询")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-分页列表查询", notes="装配式示范工程工程项目主要参加人员-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper = QueryGenerator.initQueryWrapper(zgsAssemprojectmainparticipant, req.getParameterMap());
		Page<ZgsAssemprojectmainparticipant> page = new Page<ZgsAssemprojectmainparticipant>(pageNo, pageSize);
		IPage<ZgsAssemprojectmainparticipant> pageList = zgsAssemprojectmainparticipantService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsAssemprojectmainparticipant
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-添加")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-添加", notes="装配式示范工程工程项目主要参加人员-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant) {
		zgsAssemprojectmainparticipantService.save(zgsAssemprojectmainparticipant);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param zgsAssemprojectmainparticipant
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-编辑")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-编辑", notes="装配式示范工程工程项目主要参加人员-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant) {
		zgsAssemprojectmainparticipantService.updateById(zgsAssemprojectmainparticipant);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-通过id删除")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-通过id删除", notes="装配式示范工程工程项目主要参加人员-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsAssemprojectmainparticipantService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-批量删除")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-批量删除", notes="装配式示范工程工程项目主要参加人员-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsAssemprojectmainparticipantService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "装配式示范工程工程项目主要参加人员-通过id查询")
	@ApiOperation(value="装配式示范工程工程项目主要参加人员-通过id查询", notes="装配式示范工程工程项目主要参加人员-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant = zgsAssemprojectmainparticipantService.getById(id);
		if(zgsAssemprojectmainparticipant==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(zgsAssemprojectmainparticipant);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsAssemprojectmainparticipant
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant) {
        return super.exportXls(request, zgsAssemprojectmainparticipant, ZgsAssemprojectmainparticipant.class, "装配式示范工程工程项目主要参加人员");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsAssemprojectmainparticipant.class);
    }

}
