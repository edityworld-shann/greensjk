package org.jeecg.modules.green.common.service;

import org.jeecg.modules.green.common.entity.ZgsExpertset;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家评审屏蔽设置表
 * @Author: jeecg-boot
 * @Date:   2022-02-26
 * @Version: V1.0
 */
public interface IZgsExpertsetService extends IService<ZgsExpertset> {

}
