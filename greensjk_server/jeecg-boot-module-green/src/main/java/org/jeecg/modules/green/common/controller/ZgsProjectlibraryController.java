package org.jeecg.modules.green.common.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.demo.poi.view.JeecgTemplateExcelView;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertexpert;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertexpertService;
import org.jeecg.modules.green.zjksb.entity.ZgsExpertinfo;
import org.jeecg.modules.green.zjksb.service.IZgsExpertinfoService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.def.TemplateWordConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 建筑工程项目库
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Api(tags = "建筑工程项目库")
@RestController
@RequestMapping("/common/zgsProjectlibrary")
@Slf4j
public class ZgsProjectlibraryController<T> extends JeecgController<ZgsProjectlibrary, IZgsProjectlibraryService> {

    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;

    @Autowired
    private IZgsGreenprojectplanarrangeService zgsGreenprojectplanarrangeService;
    @Autowired
    private IZgsEnergyprojectplanarrangeService zgsEnergyprojectplanarrangeService;
    @Autowired
    private IZgsAssemblesingleprojectService zgsAssemblesingleprojectService;

    @Autowired
    private IZgsBuildjointunitService zgsBuildjointunitService;
    @Autowired
    private IZgsGreenprojectmainparticipantService zgsGreenprojectmainparticipantService;
    @Autowired
    private IZgsBuildrojectmainparticipantService zgsBuildrojectmainparticipantService;
    @Autowired
    private IZgsSamplerojectmainparticipantService zgsSamplerojectmainparticipantService;
    @Autowired
    private IZgsEnergyprojectmainparticipantService zgsEnergyprojectmainparticipantService;
    @Autowired
    private IZgsAssemprojectmainparticipantService zgsAssemprojectmainparticipantService;

    @Autowired
    private IZgsMattermaterialService zgsMattermaterialService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsBuildfundbudgetService zgsBuildfundbudgetService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsExpertinfoService zgsExpertinfoService;
    @Autowired
    private IZgsExpertsetService zgsExpertsetService;
    @Autowired
    private IZgsProjectlibraryexpertService zgsProjectlibraryexpertService;
    @Autowired
    private IZgsCommonService zgsCommonService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsSacceptcertexpertService zgsSacceptcertexpertService;
    @Autowired
    private IZgsProjecttaskService iZgsProjecttaskService;

    /**
     * 分页列表查询
     *
     * @param zgsProjectlibraryListInfo
     * @param pageNo
     * @param pageSize
     * @param assType                   示范项目5小类类型
     * @param accessType                新增任务书及以下菜单关联查询标识
     * @param req
     * @return
     */
    @AutoLog(value = "建筑工程项目库-分页列表查询")
    @ApiOperation(value = "建筑工程项目库-分页列表查询", notes = "建筑工程项目库-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjectlibraryListInfo zgsProjectlibraryListInfo,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   @RequestParam(name = "assType") Integer assType,
                                   @RequestParam(name = "accessType", required = false) Integer accessType,
                                   @RequestParam(name = "year", required = false) String year,
                                   @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                   @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                   @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsProjectlibrary> queryWrapper = new QueryWrapper<>();
        QueryWrapper<ZgsProjectlibrary> queryWrapperZjk = new QueryWrapper<>();
        if (zgsProjectlibraryListInfo != null) {
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getProjectname())) {
                queryWrapper.like("l.projectname", zgsProjectlibraryListInfo.getProjectname());
                queryWrapperZjk.like("p.projectname", zgsProjectlibraryListInfo.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getApplyunit())) {
                if (assType == 1 || assType == 2 || assType == 3 || assType == 4) {
                    // queryWrapper.eq("l.applyunit", Sm4Util.encryptEcb(zgsProjectlibraryListInfo.getApplyunit()));
                    queryWrapper.eq("l.applyunit", Sm4Util.encryptEcb(zgsProjectlibraryListInfo.getApplyunit())).or().like("l.applyunit", zgsProjectlibraryListInfo.getApplyunit());
                }
                if (assType == 5) {
                    // queryWrapper.eq("l.ownerunit", Sm4Util.encryptEcb(zgsProjectlibraryListInfo.getApplyunit()));
                    queryWrapper.eq("l.ownerunit", Sm4Util.encryptEcb(zgsProjectlibraryListInfo.getApplyunit())).or().like("l.ownerunit", zgsProjectlibraryListInfo.getApplyunit());
                }
            }
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getProjectnum())) {
                queryWrapper.like("l.projectnum", zgsProjectlibraryListInfo.getProjectnum());
            }
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getSfxmtype())) {
                queryWrapper.eq("l.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_1);
                if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getSfxmstatus())) {
                    queryWrapper.eq("l.sfxmstatus", zgsProjectlibraryListInfo.getSfxmstatus());
                }
            } else {
                queryWrapper.and(qwku -> {
                    qwku.eq("l.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_2).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_3).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_2);
                });
            }
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getProjectstage())) {
                if (!"-1".equals(zgsProjectlibraryListInfo.getProjectstage())) {
                    queryWrapper.eq("l.projectstage", QueryWrapperUtil.getProjectstage(zgsProjectlibraryListInfo.getProjectstage()));
                }
            } else {
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3 && StringUtils.isEmpty(zgsProjectlibraryListInfo.getSfxmtype())) {
                    queryWrapper.and(qrt -> {
                        qrt.eq("l.projectstage", QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1)).or().isNull("l.projectstage");
                    });
                }
            }
            if (StringUtils.isNotEmpty(zgsProjectlibraryListInfo.getAgreeproject())) {
                queryWrapper.eq("b.agreeproject", zgsProjectlibraryListInfo.getAgreeproject());
            }

            // QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjectlibraryListInfo.gecommon/zgsProjectlibrarytStatus(), sysUser, 0);
        }
        if (StringUtils.isNotEmpty(year)) {
            //等于
//            queryWrapper.apply("date_format(b.applydate,'%Y')={0}", year);
            //修改位申报年度筛选
            queryWrapper.eq("l.year_num", year);
        }
        String expertId = null;
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_4) {
//                queryWrapper.eq("l.enterpriseguid", sysUser.getEnterpriseguid());
                String unitmemberid = sysUser.getUnitmemberid();
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(unitmemberid);
                String enterprisename = zgsProjectunitmember.getEnterprisename();
                QueryWrapper<ZgsProjectunitmember> wrapper = new QueryWrapper();
                wrapper.eq("higherupunit", enterprisename);
                List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(wrapper);
                if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
                    queryWrapper.ne("b.status", GlobalConstants.SHENHE_STATUS0);
                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < zgsProjectunitmemberList.size(); i++) {
                        stringBuilder.append("'");
                        stringBuilder.append(zgsProjectunitmemberList.get(i).getEnterpriseguid());
                        stringBuilder.append("'");
                        if (i < zgsProjectunitmemberList.size() - 1) {
                            stringBuilder.append(",");
                        }
                    }
                    queryWrapper.and(qwd -> {
                        qwd.inSql("l.enterpriseguid", stringBuilder.toString());
                        if (sysUser.getRealname().contains("城乡建设局")) {
                            qwd.or(q1 -> {
                                //行政区划等于且sfxmtype1项目库、sfxmstatus3已推荐
                                q1.eq("areacode", sysUser.getAreacode());
                                q1.and(q2 -> {
                                    q2.eq("sfxmtype", GlobalConstants.PROJECT_KU_TYPE_1).or().eq("sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_3).or().eq("sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_2);
                                });
                            });
                        } else {
                            qwd.eq("sfxmtype", GlobalConstants.PROJECT_KU_TYPE_2);
                        }
                    });
//                    queryWrapper.and(qw -> {
//                        qw.and(w1 -> {
//                            for (ZgsProjectunitmember projectunitmember : zgsProjectunitmemberList) {
//                                w1.or(w2 -> {
//                                    w2.eq("l.enterpriseguid", projectunitmember.getEnterpriseguid());
//                                });
//                            }
//                        });
//                    });
                } else {
                    queryWrapper.eq("l.enterpriseguid", UUID.randomUUID().toString());
                }
            } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                queryWrapper.eq("l.enterpriseguid", sysUser.getEnterpriseguid());
            } else {
                //专家
                QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                if (zgsExpertinfo != null && StringUtils.isNotEmpty(zgsExpertinfo.getId())) {
                    expertId = zgsExpertinfo.getId();
                    queryWrapperZjk.eq("e.expertguid", expertId);
                    queryWrapperZjk.orderByDesc("p1.applydate");
                }
            }
        } else {
            queryWrapper.isNotNull("b.status");
//            queryWrapper.ne("b.status", GlobalConstants.SHENHE_STATUS0);
            if (StringUtils.isEmpty(zgsProjectlibraryListInfo.getStatus())) {
                queryWrapper.and(qwp2 -> {
                    qwp2.isNotNull("b.finishdate");
                    qwp2.or(f2 -> {
                        f2.ne("b.status", GlobalConstants.SHENHE_STATUS0).ne("b.status", GlobalConstants.SHENHE_STATUS1).ne("status", GlobalConstants.SHENHE_STATUS3).ne("status", GlobalConstants.SHENHE_STATUS13);
                    });
                });
            }
        }
        //关联数据筛选使用accessType默认传1为立项项目
        if (accessType != null) {
            if (accessType == 0) {
                queryWrapper.eq("b.status", GlobalConstants.SHENHE_STATUS2);
            } else {
                queryWrapper.eq("b.agreeproject", GlobalConstants.SHENHE_STATUS2);
            }
        }
        queryWrapper.ne("l.isdelete", 1);
        queryWrapper.ne("b.isdelete", 1);
        //TODO 2022-9-21 统一修改根据初审日期进行降序处理
        boolean flag = true;
        if (sbArrow != null) {
            if (sbArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("b.applydate");
            } else if (sbArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("b.applydate");
            }
        }
        if (wcArrow != null) {
            if (wcArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("l.completedate");
            } else if (wcArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("l.completedate");
            }
        }
        if (csArrow != null) {
            if (csArrow == 0) {
                flag = false;
                queryWrapper.orderByAsc("b.firstdate");
            } else if (csArrow == 1) {
                flag = false;
                queryWrapper.orderByDesc("b.firstdate");
            }
        }
        if (flag) {
            queryWrapper.last("ORDER BY IF(isnull(b.firstdate),0,1), b.firstdate DESC");
        }

        // 推荐单位查看项目统计，状态为空
        IPage<ZgsProjectlibraryListInfo> pageListForProjectCount = null;
        // 推荐单位查看待审批项目 状态为 1(待审核)
        IPage<ZgsProjectlibraryListInfo> pageListForDsp = null;
        // 正常菜单进入，非工作台项目查询
        IPage<ZgsProjectlibraryListInfo> pageList = null;

        switch (assType) {
            case 1:
                // pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);
                // 1 表示从工作台进来，将项目数量存储redis 再更新原有工作台项目数量信息  rxl 20230727
                // 先判度redis是否已存储项目数量，如果未存储则进行redis存储，防止多次从工作台进入菜单走各个阶段的查询接口导致系统变慢
                // 绿色建筑
                redisUtil.expire("lsjz", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("lsjzOfDsp", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("lsjzOfTjdw", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

                // 默认初始进来 查询推荐单位待初审、项目统计数量
                if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw())) {
                    if (zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                        if (!redisUtil.hasKey("lsjz")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                            pageListForProjectCount = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);
                            redisUtil.set("lsjz", pageListForProjectCount.getTotal());
                        }
                    }
                    if (zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                        if (!redisUtil.hasKey("lsjzOfTjdw")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                            pageListForDsp = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);
                            redisUtil.set("lsjzOfTjdw", pageListForDsp.getTotal());
                        }
                    }

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) { // for 省厅待审批项目
                    // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                    // 省厅查看待审批项目信息
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);
                    redisUtil.set("lsjzOfDsp", pageList.getTotal());
                    // }
                } else { // 正常菜单进入
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjectlibraryListInfo.getStatus(), sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo1(queryWrapper, pageNo, pageSize);
                }

                /*redisUtil.expire("lsjz",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("lsjz")) {
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) {
                        redisUtil.set("lsjz",pageList.getTotal());
                    }
                }
                redisUtil.expire("lsjzOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("lsjzOfDsp")) {  // for 省厅待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) {
                        redisUtil.set("lsjzOfDsp",pageList.getTotal());
                    }
                }
                redisUtil.expire("lsjzOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("lsjzOfTjdw")) {  // for 推荐单位待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {
                        redisUtil.set("lsjzOfTjdw",pageList.getTotal());
                    }
                }*/
                break;
            case 2: // 建筑工程
                // rxl 20230506  修改我的工作台_示范项目申报管理_建筑工程示示范 点击“查看” 去掉不立项 项目
                /*if (StringUtils.isEmpty(zgsProjectlibraryListInfo.getProjectstage())) {
                    queryWrapper.ne("b.agreeproject",1);
                }*/
                // pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);
                redisUtil.expire("jzgc", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("jzgcOfDsp", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("jzgcOfTjdw", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

                // 默认初始进来 查询推荐单位待初审、项目统计数量
                if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw())) {
                    if (zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                        if (!redisUtil.hasKey("jzgc")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                            pageListForProjectCount = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);
                            redisUtil.set("jzgc", pageListForProjectCount.getTotal());
                        }
                    }
                    if (zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                        if (!redisUtil.hasKey("jzgcOfTjdw")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                            pageListForDsp = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);
                            redisUtil.set("jzgcOfTjdw", pageListForDsp.getTotal());
                        }
                    }

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) { // for 省厅待审批项目
                    // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                    // 省厅查看待审批项目信息
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);
                    redisUtil.set("jzgcOfDsp", pageList.getTotal());
                    // }
                } else { // 正常菜单进入
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjectlibraryListInfo.getStatus(), sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo2(queryWrapper, pageNo, pageSize);
                }

               /* // 建筑工程
                redisUtil.expire("jzgc",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzgc")) {  // StringUtils.isNotBlank() &&
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) {
                        redisUtil.set("jzgc",pageList.getTotal());
                    }
                }
                redisUtil.expire("jzgcOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzgcOfDsp")) {  // for 省厅待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) {
                        redisUtil.set("jzgcOfDsp",pageList.getTotal());
                    }
                }
                redisUtil.expire("jzgcOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzgcOfTjdw")) {  // for 推荐单位待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {
                        redisUtil.set("jzgcOfTjdw",pageList.getTotal());
                    }
                }*/
                break;
            case 3:
                pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo3(queryWrapper, pageNo, pageSize);
                break;
            case 4:
                // pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);
                // 建筑节能
                redisUtil.expire("jzjn", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("jzjnOfDsp", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("jzjnOfTjdw", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

                // 默认初始进来 查询推荐单位待初审、项目统计数量
                if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw())) {
                    if (zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                        if (!redisUtil.hasKey("jzjn")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                            pageListForProjectCount = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);
                            redisUtil.set("jzjn", pageListForProjectCount.getTotal());
                        }
                    }
                    if (zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                        if (!redisUtil.hasKey("jzjnOfTjdw")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                            pageListForDsp = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);
                            redisUtil.set("jzjnOfTjdw", pageListForDsp.getTotal());
                        }
                    }

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) { // for 省厅待审批项目
                    // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                    // 省厅查看待审批项目信息
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);
                    redisUtil.set("jzjnOfDsp", pageList.getTotal());
                    // }
                } else { // 正常菜单进入
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjectlibraryListInfo.getStatus(), sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo4(queryWrapper, pageNo, pageSize);
                }

                /*// 建筑节能
                redisUtil.expire("jzjn",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzjn")) {
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) {
                        redisUtil.set("jzjn",pageList.getTotal());
                    }
                }
                redisUtil.expire("jzjnOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzjnOfDsp")) {  // for 省厅待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) {
                        redisUtil.set("jzjnOfDsp",pageList.getTotal());
                    }
                }
                redisUtil.expire("jzjnOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("jzjnOfTjdw")) {  // for 推荐单位待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {
                        redisUtil.set("jzjnOfTjdw",pageList.getTotal());
                    }
                }*/
                break;
            case 5:
                // pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);

                // 装配式建筑
                redisUtil.expire("zpsjz", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("zpsjzOfDsp", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                redisUtil.expire("zpsjzOfTjdw", 60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）

                // 默认初始进来 查询推荐单位待初审、项目统计数量
                if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw())) {
                    if (zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) { // for 推荐单位下的 项目统计
                        if (!redisUtil.hasKey("zpsjz")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                            pageListForProjectCount = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);
                            redisUtil.set("zpsjz", pageListForProjectCount.getTotal());
                        }
                    }
                    if (zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {  // for 推荐单位待审批项目
                        if (!redisUtil.hasKey("zpsjzOfTjdw")) {
                            QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                            pageListForDsp = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);
                            redisUtil.set("zpsjzOfTjdw", pageListForDsp.getTotal());
                        }
                    }

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位点击查看 查询待审批项目（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS1, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable())) {  // 推荐单位查看 项目统计详情信息（查看列表）
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, "", sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);

                } else if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) { // for 省厅待审批项目
                    // if (!redisUtil.hasKey("sfxmrwsOfDsp")) { // 将此段代码禁掉，防止点击查看按钮 因redis查看不了列表
                    // 省厅查看待审批项目信息
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, GlobalConstants.SHENHE_STATUS4, sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);
                    redisUtil.set("zpsjzOfDsp", pageList.getTotal());
                    // }
                } else { // 正常菜单进入
                    QueryWrapperUtil.initCommonQueryWrapper(queryWrapper, zgsProjectlibraryListInfo.getStatus(), sysUser, 0);
                    pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo5(queryWrapper, pageNo, pageSize);
                }


                /*// 装配式建筑
                redisUtil.expire("zpsjz",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("zpsjz")) {
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getFlagByWorkTable()) && zgsProjectlibraryListInfo.getFlagByWorkTable().equals("1")) {
                        redisUtil.set("zpsjz",pageList.getTotal());
                    }
                }
                redisUtil.expire("zpsjzOfDsp",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("zpsjzOfDsp")) {  // for 省厅待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForSt()) && zgsProjectlibraryListInfo.getDspForSt().equals("2")) {
                        redisUtil.set("zpsjzOfDsp",pageList.getTotal());
                    }
                }
                redisUtil.expire("zpsjzOfTjdw",60);  // 设置redis过期时间，对key失效从新缓存新数据（防止有新数据上报未能及时更新）
                if (!redisUtil.hasKey("zpsjzOfTjdw")) {  // for 推荐单位待审批项目
                    if (StringUtils.isNotBlank(zgsProjectlibraryListInfo.getDspForTjdw()) && zgsProjectlibraryListInfo.getDspForTjdw().equals("3")) {
                        redisUtil.set("zpsjzOfTjdw",pageList.getTotal());
                    }
                }*/
                break;
            case 6:
                pageList = zgsProjectlibraryService.listZgsProjectlibraryListInfo6(queryWrapperZjk, pageNo, pageSize);
                break;
        }
        //上报单位spStatus==0、初审spStatus==1、专家审批spStatus==2、终审spStatus==4
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_3) {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsProjectlibraryListInfo projectlibraryListInfo = pageList.getRecords().get(m);
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    }
                    if (sysUser.getEnterpriseguid().equals(projectlibraryListInfo.getEnterpriseguid())) {
                        //作为上报本单位
                        pageList.getRecords().get(m).setSpStatus(0);
                    } else {
                        //作为审批本单位
                        //逐级退回新增显示审批按钮逻辑（省厅退回或驳回）
                        if (GlobalConstants.SHENHE_STATUS1.equals(projectlibraryListInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS5.equals(projectlibraryListInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS9.equals(projectlibraryListInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS14.equals(projectlibraryListInfo.getStatus())
                                || GlobalConstants.SHENHE_STATUS15.equals(projectlibraryListInfo.getStatus())
                        ) {
                            pageList.getRecords().get(m).setSpStatus(1);
                        }
                    }
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                        //专家审批
                        QueryWrapper<ZgsProjectlibraryexpert> wrapper = new QueryWrapper();
                        wrapper.eq("expertguid", expertId);
                        wrapper.eq("businessguid", projectlibraryListInfo.getBid());
                        ZgsProjectlibraryexpert zgsProjectlibraryexpert = zgsProjectlibraryexpertService.getOne(wrapper);
                        if (!(zgsProjectlibraryexpert != null && StringUtils.isNotEmpty(zgsProjectlibraryexpert.getAuditconclusionnum()))) {
                            pageList.getRecords().get(m).setSpStatus(2);
                        }
                    }
                    // 如果当前登录角色为个人或单位时 根据授权标识判断是否可进行编辑操作
                    if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
                        if (StrUtil.isNotBlank(projectlibraryListInfo.getSqbj()) && projectlibraryListInfo.getSqbj().equals("1")) {
                            projectlibraryListInfo.setUpdateFlag("1");   // 可编辑
                            projectlibraryListInfo.setSqbjBol(false);
                        } else if (StrUtil.isNotBlank(projectlibraryListInfo.getSqbj()) && projectlibraryListInfo.getSqbj().equals("2")) {
                            projectlibraryListInfo.setUpdateFlag("2");  // 取消编辑按钮
                            projectlibraryListInfo.setSqbjBol(true);
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(projectlibraryListInfo.getUpdateSave()) && projectlibraryListInfo.getUpdateSave().equals("1")) {  // 修改未提交
                        projectlibraryListInfo.setBjzt("1");
                    } else if (StrUtil.isNotBlank(projectlibraryListInfo.getUpdateSave()) && projectlibraryListInfo.getUpdateSave().equals("2")) {  // 修改已提交
                        projectlibraryListInfo.setBjzt("2");
                    }
                }
            }
        } else {
            if (ObjectUtil.isNotNull(pageList) && pageList.getRecords().size() > 0) {
                for (int m = 0; m < pageList.getRecords().size(); m++) {
                    ZgsProjectlibraryListInfo projectlibraryListInfo = pageList.getRecords().get(m);
                    if (StringUtils.isEmpty(pageList.getRecords().get(m).getProjectstage())) {
                        pageList.getRecords().get(m).setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    }
                    if (GlobalConstants.SHENHE_STATUS4.equals(projectlibraryListInfo.getStatus())) {
                        //形审
                        pageList.getRecords().get(m).setSpStatus(4);
                    } else if (GlobalConstants.SHENHE_STATUS8.equals(projectlibraryListInfo.getStatus())) {
                        //分配专家
                        pageList.getRecords().get(m).setSpStatus(3);
                        if (StringUtils.isNotEmpty(projectlibraryListInfo.getAgreeproject())) {
                            if (projectlibraryListInfo.getProvincialfundtotal() == null) {
                                if ("2".equals(projectlibraryListInfo.getAgreeproject()) && StringUtils.isNotEmpty(projectlibraryListInfo.getYearNum())) {
                                    if (Integer.parseInt(projectlibraryListInfo.getYearNum()) > 2022) {
                                        pageList.getRecords().get(m).setMoneyStatus(1);
                                    }
                                }
                            }
                        }
                    } else if (GlobalConstants.SHENHE_STATUS12.equals(projectlibraryListInfo.getStatus())) {
                        //是否显示立项按钮操作
                        pageList.getRecords().get(m).setSpStatus(5);
                    }
                    // 根据项目申报单位与项目负责人信息 查询所属过往申报期间是否存在逾期项目   rxl
                    if (StrUtil.isNotBlank(projectlibraryListInfo.getEnterpriseguid()) && StrUtil.isNotBlank(projectlibraryListInfo.getApplyunitprojectleader())) {
                        QueryWrapper<ZgsProjecttask> queryWrapperForYq = new QueryWrapper<>();
                        queryWrapperForYq.eq("p.enterpriseguid", projectlibraryListInfo.getEnterpriseguid());
                        queryWrapperForYq.eq("p.applyunitprojectleader", projectlibraryListInfo.getApplyunitprojectleader());
                        queryWrapperForYq.eq("t.STATUS", 8);
                        queryWrapperForYq.isNotNull("p.projectname");
                        // queryWrapperForYq.eq("t.dealtype","0").or().eq("t.dealtype","1");
                        List<ZgsProjecttask> listForYq = iZgsProjecttaskService.zgsProjectTaskListForYq(queryWrapperForYq);
                        if (ObjectUtil.isNotNull(listForYq) && listForYq.size() > 0) {
                            projectlibraryListInfo.setYqProjectList(listForYq);
                            projectlibraryListInfo.setYqFlag("1"); // 存在逾期
                            projectlibraryListInfo.setSqbjBol(true);
                        } else {
                            projectlibraryListInfo.setYqProjectList(listForYq);
                            projectlibraryListInfo.setYqFlag("0"); // 不存在逾期
                        }
                    }
                    // 列表增加“编辑状态”字段
                    if (StrUtil.isNotBlank(projectlibraryListInfo.getUpdateSave()) && projectlibraryListInfo.getUpdateSave().equals("1")) {  // 修改未提交
                        projectlibraryListInfo.setBjzt("1");  // 修改未提交
                    } else if (StrUtil.isNotBlank(projectlibraryListInfo.getUpdateSave()) && projectlibraryListInfo.getUpdateSave().equals("2")) {  // 修改已提交
                        projectlibraryListInfo.setBjzt("2");  // 修改已提交
                    }

                    if (StrUtil.isNotBlank(projectlibraryListInfo.getSqbj()) && projectlibraryListInfo.getSqbj().equals("1")) {  // 授权编辑
                        projectlibraryListInfo.setSqbjBol(false);  // 授权编辑
                        projectlibraryListInfo.setUpdateFlag("1");  //可编辑
                    } else if (StrUtil.isNotBlank(projectlibraryListInfo.getSqbj()) && projectlibraryListInfo.getSqbj().equals("2")) {  // 取消编辑
                        projectlibraryListInfo.setSqbjBol(true);  // 取消编辑
                        projectlibraryListInfo.setUpdateFlag("2");  // 不可编辑
                    }


                }
            }
        }
        return Result.OK(pageList);
    }


    /**
     * 查询当前项目编码前缀
     *
     * @return
     */
    @AutoLog(value = "查询当前项目编码前缀")
    @ApiOperation(value = "查询当前项目编码前缀")
    @GetMapping(value = "/queryApplyYear")
    public Result<?> queryApplyYear() {
        String projectNum = "JK" + zgsCommonService.queryApplyYear() + "-";
        return Result.OK(projectNum);
    }

    /**
     * 关联查询-当前用户下的示范项目和科技项目全量列表查询
     *
     * @return
     */
    @AutoLog(value = "联查询-当前用户下的示范项目和科技项目全量列表查询")
    @ApiOperation(value = "联查询-当前用户下的示范项目和科技项目全量列表查询", notes = "联查询-当前用户下的示范项目和科技项目全量列表查询")
    @GetMapping(value = "/queryProjectParentInfoList")
    public Result<?> queryProjectParentInfoList(@RequestParam(name = "accessType", required = true) Integer accessType) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsProjectParentInfo> queryWrapper = new QueryWrapper<>();
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_1 || sysUser.getLoginUserType() == GlobalConstants.loginUserType_0) {
            queryWrapper.eq("m.enterpriseguid", sysUser.getEnterpriseguid());
        }
        queryWrapper.ne("m.isdelete", 1);
        queryWrapper.isNotNull("m.id");
        queryWrapper.isNotNull("m.projectname");
        queryWrapper.isNotNull("m.projecttypenum");
        queryWrapper.and(qwd -> {
            qwd.isNull("m.ishistory").or().eq("m.ishistory", "0");
        });
        //只有立项才显示
        queryWrapper.eq("m.agreeproject", "2");
        queryWrapper.orderByDesc("projecttypenum");
        return Result.OK(zgsProjectlibraryService.listZgsProjectParentInfo(queryWrapper, accessType));
    }

    /**
     * 查询示范项目+科研项目列表-新增时用
     *
     * @return
     */
    @AutoLog(value = "查询示范项目+科研项目列表-申报单位新增时用")
    @ApiOperation(value = "查询示范项目+科研项目列表-申报单位新增时用", notes = "查询示范项目+科研项目列表-申报单位新增时用")
    @GetMapping(value = "/queryProjectParentInfoListByAccessType")
    public Result<?> queryProjectParentInfoListByAccessType(@RequestParam(name = "accessType", required = true) Integer accessType) {
        return Result.OK(zgsProjectlibraryService.listZgsProjectParentInfoByAccessType(accessType));
    }

    /**
     * 添加
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "建筑工程项目库-添加")
    @ApiOperation(value = "建筑工程项目库-添加", notes = "建筑工程项目库-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjectlibrary zgsProjectlibrary) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String id = UUID.randomUUID().toString();
        String stagestatus = null;
        zgsProjectlibrary.setId(id);
        zgsProjectlibrary.setEnterpriseguid(sysUser.getEnterpriseguid());
        zgsProjectlibrary.setCreateaccountname(sysUser.getUsername());
        zgsProjectlibrary.setCreateusername(sysUser.getRealname());
        zgsProjectlibrary.setCreatedate(new Date());
        zgsProjectlibrary.setAreacode(sysUser.getAreacode());
        zgsProjectlibrary.setAreaname(sysUser.getAreaname());
        // zgsProjectlibrary.setYearNum(GlobalConstants.PROJECT_NUM_YEAR);
        zgsProjectlibrary.setYearNum(zgsCommonService.queryApplyYear());
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getSfxmtype())) {
            if ("1".equals(zgsProjectlibrary.getSfxmtype())) {
                zgsProjectlibrary.setSfxmstatus("1");
            }
        }
        //计算当年年份
        int year = DateUtils.getYearCurrent();
        //取字典表的值
        //项目类型projecttype、示范类别demonstratetype、建筑类型projectbuildtype、建筑用途类型projectpurpose、投资类型编码projectinvesttype、项目目前进展情况projectprogress
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
            zgsProjectlibrary.setProjecttype(zgsCommonService.queryDictTextByKey("DemonstrateType", zgsProjectlibrary.getProjecttypenum()));
        }
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getDemonstratetypenum())) {
            zgsProjectlibrary.setDemonstratetype(zgsCommonService.queryDictTextByKey(zgsProjectlibrary.getProjecttypenum(), zgsProjectlibrary.getDemonstratetypenum()));
        }
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjectbuildtypenum())) {
            zgsProjectlibrary.setProjectbuildtype(zgsCommonService.queryDictTextByKey("ProjectBuildType", zgsProjectlibrary.getProjectbuildtypenum()));
        }
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjectpurposenum())) {
            zgsProjectlibrary.setProjectpurpose(zgsCommonService.queryDictTextByKey("ProjectPurposeType", zgsProjectlibrary.getProjectpurposenum()));
        }
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjectinvesttypenum())) {
            zgsProjectlibrary.setProjectinvesttype(zgsCommonService.queryDictTextByKey("ProjectInvestType", zgsProjectlibrary.getProjectinvesttypenum()));
        }
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjectprogressnum())) {
            zgsProjectlibrary.setProjectprogress(zgsCommonService.queryDictTextByKey("ProjectProgress", zgsProjectlibrary.getProjectprogressnum()));
        }
        //
        String unit = zgsProjectlibrary.getApplyunit();
        String leader = zgsProjectlibrary.getApplyunitprojectleader();
        Date startdate = zgsProjectlibrary.getStartdate();
        Date dateStart = null;
        int mesNum = 0;
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
            if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                unit = zgsProjectlibrary.getOwnerunit();
                leader = zgsProjectlibrary.getOwnercharger();
                dateStart = zgsProjectlibrary.getProjectstartdate();
            }
        }

        if (ObjectUtil.isNotEmpty(startdate)) {
            mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader("", unit, leader, startdate, 1);
        } else {
            mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader("", unit, leader, dateStart, 1);
        }
        if (mesNum != -1) {
//            return Result.error("在研阶段，项目负责人仅可上报2个项目！");
            return Result.error(GreenUtilSelf.getMesByNum(mesNum));
        }
//        if (zgsSciencetechfeasibleService.verificationUnitAndProjectLeader(zgsProjectlibrary.getApplyunit(), zgsProjectlibrary.getApplyunitprojectleader())) {
//            return Result.error("申报阶段，申报单位及项目负责人仅可上报2个项目！");
//        }
//        redisUtil.set(zgsProjectlibrary.getId(), "申报阶段");
        String bid = UUID.randomUUID().toString();
        if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
            if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                ZgsGreenbuildproject zgsGreenbuildproject = zgsProjectlibrary.getZgsGreenbuildproject();
                zgsGreenbuildproject.setId(bid);
                zgsGreenbuildproject.setEnterpriseguid(sysUser.getEnterpriseguid());
                if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsGreenbuildproject().getStatus())) {
                    //保存并上报
                    zgsGreenbuildproject.setApplydate(new Date());
                    if (!VerificationUtil.projectLeaderVerification(leader)) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                }
                //做一个限制，当年只能申报一个项目
                QueryWrapper<ZgsGreenbuildproject> qwd01 = new QueryWrapper<ZgsGreenbuildproject>();
                qwd01.eq("enterpriseguid", sysUser.getEnterpriseguid());
                qwd01.apply("date_format(applydate,'%Y')={0}", year);
//                List<ZgsGreenbuildproject> listQwd01 = zgsGreenbuildprojectService.list(qwd01);
//                if (listQwd01.size() > 0) {
//                    return Result.error("每年限申报1项绿色建筑示范项目！");
//                }
                zgsGreenbuildproject.setProjectlibraryguid(id);
                zgsGreenbuildproject.setCreateaccountname(sysUser.getUsername());
                zgsGreenbuildproject.setCreateusername(sysUser.getRealname());
                zgsGreenbuildproject.setCreatedate(new Date());
                stagestatus = zgsGreenbuildproject.getStatus();
                zgsGreenbuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenbuildproject, ValidateEncryptEntityUtil.isEncrypt);
                zgsGreenbuildprojectService.save(zgsGreenbuildproject);
                //项目主要参加人员
                List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList = zgsProjectlibrary.getZgsGreenprojectmainparticipantList();
                if (zgsGreenprojectmainparticipantList != null && zgsGreenprojectmainparticipantList.size() > 0) {
                    if (zgsGreenprojectmainparticipantList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    for (int a0 = 0; a0 < zgsGreenprojectmainparticipantList.size(); a0++) {
                        ZgsGreenprojectmainparticipant zgsGreenprojectmainparticipant = zgsGreenprojectmainparticipantList.get(a0);
                        zgsGreenprojectmainparticipant.setId(UUID.randomUUID().toString());
                        zgsGreenprojectmainparticipant.setBuildguid(bid);
                        zgsGreenprojectmainparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsGreenprojectmainparticipant.setCreateaccountname(sysUser.getUsername());
                        zgsGreenprojectmainparticipant.setCreateusername(sysUser.getRealname());
                        zgsGreenprojectmainparticipant.setCreatedate(new Date());
                        zgsGreenprojectmainparticipant = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectmainparticipant, ValidateEncryptEntityUtil.isEncrypt);
                        zgsGreenprojectmainparticipantService.save(zgsGreenprojectmainparticipant);
                    }
                }
                //计划进度与安排
                List<ZgsGreenprojectplanarrange> zgsGreenprojectplanarrangeList = zgsProjectlibrary.getZgsGreenprojectplanarrangeList();
                if (zgsGreenprojectplanarrangeList != null && zgsGreenprojectplanarrangeList.size() > 0) {
                    for (int a0 = 0; a0 < zgsGreenprojectplanarrangeList.size(); a0++) {
                        ZgsGreenprojectplanarrange zgsGreenprojectplanarrange = zgsGreenprojectplanarrangeList.get(a0);
                        zgsGreenprojectplanarrange.setId(UUID.randomUUID().toString());
                        zgsGreenprojectplanarrange.setBuildguid(bid);
                        zgsGreenprojectplanarrange.setEnterpriseguid(sysUser.getEnterpriseguid());
                        zgsGreenprojectplanarrange.setCreateaccountname(sysUser.getUsername());
                        zgsGreenprojectplanarrange.setCreateusername(sysUser.getRealname());
                        zgsGreenprojectplanarrange.setCreatedate(new Date());
                        zgsGreenprojectplanarrange = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectplanarrange, ValidateEncryptEntityUtil.isEncrypt);
                        zgsGreenprojectplanarrangeService.save(zgsGreenprojectplanarrange);
                    }
                }
            } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                ZgsBuildproject zgsBuildproject = zgsProjectlibrary.getZgsBuildproject();
                zgsBuildproject.setId(bid);
                zgsBuildproject.setEnterpriseguid(sysUser.getEnterpriseguid());
                if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsBuildproject().getStatus())) {
                    //保存并上报
                    zgsBuildproject.setApplydate(new Date());
                    if (!VerificationUtil.projectLeaderVerification(leader)) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                }
                zgsBuildproject.setProjectlibraryguid(id);
                zgsBuildproject.setCreateaccountname(sysUser.getUsername());
                zgsBuildproject.setCreateusername(sysUser.getRealname());
                zgsBuildproject.setCreatedate(new Date());
                stagestatus = zgsBuildproject.getStatus();
                zgsBuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildproject, ValidateEncryptEntityUtil.isEncrypt);
                zgsBuildprojectService.save(zgsBuildproject);
                //项目主要参加人员
                List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList = zgsProjectlibrary.getZgsBuildrojectmainparticipantList();
                if (zgsBuildrojectmainparticipantList != null && zgsBuildrojectmainparticipantList.size() > 0) {
                    if (zgsBuildrojectmainparticipantList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    for (int a0 = 0; a0 < zgsBuildrojectmainparticipantList.size(); a0++) {
                        ZgsBuildrojectmainparticipant plb = zgsBuildrojectmainparticipantList.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsBuildrojectmainparticipantService.save(plb);
                    }
                }
            } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                ZgsAssembleproject zgsAssembleproject = zgsProjectlibrary.getZgsAssembleproject();
                zgsAssembleproject.setId(bid);
                zgsAssembleproject.setEnterpriseguid(sysUser.getEnterpriseguid());
                if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsAssembleproject().getStatus())) {
                    //保存并上报
                    zgsAssembleproject.setApplydate(new Date());
                    if (!VerificationUtil.projectLeaderVerification(leader)) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                }
                zgsAssembleproject.setProjectlibraryguid(id);
                zgsAssembleproject.setCreateaccountname(sysUser.getUsername());
                zgsAssembleproject.setCreateusername(sysUser.getRealname());
                zgsAssembleproject.setCreatedate(new Date());
                stagestatus = zgsAssembleproject.getStatus();
                zgsAssembleproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsAssembleproject, ValidateEncryptEntityUtil.isEncrypt);
                zgsAssembleprojectService.save(zgsAssembleproject);
                //项目主要参加人员0建设单位负责人
                List<ZgsAssemprojectmainparticipant> plbList = zgsProjectlibrary.getZgsAssemprojectmainparticipantList0();
                if (plbList != null && plbList.size() > 0) {
                    if (plbList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    for (int a0 = 0; a0 < plbList.size(); a0++) {
                        ZgsAssemprojectmainparticipant plb = plbList.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setPersontype("0");
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsAssemprojectmainparticipantService.save(plb);
                    }
                }
                //项目主要参加人员联合申报单位负责人
                List<ZgsAssemprojectmainparticipant> plbList1 = zgsProjectlibrary.getZgsAssemprojectmainparticipantList1();
                if (plbList1 != null && plbList1.size() > 0) {
                    if (plbList1.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    for (int a0 = 0; a0 < plbList1.size(); a0++) {
                        ZgsAssemprojectmainparticipant plb = plbList1.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setPersontype("1");
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsAssemprojectmainparticipantService.save(plb);
                    }
                }
                //项目主要参加人员联合申报单位负责人
                List<ZgsAssemblesingleproject> plbList2 = zgsProjectlibrary.getZgsAssemblesingleprojectList();
                if (plbList2 != null && plbList2.size() > 0) {
                    for (int a0 = 0; a0 < plbList2.size(); a0++) {
                        ZgsAssemblesingleproject plb = plbList2.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsAssemblesingleprojectService.save(plb);
                    }
                }
            } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                ZgsEnergybuildproject zgsEnergybuildproject = zgsProjectlibrary.getZgsEnergybuildproject();
                zgsEnergybuildproject.setId(bid);
                zgsEnergybuildproject.setEnterpriseguid(sysUser.getEnterpriseguid());
                if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsEnergybuildproject().getStatus())) {
                    //保存并上报
                    zgsEnergybuildproject.setApplydate(new Date());
                    if (!VerificationUtil.projectLeaderVerification(leader)) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                }
                zgsEnergybuildproject.setProjectlibraryguid(id);
                zgsEnergybuildproject.setCreateaccountname(sysUser.getUsername());
                zgsEnergybuildproject.setCreateusername(sysUser.getRealname());
                zgsEnergybuildproject.setCreatedate(new Date());
                stagestatus = zgsEnergybuildproject.getStatus();
                zgsEnergybuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsEnergybuildproject, ValidateEncryptEntityUtil.isEncrypt);
                zgsEnergybuildprojectService.save(zgsEnergybuildproject);
                //项目主要参加人员
                List<ZgsEnergyprojectmainparticipant> plbList = zgsProjectlibrary.getZgsEnergyprojectmainparticipantList();
                if (plbList != null && plbList.size() > 0) {
                    for (int a0 = 0; a0 < plbList.size(); a0++) {
                        ZgsEnergyprojectmainparticipant plb = plbList.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsEnergyprojectmainparticipantService.save(plb);
                    }
                }
                //计划进度与安排
                List<ZgsEnergyprojectplanarrange> plbList1 = zgsProjectlibrary.getZgsEnergyprojectplanarrangeList();
                if (plbList1 != null && plbList1.size() > 0) {
                    for (int a0 = 0; a0 < plbList1.size(); a0++) {
                        ZgsEnergyprojectplanarrange plb = plbList1.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsEnergyprojectplanarrangeService.save(plb);
                    }
                }
            } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                ZgsSamplebuildproject zgsSamplebuildproject = zgsProjectlibrary.getZgsSamplebuildproject();
                zgsSamplebuildproject.setId(bid);
                zgsSamplebuildproject.setEnterpriseguid(sysUser.getEnterpriseguid());
                if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsSamplebuildproject().getStatus())) {
                    //保存并上报
                    zgsSamplebuildproject.setApplydate(new Date());
                    if (!VerificationUtil.projectLeaderVerification(leader)) {
                        return Result.error("项目负责人名称填写不规范！");
                    }
                }
                zgsSamplebuildproject.setProjectlibraryguid(id);
                zgsSamplebuildproject.setCreateaccountname(sysUser.getUsername());
                zgsSamplebuildproject.setCreateusername(sysUser.getRealname());
                zgsSamplebuildproject.setCreatedate(new Date());
                stagestatus = zgsSamplebuildproject.getStatus();
                zgsSamplebuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsSamplebuildproject, ValidateEncryptEntityUtil.isEncrypt);
                zgsSamplebuildprojectService.save(zgsSamplebuildproject);
                //项目主要参加人员
                List<ZgsSamplerojectmainparticipant> plbList = zgsProjectlibrary.getZgsSamplerojectmainparticipantList();
                if (plbList != null && plbList.size() > 0) {
                    if (plbList.size() > 15) {
                        return Result.error("人员名单数量不得超过15人！");
                    }
                    for (int a0 = 0; a0 < plbList.size(); a0++) {
                        ZgsSamplerojectmainparticipant plb = plbList.get(a0);
                        plb.setId(UUID.randomUUID().toString());
                        plb.setBuildguid(bid);
                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                        plb.setCreateaccountname(sysUser.getUsername());
                        plb.setCreateusername(sysUser.getRealname());
                        plb.setCreatedate(new Date());
                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                        zgsSamplerojectmainparticipantService.save(plb);
                    }
                }
            }
        }

        //add  添加验收专家名单  20230616  rxl
        List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsProjectlibrary.getZgsSacceptcertexpertList();
        if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
            for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                ZgsSacceptcertexpert zgsSacceptcertexpert = zgsSacceptcertexpertList.get(a1);
                zgsSacceptcertexpert.setId(UUID.randomUUID().toString());
                zgsSacceptcertexpert.setBaseguid(id);
                zgsSacceptcertexpert.setProjectStage("sbjd");   // 申报阶段
                zgsSacceptcertexpert.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsSacceptcertexpert.setCreateaccountname(sysUser.getUsername());
                zgsSacceptcertexpert.setCreateusername(sysUser.getRealname());
                zgsSacceptcertexpert.setCreatedate(new Date());
                zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpert, ValidateEncryptEntityUtil.isEncrypt));
            }
        }

        //在此新增主表内容
        zgsProjectlibrary.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
        zgsProjectlibrary.setProjectstagestatus(stagestatus);
        zgsProjectlibrary = ValidateEncryptEntityUtil.validateEncryptObject(zgsProjectlibrary, ValidateEncryptEntityUtil.isEncrypt);
        zgsProjectlibraryService.save(zgsProjectlibrary);
        //联合申报单位
        List<ZgsBuildjointunit> zgsBuildjointunitList = zgsProjectlibrary.getZgsBuildjointunitList();
        if (zgsBuildjointunitList != null && zgsBuildjointunitList.size() > 0) {
            for (int a0 = 0; a0 < zgsBuildjointunitList.size(); a0++) {
                ZgsBuildjointunit zgsBuildjointunit = zgsBuildjointunitList.get(a0);
                zgsBuildjointunit.setId(UUID.randomUUID().toString());
                zgsBuildjointunit.setBuildguid(bid);
                zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                zgsBuildjointunit.setCreatedate(new Date());
                zgsBuildjointunit = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt);
                zgsBuildjointunitService.save(zgsBuildjointunit);
            }
        }
        //经费预算
        List<ZgsBuildfundbudget> zgsBuildfundbudgetList = zgsProjectlibrary.getZgsBuildfundbudgetList();
        if (zgsBuildfundbudgetList != null && zgsBuildfundbudgetList.size() > 0) {
            for (int a0 = 0; a0 < zgsBuildfundbudgetList.size(); a0++) {
                ZgsBuildfundbudget zgsBuildfundbudget = zgsBuildfundbudgetList.get(a0);
                zgsBuildfundbudget.setId(UUID.randomUUID().toString());
                zgsBuildfundbudget.setBuildguid(bid);
                zgsBuildfundbudget.setEnterpriseguid(sysUser.getEnterpriseguid());
                zgsBuildfundbudget.setCreateaccountname(sysUser.getUsername());
                zgsBuildfundbudget.setCreateusername(sysUser.getRealname());
                zgsBuildfundbudget.setCreatedate(new Date());
                zgsBuildfundbudget = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildfundbudget, ValidateEncryptEntityUtil.isEncrypt);
                zgsBuildfundbudgetService.save(zgsBuildfundbudget);
            }
        }
        //相关附件
        List<ZgsMattermaterial> zgsMattermaterialList = zgsProjectlibrary.getZgsMattermaterialList();
        if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
            for (int a0 = 0; a0 < zgsMattermaterialList.size(); a0++) {
                String mattermaterialId = UUID.randomUUID().toString();
                ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a0);
                zgsMattermaterial.setId(mattermaterialId);
                zgsMattermaterial.setBuildguid(bid);
                zgsMattermaterialService.save(zgsMattermaterial);
                if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a1 = 0; a1 < zgsMattermaterial.getZgsAttachappendixList().size(); a1++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a1);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(mattermaterialId);
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(zgsProjectlibrary.getProjecttypenum());
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendix = ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt);
                        zgsAttachappendixService.save(zgsAttachappendix);
                    }
                }
            }
        }
        return Result.OK("添加成功！");
    }


    @AutoLog(value = "添加专家意见")
    @ApiOperation(value = "添加专家意见", notes = "添加专家意见")
    @PostMapping(value = "/addExpertMatterial")
    public Result<?> addExpertMatterial(@RequestBody ZgsMattermaterial zgsMattermaterial) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        QueryWrapper<ZgsMattermaterial> queryWrapper = new QueryWrapper();
        queryWrapper.eq("materialid", zgsMattermaterial.getMaterialid());
        queryWrapper.eq("buildguid", zgsMattermaterial.getBuildguid());
        String delType = zgsMattermaterial.getType();  // 判断附件是否为上传还是删除
        List<ZgsMattermaterial> list = zgsMattermaterialService.list(queryWrapper);
        if (list.size() > 0) {
            // 删除原有所有附件，保存最新附件
            String relationid = list.get(0).getId();
            if (StringUtils.isNotBlank(relationid)) {
                /*QueryWrapper queryWrapper1 = new QueryWrapper();
                queryWrapper1.eq("relationid",relationid);
                zgsAttachappendixService.remove(queryWrapper1);*/

                if (zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                    for (int a1 = 0; a1 < zgsMattermaterial.getZgsAttachappendixList().size(); a1++) {
                        ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a1);
                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                        zgsAttachappendix.setRelationid(list.get(0).getId());
                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                        zgsAttachappendix.setCreatetime(new Date());
                        zgsAttachappendix.setAppendixtype(zgsMattermaterial.getProjecttypenum());
                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                        zgsAttachappendix = ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt);
                        zgsAttachappendixService.save(zgsAttachappendix);
                        return Result.OK("添加成功！");
                    }
                }

            }
        }

        return Result.OK();
    }


    @AutoLog(value = "删除专家意见")
    @ApiOperation(value = "删除专家意见", notes = "删除专家意见")
    @PostMapping(value = "/delExpertMatterial")
    public Result<?> delExpertMatterial(@RequestBody Map<String, Object> params) {
        String id = params.get("id").toString();
        if (StringUtils.isNotBlank(id)) {
            zgsAttachappendixService.removeById(id);
        }

        return Result.OK();
    }


    /**
     * 推荐
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "工程示范项目库-推荐")
    @ApiOperation(value = "工程示范项目库-推荐", notes = "工程示范项目库-推荐")
    @PutMapping(value = "/tjProject")
    public Result<?> tjProject(@RequestBody ZgsProjectlibraryListInfo zgsProjectlibrary) {
//        ZgsProjectlibrary infoL = zgsProjectlibraryService.getById(zgsProjectlibrary.getId());
//        String projectNum = zgsProjectlibraryService.getProjectNumCurrent();
//        if (GlobalConstants.GreenBuildType.equals(infoL.getProjecttypenum())) {
//            ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
//            zgsGreenbuildproject.setAgreeproject(new BigDecimal(2));
//            zgsGreenbuildproject.setProjectnum(projectNum);
//            QueryWrapper<ZgsGreenbuildproject> queryWrapper1 = new QueryWrapper();
//            queryWrapper1.eq("projectlibraryguid", zgsProjectlibrary.getId());
//            zgsGreenbuildprojectService.update(zgsGreenbuildproject, queryWrapper1);
//        } else if (GlobalConstants.AssemBuidType.equals(infoL.getProjecttypenum())) {
//            ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
//            zgsAssembleproject.setAgreeproject(new BigDecimal(2));
//            zgsAssembleproject.setProjectnum(projectNum);
//            QueryWrapper<ZgsAssembleproject> queryWrapper2 = new QueryWrapper();
//            queryWrapper2.eq("projectlibraryguid", zgsProjectlibrary.getId());
//            zgsAssembleprojectService.update(zgsAssembleproject, queryWrapper2);
//        }
        ZgsProjectlibrary projectlibrary = new ZgsProjectlibrary();
        projectlibrary.setId(zgsProjectlibrary.getId());
        projectlibrary.setSfxmstatus("3");
//        projectlibrary.setProjectnum(projectNum);
        zgsProjectlibraryService.updateById(projectlibrary);
        return Result.OK("推荐成功！");
    }

    /**
     * 撤回
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "工程示范项目库-撤回")
    @ApiOperation(value = "工程示范项目库-撤回", notes = "工程示范项目库-撤回")
    @PutMapping(value = "/rollBackSp")
    public Result<?> rollBackSp(@RequestBody ZgsProjectlibraryListInfo zgsProjectlibrary) {
        if (zgsProjectlibrary != null) {
            String id = zgsProjectlibrary.getId();
            String bid = zgsProjectlibrary.getBid();
            String projecttypenum = zgsProjectlibrary.getProjecttypenum();
            String status = zgsProjectlibrary.getStatus();
            if (StringUtils.isNotEmpty(status) && StringUtils.isNotEmpty(id) && StringUtils.isNotEmpty(bid) && StringUtils.isNotEmpty(projecttypenum)) {
                if (QueryWrapperUtil.ifRollBackSp(status) && QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1).equals(zgsProjectlibrary.getProjectstage())) {
                    //更新项目状态
                    //更新退回原因为空
                    if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        if (zgsProjectlibrary.getAgreeproject() == null) {
                            ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                            zgsGreenbuildproject.setId(bid);
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsGreenbuildproject.setReturntype(null);
                            zgsGreenbuildproject.setAuditopinion(null);
                            zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                        } else {
                            ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                            zgsGreenbuildproject.setId(bid);
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            zgsGreenbuildproject.setReturntype(null);
                            zgsGreenbuildproject.setAuditopinion(null);
                            zgsGreenbuildproject.setAgreeproject(null);
                            zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                        }
                    } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        if (zgsProjectlibrary.getAgreeproject() == null) {
                            ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                            zgsBuildproject.setId(bid);
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsBuildproject.setReturntype(null);
                            zgsBuildproject.setAuditopinion(null);
                            zgsBuildprojectService.updateById(zgsBuildproject);
                        } else {
                            ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                            zgsBuildproject.setId(bid);
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            zgsBuildproject.setReturntype(null);
                            zgsBuildproject.setAuditopinion(null);
                            zgsBuildproject.setAgreeproject(null);
                            zgsBuildprojectService.updateById(zgsBuildproject);
                        }
                    } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        if (zgsProjectlibrary.getAgreeproject() == null) {
                            ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                            zgsAssembleproject.setId(bid);
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsAssembleproject.setReturntype(null);
                            zgsAssembleproject.setAuditopinion(null);
                            zgsAssembleprojectService.updateById(zgsAssembleproject);
                        } else {
                            ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                            zgsAssembleproject.setId(bid);
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            zgsAssembleproject.setReturntype(null);
                            zgsAssembleproject.setAuditopinion(null);
                            zgsAssembleproject.setAgreeproject(null);
                            zgsAssembleprojectService.updateById(zgsAssembleproject);
                        }
                    } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        if (zgsProjectlibrary.getAgreeproject() == null) {
                            ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                            zgsEnergybuildproject.setId(bid);
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsEnergybuildproject.setReturntype(null);
                            zgsEnergybuildproject.setAuditopinion(null);
                            zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                        } else {
                            ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                            zgsEnergybuildproject.setId(bid);
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            zgsEnergybuildproject.setReturntype(null);
                            zgsEnergybuildproject.setAuditopinion(null);
                            zgsEnergybuildproject.setAgreeproject(null);
                            zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                        }
                    } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        if (zgsProjectlibrary.getAgreeproject() == null) {
                            ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                            zgsSamplebuildproject.setId(bid);
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsSamplebuildproject.setReturntype(null);
                            zgsSamplebuildproject.setAuditopinion(null);
                            zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                        } else {
                            ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                            zgsSamplebuildproject.setId(bid);
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            zgsSamplebuildproject.setReturntype(null);
                            zgsSamplebuildproject.setAuditopinion(null);
                            zgsSamplebuildproject.setAgreeproject(null);
                            zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                        }
                    }
                    if (zgsProjectlibrary.getAgreeproject() == null) {
                        //更新各阶段状态
                        ZgsProjectlibrary projectlibrary = new ZgsProjectlibrary();
                        projectlibrary.setId(id);
                        projectlibrary.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                        projectlibrary.setProjectstagestatus(GlobalConstants.SHENHE_STATUS4);
                        zgsProjectlibraryService.updateById(projectlibrary);
//                    zgsProjectlibraryService.initStageAndStatus(id);
                        //删除审批记录
                        zgsReturnrecordService.deleteReturnRecordLastLog(bid);
                    } else {
//                        ZgsProjectlibrary projectlibrary = new ZgsProjectlibrary();
//                        projectlibrary.setId(id);
//                        projectlibrary.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
//                        projectlibrary.setProjectstagestatus(GlobalConstants.SHENHE_STATUS12);
//                        projectlibrary.setProvincialfundtotal(null);
//                        zgsProjectlibraryService.updateById(projectlibrary);
                        //修改为sql操作
                        zgsProjectlibraryService.rollBackSp(id);
                        //删除审批记录
                        zgsReturnrecordService.deleteReturnRecordLastLogByLx(bid);
                    }
                } else {
                    return Result.error("该项目当前状态不支持撤回操作！");
                }
            } else {
                return Result.error("该项目当前状态不支持撤回操作！");
            }
        }
        return Result.OK("撤回成功！");
    }

    /**
     * 审批
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "工程示范项目库-审批")
    @ApiOperation(value = "工程示范项目库-审批", notes = "工程示范项目库-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestBody ZgsProjectlibraryListInfo zgsProjectlibrary) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getBid())) {
            String bid = zgsProjectlibrary.getBid();
            if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                String projectlibraryguid = null;
                String enterpriseguid = null;
                ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
                zgsReturnrecord.setId(UUID.randomUUID().toString());
                zgsReturnrecord.setBusinessguid(bid);
                zgsReturnrecord.setReturnreason(zgsProjectlibrary.getAuditopinion());
                zgsReturnrecord.setReturndate(new Date());
                zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
                zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
                zgsReturnrecord.setCreatetime(new Date());
                zgsReturnrecord.setReturnperson(sysUser.getRealname());
                if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                    zgsGreenbuildproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsGreenbuildproject.setAuditopinion(null);
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsGreenbuildproject.setFirstdate(new Date());
                            zgsGreenbuildproject.setFirstperson(sysUser.getRealname());
                            zgsGreenbuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                            zgsGreenbuildproject.setFinishperson(sysUser.getRealname());
                            zgsGreenbuildproject.setFinishdate(new Date());
                            zgsGreenbuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形式审查通过后修改项目库状态位已入库
                            ZgsGreenbuildproject xmkG = zgsGreenbuildprojectService.getById(bid);
                            if (xmkG != null) {
                                ZgsProjectlibrary libG = zgsProjectlibraryService.getById(xmkG.getProjectlibraryguid());
                                if (libG != null && StringUtils.isNotEmpty(libG.getSfxmtype()) && "1".equals(libG.getSfxmtype())) {
                                    ZgsProjectlibrary libGU = new ZgsProjectlibrary();
                                    libGU.setId(xmkG.getProjectlibraryguid());
                                    libGU.setSfxmstatus("1");
                                    zgsProjectlibraryService.updateById(libGU);
                                }
                            }
                            //立项审批-补正修改
                            if (xmkG != null && StringUtils.isNotEmpty(xmkG.getLxbzStatus())) {
                                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            }
                        }
                    } else {
                        //驳回-退回（补正修改） 不通过记录状态==1
                        zgsGreenbuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                            } else {
                                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS13);
                            }
                            zgsGreenbuildproject.setFirstdate(new Date());
                            zgsGreenbuildproject.setFirstperson(sysUser.getRealname());
                            zgsGreenbuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                            } else {
                                zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS14);
                            }
                            zgsGreenbuildproject.setFinishperson(sysUser.getRealname());
                            zgsGreenbuildproject.setFinishdate(new Date());
                            zgsGreenbuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                        }
                        if (zgsProjectlibrary.getSpStatus() == 2) {
                            //补正修改
                            if (StringUtils.isEmpty(zgsGreenbuildproject.getAuditopinion())) {
                                zgsGreenbuildproject.setAuditopinion(" ");
                            }
                            zgsGreenbuildproject.setAuditopinion(zgsGreenbuildproject.getAuditopinion());
                            zgsGreenbuildproject.setReturntype(new BigDecimal(2));
                            zgsReturnrecord.setReturntype(new BigDecimal(2));
                        } else {
                            //驳回
                            zgsGreenbuildproject.setReturntype(new BigDecimal(1));
                            zgsReturnrecord.setReturntype(new BigDecimal(1));
                            if (GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                                zgsGreenbuildproject.setAgreeproject(new BigDecimal(1));
                            }
                        }
                    }
                    zgsGreenbuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenbuildproject, ValidateEncryptEntityUtil.isEncrypt);
                    zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                    //添加审批记录
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE0);
                    ZgsGreenbuildproject greenbuildproject = zgsGreenbuildprojectService.getById(bid);
                    if (greenbuildproject != null) {
                        enterpriseguid = greenbuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(greenbuildproject.getApplydate());
                        projectlibraryguid = greenbuildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                    zgsBuildproject.setId(bid);
                    //以下修改替换
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsBuildproject.setAuditopinion(null);
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsBuildproject.setFirstdate(new Date());
                            zgsBuildproject.setFirstperson(sysUser.getRealname());
                            zgsBuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                            zgsBuildproject.setFinishperson(sysUser.getRealname());
                            zgsBuildproject.setFinishdate(new Date());
                            zgsBuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            ZgsBuildproject buildProject = zgsBuildprojectService.getById(bid);
                            //立项审批-补正修改
                            if (buildProject != null && StringUtils.isNotEmpty(buildProject.getLxbzStatus())) {
                                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            }
                        }
                    } else {
                        //驳回-退回（补正修改） 不通过记录状态==1
                        zgsBuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                            } else {
                                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS13);
                            }
                            zgsBuildproject.setFirstdate(new Date());
                            zgsBuildproject.setFirstperson(sysUser.getRealname());
                            zgsBuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                            } else {
                                zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS14);
                            }
                            zgsBuildproject.setFinishperson(sysUser.getRealname());
                            zgsBuildproject.setFinishdate(new Date());
                            zgsBuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                        }
                        if (zgsProjectlibrary.getSpStatus() == 2) {
                            //补正修改
                            if (StringUtils.isEmpty(zgsBuildproject.getAuditopinion())) {
                                zgsBuildproject.setAuditopinion(" ");
                            }
                            zgsBuildproject.setAuditopinion(zgsBuildproject.getAuditopinion());
                            zgsBuildproject.setReturntype(new BigDecimal(2));
                            zgsReturnrecord.setReturntype(new BigDecimal(2));
                        } else {
                            //驳回
                            zgsBuildproject.setReturntype(new BigDecimal(1));
                            zgsReturnrecord.setReturntype(new BigDecimal(1));
                            if (GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                                zgsBuildproject.setAgreeproject(new BigDecimal(1));
                            }
                        }
                    }
                    //以上修改替换
                    zgsBuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildproject, ValidateEncryptEntityUtil.isEncrypt);
                    zgsBuildprojectService.updateById(zgsBuildproject);
                    //添加审批记录
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE1);
                    ZgsBuildproject buildproject = zgsBuildprojectService.getById(bid);
                    if (buildproject != null) {
                        enterpriseguid = buildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(buildproject.getApplydate());
                        projectlibraryguid = buildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                    zgsAssembleproject.setId(bid);
                    //以下修改替换
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsAssembleproject.setAuditopinion(null);
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsAssembleproject.setFirstdate(new Date());
                            zgsAssembleproject.setFirstperson(sysUser.getRealname());
                            zgsAssembleproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                            zgsAssembleproject.setFinishperson(sysUser.getRealname());
                            zgsAssembleproject.setFinishdate(new Date());
                            zgsAssembleproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形式审查通过后修改项目库状态位已入库
                            ZgsAssembleproject xmkG = zgsAssembleprojectService.getById(bid);
                            if (xmkG != null) {
                                ZgsProjectlibrary libG = zgsProjectlibraryService.getById(xmkG.getProjectlibraryguid());
                                if (libG != null && StringUtils.isNotEmpty(libG.getSfxmtype()) && "1".equals(libG.getSfxmtype())) {
                                    ZgsProjectlibrary libGU = new ZgsProjectlibrary();
                                    libGU.setId(xmkG.getProjectlibraryguid());
                                    libGU.setSfxmstatus("1");
                                    zgsProjectlibraryService.updateById(libGU);
                                }
                            }
                            ZgsAssembleproject assembleProject = zgsAssembleprojectService.getById(bid);
                            //立项审批-补正修改
                            if (assembleProject != null && StringUtils.isNotEmpty(assembleProject.getLxbzStatus())) {
                                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            }
                        }
                    } else {
                        //驳回-退回（补正修改） 不通过记录状态==1
                        zgsAssembleproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                            } else {
                                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS13);
                            }
                            zgsAssembleproject.setFirstdate(new Date());
                            zgsAssembleproject.setFirstperson(sysUser.getRealname());
                            zgsAssembleproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                            } else {
                                zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS14);
                            }
                            zgsAssembleproject.setFinishperson(sysUser.getRealname());
                            zgsAssembleproject.setFinishdate(new Date());
                            zgsAssembleproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                        }
                        if (zgsProjectlibrary.getSpStatus() == 2) {
                            //补正修改
                            if (StringUtils.isEmpty(zgsAssembleproject.getAuditopinion())) {
                                zgsAssembleproject.setAuditopinion(" ");
                            }
                            zgsAssembleproject.setAuditopinion(zgsAssembleproject.getAuditopinion());
                            zgsAssembleproject.setReturntype(new BigDecimal(2));
                            zgsReturnrecord.setReturntype(new BigDecimal(2));
                        } else {
                            //驳回
                            zgsAssembleproject.setReturntype(new BigDecimal(1));
                            zgsReturnrecord.setReturntype(new BigDecimal(1));
                            if (GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                                zgsAssembleproject.setAgreeproject(new BigDecimal(1));
                            }
                        }
                    }
                    //以上修改替换
                    zgsAssembleproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsAssembleproject, ValidateEncryptEntityUtil.isEncrypt);
                    zgsAssembleprojectService.updateById(zgsAssembleproject);
                    //添加审批记录
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE4);
                    ZgsAssembleproject assembleproject = zgsAssembleprojectService.getById(bid);
                    if (assembleproject != null) {
                        enterpriseguid = assembleproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(assembleproject.getApplydate());
                        projectlibraryguid = assembleproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                    zgsEnergybuildproject.setId(bid);
                    //以下修改替换
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsEnergybuildproject.setAuditopinion(null);
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsEnergybuildproject.setFirstdate(new Date());
                            zgsEnergybuildproject.setFirstperson(sysUser.getRealname());
                            zgsEnergybuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                            zgsEnergybuildproject.setFinishperson(sysUser.getRealname());
                            zgsEnergybuildproject.setFinishdate(new Date());
                            zgsEnergybuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            ZgsEnergybuildproject energyBuildProject = zgsEnergybuildprojectService.getById(bid);
                            //立项审批-补正修改
                            if (energyBuildProject != null && StringUtils.isNotEmpty(energyBuildProject.getLxbzStatus())) {
                                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            }
                        }
                    } else {
                        //驳回-退回（补正修改） 不通过记录状态==1
                        zgsEnergybuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                            } else {
                                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS13);
                            }
                            zgsEnergybuildproject.setFirstdate(new Date());
                            zgsEnergybuildproject.setFirstperson(sysUser.getRealname());
                            zgsEnergybuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                            } else {
                                zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS14);
                            }
                            zgsEnergybuildproject.setFinishperson(sysUser.getRealname());
                            zgsEnergybuildproject.setFinishdate(new Date());
                            zgsEnergybuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                        }
                        if (zgsProjectlibrary.getSpStatus() == 2) {
                            //补正修改
                            if (StringUtils.isEmpty(zgsEnergybuildproject.getAuditopinion())) {
                                zgsEnergybuildproject.setAuditopinion(" ");
                            }
                            zgsEnergybuildproject.setAuditopinion(zgsEnergybuildproject.getAuditopinion());
                            zgsEnergybuildproject.setReturntype(new BigDecimal(2));
                            zgsReturnrecord.setReturntype(new BigDecimal(2));
                        } else {
                            //驳回
                            zgsEnergybuildproject.setReturntype(new BigDecimal(1));
                            zgsReturnrecord.setReturntype(new BigDecimal(1));
                            if (GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                                zgsEnergybuildproject.setAgreeproject(new BigDecimal(1));
                            }
                        }
                    }
                    //以上修改替换
                    zgsEnergybuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsEnergybuildproject, ValidateEncryptEntityUtil.isEncrypt);
                    zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                    //添加审批记录
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE3);
                    ZgsEnergybuildproject energybuildproject = zgsEnergybuildprojectService.getById(bid);
                    if (energybuildproject != null) {
                        enterpriseguid = energybuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(energybuildproject.getApplydate());
                        projectlibraryguid = energybuildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                    zgsSamplebuildproject.setId(bid);
                    //以下修改替换
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsSamplebuildproject.setAuditopinion(null);
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS4);
                            zgsSamplebuildproject.setFirstdate(new Date());
                            zgsSamplebuildproject.setFirstperson(sysUser.getRealname());
                            zgsSamplebuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                            zgsSamplebuildproject.setFinishperson(sysUser.getRealname());
                            zgsSamplebuildproject.setFinishdate(new Date());
                            zgsSamplebuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            ZgsSamplebuildproject sampleBuildProject = zgsSamplebuildprojectService.getById(bid);
                            //立项审批-补正修改
                            if (sampleBuildProject != null && StringUtils.isNotEmpty(sampleBuildProject.getLxbzStatus())) {
                                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS12);
                            }
                        }
                    } else {
                        //驳回-退回（补正修改） 不通过记录状态==1
                        zgsSamplebuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjectlibrary.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                            } else {
                                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS13);
                            }
                            zgsSamplebuildproject.setFirstdate(new Date());
                            zgsSamplebuildproject.setFirstperson(sysUser.getRealname());
                            zgsSamplebuildproject.setFirstdepartment(sysUser.getEnterprisename());
                            //初审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                            zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                        } else if (GlobalConstants.SHENHE_STATUS4.equals(zgsProjectlibrary.getStatus())) {
                            if (zgsProjectlibrary.getSpStatus() == 2) {
                                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                            } else {
                                zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS14);
                            }
                            zgsSamplebuildproject.setFinishperson(sysUser.getRealname());
                            zgsSamplebuildproject.setFinishdate(new Date());
                            zgsSamplebuildproject.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                            //形审
                            zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                            zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                        }
                        if (zgsProjectlibrary.getSpStatus() == 2) {
                            //补正修改
                            if (StringUtils.isEmpty(zgsSamplebuildproject.getAuditopinion())) {
                                zgsSamplebuildproject.setAuditopinion(" ");
                            }
                            zgsSamplebuildproject.setAuditopinion(zgsSamplebuildproject.getAuditopinion());
                            zgsSamplebuildproject.setReturntype(new BigDecimal(2));
                            zgsReturnrecord.setReturntype(new BigDecimal(2));
                        } else {
                            //驳回
                            zgsSamplebuildproject.setReturntype(new BigDecimal(1));
                            zgsReturnrecord.setReturntype(new BigDecimal(1));
                            if (GlobalConstants.SHENHE_STATUS14.equals(zgsProjectlibrary.getStatus())) {
                                zgsSamplebuildproject.setAgreeproject(new BigDecimal(1));
                            }
                        }
                    }
                    //以上修改替换
                    zgsSamplebuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsSamplebuildproject, ValidateEncryptEntityUtil.isEncrypt);
                    zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                    //添加审批记录
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE3);
                    ZgsSamplebuildproject samplebuildproject = zgsSamplebuildprojectService.getById(bid);
                    if (samplebuildproject != null) {
                        enterpriseguid = samplebuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(samplebuildproject.getApplydate());
                        projectlibraryguid = samplebuildproject.getProjectlibraryguid();
                    }
                }
                ZgsProjectlibrary projectlibrary = zgsProjectlibraryService.getById(projectlibraryguid);
                if (projectlibrary != null) {
                    zgsReturnrecord.setProjectname(projectlibrary.getProjectname());
                    ZgsProjectlibrary lib = new ZgsProjectlibrary();
                    lib.setId(projectlibrary.getId());
                    if (zgsProjectlibrary.getSpStatus() == 1 && GlobalConstants.SHENHE_STATUS1.equals(zgsProjectlibrary.getStatus())) {
                        //更新当前项目阶段
                        lib.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                        zgsProjectlibraryService.updateById(lib);
                    }
                }
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", enterpriseguid);
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
                zgsReturnrecord.setProjectlibraryguid(projectlibraryguid);
                zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                zgsReturnrecord = ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt);
                zgsReturnrecordService.save(zgsReturnrecord);
                //实时更新阶段和状态
                zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
            }
        }
        return Result.OK("审批成功！");
    }

    /**
     * 审批
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "工程示范项目库-立项")
    @ApiOperation(value = "工程示范项目库-立项", notes = "工程示范项目库-立项")
    @PutMapping(value = "/lxProject")
    public Result<?> lxProject(@RequestBody ZgsProjectlibraryListInfo zgsProjectlibrary) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getBid())) {
            String bid = zgsProjectlibrary.getBid();
//            String projectNum = zgsProjectlibraryService.getProjectNumCurrent();
            if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                String projectlibraryguid = null;
                String enterpriseguid = null;
                ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
                zgsReturnrecord.setId(UUID.randomUUID().toString());
                zgsReturnrecord.setBusinessguid(bid);
                zgsReturnrecord.setReturnreason(zgsProjectlibrary.getAuditopinion());
                zgsReturnrecord.setReturndate(new Date());
                zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
                zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
                zgsReturnrecord.setCreatetime(new Date());
                zgsReturnrecord.setReturnperson(sysUser.getRealname());
                if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsGreenbuildproject zgsGreenbuildproject = new ZgsGreenbuildproject();
                    zgsGreenbuildproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsGreenbuildproject.setAuditopinion(null);
                        //生成项目编号
//                        zgsGreenbuildproject.setProjectnum(projectNum);
                        zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsGreenbuildproject.setAgreeproject(new BigDecimal(2));
                    } else if (zgsProjectlibrary.getSpStatus() == 2) {
                        //立项环节补正修改
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        zgsGreenbuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsGreenbuildproject.setReturntype(new BigDecimal(2));
                        zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsGreenbuildproject.setLxbzStatus("1");
                    } else {
                        //驳回 不通过记录状态==1
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        zgsGreenbuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsGreenbuildproject.setAgreeproject(new BigDecimal(1));
                    }
                    zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                    //添加审批记录
                    //立项
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE0);
                    ZgsGreenbuildproject greenbuildproject = zgsGreenbuildprojectService.getById(bid);
                    if (greenbuildproject != null) {
                        enterpriseguid = greenbuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(greenbuildproject.getFirstdate());
                        projectlibraryguid = greenbuildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsBuildproject zgsBuildproject = new ZgsBuildproject();
                    zgsBuildproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsBuildproject.setAuditopinion(null);
                        //生成项目编号
//                        zgsBuildproject.setProjectnum(projectNum);
                        zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsBuildproject.setAgreeproject(new BigDecimal(2));
                    } else if (zgsProjectlibrary.getSpStatus() == 2) {
                        //立项环节补正修改
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        zgsBuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsBuildproject.setReturntype(new BigDecimal(2));
                        zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsBuildproject.setLxbzStatus("1");
                    } else {
                        //驳回 不通过记录状态==1
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        zgsBuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsBuildproject.setAgreeproject(new BigDecimal(1));
                    }
                    zgsBuildprojectService.updateById(zgsBuildproject);
                    //添加审批记录
                    //立项
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE1);
                    ZgsBuildproject buildproject = zgsBuildprojectService.getById(bid);
                    if (buildproject != null) {
                        enterpriseguid = buildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(buildproject.getFirstdate());
                        projectlibraryguid = buildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsAssembleproject zgsAssembleproject = new ZgsAssembleproject();
                    zgsAssembleproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsAssembleproject.setAuditopinion(null);
                        //生成项目编号
//                        zgsAssembleproject.setProjectnum(projectNum);
                        zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsAssembleproject.setAgreeproject(new BigDecimal(2));
                    } else if (zgsProjectlibrary.getSpStatus() == 2) {
                        //立项环节补正修改
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        zgsAssembleproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsAssembleproject.setReturntype(new BigDecimal(2));
                        zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsAssembleproject.setLxbzStatus("1");
                    } else {
                        //驳回 不通过记录状态==1
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        zgsAssembleproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsAssembleproject.setAgreeproject(new BigDecimal(1));
                    }
                    zgsAssembleprojectService.updateById(zgsAssembleproject);
                    //添加审批记录
                    //立项
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE4);
                    ZgsAssembleproject assembleproject = zgsAssembleprojectService.getById(bid);
                    if (assembleproject != null) {
                        enterpriseguid = assembleproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(assembleproject.getApplydate());
                        projectlibraryguid = assembleproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsEnergybuildproject zgsEnergybuildproject = new ZgsEnergybuildproject();
                    zgsEnergybuildproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsEnergybuildproject.setAuditopinion(null);
                        //生成项目编号
//                        zgsEnergybuildproject.setProjectnum(projectNum);
                        zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsEnergybuildproject.setAgreeproject(new BigDecimal(2));
                    } else if (zgsProjectlibrary.getSpStatus() == 2) {
                        //立项环节补正修改
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        zgsEnergybuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsEnergybuildproject.setReturntype(new BigDecimal(2));
                        zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsEnergybuildproject.setLxbzStatus("1");
                    } else {
                        //驳回 不通过记录状态==1
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        zgsEnergybuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsEnergybuildproject.setAgreeproject(new BigDecimal(1));
                    }
                    zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                    //添加审批记录
                    //立项
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE3);
                    ZgsEnergybuildproject energybuildproject = zgsEnergybuildprojectService.getById(bid);
                    if (energybuildproject != null) {
                        enterpriseguid = energybuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(energybuildproject.getApplydate());
                        projectlibraryguid = energybuildproject.getProjectlibraryguid();
                    }
                } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                    ZgsSamplebuildproject zgsSamplebuildproject = new ZgsSamplebuildproject();
                    zgsSamplebuildproject.setId(bid);
                    if (zgsProjectlibrary.getSpStatus() == 1) {
                        //通过记录状态==0
                        zgsReturnrecord.setReturntype(new BigDecimal(0));
                        zgsSamplebuildproject.setAuditopinion(null);
                        //生成项目编号
//                        zgsSamplebuildproject.setProjectnum(projectNum);
                        zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsSamplebuildproject.setAgreeproject(new BigDecimal(2));
                    } else if (zgsProjectlibrary.getSpStatus() == 2) {
                        //立项环节补正修改
                        zgsReturnrecord.setReturntype(new BigDecimal(2));
                        zgsSamplebuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsSamplebuildproject.setReturntype(new BigDecimal(2));
                        zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS9);
                        zgsSamplebuildproject.setLxbzStatus("1");
                    } else {
                        //驳回 不通过记录状态==1
                        zgsReturnrecord.setReturntype(new BigDecimal(1));
                        zgsSamplebuildproject.setAuditopinion(zgsProjectlibrary.getAuditopinion());
                        zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                        zgsSamplebuildproject.setAgreeproject(new BigDecimal(1));
                    }
                    zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                    //添加审批记录
                    //立项
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE26);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE3);
                    ZgsSamplebuildproject samplebuildproject = zgsSamplebuildprojectService.getById(bid);
                    if (samplebuildproject != null) {
                        enterpriseguid = samplebuildproject.getEnterpriseguid();
                        zgsReturnrecord.setEnterpriseguid(enterpriseguid);
                        zgsReturnrecord.setApplydate(samplebuildproject.getApplydate());
                        projectlibraryguid = samplebuildproject.getProjectlibraryguid();
                    }
                }
                ZgsProjectlibrary projectlibrary = zgsProjectlibraryService.getById(projectlibraryguid);
                if (projectlibrary != null) {
                    zgsReturnrecord.setProjectname(projectlibrary.getProjectname());
                }
                //立项自动生成编号功能取消，改为手动添加
//                if (zgsProjectlibrary.getSpStatus() == 1) {
//                    ZgsProjectlibrary lib = new ZgsProjectlibrary();
//                    lib.setId(projectlibrary.getId());
//                    lib.setProjectnum(projectNum);
//                    zgsProjectlibraryService.updateById(lib);
//                }
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", enterpriseguid);
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
                zgsReturnrecord.setProjectlibraryguid(projectlibraryguid);
                zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                zgsReturnrecord = ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt);
                zgsReturnrecordService.save(zgsReturnrecord);
                //实时更新阶段和状态
                zgsProjectlibraryService.initStageAndStatus(projectlibraryguid);
            }
        }
        return Result.OK("立项成功！");
    }

    /**
     * 新增省科技经费总额
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "新增省科技经费总额")
    @ApiOperation(value = "新增省科技经费总额", notes = "新增省科技经费总额")
    @PutMapping(value = "/addMoney")
    public Result<?> addMoney(@RequestBody ZgsProjectlibraryListInfo zgsProjectlibrary) {
        if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getId())) {
            zgsProjectlibraryService.updateAddMoney(zgsProjectlibrary);
        }
        return Result.OK("省科技经费添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsProjectlibrary
     * @return
     */
    @AutoLog(value = "建筑工程项目库-编辑")
    @ApiOperation(value = "建筑工程项目库-编辑", notes = "建筑工程项目库-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjectlibrary zgsProjectlibrary) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjectlibrary != null && StringUtils.isNotEmpty(zgsProjectlibrary.getId())) {

            // 判断终审单位是否已取消授权编辑，如果终审单位取消授权编辑，则此次编辑就不能保存   rxl  20231116
            ZgsProjectlibrary projectlibrary = zgsProjectlibraryService.getById(zgsProjectlibrary.getId());
            if (ObjectUtil.isNotNull(projectlibrary)) {
                if (StrUtil.isNotBlank(projectlibrary.getSqbj()) && projectlibrary.getSqbj().equals("2")) {  // 终审单位取消编辑,已修改的信息则不能保存
                    Result result = new Result();

                    result.setMessage("授权编辑已被取消，已修改信息不能保存");
                    result.setCode(0);
                    result.setSuccess(false);
                    return result;

                } else {
                    String enterpriseguid = zgsProjectlibrary.getEnterpriseguid();
                    String stagestatus = null;
                    zgsProjectlibrary.setModifyaccountname(sysUser.getUsername());
                    zgsProjectlibrary.setModifyusername(sysUser.getRealname());
                    zgsProjectlibrary.setModifydate(new Date());
                    zgsProjectlibrary.setCreatedate(null);

                    String unit = zgsProjectlibrary.getApplyunit();
                    String leader = zgsProjectlibrary.getApplyunitprojectleader();
                    Date startdate = zgsProjectlibrary.getStartdate();
                    Date dateStart = null;
                    int mesNum = 0;
                    if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                        if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            unit = zgsProjectlibrary.getOwnerunit();
                            leader = zgsProjectlibrary.getOwnercharger();
                            dateStart = zgsProjectlibrary.getProjectstartdate();
                        }
                    }

                    if (ObjectUtil.isNotEmpty(startdate)) {
                        mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader(zgsProjectlibrary.getId(), unit, leader, startdate, 2);
                    } else {
                        mesNum = zgsProjectlibraryService.verificationUnitAndProjectLeader(zgsProjectlibrary.getId(), unit, leader, dateStart, 2);
                    }

                    if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                        if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            unit = zgsProjectlibrary.getOwnerunit();
                            leader = zgsProjectlibrary.getOwnercharger();
                        }
                    }

                    if (mesNum != -1) {
                        return Result.error(GreenUtilSelf.getMesByNum(mesNum));
                    }
                    String bid = null;
                    if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                        if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            //绿色建筑
                            ZgsGreenbuildproject zgsGreenbuildproject = zgsProjectlibrary.getZgsGreenbuildproject();
                            bid = zgsGreenbuildproject.getId();
                            //
                            ZgsGreenbuildproject zgsGreenbuildproject1 = zgsGreenbuildprojectService.getById(bid);
                            if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsGreenbuildproject().getStatus())) {
                                zgsGreenbuildproject.setAuditopinion(null);
                                //保存并上报
                                if (!VerificationUtil.projectLeaderVerification(leader)) {
                                    return Result.error("项目负责人名称填写不规范！");
                                }
                                zgsGreenbuildproject.setApplydate(new Date());
//                        zgsGreenbuildproject.setReturntype(null);
                                //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsGreenbuildproject1.getStatus())) {
                                    zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                                    //查分配所有修改专家状态
                                    QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
                                    queryWrapper.eq("businessguid", bid);
                                    List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(queryWrapper);
                                    if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
                                        for (ZgsProjectlibraryexpert zgsProjectlibraryexpert : zgsProjectlibraryexpertList) {
                                            ZgsProjectlibraryexpert projectlibraryexpert = new ZgsProjectlibraryexpert();
                                            projectlibraryexpert.setId(zgsProjectlibraryexpert.getId());
                                            projectlibraryexpert.setAuditconclusionnum(null);
                                            zgsProjectlibraryexpertService.updateById(projectlibraryexpert);
                                        }
                                    }
                                }
                            } else {
                                zgsGreenbuildproject.setApplydate(null);
                                //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsGreenbuildproject1.getStatus())) {
                                    zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                                }
                                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                                if (StringUtils.isNotEmpty(zgsGreenbuildproject1.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(zgsGreenbuildproject1.getStatus())) {
                                    zgsGreenbuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                                }
                            }
                            //
                            zgsGreenbuildproject.setModifyaccountname(sysUser.getUsername());
                            zgsGreenbuildproject.setModifyusername(sysUser.getRealname());
                            zgsGreenbuildproject.setModifydate(new Date());
                            zgsGreenbuildproject.setCreatedate(null);
                            zgsGreenbuildproject.setFinishdate(null);
                            zgsGreenbuildproject.setFirstdate(null);
                            stagestatus = zgsGreenbuildproject.getStatus();
                            zgsGreenbuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenbuildproject, ValidateEncryptEntityUtil.isEncrypt);
                            zgsGreenbuildprojectService.updateById(zgsGreenbuildproject);
                            //项目主要参加人员
                            List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList = zgsProjectlibrary.getZgsGreenprojectmainparticipantList();
                            QueryWrapper<ZgsGreenprojectmainparticipant> queryWrapper3_1 = new QueryWrapper<>();
                            queryWrapper3_1.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_1.eq("buildguid", bid);
                            queryWrapper3_1.ne("isdelete", 1);
                            queryWrapper3_1.orderByAsc("ordernum");
                            List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList_0 = zgsGreenprojectmainparticipantService.list(queryWrapper3_1);
                            if (zgsGreenprojectmainparticipantList != null && zgsGreenprojectmainparticipantList.size() > 0) {
                                if (zgsGreenprojectmainparticipantList.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < zgsGreenprojectmainparticipantList.size(); a0++) {
                                    ZgsGreenprojectmainparticipant zgsGreenprojectmainparticipant = zgsGreenprojectmainparticipantList.get(a0);
                                    if (StringUtils.isNotEmpty(zgsGreenprojectmainparticipant.getId())) {
                                        //编辑
                                        zgsGreenprojectmainparticipant.setModifyaccountname(sysUser.getUsername());
                                        zgsGreenprojectmainparticipant.setModifyusername(sysUser.getRealname());
                                        zgsGreenprojectmainparticipant.setModifydate(new Date());
                                        zgsGreenprojectmainparticipant = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectmainparticipant, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsGreenprojectmainparticipantService.updateById(zgsGreenprojectmainparticipant);
                                    } else {
                                        //新增
                                        zgsGreenprojectmainparticipant.setId(UUID.randomUUID().toString());
                                        zgsGreenprojectmainparticipant.setBuildguid(bid);
                                        zgsGreenprojectmainparticipant.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        zgsGreenprojectmainparticipant.setCreateaccountname(sysUser.getUsername());
                                        zgsGreenprojectmainparticipant.setCreateusername(sysUser.getRealname());
                                        zgsGreenprojectmainparticipant.setCreatedate(new Date());
                                        zgsGreenprojectmainparticipant = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectmainparticipant, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsGreenprojectmainparticipantService.save(zgsGreenprojectmainparticipant);
                                    }
                                    for (ZgsGreenprojectmainparticipant zgsGreenprojectmainparticipant1 : zgsGreenprojectmainparticipantList_0) {
                                        if (!zgsGreenprojectmainparticipant1.getId().equals(zgsGreenprojectmainparticipant.getId())) {
                                            map0.put(zgsGreenprojectmainparticipant1.getId(), zgsGreenprojectmainparticipant1.getId());
                                        } else {
                                            list0.add(zgsGreenprojectmainparticipant1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsGreenprojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsGreenprojectmainparticipantList_0 != null && zgsGreenprojectmainparticipantList_0.size() > 0) {
                                    for (ZgsGreenprojectmainparticipant zgsGreenprojectmainparticipant : zgsGreenprojectmainparticipantList_0) {
                                        zgsGreenprojectmainparticipantService.removeById(zgsGreenprojectmainparticipant.getId());
                                    }
                                }
                            }
                            //计划进度与安排
                            List<ZgsGreenprojectplanarrange> zgsGreenprojectplanarrangeList = zgsProjectlibrary.getZgsGreenprojectplanarrangeList();
                            QueryWrapper<ZgsGreenprojectplanarrange> queryWrapper1_1 = new QueryWrapper<>();
                            queryWrapper1_1.eq("enterpriseguid", enterpriseguid);
                            queryWrapper1_1.eq("buildguid", bid);
                            queryWrapper1_1.ne("isdelete", 1);
                            List<ZgsGreenprojectplanarrange> zgsGreenprojectplanarrangeList_0 = zgsGreenprojectplanarrangeService.list(queryWrapper1_1);
                            if (zgsGreenprojectplanarrangeList != null && zgsGreenprojectplanarrangeList.size() > 0) {
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < zgsGreenprojectplanarrangeList.size(); a0++) {
                                    ZgsGreenprojectplanarrange zgsGreenprojectplanarrange = zgsGreenprojectplanarrangeList.get(a0);
                                    if (StringUtils.isNotEmpty(zgsGreenprojectplanarrange.getId())) {
                                        //编辑
                                        zgsGreenprojectplanarrange.setModifyaccountname(sysUser.getUsername());
                                        zgsGreenprojectplanarrange.setModifyusername(sysUser.getRealname());
                                        zgsGreenprojectplanarrange.setModifydate(new Date());
                                        zgsGreenprojectplanarrange = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectplanarrange, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsGreenprojectplanarrangeService.updateById(zgsGreenprojectplanarrange);
                                    } else {
                                        //新增
                                        zgsGreenprojectplanarrange.setId(UUID.randomUUID().toString());
                                        zgsGreenprojectplanarrange.setBuildguid(bid);
                                        zgsGreenprojectplanarrange.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        zgsGreenprojectplanarrange.setCreateaccountname(sysUser.getUsername());
                                        zgsGreenprojectplanarrange.setCreateusername(sysUser.getRealname());
                                        zgsGreenprojectplanarrange.setCreatedate(new Date());
                                        zgsGreenprojectplanarrange = ValidateEncryptEntityUtil.validateEncryptObject(zgsGreenprojectplanarrange, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsGreenprojectplanarrangeService.save(zgsGreenprojectplanarrange);
                                    }
                                    for (ZgsGreenprojectplanarrange zgsGreenprojectplanarrange1 : zgsGreenprojectplanarrangeList_0) {
                                        if (!zgsGreenprojectplanarrange1.getId().equals(zgsGreenprojectplanarrange.getId())) {
                                            map0.put(zgsGreenprojectplanarrange1.getId(), zgsGreenprojectplanarrange1.getId());
                                        } else {
                                            list0.add(zgsGreenprojectplanarrange1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsGreenprojectplanarrangeService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsGreenprojectplanarrangeList_0 != null && zgsGreenprojectplanarrangeList_0.size() > 0) {
                                    for (ZgsGreenprojectplanarrange plb : zgsGreenprojectplanarrangeList_0) {
                                        zgsGreenprojectplanarrangeService.removeById(plb.getId());
                                    }
                                }
                            }
                        } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            ZgsBuildproject zgsBuildproject = zgsProjectlibrary.getZgsBuildproject();
                            bid = zgsBuildproject.getId();
                            //
                            ZgsBuildproject zgsBuildproject1 = zgsBuildprojectService.getById(bid);
                            if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsBuildproject().getStatus())) {
                                zgsBuildproject.setAuditopinion(null);
                                //保存并上报
                                if (!VerificationUtil.projectLeaderVerification(leader)) {
                                    return Result.error("项目负责人名称填写不规范！");
                                }
                                zgsBuildproject.setApplydate(new Date());
//                        zgsBuildproject.setReturntype(null);
                                //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsBuildproject1.getStatus())) {
                                    zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                                    //查分配所有修改专家状态
                                    QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
                                    queryWrapper.eq("businessguid", bid);
                                    List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(queryWrapper);
                                    if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
                                        for (ZgsProjectlibraryexpert zgsProjectlibraryexpert : zgsProjectlibraryexpertList) {
                                            ZgsProjectlibraryexpert projectlibraryexpert = new ZgsProjectlibraryexpert();
                                            projectlibraryexpert.setId(zgsProjectlibraryexpert.getId());
                                            projectlibraryexpert.setAuditconclusionnum(null);
                                            zgsProjectlibraryexpertService.updateById(projectlibraryexpert);
                                        }
                                    }
                                }
                            } else {
                                zgsBuildproject.setApplydate(null);
                                //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsBuildproject1.getStatus())) {
                                    zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                                }
                                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                                if (StringUtils.isNotEmpty(zgsBuildproject1.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(zgsBuildproject1.getStatus())) {
                                    zgsBuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                                }
                            }
                            //
                            zgsBuildproject.setModifyaccountname(sysUser.getUsername());
                            zgsBuildproject.setModifyusername(sysUser.getRealname());
                            zgsBuildproject.setModifydate(new Date());
                            zgsBuildproject.setCreatedate(null);
                            zgsBuildproject.setFinishdate(null);
                            zgsBuildproject.setFirstdate(null);
                            stagestatus = zgsBuildproject.getStatus();
                            zgsBuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildproject, ValidateEncryptEntityUtil.isEncrypt);
                            zgsBuildprojectService.updateById(zgsBuildproject);
                            //项目主要参加人员
                            List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList = zgsProjectlibrary.getZgsBuildrojectmainparticipantList();
                            QueryWrapper<ZgsBuildrojectmainparticipant> queryWrapper3_2 = new QueryWrapper<>();
                            queryWrapper3_2.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_2.eq("buildguid", bid);
                            queryWrapper3_2.ne("isdelete", 1);
                            queryWrapper3_2.orderByAsc("ordernum");
                            List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList_0 = zgsBuildrojectmainparticipantService.list(queryWrapper3_2);
                            if (zgsBuildrojectmainparticipantList != null && zgsBuildrojectmainparticipantList.size() > 0) {
                                if (zgsBuildrojectmainparticipantList.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < zgsBuildrojectmainparticipantList.size(); a0++) {
                                    ZgsBuildrojectmainparticipant plb = zgsBuildrojectmainparticipantList.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsBuildrojectmainparticipantService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsBuildrojectmainparticipantService.save(plb);
                                    }
                                    for (ZgsBuildrojectmainparticipant plb1 : zgsBuildrojectmainparticipantList_0) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsBuildrojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsBuildrojectmainparticipantList_0 != null && zgsBuildrojectmainparticipantList_0.size() > 0) {
                                    for (ZgsBuildrojectmainparticipant plb : zgsBuildrojectmainparticipantList_0) {
                                        zgsBuildrojectmainparticipantService.removeById(plb.getId());
                                    }
                                }
                            }
                        } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            ZgsAssembleproject zgsAssembleproject = zgsProjectlibrary.getZgsAssembleproject();
                            bid = zgsAssembleproject.getId();
                            //
                            ZgsAssembleproject zgsAssembleproject1 = zgsAssembleprojectService.getById(bid);
                            if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsAssembleproject().getStatus())) {
                                zgsAssembleproject.setAuditopinion(null);
                                //保存并上报
                                if (!VerificationUtil.projectLeaderVerification(leader)) {
                                    return Result.error("项目负责人名称填写不规范！");
                                }
                                zgsAssembleproject.setApplydate(new Date());
//                        zgsAssembleproject.setReturntype(null);
                                //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsAssembleproject1.getStatus())) {
                                    zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                                    //查分配所有修改专家状态
                                    QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
                                    queryWrapper.eq("businessguid", bid);
                                    List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(queryWrapper);
                                    if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
                                        for (ZgsProjectlibraryexpert zgsProjectlibraryexpert : zgsProjectlibraryexpertList) {
                                            ZgsProjectlibraryexpert projectlibraryexpert = new ZgsProjectlibraryexpert();
                                            projectlibraryexpert.setId(zgsProjectlibraryexpert.getId());
                                            projectlibraryexpert.setAuditconclusionnum(null);
                                            zgsProjectlibraryexpertService.updateById(projectlibraryexpert);
                                        }
                                    }
                                }
                            } else {
                                zgsAssembleproject.setApplydate(null);
                                //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsAssembleproject1.getStatus())) {
                                    zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                                }
                                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                                if (StringUtils.isNotEmpty(zgsAssembleproject1.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(zgsAssembleproject1.getStatus())) {
                                    zgsAssembleproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                                }
                            }
                            //
                            zgsAssembleproject.setModifyaccountname(sysUser.getUsername());
                            zgsAssembleproject.setModifyusername(sysUser.getRealname());
                            zgsAssembleproject.setModifydate(new Date());
                            zgsAssembleproject.setCreatedate(null);
                            zgsAssembleproject.setFinishdate(null);
                            zgsAssembleproject.setFirstdate(null);
                            stagestatus = zgsAssembleproject.getStatus();
                            zgsAssembleproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsAssembleproject, ValidateEncryptEntityUtil.isEncrypt);
                            zgsAssembleprojectService.updateById(zgsAssembleproject);
                            //项目主要参加人员0建设单位负责人
                            List<ZgsAssemprojectmainparticipant> plbList = zgsProjectlibrary.getZgsAssemprojectmainparticipantList0();
                            QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper3_5_1 = new QueryWrapper<>();
                            queryWrapper3_5_1.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_5_1.eq("buildguid", bid);
                            queryWrapper3_5_1.eq("persontype", "0");
                            queryWrapper3_5_1.ne("isdelete", 1);
                            queryWrapper3_5_1.orderByAsc("ordernum");
                            List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList0 = zgsAssemprojectmainparticipantService.list(queryWrapper3_5_1);
                            if (plbList != null && plbList.size() > 0) {
                                if (plbList.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList.size(); a0++) {
                                    ZgsAssemprojectmainparticipant plb = plbList.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemprojectmainparticipantService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setPersontype("0");
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemprojectmainparticipantService.save(plb);
                                    }
                                    for (ZgsAssemprojectmainparticipant plb1 : zgsAssemprojectmainparticipantList0) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsAssemprojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsAssemprojectmainparticipantList0 != null && zgsAssemprojectmainparticipantList0.size() > 0) {
                                    for (ZgsAssemprojectmainparticipant plb : zgsAssemprojectmainparticipantList0) {
                                        zgsAssemprojectmainparticipantService.removeById(plb.getId());
                                    }
                                }
                            }
                            //项目主要参加人员1联合申报单位负责人
                            List<ZgsAssemprojectmainparticipant> plbList1 = zgsProjectlibrary.getZgsAssemprojectmainparticipantList1();
                            QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper3_5_0 = new QueryWrapper<>();
                            queryWrapper3_5_0.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_5_0.eq("buildguid", bid);
                            queryWrapper3_5_0.eq("persontype", "1");
                            queryWrapper3_5_0.ne("isdelete", 1);
                            queryWrapper3_5_0.orderByAsc("ordernum");
                            List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList1 = zgsAssemprojectmainparticipantService.list(queryWrapper3_5_0);
                            if (plbList1 != null && plbList1.size() > 0) {
                                if (plbList1.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList1.size(); a0++) {
                                    ZgsAssemprojectmainparticipant plb = plbList1.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemprojectmainparticipantService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setPersontype("1");
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemprojectmainparticipantService.save(plb);
                                    }
                                    for (ZgsAssemprojectmainparticipant plb1 : zgsAssemprojectmainparticipantList1) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsAssemprojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsAssemprojectmainparticipantList1 != null && zgsAssemprojectmainparticipantList1.size() > 0) {
                                    for (ZgsAssemprojectmainparticipant plb : zgsAssemprojectmainparticipantList1) {
                                        zgsAssemprojectmainparticipantService.removeById(plb.getId());
                                    }
                                }
                            }
                            //单体建筑
                            List<ZgsAssemblesingleproject> plbList2 = zgsProjectlibrary.getZgsAssemblesingleprojectList();
                            QueryWrapper<ZgsAssemblesingleproject> queryWrapper1_5 = new QueryWrapper<>();
                            queryWrapper1_5.eq("enterpriseguid", enterpriseguid);
                            queryWrapper1_5.eq("buildguid", bid);
                            queryWrapper1_5.ne("isdelete", 1);
                            List<ZgsAssemblesingleproject> zgsAssemblesingleprojectList = zgsAssemblesingleprojectService.list(queryWrapper1_5);
                            if (plbList2 != null && plbList2.size() > 0) {
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList2.size(); a0++) {
                                    ZgsAssemblesingleproject plb = plbList2.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemblesingleprojectService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsAssemblesingleprojectService.save(plb);
                                    }
                                    for (ZgsAssemblesingleproject plb1 : zgsAssemblesingleprojectList) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsAssemblesingleprojectService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsAssemblesingleprojectList != null && zgsAssemblesingleprojectList.size() > 0) {
                                    for (ZgsAssemblesingleproject plb : zgsAssemblesingleprojectList) {
                                        zgsAssemblesingleprojectService.removeById(plb.getId());
                                    }
                                }
                            }
                        } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            ZgsEnergybuildproject zgsEnergybuildproject = zgsProjectlibrary.getZgsEnergybuildproject();
                            bid = zgsEnergybuildproject.getId();
                            //
                            ZgsEnergybuildproject zgsEnergybuildproject1 = zgsEnergybuildprojectService.getById(bid);
                            if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsEnergybuildproject().getStatus())) {
                                zgsEnergybuildproject.setAuditopinion(null);
                                //保存并上报
                                if (!VerificationUtil.projectLeaderVerification(leader)) {
                                    return Result.error("项目负责人名称填写不规范！");
                                }
                                zgsEnergybuildproject.setApplydate(new Date());
//                        zgsEnergybuildproject.setReturntype(null);
                                //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsEnergybuildproject1.getStatus())) {
                                    zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                                    //查分配所有修改专家状态
                                    QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
                                    queryWrapper.eq("businessguid", bid);
                                    List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(queryWrapper);
                                    if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
                                        for (ZgsProjectlibraryexpert zgsProjectlibraryexpert : zgsProjectlibraryexpertList) {
                                            ZgsProjectlibraryexpert projectlibraryexpert = new ZgsProjectlibraryexpert();
                                            projectlibraryexpert.setId(zgsProjectlibraryexpert.getId());
                                            projectlibraryexpert.setAuditconclusionnum(null);
                                            zgsProjectlibraryexpertService.updateById(projectlibraryexpert);
                                        }
                                    }
                                }
                            } else {
                                zgsEnergybuildproject.setApplydate(null);
                                //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsEnergybuildproject1.getStatus())) {
                                    zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                                }
                                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                                if (StringUtils.isNotEmpty(zgsEnergybuildproject1.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(zgsEnergybuildproject1.getStatus())) {
                                    zgsEnergybuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                                }
                            }
                            //
                            zgsEnergybuildproject.setModifyaccountname(sysUser.getUsername());
                            zgsEnergybuildproject.setModifyusername(sysUser.getRealname());
                            zgsEnergybuildproject.setModifydate(new Date());
                            zgsEnergybuildproject.setCreatedate(null);
                            zgsEnergybuildproject.setFinishdate(null);
                            zgsEnergybuildproject.setFirstdate(null);
                            stagestatus = zgsEnergybuildproject.getStatus();
                            zgsEnergybuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsEnergybuildproject, ValidateEncryptEntityUtil.isEncrypt);
                            zgsEnergybuildprojectService.updateById(zgsEnergybuildproject);
                            //项目主要参加人员
                            List<ZgsEnergyprojectmainparticipant> plbList = zgsProjectlibrary.getZgsEnergyprojectmainparticipantList();
                            QueryWrapper<ZgsEnergyprojectmainparticipant> queryWrapper3_4 = new QueryWrapper<>();
                            queryWrapper3_4.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_4.eq("buildguid", bid);
                            queryWrapper3_4.ne("isdelete", 1);
                            queryWrapper3_4.orderByAsc("ordernum");
                            List<ZgsEnergyprojectmainparticipant> zgsEnergyprojectmainparticipantList = zgsEnergyprojectmainparticipantService.list(queryWrapper3_4);
                            if (plbList != null && plbList.size() > 0) {
                                if (plbList.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList.size(); a0++) {
                                    ZgsEnergyprojectmainparticipant plb = plbList.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsEnergyprojectmainparticipantService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsEnergyprojectmainparticipantService.save(plb);
                                    }
                                    for (ZgsEnergyprojectmainparticipant plb1 : zgsEnergyprojectmainparticipantList) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsEnergyprojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsEnergyprojectmainparticipantList != null && zgsEnergyprojectmainparticipantList.size() > 0) {
                                    for (ZgsEnergyprojectmainparticipant plb : zgsEnergyprojectmainparticipantList) {
                                        zgsEnergyprojectmainparticipantService.removeById(plb.getId());
                                    }
                                }
                            }
                            //计划进度与安排
                            List<ZgsEnergyprojectplanarrange> plbList1 = zgsProjectlibrary.getZgsEnergyprojectplanarrangeList();
                            QueryWrapper<ZgsEnergyprojectplanarrange> queryWrapper1_4 = new QueryWrapper<>();
                            queryWrapper1_4.eq("enterpriseguid", enterpriseguid);
                            queryWrapper1_4.eq("buildguid", bid);
                            queryWrapper1_4.ne("isdelete", 1);
                            List<ZgsEnergyprojectplanarrange> zgsEnergyprojectplanarrangeList = zgsEnergyprojectplanarrangeService.list(queryWrapper1_4);
                            if (plbList1 != null && plbList1.size() > 0) {
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList1.size(); a0++) {
                                    ZgsEnergyprojectplanarrange plb = plbList1.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsEnergyprojectplanarrangeService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsEnergyprojectplanarrangeService.save(plb);
                                    }
                                    for (ZgsEnergyprojectplanarrange plb1 : zgsEnergyprojectplanarrangeList) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsEnergyprojectplanarrangeService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsEnergyprojectplanarrangeList != null && zgsEnergyprojectplanarrangeList.size() > 0) {
                                    for (ZgsEnergyprojectplanarrange plb : zgsEnergyprojectplanarrangeList) {
                                        zgsEnergyprojectplanarrangeService.removeById(plb.getId());
                                    }
                                }
                            }
                        } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            ZgsSamplebuildproject zgsSamplebuildproject = zgsProjectlibrary.getZgsSamplebuildproject();
                            bid = zgsSamplebuildproject.getId();
                            //
                            ZgsSamplebuildproject zgsSamplebuildproject1 = zgsSamplebuildprojectService.getById(bid);
                            if (GlobalConstants.handleSubmit.equals(zgsProjectlibrary.getZgsSamplebuildproject().getStatus())) {
                                zgsSamplebuildproject.setAuditopinion(null);
                                //保存并上报
                                if (!VerificationUtil.projectLeaderVerification(leader)) {
                                    return Result.error("项目负责人名称填写不规范！");
                                }
                                zgsSamplebuildproject.setApplydate(new Date());
//                        zgsSamplebuildproject.setReturntype(null);
                                //如果之前是专家驳回，状态修改为终审通过，退回原因赋空
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsSamplebuildproject1.getStatus())) {
                                    zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS8);
                                    //查分配所有修改专家状态
                                    QueryWrapper<ZgsProjectlibraryexpert> queryWrapper = new QueryWrapper();
                                    queryWrapper.eq("businessguid", bid);
                                    List<ZgsProjectlibraryexpert> zgsProjectlibraryexpertList = zgsProjectlibraryexpertService.list(queryWrapper);
                                    if (zgsProjectlibraryexpertList != null && zgsProjectlibraryexpertList.size() > 0) {
                                        for (ZgsProjectlibraryexpert zgsProjectlibraryexpert : zgsProjectlibraryexpertList) {
                                            ZgsProjectlibraryexpert projectlibraryexpert = new ZgsProjectlibraryexpert();
                                            projectlibraryexpert.setId(zgsProjectlibraryexpert.getId());
                                            projectlibraryexpert.setAuditconclusionnum(null);
                                            zgsProjectlibraryexpertService.updateById(projectlibraryexpert);
                                        }
                                    }
                                }
                            } else {
                                zgsSamplebuildproject.setApplydate(null);
                                //如果之前是专家驳回,点击保存依然保存成专家驳回状态
                                if (GlobalConstants.SHENHE_STATUS6.equals(zgsSamplebuildproject1.getStatus())) {
                                    zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS6);
                                }
                                //初审退回的项目，点编辑并保存的时候状态默认还是初审退回
                                if (StringUtils.isNotEmpty(zgsSamplebuildproject1.getStatus()) && GlobalConstants.SHENHE_STATUS3.equals(zgsSamplebuildproject1.getStatus())) {
                                    zgsSamplebuildproject.setStatus(GlobalConstants.SHENHE_STATUS3);
                                }
                            }
                            //
                            zgsSamplebuildproject.setModifyaccountname(sysUser.getUsername());
                            zgsSamplebuildproject.setModifyusername(sysUser.getRealname());
                            zgsSamplebuildproject.setModifydate(new Date());
                            zgsSamplebuildproject.setCreatedate(null);
                            zgsSamplebuildproject.setFinishdate(null);
                            zgsSamplebuildproject.setFirstdate(null);
                            stagestatus = zgsSamplebuildproject.getStatus();
                            zgsSamplebuildproject = ValidateEncryptEntityUtil.validateEncryptObject(zgsSamplebuildproject, ValidateEncryptEntityUtil.isEncrypt);
                            zgsSamplebuildprojectService.updateById(zgsSamplebuildproject);
                            //项目主要参加人员
                            List<ZgsSamplerojectmainparticipant> plbList = zgsProjectlibrary.getZgsSamplerojectmainparticipantList();
                            QueryWrapper<ZgsSamplerojectmainparticipant> queryWrapper3_3 = new QueryWrapper<>();
                            queryWrapper3_3.eq("enterpriseguid", enterpriseguid);
                            queryWrapper3_3.eq("buildguid", bid);
                            queryWrapper3_3.ne("isdelete", 1);
                            queryWrapper3_3.orderByAsc("ordernum");
                            List<ZgsSamplerojectmainparticipant> zgsSamplerojectmainparticipantList = zgsSamplerojectmainparticipantService.list(queryWrapper3_3);
                            if (plbList != null && plbList.size() > 0) {
                                if (plbList.size() > 15) {
                                    return Result.error("人员名单数量不得超过15人！");
                                }
                                Map<String, String> map0 = new HashMap<>();
                                List<String> list0 = new ArrayList<>();
                                for (int a0 = 0; a0 < plbList.size(); a0++) {
                                    ZgsSamplerojectmainparticipant plb = plbList.get(a0);
                                    if (StringUtils.isNotEmpty(plb.getId())) {
                                        //编辑
                                        plb.setModifyaccountname(sysUser.getUsername());
                                        plb.setModifyusername(sysUser.getRealname());
                                        plb.setModifydate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsSamplerojectmainparticipantService.updateById(plb);
                                    } else {
                                        plb.setId(UUID.randomUUID().toString());
                                        plb.setBuildguid(bid);
                                        plb.setEnterpriseguid(sysUser.getEnterpriseguid());
                                        plb.setCreateaccountname(sysUser.getUsername());
                                        plb.setCreateusername(sysUser.getRealname());
                                        plb.setCreatedate(new Date());
                                        plb = ValidateEncryptEntityUtil.validateEncryptObject(plb, ValidateEncryptEntityUtil.isEncrypt);
                                        zgsSamplerojectmainparticipantService.save(plb);
                                    }
                                    for (ZgsSamplerojectmainparticipant plb1 : zgsSamplerojectmainparticipantList) {
                                        if (!plb1.getId().equals(plb.getId())) {
                                            map0.put(plb1.getId(), plb1.getId());
                                        } else {
                                            list0.add(plb1.getId());
                                        }
                                    }
                                }
                                map0.keySet().removeAll(list0);
                                zgsSamplerojectmainparticipantService.removeByIds(new ArrayList<>(map0.keySet()));
                            } else {
                                if (zgsSamplerojectmainparticipantList != null && zgsSamplerojectmainparticipantList.size() > 0) {
                                    for (ZgsSamplerojectmainparticipant plb : zgsSamplerojectmainparticipantList) {
                                        zgsSamplerojectmainparticipantService.removeById(plb.getId());
                                    }
                                }
                            }
                        }
                    }
                    zgsProjectlibrary.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_1));
                    zgsProjectlibrary.setProjectstagestatus(stagestatus);
                    // 将授权编辑变为 取消授权编辑（说明：终审单位的此次授权编辑已使用完，想要修改，必须重新授权）
                    if (StringUtils.isNotBlank(zgsProjectlibrary.getSqbj()) && StringUtils.isNotBlank(zgsProjectlibrary.getUpdateSave())) {
                        zgsProjectlibrary.setSqbj("2");  // 取消编辑
                        zgsProjectlibrary.setUpdateSave("2");  // 修稿已提交
                    }

                    zgsProjectlibrary = ValidateEncryptEntityUtil.validateEncryptObject(zgsProjectlibrary, ValidateEncryptEntityUtil.isEncrypt);
                    zgsProjectlibraryService.updateById(zgsProjectlibrary);

                    // update  专家信息修改(查询原来的专家信息进行删除，保存新的专家信息)   rxl  20230620
                    QueryWrapper<ZgsSacceptcertexpert> expertQueryWrapper = new QueryWrapper<>();
                    expertQueryWrapper.eq("baseguid", zgsProjectlibrary.getId());
                    expertQueryWrapper.eq("project_stage", "sbjd");
                    expertQueryWrapper.ne("isdelete", 1);
                    List<ZgsSacceptcertexpert> ZgsSacceptcertexpertList = zgsSacceptcertexpertService.list(expertQueryWrapper);
                    if (ZgsSacceptcertexpertList.size() > 0) {
                        // 删除历史专家信息
                        QueryWrapper<ZgsSacceptcertexpert> expertDeleteWrapper = new QueryWrapper<>();
                        expertDeleteWrapper.eq("baseguid", zgsProjectlibrary.getId());
                        expertDeleteWrapper.eq("project_stage", "sbjd");
                        expertDeleteWrapper.ne("isdelete", 1);
                        boolean bolResult = zgsSacceptcertexpertService.remove(expertDeleteWrapper);
                        // 删除成功后，保存修改后的专家信息  rxl  20230620
                        if (bolResult) {
                            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsProjectlibrary.getZgsSacceptcertexpertList();
                            if (zgsSacceptcertexpertList != null && zgsSacceptcertexpertList.size() > 0) {
                                for (int a1 = 0; a1 < zgsSacceptcertexpertList.size(); a1++) {
                                    ZgsSacceptcertexpert zgsSacceptcertexpertForUpdate = zgsSacceptcertexpertList.get(a1);
                                    zgsSacceptcertexpertForUpdate.setId(UUID.randomUUID().toString());
                                    zgsSacceptcertexpertForUpdate.setBaseguid(zgsProjectlibrary.getId());
                                    zgsSacceptcertexpertForUpdate.setProjectStage("sbjd");   // 申报阶段
                                    zgsSacceptcertexpertForUpdate.setEnterpriseguid(sysUser.getEnterpriseguid());
                                    zgsSacceptcertexpertForUpdate.setCreateaccountname(sysUser.getUsername());
                                    zgsSacceptcertexpertForUpdate.setCreateusername(sysUser.getRealname());
                                    zgsSacceptcertexpertForUpdate.setCreatedate(new Date());
                                    zgsSacceptcertexpertService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsSacceptcertexpertForUpdate, ValidateEncryptEntityUtil.isEncrypt));
                                }
                            }
                        }
                    }

                    //联合申报单位
                    List<ZgsBuildjointunit> zgsBuildjointunitList = zgsProjectlibrary.getZgsBuildjointunitList();
                    QueryWrapper<ZgsBuildjointunit> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("enterpriseguid", enterpriseguid);
                    queryWrapper2.eq("buildguid", bid);
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsBuildjointunit> zgsBuildjointunitList_0 = zgsBuildjointunitService.list(queryWrapper2);
                    if (zgsBuildjointunitList != null && zgsBuildjointunitList.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsBuildjointunitList.size(); a0++) {
                            ZgsBuildjointunit zgsBuildjointunit = zgsBuildjointunitList.get(a0);
                            if (StringUtils.isNotEmpty(zgsBuildjointunit.getId())) {
                                //编辑
                                zgsBuildjointunit.setModifyaccountname(sysUser.getUsername());
                                zgsBuildjointunit.setModifyusername(sysUser.getRealname());
                                zgsBuildjointunit.setModifydate(new Date());
                                zgsBuildjointunit = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt);
                                zgsBuildjointunitService.updateById(zgsBuildjointunit);
                            } else {
                                zgsBuildjointunit.setId(UUID.randomUUID().toString());
                                zgsBuildjointunit.setBuildguid(bid);
                                zgsBuildjointunit.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsBuildjointunit.setCreateaccountname(sysUser.getUsername());
                                zgsBuildjointunit.setCreateusername(sysUser.getRealname());
                                zgsBuildjointunit.setCreatedate(new Date());
                                zgsBuildjointunit = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildjointunit, ValidateEncryptEntityUtil.isEncrypt);
                                zgsBuildjointunitService.save(zgsBuildjointunit);
                            }
                            for (ZgsBuildjointunit plb1 : zgsBuildjointunitList_0) {
                                if (!plb1.getId().equals(zgsBuildjointunit.getId())) {
                                    map0.put(plb1.getId(), plb1.getId());
                                } else {
                                    list0.add(plb1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsBuildjointunitService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsBuildjointunitList_0 != null && zgsBuildjointunitList_0.size() > 0) {
                            for (ZgsBuildjointunit plb : zgsBuildjointunitList_0) {
                                zgsBuildjointunitService.removeById(plb.getId());
                            }
                        }
                    }
                    //经费预算
                    List<ZgsBuildfundbudget> zgsBuildfundbudgetList = zgsProjectlibrary.getZgsBuildfundbudgetList();
                    QueryWrapper<ZgsBuildfundbudget> queryWrapper6 = new QueryWrapper<>();
                    queryWrapper6.eq("enterpriseguid", enterpriseguid);
                    queryWrapper6.eq("buildguid", bid);
                    queryWrapper6.ne("isdelete", 1);
                    queryWrapper6.orderByAsc("year");
                    List<ZgsBuildfundbudget> zgsBuildfundbudgetList_0 = zgsBuildfundbudgetService.list(queryWrapper6);
                    if (zgsBuildfundbudgetList != null && zgsBuildfundbudgetList.size() > 0) {
                        Map<String, String> map0 = new HashMap<>();
                        List<String> list0 = new ArrayList<>();
                        for (int a0 = 0; a0 < zgsBuildfundbudgetList.size(); a0++) {
                            ZgsBuildfundbudget zgsBuildfundbudget = zgsBuildfundbudgetList.get(a0);
                            if (StringUtils.isNotEmpty(zgsBuildfundbudget.getId())) {
                                //编辑
                                zgsBuildfundbudget.setModifyaccountname(sysUser.getUsername());
                                zgsBuildfundbudget.setModifyusername(sysUser.getRealname());
                                zgsBuildfundbudget.setModifydate(new Date());
                                zgsBuildfundbudget = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildfundbudget, ValidateEncryptEntityUtil.isEncrypt);
                                zgsBuildfundbudgetService.updateById(zgsBuildfundbudget);
                            } else {
                                zgsBuildfundbudget.setId(UUID.randomUUID().toString());
                                zgsBuildfundbudget.setBuildguid(bid);
                                zgsBuildfundbudget.setEnterpriseguid(sysUser.getEnterpriseguid());
                                zgsBuildfundbudget.setCreateaccountname(sysUser.getUsername());
                                zgsBuildfundbudget.setCreateusername(sysUser.getRealname());
                                zgsBuildfundbudget.setCreatedate(new Date());
                                zgsBuildfundbudget = ValidateEncryptEntityUtil.validateEncryptObject(zgsBuildfundbudget, ValidateEncryptEntityUtil.isEncrypt);
                                zgsBuildfundbudgetService.save(zgsBuildfundbudget);
                            }
                            for (ZgsBuildfundbudget plb1 : zgsBuildfundbudgetList_0) {
                                if (!plb1.getId().equals(zgsBuildfundbudget.getId())) {
                                    map0.put(plb1.getId(), plb1.getId());
                                } else {
                                    list0.add(plb1.getId());
                                }
                            }
                        }
                        map0.keySet().removeAll(list0);
                        zgsBuildfundbudgetService.removeByIds(new ArrayList<>(map0.keySet()));
                    } else {
                        if (zgsBuildfundbudgetList_0 != null && zgsBuildfundbudgetList_0.size() > 0) {
                            for (ZgsBuildfundbudget plb : zgsBuildfundbudgetList_0) {
                                zgsBuildfundbudgetService.removeById(plb.getId());
                            }
                        }
                    }
                    //相关附件
                    List<ZgsMattermaterial> zgsMattermaterialList = zgsProjectlibrary.getZgsMattermaterialList();
                    if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                        for (int a0 = 0; a0 < zgsMattermaterialList.size(); a0++) {
                            ZgsMattermaterial zgsMattermaterial = zgsMattermaterialList.get(a0);
                            String mattermaterialId = zgsMattermaterial.getId();
                            UpdateWrapper<ZgsAttachappendix> wrapper = new UpdateWrapper();
                            wrapper.eq("relationid", mattermaterialId);
                            //先删除原来的再重新添加
                            zgsAttachappendixService.remove(wrapper);
                            if (zgsMattermaterial != null && zgsMattermaterial.getZgsAttachappendixList() != null && zgsMattermaterial.getZgsAttachappendixList().size() > 0) {
                                for (int a1 = 0; a1 < zgsMattermaterial.getZgsAttachappendixList().size(); a1++) {
                                    ZgsAttachappendix zgsAttachappendix = zgsMattermaterial.getZgsAttachappendixList().get(a1);
                                    if (zgsAttachappendix != null && StringUtils.isNotEmpty(zgsAttachappendix.getId()) && StringUtils.isNotEmpty(zgsAttachappendix.getRelationid())) {
                                        zgsAttachappendix.setModifypersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setModifypersonname(sysUser.getRealname());
                                        zgsAttachappendix.setModifytime(new Date());
                                    } else {
                                        zgsAttachappendix.setId(UUID.randomUUID().toString());
                                        zgsAttachappendix.setRelationid(mattermaterialId);
                                        zgsAttachappendix.setCreatepersonaccount(sysUser.getUsername());
                                        zgsAttachappendix.setCreatepersonname(sysUser.getRealname());
                                        zgsAttachappendix.setCreatetime(new Date());
                                        zgsAttachappendix.setAppendixtype(zgsProjectlibrary.getProjecttypenum());
                                        zgsAttachappendix.setAppendixsubtype("ProofMaterial");
                                    }
                                    zgsAttachappendix = ValidateEncryptEntityUtil.validateEncryptObject(zgsAttachappendix, ValidateEncryptEntityUtil.isEncrypt);
                                    zgsAttachappendixService.save(zgsAttachappendix);
                                }
                            }
                        }
                    }
                }

            }

        }
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建筑工程项目库-通过id删除")
    @ApiOperation(value = "建筑工程项目库-通过id删除", notes = "建筑工程项目库-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjectlibraryService.deleteByItemId(id);
        zgsProjectlibraryService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "建筑工程项目库-批量删除")
    @ApiOperation(value = "建筑工程项目库-批量删除", notes = "建筑工程项目库-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        zgsProjectlibraryService.deleteByItemId(ids);
        zgsProjectlibraryService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "建筑工程项目库-通过id查询")
    @ApiOperation(value = "建筑工程项目库-通过id查询", notes = "建筑工程项目库-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = false) String id, @RequestParam(name = "assType") Integer assType) {
        ZgsProjectlibrary zgsProjectlibrary = null;
        if (StringUtils.isNotEmpty(id)) {
            zgsProjectlibrary = zgsProjectlibraryService.getById(id);
            if (zgsProjectlibrary == null) {
                return Result.error("未找到对应数据");
            } else {
                initProjectSelectById(zgsProjectlibrary, assType);
            }
        } else {
            //新增时查出附件
            zgsProjectlibrary = new ZgsProjectlibrary();
            Map<String, Object> map = new HashMap<>();
            //GreenBuildType、ConstructBuidType、AssemBuidType、EnergyBuildType、SampleBuidType
            switch (assType) {
                case 1:
                    map.put("projecttypenum", GlobalConstants.GreenBuildType);
                    zgsProjectlibrary.setProjecttypenum(GlobalConstants.GreenBuildType);
                    break;
                case 2:
                    map.put("projecttypenum", GlobalConstants.ConstructBuidType);
                    zgsProjectlibrary.setProjecttypenum(GlobalConstants.ConstructBuidType);
                    break;
                case 3:
                    map.put("projecttypenum", GlobalConstants.SampleBuidType);
                    zgsProjectlibrary.setProjecttypenum(GlobalConstants.SampleBuidType);
                    break;
                case 4:
                    map.put("projecttypenum", GlobalConstants.EnergyBuildType);
                    zgsProjectlibrary.setProjecttypenum(GlobalConstants.EnergyBuildType);
                    break;
                case 5:
                    map.put("projecttypenum", GlobalConstants.AssemBuidType);
                    zgsProjectlibrary.setProjecttypenum(GlobalConstants.AssemBuidType);
                    break;
            }
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getAddZgsMattermaterialList(map);
            zgsProjectlibrary.setZgsMattermaterialList(zgsMattermaterialList);
            zgsProjectlibrary.setSpLogStatus(0);
        }
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectlibrary, ValidateEncryptEntityUtil.isDecrypt));
    }

    /**
     * 初始化查询示范项目
     *
     * @param zgsProjectlibrary
     * @param assType
     */
    private void initProjectSelectById(ZgsProjectlibrary zgsProjectlibrary, Integer assType) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        String enterpriseguid = null;
        String buildguid = null;
        String projecttypenum = zgsProjectlibrary.getProjecttypenum();
        Map<String, Object> map = new HashMap<>();
        map.put("projecttypenum", projecttypenum);
        switch (assType) {
            case 1:
                //绿色建筑
                QueryWrapper<ZgsGreenbuildproject> queryWrapper_1 = new QueryWrapper<>();
                queryWrapper_1.eq("projectlibraryguid", zgsProjectlibrary.getId());
                queryWrapper_1.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                queryWrapper_1.ne("isdelete", 1);
                ZgsGreenbuildproject zgsGreenbuildproject = zgsGreenbuildprojectService.getOne(queryWrapper_1);
                if (ObjectUtil.isNotNull(zgsGreenbuildproject)) {
                    zgsProjectlibrary.setAgreeproject(zgsGreenbuildproject.getAgreeproject());
                    zgsProjectlibrary.setStatus(zgsGreenbuildproject.getStatus());
                    zgsProjectlibrary.setZgsGreenbuildproject(ValidateEncryptEntityUtil.validateDecryptObject(zgsGreenbuildproject, ValidateEncryptEntityUtil.isDecrypt));
                    enterpriseguid = zgsGreenbuildproject.getEnterpriseguid();
                    buildguid = zgsGreenbuildproject.getId();
                    //新增
                    if ((zgsGreenbuildproject.getFirstdate() != null || zgsGreenbuildproject.getFinishdate() != null) && zgsGreenbuildproject.getApplydate() != null) {
                        zgsProjectlibrary.setSpLogStatus(1);
                    } else {
                        zgsProjectlibrary.setSpLogStatus(0);
                    }
                }
                //屏蔽法人设置，先判断是否为专家用户类型，再查询
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                    QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                    zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                    ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                    QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                    zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                    zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                    ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                    if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                        String setValue = zgsExpertset.getSetvalue();
                        if (setValue.contains(",")) {
                            String strValue[] = setValue.split(",");
                            for (String set : strValue) {
                                if (GlobalConstants.expert_set_close1.equals(set)) {
                                    zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                    zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                    zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close4.equals(set)) {
                                }
                            }
                        } else {
                            if (GlobalConstants.expert_set_close1.equals(setValue)) {
                                zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                            }
                        }
                    }
                }
                //专家评审
                zgsProjectlibrary.setZgsSofttechexpertList(zgsProjectlibraryexpertService.getZgsProjectlibraryexpertList(buildguid));
                //
                //项目主要参加人员
                QueryWrapper<ZgsGreenprojectmainparticipant> queryWrapper3_1 = new QueryWrapper<>();
                queryWrapper3_1.eq("enterpriseguid", enterpriseguid);
                queryWrapper3_1.eq("buildguid", buildguid);
                queryWrapper3_1.ne("isdelete", 1);
                queryWrapper3_1.orderByAsc("ordernum");
                queryWrapper3_1.last("limit 15");
                List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList = zgsGreenprojectmainparticipantService.list(queryWrapper3_1);
                zgsProjectlibrary.setZgsGreenprojectmainparticipantList(ValidateEncryptEntityUtil.validateDecryptList(zgsGreenprojectmainparticipantList, ValidateEncryptEntityUtil.isDecrypt));
                //计划进度与安排赋值
                QueryWrapper<ZgsGreenprojectplanarrange> queryWrapper1_1 = new QueryWrapper<>();
                queryWrapper1_1.eq("enterpriseguid", enterpriseguid);
                queryWrapper1_1.eq("buildguid", buildguid);
                queryWrapper1_1.ne("isdelete", 1);
                List<ZgsGreenprojectplanarrange> zgsGreenprojectplanarrangeList = zgsGreenprojectplanarrangeService.list(queryWrapper1_1);
                zgsProjectlibrary.setZgsGreenprojectplanarrangeList(ValidateEncryptEntityUtil.validateDecryptList(zgsGreenprojectplanarrangeList, ValidateEncryptEntityUtil.isDecrypt));

                break;
            case 2:
                //建筑工程
                QueryWrapper<ZgsBuildproject> queryWrapper_2 = new QueryWrapper<>();
                queryWrapper_2.eq("projectlibraryguid", zgsProjectlibrary.getId());
                queryWrapper_2.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                queryWrapper_2.ne("isdelete", 1);
                ZgsBuildproject zgsBuildproject = zgsBuildprojectService.getOne(queryWrapper_2);
                zgsProjectlibrary.setAgreeproject(zgsBuildproject.getAgreeproject());
                zgsProjectlibrary.setStatus(zgsBuildproject.getStatus());
                zgsProjectlibrary.setZgsBuildproject(ValidateEncryptEntityUtil.validateDecryptObject(zgsBuildproject, ValidateEncryptEntityUtil.isDecrypt));
                enterpriseguid = zgsBuildproject.getEnterpriseguid();
                buildguid = zgsBuildproject.getId();
                //新增
                if ((zgsBuildproject.getFirstdate() != null || zgsBuildproject.getFinishdate() != null) && zgsBuildproject.getApplydate() != null) {
                    zgsProjectlibrary.setSpLogStatus(1);
                } else {
                    zgsProjectlibrary.setSpLogStatus(0);
                }
                //屏蔽法人设置，先判断是否为专家用户类型，再查询
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                    QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                    zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                    ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                    QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                    zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                    zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                    ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                    if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                        String setValue = zgsExpertset.getSetvalue();
                        if (setValue.contains(",")) {
                            String strValue[] = setValue.split(",");
                            for (String set : strValue) {
                                if (GlobalConstants.expert_set_close1.equals(set)) {
                                    zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                    zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                    zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close4.equals(set)) {
                                }
                            }
                        } else {
                            if (GlobalConstants.expert_set_close1.equals(setValue)) {
                                zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                            }
                        }
                    }
                }
                //专家评审
                zgsProjectlibrary.setZgsSofttechexpertList(zgsProjectlibraryexpertService.getZgsProjectlibraryexpertList(buildguid));
                //
                //项目主要参加人员
                QueryWrapper<ZgsBuildrojectmainparticipant> queryWrapper3_2 = new QueryWrapper<>();
                queryWrapper3_2.eq("enterpriseguid", enterpriseguid);
                queryWrapper3_2.eq("buildguid", buildguid);
                queryWrapper3_2.ne("isdelete", 1);
                queryWrapper3_2.orderByAsc("ordernum");
                queryWrapper3_2.last("limit 15");
                List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList = zgsBuildrojectmainparticipantService.list(queryWrapper3_2);
                zgsProjectlibrary.setZgsBuildrojectmainparticipantList(ValidateEncryptEntityUtil.validateDecryptList(zgsBuildrojectmainparticipantList, ValidateEncryptEntityUtil.isDecrypt));
                break;
            case 3:
                //市政公用
                QueryWrapper<ZgsSamplebuildproject> queryWrapper_3 = new QueryWrapper<>();
                queryWrapper_3.eq("projectlibraryguid", zgsProjectlibrary.getId());
                queryWrapper_3.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                queryWrapper_3.ne("isdelete", 1);
                ZgsSamplebuildproject zgsSamplebuildproject = zgsSamplebuildprojectService.getOne(queryWrapper_3);
                zgsProjectlibrary.setAgreeproject(zgsSamplebuildproject.getAgreeproject());
                zgsProjectlibrary.setStatus(zgsSamplebuildproject.getStatus());
                zgsProjectlibrary.setZgsSamplebuildproject(ValidateEncryptEntityUtil.validateDecryptObject(zgsSamplebuildproject, ValidateEncryptEntityUtil.isDecrypt));
                enterpriseguid = zgsSamplebuildproject.getEnterpriseguid();
                buildguid = zgsSamplebuildproject.getId();
                //新增
                if ((zgsSamplebuildproject.getFirstdate() != null || zgsSamplebuildproject.getFinishdate() != null) && zgsSamplebuildproject.getApplydate() != null) {
                    zgsProjectlibrary.setSpLogStatus(1);
                } else {
                    zgsProjectlibrary.setSpLogStatus(0);
                }
                //屏蔽法人设置，先判断是否为专家用户类型，再查询
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                    QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                    zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                    ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                    QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                    zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                    zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                    ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                    if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                        String setValue = zgsExpertset.getSetvalue();
                        if (setValue.contains(",")) {
                            String strValue[] = setValue.split(",");
                            for (String set : strValue) {
                                if (GlobalConstants.expert_set_close1.equals(set)) {
                                    zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                    zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                    zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close4.equals(set)) {
                                }
                            }
                        } else {
                            if (GlobalConstants.expert_set_close1.equals(setValue)) {
                                zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                            }
                        }
                    }
                }
                //专家评审
                zgsProjectlibrary.setZgsSofttechexpertList(zgsProjectlibraryexpertService.getZgsProjectlibraryexpertList(buildguid));
                //
                //项目主要参加人员
                QueryWrapper<ZgsSamplerojectmainparticipant> queryWrapper3_3 = new QueryWrapper<>();
                queryWrapper3_3.eq("enterpriseguid", enterpriseguid);
                queryWrapper3_3.eq("buildguid", buildguid);
                queryWrapper3_3.ne("isdelete", 1);
                queryWrapper3_3.orderByAsc("ordernum");
                queryWrapper3_3.last("limit 15");
                List<ZgsSamplerojectmainparticipant> zgsSamplerojectmainparticipantList = zgsSamplerojectmainparticipantService.list(queryWrapper3_3);
                zgsProjectlibrary.setZgsSamplerojectmainparticipantList(ValidateEncryptEntityUtil.validateDecryptList(zgsSamplerojectmainparticipantList, ValidateEncryptEntityUtil.isDecrypt));
                break;
            case 4:
                //节能建筑
                QueryWrapper<ZgsEnergybuildproject> queryWrapper_4 = new QueryWrapper<>();
                queryWrapper_4.eq("projectlibraryguid", zgsProjectlibrary.getId());
                queryWrapper_4.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                queryWrapper_4.ne("isdelete", 1);
                ZgsEnergybuildproject zgsEnergybuildproject = zgsEnergybuildprojectService.getOne(queryWrapper_4);
                zgsProjectlibrary.setAgreeproject(zgsEnergybuildproject.getAgreeproject());
                zgsProjectlibrary.setStatus(zgsEnergybuildproject.getStatus());
                zgsProjectlibrary.setZgsEnergybuildproject(ValidateEncryptEntityUtil.validateDecryptObject(zgsEnergybuildproject, ValidateEncryptEntityUtil.isDecrypt));
                enterpriseguid = zgsEnergybuildproject.getEnterpriseguid();
                buildguid = zgsEnergybuildproject.getId();
                //新增
                if ((zgsEnergybuildproject.getFirstdate() != null || zgsEnergybuildproject.getFinishdate() != null) && zgsEnergybuildproject.getApplydate() != null) {
                    zgsProjectlibrary.setSpLogStatus(1);
                } else {
                    zgsProjectlibrary.setSpLogStatus(0);
                }
                //屏蔽法人设置，先判断是否为专家用户类型，再查询
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                    QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                    zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                    ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                    QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                    zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                    zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                    ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                    if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                        String setValue = zgsExpertset.getSetvalue();
                        if (setValue.contains(",")) {
                            String strValue[] = setValue.split(",");
                            for (String set : strValue) {
                                if (GlobalConstants.expert_set_close1.equals(set)) {
                                    zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                    zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                    zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close4.equals(set)) {
                                }
                            }
                        } else {
                            if (GlobalConstants.expert_set_close1.equals(setValue)) {
                                zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                            }
                        }
                    }
                }
                //专家评审
                zgsProjectlibrary.setZgsSofttechexpertList(zgsProjectlibraryexpertService.getZgsProjectlibraryexpertList(buildguid));
                //
                //项目主要参加人员
                QueryWrapper<ZgsEnergyprojectmainparticipant> queryWrapper3_4 = new QueryWrapper<>();
                queryWrapper3_4.eq("enterpriseguid", enterpriseguid);
                queryWrapper3_4.eq("buildguid", buildguid);
                queryWrapper3_4.ne("isdelete", 1);
                queryWrapper3_4.orderByAsc("ordernum");
                queryWrapper3_4.last("limit 15");
                List<ZgsEnergyprojectmainparticipant> zgsEnergyprojectmainparticipantList = zgsEnergyprojectmainparticipantService.list(queryWrapper3_4);
                zgsProjectlibrary.setZgsEnergyprojectmainparticipantList(ValidateEncryptEntityUtil.validateDecryptList(zgsEnergyprojectmainparticipantList, ValidateEncryptEntityUtil.isDecrypt));
                //计划进度与安排赋值
                QueryWrapper<ZgsEnergyprojectplanarrange> queryWrapper1_4 = new QueryWrapper<>();
                queryWrapper1_4.eq("enterpriseguid", enterpriseguid);
                queryWrapper1_4.eq("buildguid", buildguid);
                queryWrapper1_4.ne("isdelete", 1);
                List<ZgsEnergyprojectplanarrange> zgsEnergyprojectplanarrangeList = zgsEnergyprojectplanarrangeService.list(queryWrapper1_4);
                zgsProjectlibrary.setZgsEnergyprojectplanarrangeList(ValidateEncryptEntityUtil.validateDecryptList(zgsEnergyprojectplanarrangeList, ValidateEncryptEntityUtil.isDecrypt));
                break;
            case 5:
                //装配式建筑
                QueryWrapper<ZgsAssembleproject> queryWrapper_5 = new QueryWrapper<>();
                queryWrapper_5.eq("projectlibraryguid", zgsProjectlibrary.getId());
                queryWrapper_5.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                queryWrapper_5.ne("isdelete", 1);
                ZgsAssembleproject zgsAssembleproject = zgsAssembleprojectService.getOne(queryWrapper_5);
                zgsProjectlibrary.setAgreeproject(zgsAssembleproject.getAgreeproject());
                zgsProjectlibrary.setStatus(zgsAssembleproject.getStatus());
                zgsProjectlibrary.setZgsAssembleproject(ValidateEncryptEntityUtil.validateDecryptObject(zgsAssembleproject, ValidateEncryptEntityUtil.isDecrypt));
                enterpriseguid = zgsAssembleproject.getEnterpriseguid();
                buildguid = zgsAssembleproject.getId();
                //新增
                if ((zgsAssembleproject.getFirstdate() != null || zgsAssembleproject.getFinishdate() != null) && zgsAssembleproject.getApplydate() != null) {
                    zgsProjectlibrary.setSpLogStatus(1);
                } else {
                    zgsProjectlibrary.setSpLogStatus(0);
                }
                //屏蔽法人设置，先判断是否为专家用户类型，再查询
                if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_2) {
                    QueryWrapper<ZgsExpertinfo> zgsExpertinfoQueryWrapper = new QueryWrapper();
                    zgsExpertinfoQueryWrapper.eq("userid", sysUser.getUnitmemberid());
                    ZgsExpertinfo zgsExpertinfo = zgsExpertinfoService.getOne(zgsExpertinfoQueryWrapper);
                    QueryWrapper<ZgsExpertset> zgsExpertsetQueryWrapper = new QueryWrapper();
                    zgsExpertsetQueryWrapper.eq("expertguid", zgsExpertinfo.getId());
                    zgsExpertsetQueryWrapper.eq("businessguid", buildguid);
                    ZgsExpertset zgsExpertset = zgsExpertsetService.getOne(zgsExpertsetQueryWrapper);
                    if (zgsExpertset != null && StringUtils.isNotEmpty(zgsExpertset.getSetvalue())) {
                        String setValue = zgsExpertset.getSetvalue();
                        if (setValue.contains(",")) {
                            String strValue[] = setValue.split(",");
                            for (String set : strValue) {
                                if (GlobalConstants.expert_set_close1.equals(set)) {
                                    zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close2.equals(set)) {
                                    zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close3.equals(set)) {
                                    zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                                } else if (GlobalConstants.expert_set_close4.equals(set)) {
                                }
                            }
                        } else {
                            if (GlobalConstants.expert_set_close1.equals(setValue)) {
                                zgsProjectlibrary.setApplyunit(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close2.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitprojectleader(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close3.equals(setValue)) {
                                zgsProjectlibrary.setApplyunitphone(GlobalConstants.expert_set_default);
                            } else if (GlobalConstants.expert_set_close4.equals(setValue)) {
                            }
                        }
                    }
                }
                //专家评审
                zgsProjectlibrary.setZgsSofttechexpertList(zgsProjectlibraryexpertService.getZgsProjectlibraryexpertList(buildguid));
                //
                //项目主要参加人员（人员类型：0建设单位负责人，1联合申报单位负责人）
                QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper3_5_0 = new QueryWrapper<>();
                queryWrapper3_5_0.eq("enterpriseguid", enterpriseguid);
                queryWrapper3_5_0.eq("buildguid", buildguid);
                // queryWrapper3_5_0.eq("persontype", "0");
                queryWrapper3_5_0.ne("isdelete", 1);
                queryWrapper3_5_0.orderByAsc("ordernum");
                if (zgsAssembleproject.getCreatedate() != null) {
                    if (GreenUtilSelf.compareToCreateDate(zgsAssembleproject.getCreatedate())) {
                        queryWrapper3_5_0.last("limit 15");
                    }
                }
                List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList0 = zgsAssemprojectmainparticipantService.list(queryWrapper3_5_0);
                zgsProjectlibrary.setZgsAssemprojectmainparticipantList0(ValidateEncryptEntityUtil.validateDecryptList(zgsAssemprojectmainparticipantList0, ValidateEncryptEntityUtil.isEncrypt));
//                QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper3_5_1 = new QueryWrapper<>();
//                queryWrapper3_5_1.eq("enterpriseguid", enterpriseguid);
//                queryWrapper3_5_1.eq("buildguid", buildguid);
//                queryWrapper3_5_1.eq("persontype", "1");
//                queryWrapper3_5_1.ne("isdelete", 1);
//                queryWrapper3_5_1.orderByAsc("ordernum");
//                queryWrapper3_5_1.last("limit 15");
//                List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList1 = zgsAssemprojectmainparticipantService.list(queryWrapper3_5_1);
//                zgsProjectlibrary.setZgsAssemprojectmainparticipantList1(zgsAssemprojectmainparticipantList1);
                //单体建筑
                QueryWrapper<ZgsAssemblesingleproject> queryWrapper1_5 = new QueryWrapper<>();
                queryWrapper1_5.eq("enterpriseguid", enterpriseguid);
                queryWrapper1_5.eq("buildguid", buildguid);
                queryWrapper1_5.ne("isdelete", 1);
                List<ZgsAssemblesingleproject> zgsAssemblesingleprojectList = zgsAssemblesingleprojectService.list(queryWrapper1_5);
                zgsProjectlibrary.setZgsAssemblesingleprojectList(ValidateEncryptEntityUtil.validateDecryptList(zgsAssemblesingleprojectList, ValidateEncryptEntityUtil.isDecrypt));
                if (zgsAssemblesingleprojectList.size() > 0) {
                    zgsProjectlibrary.setZgsAssemblesingleproject(zgsProjectlibrary.getZgsAssemblesingleprojectList().get(0));
                }
                break;
        }

        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(buildguid)) {

            //add  查询验收专家名单  20230616  rxl
            QueryWrapper<ZgsSacceptcertexpert> queryWrapper4 = new QueryWrapper<>();
            queryWrapper4.eq("enterpriseguid", enterpriseguid);
            queryWrapper4.eq("baseguid", buildguid);
            queryWrapper4.eq("project_stage", "sbjd");  // 查询当前项目在申报阶段的专家信息
            queryWrapper4.ne("isdelete", 1);
            queryWrapper4.orderByAsc("ordernum");
            List<ZgsSacceptcertexpert> zgsSacceptcertexpertList = zgsSacceptcertexpertService.list(queryWrapper4);
            zgsProjectlibrary.setZgsSacceptcertexpertList(ValidateEncryptEntityUtil.validateDecryptList(zgsSacceptcertexpertList, ValidateEncryptEntityUtil.isDecrypt));

            //联合申报单位
            QueryWrapper<ZgsBuildjointunit> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("enterpriseguid", enterpriseguid);
            queryWrapper2.eq("buildguid", buildguid);
            queryWrapper2.isNull("tag");
            queryWrapper2.ne("isdelete", 1);
            List<ZgsBuildjointunit> zgsBuildjointunitList = zgsBuildjointunitService.list(queryWrapper2);
            zgsProjectlibrary.setZgsBuildjointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsBuildjointunitList, ValidateEncryptEntityUtil.isDecrypt));
            map.put("buildguid", buildguid);
            List<ZgsMattermaterial> zgsMattermaterialList = zgsMattermaterialService.getZgsMattermaterialList(map);
            if (zgsMattermaterialList != null && zgsMattermaterialList.size() > 0) {
                for (int i = 0; i < zgsMattermaterialList.size(); i++) {
                    QueryWrapper<ZgsAttachappendix> queryWrapper5 = new QueryWrapper<>();
                    queryWrapper5.eq("relationid", zgsMattermaterialList.get(i).getId());
                    queryWrapper5.ne("isdelete", 1);
                    List<ZgsAttachappendix> zgsAttachappendixList = ValidateEncryptEntityUtil.validateDecryptList(zgsAttachappendixService.list(queryWrapper5), ValidateEncryptEntityUtil.isDecrypt);
                    if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
                        for (int j = 0; j < zgsAttachappendixList.size(); j++) {
                            zgsAttachappendixList.get(j).setFileOnlineUrl(viewloadUrl + zgsAttachappendixList.get(j).getAppendixpath());
                        }
                    }
                    zgsMattermaterialList.get(i).setZgsAttachappendixList(zgsAttachappendixList);
                }
                zgsProjectlibrary.setZgsMattermaterialList(zgsMattermaterialList);
            }
            //经费预算
            QueryWrapper<ZgsBuildfundbudget> queryWrapper6 = new QueryWrapper<>();
            queryWrapper6.eq("enterpriseguid", enterpriseguid);
            queryWrapper6.eq("buildguid", buildguid);
            queryWrapper6.ne("isdelete", 1);
            queryWrapper6.orderByAsc("year");
            List<ZgsBuildfundbudget> zgsBuildfundbudgetList = zgsBuildfundbudgetService.list(queryWrapper6);
            zgsProjectlibrary.setZgsBuildfundbudgetList(ValidateEncryptEntityUtil.validateDecryptList(zgsBuildfundbudgetList, ValidateEncryptEntityUtil.isDecrypt));
        }
    }

    /**
     * 导出excel
     *
     * @param
     * @param
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsProjectlibraryListInfo zgsProjectlibraryListInfo,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  @RequestParam(name = "assType") Integer assType,
                                  @RequestParam(name = "accessType", required = false) Integer accessType,
                                  @RequestParam(name = "year", required = false) String year,
                                  @RequestParam(name = "sbArrow", required = false) Integer sbArrow,
                                  @RequestParam(name = "wcArrow", required = false) Integer wcArrow,
                                  @RequestParam(name = "csArrow", required = false) Integer csArrow,
                                  HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsProjectlibraryListInfo, 1, 9999, assType, accessType, year, sbArrow, wcArrow, csArrow, req);
        IPage<ZgsProjectlibraryListInfo> pageList = (IPage<ZgsProjectlibraryListInfo>) result.getResult();
        List<ZgsProjectlibraryListInfo> list = pageList.getRecords();
        List<ZgsProjectlibraryListInfo> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        List<Map<String, String>> listMap = new ArrayList<>();
        if (exportList != null && exportList.size() > 0) {
            for (int i = 0; i < exportList.size(); i++) {
                ZgsProjectlibraryListInfo info = exportList.get(i);
                Map<String, String> lm = new HashMap<>();
                zgsProjectlibraryService.getExportXlsData(info.getId(), info.getBid(), lm);
                lm.put("id", i + 1 + "");
                lm.put("link", info.getApplyunit() + "\n" + info.getApplyunitprojectleader() + "\n" + info.getApplyunitphone());
//                lm.put("projectname", info.getProjectname());
//                lm.put("startenddate", "2022-12-22\n2023-12-22");
//                lm.put("content", "测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试测试");
//                lm.put("link", "测试测试测试测单位\n立德\n18210502471");
//                lm.put("unit", "测试测试测试测单位1\n测试测试测试测单位2");
//                lm.put("bk", "0");
//                lm.put("zc", "100");
                listMap.add(lm);
            }
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateExcelView());
        switch (assType) {
            case 1:
                //绿色建筑示范
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-1申报书汇总表.xlsx" + "");
                break;
            case 2:
                //建筑工程示范
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-2申报书汇总表.xlsx" + "");
                break;
            case 3:
                //市政公用工程
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-3申报书汇总表.xlsx" + "");
                break;
            case 4:
                //建筑节能
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-4申报书汇总表.xlsx" + "");
                break;
            case 5:
                //装配式
                mv.addObject(TemplateWordConstants.URL, wordTemplate + "/0-5申报书汇总表.xlsx" + "");
                break;
        }
//        String title = "示范申报项目";
        //此处设置的filename无效 ,前端会重更新设置一下
//        mv.addObject(NormalExcelConstants.FILE_NAME, title);
//        mv.addObject(NormalExcelConstants.CLASS, ZgsProjectlibraryListInfo.class);
//        ExportParams params = new ExportParams(title + "清单", "导出人:" + sysUser.getRealname(), title);
//        params.setAddIndex(true);
//        mv.addObject(NormalExcelConstants.PARAMS, params);
        mv.addObject(NormalExcelConstants.MAP_LIST, listMap);
        //return super.exportXls(request, zgsProjectlibrary, ZgsProjectlibrary.class, "建筑工程项目库");
        return mv;
    }

    private String getId(ZgsProjectlibraryListInfo item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjectlibrary.class);
    }

    /**
     * 导出WORD
     *
     * @param request
     * @param id
     */
    @AutoLog(value = "建筑工程项目库-id查询导出word")
    @ApiOperation(value = "建筑工程项目库-id查询导出word", notes = "建筑工程项目库-id查询导出word")
    @GetMapping(value = "/exportWord")
    public ModelAndView exportWord(@RequestParam(name = "id", required = true) String id, @RequestParam(name = "assType") Integer assType, HttpServletRequest request) throws Exception {
        //查询实体对象
        ZgsProjectlibrary zgsProjectlibrary = zgsProjectlibraryService.getById(id);
        if (zgsProjectlibrary != null) {
            initProjectSelectById(zgsProjectlibrary, assType);
            zgsProjectlibrary = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectlibrary, ValidateEncryptEntityUtil.isDecrypt);
        }
        ModelAndView mv = new ModelAndView(new JeecgTemplateWordView());
        String templateName = "1绿色建筑示范工程申报书.docx";
        switch (assType) {
            case 1:
                templateName = "1绿色建筑示范工程申报书.docx";
                break;
            case 2:
                templateName = "2建设工程示范工程申报书.docx";
                break;
            case 3:
                templateName = "3市政工程示范工程申报书.docx";
                break;
            case 4:
                templateName = "5建筑节能示范工程申报书.docx";
                break;
            case 5:
                templateName = "4甘肃省装配式建筑示范工程申报书.docx";
                break;
        }

        /**
         * 模板地址，从参数里面获取，
         */
        mv.addObject(TemplateWordConstants.URL, wordTemplate + "/" + templateName);
        Map<String, Object> templateMap = BeanUtil.beanToMap(zgsProjectlibrary);
        /**
         * 模板参数
         */
        mv.addObject(TemplateWordConstants.MAP_DATA, templateMap);
        return mv;
    }


    /**
     * @describe: 示范项目总览分页列表查询
     * @author: renxiaoliang
     * @date: 2023/6/20 15:59
     */
    @AutoLog(value = "示范项目总览-分页列表查询")
    @ApiOperation(value = "示范项目总览-分页列表查询", notes = "示范项目总览-分页列表查询")
    @GetMapping(value = "/queryDemoProjectList")
    public Map<String, Object> queryDemoProjectList(@RequestParam Map<String, Object> params, HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> map = new HashMap<>();
        Result result = new Result();

        Integer pageNo = Integer.parseInt(params.get("pageNo").toString());
        Integer pageSize = Integer.parseInt(params.get("pageSize").toString());
        String fieldStr = "";
        if (StrUtil.isNotBlank(params.get("fieldArray").toString())) {
            fieldStr = params.get("fieldArray").toString();
        }


        String agreeproject = ""; // 是否立项  1 ：不同意立项 ，2：同意立项
        String sfhs = "";  // 是否会审（1是，0否
        String projectName = "";  // 项目名称
        String projectNum = ""; // 项目编号
        String commitmentunit = "";  // 申报单位
        String projectstage = "";  // 项目阶段
        String year = ""; // 年度
        String status = ""; // 状态  4：初审通过   8：形审通过   9：形审退回   14：形审驳回
        String fundType = "";  // 资金类型   1：省科技资金支持   0：省科技资金未支持
        int assType = Integer.parseInt(params.get("assType").toString());  // 示范项目类型   1：绿色建筑示范项目  2：建筑工程示范项目    3：市政工程项目    4：建筑节能示范项目    5：装配式示范项目

        QueryWrapper<ZgsProjectlibrary> queryWrapperOfDemoProject = new QueryWrapper<>();  // 示范类项目查询条件组装
        QueryWrapper<ZgsProjectlibrary> queryWrapperOfAssemblyProject = new QueryWrapper<>();  // 示范类项目查询条件组装(装配式)

        if (ObjectUtil.isNotNull(params.get("agreeproject")) && ObjectUtil.isNotEmpty(params.get("agreeproject"))) {
            agreeproject = params.get("agreeproject").toString();
            if (ObjectUtil.isNotNull(params.get("Status")) && ObjectUtil.isNotEmpty(params.get("Status"))) {
                status = params.get("Status").toString();
                if (!status.equals("1") && !status.equals("3") && !status.equals("4") && !status.equals("13")  // 待审核、初审退回、初审通过、初审驳回
                ) {
                    queryWrapperOfDemoProject.eq("b.agreeproject", agreeproject);
                    queryWrapperOfAssemblyProject.eq("b.agreeproject", agreeproject);
                }
            }
        }

        if (ObjectUtil.isNotNull(params.get("sfhs")) && ObjectUtil.isNotEmpty(params.get("sfhs"))) {
            sfhs = params.get("sfhs").toString();
            queryWrapperOfDemoProject.eq("l.sfhs", sfhs);
            queryWrapperOfAssemblyProject.eq("l.sfhs", sfhs);
        }

        if (ObjectUtil.isNotNull(params.get("projectName")) && ObjectUtil.isNotEmpty(params.get("projectName"))) {
            projectName = params.get("projectName").toString();
            queryWrapperOfDemoProject.like("l.projectname", projectName);
            queryWrapperOfAssemblyProject.like("l.projectname", projectName);
        }

        if (ObjectUtil.isNotNull(params.get("projectNum")) && ObjectUtil.isNotEmpty(params.get("projectNum"))) {
            projectNum = params.get("projectNum").toString();
            queryWrapperOfDemoProject.like("l.projectnum", projectNum);
            queryWrapperOfAssemblyProject.like("l.projectnum", projectNum);
        }


        if (ObjectUtil.isNotNull(params.get("commitmentunit")) && ObjectUtil.isNotEmpty(params.get("commitmentunit"))) {
            commitmentunit = params.get("commitmentunit").toString();
            if (assType == 1 || assType == 2 || assType == 3 || assType == 4) {
                // 查询 绿色建筑示范项目、建筑工程示范项目、市政工程项目、建筑节能示范项目；
                String finalCommitmentunit = commitmentunit;
                queryWrapperOfDemoProject.and(wrapper -> wrapper.eq("l.applyunit", Sm4Util.encryptEcb(finalCommitmentunit)).or().like("l.applyunit", finalCommitmentunit));
            } else if (assType == 5) {
                // 查询  装配式示范项目；
                String finalCommitmentunit1 = commitmentunit;
                queryWrapperOfAssemblyProject.and(wrapper -> wrapper.eq("l.ownerunit", Sm4Util.encryptEcb(finalCommitmentunit1)).or().like("l.ownerunit", finalCommitmentunit1));
            } else if (assType == 6) {
                // 按申报单位查询全部；
                String finalCommitmentunit2 = commitmentunit;
                queryWrapperOfDemoProject.and(wrapper -> wrapper.eq("l.applyunit", Sm4Util.encryptEcb(finalCommitmentunit2)).or().like("l.applyunit", finalCommitmentunit2));
                String finalCommitmentunit3 = commitmentunit;
                queryWrapperOfAssemblyProject.and(wrapper -> wrapper.eq("l.ownerunit", Sm4Util.encryptEcb(finalCommitmentunit3)).or().like("l.ownerunit", finalCommitmentunit3));
            }
        }

        // 示范项目类型 暂不确定，先在条件上加上，后期需要修改再处理
        queryWrapperOfDemoProject.and(wrapper -> wrapper.eq("l.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_2).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_3).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_2));
        queryWrapperOfAssemblyProject.and(wrapper -> wrapper.eq("l.sfxmtype", GlobalConstants.PROJECT_KU_TYPE_2).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_3).or().eq("l.sfxmstatus", GlobalConstants.PROJECT_KU_STATUS_2));


        if (ObjectUtil.isNotNull(params.get("Year")) && ObjectUtil.isNotEmpty(params.get("Year"))) {
            year = params.get("Year").toString();
            queryWrapperOfDemoProject.eq("l.year_num", year);
            queryWrapperOfAssemblyProject.eq("l.year_num", year);

        }

        if (ObjectUtil.isNotNull(params.get("Status")) && ObjectUtil.isNotEmpty(params.get("Status"))) {
            status = params.get("Status").toString();
            if (status.equals("1") || status.equals("3") || status.equals("4") || status.equals("13")  // 待审核、初审退回、初审通过、初审驳回
            ) {
                queryWrapperOfDemoProject.and((wrapper) -> {
                    wrapper.isNull("b.agreeproject").or().eq("b.agreeproject", "'" + "'");
                });
                queryWrapperOfAssemblyProject.and((wrapper) -> {
                    wrapper.isNull("b.agreeproject").or().eq("b.agreeproject", "'" + "'");
                });
            }
            queryWrapperOfDemoProject.eq("l.projectstagestatus", status);
            queryWrapperOfAssemblyProject.eq("l.projectstagestatus", status);
        }

        if (ObjectUtil.isNotNull(params.get("projectstage")) && ObjectUtil.isNotEmpty(params.get("projectstage"))) {
            projectstage = params.get("projectstage").toString();
            queryWrapperOfDemoProject.eq("l.projectstage", projectstage);
            queryWrapperOfAssemblyProject.eq("l.projectstage", projectstage);
        }

        // 暂未使用，得关联其他表
        if (ObjectUtil.isNotNull(params.get("fundType")) && ObjectUtil.isNotEmpty(params.get("fundType"))) {
            fundType = params.get("fundType").toString();
            // 1：省科技经费支持   0：省科技经费未支持
            if (fundType.equals("1")) {
                queryWrapperOfDemoProject.gt("z.provincialfund", 0);
                queryWrapperOfAssemblyProject.gt("z.provincialfund", 0);
            } else {
                queryWrapperOfDemoProject.eq("z.provincialfund", 0);
                queryWrapperOfAssemblyProject.eq("z.provincialfund", 0);
            }
        }

        queryWrapperOfDemoProject.isNotNull("b.status");
        queryWrapperOfDemoProject.ne("l.isdelete", 1);
        queryWrapperOfDemoProject.ne("b.isdelete", 1);
        queryWrapperOfDemoProject.ne("l.projectstagestatus", "0");
        queryWrapperOfDemoProject.orderByDesc("b.firstdate");
        queryWrapperOfDemoProject.orderByDesc("b.applydate");


        queryWrapperOfAssemblyProject.isNotNull("b.status");
        queryWrapperOfAssemblyProject.ne("l.isdelete", 1);
        queryWrapperOfAssemblyProject.ne("l.isdelete", 1);
        queryWrapperOfAssemblyProject.ne("l.projectstagestatus", "0");
        queryWrapperOfAssemblyProject.orderByDesc("b.firstdate");
        queryWrapperOfAssemblyProject.orderByDesc("b.applydate");

        IPage<ZgsProjectlibraryListInfo> pageList = null;
        List<ZgsProjectlibraryListInfo> totalResultList = new ArrayList<>();

        List<ZgsProjectlibraryListInfo> oneResultList = new ArrayList<>();
        List<ZgsProjectlibraryListInfo> twoResultList = new ArrayList<>();
        List<ZgsProjectlibraryListInfo> threeResultList = new ArrayList<>();
        List<ZgsProjectlibraryListInfo> fourResultList = new ArrayList<>();
        List<ZgsProjectlibraryListInfo> fiveResultList = new ArrayList<>();
        // 重新定义新字符串，将组装好的字段重新进行拼接
        String newFieldStr = "";

        // 组装动态查询的字段，根据不同的表进行别名指定
        if (StrUtil.isNotBlank(fieldStr)) {  // 此处判断用作查询导出
            String[] fieldArray = fieldStr.split(",");
            if (fieldArray.length > 0) {
                for (int i = 0; i < fieldArray.length; i++) {
                    String field = fieldArray[i];
                    switch (field) {
                        case "projectname":
                            field = "l.projectname,";
                            newFieldStr += field;
                            break;
                        case "projectnum":
                            field = "l.projectnum,";
                            newFieldStr += field;
                            break;
                        case "demonstratetype":
                            field = "t.item_text as demonstratetype,";
                            newFieldStr += field;
                            break;
                        case "applyunit":
                            field = "l.applyunit,";
                            newFieldStr += field;
                            break;
                        case "completedate":
                            field = "l.completedate,";
                            newFieldStr += field;
                            break;
                        case "applyunitprojectleader":
                            field = "l.applyunitprojectleader,";
                            newFieldStr += field;
                            break;
                        case "projectstage":
                            field = "l.projectstage,";
                            newFieldStr += field;
                            break;
                        case "projectstagestatusDict":
                            field = "d.item_text as projectstagestatusDict,";
                            newFieldStr += field;
                            break;
                        case "sfhs":
                            // field = "l.sfhs,";
                            field = "if(l.sfhs = 1, '是','否') as sfhs, \n";
                            newFieldStr += field;
                            break;
                        case "agreeproject":
                            // field = "if(b.agreeproject = '' or b.agreeproject is null,'',b.agreeproject) agreeproject,";
                            field = "case when  b.agreeproject = 1 then '不同意立项'\n" +
                                    "when  b.agreeproject = 2 then '同意立项'\n" +
                                    "else '' end agreeproject, \n";
                            newFieldStr += field;
                            break;
                        case "auditopinion":
                            field = "b.auditopinion,";
                            newFieldStr += field;
                            break;
                        default:
                            break;
                    }
                }
                if (StrUtil.isNotBlank(newFieldStr)) {
                    newFieldStr = newFieldStr.substring(0, newFieldStr.lastIndexOf(","));
                }
            }
        }


        switch (assType) {
            case 1:
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {  // 此处判断用作查询导出
                    oneResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoOneForExport(newFieldStr, queryWrapperOfDemoProject);
                    map.put("list", oneResultList);
                    result.setResult(map);

                } else {
                    // 对集合数据重新分页处理
                    oneResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoOne(queryWrapperOfDemoProject);
                    if (oneResultList.size() > 0) {
                        map.put("total", oneResultList.size());
                        map.put("list", dealPaging(oneResultList, pageNo, pageSize));
                        map.put("msg", "查询成功");
                        // result.setResult(map);
                    } else {
                        map.put("total", 0);
                        map.put("list", "");
                        map.put("msg", "暂无数据");
                        // result.setResult(map);
                    }
                }

                break;
            case 2:
                // 对集合数据重新分页处理
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {  // 此处判断用作查询导出
                    twoResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoTwoForExport(newFieldStr, queryWrapperOfDemoProject);
                    // result.setResult(twoResultList);
                    map.put("list", twoResultList);
                } else {
                    twoResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoTwo(queryWrapperOfDemoProject);
                    if (twoResultList.size() > 0) {
                        map.put("total", twoResultList.size());
                        map.put("list", dealPaging(twoResultList, pageNo, pageSize));
                        map.put("msg", "查询成功");
                        // result.setResult(map);
                    } else {
                        map.put("total", 0);
                        map.put("list", "");
                        map.put("msg", "暂无数据");
                        // result.setResult(map);
                    }
                }

                break;
            case 3:
                // 对集合数据重新分页处理
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {  // 此处判断用作查询导出
                    threeResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoThreeForExport(newFieldStr, queryWrapperOfDemoProject);
                    // result.setResult(threeResultList);
                    map.put("list", threeResultList);

                } else {
                    threeResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoThree(queryWrapperOfDemoProject);
                    if (threeResultList.size() > 0) {
                        map.put("total", threeResultList.size());
                        map.put("list", dealPaging(threeResultList, pageNo, pageSize));
                        map.put("msg", "查询成功");
                        // result.setResult(map);
                    } else {
                        map.put("total", 0);
                        map.put("list", "");
                        map.put("msg", "暂无数据");
                        // result.setResult(map);
                    }
                }

                break;
            case 4:
                // 对集合数据重新分页处理
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {  // 此处判断用作查询导出
                    fourResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFourForExport(newFieldStr, queryWrapperOfDemoProject);
                    // result.setResult(fourResultList);
                    map.put("list", fourResultList);

                } else {
                    fourResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFour(queryWrapperOfDemoProject);
                    if (fourResultList.size() > 0) {
                        map.put("total", fourResultList.size());
                        map.put("list", dealPaging(fourResultList, pageNo, pageSize));
                        map.put("msg", "查询成功");
                        // result.setResult(map);
                    } else {
                        map.put("total", 0);
                        map.put("list", "");
                        map.put("msg", "暂无数据");
                        // result.setResult(map);
                    }
                }

                break;
            case 5:

                // 对集合数据重新分页处理
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {  // 此处判断用作查询导出
                    fiveResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFiveForExport(newFieldStr, queryWrapperOfAssemblyProject);
                    map.put("list", fiveResultList);
                    // result.setResult(fiveResultList);

                } else {
                    fiveResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFive(queryWrapperOfAssemblyProject);
                    if (fiveResultList.size() > 0) {
                        map.put("total", fiveResultList.size());
                        map.put("list", dealPaging(fiveResultList, pageNo, pageSize));
                        map.put("msg", "查询成功");
                        // result.setResult(map);
                    } else {
                        map.put("total", 0);
                        map.put("list", "");
                        map.put("msg", "暂无数据");
                        // result.setResult(map);
                    }
                }

                break;
            case 6: // 未选择示范项目类型
                if (StrUtil.isNotBlank(fieldStr) && StrUtil.isNotBlank(newFieldStr)) {
                    String fieldForAllData = "";
                    String[] fieldArray = fieldStr.split(",");
                    if (fieldArray.length > 0) {
                        for (int i = 0; i < fieldArray.length; i++) {
                            String field = fieldArray[i];
                            switch (field) {
                                case "projectname":
                                    field = "m.projectname,";
                                    fieldForAllData += field;
                                    break;
                                case "projectnum":
                                    field = "m.projectnum,";
                                    fieldForAllData += field;
                                    break;
                                case "demonstratetype":
                                    field = "m.demonstratetype,";
                                    fieldForAllData += field;
                                    break;
                                case "applyunit":
                                    field = "m.applyunit,";
                                    fieldForAllData += field;
                                    break;
                                case "completedate":
                                    field = "m.completedate,";
                                    fieldForAllData += field;
                                    break;
                                case "applyunitprojectleader":
                                    field = "m.applyunitprojectleader,";
                                    fieldForAllData += field;
                                    break;
                                case "projectstage":
                                    field = "m.projectstage,";
                                    fieldForAllData += field;
                                    break;
                                case "projectstagestatusDict":
                                    field = "m.projectstagestatusDict,";
                                    fieldForAllData += field;
                                    break;
                                case "sfhs":
                                    field = "if(m.sfhs = 1, '是','否') as sfhs, \n";
                                    fieldForAllData += field;
                                    break;
                                case "agreeproject":
                                    // field = "if(m.agreeproject = '' or m.agreeproject is null,'',m.agreeproject) agreeproject,";
                                    field = "case when  m.agreeproject = 1 then '不同意立项'\n" +
                                            "when  m.agreeproject = 2 then '同意立项'\n" +
                                            "else '' end agreeproject, ";
                                    fieldForAllData += field;
                                    break;
                                case "auditopinion":
                                    field = "m.auditopinion,";
                                    fieldForAllData += field;
                                    break;
                                default:
                                    break;
                            }
                        }
                        fieldForAllData = fieldForAllData.substring(0, fieldForAllData.lastIndexOf(","));
                    }

                    totalResultList = zgsProjectlibraryService.selectAllDataForExport(fieldForAllData, queryWrapperOfDemoProject);

                    fiveResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFiveForExport(newFieldStr, queryWrapperOfAssemblyProject);

                } else {
                    totalResultList = zgsProjectlibraryService.selectAllData(queryWrapperOfDemoProject);  // 使用union进行组合查询
                    fiveResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFive(queryWrapperOfAssemblyProject);
                }

//                oneResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoOne(queryWrapperOfDemoProject);
//                twoResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoTwo(queryWrapperOfDemoProject);
//                threeResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoThree(queryWrapperOfDemoProject);
//                fourResultList = zgsProjectlibraryService.listZgsProjectlibraryListInfoFour(queryWrapperOfDemoProject);

//                totalResultList.addAll(oneResultList);
//                totalResultList.addAll(twoResultList);
//                totalResultList.addAll(threeResultList);
//                totalResultList.addAll(fourResultList);
                totalResultList.addAll(fiveResultList);

                // 对集合数据重新分页处理
                if (StrUtil.isNotBlank(fieldStr)) {  // 此处判断用作查询导出
                    map.put("list", totalResultList);

                } else if (totalResultList.size() > 0) {
                    map.put("total", totalResultList.size());
                    map.put("list", dealPaging(totalResultList, pageNo, pageSize));
                    map.put("msg", "查询成功");
                    // result.setResult(map);
                } else {
                    map.put("total", 0);
                    map.put("list", "");
                    map.put("msg", "暂无数据");
                }
                break;

            default:
                break;
        }

        return map;
    }

    /**
     * @describe: 项目总览查询导出
     * @author: renxiaoliang
     * @date: 2024/2/19 16:55
     */
        @RequestMapping(value = "/dataSummaryExport")
        public ModelAndView exportXls (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo,@RequestParam Map < String, Object > params,HttpServletRequest req) {
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

            /*Result<?> result = queryPageList(zgsProjectlibraryListInfo, 1, 9999, 1, null, "", null, null,null, req);
            IPage<ZgsProjectlibraryListInfo> pageList = (IPage<ZgsProjectlibraryListInfo>) result.getResult();
            List<ZgsProjectlibraryListInfo> list = pageList.getRecords();
            List<ZgsProjectlibraryListInfo> exportList = list;*/


            Map<String, Object> result = queryDemoProjectList(params, req);
            List<ZgsProjectlibraryListInfo> exportList = (List<ZgsProjectlibraryListInfo>) result.get("list");


            String title = "示范项目汇总信息";
            ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
            mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
            mv.addObject(NormalExcelConstants.CLASS, ZgsProjectlibraryListInfo.class);
            mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title));
            mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
            if (ObjectUtil.isNotEmpty(params.get("fieldArray"))) {
                mv.addObject(NormalExcelConstants.EXPORT_FIELDS, params.get("fieldArray").toString());
            }
            // mv.addObject(NormalExcelConstants.EXPORT_FIELDS, "projectname,projectnum");


            return mv;

        }
    /*@RequestMapping(value = "/dataSummaryExport")
    public ModelAndView exportXls(HttpServletResponse response, @RequestParam Map<String, Object> params, ZgsProjectlibraryListInfo dto) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Map<String, Object> result = queryDemoProjectList(params);

        String fieldStr = "";
        List<String> headerList = new ArrayList<>(); // 添加表头
        List<String> columnList = new ArrayList<>(); // 添加字段
        if (ObjectUtil.isNotEmpty(params.get("fieldArray"))) {

            fieldStr = params.get("fieldArray").toString();
            String fieldForAllData = "";
            String[] fieldArray = fieldStr.split(",");
            if (fieldArray.length > 0) {
                for (int i = 0; i < fieldArray.length; i++) {
                    String field = fieldArray[i];
                    switch (field) {
                        case "projectname":
                            field = "m.projectname,";
                            fieldForAllData += field;

                            headerList.add("项目名称");
                            columnList.add("projectname");
                            break;
                        case "projectnum":
                            field = "m.projectnum,";
                            fieldForAllData += field;

                            headerList.add("项目编号");
                            columnList.add("projectnum");
                            break;
                        case "demonstratetype":
                            field = "m.demonstratetype,";
                            fieldForAllData += field;

                            headerList.add("示范类别");
                            columnList.add("demonstratetype");
                            break;
                        case "applyunit":
                            field = "m.applyunit,";
                            fieldForAllData += field;

                            headerList.add("申报单位");
                            columnList.add("applyunit");
                            break;
                        case "completedate":
                            field = "m.completedate,";
                            fieldForAllData += field;

                            headerList.add("完成日期");
                            columnList.add("completedate");
                            break;
                        case "applyunitprojectleader":
                            field = "m.applyunitprojectleader,";
                            fieldForAllData += field;

                            headerList.add("负责人");
                            columnList.add("applyunitprojectleader");
                            break;
                        case "projectstage":
                            field = "m.projectstage,";
                            fieldForAllData += field;

                            headerList.add("项目阶段");
                            columnList.add("projectstage");
                            break;
                        case "projectstagestatusDict":
                            field = "m.projectstagestatusDict,";
                            fieldForAllData += field;

                            headerList.add("项目状态");
                            columnList.add("projectstagestatusDict");
                            break;
                        case "sfhs":
                            field = "m.sfhs,";
                            fieldForAllData += field;

                            headerList.add("是否会审");
                            columnList.add("sfhs");
                            break;
                        case "agreeproject":
                            field = "if(m.agreeproject = '' or m.agreeproject is null,'',m.agreeproject) agreeproject,";
                            fieldForAllData += field;

                            headerList.add("是否立项");
                            columnList.add("agreeproject");
                            break;
                        case "auditopinion":
                            field = "m.auditopinion,";
                            fieldForAllData += field;

                            headerList.add("退回原因");
                            columnList.add("auditopinion");
                            break;
                        default:
                            break;
                    }
                }
                fieldForAllData = fieldForAllData.substring(0, fieldForAllData.lastIndexOf(","));
            }
        }

        List<ZgsProjectlibraryListInfo> exportList = (List<ZgsProjectlibraryListInfo>) result.get("list");
        String title = "示范项目任务书";

        myExportXls(response, headerList, columnList, exportList);
        return null;
    }*/

    /*public void myExportXls(HttpServletResponse response, List<String> headers, List<String> columns, List<ZgsProjectlibraryListInfo> exportList) {

        ExcelUtils<ZgsProjectlibraryListInfo> eu = new ExcelUtils<ZgsProjectlibraryListInfo>();
        // ZgsProjectlibraryController<ZgsProjectlibraryListInfo> a = new ZgsProjectlibraryController<ZgsProjectlibraryListInfo>();
        // a.exportExcel(response, "示范项目汇总数据", headers, columns, exportList, "示范项目汇总数据导出.xls", "yyyy-MM-dd HH:mm:ss");//目前这个文件名没有什么用，前端传过来会修改掉
    }*/


    /**
     * @describe: 集合分页
     * @author: renxiaoliang
     * @date: 2023/6/21 14:17
     */
    public List<ZgsProjectlibraryListInfo> dealPaging(List<ZgsProjectlibraryListInfo> f, int pageNo, int dataSize) {
        /*
         * 经过测试发现当pageNo为0或者小于时，也就是第0页时，程序会报错，所以需要处理一下pageNo的值
         *
         * 先进行空值的判断，避免程序出现null异常
         *
         * 当pageNo的值小于等于0时，我们让它的值为1
         */
        //参数的校验
        if (f == null) {//当传入过来的list集合为null时，先进行实例化
            f = new ArrayList<ZgsProjectlibraryListInfo>();
        }
        if ((Object) pageNo == null) {//当传入过来的pageNo为null时，先进行赋值操作
            pageNo = 1;
        }
        if ((Object) dataSize == null) {//当传入过来的dataSize为null时，先进行赋值操作
            dataSize = 1;
        }
        if (pageNo <= 0) {
            pageNo = 1;
        }

        //记录一下数据一共有多少条
        int totalitems = f.size();
        //实例化一个接受分页处理之后的数据
        List<ZgsProjectlibraryListInfo> afterList = new ArrayList<ZgsProjectlibraryListInfo>();
        /*
         * 进行分页处理,采用for循环的方式来进行处理
         *
         * 首先for循环中，i应该从哪里开始:i应该从 (当前是第几页 -1 乘以 条数) 开始
         *
         * 然后for循环应该到哪里结束，也就是i应该小于:判断(开始的索引+显示条数)是不是大于总条数，如果大于就是总条数，如果小于就是(开始的索引+显示条数)
         *
         * 然后让i++
         */

        for (int i = (pageNo - 1) * dataSize;
             i < (((pageNo - 1) * dataSize) + dataSize >
                     totalitems ? totalitems : ((pageNo - 1) * dataSize) + dataSize);
             i++) {
            //然后将数据存入afterList中

            afterList.add(f.get(i));
        }
        //然后将处理后的数据集合进行返回
        return afterList;
    }


    /**
     * @describe: 省厅角色下授权编辑按钮状态修改
     * @author: renxiaoliang
     * @date: 2023/11/15 16:08
     */
    @AutoLog(value = "授权编辑—状态修改")
    @ApiOperation(value = "授权编辑—状态修改", notes = "授权编辑—状态修改")
    @PostMapping(value = "/accreditUpdate")
    public Result<?> accreditUpdate(@RequestBody Map<String, Object> params) {
        Result result = new Result();

        String id = params.get("id").toString();
        String sqbj = params.get("sqbj").toString();  // 授权编辑  1 同意编辑  2 取消编辑
        String updateSave = params.get("updateSave").toString();  // 编辑保存  1 修改未提交  2 修改已提交

        if (StrUtil.isNotBlank(id) && StrUtil.isNotBlank(sqbj) && StrUtil.isNotBlank(updateSave)) {
            ZgsProjectlibrary zgsProjectlibrary = new ZgsProjectlibrary();
            zgsProjectlibrary.setId(id);
            zgsProjectlibrary.setSqbj(sqbj);
            zgsProjectlibrary.setUpdateSave(updateSave);
            if (zgsProjectlibraryService.updateById(zgsProjectlibrary)) {
                result.setMessage("授权编辑成功");
                result.setCode(1);
                result.setSuccess(true);
            } else {
                result.setMessage("授权编辑失败");
                result.setCode(0);
                result.setSuccess(false);
            }
        } else {
            result.setMessage("授权编辑失败");
            result.setCode(0);
            result.setSuccess(false);
        }
        return result;
    }







    /*public void exportExcel(HttpServletResponse response, String title, List<String> headers, List<String> columns, List<ZgsProjectlibraryListInfo> exportList, String filename, String datePattern){
        // 声明一个工作薄
        HSSFWorkbook workbook = new HSSFWorkbook();
        // 生成一个表格
        HSSFSheet sheet = workbook.createSheet(title);
        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((int) 15);

        // 生成一个样式（用于标题）
        HSSFCellStyle style = workbook.createCellStyle();
        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.HSSFColorPredefined.SKY_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        // 生成一个字体
        HSSFFont font = workbook.createFont();
        font.setColor(HSSFColor.HSSFColorPredefined.VIOLET.getIndex());
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        // 把字体应用到当前的样式
        style.setFont(font);

        // 生成并设置另一个样式（用于内容）
        HSSFCellStyle style2 = workbook.createCellStyle();
        style2.setFillForegroundColor(HSSFColor.HSSFColorPredefined.LIGHT_YELLOW.getIndex());
        style2.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style2.setBorderBottom(BorderStyle.THIN);
        style2.setBorderLeft(BorderStyle.THIN);
        style2.setBorderRight(BorderStyle.THIN);
        style2.setBorderTop(BorderStyle.THIN);
        style2.setAlignment(HorizontalAlignment.CENTER);
        style2.setVerticalAlignment(VerticalAlignment.CENTER);
        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBold(true);
        // 把字体应用到当前的样式
        style2.setFont(font2);

        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);
        for (int i = 0; i < headers.size(); i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellStyle(style);
            HSSFRichTextString text = new HSSFRichTextString(headers.get(i));
            cell.setCellValue(text);
        }

        // 遍历集合数据，产生数据行
        Iterator<ZgsProjectlibraryListInfo> it = exportList.iterator();
        int index = 0;
        while (it.hasNext()) {
            index++;
            row = sheet.createRow(index);
            ZgsProjectlibraryListInfo t = it.next();
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
            //Field[] fields = t.getClass().getDeclaredFields();
            //for (int i = 0; i < fields.length; i++) {
            for (int i = 0; i < columns.size(); i++) {
                HSSFCell cell = row.createCell(i);
                cell.setCellStyle(style2);
                //Field field = fields[i];
                //String fieldName = field.getName();
                String fieldName = columns.get(i);
                String getMethodName = "get"
                        + fieldName.substring(0, 1).toUpperCase()
                        + fieldName.substring(1);
                try {
                    Class<? extends Object> tCls = t.getClass();
                    Method getMethod = tCls.getMethod(getMethodName,new Class[] {});
                    Object value = getMethod.invoke(t, new Object[] {});
                    // 判断值的类型后进行强制类型转换
                    String textValue = null;

                    if (value instanceof Boolean) {
                        boolean bValue = (Boolean) value;
                        textValue = "男";
                        if (!bValue) {
                            textValue = "女";
                        }
                    } else if (value instanceof Date) {
                        Date date = (Date) value;
                        SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
                        textValue = sdf.format(date);
                    } else {
                        // 其它数据类型都当作字符串简单处理
                        if(Objects.nonNull(value)) {
                            textValue = value.toString();
                        }
                        else {
                            textValue = "";
                        }

                    }
                    // 如果不是图片数据，就利用正则表达式判断textValue是否全部由数字组成
                    if (textValue != null) {
                        Pattern p = Pattern.compile("^//d+(//.//d+)?$");
                        Matcher matcher = p.matcher(textValue);
                        if (matcher.matches()) {
                            // 是数字当作double处理
                            cell.setCellValue(Double.parseDouble(textValue));
                        } else {
                            HSSFRichTextString richString = new HSSFRichTextString(textValue);
                            HSSFFont font3 = workbook.createFont();
                            //font3.setColor(HSSFColor.BLUE.index);
                            richString.applyFont(font3);
                            cell.setCellValue(richString);
                        }
                    }
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }  finally {
                    // 清理资源
                }
            }
        }
        try {
            //OutputStream out = new FileOutputStream("/opt/upFiles/"+filename);
            //workbook.write(out);
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
            workbook.write(response.getOutputStream());
            //out.close();
            log.info("导出成功");
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/





    }