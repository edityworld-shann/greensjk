package org.jeecg.modules.green.lsfz.controller;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.green.lsfz.entity.ZgsGreendevelopDetail;
import org.jeecg.modules.green.lsfz.service.ZgsGreendevelopDetailService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @describe:绿色发展项目详情信息
 * @author: renxiaoliang
 * @date: 2023/12/14 11:24
 */
@Api(tags = "绿色发展项目详情信息")
@RestController
@RequestMapping("/lsfz/zgsGreenDevelopDetail")
@Slf4j
public class ZgsGreendevelopDetailController extends JeecgController<ZgsGreendevelopDetail, ZgsGreendevelopDetailService> {






}
