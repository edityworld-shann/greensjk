package org.jeecg.modules.green.common.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 项目单位注册信息表
 * @Author: jeecg-boot
 * @Date: 2022-02-01
 * @Version: V1.0
 */
@Api(tags = "项目单位注册信息表")
@RestController
@RequestMapping("/common/zgsProjectunitmember")
@Slf4j
public class ZgsProjectunitmemberController extends JeecgController<ZgsProjectunitmember, IZgsProjectunitmemberService> {
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;

    /**
     * 分页列表查询
     *
     * @param zgsProjectunitmember
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-分页列表查询")
    @ApiOperation(value = "项目单位注册信息表-分页列表查询", notes = "项目单位注册信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsProjectunitmember zgsProjectunitmember,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsProjectunitmember> queryWrapper = QueryGenerator.initQueryWrapper(zgsProjectunitmember, req.getParameterMap());
        Page<ZgsProjectunitmember> page = new Page<ZgsProjectunitmember>(pageNo, pageSize);
        IPage<ZgsProjectunitmember> pageList = zgsProjectunitmemberService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsProjectunitmember
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-添加")
    @ApiOperation(value = "项目单位注册信息表-添加", notes = "项目单位注册信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsProjectunitmember zgsProjectunitmember) {
        zgsProjectunitmemberService.save(zgsProjectunitmember);
        return Result.OK("添加成功！");
    }

    /**
     * 审批新注册用户
     *
     * @param zgsProjectunitmember
     * @return
     */
    @AutoLog(value = "审批新注册用户")
    @ApiOperation(value = "审批新注册用户", notes = "审批新注册用户")
    @PutMapping(value = "/spRegistUser")
    public Result<?> blackUser(@RequestBody ZgsProjectunitmember zgsProjectunitmember) {
        zgsProjectunitmemberService.updateUserRegistStatus(zgsProjectunitmember);
        return Result.OK("审批通过!");
    }

    /**
     * 审批新注册用户-管理员直接审批通过
     *
     * @param zgsProjectunitmember
     * @return
     */
    @AutoLog(value = "审批新注册用户-管理员直接审批通过")
    @ApiOperation(value = "审批新注册用户-管理员直接审批通过", notes = "审批新注册用户-管理员直接审批通过")
    @PostMapping(value = "/spRegistUserByAdmin")
    public Result<?> blackUserByAdmin(@RequestBody ZgsProjectunitmember zgsProjectunitmember) {
        zgsProjectunitmemberService.updateUserRegistStatusByAdmin(zgsProjectunitmember);
        return Result.OK("审批通过!");
    }

    /**
     * 编辑
     *
     * @param zgsProjectunitmember
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-编辑")
    @ApiOperation(value = "项目单位注册信息表-编辑", notes = "项目单位注册信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsProjectunitmember zgsProjectunitmember) {
        zgsProjectunitmemberService.updateById(zgsProjectunitmember);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-通过id删除")
    @ApiOperation(value = "项目单位注册信息表-通过id删除", notes = "项目单位注册信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsProjectunitmemberService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-批量删除")
    @ApiOperation(value = "项目单位注册信息表-批量删除", notes = "项目单位注册信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsProjectunitmemberService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "项目单位注册信息表-通过id查询")
    @ApiOperation(value = "项目单位注册信息表-通过id查询", notes = "项目单位注册信息表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getById(id);
        if (zgsProjectunitmember == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsProjectunitmember);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsProjectunitmember
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsProjectunitmember zgsProjectunitmember) {
        return super.exportXls(request, zgsProjectunitmember, ZgsProjectunitmember.class, "项目单位注册信息表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsProjectunitmember.class);
    }

}
