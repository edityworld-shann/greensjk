package org.jeecg.modules.green.report.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.report.entity.ZgsReportProject;
import org.jeecg.modules.green.report.mapper.ZgsReportProjectMapper;
import org.jeecg.modules.green.report.service.IZgsReportProjectService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * @Description: 绿色建筑和建筑节能示范项目库
 * @Author: jeecg-boot
 * @Date: 2022-08-02
 * @Version: V1.0
 */
@Service
public class ZgsReportProjectServiceImpl extends ServiceImpl<ZgsReportProjectMapper, ZgsReportProject> implements IZgsReportProjectService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;

    @Override
    public int spProject(ZgsReportProject zgsReportProject) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsReportProject != null && StringUtils.isNotEmpty(zgsReportProject.getId())) {
            ZgsReportProject reportProject = new ZgsReportProject();
            reportProject.setId(zgsReportProject.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsReportProject.getId());
            zgsReturnrecord.setReturnreason(zgsReportProject.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE25);
            ZgsReportProject baseInfo = getById(zgsReportProject.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(zgsProjectunitmember.getEnterprisename());
                }
            }
            if (zgsReportProject.getSpStatus() == 1) {
                //通过记录状态==0
                reportProject.setBackreason("");
                reportProject.setApplystate(new BigDecimal(GlobalConstants.apply_state2));
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
            } else {
                reportProject.setBackreason(zgsReportProject.getAuditopinion());
                reportProject.setApplystate(new BigDecimal(GlobalConstants.apply_state3));
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
            }
            zgsReturnrecordService.save(zgsReturnrecord);
            return this.baseMapper.updateById(reportProject);
        }
        return 0;
    }
}
