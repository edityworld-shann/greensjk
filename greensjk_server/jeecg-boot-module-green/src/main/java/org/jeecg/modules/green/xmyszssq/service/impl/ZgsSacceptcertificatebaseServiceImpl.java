package org.jeecg.modules.green.xmyszssq.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsReturnrecord;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsReturnrecordService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.mapper.ZgsSacceptcertificatebaseMapper;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description: 科研项目验收证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsSacceptcertificatebaseServiceImpl extends ServiceImpl<ZgsSacceptcertificatebaseMapper, ZgsSacceptcertificatebase> implements IZgsSacceptcertificatebaseService {

    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;


    @Override
    public int spProject(ZgsSacceptcertificatebase zgsSacceptcertificatebase) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsSacceptcertificatebase != null && StringUtils.isNotEmpty(zgsSacceptcertificatebase.getId())) {
            ZgsSacceptcertificatebase sacceptcertificatebase = new ZgsSacceptcertificatebase();
            sacceptcertificatebase.setId(zgsSacceptcertificatebase.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(zgsSacceptcertificatebase.getId());
            zgsReturnrecord.setReturnreason(zgsSacceptcertificatebase.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE14);
            ZgsSacceptcertificatebase baseInfo = getById(zgsSacceptcertificatebase.getId());
            if (baseInfo != null) {
                zgsReturnrecord.setEnterpriseguid(baseInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(baseInfo.getApplydate());
                zgsReturnrecord.setProjectname(baseInfo.getProjectname());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", baseInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            if (zgsSacceptcertificatebase.getSpStatus() == 1) {
                //通过记录状态==0
                sacceptcertificatebase.setAuditopinion(null);
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSacceptcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsSacceptcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsSacceptcertificatebase.getStatus())) {
                    sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS4);
                    sacceptcertificatebase.setFirstdate(new Date());
                    sacceptcertificatebase.setFirstperson(sysUser.getRealname());
                    sacceptcertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    sacceptcertificatebase.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
                    //初审xmyszssq/zgsExamplecertificatebase
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS2);
                    sacceptcertificatebase.setFinishperson(sysUser.getRealname());
                    sacceptcertificatebase.setFinishdate(new Date());
                    sacceptcertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    // 因成果信息展示只在终审通过后做显示，在系统列表上不查看其状态，因此在每个角色审批下不做状态信息修改，只在科研项目验收证书终审通过时 对应的项目成果信息状态进行更新（通过）  rxl 20230629
                    // 二次修改，验收证书终审通过后且申报单位同意进行成果信息发布，则对该项目在成果简介阶段的状态直接进行更新，更新为"终审通过"状态，否则该项目不能进入成果简介阶段
                    /*if (StringUtils.isNotBlank(baseInfo.getResultShow()) && baseInfo.getResultShow().equals("1")) {
                        QueryWrapper<ZgsPlanresultbase> queryWrapper = new QueryWrapper();
                        queryWrapper.eq("projectlibraryguid",baseInfo.getProjectlibraryguid());
                        ZgsPlanresultbase zgsPlanresultbase = zgsPlanresultbaseService.getOne(queryWrapper);
                        if (zgsPlanresultbase != null) {
                            zgsPlanresultbase.setId(zgsPlanresultbase.getId());
                            zgsPlanresultbase.setStatus(GlobalConstants.SHENHE_STATUS8);  // 设置项目成果信息状态为 形审通过
                            zgsPlanresultbaseService.updateById(zgsPlanresultbase);
                        }
                    }*/
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                sacceptcertificatebase.setAuditopinion(zgsSacceptcertificatebase.getAuditopinion());
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsSacceptcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS5.equals(zgsSacceptcertificatebase.getStatus()) || GlobalConstants.SHENHE_STATUS15.equals(zgsSacceptcertificatebase.getStatus())) {
                    if (zgsSacceptcertificatebase.getSpStatus() == 2) {
                        sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    sacceptcertificatebase.setFirstdate(new Date());
                    sacceptcertificatebase.setFirstperson(sysUser.getRealname());
                    sacceptcertificatebase.setFirstdepartment(sysUser.getEnterprisename());
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    if (zgsSacceptcertificatebase.getSpStatus() == 2) {
                        sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS5);
                    } else {
                        sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS15);
                    }
                    sacceptcertificatebase.setFinishperson(sysUser.getRealname());
                    sacceptcertificatebase.setFinishdate(new Date());
                    sacceptcertificatebase.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //终审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE21);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    if (zgsSacceptcertificatebase.getSpStatus() == 3) {
                        zgsReturnrecord.setReturntype(new BigDecimal(3));
                        sacceptcertificatebase.setStatus(GlobalConstants.SHENHE_STATUS16);
                        //对应申报项目修改isdealoverdue=2且新增项目终止记录
                        zgsPlannedprojectchangeService.updateProjectIsDealOverdue(baseInfo.getProjectlibraryguid(), baseInfo.getEnterpriseguid(), zgsSacceptcertificatebase.getAuditopinion());
                    }
                }
                if (zgsSacceptcertificatebase.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsSacceptcertificatebase.getAuditopinion())) {
                        zgsSacceptcertificatebase.setAuditopinion(" ");
                    }
                    sacceptcertificatebase.setAuditopinion(zgsSacceptcertificatebase.getAuditopinion());
                    sacceptcertificatebase.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else if (zgsSacceptcertificatebase.getSpStatus() == 3) {
                    zgsReturnrecord.setReturntype(new BigDecimal(3));
                } else {
                    //驳回
                    sacceptcertificatebase.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(baseInfo.getProjectlibraryguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_52));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(sacceptcertificatebase, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }


    /**
     * 顺序编号（科研项目验收证书）
     * 实例：甘建科验[2022] 1 号
     *
     * @return
     */
    @Override
    public String number() {
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        String qw = "甘建科验[" + currentYear + "]";
        QueryWrapper<ZgsSacceptcertificatebase> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("projectnumber", qw);
        int count = this.baseMapper.selectCount(queryWrapper);
        return qw + " " + (count + 1) + " 号";
    }
}
