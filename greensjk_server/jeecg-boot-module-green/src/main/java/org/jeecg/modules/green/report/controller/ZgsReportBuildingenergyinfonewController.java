package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.green.report.service.IZgsReportBuildingenergyinfonewdetailService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: zgs_report_buildingenergyinfonew
 * @Author: jeecg-boot
 * @Date:   2022-03-16
 * @Version: V1.0
 */
@Api(tags="建筑节能统计管理（区县）")
@RestController
@RequestMapping("/report/zgsReportBuildingenergyinfonew")
@Slf4j
public class ZgsReportBuildingenergyinfonewController extends JeecgController<ZgsReportBuildingenergyinfonew, IZgsReportBuildingenergyinfonewService> {
	@Autowired
	private IZgsReportBuildingenergyinfonewService zgsReportBuildingenergyinfonewService;

	 @Autowired
	 private IZgsReportBuildingenergyinfonewdetailService zgsReportBuildingenergyinfonewdetailService;
	
	/**
	 * 分页列表查询
	 *
	 * @param zgsReportBuildingenergyinfonew
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（区县）-分页列表查询")
	@ApiOperation(value="建筑节能统计管理（区县）-分页列表查询", notes="建筑节能统计管理（区县）-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ZgsReportBuildingenergyinfonew zgsReportBuildingenergyinfonew,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                   @RequestParam(name = "applyState", required = false) String applyState,
								   HttpServletRequest req) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		QueryWrapper<ZgsReportBuildingenergyinfonew> qW = new QueryWrapper<>();
		if(zgsReportBuildingenergyinfonew!=null){
			if(zgsReportBuildingenergyinfonew.getApplystate()!=null){
				//有问题，BigDecimal applystate 需要判空？
				BigDecimal applystate  = zgsReportBuildingenergyinfonew.getApplystate();
				qW.eq("applystate",applystate);
			}
			Integer userType = sysUser.getLoginUserType();
			//6县区，7市区
			if(userType==6){
				qW.eq("areacode",sysUser.getAreacode());
			}else if(userType==7){
				qW.like("areacode",sysUser.getAreacode());
			}
			if(StringUtils.isNotEmpty(zgsReportBuildingenergyinfonew.getReporttm())){
				String reporttm  = zgsReportBuildingenergyinfonew.getReporttm();
				qW.eq("reporttm",reporttm);
			}
		}
    //  参数列表
    List list = new ArrayList();
    String[] splitArray = null;
    if(applyState != null){
      splitArray = applyState.split(",");
    }
    if (splitArray != null){
      if (splitArray[0] != "" && splitArray[0] != null){
        for (int i = 0;i < splitArray.length;i++){
          list.add(splitArray[i]);
        }
        qW.in("applystate", list);
      }
    }
		qW.ne("isdelete",1);
		qW.orderByDesc("createtime");

		Page<ZgsReportBuildingenergyinfonew> page = new Page<ZgsReportBuildingenergyinfonew>(pageNo, pageSize);
		IPage<ZgsReportBuildingenergyinfonew> pageList = zgsReportBuildingenergyinfonewService.page(page, qW);
		for(int i = 0;i < pageList.getRecords().size();i++){
			QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
			qWdetail.eq("buildingenergyinfonewguid",pageList.getRecords().get(i).getId());
			List<ZgsReportBuildingenergyinfonewdetail> detailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
      pageList.getRecords().get(i).setZgsReportBuildingenergyinfonewdetailList(detailList);
		}
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param zgsReportBuildingenergyinfonew
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（区县）-添加")
	@ApiOperation(value="建筑节能统计管理（区县）-添加", notes="建筑节能统计管理（区县）-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ZgsReportBuildingenergyinfonew zgsReportBuildingenergyinfonew) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		//生成主表ID
		String id = UUID.randomUUID().toString();
		zgsReportBuildingenergyinfonew.setId(id);
		//申报状态  0未上报
		zgsReportBuildingenergyinfonew.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
		//行政区域
    zgsReportBuildingenergyinfonew.setAreacode(sysUser.getAreacode());
    zgsReportBuildingenergyinfonew.setAreaname(sysUser.getAreaname());
    zgsReportBuildingenergyinfonew.setIsdelete(new BigDecimal(0));
    zgsReportBuildingenergyinfonew.setCreatepersonaccount(sysUser.getUsername());
    zgsReportBuildingenergyinfonew.setCreatepersonname(sysUser.getRealname());
    zgsReportBuildingenergyinfonew.setCreatetime(new Date());
    zgsReportBuildingenergyinfonewService.save(zgsReportBuildingenergyinfonew);
		//新增明细数据
		List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfonew.getZgsReportBuildingenergyinfonewdetailList();
		if(zgsReportBuildingenergyinfonewdetailList!=null&&zgsReportBuildingenergyinfonewdetailList.size()>0){
			for(ZgsReportBuildingenergyinfonewdetail dtl:zgsReportBuildingenergyinfonewdetailList){
				//生成明细表ID
				String id2 = UUID.randomUUID().toString();
				dtl.setId(id2);
				//关联主表ID
				dtl.setBuildingenergyinfonewguid(id);
				zgsReportBuildingenergyinfonewdetailService.save(dtl);
			}
		}
		return Result.OK("添加成功！");
	}


	/**
	 *  编辑
	 *
	 * @param zgsReportBuildingenergyinfonew
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（区县）-编辑")
	@ApiOperation(value="建筑节能统计管理（区县）-编辑", notes="建筑节能统计管理（区县）-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ZgsReportBuildingenergyinfonew zgsReportBuildingenergyinfonew) {
		LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
		if (zgsReportBuildingenergyinfonew != null && StringUtils.isNotEmpty(zgsReportBuildingenergyinfonew.getId())) {
			String id = zgsReportBuildingenergyinfonew.getId();
			if (GlobalConstants.apply_state1.equals(zgsReportBuildingenergyinfonew.getApplystate())) {
				//保存并上报
				zgsReportBuildingenergyinfonew.setApplydate(new Date());
			}
			if (GlobalConstants.apply_state3.equals(zgsReportBuildingenergyinfonew.getApplystate())) {
				//退回
				zgsReportBuildingenergyinfonew.setBackrdate(new Date());
			}
			zgsReportBuildingenergyinfonew.setModifypersonaccount(sysUser.getUsername());
			zgsReportBuildingenergyinfonew.setModifypersonname(sysUser.getRealname());
			zgsReportBuildingenergyinfonew.setModifytime(new Date());
			zgsReportBuildingenergyinfonewService.updateById(zgsReportBuildingenergyinfonew);
			//更新明细表数据
			List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfonew.getZgsReportBuildingenergyinfonewdetailList();
			if(zgsReportBuildingenergyinfonewdetailList!=null&&zgsReportBuildingenergyinfonewdetailList.size()>0){
				for(ZgsReportBuildingenergyinfonewdetail dtl:zgsReportBuildingenergyinfonewdetailList){
					if (StringUtils.isNotEmpty(dtl.getId())) {
						//编辑
						zgsReportBuildingenergyinfonewdetailService.updateById(dtl);
					} else {
						//新增//生成明细表ID
						String id2 = UUID.randomUUID().toString();
						dtl.setId(id2);
						//关联主表ID
						dtl.setBuildingenergyinfonewguid(id);
						zgsReportBuildingenergyinfonewdetailService.save(dtl);
					}

				}
			}else{
				//删除明细表数据
				QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
				qWdetail.eq("buildingenergyinfonewguid",id);
				zgsReportBuildingenergyinfonewdetailService.remove(qWdetail);
			}
		}

		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonew-通过id删除")
	@ApiOperation(value="zgs_report_buildingenergyinfonew-通过id删除", notes="zgs_report_buildingenergyinfonew-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		zgsReportBuildingenergyinfonewService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "zgs_report_buildingenergyinfonew-批量删除")
	@ApiOperation(value="zgs_report_buildingenergyinfonew-批量删除", notes="zgs_report_buildingenergyinfonew-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.zgsReportBuildingenergyinfonewService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "建筑节能统计管理（区县）-通过id查询")
	@ApiOperation(value="建筑节能统计管理（区县）-通过id查询", notes="建筑节能统计管理（区县）-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ZgsReportBuildingenergyinfonew zgsReportBuildingenergyinfonew = zgsReportBuildingenergyinfonewService.getById(id);
		if(zgsReportBuildingenergyinfonew==null) {
			return Result.error("未找到对应数据");
		}else{
			QueryWrapper<ZgsReportBuildingenergyinfonewdetail> qWdetail = new QueryWrapper<>();
			qWdetail.eq("buildingenergyinfonewguid",id);
			List<ZgsReportBuildingenergyinfonewdetail> zgsReportBuildingenergyinfonewdetailList = zgsReportBuildingenergyinfonewdetailService.list(qWdetail);
			zgsReportBuildingenergyinfonew.setZgsReportBuildingenergyinfonewdetailList(zgsReportBuildingenergyinfonewdetailList);
		}
		return Result.OK(zgsReportBuildingenergyinfonew);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param zgsReportBuildingenergyinfonew
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportBuildingenergyinfonew zgsReportBuildingenergyinfonew) {
        return super.exportXls(request, zgsReportBuildingenergyinfonew, ZgsReportBuildingenergyinfonew.class, "zgs_report_buildingenergyinfonew");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportBuildingenergyinfonew.class);
    }

}
