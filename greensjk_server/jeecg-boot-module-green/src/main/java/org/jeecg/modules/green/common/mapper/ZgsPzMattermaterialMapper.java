package org.jeecg.modules.green.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsPzMattermaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 配置库-各类项目工程申报材料配置表
 * @Author: jeecg-boot
 * @Date:   2022-02-09
 * @Version: V1.0
 */
public interface ZgsPzMattermaterialMapper extends BaseMapper<ZgsPzMattermaterial> {

}
