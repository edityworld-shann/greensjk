package org.jeecg.modules.green.common.po;

import lombok.Data;

@Data
public class ZgsExpertinfoPo {
    private String itemText;
    private String itemValue;
    private int countUser;
}
