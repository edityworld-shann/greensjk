package org.jeecg.modules.utils;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;




  import lombok.extern.slf4j.Slf4j;
//  import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;
  import org.apache.http.HttpStatus;
  import org.apache.http.NameValuePair;
  import org.apache.http.client.entity.UrlEncodedFormEntity;
  import org.apache.http.client.methods.CloseableHttpResponse;
  import org.apache.http.client.methods.HttpGet;
  import org.apache.http.client.methods.HttpPost;
  import org.apache.http.client.methods.HttpRequestBase;
  import org.apache.http.client.utils.URIBuilder;
  import org.apache.http.entity.ContentType;
  import org.apache.http.entity.StringEntity;
  import org.apache.http.impl.client.CloseableHttpClient;
  import org.apache.http.impl.client.HttpClients;
  import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
  import org.apache.http.message.BasicNameValuePair;
  import org.apache.http.util.EntityUtils;

  import java.io.UnsupportedEncodingException;
  import java.net.URISyntaxException;
  import java.util.ArrayList;
  import java.util.HashMap;
  import java.util.Map;
  import java.util.Objects;

/**
 * HttpClient封装工具类
 *
 * @author CaiZhiGang
 * @Title HttpClientUtils
 * @ProjectName eca.basicservice.common
 * @date 2022/1/18 17:01
 */
public final class HttpClientUtilJK {

  private static final String UTF_8 = "UTF-8";
  private static final String EMPTY_STR = "";
  private static volatile PoolingHttpClientConnectionManager cm;// 连接池配置管理

  private HttpClientUtilJK() {
  }

  /**
   * 配置初始化
   */
  private static synchronized void init() {
    if (cm == null) {
      cm = new PoolingHttpClientConnectionManager();
      cm.setMaxTotal(50);// 整个连接池最大连接数
      cm.setDefaultMaxPerRoute(25);// 每路由最大连接数，默认值是2
    }
  }

  /**
   * 通过连接池获取HttpClient
   */
  private static CloseableHttpClient getHttpClient() {
    // 初始化
    init();
    // 返回httpclient
    return HttpClients.custom().setConnectionManager(cm).build();
  }

  /**
   * get请求
   */
  public static String httpGetRequest(String url) throws Exception {

    return httpGetRequest(url, null, null);
  }

  /**
   * get请求，参数
   */
  public static String httpGetRequest(String url, Map<String, Object> params) throws Exception {

    return httpGetRequest(url, null, params);
  }


  /**
   * get请求，请求头
   */
  public static String httpGetRequest(String url, HashMap<String, Object> headers) throws Exception {

    return httpGetRequest(url, headers, null);
  }


  /**
   * get请求，请求头 + 参数
   */
  public static String httpGetRequest(String url, Map<String, Object> headers, Map<String, Object> params) throws Exception {
    HttpGet httpGet = null;
    try {
      URIBuilder ub = new URIBuilder(url);

      if (!Objects.isNull(params)) {
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
        ub.setParameters(pairs);
      }
      httpGet = new HttpGet(ub.build());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    if (!Objects.isNull(headers)) {
      for (Map.Entry<String, Object> header : headers.entrySet()) {
        httpGet.addHeader(header.getKey(), String.valueOf(header.getValue()));
      }
    }

    return getResultOfString(httpGet);
  }


  /**
   * post请求
   */
  public static String httpPostRequest(String url) throws Exception {
    return httpPostRequest(url, null, null, null);
  }

  /**
   * post请求，请求头
   */
  public static String httpPostRequest(String url, HashMap<String, Object> headers) throws Exception {
    return httpPostRequest(url, headers, null, null);
  }

  /**
   * post请求，json体
   */
  public static String httpPostRequest(String url, String jsonStrBody) throws Exception {
    return httpPostRequest(url, null, null, jsonStrBody);
  }


  /**
   * post请求，formdata参数
   */
  public static String httpPostRequest(String url, Map<String, Object> params) throws Exception {
    return httpPostRequest(url, null, params, null);
  }

  /**
   * post请求，请求头 + json体
   */
  public static String httpPostRequest(String url, HashMap<String, Object> headers, String jsonStrBody) throws Exception {
    return httpPostRequest(url, headers, null, jsonStrBody);
  }


  /**
   * post请求，请求头 + formdata参数
   */
  public static String httpPostRequest(String url, HashMap<String, Object> headers, Map<String, Object> params) throws Exception {
    return httpPostRequest(url, headers, params, null);
  }


  /**
   * post请求汇总方法：请求头，formdata参数，jsonbody参数
   */
  private static String httpPostRequest(String url, Map<String, Object> headers, Map<String, Object> params, String jsonStrBody) throws Exception {
    HttpPost httpPost = new HttpPost(url);

    if (!Objects.isNull(headers)) {
      for (Map.Entry<String, Object> param : headers.entrySet()) {
        httpPost.addHeader(param.getKey(), String.valueOf(param.getValue()));
      }
    }
    if (!Objects.isNull(params)) {
      ArrayList<NameValuePair> pairs = covertParams2NVPS(params);
      try {
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));
      } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
      }
    }
    if (!Objects.isNull(jsonStrBody)) {
      StringEntity se = new StringEntity(jsonStrBody, UTF_8);
      se.setContentType(String.valueOf(ContentType.APPLICATION_JSON));
      httpPost.setEntity(se);
    }
    return getResultOfString(httpPost);
  }


  /**
   * 处理Http请求，获取String结果
   */
  private static String getResultOfString(HttpRequestBase request) throws Exception {
    CloseableHttpClient httpClient = getHttpClient();

    String result = null;

    CloseableHttpResponse response = httpClient.execute(request);
    int statusCode = response.getStatusLine().getStatusCode();
    result = EntityUtils.toString(response.getEntity(), UTF_8);
    // 去除转义字符
    result = StringEscapeUtils.unescapeJava(result);
    if (statusCode != HttpStatus.SC_OK) {
      request.abort();
      throw new Exception("请求异常：" + result);
    }
    response.close();
    //httpClient.close();
    return result;
  }


  /**
   * 转换成http参数对
   */
  private static ArrayList<NameValuePair> covertParams2NVPS(Map<String, Object> params) {
    ArrayList<NameValuePair> pairs = new ArrayList<>();
    for (Map.Entry<String, Object> param : params.entrySet()) {
      pairs.add(new BasicNameValuePair(param.getKey(), String.valueOf(param.getValue())));
    }
    return pairs;
  }



}
