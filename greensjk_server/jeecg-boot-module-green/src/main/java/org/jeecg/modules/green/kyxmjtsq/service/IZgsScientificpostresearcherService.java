package org.jeecg.modules.green.kyxmjtsq.service;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研结题主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface IZgsScientificpostresearcherService extends IService<ZgsScientificpostresearcher> {

}
