package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.druid.util.Utils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompanyService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompletedService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProductioncapacityService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProjectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 装配式建筑-项目库-申报
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Api(tags = "装配式建筑-项目库-申报")
@RestController
@RequestMapping("/report/zgsReportMonthfabrAreaCityProject")
@Slf4j
public class ZgsReportMonthfabrAreaCityProjectController extends JeecgController<ZgsReportMonthfabrAreaCityProject, IZgsReportMonthfabrAreaCityProjectService> {
    @Autowired
    private IZgsReportMonthfabrAreaCityProjectService zgsReportMonthfabrAreaCityProjectService;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;

    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService zgsReportMonthfabrAreaCityCompanyService;

    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService zgsReportMonthfabrAreaCityProductioncapacityService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabrAreaCityProject
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-分页列表查询")
    @ApiOperation(value = "装配式建筑-项目库-申报-分页列表查询", notes = "装配式建筑-项目库-申报-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityProject> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (StringUtils.isNotBlank(zgsReportMonthfabrAreaCityProject.getProjecttype())) {
            queryWrapper.eq("projecttype", zgsReportMonthfabrAreaCityProject.getProjecttype());
        }
        if (zgsReportMonthfabrAreaCityProject != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getProjectname())) {
                queryWrapper.like("projectname", zgsReportMonthfabrAreaCityProject.getProjectname());
            }
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProject.getReporttm());
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProject.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProject.getAreacode());
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityProject.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProject.getAreacode());
                } else {
//          queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
                }
            } else {
//        queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProject.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProject.getAreacode());
//          queryWrapper.eq("applystate", 2);
//          queryWrapper.eq("applystate", GlobalConstants.apply_state2).or().eq("applystate", GlobalConstants.apply_state1);
                    queryWrapper.and(
                            QueryWrapper -> QueryWrapper.eq("applystate", GlobalConstants.apply_state2)
                                    .or().eq("applystate", GlobalConstants.apply_state1)
                    );
                } else {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProject.getAreacode());
                }
            } else {
                queryWrapper.eq("areacode", sysUser.getAreacode());
                //再加上县区已审核通过的
                queryWrapper.or(w1 -> {
                    if (StringUtils.isNotBlank(zgsReportMonthfabrAreaCityProject.getProjecttype())) {
                        w1.eq("projecttype", zgsReportMonthfabrAreaCityProject.getProjecttype());
                    }
                    if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getReporttm())) {
                        w1.eq("reporttm", zgsReportMonthfabrAreaCityProject.getReporttm());
                    }
                    if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getProjectname())) {
                        w1.like("projectname", zgsReportMonthfabrAreaCityProject.getProjectname());
                    }
                    w1.likeRight("areacode", sysUser.getAreacode());
                    w1.and(w2 -> {
                        w2.eq("applystate", GlobalConstants.apply_state2).or().eq("applystate", GlobalConstants.apply_state1);
                        w2.last(" and length(areacode)=6");
                    });
                });
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        queryWrapper.orderByAsc("areacode");
        Page<ZgsReportMonthfabrAreaCityProject> page = new Page<ZgsReportMonthfabrAreaCityProject>(pageNo, pageSize);
        IPage<ZgsReportMonthfabrAreaCityProject> pageList = zgsReportMonthfabrAreaCityProjectService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsReportMonthfabrAreaCityProject
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-添加")
    @ApiOperation(value = "装配式建筑-项目库-申报-添加", notes = "装配式建筑-项目库-申报-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        zgsReportMonthfabrAreaCityProject.setId(UUID.randomUUID().toString());
        zgsReportMonthfabrAreaCityProject.setCreatepersonaccount(sysUser.getUsername());
        zgsReportMonthfabrAreaCityProject.setCreatepersonname(sysUser.getRealname());
        zgsReportMonthfabrAreaCityProject.setCreatetime(new Date());
        zgsReportMonthfabrAreaCityProject.setAreacode(sysUser.getAreacode());
        zgsReportMonthfabrAreaCityProject.setAreaname(sysUser.getAreaname());
        //计算月份、年、季度
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String yearMonth = simpleDateFormat.format(new Date());
        if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProject.getFilltm())) {
            yearMonth = zgsReportMonthfabrAreaCityProject.getFilltm().substring(0, 7);
        }
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        zgsReportMonthfabrAreaCityProject.setFilltm(yearMonth);
        zgsReportMonthfabrAreaCityProject.setQuarter(new BigDecimal(quarter));
        zgsReportMonthfabrAreaCityProject.setYear(new BigDecimal(year));
//      zgsReportMonthfabrAreaCityProject.setProjecttype("2");
        //限制通过之后再次添加本月项目清单
        //添加前先查询是否本月已上报
        QueryWrapper<ZgsReportMonthfabrAreaCityProject> queryWrapper = new QueryWrapper<ZgsReportMonthfabrAreaCityProject>();
        queryWrapper.eq("filltm", yearMonth);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.eq("applystate", GlobalConstants.apply_state2);
        List<ZgsReportMonthfabrAreaCityProject> list = zgsReportMonthfabrAreaCityProjectService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("本月已审批，退回后方可新增！");
        }
        zgsReportMonthfabrAreaCityProjectService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityProject, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }


    /**
     * 补零0️⃣
     * 新建建筑项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "新建建筑项目清单-本月一键上报（县区或市州）", notes = "新建建筑项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/summaryZero")
    public Result<?> summaryZero(@RequestParam(name = "reporttm") String filltm,
                                 @RequestParam(name = "newConstructionArea", defaultValue = "1", required = false) double newConstructionArea,
                                 @RequestParam(name = "completedArea", defaultValue = "1", required = false) double completedArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "ProductioncapacityZeroItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        // ====--====--====--====--====-- 新开工、已竣工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        //   新开工 上报
        //再判断本月是否已上报
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperXkg = new QueryWrapper();
//        wrapperXkg.eq("reporttm", filltm);
//        wrapperXkg.eq("areacode", sysUser.getAreacode());
//        wrapperXkg.ne("applystate", GlobalConstants.apply_state3);
//        wrapperXkg.ne("isdelete", 1);
//        List<ZgsReportMonthfabrAreaCityCompleted> listTotalXkg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperXkg);
//        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperYjg = new QueryWrapper();
//        wrapperYjg.eq("reporttm", filltm);
//        wrapperYjg.eq("areacode", sysUser.getAreacode());
//        wrapperYjg.ne("applystate", GlobalConstants.apply_state3);
//        wrapperYjg.ne("isdelete", 1);
//        List<ZgsReportMonthfabrAreaCityCompleted> listTotalYjg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperYjg);
        QueryWrapper<ZgsReportMonthfabrAreaCityProject> queryWrapperXkg = new QueryWrapper();
        queryWrapperXkg.eq("reporttm", filltm);
        queryWrapperXkg.eq("areacode", sysUser.getAreacode());
        queryWrapperXkg.ne("isdelete", 1);
        List<ZgsReportMonthfabrAreaCityProject> listXkg = zgsReportMonthfabrAreaCityProjectService.list(queryWrapperXkg);
        if (listXkg.size() > 0) {
            return Result.error("本月项目清单删除后，方可进行无数据上报操作！");
        }
        // ====--====--====--====--====-- 新开工、已竣工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapper = new QueryWrapper();
//     wrapper.eq("reporttm", filltm);
//     wrapper.eq("areacode", sysUser.getAreacode());
//     wrapper.ne("applystate", GlobalConstants.apply_state3);
//     wrapper.ne("isdelete", 1);
//     //     wrapper.ne("projecttype", projecttype);
//     List<ZgsReportMonthfabrAreaCityProductioncapacity> listTotal = zgsReportMonthfabrAreaCityProductioncapacityService.list(wrapper);
//     if (listTotal.size() > 0) {
//       return Result.error("本月生产产能数据已上报！");
//     }
        // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        //1、清单 零 添加 处理
//     zgsReportMonthfabrAreaCityCompanyService.addIterm(filltm);     //  生产产能
        zgsReportMonthfabrAreaCityProjectService.addIterm(filltm);     //  新开工、已竣工
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalZero(year, quarter, filltm, "1", newConstructionArea, completedArea);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalZero(year, quarter, filltm, "2", newConstructionArea, completedArea);
//     zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrAreaTotalZero(year, quarter, filltm, null);
        return Result.OK("本月一键上报成功！");
    }


    /**
     * 新建建筑项目清单-本月一键上报（县区或市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑项目清单-本月一键上报（县区或市州）")
    @ApiOperation(value = "新建建筑项目清单-本月一键上报（县区或市州）", notes = "新建建筑项目清单-本月一键上报（县区或市州）")
    @GetMapping(value = "/summary")
    public Result<?> summary(@RequestParam(name = "reporttm") String filltm,
                             @RequestParam(name = "newConstructionArea") double newConstructionArea,
                             @RequestParam(name = "completedArea") double completedArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "ProductioncapacityItem" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
//        String yearMonth = simpleDateFormat.format(new Date());
        String year = filltm.substring(0, 4);
        String month = filltm.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        // ====--====--====--====--====-- 新开工、已竣工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        //   新开工 上报
        QueryWrapper<ZgsReportMonthfabrAreaCityProject> queryWrapperXkg = new QueryWrapper();
        queryWrapperXkg.eq("reporttm", filltm);
        queryWrapperXkg.eq("areacode", sysUser.getAreacode());
        queryWrapperXkg.ne("isdelete", 1);
        List<ZgsReportMonthfabrAreaCityProject> listXkg = zgsReportMonthfabrAreaCityProjectService.list(queryWrapperXkg);
        if (listXkg.size() == 0) {
            return Result.error("请先填写新开工、或已竣工清单！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperXkg = new QueryWrapper();
//     wrapperXkg.eq("reporttm", filltm);
//     wrapperXkg.eq("areacode", sysUser.getAreacode());
//     wrapperXkg.ne("applystate", GlobalConstants.apply_state3);
//     wrapperXkg.ne("isdelete", 1);
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotalXkg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperXkg);
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperYjg = new QueryWrapper();
//     wrapperYjg.eq("reporttm", filltm);
//     wrapperYjg.eq("areacode", sysUser.getAreacode());
//     wrapperYjg.ne("applystate", GlobalConstants.apply_state3);
//     wrapperYjg.ne("isdelete", 1);
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotalYjg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperYjg);
//     if (listTotalXkg.size() > 0&&listTotalYjg.size() > 0) {
//       return Result.error("本月新开工、或已竣工，数据已上报！");
//     }
        // ====--====--====--====--====-- 新开工、已竣工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--

        // 上报去掉 装配式建筑生产产能清单  rxl  20230630
        /*QueryWrapper<ZgsReportMonthfabrAreaCityCompany> queryWrapper = new QueryWrapper();
        queryWrapper.eq("reporttm", filltm);
        queryWrapper.eq("areacode", sysUser.getAreacode());
        queryWrapper.ne("isdelete", 1);
        //     queryWrapper.ne("projecttype", projecttype);
        List<ZgsReportMonthfabrAreaCityCompany> list = zgsReportMonthfabrAreaCityCompanyService.list(queryWrapper);
        if (list.size() == 0) {
            return Result.error("请先填写生产产能清单！");
        }*/
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapper = new QueryWrapper();
//     wrapper.eq("reporttm", filltm);
//     wrapper.eq("areacode", sysUser.getAreacode());
//     wrapper.ne("applystate", GlobalConstants.apply_state3);
//     wrapper.ne("isdelete", 1);
//     //     wrapper.ne("projecttype", projecttype);
//     List<ZgsReportMonthfabrAreaCityProductioncapacity> listTotal = zgsReportMonthfabrAreaCityProductioncapacityService.list(wrapper);
//     if (listTotal.size() > 0) {
//       return Result.error("本月生产产能数据已上报！");
//     }
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotal(year, quarter, filltm, "1", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotal(year, quarter, filltm, "2", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrAreaTotal(year, quarter, filltm, null);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(0, year, quarter, filltm, "1", newConstructionArea, completedArea, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrAreaTotalALL(0, year, quarter, filltm, "2", newConstructionArea, completedArea, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrAreaTotalALL(0, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), GlobalConstants.apply_state1, sysUser);
        // ====--====--====--====--====-- 生产产能 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        return Result.OK("本月一键上报成功！");
    }


    /**
     * 编辑
     *
     * @param zgsReportMonthfabrAreaCityProject
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-编辑")
    @ApiOperation(value = "装配式建筑-项目库-申报-编辑", notes = "装配式建筑-项目库-申报-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject) {
        zgsReportMonthfabrAreaCityProjectService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityProject, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }


    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-通过id删除")
    @ApiOperation(value = "装配式建筑-项目库-申报-通过id删除", notes = "装配式建筑-项目库-申报-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabrAreaCityProject project = zgsReportMonthfabrAreaCityProjectService.getById(id);
        if (project != null) {
            if ("0".equals(project.getApplystate().toString()) || "3".equals(project.getApplystate().toString())) {
                zgsReportMonthfabrAreaCityProjectService.removeById(id);
            } else {
                return Result.error("清单已上报，禁止删除！");
            }
        }
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-批量删除")
    @ApiOperation(value = "装配式建筑-项目库-申报-批量删除", notes = "装配式建筑-项目库-申报-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabrAreaCityProjectService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-项目库-申报-通过id查询")
    @ApiOperation(value = "装配式建筑-项目库-申报-通过id查询", notes = "装配式建筑-项目库-申报-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportMonthfabrAreaCityProjectService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportMonthfabrAreaCityProject == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportMonthfabrAreaCityProject);
    }

    /**
     * 导出excel
     *
     * @param req
     * @param zgsReportMonthfabrAreaCityProject
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(ZgsReportMonthfabrAreaCityProject zgsReportMonthfabrAreaCityProject,
                                  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                  @RequestParam(name = "pageSize", defaultValue = "9999") Integer pageSize,
                                  HttpServletRequest req) {
        String title = "装配式建筑项目清单";
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        Result<?> result = queryPageList(zgsReportMonthfabrAreaCityProject, 1, 9999, req);
        IPage<ZgsReportMonthfabrAreaCityProject> pageList = (IPage<ZgsReportMonthfabrAreaCityProject>) result.getResult();
        for (ZgsReportMonthfabrAreaCityProject zgsR : pageList.getRecords()) {
            //1：保障性住房、2：商品住房、3：公共建筑、4：农村及旅游景观项目、5：其它
            //1：装配式混凝土结构建筑、2：装配式钢结构建筑、3：装配式木结构建筑、4：其它
            if (StringUtils.isNotEmpty(zgsR.getBuildType())) {
                switch (zgsR.getBuildType()) {
                    case "1":
                        zgsR.setBuildType("保障性住房");
                        break;
                    case "2":
                        zgsR.setBuildType("商品住房");
                        break;
                    case "3":
                        zgsR.setBuildType("公共建筑");
                        break;
                    case "4":
                        zgsR.setBuildType("农村及旅游景观项目");
                        break;
                    case "5":
                        zgsR.setBuildType("其它");
                        break;
                }
            }
            if (StringUtils.isNotEmpty(zgsR.getStructureType())) {
                switch (zgsR.getStructureType()) {
                    case "1":
                        zgsR.setStructureType("装配式混凝土结构建筑");
                        break;
                    case "2":
                        zgsR.setStructureType("装配式钢结构建筑");
                        break;
                    case "3":
                        zgsR.setStructureType("装配式木结构建筑");
                        break;
                    case "4":
                        zgsR.setStructureType("其它");
                        break;
                }
            }
        }
        List<ZgsReportMonthfabrAreaCityProject> list = pageList.getRecords();
        List<ZgsReportMonthfabrAreaCityProject> exportList = null;
        String selections = req.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportMonthfabrAreaCityProject.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportMonthfabrAreaCityProject, ZgsReportMonthfabrAreaCityProject.class, "装配式建筑-项目库-申报");
        return mv;
    }

    private String getId(ZgsReportMonthfabrAreaCityProject item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabrAreaCityProject.class);
    }

}
