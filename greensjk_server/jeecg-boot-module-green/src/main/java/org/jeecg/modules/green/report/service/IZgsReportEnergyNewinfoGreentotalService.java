package org.jeecg.modules.green.report.service;

import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.modules.green.report.entity.ZgsReportEnergyNewinfoGreentotal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 绿色建筑汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-19
 * @Version: V1.0
 */
public interface IZgsReportEnergyNewinfoGreentotalService extends IService<ZgsReportEnergyNewinfoGreentotal> {
//    void initEnergyNewinfoGreenTotal(String year, int quarter, String yearMonth);

//    void initEnergyNewinfoGreenTotalCity(String year, int quarter, String yearMonth);

    void initEnergyNewinfoGreenTotalCityAll(String year, int quarter, String yearMonth, String areacode, String areaname, LoginUser sysUser, Integer type, Integer applyState);

    void spProject(String id, Integer spStatus, String backreason);

    ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataProvince(String filltm, String areacode, Integer applystate);

    ZgsReportEnergyNewinfoGreentotal lastGreenTotalDataCity(String filltm, String areacode, Integer applystate);

    void initEnergyNewinfoGreenTotalZero(String year, int quarter, String yearMonth);

    void energyNewinfoGreenTotalAll(int type, String year, int quarter, String yearMonth, String areacode, String areaname, Integer applyState, LoginUser sysUser);

}
