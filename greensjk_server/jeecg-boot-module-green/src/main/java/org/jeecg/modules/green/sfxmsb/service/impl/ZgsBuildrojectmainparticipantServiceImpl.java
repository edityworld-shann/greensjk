package org.jeecg.modules.green.sfxmsb.service.impl;

import org.jeecg.modules.green.sfxmsb.entity.ZgsBuildrojectmainparticipant;
import org.jeecg.modules.green.sfxmsb.mapper.ZgsBuildrojectmainparticipantMapper;
import org.jeecg.modules.green.sfxmsb.service.IZgsBuildrojectmainparticipantService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 建筑工程项目主要参加人员
 * @Author: jeecg-boot
 * @Date:   2022-02-01
 * @Version: V1.0
 */
@Service
public class ZgsBuildrojectmainparticipantServiceImpl extends ServiceImpl<ZgsBuildrojectmainparticipantMapper, ZgsBuildrojectmainparticipant> implements IZgsBuildrojectmainparticipantService {

}
