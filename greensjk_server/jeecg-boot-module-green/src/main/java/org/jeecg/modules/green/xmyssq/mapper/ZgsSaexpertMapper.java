package org.jeecg.modules.green.xmyssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.common.entity.ZgsSofttechexpert;
import org.jeecg.modules.green.xmyssq.entity.ZgsSaexpert;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目验收专家审批记录
 * @Author: jeecg-boot
 * @Date: 2022-03-15
 * @Version: V1.0
 */
public interface ZgsSaexpertMapper extends BaseMapper<ZgsSaexpert> {
    List<ZgsSaexpert> getZgsSaexpertList(@Param("businessguid") String businessguid);
}
