package org.jeecg.modules.green.xmyssq.service.impl;

import org.jeecg.modules.green.xmyssq.entity.ZgsScientificprojectfinish;
import org.jeecg.modules.green.xmyssq.mapper.ZgsScientificprojectfinishMapper;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificprojectfinishService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 项目完成单位情况
 * @Author: jeecg-boot
 * @Date:   2022-02-13
 * @Version: V1.0
 */
@Service
public class ZgsScientificprojectfinishServiceImpl extends ServiceImpl<ZgsScientificprojectfinishMapper, ZgsScientificprojectfinish> implements IZgsScientificprojectfinishService {

}
