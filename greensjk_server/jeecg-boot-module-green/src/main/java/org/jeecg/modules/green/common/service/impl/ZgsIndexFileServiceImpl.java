package org.jeecg.modules.green.common.service.impl;

import org.jeecg.modules.green.common.entity.ZgsIndexFile;
import org.jeecg.modules.green.common.mapper.ZgsIndexFileMapper;
import org.jeecg.modules.green.common.service.IZgsIndexFileService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 登录页文件下载表
 * @Author: jeecg-boot
 * @Date:   2022-03-05
 * @Version: V1.0
 */
@Service
public class ZgsIndexFileServiceImpl extends ServiceImpl<ZgsIndexFileMapper, ZgsIndexFile> implements IZgsIndexFileService {

}
