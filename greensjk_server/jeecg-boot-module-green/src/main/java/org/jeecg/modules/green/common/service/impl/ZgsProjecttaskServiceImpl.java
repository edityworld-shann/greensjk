package org.jeecg.modules.green.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.mapper.ZgsProjecttaskMapper;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertparticipant;
import org.jeecg.modules.utils.QueryWrapperUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @Description: 示范项目任务书
 * @Author: jeecg-boot
 * @Date: 2022-02-11
 * @Version: V1.0
 */
@Service
@Slf4j
public class ZgsProjecttaskServiceImpl extends ServiceImpl<ZgsProjecttaskMapper, ZgsProjecttask> implements IZgsProjecttaskService {

    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;
    @Autowired
    private IZgsBuildjointunitService zgsBuildjointunitService;
    @Autowired
    private IZgsTaskbuildfundbudgetService zgsTaskbuildfundbudgetService;
    @Autowired
    private IZgsBuildfundbudgetService zgsBuildfundbudgetService;
    @Autowired
    private IZgsGreenprojectmainparticipantService zgsGreenprojectmainparticipantService;
    @Autowired
    private IZgsBuildrojectmainparticipantService zgsBuildrojectmainparticipantService;
    @Autowired
    private IZgsSamplerojectmainparticipantService zgsSamplerojectmainparticipantService;
    @Autowired
    private IZgsEnergyprojectmainparticipantService zgsEnergyprojectmainparticipantService;
    @Autowired
    private IZgsAssemprojectmainparticipantService zgsAssemprojectmainparticipantService;
    @Autowired
    private IZgsTaskprojectmainparticipantService zgsTaskprojectmainparticipantService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsPlannedchangeparticipantService zgsPlannedchangeparticipantService;
    @Autowired
    private AsyncService asyncService;
    @Autowired
    private IZgsReturnrecordService zgsReturnrecordService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Page<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfo(Wrapper<ZgsProjecttask> queryWrapper, Integer pageNo, Integer pageSize) {
        Page<ZgsProjectlibraryListInfo> pageMode = new Page<>(pageNo, pageSize);
        Page<ZgsProjectlibraryListInfo> listInfoPage = this.baseMapper.listZgsProjecttaskListInfo(pageMode, queryWrapper);
        listInfoPage.setRecords(ValidateEncryptEntityUtil.validateDecryptList(listInfoPage.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return listInfoPage;
    }

    @Override
    public List<ZgsProjectlibraryListInfo> listZgsProjecttaskListInfoTask(Wrapper<ZgsProjecttask> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.listZgsProjecttaskListInfoTask(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }

    @Override
    public int spProject(ZgsProjecttask zgsProjecttask) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getId())) {
            ZgsProjecttask projecttask = new ZgsProjecttask();
            projecttask.setId(zgsProjecttask.getId());
            ZgsReturnrecord zgsReturnrecord = new ZgsReturnrecord();
            zgsReturnrecord.setId(UUID.randomUUID().toString());
            zgsReturnrecord.setBusinessguid(projecttask.getId());
            zgsReturnrecord.setReturnreason(zgsProjecttask.getAuditopinion());
            zgsReturnrecord.setReturndate(new Date());
            zgsReturnrecord.setCreatepersonname(sysUser.getRealname());
            zgsReturnrecord.setCreatepersonaccount(sysUser.getUsername());
            zgsReturnrecord.setCreatetime(new Date());
            zgsReturnrecord.setReturnperson(sysUser.getRealname());
            zgsReturnrecord.setBusinesstype(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE7);
            ZgsProjecttask taskInfo = getById(zgsProjecttask.getId());
            if (taskInfo != null) {
                zgsReturnrecord.setEnterpriseguid(taskInfo.getEnterpriseguid());
                zgsReturnrecord.setApplydate(taskInfo.getApplydate());
                QueryWrapper<ZgsProjectunitmember> queryWrapperZgs = new QueryWrapper();
                queryWrapperZgs.eq("enterpriseguid", taskInfo.getEnterpriseguid());
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberService.getOne(queryWrapperZgs);
                if (zgsProjectunitmember != null) {
                    zgsReturnrecord.setEnterprisename(Sm4Util.decryptEcb(zgsProjectunitmember.getEnterprisename()));
                }
            }
            ZgsProjectlibrary zgsProjectlibrary = zgsProjectlibraryService.getById(taskInfo.getProjectguid());
            if (zgsProjectlibrary != null) {
                zgsReturnrecord.setProjectname(zgsProjectlibrary.getProjectname());
            }
            if (zgsProjecttask.getSpStatus() == 1) {
                //通过记录状态==0
                projecttask.setAuditopinion(null);
                projecttask.setFundRemark(zgsProjecttask.getFundRemark());
                zgsReturnrecord.setReturntype(new BigDecimal(0));
                //通过
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjecttask.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjecttask.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjecttask.getStatus())) {
                    projecttask.setStatus(GlobalConstants.SHENHE_STATUS4);
                    projecttask.setFirstdate(new Date());
                    projecttask.setFirstperson(sysUser.getRealname());
                    //初审通过后修改上报日期
                    projecttask.setApplydate(new Date());
                    projecttask.setFirstdepartment(sysUser.getEnterprisename());
                    projecttask.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
                    //初审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());
                } else {
                    projecttask.setFundType(initFundType(taskInfo.getProjectguid()));
                    projecttask.setStatus(GlobalConstants.SHENHE_STATUS8);
                    projecttask.setFinishperson(sysUser.getRealname());
                    projecttask.setFinishdate(new Date());
                    projecttask.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    //形审通过异步生成任务书pdf
                    asyncService.initProjectTaskPdfUrl(zgsProjecttask.getId());
                    //形审
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
            } else {
                //驳回 不通过记录状态==1
                projecttask.setAuditopinion(zgsProjecttask.getAuditopinion());
                projecttask.setFundRemark(zgsProjecttask.getFundRemark());
                zgsReturnrecord.setReturntype(new BigDecimal(1));
                if (GlobalConstants.SHENHE_STATUS1.equals(zgsProjecttask.getStatus()) || GlobalConstants.SHENHE_STATUS9.equals(zgsProjecttask.getStatus()) || GlobalConstants.SHENHE_STATUS14.equals(zgsProjecttask.getStatus())) {
                    if (zgsProjecttask.getSpStatus() == 2) {
                        projecttask.setStatus(GlobalConstants.SHENHE_STATUS3);
                    } else {
                        projecttask.setStatus(GlobalConstants.SHENHE_STATUS13);
                    }
                    projecttask.setFirstdate(new Date());
                    projecttask.setFirstperson(sysUser.getRealname());
                    projecttask.setAuditopinion(zgsProjecttask.getAuditopinion());
                    projecttask.setFirstdepartment(sysUser.getEnterprisename());
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE20);
                    zgsReturnrecord.setReturndepartment(sysUser.getEnterprisename());

                } else {
                    if (zgsProjecttask.getSpStatus() == 2) {
                        projecttask.setStatus(GlobalConstants.SHENHE_STATUS9);
                    } else {
                        projecttask.setStatus(GlobalConstants.SHENHE_STATUS14);
                    }
                    projecttask.setFinishperson(sysUser.getRealname());
                    projecttask.setFinishdate(new Date());
                    projecttask.setAuditopinion(zgsProjecttask.getAuditopinion());
                    projecttask.setFinishdepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                    zgsReturnrecord.setReturnstep(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE24);
                    zgsReturnrecord.setReturndepartment(GlobalConstants.TASK_RETURN_BUSINESS_TYPE_VALUE23);
                }
                if (zgsProjecttask.getSpStatus() == 2) {
                    //补正修改
                    if (StringUtils.isEmpty(zgsProjecttask.getAuditopinion())) {
                        zgsProjecttask.setAuditopinion(" ");
                    }
                    projecttask.setAuditopinion(zgsProjecttask.getAuditopinion());
                    projecttask.setReturntype(new BigDecimal(2));
                    zgsReturnrecord.setReturntype(new BigDecimal(2));
                } else {
                    //驳回
                    projecttask.setReturntype(new BigDecimal(1));
                    zgsReturnrecord.setReturntype(new BigDecimal(1));
                }
            }
            zgsReturnrecord.setProjectlibraryguid(taskInfo.getProjectguid());
            zgsReturnrecord.setProjectstage(QueryWrapperUtil.getProjectstage(GlobalConstants.PROJECT_STAGE_2));
            zgsReturnrecordService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReturnrecord, ValidateEncryptEntityUtil.isEncrypt));
            log.info("spProject-------结束");
            return this.baseMapper.updateById(ValidateEncryptEntityUtil.validateEncryptObject(projecttask, ValidateEncryptEntityUtil.isEncrypt));
        }
        return 0;
    }

    @Override
    public int changeBg(ZgsProjecttask zgsProjecttask) {
        if (zgsProjecttask != null && StringUtils.isNotEmpty(zgsProjecttask.getId()) && StringUtils.isNotEmpty(zgsProjecttask.getProjectguid())) {
            QueryWrapper<ZgsPlannedprojectchange> queryWrapper = new QueryWrapper();
            queryWrapper.eq("projectlibraryguid", zgsProjecttask.getProjectguid());
            queryWrapper.ne("isdelete", 1);
            List<ZgsPlannedprojectchange> listBg = zgsPlannedprojectchangeService.list(queryWrapper);
            int size = listBg.size();
            //首次新增变更不需要进行二次变更操作
//            if (size == 0) {
//                return 0;
//            } else {
            ZgsProjecttask projecttask = new ZgsProjecttask();
            projecttask.setBgcount(++size);
            projecttask.setId(zgsProjecttask.getId());
            return this.baseMapper.updateById(projecttask);
//            }
        }
        return 0;
    }

    @Override
    public Object queryByProjectGuid(String projectguid, Integer accessType) {
        ZgsProjecttask zgsProjecttask = getByProjectGuid(projectguid, accessType);
        if (zgsProjecttask != null) {
            return zgsProjecttask;
        } else {
            return zgsSciencetechtaskService.getBySciencetechguid(projectguid, accessType);
        }
    }

    @Override
    public String queryByProjectGuid(String projectguid) {
        QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("projectguid", projectguid);
        ZgsProjecttask zgsProjecttask = this.baseMapper.selectOne(queryWrapper1);
        if (zgsProjecttask != null) {
            return zgsProjecttask.getId();
        } else {
            QueryWrapper<ZgsSciencetechtask> queryWrapper2 = new QueryWrapper<>();
            queryWrapper2.eq("sciencetechguid", projectguid);
            ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper2);
            return zgsSciencetechtask.getId();
        }
    }

    @Override
    public List<ZgsPlannedchangeparticipant> queryTaskparticipantList(String taskguid) {
        String projectguid = null;
        ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getById(taskguid);
        if (zgsSciencetechtask != null) {
            projectguid = zgsSciencetechtask.getSciencetechguid();
        } else {
            ZgsProjecttask zgsProjecttask = getById(taskguid);
            if (zgsProjecttask != null) {
                projectguid = zgsProjecttask.getProjectguid();
            }
        }
        ZgsProjecttask zgsProjecttask = getByProjectGuid(projectguid, null);
        List<ZgsPlannedchangeparticipant> list = null;
        if (zgsProjecttask != null) {
            List<ZgsTaskprojectmainparticipant> zgsTaskparticipantList1 = zgsProjecttask.getZgsTaskparticipantList();
            list = copy(zgsTaskparticipantList1, ZgsPlannedchangeparticipant.class);
        } else {
            List<ZgsTaskscienceparticipant> zgsTaskparticipantList2 = zgsSciencetechtaskService.getBySciencetechguid(projectguid, null).getZgsTaskparticipantList();
            list = copy(zgsTaskparticipantList2, ZgsPlannedchangeparticipant.class);
        }
        return list;
    }

    public <T> List<T> copy(List<?> zgsTaskparticipantList, Class<T> temp) {
        String oldOb = JSON.toJSONString(zgsTaskparticipantList);
        return JSON.parseArray(oldOb, temp);
    }

    @Override
    public ZgsProjecttask getByProjectGuid(String projectguid, Integer accessType) {
        ZgsProjecttask zgsProjecttask = null;
        try {
            ZgsProjectlibrary zgsProjectlibrary = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectlibraryService.getById(projectguid), ValidateEncryptEntityUtil.isDecrypt);
            //示范否则为科技项目
            if (zgsProjectlibrary != null) {
                //查询任务书数据
                QueryWrapper<ZgsProjecttask> queryWrapper = new QueryWrapper();
                queryWrapper.eq("projectguid", projectguid);
                queryWrapper.ne("isdelete", 1);
                zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(this.baseMapper.selectOne(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
                /* if (accessType != null && accessType == 11) {
                    //变更新增选中项目后，判断是否逾期，逾期前一个月可操作变更
                    Date dateYq = zgsProjectlibrary.getCompletedate();
                    //装配式判断下
                    if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                        dateYq = zgsProjectlibrary.getProjectenddate();
                    }
                    //判断是否有延期申请变更
                    QueryWrapper<ZgsPlannedprojectchange> bglog1 = new QueryWrapper();
                    bglog1.and(bg -> {
                        bg.eq("projectlibraryguid", zgsProjectlibrary.getId());
                    });
                    bglog1.eq("status", GlobalConstants.SHENHE_STATUS8);
                    bglog1.ne("isdelete", 1);
                    bglog1.orderByDesc("firstdate");
                    List<ZgsPlannedprojectchange> bgLogList1 = zgsPlannedprojectchangeService.list(bglog1);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail1 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList1 = null;
                    if (bgLogList1.size() > 0) {
                        for (int m = 0; m < bgLogList1.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog11 = new QueryWrapper();
                            bglog11.eq("baseguid", bgLogList1.get(m).getId());
                            bglog11.eq("item_key", 7);
                            bglog11.ne("isdelete", 1);
                            bglog11.orderByDesc("createdate");
                            plannedprojectchangedetailList1 = zgsPlannedprojectchangedetailService.list(bglog11);
                            if (plannedprojectchangedetailList1 != null && plannedprojectchangedetailList1.size() > 0) {
                                zgsPlannedprojectchangedetail1 = plannedprojectchangedetailList1.get(0);
                            }
                        }
                    }
                    if (zgsPlannedprojectchangedetail1 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail1.getChangecontent())) {
                        //获取到该项目变更日期
                        dateYq = dateFormat.parse(zgsPlannedprojectchangedetail1.getChangecontent());
                    }
                    //时间对比=天
                    long dateTemp = (dateYq.getTime() - new Date().getTime()) / (24 * 60 * 60 * 1000);
                    if (dateTemp < 0) {
                        //判断是否人为新增变更次数
                        QueryWrapper<ZgsPlannedprojectchange> qTask = new QueryWrapper();
                        qTask.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        int bg_count = 0;
                        if (zgsProjecttask.getBgcount() != null) {
                            bg_count = zgsProjecttask.getBgcount();
                        }
                        int size_bg = zgsPlannedprojectchangeService.list(qTask).size();
                        if (!(size_bg < bg_count)) {
                            //该项目已逾期，不允许变更
                            zgsProjecttask = new ZgsProjecttask();
                            zgsProjecttask.setIsYuQ(1);
                            return zgsProjecttask;
                        }
                    }
                }*/
                //
                boolean isFlag = false;
                if (accessType != null && accessType == 1) {
                    //新增任务书时的关联查询accessType == 1
                    isFlag = true;
                    zgsProjecttask = new ZgsProjecttask();
                    zgsProjecttask.setProjectguid(projectguid);
                } else {
                    if (zgsProjecttask == null) {
                        return null;
                    }
                }
                if (zgsProjectlibrary.getStartdate() == null) {
                    zgsProjectlibrary.setStartdate(zgsProjectlibrary.getProjectstartdate());
                    zgsProjectlibrary.setCompletedate(zgsProjectlibrary.getProjectenddate());
                }
                //
                zgsProjecttask.setBid(projectguid);
                zgsProjecttask.setProjectname(zgsProjectlibrary.getProjectname());
                zgsProjecttask.setProjectleader(zgsProjectlibrary.getApplyunitprojectleader());
                zgsProjecttask.setCommitmentunit(zgsProjectlibrary.getApplyunit());
                zgsProjecttask.setStartdate(zgsProjectlibrary.getStartdate());
                zgsProjecttask.setEnddate(zgsProjectlibrary.getCompletedate());
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                //费用编辑状态0可编辑，1不可编辑，2明细可编辑
                if (StringUtils.isEmpty(zgsProjectlibrary.getFeeStatus())) {
                    zgsProjecttask.setFeeStatus("1");
                } else {
                    zgsProjecttask.setFeeStatus(zgsProjectlibrary.getFeeStatus());
                }
                //
//            zgsProjecttask.setZgsProjectlibrary(zgsProjectlibrary);
                //再获取工程示范项目
                //绿色建筑示范 GreenBuildType;建筑工程示范	ConstructBuidType;装配式建筑示范 AssemBuidType;建筑节能示范	EnergyBuildType;市政工程示范 SampleBuidType
                String projecttypenum = zgsProjectlibrary.getProjecttypenum();
                String enterpriseguid = zgsProjectlibrary.getEnterpriseguid();
                if (isFlag) {
                    String buildguid = null;
                    ZgsCommonFee zgsCommonFee = new ZgsCommonFee();
                    if (GlobalConstants.GreenBuildType.equals(projecttypenum)) {
                        zgsProjecttask.setDemonstratetype(GlobalConstants.GreenBuildType_VALUE);
                        zgsProjecttask.setDemonstratetypenum(GlobalConstants.GreenBuildType);
                        QueryWrapper<ZgsGreenbuildproject> queryWrapper_1 = new QueryWrapper<>();
                        queryWrapper_1.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        queryWrapper_1.eq("enterpriseguid", enterpriseguid);
                        queryWrapper_1.ne("isdelete", 1);
                        ZgsGreenbuildproject zgsGreenbuildproject = ValidateEncryptEntityUtil.validateDecryptObject(zgsGreenbuildprojectService.getOne(queryWrapper_1), ValidateEncryptEntityUtil.isDecrypt);
                        buildguid = zgsGreenbuildproject.getId();
//                        zgsProjecttask.setProjectnum(zgsGreenbuildproject.getProjectnum());
                        zgsProjecttask.setProjectsummary(zgsGreenbuildproject.getProjectsurvey());
                        zgsProjecttask.setProdemoncontent("示范内容：" + zgsGreenbuildproject.getDemonstratecontent() + "\n\n考核目标：" + zgsGreenbuildproject.getDemonstratetarget());
                        BeanUtils.copyProperties(zgsGreenbuildproject, zgsCommonFee);
                        //绿色项目组成员
                        QueryWrapper<ZgsGreenprojectmainparticipant> queryWrapper3_1 = new QueryWrapper<>();
                        queryWrapper3_1.eq("enterpriseguid", enterpriseguid);
                        queryWrapper3_1.eq("buildguid", buildguid);
                        queryWrapper3_1.ne("isdelete", 1);
                        queryWrapper3_1.orderByAsc("ordernum");
                        queryWrapper3_1.last("limit 15");
                        List<ZgsGreenprojectmainparticipant> zgsGreenprojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsGreenprojectmainparticipantService.list(queryWrapper3_1), ValidateEncryptEntityUtil.isDecrypt);
                        if (zgsGreenprojectmainparticipantList != null && zgsGreenprojectmainparticipantList.size() > 0) {
                            ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = null;
                            ZgsGreenprojectmainparticipant zgsGreenprojectmainparticipant = null;
                            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = new ArrayList<>();
                            for (int i = 0; i < zgsGreenprojectmainparticipantList.size(); i++) {
                                int orderNum = i + 1;
                                zgsGreenprojectmainparticipant = zgsGreenprojectmainparticipantList.get(i);
                                zgsTaskprojectmainparticipant = new ZgsTaskprojectmainparticipant();
                                //姓名
                                zgsTaskprojectmainparticipant.setPersonname(zgsGreenprojectmainparticipant.getPersonname());
                                //职务
                                zgsTaskprojectmainparticipant.setPersonduty(zgsGreenprojectmainparticipant.getPersonduty());
                                //职称
                                zgsTaskprojectmainparticipant.setPersonpost(zgsGreenprojectmainparticipant.getPersonpost());
                                //承担主要工作
                                zgsTaskprojectmainparticipant.setWorkcontent(zgsGreenprojectmainparticipant.getWorkcontent());
                                //文化程度
                                zgsTaskprojectmainparticipant.setStandardOfCulture(zgsGreenprojectmainparticipant.getStandardOfCulture());
                                //单位名称
                                zgsTaskprojectmainparticipant.setUnit(zgsGreenprojectmainparticipant.getApplyunit());
                                //排序号
                                zgsTaskprojectmainparticipant.setOrdernum(new BigDecimal(orderNum));
                                //性别
                                zgsTaskprojectmainparticipant.setSex(zgsGreenprojectmainparticipant.getSex());
                                //专业
                                zgsTaskprojectmainparticipant.setProfessional(zgsGreenprojectmainparticipant.getSpecialty());
                                //身份证号码
                                zgsTaskprojectmainparticipant.setIdcard(zgsGreenprojectmainparticipant.getIdcard());
                                zgsTaskprojectmainparticipantList.add(zgsTaskprojectmainparticipant);
                            }
                            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                        }
                    } else if (GlobalConstants.ConstructBuidType.equals(projecttypenum)) {
                        zgsProjecttask.setDemonstratetype(GlobalConstants.ConstructBuidType_VALUE);
                        zgsProjecttask.setDemonstratetypenum(GlobalConstants.ConstructBuidType);
                        QueryWrapper<ZgsBuildproject> queryWrapper_2 = new QueryWrapper<>();
                        queryWrapper_2.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        queryWrapper_2.eq("enterpriseguid", enterpriseguid);
                        queryWrapper_2.ne("isdelete", 1);
                        ZgsBuildproject zgsBuildproject = ValidateEncryptEntityUtil.validateDecryptObject(zgsBuildprojectService.getOne(queryWrapper_2), ValidateEncryptEntityUtil.isDecrypt);
                        buildguid = zgsBuildproject.getId();
//                        zgsProjecttask.setProjectnum(zgsBuildproject.getProjectnum());
                        zgsProjecttask.setProjectsummary(zgsBuildproject.getProjectsummary());
                        zgsProjecttask.setProdemoncontent(zgsBuildproject.getMaintechcontent());
                        BeanUtils.copyProperties(zgsBuildproject, zgsCommonFee);
                        //建筑工程项目组成员
                        QueryWrapper<ZgsBuildrojectmainparticipant> queryWrapper3_2 = new QueryWrapper<>();
                        queryWrapper3_2.eq("enterpriseguid", enterpriseguid);
                        queryWrapper3_2.eq("buildguid", buildguid);
                        queryWrapper3_2.ne("isdelete", 1);
                        queryWrapper3_2.orderByAsc("ordernum");
                        queryWrapper3_2.last("limit 15");
                        List<ZgsBuildrojectmainparticipant> zgsBuildrojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsBuildrojectmainparticipantService.list(queryWrapper3_2), ValidateEncryptEntityUtil.isDecrypt);
                        if (zgsBuildrojectmainparticipantList != null && zgsBuildrojectmainparticipantList.size() > 0) {
                            ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = null;
                            ZgsBuildrojectmainparticipant zgsBuildrojectmainparticipant = null;
                            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = new ArrayList<>();
                            for (int i = 0; i < zgsBuildrojectmainparticipantList.size(); i++) {
                                int orderNum = i + 1;
                                zgsBuildrojectmainparticipant = zgsBuildrojectmainparticipantList.get(i);
                                zgsTaskprojectmainparticipant = new ZgsTaskprojectmainparticipant();
                                //姓名
                                zgsTaskprojectmainparticipant.setPersonname(zgsBuildrojectmainparticipant.getPersonname());
                                //职务
                                zgsTaskprojectmainparticipant.setPersonduty(zgsBuildrojectmainparticipant.getPersonduty());
                                //职称
                                zgsTaskprojectmainparticipant.setPersonpost(zgsBuildrojectmainparticipant.getPersonpost());
                                //承担主要工作
                                zgsTaskprojectmainparticipant.setWorkcontent(zgsBuildrojectmainparticipant.getWorkcontent());
                                //文化程度
                                zgsTaskprojectmainparticipant.setStandardOfCulture(zgsBuildrojectmainparticipant.getStandardOfCulture());
                                //单位名称
                                zgsTaskprojectmainparticipant.setUnit(zgsBuildrojectmainparticipant.getApplyunit());
                                //排序号
                                zgsTaskprojectmainparticipant.setOrdernum(new BigDecimal(orderNum));
                                //性别
                                zgsTaskprojectmainparticipant.setSex(zgsBuildrojectmainparticipant.getSex());
                                //专业
                                zgsTaskprojectmainparticipant.setProfessional(zgsBuildrojectmainparticipant.getSpecialty());
                                //身份证号码
                                zgsTaskprojectmainparticipant.setIdcard(zgsBuildrojectmainparticipant.getIdcard());
                                zgsTaskprojectmainparticipantList.add(zgsTaskprojectmainparticipant);
                            }
                            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                        }
                    } else if (GlobalConstants.AssemBuidType.equals(projecttypenum)) {
                        zgsProjecttask.setDemonstratetype(GlobalConstants.AssemBuidType_VALUE);
                        zgsProjecttask.setDemonstratetypenum(GlobalConstants.AssemBuidType);
                        zgsProjectlibrary.setStartdate(zgsProjectlibrary.getProjectstartdate());
                        zgsProjectlibrary.setCompletedate(zgsProjectlibrary.getProjectenddate());
                        QueryWrapper<ZgsAssembleproject> queryWrapper_5 = new QueryWrapper<>();
                        queryWrapper_5.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        queryWrapper_5.eq("enterpriseguid", enterpriseguid);
                        queryWrapper_5.ne("isdelete", 1);
                        ZgsAssembleproject zgsAssembleproject = ValidateEncryptEntityUtil.validateDecryptObject(zgsAssembleprojectService.getOne(queryWrapper_5), ValidateEncryptEntityUtil.isDecrypt);
                        buildguid = zgsAssembleproject.getId();
//                        zgsProjecttask.setProjectnum(zgsAssembleproject.getProjectnum());
                        zgsProjecttask.setProjectsummary(zgsAssembleproject.getProjectsurvey());
                        BeanUtils.copyProperties(zgsAssembleproject, zgsCommonFee);
                        //装配式建筑项目组成员
                        QueryWrapper<ZgsAssemprojectmainparticipant> queryWrapper3_5_0 = new QueryWrapper<>();
                        queryWrapper3_5_0.eq("enterpriseguid", enterpriseguid);
                        queryWrapper3_5_0.eq("buildguid", buildguid);
                        queryWrapper3_5_0.ne("isdelete", 1);
                        queryWrapper3_5_0.orderByAsc("persontype,ordernum");
                        queryWrapper3_5_0.last("limit 15");
                        List<ZgsAssemprojectmainparticipant> zgsAssemprojectmainparticipantList0 = ValidateEncryptEntityUtil.validateDecryptList(zgsAssemprojectmainparticipantService.list(queryWrapper3_5_0), ValidateEncryptEntityUtil.isDecrypt);
                        if (zgsAssemprojectmainparticipantList0 != null && zgsAssemprojectmainparticipantList0.size() > 0) {
                            ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = null;
                            ZgsAssemprojectmainparticipant zgsAssemprojectmainparticipant = null;
                            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = new ArrayList<>();
                            for (int i = 0; i < zgsAssemprojectmainparticipantList0.size(); i++) {
                                int orderNum = i + 1;
                                zgsAssemprojectmainparticipant = zgsAssemprojectmainparticipantList0.get(i);
                                zgsTaskprojectmainparticipant = new ZgsTaskprojectmainparticipant();
                                //姓名
                                zgsTaskprojectmainparticipant.setPersonname(zgsAssemprojectmainparticipant.getPersonname());
                                //职务
//                                zgsTaskprojectmainparticipant.setPersonduty(zgsAssemprojectmainparticipant.getPersonduty());
                                //职称
                                zgsTaskprojectmainparticipant.setPersonpost(zgsAssemprojectmainparticipant.getPersonpost());
                                //承担主要工作
                                zgsTaskprojectmainparticipant.setWorkcontent(zgsAssemprojectmainparticipant.getWorkcontent());
                                //文化程度
                                zgsTaskprojectmainparticipant.setStandardOfCulture(zgsAssemprojectmainparticipant.getStandardOfCulture());
                                //单位名称
                                zgsTaskprojectmainparticipant.setUnit(zgsAssemprojectmainparticipant.getApplyunit());
                                //排序号
                                zgsTaskprojectmainparticipant.setOrdernum(new BigDecimal(orderNum));
                                //性别
                                zgsTaskprojectmainparticipant.setSex(zgsAssemprojectmainparticipant.getSex());
                                //专业
                                zgsTaskprojectmainparticipant.setProfessional(zgsAssemprojectmainparticipant.getSpecialty());
                                //身份证号码
                                zgsTaskprojectmainparticipant.setIdcard(zgsAssemprojectmainparticipant.getIdcard());
                                zgsTaskprojectmainparticipantList.add(zgsTaskprojectmainparticipant);
                            }
                            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                        }
                    } else if (GlobalConstants.EnergyBuildType.equals(projecttypenum)) {
                        zgsProjecttask.setDemonstratetype(GlobalConstants.EnergyBuildType_VALUE);
                        zgsProjecttask.setDemonstratetypenum(GlobalConstants.EnergyBuildType);
                        QueryWrapper<ZgsEnergybuildproject> queryWrapper_4 = new QueryWrapper<>();
                        queryWrapper_4.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        queryWrapper_4.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                        queryWrapper_4.ne("isdelete", 1);
                        ZgsEnergybuildproject zgsEnergybuildproject = ValidateEncryptEntityUtil.validateDecryptObject(zgsEnergybuildprojectService.getOne(queryWrapper_4), ValidateEncryptEntityUtil.isDecrypt);
                        buildguid = zgsEnergybuildproject.getId();
//                        zgsProjecttask.setProjectnum(zgsEnergybuildproject.getProjectnum());
                        zgsProjecttask.setProjectsummary(zgsEnergybuildproject.getProjectsurvey());
                        zgsProjecttask.setProdemoncontent("示范内容：" + zgsEnergybuildproject.getDemonstratecontent() + "\n\n考核目标：" + zgsEnergybuildproject.getDemonstratetarget());
                        BeanUtils.copyProperties(zgsEnergybuildproject, zgsCommonFee);
                        //节能建筑项目组成员
                        QueryWrapper<ZgsEnergyprojectmainparticipant> queryWrapper3_4 = new QueryWrapper<>();
                        queryWrapper3_4.eq("enterpriseguid", enterpriseguid);
                        queryWrapper3_4.eq("buildguid", buildguid);
                        queryWrapper3_4.ne("isdelete", 1);
                        queryWrapper3_4.orderByAsc("ordernum");
                        queryWrapper3_4.last("limit 15");
                        List<ZgsEnergyprojectmainparticipant> zgsEnergyprojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsEnergyprojectmainparticipantService.list(queryWrapper3_4), ValidateEncryptEntityUtil.isDecrypt);
                        if (zgsEnergyprojectmainparticipantList != null && zgsEnergyprojectmainparticipantList.size() > 0) {
                            ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = null;
                            ZgsEnergyprojectmainparticipant zgsEnergyprojectmainparticipant = null;
                            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = new ArrayList<>();
                            for (int i = 0; i < zgsEnergyprojectmainparticipantList.size(); i++) {
                                int orderNum = i + 1;
                                zgsEnergyprojectmainparticipant = zgsEnergyprojectmainparticipantList.get(i);
                                zgsTaskprojectmainparticipant = new ZgsTaskprojectmainparticipant();
                                //姓名
                                zgsTaskprojectmainparticipant.setPersonname(zgsEnergyprojectmainparticipant.getPersonname());
                                //职务
                                zgsTaskprojectmainparticipant.setPersonduty(zgsEnergyprojectmainparticipant.getPersonduty());
                                //职称
                                zgsTaskprojectmainparticipant.setPersonpost(zgsEnergyprojectmainparticipant.getPersonpost());
                                //承担主要工作
                                zgsTaskprojectmainparticipant.setWorkcontent(zgsEnergyprojectmainparticipant.getWorkcontent());
                                //文化程度
                                zgsTaskprojectmainparticipant.setStandardOfCulture(zgsEnergyprojectmainparticipant.getStandardOfCulture());
                                //单位名称
                                zgsTaskprojectmainparticipant.setUnit(zgsEnergyprojectmainparticipant.getApplyunit());
                                //排序号
                                zgsTaskprojectmainparticipant.setOrdernum(new BigDecimal(orderNum));
                                //性别
                                zgsTaskprojectmainparticipant.setSex(zgsEnergyprojectmainparticipant.getSex());
                                //专业
                                zgsTaskprojectmainparticipant.setProfessional(zgsEnergyprojectmainparticipant.getSpecialty());
                                //身份证号码
                                zgsTaskprojectmainparticipant.setIdcard(zgsEnergyprojectmainparticipant.getIdcard());
                                zgsTaskprojectmainparticipantList.add(zgsTaskprojectmainparticipant);
                            }
                            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                        }
                    } else if (GlobalConstants.SampleBuidType.equals(projecttypenum)) {
                        zgsProjecttask.setDemonstratetype(GlobalConstants.SampleBuidType_VALUE);
                        zgsProjecttask.setDemonstratetypenum(GlobalConstants.SampleBuidType);
                        QueryWrapper<ZgsSamplebuildproject> queryWrapper_3 = new QueryWrapper<>();
                        queryWrapper_3.eq("projectlibraryguid", zgsProjectlibrary.getId());
                        queryWrapper_3.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                        queryWrapper_3.ne("isdelete", 1);
                        ZgsSamplebuildproject zgsSamplebuildproject = ValidateEncryptEntityUtil.validateDecryptObject(zgsSamplebuildprojectService.getOne(queryWrapper_3), ValidateEncryptEntityUtil.isDecrypt);
                        buildguid = zgsSamplebuildproject.getId();
//                        zgsProjecttask.setProjectnum(zgsSamplebuildproject.getProjectnum());
                        zgsProjecttask.setProjectsummary(zgsSamplebuildproject.getProjectsummary());
                        BeanUtils.copyProperties(zgsSamplebuildproject, zgsCommonFee);
                        //市政工程项目组成员
                        QueryWrapper<ZgsSamplerojectmainparticipant> queryWrapper3_3 = new QueryWrapper<>();
                        queryWrapper3_3.eq("enterpriseguid", enterpriseguid);
                        queryWrapper3_3.eq("buildguid", buildguid);
                        queryWrapper3_3.ne("isdelete", 1);
                        queryWrapper3_3.orderByAsc("ordernum");
                        queryWrapper3_3.last("limit 15");
                        List<ZgsSamplerojectmainparticipant> zgsSamplerojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsSamplerojectmainparticipantService.list(queryWrapper3_3), ValidateEncryptEntityUtil.isDecrypt);
                        if (zgsSamplerojectmainparticipantList != null && zgsSamplerojectmainparticipantList.size() > 0) {
                            ZgsTaskprojectmainparticipant zgsTaskprojectmainparticipant = null;
                            ZgsSamplerojectmainparticipant zgsSamplerojectmainparticipant = null;
                            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = new ArrayList<>();
                            for (int i = 0; i < zgsSamplerojectmainparticipantList.size(); i++) {
                                int orderNum = i + 1;
                                zgsSamplerojectmainparticipant = zgsSamplerojectmainparticipantList.get(i);
                                zgsTaskprojectmainparticipant = new ZgsTaskprojectmainparticipant();
                                //姓名
                                zgsTaskprojectmainparticipant.setPersonname(zgsSamplerojectmainparticipant.getPersonname());
                                //职务
                                zgsTaskprojectmainparticipant.setPersonduty(zgsSamplerojectmainparticipant.getPersonduty());
                                //职称
                                zgsTaskprojectmainparticipant.setPersonpost(zgsSamplerojectmainparticipant.getPersonpost());
                                //承担主要工作
                                zgsTaskprojectmainparticipant.setWorkcontent(zgsSamplerojectmainparticipant.getWorkcontent());
                                //文化程度
                                zgsTaskprojectmainparticipant.setStandardOfCulture(zgsSamplerojectmainparticipant.getStandardOfCulture());
                                //单位名称
                                zgsTaskprojectmainparticipant.setUnit(zgsSamplerojectmainparticipant.getApplyunit());
                                //排序号
                                zgsTaskprojectmainparticipant.setOrdernum(new BigDecimal(orderNum));
                                //性别
                                zgsTaskprojectmainparticipant.setSex(zgsSamplerojectmainparticipant.getSex());
                                //专业
                                zgsTaskprojectmainparticipant.setProfessional(zgsSamplerojectmainparticipant.getSpecialty());
                                //身份证号码
                                zgsTaskprojectmainparticipant.setIdcard(zgsSamplerojectmainparticipant.getIdcard());
                                zgsTaskprojectmainparticipantList.add(zgsTaskprojectmainparticipant);
                            }
                            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                        }
                    }
                    zgsProjecttask.setBuildid(buildguid);
                    zgsProjecttask.setZgsCommonFee(zgsCommonFee);
                    //示范工程联合申报单位
                    QueryWrapper<ZgsBuildjointunit> queryWrapper2 = new QueryWrapper<>();
                    queryWrapper2.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                    queryWrapper2.eq("buildguid", buildguid);
                    queryWrapper2.isNull("tag");
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsBuildjointunit> zgsBuildjointunitList = zgsBuildjointunitService.list(queryWrapper2);
                    zgsProjecttask.setZgsBuildjointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsBuildjointunitList, ValidateEncryptEntityUtil.isDecrypt));
                    //经费预算
                    QueryWrapper<ZgsBuildfundbudget> queryWrapper6 = new QueryWrapper<>();
                    queryWrapper6.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
                    queryWrapper6.eq("buildguid", buildguid);
                    queryWrapper6.ne("isdelete", 1);
                    queryWrapper6.orderByAsc("year");
                    List<ZgsBuildfundbudget> zgsBuildfundbudgetList = zgsBuildfundbudgetService.list(queryWrapper6);
                    ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = new ZgsTaskbuildfundbudget();
                    List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = new ArrayList<>();
                    BigDecimal provincialfund = new BigDecimal(0);
                    BigDecimal bankloans = new BigDecimal(0);
                    BigDecimal unitselffund = new BigDecimal(0);
                    BigDecimal nationalinvest = new BigDecimal(0);
                    BigDecimal totalfund = new BigDecimal(0);
                    if (zgsBuildfundbudgetList != null && zgsBuildfundbudgetList.size() > 0) {
                        ZgsBuildfundbudget zgsBuildfundbudget = null;
                        for (int i = 0; i < zgsBuildfundbudgetList.size(); i++) {
                            //计算累计
                            zgsBuildfundbudget = zgsBuildfundbudgetList.get(i);
                            if (zgsBuildfundbudget.getProvincialfund() != null) {
                                provincialfund = provincialfund.add(zgsBuildfundbudget.getProvincialfund());
                            }
                            if (zgsBuildfundbudget.getBankloans() != null) {
                                bankloans = bankloans.add(zgsBuildfundbudget.getBankloans());
                            }
                            if (zgsBuildfundbudget.getUnitselffund() != null) {
                                unitselffund = unitselffund.add(zgsBuildfundbudget.getUnitselffund());
                            }
                            if (zgsBuildfundbudget.getNationalinvest() != null) {
                                nationalinvest = nationalinvest.add(zgsBuildfundbudget.getNationalinvest());
                            }
                            if (zgsBuildfundbudget.getTotalfund() != null) {
                                totalfund = totalfund.add(zgsBuildfundbudget.getTotalfund());
                            }
                        }
                    }
//                    zgsTaskbuildfundbudget.setProvincialfund(provincialfund);
                    zgsTaskbuildfundbudget.setProvincialfund(zgsProjectlibrary.getProvincialfundtotal());
                    if (StringUtils.isNotEmpty(zgsProjecttask.getFeeStatus())) {
                        if ("1".equals(zgsProjecttask.getFeeStatus()) || "2".equals(zgsProjecttask.getFeeStatus())) {
                            zgsTaskbuildfundbudget.setProvincialfund(new BigDecimal(0));
                        }
                    }
                    // zgsTaskbuildfundbudget.setBankloans(new BigDecimal(0));
                    zgsTaskbuildfundbudget.setBankloans(bankloans);
                    zgsTaskbuildfundbudget.setUnitselffund(unitselffund);
                    // zgsTaskbuildfundbudget.setNationalinvest(new BigDecimal(0));
                    zgsTaskbuildfundbudget.setNationalinvest(nationalinvest);
                    zgsTaskbuildfundbudget.setTotalfund(totalfund);
//                    zgsTaskbuildfundbudget.setProvincialfundtotal(zgsProjectlibrary.getProvincialfundtotal());
                    zgsTaskbuildfundbudgetList.add(zgsTaskbuildfundbudget);
                    zgsProjecttask.setZgsTaskbuildfundbudgetList(zgsTaskbuildfundbudgetList);
                } else {
                    //任务书以下菜单全部信息，如人员、费用、单位都从任务书关联查询
                    //经费预算
                    QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper3 = new QueryWrapper<>();
                    queryWrapper3.eq("taskguid", zgsProjecttask.getId());
                    queryWrapper3.ne("isdelete", 1);
                    queryWrapper3.orderByAsc("year");
                    List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsTaskbuildfundbudgetService.list(queryWrapper3);
                    if (zgsTaskbuildfundbudgetList.size() > 0) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(0);
                        if (zgsTaskbuildfundbudget != null) {
                            if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                                zgsProjecttask.setFundType("1");
                            }
                        }
                    }
                    zgsProjecttask.setZgsTaskbuildfundbudgetList(zgsTaskbuildfundbudgetList);
                    //费用明细
                    String buildguid = null;
                    ZgsCommonFee zgsCommonFee = new ZgsCommonFee();
                    buildguid = initCommonFeeData(projecttypenum, zgsProjectlibrary, buildguid, zgsProjecttask, 0, zgsCommonFee);
                    zgsProjecttask.setZgsCommonFee(zgsCommonFee);
                    //项目组成员
                    QueryWrapper<ZgsTaskprojectmainparticipant> queryWrapper1 = new QueryWrapper<>();
                    queryWrapper1.eq("taskguid", zgsProjecttask.getId());
                    queryWrapper1.ne("isdelete", 1);
                    queryWrapper1.orderByAsc("ordernum");
                    queryWrapper1.last("limit 15");
                    List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsTaskprojectmainparticipantService.list(queryWrapper1), ValidateEncryptEntityUtil.isDecrypt);
                    //先查该项目所有变更记录为下面做筛选条件使用
                    QueryWrapper<ZgsPlannedprojectchange> mqwd = new QueryWrapper();
                    mqwd.eq("projectlibraryguid", zgsProjectlibrary.getId());
                    mqwd.eq("status", GlobalConstants.SHENHE_STATUS8);
                    mqwd.ne("isdelete", 1);
                    List<ZgsPlannedprojectchange> listBg = zgsPlannedprojectchangeService.list(mqwd);
                    if (listBg.size() > 0) {
                        //查变更历史记录（多次变更按照创建日期升序循环遍历）
                        QueryWrapper<ZgsPlannedprojectchangedetail> mqueryWrapper = new QueryWrapper();
                        mqueryWrapper.ne("isdelete", 1);
                        StringBuilder stringBuilder0_1 = new StringBuilder();
                        for (int i = 0; i < listBg.size(); i++) {
                            stringBuilder0_1.append("'");
                            stringBuilder0_1.append(listBg.get(i).getId());
                            stringBuilder0_1.append("'");
                            if (i < listBg.size() - 1) {
                                stringBuilder0_1.append(",");
                            }
                        }
                        mqueryWrapper.inSql("baseguid", stringBuilder0_1.toString());
                        mqueryWrapper.orderByAsc("createdate");
                        List<ZgsPlannedprojectchangedetail> zgsPlannedprojectchangedetailList = zgsPlannedprojectchangedetailService.list(mqueryWrapper);
//                        未使用（0项目名称、1立项编号、2项目承担单位、3任务书编号、9电子邮箱、10邮编）
//                        已使用（4项目责任人、5工作单位、6通信地址、7终止时间、8联系电话、11项目组成员）
                        if (zgsPlannedprojectchangedetailList.size() > 0) {
                            for (ZgsPlannedprojectchangedetail plannedprojectchangedetail : zgsPlannedprojectchangedetailList) {
                                String itemKey = plannedprojectchangedetail.getItemKey();
                                if ("4".equals(itemKey)) {
                                    //项目负责人
                                    zgsProjecttask.setProjectleader(plannedprojectchangedetail.getChangecontent());
                                } else if ("5".equals(itemKey)) {
                                    //工作单位
                                    zgsProjecttask.setCommitmentunit(plannedprojectchangedetail.getChangecontent());
                                } else if ("6".equals(itemKey)) {
                                    //通信地址
                                    zgsProjecttask.setUnitaddress(plannedprojectchangedetail.getChangecontent());
                                } else if ("7".equals(itemKey)) {
                                    //终止时间
                                    try {
                                        zgsProjecttask.setEnddate(DateUtils.date_sdf.get().parse(plannedprojectchangedetail.getChangecontent()));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                } else if ("8".equals(itemKey)) {
                                    //联系电话
                                    zgsProjecttask.setHeadmobile(plannedprojectchangedetail.getChangecontent());
                                }
                            }
                        }
                    }
                    //
                    zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
                    List<ZgsPlannedchangeparticipant> pants = null;
                    //人员问题单独再针对每个菜单对应的实体类转换一下，否则初次新增时因对象不同，不能回显
                    //判断是否有项目组成员变更，如果有从变更查出替代
                    if (accessType != null && zgsTaskprojectmainparticipantList.size() > 0) {
                        if (StringUtils.isNotEmpty(zgsProjecttask.getId()) && listBg.size() > 0) {
                            QueryWrapper<ZgsPlannedchangeparticipant> xmzcyBg = new QueryWrapper();
                            xmzcyBg.eq("taskguid", zgsProjecttask.getId());
                            xmzcyBg.orderByAsc("ordernum");
                            pants = ValidateEncryptEntityUtil.validateDecryptList(zgsPlannedchangeparticipantService.list(xmzcyBg), ValidateEncryptEntityUtil.isDecrypt);
                            if (pants.size() > 0) {
                                zgsTaskprojectmainparticipantList = copy(pants, ZgsTaskprojectmainparticipant.class);
                            }
                        }
                        //accessType=1(任务书新增)、accessType=2(项目验收管理-科研项目验收申请)、accessType=3(项目验收管理-科研项目结题申请)
                        //accessType=4(项目验收结题证书-科研项目验收证书人员)、accessType=5(项目验收结题证书-示范项目验收证书人员)、accessType=6(项目验收结题证书-科研项目结题证书人员)
                        switch (accessType) {
                            case 5:
                                //示范项目验收证书人员
                                List<ZgsExamplecertparticipant> zgsExamplecertparticipantList = new ArrayList<>();
                                ZgsExamplecertparticipant searcher5 = null;
                                for (ZgsTaskprojectmainparticipant pant : zgsTaskprojectmainparticipantList) {
                                    searcher5 = new ZgsExamplecertparticipant();
                                    searcher5.setName(pant.getPersonname());
                                    searcher5.setSex(pant.getSex());
                                    searcher5.setTechnicaltitle(pant.getPersonpost());
                                    searcher5.setWorkunit(pant.getUnit());
                                    searcher5.setContribution(pant.getWorkcontent());
                                    searcher5.setOrdernum(pant.getOrdernum());
                                    searcher5.setIdcard(pant.getIdcard());
                                    searcher5.setSpecialty(pant.getProfessional());
                                    searcher5.setEducation(pant.getStandardOfCulture());
                                    zgsExamplecertparticipantList.add(searcher5);
                                }
                                zgsProjecttask.setZgsExamplecertparticipantList(zgsExamplecertparticipantList);
                                break;
                        }
                    }
                }
                zgsProjecttask.setZgsProjectlibrary(zgsProjectlibrary);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return zgsProjecttask;
    }

    @Override
    public String initFundType(String baseguid) {
        String fundType = "0";
        if (StringUtils.isNotEmpty(baseguid)) {
            QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper();
            queryWrapper1.eq("projectguid", baseguid);
            queryWrapper1.ne("isdelete", 1);
            ZgsProjecttask zgsProjecttask = this.baseMapper.selectOne(queryWrapper1);
            //示范任务书
            if (zgsProjecttask != null) {
                //任务书以下菜单全部信息，如人员、费用、单位都从任务书关联查询
                //经费预算
                QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper3 = new QueryWrapper<>();
                queryWrapper3.eq("taskguid", zgsProjecttask.getId());
                queryWrapper3.ne("isdelete", 1);
                queryWrapper3.orderByAsc("year");
                List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsTaskbuildfundbudgetService.list(queryWrapper3);
                if (zgsTaskbuildfundbudgetList.size() > 0) {
                    ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(0);
                    if (zgsTaskbuildfundbudget != null) {
                        if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                            fundType = "1";
                        }
                    }
                }
            } else {
                //科技任务书
                QueryWrapper<ZgsSciencetechtask> queryWrapper2 = new QueryWrapper();
                queryWrapper2.eq("sciencetechguid", baseguid);
                queryWrapper2.ne("isdelete", 1);
                ZgsSciencetechtask zgsSciencetechtask = zgsSciencetechtaskService.getOne(queryWrapper2);
                if (zgsSciencetechtask != null) {
                    QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper4 = new QueryWrapper<>();
                    queryWrapper4.eq("taskguid", zgsSciencetechtask.getId());
                    queryWrapper4.ne("isdelete", 1);
                    queryWrapper4.orderByAsc("year");
                    List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = zgsTaskbuildfundbudgetService.list(queryWrapper4);
                    if (zgsTaskbuildfundbudgetList.size() > 0) {
                        ZgsTaskbuildfundbudget zgsTaskbuildfundbudget = zgsTaskbuildfundbudgetList.get(0);
                        if (zgsTaskbuildfundbudget != null) {
                            if (zgsTaskbuildfundbudget.getProvincialfund().compareTo(new BigDecimal(0)) >= 0) {
                                fundType = "1";
                            }
                        }
                    }
                }
            }
        }
        return fundType;
    }

    @Override
    public void initProjectSelectById(ZgsProjecttask zgsProjecttask) {
        String taskid = zgsProjecttask.getId();
        String enterpriseguid = zgsProjecttask.getEnterpriseguid();
        String projectguid = zgsProjecttask.getProjectguid();
        if (StringUtils.isNotEmpty(enterpriseguid) && StringUtils.isNotEmpty(projectguid)) {
            //项目组成员
            QueryWrapper<ZgsTaskprojectmainparticipant> queryWrapper1 = new QueryWrapper<>();
            queryWrapper1.eq("taskguid", taskid);
            queryWrapper1.ne("isdelete", 1);
            queryWrapper1.orderByAsc("ordernum");
            queryWrapper1.last("limit 15");
            List<ZgsTaskprojectmainparticipant> zgsTaskprojectmainparticipantList = ValidateEncryptEntityUtil.validateDecryptList(zgsTaskprojectmainparticipantService.list(queryWrapper1), ValidateEncryptEntityUtil.isDecrypt);
            for (ZgsTaskprojectmainparticipant zj : zgsTaskprojectmainparticipantList) {
                if (StringUtils.isEmpty(zj.getSex())) {
                    zj.setSex(zgsProjecttask.getHeadsex());
                }
                if (StringUtils.isEmpty(zj.getProfessional())) {
                    zj.setProfessional(zgsProjecttask.getMajor());
                }
            }
            zgsProjecttask.setZgsTaskparticipantList(zgsTaskprojectmainparticipantList);
            //项目其他主要参加单位
            //或联合申报单位
            //先获取项目库
            ZgsProjectlibrary zgsProjectlibrary = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectlibraryService.getById(projectguid), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjectlibrary != null) {
                zgsProjecttask.setZgsProjectlibrary(zgsProjectlibrary);
                //费用编辑状态0可编辑，1不可编辑，2明细可编辑
                if (StringUtils.isEmpty(zgsProjectlibrary.getFeeStatus())) {
                    zgsProjecttask.setFeeStatus("1");
                } else {
                    zgsProjecttask.setFeeStatus(zgsProjectlibrary.getFeeStatus());
                }
                if (zgsProjectlibrary.getStartdate() == null) {
                    zgsProjectlibrary.setStartdate(zgsProjectlibrary.getProjectstartdate());
                    zgsProjectlibrary.setCompletedate(zgsProjectlibrary.getProjectenddate());
                }
                //再获取工程示范项目
                //绿色建筑示范 GreenBuildType;建筑工程示范	ConstructBuidType;装配式建筑示范 AssemBuidType;建筑节能示范	EnergyBuildType;市政工程示范 SampleBuidType
                String projecttypenum = zgsProjectlibrary.getProjecttypenum();
                if (StringUtils.isNotEmpty(projecttypenum)) {
                    String buildguid = null;
                    //
                    ZgsCommonFee zgsCommonFee = new ZgsCommonFee();
                    buildguid = initCommonFeeData(projecttypenum, zgsProjectlibrary, buildguid, zgsProjecttask, 1, zgsCommonFee);
                    //
                    QueryWrapper<ZgsBuildjointunit> queryWrapper2 = new QueryWrapper<>();
//                    queryWrapper2.eq("enterpriseguid", enterpriseguid);
                    queryWrapper2.eq("buildguid", buildguid);
                    queryWrapper2.eq("tag", "1");
                    queryWrapper2.ne("isdelete", 1);
                    List<ZgsBuildjointunit> zgsBuildjointunitList = zgsBuildjointunitService.list(queryWrapper2);
                    if (zgsBuildjointunitList.size() == 0) {
                        QueryWrapper<ZgsBuildjointunit> queryWrapper2_1 = new QueryWrapper<>();
//                        queryWrapper2_1.eq("enterpriseguid", enterpriseguid);
                        queryWrapper2_1.eq("buildguid", buildguid);
                        queryWrapper2_1.ne("isdelete", 1);
                        zgsBuildjointunitList = zgsBuildjointunitService.list(queryWrapper2_1);
                    }
                    zgsProjecttask.setZgsBuildjointunitList(ValidateEncryptEntityUtil.validateDecryptList(zgsBuildjointunitList, ValidateEncryptEntityUtil.isDecrypt));
                }
            }
            //经费预算
            QueryWrapper<ZgsTaskbuildfundbudget> queryWrapper3 = new QueryWrapper<>();
            queryWrapper3.eq("taskguid", taskid);
            queryWrapper3.ne("isdelete", 1);
            queryWrapper3.orderByAsc("year");
            List<ZgsTaskbuildfundbudget> zgsTaskbuildfundbudgetList = ValidateEncryptEntityUtil.validateDecryptList(zgsTaskbuildfundbudgetService.list(queryWrapper3), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsTaskbuildfundbudgetList.size() > 0) {
                for (int i = 0; i < zgsTaskbuildfundbudgetList.size(); i++) {
                    zgsTaskbuildfundbudgetList.get(i).setProvinceFundRemark(zgsProjectlibrary.getFundRemark());
                }
            }
            zgsProjecttask.setZgsTaskbuildfundbudgetList(zgsTaskbuildfundbudgetList);
        }
    }

    /**
     * 初始化费用明细
     *
     * @param projecttypenum
     * @param zgsProjectlibrary
     * @param buildguid
     * @param zgsProjecttask
     * @param type
     */
    String initCommonFeeData(String projecttypenum, ZgsProjectlibrary zgsProjectlibrary, String buildguid, ZgsProjecttask zgsProjecttask, int type, ZgsCommonFee zgsCommonFee) {

        if (GlobalConstants.GreenBuildType.equals(projecttypenum)) {
            QueryWrapper<ZgsGreenbuildproject> queryWrapper_1 = new QueryWrapper<>();
            queryWrapper_1.eq("projectlibraryguid", zgsProjectlibrary.getId());
            queryWrapper_1.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
            queryWrapper_1.ne("isdelete", 1);
            ZgsGreenbuildproject zgsGreenbuildproject = zgsGreenbuildprojectService.getOne(queryWrapper_1);
            buildguid = zgsGreenbuildproject.getId();
            if (type == 1) {
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                zgsProjecttask.setProjectsummary(zgsGreenbuildproject.getProjectsurvey());
            }
            BeanUtils.copyProperties(zgsGreenbuildproject, zgsCommonFee);
        } else if (GlobalConstants.ConstructBuidType.equals(projecttypenum)) {
            QueryWrapper<ZgsBuildproject> queryWrapper_2 = new QueryWrapper<>();
            queryWrapper_2.eq("projectlibraryguid", zgsProjectlibrary.getId());
            queryWrapper_2.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
            queryWrapper_2.ne("isdelete", 1);
            ZgsBuildproject zgsBuildproject = zgsBuildprojectService.getOne(queryWrapper_2);
            buildguid = zgsBuildproject.getId();
            if (type == 1) {
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                zgsProjecttask.setProjectsummary(zgsBuildproject.getProjectsummary());
            }
            BeanUtils.copyProperties(zgsBuildproject, zgsCommonFee);
        } else if (GlobalConstants.AssemBuidType.equals(projecttypenum)) {
            QueryWrapper<ZgsAssembleproject> queryWrapper_5 = new QueryWrapper<>();
            queryWrapper_5.eq("projectlibraryguid", zgsProjectlibrary.getId());
            queryWrapper_5.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
            queryWrapper_5.ne("isdelete", 1);
            ZgsAssembleproject zgsAssembleproject = zgsAssembleprojectService.getOne(queryWrapper_5);
            buildguid = zgsAssembleproject.getId();
            if (type == 1) {
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                zgsProjecttask.setProjectsummary(zgsAssembleproject.getProjectsurvey());
            }
            BeanUtils.copyProperties(zgsAssembleproject, zgsCommonFee);
        } else if (GlobalConstants.EnergyBuildType.equals(projecttypenum)) {
            QueryWrapper<ZgsEnergybuildproject> queryWrapper_4 = new QueryWrapper<>();
            queryWrapper_4.eq("projectlibraryguid", zgsProjectlibrary.getId());
            queryWrapper_4.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
            queryWrapper_4.ne("isdelete", 1);
            ZgsEnergybuildproject zgsEnergybuildproject = zgsEnergybuildprojectService.getOne(queryWrapper_4);
            buildguid = zgsEnergybuildproject.getId();
            if (type == 1) {
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                zgsProjecttask.setProjectsummary(zgsEnergybuildproject.getProjectsurvey());
            }
            BeanUtils.copyProperties(zgsEnergybuildproject, zgsCommonFee);
        } else if (GlobalConstants.SampleBuidType.equals(projecttypenum)) {
            QueryWrapper<ZgsSamplebuildproject> queryWrapper_3 = new QueryWrapper<>();
            queryWrapper_3.eq("projectlibraryguid", zgsProjectlibrary.getId());
            queryWrapper_3.eq("enterpriseguid", zgsProjectlibrary.getEnterpriseguid());
            queryWrapper_3.ne("isdelete", 1);
            ZgsSamplebuildproject zgsSamplebuildproject = zgsSamplebuildprojectService.getOne(queryWrapper_3);
            buildguid = zgsSamplebuildproject.getId();
            if (type == 1) {
                zgsProjecttask.setProjectnum(zgsProjectlibrary.getProjectnum());
                zgsProjecttask.setProjectsummary(zgsSamplebuildproject.getProjectsummary());
            }
            BeanUtils.copyProperties(zgsSamplebuildproject, zgsCommonFee);
        }
        zgsProjecttask.setZgsCommonFee(zgsCommonFee);
        return buildguid;
    }

    @Override
    public boolean selectProjectExist(String projectguid) {
        QueryWrapper<ZgsProjecttask> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        queryWrapper.eq("projectguid", projectguid);
        List<ZgsProjecttask> list = this.baseMapper.selectList(queryWrapper);
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<ZgsProjecttask> zgsProjectTaskListForYq(Wrapper<ZgsProjecttask> queryWrapper) {
        return ValidateEncryptEntityUtil.validateDecryptList(this.baseMapper.zgsProjectTaskListForYq(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
    }
}
