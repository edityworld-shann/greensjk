package org.jeecg.modules.green.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.ZgsProjectlibrary;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.service.AsyncService;
import org.jeecg.modules.green.common.service.IZgsProjecttaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/5/1523:33
 */
@Service
@Slf4j
public class AsyncServiceImpl implements AsyncService {

    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;

    @Override
    @Async
    public void initProjectTaskPdfUrl(String id) {
        log.info("终审通过异步生成示范任务书pdf-------开始");
        try {
            ZgsProjecttask zgsProjecttask = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjecttaskService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
            if (zgsProjecttask != null) {
                zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
                ZgsProjectlibrary zgsProjectlibrary = zgsProjecttask.getZgsProjectlibrary();
                if (zgsProjectlibrary != null) {
                    String fileName = zgsProjectlibrary.getProjectname();
                    if (StringUtils.isNotEmpty(fileName)) {
                        String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "8建设厅任务书-示范工程.docx", BeanUtil.beanToMap(zgsProjecttask));
                        ZgsProjecttask projecttask = new ZgsProjecttask();
                        projecttask.setId(zgsProjecttask.getId());
                        projecttask.setPdfUrl(filePath.replace(".doc", ".pdf"));
                        zgsProjecttaskService.updateById(projecttask);
                        String pdf_filePath = upload + File.separator + filePath;
                        pdf_filePath = pdf_filePath.replace("//", "\\");
                        final String newPath = pdf_filePath;
                        File file = new File(pdf_filePath);
                        if (file.exists() && file.length() > 0) {
                            log.info("文件转换即将开始");
//                            LibreOfficeUtil.wordConverterToPdf("D:\\opt\\upFiles\\green_task\\2022-05-16\\关联项目测试使用被修改1.doc");
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(10000);
                                        LibreOfficeUtil.wordConverterToPdf(newPath);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } else {
                            log.info("文件转换条件不成立");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("终审通过异步生成示范任务书pdf-------结束");
    }
}
