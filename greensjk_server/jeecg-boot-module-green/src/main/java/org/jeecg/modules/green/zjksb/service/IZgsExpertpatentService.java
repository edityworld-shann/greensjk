package org.jeecg.modules.green.zjksb.service;

import org.jeecg.modules.green.zjksb.entity.ZgsExpertpatent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 专家库专家专利信息
 * @Author: jeecg-boot
 * @Date:   2022-02-18
 * @Version: V1.0
 */
public interface IZgsExpertpatentService extends IService<ZgsExpertpatent> {

}
