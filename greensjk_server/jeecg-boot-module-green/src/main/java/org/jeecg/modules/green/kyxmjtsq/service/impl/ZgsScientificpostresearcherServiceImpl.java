package org.jeecg.modules.green.kyxmjtsq.service.impl;

import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostresearcher;
import org.jeecg.modules.green.kyxmjtsq.mapper.ZgsScientificpostresearcherMapper;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostresearcherService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 科研结题主要研究人员名单
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
@Service
public class ZgsScientificpostresearcherServiceImpl extends ServiceImpl<ZgsScientificpostresearcherMapper, ZgsScientificpostresearcher> implements IZgsScientificpostresearcherService {

}
