package org.jeecg.modules.green.zqcysb.service;

import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 建设科技示范项目实施情况中期查验申请业务主表
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsMidterminspectionService extends IService<ZgsMidterminspection> {
    int spProject(ZgsMidterminspection zgsMidterminspection);
}
