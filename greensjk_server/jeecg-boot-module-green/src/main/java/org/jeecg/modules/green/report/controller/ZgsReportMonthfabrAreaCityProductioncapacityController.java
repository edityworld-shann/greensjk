package org.jeecg.modules.green.report.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javassist.runtime.Desc;
import net.sf.jsqlparser.expression.DoubleValue;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityCompleted;
import org.jeecg.modules.green.report.entity.ZgsReportMonthfabrAreaCityProductioncapacity;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompanyService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityCompletedService;
import org.jeecg.modules.green.report.service.IZgsReportMonthfabrAreaCityProductioncapacityService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 装配式建筑-生产产能-汇总表
 * @Author: jeecg-boot
 * @Date: 2022-05-20
 * @Version: V1.0
 */
@Api(tags = "装配式建筑-生产产能-汇总表")
@RestController
@RequestMapping("/report/zgsReportMonthfabrAreaCityProductioncapacity")
@Slf4j
public class ZgsReportMonthfabrAreaCityProductioncapacityController extends JeecgController<ZgsReportMonthfabrAreaCityProductioncapacity, IZgsReportMonthfabrAreaCityProductioncapacityService> {

    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService zgsReportMonthfabrAreaCityProductioncapacityService;

    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService zgsReportMonthfabrAreaCityCompletedService;

    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService zgsReportMonthfabrAreaCityCompanyService;
    @Autowired
    private RedisUtil redisUtil;


    /**
     * 滚动屏 - 新开工装配式建筑面积
     *
     * @param filltm
     */
    @AutoLog(value = "滚动屏 - 新开工装配式建筑面积")
    @ApiOperation(value = "滚动屏 - 新开工装配式建筑面积", notes = "滚动屏 - 新开工装配式建筑面积")
    @GetMapping(value = "/newConsFabricat")
    public Result<?> totalNewconstructionFabricatAreaData(@RequestParam(name = "filltm", required = true) String filltm) {
        // ===============   新开工装配式建筑面积 Begin  ====---------------===================--------------- ===================---------------===================---------------
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = zgsReportMonthfabrAreaCityProductioncapacityService.totalNewconstructionFabricatAreaData(filltm);
        Map<String, Object> map = new HashMap();
        Map<String, Object> mapList = new HashMap();
        if (list != null) {
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(list.size() - 1).getYearArea() != null && list.get(list.size() - 1).getYearArea() != 0) {
                    double v = list.get(i).getYearArea() / list.get(list.size() - 1).getYearArea() * 100;
                    String format = new Formatter().format("%.2f", v).toString();
                    Double d = Double.valueOf(format);
                    map.put(list.get(i).getAreaname(), d);
                }
            }
        }
        mapList.put("新开工装配式建筑-全省本年累计", list.get(list.size() - 1).getYearArea());
        mapList.put("newConsFabricatList", map);
        // ===============   新开工装配式建筑面积 End  ====---------------===================--------------- ===================---------------===================---------------


        // ===============   新开工装配式建筑面积 Begin  ====---------------===================--------------- ===================---------------===================---------------
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list2 = zgsReportMonthfabrAreaCityProductioncapacityService.totalNewconstructionAreaData(filltm);
        Map<String, Object> map2 = new HashMap();
        if (list2 != null) {
            for (int i = 0; i < list2.size() - 1; i++) {
                if (list2.get(list2.size() - 1).getYearArea() != null && list2.get(list2.size() - 1).getYearArea() != 0) {
                    double v = list2.get(i).getYearArea() / list2.get(list2.size() - 1).getYearArea() * 100;
                    String format = new Formatter().format("%.2f", v).toString();
                    Double d = Double.valueOf(format);
                    map2.put(list2.get(i).getAreaname(), d);
                }
            }
        }

        if (list2.get(list2.size() - 1).getYearArea() != null && list2.get(list2.size() - 1).getYearArea() != 0) {
            double all = list.get(list.size() - 1).getYearArea() / list2.get(list2.size() - 1).getYearArea() * 100;
            String format = new Formatter().format("%.2f", all).toString();
            Double d = Double.valueOf(format);
            mapList.put("新开工装配式建筑面积在新开工装配式建筑面积的占比", d);
        }

        mapList.put("新开工-全省本年累计", list2.get(list2.size() - 1).getYearArea());
        mapList.put("newConsList", map2);
        // ===============   新开工装配式建筑面积 End  ====---------------===================--------------- ===================---------------===================---------------
        return Result.OK(mapList);
    }


    /**
     * 滚动屏 - 新开工装配式建筑面积
     *
     * @param filltm
     */
    @AutoLog(value = "滚动屏 - 新建 - 建筑面积")
    @ApiOperation(value = "滚动屏 - 新建 - 建筑面积", notes = "滚动屏 - 新建 - 建筑面积")
    @GetMapping(value = "/newBuildingArea")
    public Result<?> totalNewBuildingAreaData(@RequestParam(name = "filltm", required = true) String filltm) {
        // ===============   新开工装配式建筑面积 Begin  ====---------------===================--------------- ===================---------------===================---------------
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = zgsReportMonthfabrAreaCityProductioncapacityService.totalGreenBuildingAreaData(filltm);
        Map<String, Object> map = new HashMap();
        Map<String, Object> mapList = new HashMap();
        if (list != null) {
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i).getYearArea() != null) {
                    String format = new Formatter().format("%.2f", list.get(i).getYearArea()).toString();
                    Double d = Double.valueOf(format);
                    map.put(list.get(i).getAreaname(), d);
                }
            }
        }
        mapList.put("绿色建筑面积-全省本年累计", list.get(list.size() - 1).getYearArea());
        mapList.put("greenBuilding", map);
        // ===============   新开工装配式建筑面积 End  ====---------------===================--------------- ===================---------------===================---------------


        // ===============   新开工装配式建筑面积 Begin  ====---------------===================--------------- ===================---------------===================---------------
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list2 = zgsReportMonthfabrAreaCityProductioncapacityService.totalNewBuildingAreaData(filltm);
        Map<String, Object> map2 = new HashMap();
        if (list2 != null) {
            for (int i = 0; i < list2.size() - 1; i++) {
                if (list2.get(i).getYearArea() != null) {
                    String format = new Formatter().format("%.2f", list2.get(i).getYearArea()).toString();
                    Double d = Double.valueOf(format);
                    map2.put(list2.get(i).getAreaname(), d);
                }
            }
        }

        if (list2.get(list2.size() - 1).getYearArea() != null && list2.get(list2.size() - 1).getYearArea() != 0) {
            double all = list.get(list.size() - 1).getYearArea() / list2.get(list2.size() - 1).getYearArea() * 100;
            String format = new Formatter().format("%.2f", all).toString();
            Double d = Double.valueOf(format);
            mapList.put("绿色建筑面积在建筑面积的占比", d);
        }

        mapList.put("新建建筑面积-全省本年累计", list2.get(list2.size() - 1).getYearArea());
        mapList.put("newBuildingList", map2);
        // ===============   新开工装配式建筑面积 End  ====---------------===================--------------- ===================---------------===================---------------


        // ===============   新开工装配式建筑面积 Begin  ====---------------===================--------------- ===================---------------===================---------------
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list3 = zgsReportMonthfabrAreaCityProductioncapacityService.totalGreenBuildingLevelData(filltm);
        Map<String, Object> map3 = new HashMap();
        Map<String, Object> map4 = new HashMap();
        if (list3 != null) {
            for (int i = 0; i < list3.size(); i++) {
                if (list3.get(i).getYearNumber() != null) {
                    map3.put(list3.get(i).getBaseType(), list3.get(i).getYearNumber());
                }

                if (list3.get(i).getYearArea() != null) {
                    String format = new Formatter().format("%.2f", list3.get(i).getYearArea()).toString();
                    Double d = Double.valueOf(format);
                    map4.put(list3.get(i).getBaseType(), d);
                }
            }
        }

        if (list2.get(list2.size() - 1).getYearArea() != null && list2.get(list2.size() - 1).getYearArea() != 0) {
            double all = list.get(list.size() - 1).getYearArea() / list2.get(list2.size() - 1).getYearArea() * 100;
            String format = new Formatter().format("%.2f", all).toString();
            Double d = Double.valueOf(format);
            mapList.put("绿色建筑面积在建筑面积的占比", d);
        }

        mapList.put("新建建筑面积-全省本年累计", list2.get(list2.size() - 1).getYearArea());
        mapList.put("newBuildingList", map2);
        mapList.put("greenBuildingLevelProjectList", map3);
        mapList.put("greenBuildingLevelAreaList", map4);
        // ===============   新开工装配式建筑面积 End  ====---------------===================--------------- ===================---------------===================---------------
        return Result.OK(mapList);
    }


    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabrAreaCityProductioncapacity
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-分页列表查询")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-分页列表查询", notes = "装配式建筑-生产产能-汇总表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        if (zgsReportMonthfabrAreaCityProductioncapacity != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProductioncapacity.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityProductioncapacity.getApplystate());
                }
            }
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.isNull("area_type");
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.isNull("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.isNull("area_type");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.eq("areacode", sysUser.getAreacode());
        }
        Page<ZgsReportMonthfabrAreaCityProductioncapacity> page = new Page<ZgsReportMonthfabrAreaCityProductioncapacity>(pageNo, pageSize);
        IPage<ZgsReportMonthfabrAreaCityProductioncapacity> pageList = zgsReportMonthfabrAreaCityProductioncapacityService.page(page, queryWrapper);
        pageList.setRecords(ValidateEncryptEntityUtil.validateDecryptList(pageList.getRecords(), ValidateEncryptEntityUtil.isDecrypt));
        return Result.OK(pageList);
    }


    /**
     * 分页列表查询
     *
     * @param zgsReportMonthfabrAreaCityProductioncapacity
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-列表查询")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-列表查询", notes = "装配式建筑-生产产能-汇总表-列表查询")
    @GetMapping(value = "/queryList")
    public Result<?> queryList(ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity,
                               @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                               @RequestParam(name = "pageSize", defaultValue = "99") Integer pageSize,
                               HttpServletRequest req) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportMonthfabrAreaCityProductioncapacity != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProductioncapacity.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityProductioncapacity.getApplystate());
                    applystate = zgsReportMonthfabrAreaCityProductioncapacity.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
//           queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
//         queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.isNull("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.isNull("area_type");
            }
            queryWrapper.isNull("area_type");
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.likeRight("areacode", sysUser.getAreacode());
            queryWrapper.isNull("area_type");
        }
        queryWrapper.orderByAsc("areacode");
        List<ZgsReportMonthfabrAreaCityProductioncapacity> areaCityCompletedList = zgsReportMonthfabrAreaCityProductioncapacityService.list(queryWrapper);
        ZgsReportMonthfabrAreaCityProductioncapacity total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 4 || zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataCity(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), zgsReportMonthfabrAreaCityProductioncapacity.getAreacode(), applystate);
                } else {
                    total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataProvince(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), applystate);
                }
            } else {
                total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataProvince(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProductioncapacity.getReporttm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            totalQueryWrapper.ne("isdelete", 1);
            totalQueryWrapper.orderByAsc(" createtime");
            ZgsReportMonthfabrAreaCityProductioncapacity totalCity = zgsReportMonthfabrAreaCityProductioncapacityService.getOne(totalQueryWrapper);
            total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataCity(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), sysUser.getAreacode(), applystate);
            if (total != null) {
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                //计算月份、年、季度
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                String yearMonth = simpleDateFormat.format(new Date());
                String year = yearMonth.substring(0, 4);
                String month = yearMonth.substring(5, 7);
                int intMonth = Integer.parseInt(month);
                int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//                total.setCreatepersonaccount(sysUser.getUsername());
//                total.setCreatepersonname(sysUser.getRealname());
                total.setCreatetime(new Date());
                total.setAreacode(sysUser.getAreacode());
                total.setAreaname(sysUser.getAreaname());
                total.setFilltm(yearMonth);
                total.setReporttm(yearMonth);
                total.setIsdelete(new BigDecimal(0));
                total.setQuarter(new BigDecimal(quarter));
                total.setYear(new BigDecimal(year));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (areaCityCompletedList.size() > 0) {
            if (total != null) {
                areaCityCompletedList.add(total);
            }
        }
        if (areaCityCompletedList.size() > 1) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                areaCityCompletedList.get(areaCityCompletedList.size() - 1).setApplystate(null);
            }
        }
        return Result.OK(ValidateEncryptEntityUtil.validateDecryptList(areaCityCompletedList, ValidateEncryptEntityUtil.isDecrypt));
    }


    /**
     * 装配式建筑-已竣工-汇总表-审批
     *
     * @param id
     * @param spStatus
     * @return
     */
    @AutoLog(value = "装配式建筑-已竣工-汇总表-审批")
    @ApiOperation(value = "装配式建筑-已竣工-汇总表-审批", notes = "装配式建筑-已竣工-汇总表-审批")
    @PutMapping(value = "/spProject")
    public Result<?> spProject(@RequestParam(name = "id") String id,
                               @RequestParam(name = "spStatus") Integer spStatus,
                               @RequestParam(name = "backreason", required = false) String backreason) {
        zgsReportMonthfabrAreaCityProductioncapacityService.spProject(id, spStatus, backreason);
        return Result.OK("操作成功!");
    }


    /**
     * 新建建筑节能情况汇总表-本月一键上报（市州）
     *
     * @param filltm
     * @return
     */
    @AutoLog(value = "新建建筑节能情况汇总表-本月一键上报（市州）")
    @ApiOperation(value = "新建建筑节能情况汇总表-本月一键上报（市州）", notes = "新建建筑节能情况汇总表-本月一键上报（市州）")
    @GetMapping(value = "/summary")
    public Result<?> summary(@RequestParam(name = "reporttm") String filltm,
                             @RequestParam(name = "projecttype") String projecttype,
                             @RequestParam(name = "newConstructionArea") double newConstructionArea,
                             @RequestParam(name = "completedArea") double completedArea) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        //添加事务锁，防3秒频繁点击
        if (!redisUtil.lock(filltm + "Productioncapacity" + sysUser.getAreacode(), String.valueOf(new Date().getTime()))) {
            return Result.error("上报操作频繁，请稍后再试！");
        }
        //计算月份、年、季度
        String yearMonth = filltm.substring(0, 7);
        String year = yearMonth.substring(0, 4);
        String month = yearMonth.substring(5, 7);
        int intMonth = Integer.parseInt(month);
        int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
        if (!filltm.equals(yearMonth)) {
//            return Result.error("上报日期错误！");
        }
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("reporttm", filltm);
        queryWrapper.likeRight("areacode", sysUser.getAreacode());
        queryWrapper.isNull("area_type");
        queryWrapper.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = zgsReportMonthfabrAreaCityProductioncapacityService.list(queryWrapper);
        if (list.size() > 0) {
            return Result.error("请先处理生产产能汇总未审批记录！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> wrapper = new QueryWrapper();
//     wrapper.eq("reporttm", filltm);
//     wrapper.eq("areacode", sysUser.getAreacode());
//     wrapper.ne("applystate", GlobalConstants.apply_state3);
//     wrapper.isNotNull("area_type");
//     List<ZgsReportMonthfabrAreaCityProductioncapacity> listTotal = zgsReportMonthfabrAreaCityProductioncapacityService.list(wrapper);
//     if (listTotal.size() > 0) {
//       return Result.error("本月汇总数据已上报！");
//     }
        // ====--====--====--====--====-- 新开工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapperXkg = new QueryWrapper();
        queryWrapperXkg.eq("reporttm", filltm);
        queryWrapperXkg.likeRight("areacode", sysUser.getAreacode());
        queryWrapperXkg.isNull("area_type");
        queryWrapperXkg.eq("projecttype", 1);
        queryWrapperXkg.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityCompleted> listXkg = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapperXkg);
        if (listXkg.size() > 0) {
            return Result.error("请先处理新开工汇总未审批记录！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperXkg = new QueryWrapper();
//     wrapperXkg.eq("reporttm", filltm);
//     wrapperXkg.eq("areacode", sysUser.getAreacode());
//     wrapperXkg.eq("projecttype", 1);
//     wrapperXkg.ne("applystate", GlobalConstants.apply_state3);
//     wrapperXkg.isNotNull("area_type");
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotalXkg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperXkg);
//     if (listTotalXkg.size() > 0) {
//       return Result.error("本月汇总数据已上报！");
//     }
        // ====--====--====--====--====-- 新开工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
        // ====--====--====--====--====-- 已竣工 上报 （区县 → 市州）    Begin ====--====--====--====--====--====--====--====--====--====--
        QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> queryWrapperYjg = new QueryWrapper();
        queryWrapperYjg.eq("reporttm", filltm);
        queryWrapperYjg.likeRight("areacode", sysUser.getAreacode());
        queryWrapperYjg.isNull("area_type");
        queryWrapperYjg.eq("projecttype", 2);
        queryWrapperYjg.eq("applystate", GlobalConstants.apply_state1);
        List<ZgsReportMonthfabrAreaCityCompleted> listYjg = zgsReportMonthfabrAreaCityCompletedService.list(queryWrapperYjg);
        if (listYjg.size() > 0) {
            return Result.error("请先处理已竣工汇总未审批记录！");
        }
        //再判断本月是否已上报
//     QueryWrapper<ZgsReportMonthfabrAreaCityCompleted> wrapperYjg = new QueryWrapper();
//     wrapperYjg.eq("reporttm", filltm);
//     wrapperYjg.eq("areacode", sysUser.getAreacode());
//     wrapperYjg.eq("projecttype", 2);
//     wrapperYjg.ne("applystate", GlobalConstants.apply_state3);
//     wrapperYjg.isNotNull("area_type");
//     List<ZgsReportMonthfabrAreaCityCompleted> listTotalYjg = zgsReportMonthfabrAreaCityCompletedService.list(wrapperYjg);
//     if (listTotalYjg.size() > 0) {
//       return Result.error("本月汇总数据已上报！");
//     }
        // ====--====--====--====--====-- 已竣工 上报 （区县 → 市州）    End ====--====--====--====--====--====--====--====--====--====--
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotal(year, quarter, filltm, "1", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotal(year, quarter, filltm, "2", newConstructionArea, completedArea);
//        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrCityTotal(year, quarter, filltm, null);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(0, GlobalConstants.apply_state1, year, quarter, filltm, "1", sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        zgsReportMonthfabrAreaCityCompletedService.initMonthfabrCityTotalAll(0, GlobalConstants.apply_state1, year, quarter, filltm, "2", sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        zgsReportMonthfabrAreaCityProductioncapacityService.initMonthfabrCityTotalALL(0, GlobalConstants.apply_state1, year, quarter, filltm, sysUser.getAreacode(), sysUser.getAreaname(), sysUser);
        return Result.OK("本月汇总一键上报成功！");
    }


    /**
     * 添加
     *
     * @param zgsReportMonthfabrAreaCityProductioncapacity
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-添加")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-添加", notes = "装配式建筑-生产产能-汇总表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (zgsReportMonthfabrAreaCityProductioncapacity != null) {
            zgsReportMonthfabrAreaCityProductioncapacity.setAreaname(sysUser.getAreaname());
            zgsReportMonthfabrAreaCityProductioncapacity.setAreacode(sysUser.getAreacode());
//      zgsReportMonthfabrAreaCityProductioncapacity.setAreaType(sysUser.getUsertype());   //  默认为空：市州内上报，1市州向省厅上报
            zgsReportMonthfabrAreaCityProductioncapacity.setCreatepersonaccount(sysUser.getUsertype());
            zgsReportMonthfabrAreaCityProductioncapacity.setCreatepersonname(sysUser.getUsertype());
            zgsReportMonthfabrAreaCityProductioncapacity.setCreatetime(new Date());
            zgsReportMonthfabrAreaCityProductioncapacity.setIsdelete(new BigDecimal(0));
            zgsReportMonthfabrAreaCityProductioncapacity.setYear(new BigDecimal(2022));
            zgsReportMonthfabrAreaCityProductioncapacity.setApplystate(new BigDecimal(0));   //  申报状态 0未上报 1 已上报 3 退回
        }
        zgsReportMonthfabrAreaCityProductioncapacityService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityProductioncapacity, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("添加成功！");
    }

    /**
     * 编辑
     *
     * @param zgsReportMonthfabrAreaCityProductioncapacity
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-编辑")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-编辑", notes = "装配式建筑-生产产能-汇总表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity) {
        zgsReportMonthfabrAreaCityProductioncapacityService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsReportMonthfabrAreaCityProductioncapacity, ValidateEncryptEntityUtil.isEncrypt));
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-通过id删除")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-通过id删除", notes = "装配式建筑-生产产能-汇总表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsReportMonthfabrAreaCityProductioncapacityService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-批量删除")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-批量删除", notes = "装配式建筑-生产产能-汇总表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsReportMonthfabrAreaCityProductioncapacityService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "装配式建筑-生产产能-汇总表-通过id查询")
    @ApiOperation(value = "装配式建筑-生产产能-汇总表-通过id查询", notes = "装配式建筑-生产产能-汇总表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity = ValidateEncryptEntityUtil.validateDecryptObject(zgsReportMonthfabrAreaCityProductioncapacityService.getById(id), ValidateEncryptEntityUtil.isDecrypt);
        if (zgsReportMonthfabrAreaCityProductioncapacity == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsReportMonthfabrAreaCityProductioncapacity);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsReportMonthfabrAreaCityProductioncapacity
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsReportMonthfabrAreaCityProductioncapacity zgsReportMonthfabrAreaCityProductioncapacity) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("isdelete", 1);
        Integer applystate = null;
        if (zgsReportMonthfabrAreaCityProductioncapacity != null) {
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm())) {
                queryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProductioncapacity.getReporttm());
            }
            if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate() != null) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getApplystate().intValue() != -1) {
                    queryWrapper.eq("applystate", zgsReportMonthfabrAreaCityProductioncapacity.getApplystate());
                    applystate = zgsReportMonthfabrAreaCityProductioncapacity.getApplystate().intValue();
                }
            }
        }
        //省市退回过滤掉已退回状态的数据，但是县区可以看到
        if (sysUser.getLoginUserType() != GlobalConstants.loginUserType_6) {
            queryWrapper.ne("applystate", GlobalConstants.apply_state3);
        }
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //省
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 4) {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.orderByAsc("area_type");
                    queryWrapper.isNull("area_type");
//                    queryWrapper.last(" and length(areacode)=6");
                } else if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.isNotNull("area_type");
                    queryWrapper.orderByAsc("area_type");
//           queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
                }
            } else {
                queryWrapper.isNotNull("area_type");
                queryWrapper.orderByAsc("area_type");
//         queryWrapper.last(" and length(areacode)=4 ORDER BY createtime DESC");
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //市
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    queryWrapper.eq("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                } else {
                    queryWrapper.likeRight("areacode", zgsReportMonthfabrAreaCityProductioncapacity.getAreacode());
                    queryWrapper.isNull("area_type");
                }
            } else {
                queryWrapper.likeRight("areacode", sysUser.getAreacode());
                queryWrapper.isNull("area_type");
            }
            queryWrapper.isNull("area_type");
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_6) {
            //县
            queryWrapper.likeRight("areacode", sysUser.getAreacode());
            queryWrapper.isNull("area_type");
        }
        queryWrapper.orderByAsc("areacode");
        List<ZgsReportMonthfabrAreaCityProductioncapacity> areaCityCompletedList = zgsReportMonthfabrAreaCityProductioncapacityService.list(queryWrapper);
        ZgsReportMonthfabrAreaCityProductioncapacity total = null;
        //获取合计数据
        if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
            //如果省厅账号，直接计算合计值
            //计算方法：累加多有市区的汇总，且areaType不等于空
            if (StringUtils.isNotEmpty(zgsReportMonthfabrAreaCityProductioncapacity.getAreacode())) {
                if (zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 4 || zgsReportMonthfabrAreaCityProductioncapacity.getAreacode().length() == 6) {
                    total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataCity(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), zgsReportMonthfabrAreaCityProductioncapacity.getAreacode(), applystate);
                } else {
                    total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataProvince(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), applystate);
                }
            } else {
                total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataProvince(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), applystate);
            }
        } else if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_7) {
            //如果市州
            //先判断是否存在本市上报汇总记录，根据areaType不等于空判断
            //如果有汇总记录不用add对象
            //否则需要计算areaType等于空，且属于本市及县汇总值
            QueryWrapper<ZgsReportMonthfabrAreaCityProductioncapacity> totalQueryWrapper = new QueryWrapper();
            totalQueryWrapper.eq("reporttm", zgsReportMonthfabrAreaCityProductioncapacity.getReporttm());
            totalQueryWrapper.eq("areacode", sysUser.getAreacode());
            totalQueryWrapper.isNotNull("area_type");
            totalQueryWrapper.ne("isdelete", 1);
            totalQueryWrapper.orderByAsc(" createtime");
            ZgsReportMonthfabrAreaCityProductioncapacity totalCity = zgsReportMonthfabrAreaCityProductioncapacityService.getOne(totalQueryWrapper);
            total = zgsReportMonthfabrAreaCityProductioncapacityService.lastTotalDataCity(zgsReportMonthfabrAreaCityProductioncapacity.getReporttm(), sysUser.getAreacode(), applystate);
            if (total != null) {
                total.setApplystate(new BigDecimal(GlobalConstants.apply_state0));
                //计算月份、年、季度
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
                String yearMonth = simpleDateFormat.format(new Date());
                String year = yearMonth.substring(0, 4);
                String month = yearMonth.substring(5, 7);
                int intMonth = Integer.parseInt(month);
                int quarter = intMonth % 3 == 0 ? intMonth / 3 : intMonth / 3 + 1;
//                total.setCreatepersonaccount(sysUser.getUsername());
//                total.setCreatepersonname(sysUser.getRealname());
                total.setCreatetime(new Date());
                total.setAreacode(sysUser.getAreacode());
                total.setAreaname(sysUser.getAreaname());
                total.setFilltm(yearMonth);
                total.setReporttm(yearMonth);
                total.setIsdelete(new BigDecimal(0));
                total.setQuarter(new BigDecimal(quarter));
                total.setYear(new BigDecimal(year));
                total.setAreaType("1");
            }
            if (totalCity != null) {
                total.setApplystate(totalCity.getApplystate());
                total.setBackreason(totalCity.getBackreason());
            }
        }
        if (areaCityCompletedList.size() > 0) {
            if (total != null) {
                total.setAreaname("合计");
                areaCityCompletedList.add(total);
            }
        }
        if (areaCityCompletedList.size() > 1) {
            if (sysUser.getLoginUserType() == GlobalConstants.loginUserType_3) {
                areaCityCompletedList.get(areaCityCompletedList.size() - 1).setApplystate(null);
            }
        }
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = areaCityCompletedList;
        List<ZgsReportMonthfabrAreaCityProductioncapacity> exportList = null;
        String selections = request.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = list.stream().filter(item -> selectionList.contains(getId(item))).collect(Collectors.toList());
        } else {
            exportList = list;
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        String title = "装配式建筑-生产产能-汇总表";
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, ZgsReportMonthfabrAreaCityProductioncapacity.class);
        mv.addObject(NormalExcelConstants.PARAMS, new ExportParams(title, "导出人:" + sysUser.getRealname(), title));
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        //return super.exportXls(request, zgsReportMonthfabrAreaCityProductioncapacity, ZgsReportMonthfabrAreaCityProductioncapacity.class, "装配式建筑-生产产能-汇总表");
        return mv;
    }

    private String getId(ZgsReportMonthfabrAreaCityProductioncapacity item) {
        try {
            return PropertyUtils.getProperty(item, "id").toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsReportMonthfabrAreaCityProductioncapacity.class);
    }

}
