package org.jeecg.modules.green.xmyszssq.service;

import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 科研项目验收证书
 * @Author: jeecg-boot
 * @Date: 2022-02-14
 * @Version: V1.0
 */
public interface IZgsSacceptcertificatebaseService extends IService<ZgsSacceptcertificatebase> {
    int spProject(ZgsSacceptcertificatebase zgsSacceptcertificatebase);

    public String number();
}
