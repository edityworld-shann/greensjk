package org.jeecg.modules.green.jhxmbgsq.controller;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.green.common.service.IZgsProjecttaskService;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

/**
 * @Description: 计划变更项目组成员表
 * @Author: jeecg-boot
 * @Date: 2022-07-14
 * @Version: V1.0
 */
@Api(tags = "计划变更项目组成员表")
@RestController
@RequestMapping("/jhxmbgsq/zgsPlannedchangeparticipant")
@Slf4j
public class ZgsPlannedchangeparticipantController extends JeecgController<ZgsPlannedchangeparticipant, IZgsPlannedchangeparticipantService> {
    @Autowired
    private IZgsPlannedchangeparticipantService zgsPlannedchangeparticipantService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;

    /**
     * 分页列表查询
     *
     * @param zgsPlannedchangeparticipant
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-分页列表查询")
    @ApiOperation(value = "计划变更项目组成员表-分页列表查询", notes = "计划变更项目组成员表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(ZgsPlannedchangeparticipant zgsPlannedchangeparticipant,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<ZgsPlannedchangeparticipant> queryWrapper = QueryGenerator.initQueryWrapper(zgsPlannedchangeparticipant, req.getParameterMap());
        Page<ZgsPlannedchangeparticipant> page = new Page<ZgsPlannedchangeparticipant>(pageNo, pageSize);
        IPage<ZgsPlannedchangeparticipant> pageList = zgsPlannedchangeparticipantService.page(page, queryWrapper);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param zgsPlannedchangeparticipant
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-添加")
    @ApiOperation(value = "计划变更项目组成员表-添加", notes = "计划变更项目组成员表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody ZgsPlannedchangeparticipant zgsPlannedchangeparticipant) {
        zgsPlannedchangeparticipantService.save(zgsPlannedchangeparticipant);
        return Result.OK("添加成功！");
    }

    /**
     * 添加List
     *
     * @param zgsTaskparticipantList
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-添加List")
    @ApiOperation(value = "计划变更项目组成员表-添加List", notes = "计划变更项目组成员表-添加List")
    @PostMapping(value = "/addList")
    public Result<?> addList(@RequestBody List<ZgsPlannedchangeparticipant> zgsTaskparticipantList) {
        StringBuilder stringBuilder = new StringBuilder();
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        if (!zgsTaskparticipantList.isEmpty()) {
            String taskguid = null;
            for (ZgsPlannedchangeparticipant zgsPlannedchangeparticipant : zgsTaskparticipantList) {
                if (StringUtils.isNotEmpty(zgsPlannedchangeparticipant.getTaskguid())) {
                    taskguid = zgsPlannedchangeparticipant.getTaskguid();
                }
                ZgsPlannedchangeparticipant plannedchangeparticipant = zgsPlannedchangeparticipantService.getById(zgsPlannedchangeparticipant.getId());
                if (plannedchangeparticipant != null) {
                    zgsPlannedchangeparticipantService.updateById(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedchangeparticipant, ValidateEncryptEntityUtil.isEncrypt));
                } else {
                    zgsPlannedchangeparticipant.setTaskguid(taskguid);
                    zgsPlannedchangeparticipant.setIsdelete(new BigDecimal(0));
                    zgsPlannedchangeparticipant.setCreatedate(new Date());
                    zgsPlannedchangeparticipant.setCreateaccountname(sysUser.getUsername());
                    zgsPlannedchangeparticipant.setCreateusername(sysUser.getRealname());
                    zgsPlannedchangeparticipantService.save(ValidateEncryptEntityUtil.validateEncryptObject(zgsPlannedchangeparticipant, ValidateEncryptEntityUtil.isEncrypt));
                }
            }
            QueryWrapper<ZgsPlannedchangeparticipant> queryWrapper = new QueryWrapper();
            queryWrapper.eq("taskguid", taskguid);
            List<ZgsPlannedchangeparticipant> list = zgsPlannedchangeparticipantService.list(queryWrapper);
            for (ZgsPlannedchangeparticipant pant : list) {
                if (!zgsTaskparticipantList.stream().anyMatch(m -> m.getId().equals(pant.getId()))) {
                    zgsPlannedchangeparticipantService.removeById(pant.getId());
                }
            }
            QueryWrapper<ZgsPlannedchangeparticipant> queryWrapperComp = new QueryWrapper();
            queryWrapperComp.eq("taskguid", taskguid);
            List<ZgsPlannedchangeparticipant> listComp = ValidateEncryptEntityUtil.validateDecryptList(zgsPlannedchangeparticipantService.list(queryWrapperComp), ValidateEncryptEntityUtil.isDecrypt);
//            log.info(listComp.toString());
            //再查出任务书关联的项目组成员做对比，输出文字
            List<ZgsPlannedchangeparticipant> pantsComp = zgsProjecttaskService.queryTaskparticipantList(taskguid);
            Map<String, ZgsPlannedchangeparticipant> map = new HashMap<>();
            if (pantsComp.size() > 0) {
                for (ZgsPlannedchangeparticipant zgsPant : pantsComp) {
                    map.put(zgsPant.getId(), zgsPant);
                }
            }
//            log.info(pantsComp.toString());
            if (listComp.size() > 0) {
                //先比较新表
                //变更前字段：XXX，变更前内容：XXX。变更后字段：XXX。变更后内容：XXX”
                for (ZgsPlannedchangeparticipant pant1 : listComp) {
                    StringBuilder builder = new StringBuilder();
                    int status_0 = -1;
                    int status = -1;
                    if (pantsComp.stream().anyMatch(m -> m.getId().equals(pant1.getId()))) {
                        ZgsPlannedchangeparticipant zgsPant = map.get(pant1.getId());
                        //修改
                        if (StringUtils.isNotEmpty(pant1.getPersonname()) && !pant1.getPersonname().equals(zgsPant.getPersonname())) {
                            //姓名
                            builder.append("姓名：" + zgsPant.getPersonname());
                            builder.append(",变更后内容：" + pant1.getPersonname() + "；\r\n");
                            status_0 = 1;
                        }
                        if (StringUtils.isNotEmpty(pant1.getSex()) && !pant1.getSex().equals(zgsPant.getSex())) {
                            //性别
                            if (StringUtils.isEmpty(zgsPant.getSex())) {
                                builder.append("变更字段：性别，变更前内容：空");
                            } else {
                                builder.append("变更字段：性别，变更前内容：" + zgsPant.getSex());
                            }
                            builder.append(",变更后内容：" + pant1.getSex() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getIdcard()) && !pant1.getIdcard().equals(zgsPant.getIdcard())) {
                            //身份证
                            if (StringUtils.isEmpty(zgsPant.getIdcard())) {
                                builder.append("变更字段：身份证，变更前内容：空");
                            } else {
                                builder.append("变更字段：身份证，变更前内容：" + zgsPant.getIdcard());
                            }
                            builder.append(",变更后内容：" + pant1.getIdcard() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getStandardOfCulture()) && !pant1.getStandardOfCulture().equals(zgsPant.getStandardOfCulture())) {
                            //文化程度
                            if (StringUtils.isEmpty(zgsPant.getStandardOfCulture())) {
                                builder.append("变更字段：文化程度，变更前内容：空");
                            } else {
                                builder.append("变更字段：文化程度，变更前内容：" + zgsPant.getStandardOfCulture());
                            }
                            builder.append(",变更后内容：" + pant1.getStandardOfCulture() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getPersonpost()) && !pant1.getPersonpost().equals(zgsPant.getPersonpost())) {
                            //职称
                            if (StringUtils.isEmpty(zgsPant.getPersonpost())) {
                                builder.append("变更字段：职称，变更前内容：空");
                            } else {
                                builder.append("变更字段：职称，变更前内容：" + zgsPant.getPersonpost());
                            }
                            builder.append(",变更后内容：" + pant1.getPersonpost() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getProfessional()) && !pant1.getProfessional().equals(zgsPant.getProfessional())) {
                            //专业
                            if (StringUtils.isEmpty(zgsPant.getProfessional())) {
                                builder.append("变更字段：专业，变更前内容：空");
                            } else {
                                builder.append("变更字段：专业，变更前内容：" + zgsPant.getProfessional());
                            }
                            builder.append(",变更后内容：" + pant1.getProfessional() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getUnit()) && !pant1.getUnit().equals(zgsPant.getUnit())) {
                            //单位
                            if (StringUtils.isEmpty(zgsPant.getUnit())) {
                                builder.append("变更字段：单位，变更前内容：空");
                            } else {
                                builder.append("变更字段：单位，变更前内容：" + zgsPant.getUnit());
                            }
                            builder.append(",变更后内容：" + pant1.getUnit() + "；\r\n");
                            status = 0;
                        }
                        if (StringUtils.isNotEmpty(pant1.getWorkcontent()) && !pant1.getWorkcontent().equals(zgsPant.getWorkcontent())) {
                            //承担主要工作
                            if (StringUtils.isEmpty(zgsPant.getWorkcontent())) {
                                builder.append("变更字段：承担主要工作，变更前内容：空");
                            } else {
                                builder.append("变更字段：承担主要工作，变更前内容：" + zgsPant.getWorkcontent());
                            }
                            builder.append(",变更后内容：" + pant1.getWorkcontent() + "；\r\n");
                            status = 0;
                        }
                        if (status_0 != 1 && status == 0) {
                            stringBuilder.append("姓名：" + zgsPant.getPersonname());
                            stringBuilder.append(",");
                        }
                        stringBuilder.append(builder.toString());
                    } else {
                        //新增
                        stringBuilder.append("新增项目组成员：" + pant1.getPersonname() + "；\r\n");
                    }
                }
                //再比较旧表
                for (ZgsPlannedchangeparticipant pant2 : pantsComp) {
                    if (!listComp.stream().anyMatch(m -> m.getId().equals(pant2.getId()))) {
                        //删除
                        stringBuilder.append("删除项目组成员：" + pant2.getPersonname() + "；\r\n");
                    }
                }
            }
        }
        return Result.OK(stringBuilder.toString());
    }

    /**
     * 编辑
     *
     * @param zgsPlannedchangeparticipant
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-编辑")
    @ApiOperation(value = "计划变更项目组成员表-编辑", notes = "计划变更项目组成员表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody ZgsPlannedchangeparticipant zgsPlannedchangeparticipant) {
        zgsPlannedchangeparticipantService.updateById(zgsPlannedchangeparticipant);
        return Result.OK("编辑成功!");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-通过id删除")
    @ApiOperation(value = "计划变更项目组成员表-通过id删除", notes = "计划变更项目组成员表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        zgsPlannedchangeparticipantService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-批量删除")
    @ApiOperation(value = "计划变更项目组成员表-批量删除", notes = "计划变更项目组成员表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.zgsPlannedchangeparticipantService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "计划变更项目组成员表-通过id查询")
    @ApiOperation(value = "计划变更项目组成员表-通过id查询", notes = "计划变更项目组成员表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        ZgsPlannedchangeparticipant zgsPlannedchangeparticipant = zgsPlannedchangeparticipantService.getById(id);
        if (zgsPlannedchangeparticipant == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(zgsPlannedchangeparticipant);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param zgsPlannedchangeparticipant
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ZgsPlannedchangeparticipant zgsPlannedchangeparticipant) {
        return super.exportXls(request, zgsPlannedchangeparticipant, ZgsPlannedchangeparticipant.class, "计划变更项目组成员表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ZgsPlannedchangeparticipant.class);
    }

}
