package org.jeecg.modules.green.xmyszssq.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 科研项目验收证书
 * @Author: jeecg-boot
 * @Date:   2022-02-14
 * @Version: V1.0
 */
public interface ZgsSacceptcertificatebaseMapper extends BaseMapper<ZgsSacceptcertificatebase> {

}
