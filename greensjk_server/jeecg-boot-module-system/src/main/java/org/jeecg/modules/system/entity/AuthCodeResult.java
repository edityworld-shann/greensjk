package org.jeecg.modules.system.entity;

import lombok.Data;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/218:19
 */
@Data
public class AuthCodeResult {
    private String code;
    private String msg;
    private AuthCodeUserInfo data;
}
