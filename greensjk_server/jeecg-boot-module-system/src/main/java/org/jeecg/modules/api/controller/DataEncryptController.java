package org.jeecg.modules.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsTaskchange;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsTaskchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancescoreService;
import org.jeecg.modules.green.kyxmjtsq.entity.*;
import org.jeecg.modules.green.kyxmjtsq.service.*;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostprojectfinishService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostresearcherService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencefundbudgetService;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceplanarrangeService;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.*;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.jeecg.modules.green.xmyssq.entity.*;
import org.jeecg.modules.green.xmyssq.service.*;
import org.jeecg.modules.green.xmyszssq.entity.*;
import org.jeecg.modules.green.xmyszssq.service.*;
import org.jeecg.modules.green.zjksb.entity.*;
import org.jeecg.modules.green.zjksb.service.*;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@RestController
@RequestMapping("/data/encrypt")
@Api(tags="DataEncryptController")
public class DataEncryptController {
    @Autowired
    private IZgsAssembleprojectService izgsassembleprojectservice;
    @Autowired
    private IZgsAssemblesingleprojectService izgsassemblesingleprojectservice;
    @Autowired
    private IZgsAssemprojectmainparticipantService izgsassemprojectmainparticipantservice;
    @Autowired
    private IZgsAttachappendixService izgsattachappendixservice;
    @Autowired
    private IZgsBuildfundbudgetService izgsbuildfundbudgetservice;
    @Autowired
    private IZgsBuildjointunitService izgsbuildjointunitservice;
    @Autowired
    private IZgsBuildprojectService izgsbuildprojectservice;
    @Autowired
    private IZgsBuildrojectmainparticipantService izgsbuildrojectmainparticipantservice;
    @Autowired
    private IZgsDemoprojectacceptanceService izgsdemoprojectacceptanceservice;
    @Autowired
    private IZgsDemoprojectapplicationService izgsdemoprojectapplicationservice;
    @Autowired
    private IZgsEnergybuildprojectService izgsenergybuildprojectservice;
    @Autowired
    private IZgsEnergyprojectmainparticipantService izgsenergyprojectmainparticipantservice;
    @Autowired
    private IZgsEnergyprojectplanarrangeService izgsenergyprojectplanarrangeservice;
    @Autowired
    private IZgsExamplecertificatebaseService izgsexamplecertificatebaseservice;
    @Autowired
    private IZgsExamplecertificateexpertService izgsexamplecertificateexpertservice;
    @Autowired
    private IZgsExamplecertimplementunitService izgsexamplecertimplementunitservice;
    @Autowired
    private IZgsExamplecertparticipantService izgsexamplecertparticipantservice;
    @Autowired
    private IZgsExpertawardService izgsexpertawardservice;
    @Autowired
    private IZgsExpertinfoService izgsexpertinfoservice;
    @Autowired
    private IZgsExpertmonographService izgsexpertmonographservice;
    @Autowired
    private IZgsExpertpaperService izgsexpertpaperservice;
    @Autowired
    private IZgsExpertpatentService izgsexpertpatentservice;
    @Autowired
    private IZgsExpertprojectbearService izgsexpertprojectbearservice;
    @Autowired
    private IZgsExpertsetService izgsexpertsetservice;
    @Autowired
    private IZgsExpertworkresumeService izgsexpertworkresumeservice;
    @Autowired
    private IZgsGreenbuildprojectService izgsgreenbuildprojectservice;
    @Autowired
    private IZgsGreenprojectmainparticipantService izgsgreenprojectmainparticipantservice;
    @Autowired
    private IZgsGreenprojectplanarrangeService izgsgreenprojectplanarrangeservice;
    @Autowired
    private IZgsIndexFileService izgsindexfileservice;
    @Autowired
    private IZgsMattermaterialService izgsmattermaterialservice;
    @Autowired
    private IZgsMidterminspectionService izgsmidterminspectionservice;
    @Autowired
    private IZgsPerformancebaseService izgsperformancebaseservice;
    @Autowired
    private IZgsPerformancescoreService izgsperformancescoreservice;
    @Autowired
    private IZgsPlannedchangeparticipantService izgsplannedchangeparticipantservice;
    @Autowired
    private IZgsPlannedprojectchangeService izgsplannedprojectchangeservice;
    @Autowired
    private IZgsPlannedprojectchangedetailService izgsplannedprojectchangedetailservice;
    @Autowired
    private IZgsPlanresultbaseService izgsplanresultbaseservice;
    @Autowired
    private IZgsProjectlibraryService izgsprojectlibraryservice;
    @Autowired
    private IZgsProjectlibraryexpertService izgsprojectlibraryexpertservice;
    @Autowired
    private IZgsProjecttaskService izgsprojecttaskservice;
    @Autowired
    private IZgsProjectunithistoryService izgsprojectunithistoryservice;
    @Autowired
    private IZgsProjectunitmemberService izgsprojectunitmemberservice;
    @Autowired
    private IZgsProjectunitmembersendService izgsprojectunitmembersendservice;
    @Autowired
    private IZgsPtempTjService izgsptemptjservice;
    @Autowired
    private IZgsPzMattermaterialService izgspzmattermaterialservice;
    @Autowired
    private IZgsReportAreacodeValuesService izgsreportareacodevaluesservice;
    @Autowired
    private IZgsReportEnergyExistinfoService izgsreportenergyexistinfoservice;
    @Autowired
    private IZgsReportEnergyExistinfoTotalService izgsreportenergyexistinfototalservice;
    @Autowired
    private IZgsReportEnergyNewinfoService izgsreportenergynewinfoservice;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService izgsreportenergynewinfogreentotalservice;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService izgsreportenergynewinfototalservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService izgsreportmonthfabrareacitycompanyservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService izgsreportmonthfabrareacitycompletedservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityNewconstructionService izgsreportmonthfabrareacitynewconstructionservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService izgsreportmonthfabrareacityproductioncapacityservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityProjectService izgsreportmonthfabrareacityprojectservice;
    @Autowired
    private IZgsReportProjectService izgsreportprojectservice;
    @Autowired
    private IZgsReturnrecordService izgsreturnrecordservice;
    @Autowired
    private IZgsSacceptcertexpertService izgssacceptcertexpertservice;
    @Autowired
    private IZgsSacceptcertificatebaseService izgssacceptcertificatebaseservice;
    @Autowired
    private IZgsSacceptcertprojectfinishService izgssacceptcertprojectfinishservice;
    @Autowired
    private IZgsSacceptcertresearcherService izgssacceptcertresearcherservice;
    @Autowired
    private IZgsSaexpertService izgssaexpertservice;
    @Autowired
    private IZgsSamplebuildprojectService izgssamplebuildprojectservice;
    @Autowired
    private IZgsSamplerojectmainparticipantService izgssamplerojectmainparticipantservice;
    @Autowired
    private IZgsSciencefundbudgetService izgssciencefundbudgetservice;
    @Autowired
    private IZgsSciencejointunitService izgssciencejointunitservice;
    @Autowired
    private IZgsScienceparticipantService izgsscienceparticipantservice;
    @Autowired
    private IZgsScienceplanarrangeService izgsscienceplanarrangeservice;
    @Autowired
    private IZgsSciencetechfeasibleService izgssciencetechfeasibleservice;
    @Autowired
    private IZgsSciencetechtaskService izgssciencetechtaskservice;
    @Autowired
    private IZgsScientificbaseService izgsscientificbaseservice;
    @Autowired
    private IZgsScientificexpenditureclearService izgsscientificexpenditureclearservice;
    @Autowired
    private IZgsScientificpexecutivelistService izgsscientificpexecutivelistservice;
    @Autowired
    private IZgsScientificpostbaseService izgsscientificpostbaseservice;
    @Autowired
    private IZgsScientificpostelistService izgsscientificpostelistservice;
    @Autowired
    private IZgsScientificpostexpendclearService izgsscientificpostexpendclearservice;
    @Autowired
    private IZgsScientificpostprojectexecutService izgsscientificpostprojectexecutservice;
    @Autowired
    private IZgsScientificpostprojectfinishService izgsscientificpostprojectfinishservice;
    @Autowired
    private IZgsScientificpostresearcherService izgsscientificpostresearcherservice;
    @Autowired
    private IZgsScientificprojectexecutiveService izgsscientificprojectexecutiveservice;
    @Autowired
    private IZgsScientificprojectfinishService izgsscientificprojectfinishservice;
    @Autowired
    private IZgsScientificresearcherService izgsscientificresearcherservice;
    @Autowired
    private IZgsSmsSendingRecordLogService izgssmssendingrecordlogservice;
    @Autowired
    private IZgsSofttechexpertService izgssofttechexpertservice;
    @Autowired
    private IZgsSpostcertificatebaseService izgsspostcertificatebaseservice;
    @Autowired
    private IZgsSpostprojectfinishService izgsspostprojectfinishservice;
    @Autowired
    private IZgsSpostresearcherService izgsspostresearcherservice;
    @Autowired
    private IZgsTaskbuildfundbudgetService izgstaskbuildfundbudgetservice;
    @Autowired
    private IZgsTaskchangeService izgstaskchangeservice;
    @Autowired
    private IZgsTaskprojectmainparticipantService izgstaskprojectmainparticipantservice;
    @Autowired
    private IZgsTaskscienceparticipantService izgstaskscienceparticipantservice;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private RedisUtil redisUtil;

    // private static boolean isEncrypt = false;
    

    
    // 判断是否加密工具类
    private static QueryWrapper getQueryByEncrypt(boolean flag) {
        QueryWrapper queryWrapper = new QueryWrapper();
        // 根据is_encrypt 字段判断加密状态
        if (!flag) {
            // 已加密的结果集
            queryWrapper.eq("is_encrypt", 1);
        }else{
            // 未加密的结果集
            queryWrapper.isNull("is_encrypt");
        }
        return queryWrapper;
    }


    @GetMapping("/encryptInitZgsAssembleproject")
    public void encryptInitZgsAssembleproject(@RequestParam("flag") boolean flag) {
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsAssembleproject> list = izgsassembleprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssembleproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassembleprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsAssemblesingleproject")
    public void encryptInitZgsAssemblesingleproject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsAssemblesingleproject> list = izgsassemblesingleprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssemblesingleproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassemblesingleprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsAssemprojectmainparticipant")
    public void encryptInitZgsAssemprojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsAssemprojectmainparticipant> list = izgsassemprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssemprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassemprojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsAttachappendix")
    public void encryptInitZgsAttachappendix(@RequestParam("flag") boolean flag) {
        QueryWrapper queryWrapper = new QueryWrapper();
        // if (isFlag) {
        //     queryWrapper.isNull("is_encrypt");
        // }
        List<ZgsAttachappendix> list = izgsattachappendixservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAttachappendix info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsattachappendixservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsBuildfundbudget")
    public void encryptInitZgsBuildfundbudget(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsBuildfundbudget> list = izgsbuildfundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildfundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildfundbudgetservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsBuildjointunit")
    public void encryptInitZgsBuildjointunit(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsBuildjointunit> list = izgsbuildjointunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildjointunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildjointunitservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsBuildproject")
    public void encryptInitZgsBuildproject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsBuildproject> list = izgsbuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsBuildrojectmainparticipant")
    public void encryptInitZgsBuildrojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsBuildrojectmainparticipant> list = izgsbuildrojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildrojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildrojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsDemoprojectacceptance")
    public void encryptInitZgsDemoprojectacceptance(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsDemoprojectacceptance> list = izgsdemoprojectacceptanceservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsDemoprojectacceptance info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsdemoprojectacceptanceservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsDemoprojectapplication")
    public void encryptInitZgsDemoprojectapplication(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsDemoprojectapplication> list = izgsdemoprojectapplicationservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsDemoprojectapplication info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsdemoprojectapplicationservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsEnergybuildproject")
    public void encryptInitZgsEnergybuildproject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsEnergybuildproject> list = izgsenergybuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergybuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergybuildprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsEnergyprojectmainparticipant")
    public void encryptInitZgsEnergyprojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsEnergyprojectmainparticipant> list = izgsenergyprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergyprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergyprojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsEnergyprojectplanarrange")
    public void encryptInitZgsEnergyprojectplanarrange(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsEnergyprojectplanarrange> list = izgsenergyprojectplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergyprojectplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergyprojectplanarrangeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExamplecertificatebase")
    public void encryptInitZgsExamplecertificatebase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExamplecertificatebase> list = izgsexamplecertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertificatebaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExamplecertificateexpert")
    public void encryptInitZgsExamplecertificateexpert(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExamplecertificateexpert> list = izgsexamplecertificateexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertificateexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertificateexpertservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExamplecertimplementunit")
    public void encryptInitZgsExamplecertimplementunit(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExamplecertimplementunit> list = izgsexamplecertimplementunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertimplementunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertimplementunitservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExamplecertparticipant")
    public void encryptInitZgsExamplecertparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "dabab5e0-f8fc-40a9-9be2-c0d925b6ca23");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExamplecertparticipant> list = izgsexamplecertparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertaward")
    public void encryptInitZgsExpertaward(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertaward> list = izgsexpertawardservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertaward info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertawardservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertinfo")
    public void encryptInitZgsExpertinfo(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertinfo> list = izgsexpertinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertinfoservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertmonograph")
    public void encryptInitZgsExpertmonograph(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertmonograph> list = izgsexpertmonographservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertmonograph info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertmonographservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertpaper")
    public void encryptInitZgsExpertpaper(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertpaper> list = izgsexpertpaperservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertpaper info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertpaperservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertpatent")
    public void encryptInitZgsExpertpatent(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertpatent> list = izgsexpertpatentservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertpatent info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertpatentservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertprojectbear")
    public void encryptInitZgsExpertprojectbear(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertprojectbear> list = izgsexpertprojectbearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertprojectbear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertprojectbearservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertset")
    public void encryptInitZgsExpertset(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertset> list = izgsexpertsetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertset info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertsetservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsExpertworkresume")
    public void encryptInitZgsExpertworkresume(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsExpertworkresume> list = izgsexpertworkresumeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertworkresume info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertworkresumeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsGreenbuildproject")
    public void encryptInitZgsGreenbuildproject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsGreenbuildproject> list = izgsgreenbuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenbuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenbuildprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsGreenprojectmainparticipant")
    public void encryptInitZgsGreenprojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsGreenprojectmainparticipant> list = izgsgreenprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenprojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsGreenprojectplanarrange")
    public void encryptInitZgsGreenprojectplanarrange(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsGreenprojectplanarrange> list = izgsgreenprojectplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenprojectplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenprojectplanarrangeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsIndexFile")
    public void encryptInitZgsIndexFile(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsIndexFile> list = izgsindexfileservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsIndexFile info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsindexfileservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsMattermaterial")
    public void encryptInitZgsMattermaterial(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsMattermaterial> list = izgsmattermaterialservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsMattermaterial info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsmattermaterialservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsMidterminspection")
    public void encryptInitZgsMidterminspection(@RequestParam("flag") boolean flag) {
       /* QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsMidterminspection> list = izgsmidterminspectionservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsMidterminspection info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsmidterminspectionservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPerformancebase")
    public void encryptInitZgsPerformancebase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPerformancebase> list = izgsperformancebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPerformancebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsperformancebaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPerformancescore")
    public void encryptInitZgsPerformancescore(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPerformancescore> list = izgsperformancescoreservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPerformancescore info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsperformancescoreservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPlannedchangeparticipant")
    public void encryptInitZgsPlannedchangeparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPlannedchangeparticipant> list = izgsplannedchangeparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedchangeparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedchangeparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPlannedprojectchange")
    public void encryptInitZgsPlannedprojectchange(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPlannedprojectchange> list = izgsplannedprojectchangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedprojectchange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedprojectchangeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPlannedprojectchangedetail")
    public void encryptInitZgsPlannedprojectchangedetail(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPlannedprojectchangedetail> list = izgsplannedprojectchangedetailservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedprojectchangedetail info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedprojectchangedetailservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPlanresultbase")
    public void encryptInitZgsPlanresultbase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPlanresultbase> list = izgsplanresultbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlanresultbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplanresultbaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjectlibrary")
    public void encryptInitZgsProjectlibrary(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjectlibrary> list = izgsprojectlibraryservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectlibrary info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectlibraryservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjectlibraryexpert")
    public void encryptInitZgsProjectlibraryexpert(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjectlibraryexpert> list = izgsprojectlibraryexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectlibraryexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectlibraryexpertservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjecttask")
    public void encryptInitZgsProjecttask(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjecttask> list = izgsprojecttaskservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjecttask info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojecttaskservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjectunithistory")
    public void encryptInitZgsProjectunithistory(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjectunithistory> list = izgsprojectunithistoryservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunithistory info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunithistoryservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjectunitmember")
    public void encryptInitZgsProjectunitmember(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjectunitmember> list = izgsprojectunitmemberservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunitmember info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunitmemberservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsProjectunitmembersend")
    public void encryptInitZgsProjectunitmembersend(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsProjectunitmembersend> list = izgsprojectunitmembersendservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunitmembersend info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunitmembersendservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPtempTj")
    public void encryptInitZgsPtempTj(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPtempTj> list = izgsptemptjservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPtempTj info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsptemptjservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsPzMattermaterial")
    public void encryptInitZgsPzMattermaterial(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsPzMattermaterial> list = izgspzmattermaterialservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPzMattermaterial info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgspzmattermaterialservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportAreacodeValues")
    public void encryptInitZgsReportAreacodeValues(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportAreacodeValues> list = izgsreportareacodevaluesservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportAreacodeValues info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportareacodevaluesservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportEnergyExistinfo")
    public void encryptInitZgsReportEnergyExistinfo(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportEnergyExistinfo> list = izgsreportenergyexistinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyExistinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergyexistinfoservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportEnergyExistinfoTotal")
    public void encryptInitZgsReportEnergyExistinfoTotal(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportEnergyExistinfoTotal> list = izgsreportenergyexistinfototalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyExistinfoTotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergyexistinfototalservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportEnergyNewinfo")
    public void encryptInitZgsReportEnergyNewinfo(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportEnergyNewinfo> list = izgsreportenergynewinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfoservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportEnergyNewinfoGreentotal")
    public void encryptInitZgsReportEnergyNewinfoGreentotal(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportEnergyNewinfoGreentotal> list = izgsreportenergynewinfogreentotalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfoGreentotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfogreentotalservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportEnergyNewinfoTotal")
    public void encryptInitZgsReportEnergyNewinfoTotal(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportEnergyNewinfoTotal> list = izgsreportenergynewinfototalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfoTotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfototalservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportMonthfabrAreaCityCompany")
    public void encryptInitZgsReportMonthfabrAreaCityCompany(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportMonthfabrAreaCityCompany> list = izgsreportmonthfabrareacitycompanyservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityCompany info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitycompanyservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportMonthfabrAreaCityCompleted")
    public void encryptInitZgsReportMonthfabrAreaCityCompleted(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportMonthfabrAreaCityCompleted> list = izgsreportmonthfabrareacitycompletedservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityCompleted info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitycompletedservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportMonthfabrAreaCityNewconstruction")
    public void encryptInitZgsReportMonthfabrAreaCityNewconstruction(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportMonthfabrAreaCityNewconstruction> list = izgsreportmonthfabrareacitynewconstructionservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityNewconstruction info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitynewconstructionservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportMonthfabrAreaCityProductioncapacity")
    public void encryptInitZgsReportMonthfabrAreaCityProductioncapacity(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = izgsreportmonthfabrareacityproductioncapacityservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityProductioncapacity info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacityproductioncapacityservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportMonthfabrAreaCityProject")
    public void encryptInitZgsReportMonthfabrAreaCityProject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportMonthfabrAreaCityProject> list = izgsreportmonthfabrareacityprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityProject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacityprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReportProject")
    public void encryptInitZgsReportProject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReportProject> list = izgsreportprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportProject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsReturnrecord")
    public void encryptInitZgsReturnrecord(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsReturnrecord> list = izgsreturnrecordservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReturnrecord info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreturnrecordservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSacceptcertexpert")
    public void encryptInitZgsSacceptcertexpert(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSacceptcertexpert> list = izgssacceptcertexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertexpertservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSacceptcertificatebase")
    public void encryptInitZgsSacceptcertificatebase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSacceptcertificatebase> list = izgssacceptcertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertificatebaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSacceptcertprojectfinish")
    public void encryptInitZgsSacceptcertprojectfinish(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSacceptcertprojectfinish> list = izgssacceptcertprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertprojectfinishservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSacceptcertresearcher")
    public void encryptInitZgsSacceptcertresearcher(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            //1、手动修改填补数据
            queryWrapper.eq("baseguid", "036f20ee-a81c-47b5-9031-6c33492d0663");
            //2、修改数据加密字段标识为null
            //
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSacceptcertresearcher> list = izgssacceptcertresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertresearcherservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSaexpert")
    public void encryptInitZgsSaexpert(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSaexpert> list = izgssaexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSaexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssaexpertservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSamplebuildproject")
    public void encryptInitZgsSamplebuildproject(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSamplebuildproject> list = izgssamplebuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSamplebuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssamplebuildprojectservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSamplerojectmainparticipant")
    public void encryptInitZgsSamplerojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSamplerojectmainparticipant> list = izgssamplerojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSamplerojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssamplerojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSciencefundbudget")
    public void encryptInitZgsSciencefundbudget(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSciencefundbudget> list = izgssciencefundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencefundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencefundbudgetservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSciencejointunit")
    public void encryptInitZgsSciencejointunit(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSciencejointunit> list = izgssciencejointunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencejointunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencejointunitservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScienceparticipant")
    public void encryptInitZgsScienceparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("sciencebaseguid", "44ce2116-136d-420a-9f0a-b3c103c6246b");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScienceparticipant> list = izgsscienceparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScienceparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscienceparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScienceplanarrange")
    public void encryptInitZgsScienceplanarrange(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScienceplanarrange> list = izgsscienceplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScienceplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscienceplanarrangeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSciencetechfeasible")
    public void encryptInitZgsSciencetechfeasible(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSciencetechfeasible> list = izgssciencetechfeasibleservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencetechfeasible info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencetechfeasibleservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSciencetechtask")
    public void encryptInitZgsSciencetechtask(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSciencetechtask> list = izgssciencetechtaskservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencetechtask info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencetechtaskservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificbase")
    public void encryptInitZgsScientificbase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificbase> list = izgsscientificbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificbaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificexpenditureclear")
    public void encryptInitZgsScientificexpenditureclear(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificexpenditureclear> list = izgsscientificexpenditureclearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificexpenditureclear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificexpenditureclearservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpexecutivelist")
    public void encryptInitZgsScientificpexecutivelist(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpexecutivelist> list = izgsscientificpexecutivelistservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpexecutivelist info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpexecutivelistservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostbase")
    public void encryptInitZgsScientificpostbase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostbase> list = izgsscientificpostbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostbaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostelist")
    public void encryptInitZgsScientificpostelist(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostelist> list = izgsscientificpostelistservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostelist info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostelistservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostexpendclear")
    public void encryptInitZgsScientificpostexpendclear(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostexpendclear> list = izgsscientificpostexpendclearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostexpendclear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostexpendclearservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostprojectexecut")
    public void encryptInitZgsScientificpostprojectexecut(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostprojectexecut> list = izgsscientificpostprojectexecutservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostprojectexecut info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostprojectexecutservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostprojectfinish")
    public void encryptInitZgsScientificpostprojectfinish(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostprojectfinish> list = izgsscientificpostprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostprojectfinishservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificpostresearcher")
    public void encryptInitZgsScientificpostresearcher(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "6ef8a2e6-2176-47f3-860d-abb076cdf232");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificpostresearcher> list = izgsscientificpostresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostresearcherservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificprojectexecutive")
    public void encryptInitZgsScientificprojectexecutive(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificprojectexecutive> list = izgsscientificprojectexecutiveservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificprojectexecutive info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificprojectexecutiveservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificprojectfinish")
    public void encryptInitZgsScientificprojectfinish(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificprojectfinish> list = izgsscientificprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificprojectfinishservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsScientificresearcher")
    public void encryptInitZgsScientificresearcher(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "5c072882-7f50-4d64-8ba9-79b3e79dc6e3");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsScientificresearcher> list = izgsscientificresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificresearcherservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSmsSendingRecordLog")
    public void encryptInitZgsSmsSendingRecordLog(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSmsSendingRecordLog> list = izgssmssendingrecordlogservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSmsSendingRecordLog info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssmssendingrecordlogservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSofttechexpert")
    public void encryptInitZgsSofttechexpert(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSofttechexpert> list = izgssofttechexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSofttechexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssofttechexpertservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSpostcertificatebase")
    public void encryptInitZgsSpostcertificatebase(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSpostcertificatebase> list = izgsspostcertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostcertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostcertificatebaseservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSpostprojectfinish")
    public void encryptInitZgsSpostprojectfinish(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "869c4562-04bf-43ad-a5ba-2ac9e2e36acd");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSpostprojectfinish> list = izgsspostprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostprojectfinishservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsSpostresearcher")
    public void encryptInitZgsSpostresearcher(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "c0655b0c-1b2d-4c0f-bc4f-916cb3d586df");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsSpostresearcher> list = izgsspostresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostresearcherservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsTaskbuildfundbudget")
    public void encryptInitZgsTaskbuildfundbudget(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsTaskbuildfundbudget> list = izgstaskbuildfundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskbuildfundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskbuildfundbudgetservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsTaskchange")
    public void encryptInitZgsTaskchange(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsTaskchange> list = izgstaskchangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskchange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskchangeservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsTaskprojectmainparticipant")
    public void encryptInitZgsTaskprojectmainparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsTaskprojectmainparticipant> list = izgstaskprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskprojectmainparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitZgsTaskscienceparticipant")
    public void encryptInitZgsTaskscienceparticipant(@RequestParam("flag") boolean flag) {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("taskguid", "79919d2a-35be-4606-a996-d9f7f6024ed9");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List<ZgsTaskscienceparticipant> list = izgstaskscienceparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskscienceparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskscienceparticipantservice.updateById(info);
            }
        }
    }

    @GetMapping("/encryptInitSysUser")
    public void encryptInitSysUser(@RequestParam("flag") boolean flag) {
        // QueryWrapper queryWrapper = new QueryWrapper();
        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        StringBuilder sb = new StringBuilder();
        /*if (isFlag) {
            queryWrapper.isNull("is_encrypt");
            sb.append(" or (is_encrypt IS NULL and del_flag = 1)");
        } else {
            sb.append(" or del_flag = 1");
        }*/

         sb.append(" or (is_encrypt = 1 and del_flag = 1)");
         queryWrapper.last(sb.toString());

        List<SysUser> list = sysUserService.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                SysUser info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                sysUserService.updateSysUserEncryptDecryptEcb(info);
            }
        }
    }




}
