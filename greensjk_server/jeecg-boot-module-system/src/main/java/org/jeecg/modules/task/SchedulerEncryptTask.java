package org.jeecg.modules.task;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedchangeparticipant;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsTaskchange;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedchangeparticipantService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsTaskchangeService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancescore;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancescoreService;
import org.jeecg.modules.green.kyxmjtsq.entity.*;
import org.jeecg.modules.green.kyxmjtsq.service.*;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostprojectfinish;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostresearcher;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostprojectfinishService;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostresearcherService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencefundbudget;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencejointunit;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceparticipant;
import org.jeecg.modules.green.kyxmsb.entity.ZgsScienceplanarrange;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencefundbudgetService;
import org.jeecg.modules.green.kyxmsb.service.IZgsSciencejointunitService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceparticipantService;
import org.jeecg.modules.green.kyxmsb.service.IZgsScienceplanarrangeService;
import org.jeecg.modules.green.report.entity.*;
import org.jeecg.modules.green.report.service.*;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.sms.entity.ZgsSmsSendingRecordLog;
import org.jeecg.modules.green.sms.service.IZgsSmsSendingRecordLogService;
import org.jeecg.modules.green.task.entity.ZgsTaskbuildfundbudget;
import org.jeecg.modules.green.task.entity.ZgsTaskprojectmainparticipant;
import org.jeecg.modules.green.task.entity.ZgsTaskscienceparticipant;
import org.jeecg.modules.green.task.service.IZgsTaskbuildfundbudgetService;
import org.jeecg.modules.green.task.service.IZgsTaskprojectmainparticipantService;
import org.jeecg.modules.green.task.service.IZgsTaskscienceparticipantService;
import org.jeecg.modules.green.xmyssq.entity.*;
import org.jeecg.modules.green.xmyssq.service.*;
import org.jeecg.modules.green.xmyszssq.entity.*;
import org.jeecg.modules.green.xmyszssq.service.*;
import org.jeecg.modules.green.zjksb.entity.*;
import org.jeecg.modules.green.zjksb.service.*;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ASUS
 * @date 2018/7/25
 */
@Slf4j
@Component
public class SchedulerEncryptTask {
    @Autowired
    private IZgsAssembleprojectService izgsassembleprojectservice;
    @Autowired
    private IZgsAssemblesingleprojectService izgsassemblesingleprojectservice;
    @Autowired
    private IZgsAssemprojectmainparticipantService izgsassemprojectmainparticipantservice;
    @Autowired
    private IZgsAttachappendixService izgsattachappendixservice;
    @Autowired
    private IZgsBuildfundbudgetService izgsbuildfundbudgetservice;
    @Autowired
    private IZgsBuildjointunitService izgsbuildjointunitservice;
    @Autowired
    private IZgsBuildprojectService izgsbuildprojectservice;
    @Autowired
    private IZgsBuildrojectmainparticipantService izgsbuildrojectmainparticipantservice;
    @Autowired
    private IZgsDemoprojectacceptanceService izgsdemoprojectacceptanceservice;
    @Autowired
    private IZgsDemoprojectapplicationService izgsdemoprojectapplicationservice;
    @Autowired
    private IZgsEnergybuildprojectService izgsenergybuildprojectservice;
    @Autowired
    private IZgsEnergyprojectmainparticipantService izgsenergyprojectmainparticipantservice;
    @Autowired
    private IZgsEnergyprojectplanarrangeService izgsenergyprojectplanarrangeservice;
    @Autowired
    private IZgsExamplecertificatebaseService izgsexamplecertificatebaseservice;
    @Autowired
    private IZgsExamplecertificateexpertService izgsexamplecertificateexpertservice;
    @Autowired
    private IZgsExamplecertimplementunitService izgsexamplecertimplementunitservice;
    @Autowired
    private IZgsExamplecertparticipantService izgsexamplecertparticipantservice;
    @Autowired
    private IZgsExpertawardService izgsexpertawardservice;
    @Autowired
    private IZgsExpertinfoService izgsexpertinfoservice;
    @Autowired
    private IZgsExpertmonographService izgsexpertmonographservice;
    @Autowired
    private IZgsExpertpaperService izgsexpertpaperservice;
    @Autowired
    private IZgsExpertpatentService izgsexpertpatentservice;
    @Autowired
    private IZgsExpertprojectbearService izgsexpertprojectbearservice;
    @Autowired
    private IZgsExpertsetService izgsexpertsetservice;
    @Autowired
    private IZgsExpertworkresumeService izgsexpertworkresumeservice;
    @Autowired
    private IZgsGreenbuildprojectService izgsgreenbuildprojectservice;
    @Autowired
    private IZgsGreenprojectmainparticipantService izgsgreenprojectmainparticipantservice;
    @Autowired
    private IZgsGreenprojectplanarrangeService izgsgreenprojectplanarrangeservice;
    @Autowired
    private IZgsIndexFileService izgsindexfileservice;
    @Autowired
    private IZgsMattermaterialService izgsmattermaterialservice;
    @Autowired
    private IZgsMidterminspectionService izgsmidterminspectionservice;
    @Autowired
    private IZgsPerformancebaseService izgsperformancebaseservice;
    @Autowired
    private IZgsPerformancescoreService izgsperformancescoreservice;
    @Autowired
    private IZgsPlannedchangeparticipantService izgsplannedchangeparticipantservice;
    @Autowired
    private IZgsPlannedprojectchangeService izgsplannedprojectchangeservice;
    @Autowired
    private IZgsPlannedprojectchangedetailService izgsplannedprojectchangedetailservice;
    @Autowired
    private IZgsPlanresultbaseService izgsplanresultbaseservice;
    @Autowired
    private IZgsProjectlibraryService izgsprojectlibraryservice;
    @Autowired
    private IZgsProjectlibraryexpertService izgsprojectlibraryexpertservice;
    @Autowired
    private IZgsProjecttaskService izgsprojecttaskservice;
    @Autowired
    private IZgsProjectunithistoryService izgsprojectunithistoryservice;
    @Autowired
    private IZgsProjectunitmemberService izgsprojectunitmemberservice;
    @Autowired
    private IZgsProjectunitmembersendService izgsprojectunitmembersendservice;
    @Autowired
    private IZgsPtempTjService izgsptemptjservice;
    @Autowired
    private IZgsPzMattermaterialService izgspzmattermaterialservice;
    @Autowired
    private IZgsReportAreacodeValuesService izgsreportareacodevaluesservice;
    @Autowired
    private IZgsReportEnergyExistinfoService izgsreportenergyexistinfoservice;
    @Autowired
    private IZgsReportEnergyExistinfoTotalService izgsreportenergyexistinfototalservice;
    @Autowired
    private IZgsReportEnergyNewinfoService izgsreportenergynewinfoservice;
    @Autowired
    private IZgsReportEnergyNewinfoGreentotalService izgsreportenergynewinfogreentotalservice;
    @Autowired
    private IZgsReportEnergyNewinfoTotalService izgsreportenergynewinfototalservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompanyService izgsreportmonthfabrareacitycompanyservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityCompletedService izgsreportmonthfabrareacitycompletedservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityNewconstructionService izgsreportmonthfabrareacitynewconstructionservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityProductioncapacityService izgsreportmonthfabrareacityproductioncapacityservice;
    @Autowired
    private IZgsReportMonthfabrAreaCityProjectService izgsreportmonthfabrareacityprojectservice;
    @Autowired
    private IZgsReportProjectService izgsreportprojectservice;
    @Autowired
    private IZgsReturnrecordService izgsreturnrecordservice;
    @Autowired
    private IZgsSacceptcertexpertService izgssacceptcertexpertservice;
    @Autowired
    private IZgsSacceptcertificatebaseService izgssacceptcertificatebaseservice;
    @Autowired
    private IZgsSacceptcertprojectfinishService izgssacceptcertprojectfinishservice;
    @Autowired
    private IZgsSacceptcertresearcherService izgssacceptcertresearcherservice;
    @Autowired
    private IZgsSaexpertService izgssaexpertservice;
    @Autowired
    private IZgsSamplebuildprojectService izgssamplebuildprojectservice;
    @Autowired
    private IZgsSamplerojectmainparticipantService izgssamplerojectmainparticipantservice;
    @Autowired
    private IZgsSciencefundbudgetService izgssciencefundbudgetservice;
    @Autowired
    private IZgsSciencejointunitService izgssciencejointunitservice;
    @Autowired
    private IZgsScienceparticipantService izgsscienceparticipantservice;
    @Autowired
    private IZgsScienceplanarrangeService izgsscienceplanarrangeservice;
    @Autowired
    private IZgsSciencetechfeasibleService izgssciencetechfeasibleservice;
    @Autowired
    private IZgsSciencetechtaskService izgssciencetechtaskservice;
    @Autowired
    private IZgsScientificbaseService izgsscientificbaseservice;
    @Autowired
    private IZgsScientificexpenditureclearService izgsscientificexpenditureclearservice;
    @Autowired
    private IZgsScientificpexecutivelistService izgsscientificpexecutivelistservice;
    @Autowired
    private IZgsScientificpostbaseService izgsscientificpostbaseservice;
    @Autowired
    private IZgsScientificpostelistService izgsscientificpostelistservice;
    @Autowired
    private IZgsScientificpostexpendclearService izgsscientificpostexpendclearservice;
    @Autowired
    private IZgsScientificpostprojectexecutService izgsscientificpostprojectexecutservice;
    @Autowired
    private IZgsScientificpostprojectfinishService izgsscientificpostprojectfinishservice;
    @Autowired
    private IZgsScientificpostresearcherService izgsscientificpostresearcherservice;
    @Autowired
    private IZgsScientificprojectexecutiveService izgsscientificprojectexecutiveservice;
    @Autowired
    private IZgsScientificprojectfinishService izgsscientificprojectfinishservice;
    @Autowired
    private IZgsScientificresearcherService izgsscientificresearcherservice;
    @Autowired
    private IZgsSmsSendingRecordLogService izgssmssendingrecordlogservice;
    @Autowired
    private IZgsSofttechexpertService izgssofttechexpertservice;
    @Autowired
    private IZgsSpostcertificatebaseService izgsspostcertificatebaseservice;
    @Autowired
    private IZgsSpostprojectfinishService izgsspostprojectfinishservice;
    @Autowired
    private IZgsSpostresearcherService izgsspostresearcherservice;
    @Autowired
    private IZgsTaskbuildfundbudgetService izgstaskbuildfundbudgetservice;
    @Autowired
    private IZgsTaskchangeService izgstaskchangeservice;
    @Autowired
    private IZgsTaskprojectmainparticipantService izgstaskprojectmainparticipantservice;
    @Autowired
    private IZgsTaskscienceparticipantService izgstaskscienceparticipantservice;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private RedisUtil redisUtil;

    private static boolean isEncrypt = true;
    private static boolean isFlag = true;

//    @Scheduled(cron = "0 13 17 * * *")
//    @Scheduled(cron = "0 10 15 * * *")
//    @Scheduled(cron = "0 57 01 * * *")
//    @Scheduled(cron = "0 0/1 * * * ?")  // 每两分钟执行一次
//    @Scheduled(cron = "0 0 1 * * ?")
//    @Scheduled(fixedDelay = 3600000)
    public void initAllEncryptData() {
        log.info("encryptData-ready");
        //无需处理start
//        encryptInitZgsMattermaterial();
//        encryptInitZgsPzMattermaterial();
//        encryptInitZgsReportAreacodeValues();
//        encryptInitZgsReportProject();
//        encryptInitZgsReportMonthfabrAreaCityNewconstruction();
        //无需处理end
//        if (redisUtil.get("initDataEncrypt_N") == null && isEncrypt) {
        if (isFlag) {
            log.info("encryptData-start");
              // isEncrypt = false;
//            redisUtil.set("initDataEncrypt_N", "test");
//          encryptInitZgsAttachappendix();
            encryptInitZgsAssembleproject();   // 开始解密(第一个)
            encryptInitZgsAssemblesingleproject();
            encryptInitZgsAssemprojectmainparticipant();
            encryptInitZgsBuildfundbudget();
            encryptInitZgsBuildjointunit();
            encryptInitZgsBuildproject();
            encryptInitZgsBuildrojectmainparticipant();
            encryptInitZgsDemoprojectacceptance();
            encryptInitZgsDemoprojectapplication();
            encryptInitZgsEnergybuildproject();
            encryptInitZgsEnergyprojectmainparticipant();
            encryptInitZgsEnergyprojectplanarrange();
            encryptInitZgsExamplecertificatebase();
            encryptInitZgsExamplecertificateexpert();
            encryptInitZgsExamplecertimplementunit();
            encryptInitZgsExamplecertparticipant();
            encryptInitZgsExpertaward();
            encryptInitZgsExpertinfo();
            encryptInitZgsExpertmonograph();
            encryptInitZgsExpertpaper();
            encryptInitZgsExpertpatent();
            encryptInitZgsExpertprojectbear();
            encryptInitZgsExpertset();
            encryptInitZgsExpertworkresume();
            encryptInitZgsGreenbuildproject();
            encryptInitZgsGreenprojectmainparticipant();
            encryptInitZgsGreenprojectplanarrange();
            encryptInitZgsIndexFile();
            encryptInitZgsMidterminspection();
            encryptInitZgsPerformancebase();
            encryptInitZgsPerformancescore();
            encryptInitZgsPlannedchangeparticipant();
            encryptInitZgsPlannedprojectchange();
            encryptInitZgsPlannedprojectchangedetail();
            encryptInitZgsPlanresultbase();
            encryptInitZgsProjectlibrary();
            encryptInitZgsProjectlibraryexpert();
            encryptInitZgsProjecttask();
            encryptInitZgsProjectunithistory();
            encryptInitZgsProjectunitmember();
            encryptInitZgsProjectunitmembersend();
            encryptInitZgsPtempTj();
            encryptInitZgsReportEnergyExistinfo();
            encryptInitZgsReportEnergyExistinfoTotal();
            encryptInitZgsReportEnergyNewinfo();
            encryptInitZgsReportEnergyNewinfoGreentotal();
            encryptInitZgsReportEnergyNewinfoTotal();
            encryptInitZgsReportMonthfabrAreaCityCompany();
            encryptInitZgsReportMonthfabrAreaCityCompleted();
            encryptInitZgsReportMonthfabrAreaCityProductioncapacity();
            encryptInitZgsReportMonthfabrAreaCityProject();
            encryptInitZgsReturnrecord();
            encryptInitZgsSacceptcertexpert();
            encryptInitZgsSacceptcertificatebase();
            encryptInitZgsSacceptcertprojectfinish();
            encryptInitZgsSacceptcertresearcher();
            encryptInitZgsSaexpert();
            encryptInitZgsSamplebuildproject();
            encryptInitZgsSamplerojectmainparticipant();
            encryptInitZgsSciencefundbudget();
            encryptInitZgsSciencejointunit();
            encryptInitZgsScienceparticipant();
            encryptInitZgsScienceplanarrange();
            encryptInitZgsSciencetechfeasible();
            encryptInitZgsSciencetechtask();
            encryptInitZgsScientificbase();
            encryptInitZgsScientificexpenditureclear();
            encryptInitZgsScientificpexecutivelist();
            encryptInitZgsScientificpostbase();
            encryptInitZgsScientificpostelist();
            encryptInitZgsScientificpostexpendclear();
            encryptInitZgsScientificpostprojectexecut();
            encryptInitZgsScientificpostprojectfinish();
            encryptInitZgsScientificpostresearcher();
            encryptInitZgsScientificprojectexecutive();
            encryptInitZgsScientificprojectfinish();
            encryptInitZgsScientificresearcher();
            encryptInitZgsSmsSendingRecordLog();
            encryptInitZgsSofttechexpert();
            encryptInitZgsSpostcertificatebase();
            encryptInitZgsSpostprojectfinish();
            encryptInitZgsSpostresearcher();
            encryptInitZgsTaskbuildfundbudget();
            encryptInitZgsTaskchange();
            encryptInitZgsTaskprojectmainparticipant();
            encryptInitZgsTaskscienceparticipant();
//            encryptInitSysUser();
            log.info("encryptData-end");
        }
        log.info("encryptData-over");
        // 只完成一次调度解密
        isFlag = false;
    }

    // 判断是否加密工具类
    private static QueryWrapper getQueryByEncrypt() {
        QueryWrapper queryWrapper = new QueryWrapper();
        // 根据is_encrypt 字段判断加密状态
        if (!isEncrypt) {
            // 已加密的结果集
            queryWrapper.eq("is_encrypt", 1);
        }else{
            // 未加密的结果集
            queryWrapper.isNull("is_encrypt");
        }
        return queryWrapper;
    }


    private void encryptInitZgsAssembleproject() {
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsAssembleproject> list = izgsassembleprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssembleproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassembleprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsAssemblesingleproject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsAssemblesingleproject> list = izgsassemblesingleprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssemblesingleproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassemblesingleprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsAssemprojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsAssemprojectmainparticipant> list = izgsassemprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAssemprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsassemprojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsAttachappendix() {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }
        List<ZgsAttachappendix> list = izgsattachappendixservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsAttachappendix info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsattachappendixservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsBuildfundbudget() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsBuildfundbudget> list = izgsbuildfundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildfundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildfundbudgetservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsBuildjointunit() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsBuildjointunit> list = izgsbuildjointunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildjointunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildjointunitservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsBuildproject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsBuildproject> list = izgsbuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsBuildrojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsBuildrojectmainparticipant> list = izgsbuildrojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsBuildrojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsbuildrojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsDemoprojectacceptance() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsDemoprojectacceptance> list = izgsdemoprojectacceptanceservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsDemoprojectacceptance info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsdemoprojectacceptanceservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsDemoprojectapplication() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsDemoprojectapplication> list = izgsdemoprojectapplicationservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsDemoprojectapplication info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsdemoprojectapplicationservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsEnergybuildproject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsEnergybuildproject> list = izgsenergybuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergybuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergybuildprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsEnergyprojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsEnergyprojectmainparticipant> list = izgsenergyprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergyprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergyprojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsEnergyprojectplanarrange() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsEnergyprojectplanarrange> list = izgsenergyprojectplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsEnergyprojectplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsenergyprojectplanarrangeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExamplecertificatebase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExamplecertificatebase> list = izgsexamplecertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertificatebaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExamplecertificateexpert() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExamplecertificateexpert> list = izgsexamplecertificateexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertificateexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertificateexpertservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExamplecertimplementunit() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExamplecertimplementunit> list = izgsexamplecertimplementunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertimplementunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertimplementunitservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExamplecertparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "dabab5e0-f8fc-40a9-9be2-c0d925b6ca23");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExamplecertparticipant> list = izgsexamplecertparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExamplecertparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexamplecertparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertaward() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertaward> list = izgsexpertawardservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertaward info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertawardservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertinfo() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertinfo> list = izgsexpertinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertinfoservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertmonograph() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertmonograph> list = izgsexpertmonographservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertmonograph info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertmonographservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertpaper() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertpaper> list = izgsexpertpaperservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertpaper info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertpaperservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertpatent() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertpatent> list = izgsexpertpatentservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertpatent info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertpatentservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertprojectbear() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertprojectbear> list = izgsexpertprojectbearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertprojectbear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertprojectbearservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertset() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertset> list = izgsexpertsetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertset info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertsetservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsExpertworkresume() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsExpertworkresume> list = izgsexpertworkresumeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsExpertworkresume info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsexpertworkresumeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsGreenbuildproject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsGreenbuildproject> list = izgsgreenbuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenbuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenbuildprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsGreenprojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsGreenprojectmainparticipant> list = izgsgreenprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenprojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsGreenprojectplanarrange() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsGreenprojectplanarrange> list = izgsgreenprojectplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsGreenprojectplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsgreenprojectplanarrangeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsIndexFile() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsIndexFile> list = izgsindexfileservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsIndexFile info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsindexfileservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsMattermaterial() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsMattermaterial> list = izgsmattermaterialservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsMattermaterial info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsmattermaterialservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsMidterminspection() {
       /* QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsMidterminspection> list = izgsmidterminspectionservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsMidterminspection info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsmidterminspectionservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPerformancebase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPerformancebase> list = izgsperformancebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPerformancebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsperformancebaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPerformancescore() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPerformancescore> list = izgsperformancescoreservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPerformancescore info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsperformancescoreservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPlannedchangeparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPlannedchangeparticipant> list = izgsplannedchangeparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedchangeparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedchangeparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPlannedprojectchange() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPlannedprojectchange> list = izgsplannedprojectchangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedprojectchange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedprojectchangeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPlannedprojectchangedetail() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPlannedprojectchangedetail> list = izgsplannedprojectchangedetailservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlannedprojectchangedetail info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplannedprojectchangedetailservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPlanresultbase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPlanresultbase> list = izgsplanresultbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPlanresultbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsplanresultbaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjectlibrary() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjectlibrary> list = izgsprojectlibraryservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectlibrary info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectlibraryservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjectlibraryexpert() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjectlibraryexpert> list = izgsprojectlibraryexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectlibraryexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectlibraryexpertservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjecttask() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjecttask> list = izgsprojecttaskservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjecttask info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojecttaskservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjectunithistory() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjectunithistory> list = izgsprojectunithistoryservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunithistory info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunithistoryservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjectunitmember() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjectunitmember> list = izgsprojectunitmemberservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunitmember info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunitmemberservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsProjectunitmembersend() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsProjectunitmembersend> list = izgsprojectunitmembersendservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsProjectunitmembersend info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsprojectunitmembersendservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPtempTj() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPtempTj> list = izgsptemptjservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPtempTj info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsptemptjservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsPzMattermaterial() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsPzMattermaterial> list = izgspzmattermaterialservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsPzMattermaterial info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgspzmattermaterialservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportAreacodeValues() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportAreacodeValues> list = izgsreportareacodevaluesservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportAreacodeValues info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportareacodevaluesservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportEnergyExistinfo() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportEnergyExistinfo> list = izgsreportenergyexistinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyExistinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergyexistinfoservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportEnergyExistinfoTotal() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportEnergyExistinfoTotal> list = izgsreportenergyexistinfototalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyExistinfoTotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergyexistinfototalservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportEnergyNewinfo() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportEnergyNewinfo> list = izgsreportenergynewinfoservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfo info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfoservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportEnergyNewinfoGreentotal() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportEnergyNewinfoGreentotal> list = izgsreportenergynewinfogreentotalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfoGreentotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfogreentotalservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportEnergyNewinfoTotal() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportEnergyNewinfoTotal> list = izgsreportenergynewinfototalservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportEnergyNewinfoTotal info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportenergynewinfototalservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportMonthfabrAreaCityCompany() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportMonthfabrAreaCityCompany> list = izgsreportmonthfabrareacitycompanyservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityCompany info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitycompanyservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportMonthfabrAreaCityCompleted() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportMonthfabrAreaCityCompleted> list = izgsreportmonthfabrareacitycompletedservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityCompleted info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitycompletedservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportMonthfabrAreaCityNewconstruction() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportMonthfabrAreaCityNewconstruction> list = izgsreportmonthfabrareacitynewconstructionservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityNewconstruction info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacitynewconstructionservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportMonthfabrAreaCityProductioncapacity() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportMonthfabrAreaCityProductioncapacity> list = izgsreportmonthfabrareacityproductioncapacityservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityProductioncapacity info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacityproductioncapacityservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportMonthfabrAreaCityProject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportMonthfabrAreaCityProject> list = izgsreportmonthfabrareacityprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportMonthfabrAreaCityProject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportmonthfabrareacityprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReportProject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReportProject> list = izgsreportprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReportProject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreportprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsReturnrecord() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsReturnrecord> list = izgsreturnrecordservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsReturnrecord info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsreturnrecordservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSacceptcertexpert() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSacceptcertexpert> list = izgssacceptcertexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertexpertservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSacceptcertificatebase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSacceptcertificatebase> list = izgssacceptcertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertificatebaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSacceptcertprojectfinish() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSacceptcertprojectfinish> list = izgssacceptcertprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertprojectfinishservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSacceptcertresearcher() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            //1、手动修改填补数据
            queryWrapper.eq("baseguid", "036f20ee-a81c-47b5-9031-6c33492d0663");
            //2、修改数据加密字段标识为null
            //
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSacceptcertresearcher> list = izgssacceptcertresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSacceptcertresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssacceptcertresearcherservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSaexpert() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSaexpert> list = izgssaexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSaexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssaexpertservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSamplebuildproject() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSamplebuildproject> list = izgssamplebuildprojectservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSamplebuildproject info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssamplebuildprojectservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSamplerojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSamplerojectmainparticipant> list = izgssamplerojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSamplerojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssamplerojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSciencefundbudget() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSciencefundbudget> list = izgssciencefundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencefundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencefundbudgetservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSciencejointunit() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSciencejointunit> list = izgssciencejointunitservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencejointunit info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencejointunitservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScienceparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("sciencebaseguid", "44ce2116-136d-420a-9f0a-b3c103c6246b");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScienceparticipant> list = izgsscienceparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScienceparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscienceparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScienceplanarrange() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScienceplanarrange> list = izgsscienceplanarrangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScienceplanarrange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscienceplanarrangeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSciencetechfeasible() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSciencetechfeasible> list = izgssciencetechfeasibleservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencetechfeasible info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencetechfeasibleservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSciencetechtask() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSciencetechtask> list = izgssciencetechtaskservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSciencetechtask info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssciencetechtaskservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificbase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificbase> list = izgsscientificbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificbaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificexpenditureclear() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificexpenditureclear> list = izgsscientificexpenditureclearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificexpenditureclear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificexpenditureclearservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpexecutivelist() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpexecutivelist> list = izgsscientificpexecutivelistservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpexecutivelist info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpexecutivelistservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostbase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostbase> list = izgsscientificpostbaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostbase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostbaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostelist() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostelist> list = izgsscientificpostelistservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostelist info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostelistservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostexpendclear() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostexpendclear> list = izgsscientificpostexpendclearservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostexpendclear info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostexpendclearservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostprojectexecut() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostprojectexecut> list = izgsscientificpostprojectexecutservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostprojectexecut info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostprojectexecutservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostprojectfinish() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostprojectfinish> list = izgsscientificpostprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostprojectfinishservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificpostresearcher() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "6ef8a2e6-2176-47f3-860d-abb076cdf232");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificpostresearcher> list = izgsscientificpostresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificpostresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificpostresearcherservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificprojectexecutive() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificprojectexecutive> list = izgsscientificprojectexecutiveservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificprojectexecutive info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificprojectexecutiveservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificprojectfinish() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificprojectfinish> list = izgsscientificprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificprojectfinishservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsScientificresearcher() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "5c072882-7f50-4d64-8ba9-79b3e79dc6e3");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsScientificresearcher> list = izgsscientificresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsScientificresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsscientificresearcherservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSmsSendingRecordLog() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSmsSendingRecordLog> list = izgssmssendingrecordlogservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSmsSendingRecordLog info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssmssendingrecordlogservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSofttechexpert() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSofttechexpert> list = izgssofttechexpertservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSofttechexpert info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgssofttechexpertservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSpostcertificatebase() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSpostcertificatebase> list = izgsspostcertificatebaseservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostcertificatebase info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostcertificatebaseservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSpostprojectfinish() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "869c4562-04bf-43ad-a5ba-2ac9e2e36acd");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSpostprojectfinish> list = izgsspostprojectfinishservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostprojectfinish info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostprojectfinishservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsSpostresearcher() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("baseguid", "c0655b0c-1b2d-4c0f-bc4f-916cb3d586df");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsSpostresearcher> list = izgsspostresearcherservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsSpostresearcher info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgsspostresearcherservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsTaskbuildfundbudget() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsTaskbuildfundbudget> list = izgstaskbuildfundbudgetservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskbuildfundbudget info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskbuildfundbudgetservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsTaskchange() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsTaskchange> list = izgstaskchangeservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskchange info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskchangeservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsTaskprojectmainparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsTaskprojectmainparticipant> list = izgstaskprojectmainparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskprojectmainparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskprojectmainparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitZgsTaskscienceparticipant() {
        /*QueryWrapper queryWrapper = new QueryWrapper();
        if (isFlag) {
//            queryWrapper.isNull("is_encrypt");
            queryWrapper.eq("taskguid", "79919d2a-35be-4606-a996-d9f7f6024ed9");
        }*/
        QueryWrapper queryWrapper = getQueryByEncrypt();
        List<ZgsTaskscienceparticipant> list = izgstaskscienceparticipantservice.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                ZgsTaskscienceparticipant info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                izgstaskscienceparticipantservice.updateById(info);
            }
        }
    }

    private void encryptInitSysUser() {
        QueryWrapper queryWrapper = new QueryWrapper();
        // QueryWrapper queryWrapper = getQueryByEncrypt();
        StringBuilder sb = new StringBuilder();
        if (isFlag) {
            queryWrapper.isNull("is_encrypt");
            sb.append(" or (is_encrypt IS NULL and del_flag = 1)");
        } else {
            sb.append(" or del_flag = 1");
        }

         // sb.append(" or (is_encrypt = 1 and del_flag = 1)");
         queryWrapper.last(sb.toString());

        List<SysUser> list = sysUserService.list(queryWrapper);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                SysUser info = list.get(i);
                if (info.getIsEncrypt() == null) {
                    info.setIsEncrypt(1);
                    info = ValidateEncryptEntityUtil.validateEncryptObject(info, ValidateEncryptEntityUtil.isEncrypt);
                } else {
                    info.setIsEncrypt(null);
                    info = ValidateEncryptEntityUtil.validateDecryptObject(info, ValidateEncryptEntityUtil.isDecrypt);
                }
                sysUserService.updateSysUserEncryptDecryptEcb(info);
            }
        }
    }


}
