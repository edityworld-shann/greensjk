package org.jeecg.modules.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;
import org.jeecg.modules.green.common.entity.ZgsProjectunithistory;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 个人、企业、专家、黑白名单控制实体类
 * </p>
 *
 * @Author scott
 * @since 2018-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserCommonInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    private String id;

    /**
     * 登录账号
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private String username;

    /**
     * 真实姓名
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private String realname;

    /**
     * 状态(1：白名单  2：黑名单 ）
     */
    @Excel(name = "状态", width = 15, dicCode = "user_status")
    @Dict(dicCode = "user_status")
    private Integer status;
    /**
     * 审核状态
     */
    @Dict(dicCode = "sfproject_status")
    private String statussp;
    /**
     * 登录账号类型：0单位，1个人，2专家库，3住建厅
     */
    @Dict(dicCode = "user_type")
    private Integer loginUserType;
    /**
     * 关联表主键
     */
    private String unitmemberid;
    /**
     * 项目单位ID
     */
    private java.lang.String enterpriseguid;
    /**
     * 项目单位名称/人员姓名（帐号类型为个人时填写）
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String enterprisename;
    /**
     * 统一社会信用编码18位或组织机构代码无横线9位
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String unifiedcreditencode;
    /**
     * 单位地址
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String address;
    /**
     * 单位联系人
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String linkuser;

    /**
     * 性别（1：男 2：女）
     */
    private Integer sex;
    /**
     * 单位联系电话
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String phone;

    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String mobilephone;
    /**
     * 传真
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String fax;
    /**
     * 电子邮箱
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String email;
    /**
     * 单位法定代表人
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalperson;
    /**
     * 单位法定代表人身份证号，帐号类型个人时的个人身份证号
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String legalpersonidcard;
    /**
     * 单位所属区域
     */
    private java.lang.String cityname;

    /**
     * 上级推荐单位（市州建设局或省直单位）
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String higherupunit;

    /**
     * 聘用单位名称，帐号类型为个人时填写
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String employunit;
    /**
     * 就职部门*，帐号类型为个人时填写
     */
    @ValidateEncryptEntity(isEncrypt = true)
    private java.lang.String department;

    private List<ZgsProjectunithistory> zgsProjectunithistoryList;

    //营业执照图片名称
    private java.lang.String appendixname;
    //营业执照图片路径
    private java.lang.String appendixpath;
    //行政区划码表
    private java.lang.String areacode;
    //推荐单位
    private java.lang.String higherupunitnum;
    //初审形审按钮控制
    private Integer spStatus;
}
