package org.jeecg.modules.system.controller;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.aliyuncs.exceptions.ClientException;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.JsonObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.codec.Base64;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.httputil.HttpHeader;
import org.jeecg.common.httputil.HttpParamers;
import org.jeecg.common.httputil.HttpService;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.EncryptedString;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.base.service.BaseCommonService;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmember;
import org.jeecg.modules.green.common.entity.ZgsProjectunitmembersend;
import org.jeecg.modules.green.common.service.IZgsProjectunitmemberService;
import org.jeecg.modules.green.common.service.IZgsProjectunitmembersendService;
import org.jeecg.modules.system.entity.*;
import org.jeecg.modules.system.model.SysLoginModel;
import org.jeecg.modules.system.service.*;
import org.jeecg.modules.system.util.RSACoder;
import org.jeecg.modules.system.util.RandImageUtil;
import org.jeecg.modules.utils.SMSTokenUtil;
import org.jeecg.modules.utils.SendSmsInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @Author scott
 * @since 2018-12-17
 */
@RestController
@RequestMapping("/sys")
@Api(tags = "用户登录")
@Slf4j
public class LoginController {
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysBaseAPI sysBaseAPI;
    @Autowired
    private ISysLogService logService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private ISysTenantService sysTenantService;
    @Autowired
    private ISysDictService sysDictService;
    @Resource
    private BaseCommonService baseCommonService;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Value("${jeecg.path.downloadUrl}")
    private String downloadUrl;
    @Value("${jeecg.systemUrl}")
    private String systemUrl;
    @Value("${jeecg.loginBaseUrl}")
    private String loginBaseUrl;
    @Value("${jeecg.clientId}")
    private String clientId;
    @Value("${jeecg.clientSecret}")
    private String clientSecret;
    @Value("${jeecg.loginCallBackUrl}")
    private String loginCallBackUrl;
    private static final String BASE_CHECK_CODES = "qwertyuiplkjhgfdsazxcvbnmQWERTYUPLKJHGFDSAZXCVBNM1234567890";
    @Autowired
    private SMSTokenUtil smsTokenUtil;
    @Autowired
    private IZgsProjectunitmembersendService zgsProjectunitmembersendService;

    @ApiOperation("登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result<JSONObject> login(@RequestBody SysLoginModel sysLoginModel) {
        Result<JSONObject> result = new Result<JSONObject>();
//        String username = sysLoginModel.getUsername();
//        String password = sysLoginModel.getPassword();
        String username = new String(RSACoder.decryptPrivateKey(java.util.Base64.getDecoder().decode(sysLoginModel.getUsername()), GlobalConstants.PRIVATE_KEY), StandardCharsets.UTF_8);
        String password = new String(RSACoder.decryptPrivateKey(java.util.Base64.getDecoder().decode(sysLoginModel.getPassword()), GlobalConstants.PRIVATE_KEY), StandardCharsets.UTF_8);
        log.info("登录账号------" + username);
        //update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题
        //前端密码加密，后端进行密码解密
        //password = AesEncryptUtil.desEncrypt(sysLoginModel.getPassword().replaceAll("%2B", "\\+")).trim();//密码解密
        //update-begin--Author:scott  Date:20190805 for：暂时注释掉密码加密逻辑，有点问题

        //update-begin-author:taoyan date:20190828 for:校验验证码
        String captcha = sysLoginModel.getCaptcha();
        if (captcha == null) {
            result.error500("验证码无效");
            return result;
        }
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = MD5Util.MD5Encode(lowerCaseCaptcha + sysLoginModel.getCheckKey(), "utf-8");
        Object checkCode = redisUtil.get(realKey);
        //当进入登录页时，有一定几率出现验证码错误 #1714
        if (checkCode == null || !checkCode.toString().equals(lowerCaseCaptcha)) {
            result.error500("验证码错误");
            return result;
        }
        //update-end-author:taoyan date:20190828 for:校验验证码

        //1. 校验用户是否有效
        //update-begin-author:wangshuai date:20200601 for: 登录代码验证用户是否注销bug，if条件永远为false
        LambdaQueryWrapper<SysUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysUser::getUsername, Sm4Util.encryptEcb(username));
        SysUser sysUser = ValidateEncryptEntityUtil.validateDecryptObject(sysUserService.getOne(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
        //
        if (sysUser != null) {
            String unitmemberid = sysUser.getUnitmemberid();
            if (StringUtils.isNotEmpty(unitmemberid)) {
                ZgsProjectunitmember zgsProjectunitmember = ValidateEncryptEntityUtil.validateDecryptObject(zgsProjectunitmemberService.getById(unitmemberid), ValidateEncryptEntityUtil.isDecrypt);
                if (zgsProjectunitmember != null) {
                    sysUser.setPhone(zgsProjectunitmember.getMobilephone());
                    sysUser.setEmail(zgsProjectunitmember.getEmail());
                    sysUser.setEmployunit(zgsProjectunitmember.getEmployunit());
                    sysUser.setDepartment(zgsProjectunitmember.getDepartment());
                }
            }
        }

        //update-end-author:wangshuai date:20200601 for: 登录代码验证用户是否注销bug，if条件永远为false
        result = sysUserService.checkUserIsEffective(sysUser);
        if (!result.isSuccess()) {
            return result;
        }

        //2. 校验用户名或密码是否正确
        String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
        String syspassword = sysUser.getPassword();
        if (!syspassword.equals(userpassword)) {
            result.error500("用户名或密码错误");
            return result;
        }
        //3、判断登录页用户类型：1、申报单位/个人 2、推荐单位 3、评审专家
        if (StringUtils.isNotEmpty(sysLoginModel.getType())) {
            if ("1".equals(sysLoginModel.getType())) {
                if (sysUser.getLoginUserType() != 0 && sysUser.getLoginUserType() != 1 && sysUser.getLoginUserType() != 6 && sysUser.getLoginUserType() != 7) {
                    result.error500("非申报单位/个人用户");
                    return result;
                }
                if (sysUser.getLoginUserType() == 6 || sysUser.getLoginUserType() == 7) {
//                    sysUser.setRealname(sysUser.getAreaname());
                }
            }
            if ("2".equals(sysLoginModel.getType())) {
                if (sysUser.getLoginUserType() != 4 && sysUser.getLoginUserType() != 8) {
                    result.error500("非推荐单位或金融单位用户");
                    return result;
                }
            }
            if ("3".equals(sysLoginModel.getType())) {
                if (sysUser.getLoginUserType() != 2) {
                    result.error500("非评审专家用户");
                    return result;
                }
            }
            if ("4".equals(sysLoginModel.getType())) {
                if (sysUser.getLoginUserType() != 3) {
                    result.error500("非终审单位用户");
                    return result;
                }
            }
        }

        if (GreenUtilSelf.matchesPassWord(password)) {
            //表示密码弱口令弹出修改密码窗口
            sysUser.setIspwd("2");
        }
        //用户登录信息
        userInfo(sysUser, result);
        //update-begin--Author:liusq  Date:20210126  for：登录成功，删除redis中的验证码
        redisUtil.del(realKey);
        //update-begin--Author:liusq  Date:20210126  for：登录成功，删除redis中的验证码
        LoginUser loginUser = new LoginUser();
        BeanUtils.copyProperties(sysUser, loginUser);
        baseCommonService.addLog("用户名: " + username + ",登录成功！", CommonConstant.LOG_TYPE_1, null, loginUser);
        //update-end--Author:wangshuai  Date:20200714  for：登录日志没有记录人员
        return result;
    }


    /**
     * 【vue3专用】获取用户信息
     */
    @GetMapping("/user/getUserInfo")
    public Result<JSONObject> getUserInfo(HttpServletRequest request) {
        Result<JSONObject> result = new Result<JSONObject>();
        String username = JwtUtil.getUserNameByToken(request);
        if (oConvertUtils.isNotEmpty(username)) {
            // 根据用户名查询用户信息
            SysUser sysUser = ValidateEncryptEntityUtil.validateDecryptObject(sysUserService.getUserByName(Sm4Util.encryptEcb(username)), ValidateEncryptEntityUtil.isDecrypt);
            //用户登录信息
            Result<JSONObject> resultObj = userInfo(sysUser, result);
            JSONObject jsonObject = resultObj.getResult();
            JSONObject obj = new JSONObject();
            obj.put("userInfo", jsonObject.get("userInfo"));
            obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
            result.setResult(obj);
            result.success("");
        }
        return result;

    }

    /**
     * 退出登录
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/logout")
    public Result<Object> logout(HttpServletRequest request, HttpServletResponse response) {
        //用户退出逻辑
        String token = request.getHeader(CommonConstant.X_ACCESS_TOKEN);
        if (oConvertUtils.isEmpty(token)) {
            return Result.error("退出登录失败！");
        }
        String username = JwtUtil.getUsername(token);
        LoginUser sysUser = sysBaseAPI.getUserByName(username);
        if (sysUser != null) {
            //update-begin--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
            baseCommonService.addLog("用户名: " + sysUser.getRealname() + ",退出成功！", CommonConstant.LOG_TYPE_1, null, sysUser);
            //update-end--Author:wangshuai  Date:20200714  for：登出日志没有记录人员
            log.info(" 用户名:  " + sysUser.getRealname() + ",退出成功！ ");
            //清空用户登录Token缓存
            redisUtil.del(CommonConstant.PREFIX_USER_TOKEN + token);
            //清空用户登录Shiro权限缓存
            redisUtil.del(CommonConstant.PREFIX_USER_SHIRO_CACHE + sysUser.getId());
            //清空用户的缓存信息（包括部门信息），例如sys:cache:user::<username>
            redisUtil.del(String.format("%s::%s", CacheConstant.SYS_USERS_CACHE, sysUser.getUsername()));
            //调用shiro的logout
            SecurityUtils.getSubject().logout();
            return Result.ok("退出登录成功！");
        } else {
            return Result.error("Token无效!");
        }
    }

    /**
     * 获取访问量
     *
     * @return
     */
    @GetMapping("loginfo")
    public Result<JSONObject> loginfo() {
        Result<JSONObject> result = new Result<JSONObject>();
        JSONObject obj = new JSONObject();
        //update-begin--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
        // 获取一天的开始和结束时间
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date dayStart = calendar.getTime();
        calendar.add(Calendar.DATE, 1);
        Date dayEnd = calendar.getTime();
        // 获取系统访问记录
        Long totalVisitCount = logService.findTotalVisitCount();
        obj.put("totalVisitCount", totalVisitCount);
        Long todayVisitCount = logService.findTodayVisitCount(dayStart, dayEnd);
        obj.put("todayVisitCount", todayVisitCount);
        Long todayIp = logService.findTodayIp(dayStart, dayEnd);
        //update-end--Author:zhangweijian  Date:20190428 for：传入开始时间，结束时间参数
        obj.put("todayIp", todayIp);
        result.setResult(obj);
        result.success("登录成功");
        return result;
    }

    /**
     * 获取访问量
     *
     * @return
     */
    @GetMapping("visitInfo")
    public Result<List<Map<String, Object>>> visitInfo() {
        Result<List<Map<String, Object>>> result = new Result<List<Map<String, Object>>>();
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        Date dayEnd = calendar.getTime();
        calendar.add(Calendar.DAY_OF_MONTH, -7);
        Date dayStart = calendar.getTime();
        List<Map<String, Object>> list = logService.findVisitCount(dayStart, dayEnd);
        result.setResult(oConvertUtils.toLowerCasePageList(list));
        return result;
    }


    /**
     * 登陆成功选择用户当前部门
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/selectDepart", method = RequestMethod.PUT)
    public Result<JSONObject> selectDepart(@RequestBody SysUser user) {
        Result<JSONObject> result = new Result<JSONObject>();
        String username = user.getUsername();
        if (oConvertUtils.isEmpty(username)) {
            LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
            username = sysUser.getUsername();
        }
        String orgCode = user.getOrgCode();
        this.sysUserService.updateUserDepart(username, orgCode);
        SysUser sysUser = sysUserService.getUserByName(username);
        JSONObject obj = new JSONObject();
        obj.put("userInfo", sysUser);
        result.setResult(obj);
        return result;
    }

    /**
     * 短信登录接口
     *
     * @param jsonObject
     * @return
     */
    @PostMapping(value = "/sms")
    public Result<String> sms(@RequestBody JSONObject jsonObject) {
        Result<String> result = new Result<String>();
        String mobile = jsonObject.get("mobile").toString();
        //手机号模式 登录模式: "2"  注册模式: "1"
        String smsmode = jsonObject.get("smsmode").toString();
        log.info(mobile);
        if (oConvertUtils.isEmpty(mobile)) {
            result.setMessage("手机号不允许为空！");
            result.setSuccess(false);
            return result;
        }
        Object object = redisUtil.get(mobile);
        if (object != null) {
            result.setMessage("验证码5分钟内，仍然有效！");
            result.setSuccess(false);
            return result;
        }

        //随机数
        String captcha = RandomUtil.randomNumbers(6);
        JSONObject obj = new JSONObject();
        obj.put("code", captcha);
        try {
            boolean b = false;
            Result<?> resultMs = null;
            //注册模板
            if (CommonConstant.SMS_TPL_TYPE_1.equals(smsmode)) {
                SysUser sysUser = sysUserService.getUserByPhone(Sm4Util.encryptEcb(mobile));
                if (sysUser != null) {
                    result.error500("手机号已经注册，请直接登录！");
                    baseCommonService.addLog("手机号已经注册，请直接登录！", CommonConstant.LOG_TYPE_1, null);
                    return result;
                }
                b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.REGISTER_TEMPLATE_CODE);
            } else {
                //登录模式，校验用户有效性
                List<SysUser> userList = sysUserService.getUserByPhoneList(Sm4Util.encryptEcb(mobile));
                SysUser sysUser = null;
                if (userList.size() == 0) {
                    result.error500("该用户不存在，请注册");
                    return result;
                } else if (userList.size() == 1) {
                    //情况1-1：推荐单位不可使用手机号密码找回
                    sysUser = userList.get(0);
                    if (sysUser.getLoginUserType() != null && GlobalConstants.loginUserType_4 == sysUser.getLoginUserType()) {
                        result.error500("推荐单位密码问题统一通过正式文件（函）按程序向省住建厅申请");
                        return result;
                    }
                } else {
                    result.error500("手机号注册了多个账号，请联系管理员");
                    return result;
                }
                result = sysUserService.checkUserIsEffective(sysUser);
                if (!result.isSuccess()) {
                    String message = result.getMessage();
                    if ("该用户不存在，请注册".equals(message)) {
                        result.error500("该用户不存在或未绑定手机号");
                    }
                    return result;
                }

                /**
                 * smsmode 短信模板方式  0 .登录模板、1.注册模板、2.忘记密码模板
                 */
                if (CommonConstant.SMS_TPL_TYPE_0.equals(smsmode)) {
                    //登录模板
                    b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.LOGIN_TEMPLATE_CODE);
                } else if (CommonConstant.SMS_TPL_TYPE_2.equals(smsmode)) {
                    //忘记密码模板
//                    b = DySmsHelper.sendSms(mobile, obj, DySmsEnum.FORGET_PASSWORD_TEMPLATE_CODE);
                    //替换成自己的短信模板
                    HashMap<String, Object> fillContent = new HashMap<>();
//                    fillContent.put("content", "忘记密码");
                    HashMap<String, Object> paramsMap = new HashMap<>();
                    paramsMap.put("phoneNumber", mobile);
                    paramsMap.put("smsType", 3);
                    paramsMap.put("companyName", Sm4Util.decryptEcb(sysUser.getRealname()));
                    paramsMap.put("captcha", captcha);
                    paramsMap.put("templateId", 13);
                    paramsMap.put("fillContent", fillContent);
                    resultMs = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                }
            }

//            if (b == false) {
////                result.setMessage("短信验证码发送失败,请稍后重试");
////                result.setSuccess(false);
////                return result;
////            }
            if (resultMs != null && resultMs.getCode() == 200) {
                result.setMessage("发送短信成功！");
                result.setSuccess(true);
            } else {
                result.setMessage("发送短信失败！");
                result.setSuccess(false);
                return result;
            }
            //验证码5分钟内有效
            redisUtil.set(mobile, captcha, 300);
            //update-begin--Author:scott  Date:20190812 for：issues#391
            //result.setResult(captcha);
            //update-end--Author:scott  Date:20190812 for：issues#391
            result.setSuccess(true);
        } catch (ClientException e) {
            e.printStackTrace();
            result.error500("短信接口未配置，请联系管理员！");
            return result;
        }
        return result;
    }


    /**
     * 手机号登录接口
     *
     * @param jsonObject
     * @return
     */
    @ApiOperation("手机号登录接口")
    @PostMapping("/phoneLogin")
    public Result<JSONObject> phoneLogin(@RequestBody JSONObject jsonObject) {
        Result<JSONObject> result = new Result<JSONObject>();
        String phone = jsonObject.getString("mobile");

        //校验用户有效性
        SysUser sysUser = ValidateEncryptEntityUtil.validateDecryptObject(sysUserService.getUserByPhone(Sm4Util.encryptEcb(phone)), ValidateEncryptEntityUtil.isDecrypt);
        result = sysUserService.checkUserIsEffective(sysUser);
        if (!result.isSuccess()) {
            return result;
        }

        String smscode = jsonObject.getString("captcha");
        Object code = redisUtil.get(Sm4Util.encryptEcb(phone));
        if (!smscode.equals(code)) {
            result.setMessage("手机验证码错误");
            return result;
        }
        //用户信息
        userInfo(sysUser, result);
        //添加日志
        baseCommonService.addLog("用户名: " + sysUser.getUsername() + ",登录成功！", CommonConstant.LOG_TYPE_1, null);

        return result;
    }


    /**
     * 用户信息
     *
     * @param sysUser
     * @param result
     * @return
     */
    private Result<JSONObject> userInfo(SysUser sysUser, Result<JSONObject> result) {
        String syspassword = sysUser.getPassword();
        String username = sysUser.getUsername();
        // 获取用户部门信息
        JSONObject obj = new JSONObject();
        List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
        obj.put("departs", departs);
        if (departs == null || departs.size() == 0) {
            obj.put("multi_depart", 0);
        } else if (departs.size() == 1) {
            sysUserService.updateUserDepart(Sm4Util.encryptEcb(username), departs.get(0).getOrgCode());
            obj.put("multi_depart", 1);
        } else {
            //查询当前是否有登录部门
            // update-begin--Author:wangshuai Date:20200805 for：如果用戶为选择部门，数据库为存在上一次登录部门，则取一条存进去
            SysUser sysUserById = sysUserService.getById(sysUser.getId());
            if (oConvertUtils.isEmpty(sysUserById.getOrgCode())) {
                sysUserService.updateUserDepart(Sm4Util.encryptEcb(username), departs.get(0).getOrgCode());
            }
            // update-end--Author:wangshuai Date:20200805 for：如果用戶为选择部门，数据库为存在上一次登录部门，则取一条存进去
            obj.put("multi_depart", 2);
        }
        // update-begin--Author:sunjianlei Date:20210802 for：获取用户租户信息
        String tenantIds = sysUser.getRelTenantIds();
        if (oConvertUtils.isNotEmpty(tenantIds)) {
            List<String> tenantIdList = Arrays.asList(tenantIds.split(","));
            // 该方法仅查询有效的租户，如果返回0个就说明所有的租户均无效。
            List<SysTenant> tenantList = sysTenantService.queryEffectiveTenant(tenantIdList);
            if (tenantList.size() == 0) {
                result.error500("与该用户关联的租户均已被冻结，无法登录！");
                return result;
            } else {
                obj.put("tenantList", tenantList);
            }
        }
        // update-end--Author:sunjianlei Date:20210802 for：获取用户租户信息
        // 生成token
        String token = JwtUtil.sign(username, syspassword);
        // 设置token缓存有效时间
        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 24 / 1000);
        obj.put("token", token);
        obj.put("userInfo", sysUser);
        obj.put("sysAllDictItems", sysDictService.queryAllDictItems());
        result.setResult(obj);
        result.success("登录成功");
        return result;
    }

    /**
     * 获取加密字符串
     *
     * @return
     */
    @GetMapping(value = "/getEncryptedString")
    public Result<Map<String, String>> getEncryptedString() {
        Result<Map<String, String>> result = new Result<Map<String, String>>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("key", EncryptedString.key);
        map.put("iv", EncryptedString.iv);
        result.setResult(map);
        return result;
    }

    /**
     * 后台生成图形验证码 ：有效
     *
     * @param response
     * @param key
     */
    @ApiOperation("获取验证码")
    @GetMapping(value = "/randomImage/{key}")
    public Result<String> randomImage(HttpServletResponse response, @PathVariable String key) {
        Result<String> res = new Result<String>();
        res.setMessage(downloadUrl + GlobalConstants.INDEX_CNS);
        try {
            String code = RandomUtil.randomString(BASE_CHECK_CODES, 4);
            String lowerCaseCode = code.toLowerCase();
            String realKey = MD5Util.MD5Encode(lowerCaseCode + key, "utf-8");
            redisUtil.set(realKey, lowerCaseCode, 60);
            String base64 = RandImageUtil.generate(code);
            res.setSuccess(true);
            res.setResult(base64);
        } catch (Exception e) {
            res.error500("获取验证码出错" + e.getMessage());
            e.printStackTrace();
        }
        return res;
    }

    /**
     * app登录
     *
     * @param sysLoginModel
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/mLogin", method = RequestMethod.POST)
    public Result<JSONObject> mLogin(@RequestBody SysLoginModel sysLoginModel) throws Exception {
        Result<JSONObject> result = new Result<JSONObject>();
        String username = sysLoginModel.getUsername();
        String password = sysLoginModel.getPassword();

        //1. 校验用户是否有效
        SysUser sysUser = sysUserService.getUserByName(username);
        result = sysUserService.checkUserIsEffective(sysUser);
        if (!result.isSuccess()) {
            return result;
        }

        //2. 校验用户名或密码是否正确
        String userpassword = PasswordUtil.encrypt(username, password, sysUser.getSalt());
        String syspassword = sysUser.getPassword();
        if (!syspassword.equals(userpassword)) {
            result.error500("用户名或密码错误");
            return result;
        }

        String orgCode = sysUser.getOrgCode();
        if (oConvertUtils.isEmpty(orgCode)) {
            //如果当前用户无选择部门 查看部门关联信息
            List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
            if (departs == null || departs.size() == 0) {
                result.error500("用户暂未归属部门,不可登录!");
                return result;
            }
            orgCode = departs.get(0).getOrgCode();
            sysUser.setOrgCode(orgCode);
            this.sysUserService.updateUserDepart(username, orgCode);
        }
        JSONObject obj = new JSONObject();
        //用户登录信息
        obj.put("userInfo", sysUser);

        // 生成token
        String token = JwtUtil.sign(username, syspassword);
        // 设置超时时间
        redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
        redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 24 / 1000);

        //token 信息
        obj.put("token", token);
        result.setResult(obj);
        result.setSuccess(true);
        result.setCode(200);
        baseCommonService.addLog("用户名: " + username + ",登录成功[移动端]！", CommonConstant.LOG_TYPE_1, null);
        return result;
    }

    /**
     * 图形验证码
     *
     * @param sysLoginModel
     * @return
     */
    @RequestMapping(value = "/checkCaptcha", method = RequestMethod.POST)
    public Result<?> checkCaptcha(@RequestBody SysLoginModel sysLoginModel) {
        String captcha = sysLoginModel.getCaptcha();
        String checkKey = sysLoginModel.getCheckKey();
        if (captcha == null) {
            return Result.error("验证码无效");
        }
        String lowerCaseCaptcha = captcha.toLowerCase();
        String realKey = MD5Util.MD5Encode(lowerCaseCaptcha + checkKey, "utf-8");
        Object checkCode = redisUtil.get(realKey);
        if (checkCode == null || !checkCode.equals(lowerCaseCaptcha)) {
            return Result.error("验证码错误");
        }
        return Result.ok();
    }

    @ApiOperation("住建厅统一认证")
    @GetMapping(value = "/authCode")
    public ModelAndView authCode(@RequestParam(name = "code") String code) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            log.info("获取授权码" + code);
            String Authorization = clientId + ":" + clientSecret;
            Map<String, String> mapToken = new HashMap<String, String>();
            mapToken.put("isToken", "false");
            mapToken.put("Authorization", "Basic " + Base64.encodeToString(Authorization.getBytes("UTF-8")));
            HttpHeader httpHeaderToken = new HttpHeader(mapToken);
            HttpParamers httpParamersToken = HttpParamers.httpPostParamers();
            httpParamersToken.addParam("grant_type", GlobalConstants.AUTHORIZATION_CODE);
            httpParamersToken.addParam("code", code);
            httpParamersToken.addParam("redirect_uri", loginCallBackUrl);
//            httpParamersToken.setJsonParamer();//设置后isJson为true走JSON_CONTENT_FORM,否则CONTENT_FORM
            HttpService httpService = new HttpService(loginBaseUrl);
            String result = httpService.service(GlobalConstants.LOGIN_GET_TOKEN_URL, httpParamersToken, httpHeaderToken);
            AccessTokenInfo accessTokenInfo = JSONObject.parseObject(result, new TypeReference<AccessTokenInfo>() {
            });
            String access_token = accessTokenInfo.getAccess_token();
            log.info("授权码请求令牌" + accessTokenInfo.toString());
            //获取用户信息
            Map<String, String> mapUser = new HashMap<String, String>();
            mapUser.put("Authorization", "Bearer " + access_token);
            HttpHeader httpHeaderUser = new HttpHeader(mapUser);
            HttpParamers httpParamersUser = HttpParamers.httpPostParamers();
            httpParamersUser.setJsonParamer();
            HttpService httpServiceUser = new HttpService(loginBaseUrl);
            String resultUser = httpServiceUser.service(GlobalConstants.GETUSERINFO_URL, httpParamersUser, httpHeaderUser);
            //{"code":"00000","msg":"成功",
            // "data":{"id":"13",
            // "createTime":"2022-02-09 04:31:00","updateTime":"2022-03-01 03:20:50",
            // "username":"testsso","remarks":"勿删，勿改，对接用的","nickname":"第三方测试",
            // "email":"","sex":"2","phoneNumber":null,"avatar":null,"idcard":null,"fullName":null,"birthday":null,"nation":null,"nativePlace":null,"title":null,"officeLocation":null,"officeTelephone":null,"fax":null,"labelIds":[],
            // "mainLabelId":"07739869522066a79954ec8711a80fb5","mainLabelName":"第三方测试"}}
            AuthCodeResult authCodeResult = JSONObject.parseObject(resultUser, new TypeReference<AuthCodeResult>() {
            });
            AuthCodeUserInfo authCodeUserInfo = authCodeResult.getData();
            log.info("用户基本信息" + authCodeUserInfo.toString());
            SysUser sysUser = ValidateEncryptEntityUtil.validateDecryptObject(sysUserService.getById(authCodeUserInfo.getId()), ValidateEncryptEntityUtil.isDecrypt);
            if (sysUser != null) {
                String token = JwtUtil.sign(sysUser.getUsername(), sysUser.getPassword());
                // 设置token缓存有效时间1646841729278
                redisUtil.set(CommonConstant.PREFIX_USER_TOKEN + token, token);
                redisUtil.expire(CommonConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME * 24 / 1000);
                SysUser user = new SysUser();
                user.setId(sysUser.getId());
                user.setToken(token);
                sysUserService.updateById(user);
                modelAndView.setViewName("redirect:" + systemUrl + "?token=" + token);
                log.info("token=" + token + ",EXPIRE_TIME=" + JwtUtil.EXPIRE_TIME * 24 / 1000);
            } else {
                modelAndView.setViewName("redirect:" + systemUrl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelAndView;
    }

    @ApiOperation("根据请求头中的token获取用户信息")
    @GetMapping("/getUserByToken")
    public Result<JSONObject> getUserByToken(HttpServletRequest request) {
        String accessToken = request.getParameter("token");
        Result<JSONObject> result = new Result<JSONObject>();
        String username = JwtUtil.getUserNameByToken(accessToken);
        if (oConvertUtils.isNotEmpty(username)) {
            // 根据用户名查询用户信息
            SysUser sysUser = ValidateEncryptEntityUtil.validateDecryptObject(sysUserService.getUserByName(Sm4Util.encryptEcb(username)), ValidateEncryptEntityUtil.isDecrypt);
            result = sysUserService.checkUserIsEffective(sysUser);
            userInfo(sysUser, result);
        }
        return result;
    }

    @AutoLog(value = "发送短信-外网")
    @ApiOperation(value = "发送短信-外网", notes = "发送短信-外网")
    @PostMapping(value = "/sendSMSCommonOnlyWay")
    public Result<?> sendSMSCommonOnlyWay(@RequestBody SendSmsInfo sendSmsInfo) {
        Result<?> result = null;
        if (sendSmsInfo.getType() == 1) {
            QueryWrapper<ZgsProjectunitmembersend> queryWrapper = new QueryWrapper();
            queryWrapper.isNull("isflag");
            List<ZgsProjectunitmembersend> userList = ValidateEncryptEntityUtil.validateDecryptList(zgsProjectunitmembersendService.list(queryWrapper), ValidateEncryptEntityUtil.isDecrypt);
            if (userList != null && userList.size() > 0) {
                for (int i = 0; i < userList.size(); i++) {
                    ZgsProjectunitmembersend sysUser = userList.get(i);
                    //
                    HashMap<String, Object> paramsMap = new HashMap<>();
                    paramsMap.put("phoneNumber", sysUser.getApplyunitphone());
                    paramsMap.put("smsType", 3);
                    paramsMap.put("type", sendSmsInfo.getType());
                    if (StringUtils.isNotEmpty(sysUser.getApplyunit())) {
                        paramsMap.put("companyName", sysUser.getApplyunit());
                    } else if (StringUtils.isNotEmpty(sysUser.getApplyunitprojectleader())) {
                        paramsMap.put("companyName", sysUser.getApplyunitprojectleader());
                    } else {
                        paramsMap.put("companyName", "无");
                    }
                    paramsMap.put("templateId", sysUser.getTemplateid());
                    paramsMap.put("fillContent", sendSmsInfo.getFillContent());
                    //
                    log.info(JSONObject.toJSONString(paramsMap));
                    result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(paramsMap));
                    if (result.getCode() == 200) {
                        log.info(sysUser.getId() + "-发送短信成功！-" + sysUser.getApplyunitphone());
                        ZgsProjectunitmembersend projectunitmembersend = new ZgsProjectunitmembersend();
                        projectunitmembersend.setId(sysUser.getId());
                        projectunitmembersend.setIsflag("1");
                        zgsProjectunitmembersendService.updateById(projectunitmembersend);
                    } else {
                        log.info(sysUser.getId() + "-发送短信失败！-" + sysUser.getApplyunitphone());
                    }
                }
            }
        } else {
            log.info("type=0>sendSMSCommonOnlyWay");
            result = smsTokenUtil.sendSMSService(JSONObject.toJSONString(sendSmsInfo));
        }
        return result;
    }

}
