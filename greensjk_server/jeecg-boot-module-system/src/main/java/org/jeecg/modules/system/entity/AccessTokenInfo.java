package org.jeecg.modules.system.entity;

import lombok.Data;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/115:28
 */
@Data
public class AccessTokenInfo {
    //{\"access_token\":\"006bd993-60ae-4498-9a70-b043bc0c7154\",\"token_type\":\"bearer\",\"refresh_token\":\"06901587-9db0-4e2a-b8bb-2ce9a9373aed\",\"expires_in\":43194,\"scope\":\"server\",\"license\":\"made by eca\",\"clientId\":\"lsjnjz\",\"user_id\":\"13\",\"label_ids\":[\"07739869522066a79954ec8711a80fb5\"],\"username\":\"testsso\"}
    /**
     * 接口访问令牌
     */
    private String access_token;
    /**
     * 令牌类型
     */
    private String token_type;
    /**
     * 刷新令牌
     */
    private String refresh_token;
    /**
     * access_token接口调用凭证超时时间，单位（秒）
     */
    private long expires_in;
    /**
     * 权限范围
     */
    private String scope;
    /**
     *
     */
    private String license;
    /**
     * 应用ID
     */
    private String clientId;
    /**
     *
     */
    private String user_id;
    /**
     * 标签集合
     */
    private String[] label_ids;
    /**
     * 用户名
     */
    private String username;
}
