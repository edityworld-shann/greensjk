package org.jeecg.modules.system.entity;

import lombok.Data;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/218:16
 */
@Data
public class AuthCodeUserInfo {
    //{"code":"00000","msg":"成功",
    // "data":{"id":"13",
    // "createTime":"2022-02-09 04:31:00","updateTime":"2022-03-01 03:20:50",
    // "username":"testsso","remarks":"勿删，勿改，对接用的","nickname":"第三方测试",
    // "email":"","sex":"2","phoneNumber":null,"avatar":null,"idcard":null,"fullName":null,"birthday":null,"nation":null,"nativePlace":null,"title":null,"officeLocation":null,"officeTelephone":null,"fax":null,"labelIds":[],
    // "mainLabelId":"07739869522066a79954ec8711a80fb5","mainLabelName":"第三方测试"}}
    private String id;
    private String username;
    private String nickname;
    private String sex;
    private String mainLabelId;
    private String mainLabelName;
}
