package org.jeecg.modules.api.controller;


import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.util.LibreOfficeUtil;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.jeecg.modules.green.common.entity.ZgsProjecttask;
import org.jeecg.modules.green.common.entity.ZgsSciencetechtask;
import org.jeecg.modules.green.common.service.AsyncScienceService;
import org.jeecg.modules.green.common.service.AsyncService;
import org.jeecg.modules.green.common.service.IZgsProjecttaskService;
import org.jeecg.modules.green.common.service.IZgsSciencetechtaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/data/taskUtils")
@Api(tags = "taskPDFUtils")
@Slf4j
public class TaskPdfUtils {

    @Autowired
    private IZgsSciencetechtaskService izgssciencetechtaskservice;
    @Autowired
    private IZgsProjecttaskService izgsprojecttaskservice;
    @Autowired
    private AsyncService asyncService;
    @Autowired
    private AsyncScienceService asyncScienceService;

    @Value("${jeecg.path.upload}")
    private String upload;

    @GetMapping("/greenTask")
    public String greenTask() throws IOException {

        List<ZgsProjecttask> taskList = izgsprojecttaskservice.list();
        for (ZgsProjecttask task : taskList) {
            task = ValidateEncryptEntityUtil.validateDecryptObject(task, ValidateEncryptEntityUtil.isDecrypt);
            String pdfFilePath = upload + task.getPdfUrl();
            pdfFilePath = pdfFilePath.replace("\\", "/");

            File pdfFile = new File(pdfFilePath);
            if (pdfFile.exists()) {
                log.info("pad file exists........  for next....");
                continue;
            }
            String docFilePath = pdfFilePath.replace(".pdf", ".doc");
            File docFile = new File(docFilePath);
            if (docFile.exists()){
                log.info("pdf file not exists, doc file exists.......  make the pdf file....");
                LibreOfficeUtil.wordConverterToPdf(docFilePath);
            } else {
                log.info("pdf file not exists, doc file not exists.......  rebuild the task files. ");

                asyncService.initProjectTaskPdfUrl(task.getId());
            }
        }
        return "OK";
    }
    @GetMapping("/scienceTask")
    public String scienceTask() throws IOException {
        List<ZgsSciencetechtask> taskList = izgssciencetechtaskservice.list();
        for (ZgsSciencetechtask task : taskList) {
            task = ValidateEncryptEntityUtil.validateDecryptObject(task, ValidateEncryptEntityUtil.isDecrypt);
            String pdfFilePath = upload + task.getPdfUrl();
            pdfFilePath = pdfFilePath.replace("\\", "/");

            File pdfFile = new File(pdfFilePath);
            if (pdfFile.exists()) {
                log.info("pad file exists........  for next....");
                continue;
            }
            String docFilePath = pdfFilePath.replace(".pdf", ".doc");
            File docFile = new File(docFilePath);
            if (docFile.exists()){
                log.info("pdf file not exists, doc file exists.......  make the pdf file....");
                LibreOfficeUtil.wordConverterToPdf(docFilePath);
            } else {
                log.info("pdf file not exists, doc file not exists.......  rebuild the task files. ");

                asyncScienceService.initScienceTechtaskPdfUrl(task.getId());
            }
        }
        return "OK";
    }
}
