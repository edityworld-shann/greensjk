package org.jeecg.modules.system.util;


import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


/**
 * @author ASUS
 */
public abstract class RSACoder extends Coder {
    public static final String KEY_ALGORITHM = "RSA";
    public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

    /**
     * 用私钥对信息生成数字签名
     *
     * @param data       加密数据
     * @param privateKey 私钥
     * @return
     * @throws Exception
     */
    public static String sign(byte[] data, String privateKey) throws Exception {
        //解密由base64编码的私钥
        byte[] keyBytes = decryptBASE64(privateKey);

        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);

        //取私钥对象
        PrivateKey pKey = keyFactory.generatePrivate(pkcs8EncodedKeySpec);

        //用私钥生成数字签名
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(pKey);
        signature.update(data);

        return encryptBASE64(signature.sign());
    }

    /**
     * 校验数字签名
     *
     * @param data      加密数据
     * @param publicKey 公钥
     * @param sign      数字签名
     * @return
     * @throws Exception
     */
    public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {

        //解密有base64编码的公钥
        byte[] keyBytes = decryptBASE64(publicKey);

        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);

        //取公钥对象
        PublicKey pKey = keyFactory.generatePublic(keySpec);

        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(pKey);
        signature.update(data);
        //验证签名是否正常
        return signature.verify(decryptBASE64(sign));
    }

    /**
     * 解密
     * 用私钥解密
     *
     * @param data 加密数据
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptPrivateKey(byte[] data, String key) {

        byte[] temp = null;
        try {
            byte[] keyBytes = decryptBASE64(key);
            //取得私钥
            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
            Key pKey = factory.generatePrivate(encodedKeySpec);

            //对数据解密
            Cipher cipher = Cipher.getInstance(factory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, pKey);
            temp = cipher.doFinal(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    /**
     * 用公钥解密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] decryptByPublicKey(byte[] data, String key) throws Exception {

        //解密
        byte[] keyBytes = decryptBASE64(key);

        //取得公钥
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key pKey = keyFactory.generatePublic(keySpec);

        //对数据解密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, pKey);

        return cipher.doFinal(data);
    }

    /**
     * 用公钥加密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPublicKey(byte[] data, String key) throws Exception {

        byte[] keyBytes = decryptBASE64(key);

        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key pKey = keyFactory.generatePublic(keySpec);


        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, pKey);

        return cipher.doFinal(data);
    }

    /**
     * 用私钥加密
     *
     * @param data
     * @param key
     * @return
     * @throws Exception
     */
    public static byte[] encryptByPrivateKey(byte[] data, String key) throws Exception {

        byte[] keyBytes = decryptBASE64(key);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateKey = keyFactory.generatePrivate(keySpec);

        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);

        return cipher.doFinal(data);
    }

    public static void main(String[] args) throws Exception {
        //        String PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds=";
//        String str="SLUTvwcGx8amSZzhopxzAwoiBjJZT3GXi4CFxAfOh9MLLrGn9jwIIiqaYJNosAkLpbp4gmDx3Ubx7fVfWW4cardQ3piheXhPP0a1EQfup1B1FLZjRfdhHcL7gvlrJJxzePAvPzuU8LWIfQY9bWc4Jcdx1OzeM+C8RFMMzkthTh7xFNP4G2qn4TjSUSKYBDzX3DGGoMnf1uQi3ec44d7OrFq6aa8XUwxDn+JmWxB9+Gx0NwZAudvTYq1rV0qx6MpILwKSy2Pp69JK+Fkz+IwZA4rlQq2pLm7tgui22QngzG0mPbolMFy1nRsr50C30kxMQOQxfQ9uCzsvgrgx+YwZfg==";
//        System.out.println(new String(decryptPrivateKey(Base64.decode(str),PRIVATE_KEY)));
//        String publicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApmk2cu95+MT/xbBvllnCt87P4jzQWQJTU0JuoxN6JQJWK1CPRllpbRIN1+PqxXri8F8M+sbR05jS4+/zZwSttsBmhZBVhmCmud9StO+UkY8S701CuOR3I//XWOnwDKBz+82OyDTisY2seQ502ZW//sneApTbUwCxWe77upE4Y91ieDm/klQXNIEQBrUi702RaSghFZmWDNyYdCka1juWwXD9p3TU5NcgTY8bvrQNIZ/DE8uV80n1tuThfOSYHOynP7lwzPrUs6t/XqgxiUViPiWXTsil9Bk6xTI+kTXpGwbAgjFUEJTkbUv6KD5Sl8+ylvXYTlbgL7nXk5ExyvSwewIDAQAB";
//
//        byte[] bytes = encryptByPublicKey("3210917q".getBytes(), publicKey);
//
//        System.out.println(Base64.encodeBase64String(bytes));
//
//        System.out.println(BigDecimal.valueOf(1L).divide(BigDecimal.valueOf(2L)));

//        String PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds=";
//        String str="QZwkZRfBOL/33liHPtAnmZDI7KWux3b4FgFmlttmBi/Q/2iFCoqilL7BWTQ30Qobr7RtkHUG1a62YNKGAnlJgXeVkXbDQj6LIb268D/ngBbRyi8pKOET7ErDuEYeAhDEJA+ibLktyICMD9LV7MxEcU5DSMWHWnfGH68nk9eezE6e73NKQ95P9o2QUTlGJxQjFElUjaFNug3kHHchmuWPrICCCWPzHT1hTfqpaRqhCvtZ3aiFiDgt+PG/q07OPVcIDdbSH3SFEwpwdVz37zmIaUSe3z5UT4vHvseYk4Odq2x7HTh5Cp3QHvZuuOdBsRJINOSHZLWV4g67MBx2B5Ourw==";
//        System.out.println(new String(decryptPrivateKey(Base64.decodeBase64(str.getBytes()),PRIVATE_KEY)));
//        String PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds=";
//        String str="SLUTvwcGx8amSZzhopxzAwoiBjJZT3GXi4CFxAfOh9MLLrGn9jwIIiqaYJNosAkLpbp4gmDx3Ubx7fVfWW4cardQ3piheXhPP0a1EQfup1B1FLZjRfdhHcL7gvlrJJxzePAvPzuU8LWIfQY9bWc4Jcdx1OzeM+C8RFMMzkthTh7xFNP4G2qn4TjSUSKYBDzX3DGGoMnf1uQi3ec44d7OrFq6aa8XUwxDn+JmWxB9+Gx0NwZAudvTYq1rV0qx6MpILwKSy2Pp69JK+Fkz+IwZA4rlQq2pLm7tgui22QngzG0mPbolMFy1nRsr50C30kxMQOQxfQ9uCzsvgrgx+YwZfg==";
//        System.out.println(new String(decryptPrivateKey(Base64.decodeBase64(str),PRIVATE_KEY)));

//        String regex = "^([A-Z]|[a-z]|[0-9]|[`~!@#$%^&*()+=|{}':;',\\\\\\\\[\\\\\\\\].<>/?~！@#￥%……&*（）――+|{}【】‘；：”“'。，、？]){6,20}$";
//        String regex = "^([A-Za-z]+d+[`~!@#$%^&*()+=|{}':;',\\\\\\\\[\\\\\\\\].<>/?~]){6,20}$";
//        String regex = "[^a-zA-Z+]{6,20}";
//        Pattern pattern = Pattern.compile(regex);
//        System.out.println(pattern.matcher("qqqqqqq").matches());

//        System.out.println(getMac("192.168.1.117"));
//        System.out.println(code);

        //测试
        byte[] bytes = encryptByPublicKey("szf".getBytes(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApmk2cu95+MT/xbBvllnCt87P4jzQWQJTU0JuoxN6JQJWK1CPRllpbRIN1+PqxXri8F8M+sbR05jS4+/zZwSttsBmhZBVhmCmud9StO+UkY8S701CuOR3I//XWOnwDKBz+82OyDTisY2seQ502ZW//sneApTbUwCxWe77upE4Y91ieDm/klQXNIEQBrUi702RaSghFZmWDNyYdCka1juWwXD9p3TU5NcgTY8bvrQNIZ/DE8uV80n1tuThfOSYHOynP7lwzPrUs6t/XqgxiUViPiWXTsil9Bk6xTI+kTXpGwbAgjFUEJTkbUv6KD5Sl8+ylvXYTlbgL7nXk5ExyvSwewIDAQAB");
        System.out.println(Base64.encodeBase64String(bytes));
        String unam1 = new String(RSACoder.decryptPrivateKey(java.util.Base64.getDecoder().decode("kQxM6ruj9VeloCuErMWxdZU6E47qJAQFWd/AARGMPzMnGwU+6/PSPm8XTLAk0b/9En/Ai/rbuSH4dFO30Rhwj06oJRc6nZoGDJNX/HqcdHloO+IpjO7Bi646/dEjHA+G2wz+r7iyX9tmozixO0KbRQxFIm0cxDFanEMpPQzEemjLMlqluUEZvgtZUu6JgtHpREuZOdstV+cEvWhfaUagX+9i/YR4pNwJWrHc06NMvjyi/RXPXroFhTb0oFSs7YN5+7cBRwP1jiPNmDZwYHnhOnAquQT+kvxH6IsRDQ/KWTDCidPqUF0Ax2mIdzSD4z4kQhSkx/3Q4A71E8/ZmyqboA=="), "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds="));
        System.out.println(unam1);
        String unam2 = new String(RSACoder.decryptPrivateKey(bytes, "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds="));
        System.out.println(unam2);
    }

    //2C-4D-54-2E-D1-3A
    public static String getMac(String ip) {
        String str = null;
        String mac = null;
        try {
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader ir = new InputStreamReader(p.getInputStream(), "gbk");
            LineNumberReader input = new LineNumberReader(ir);
            for (; true; ) {
                str = input.readLine();
                if (str != null) {
                    if (str.indexOf("MAC 地址") > 1) {
                        mac = str.substring(str.indexOf("MAC 地址") + 9);
                        break;
                    }
                }
            }
            System.out.println(mac);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mac;
    }


}
