package org.jeecg.modules.task;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.CommonSendStatus;
import org.jeecg.common.constant.GlobalConstants;
import org.jeecg.common.constant.WebsocketConst;
import org.jeecg.common.util.*;
import org.jeecg.common.util.encryption.Sm4Util;
import org.jeecg.modules.demo.poi.view.JeecgTemplateWordView;
import org.jeecg.modules.green.common.entity.*;
import org.jeecg.modules.green.common.service.*;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchange;
import org.jeecg.modules.green.jhxmbgsq.entity.ZgsPlannedprojectchangedetail;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangeService;
import org.jeecg.modules.green.jhxmbgsq.service.IZgsPlannedprojectchangedetailService;
import org.jeecg.modules.green.jhxmcgjj.entity.ZgsPlanresultbase;
import org.jeecg.modules.green.jhxmcgjj.service.IZgsPlanresultbaseService;
import org.jeecg.modules.green.jxzpsq.entity.ZgsPerformancebase;
import org.jeecg.modules.green.jxzpsq.service.IZgsPerformancebaseService;
import org.jeecg.modules.green.kyxmjtsq.entity.ZgsScientificpostbase;
import org.jeecg.modules.green.kyxmjtsq.service.IZgsScientificpostbaseService;
import org.jeecg.modules.green.kyxmjtzs.entity.ZgsSpostcertificatebase;
import org.jeecg.modules.green.kyxmjtzs.service.IZgsSpostcertificatebaseService;
import org.jeecg.modules.green.kyxmsb.entity.ZgsSciencetechfeasible;
import org.jeecg.modules.green.report.mapper.*;
import org.jeecg.modules.green.report.service.*;
import org.jeecg.modules.green.sfxmsb.entity.*;
import org.jeecg.modules.green.sfxmsb.service.*;
import org.jeecg.modules.green.xmyssq.entity.ZgsDemoprojectacceptance;
import org.jeecg.modules.green.xmyssq.entity.ZgsScientificbase;
import org.jeecg.modules.green.xmyssq.service.IZgsDemoprojectacceptanceService;
import org.jeecg.modules.green.xmyssq.service.IZgsScientificbaseService;
import org.jeecg.modules.green.xmyszssq.entity.ZgsExamplecertificatebase;
import org.jeecg.modules.green.xmyszssq.entity.ZgsSacceptcertificatebase;
import org.jeecg.modules.green.xmyszssq.service.IZgsExamplecertificatebaseService;
import org.jeecg.modules.green.xmyszssq.service.IZgsSacceptcertificatebaseService;
import org.jeecg.modules.green.zqcysb.entity.ZgsMidterminspection;
import org.jeecg.modules.green.zqcysb.service.IZgsMidterminspectionService;
import org.jeecg.modules.message.websocket.WebSocket;
import org.jeecg.modules.system.entity.SysAnnouncement;
import org.jeecg.modules.system.entity.SysAnnouncementSend;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.*;
import org.jeecg.modules.utils.HttpClientUtilJK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author ASUS
 * @date 2018/7/25
 */
@Slf4j
@Component
public class SchedulerTask {

    @Value("${jeecg.path.wordTemplate}")
    private String wordTemplate;
    @Value("${jeecg.path.upload}")
    private String upload;
    @Value("${jeecg.smsBaseUrl}")
    private String smsBaseUrl;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IZgsProjectunitmemberService zgsProjectunitmemberService;
    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    @Autowired
    private IZgsAttachappendixService zgsAttachappendixService;
    @Autowired
    private IZgsSciencetechtaskService zgsSciencetechtaskService;
    @Autowired
    private IZgsProjecttaskService zgsProjecttaskService;
    //逾期定时
    @Autowired
    private IZgsSciencetechfeasibleService zgsSciencetechfeasibleService;
    @Autowired
    private IZgsProjectlibraryService zgsProjectlibraryService;
    @Autowired
    private IZgsGreenbuildprojectService zgsGreenbuildprojectService;
    @Autowired
    private IZgsBuildprojectService zgsBuildprojectService;
    @Autowired
    private IZgsSamplebuildprojectService zgsSamplebuildprojectService;
    @Autowired
    private IZgsEnergybuildprojectService zgsEnergybuildprojectService;
    @Autowired
    private IZgsAssembleprojectService zgsAssembleprojectService;
    @Autowired
    private IZgsPerformancebaseService zgsPerformancebaseService;
    @Autowired
    private IZgsScientificpostbaseService zgsScientificpostbaseService;
    @Autowired
    private IZgsMidterminspectionService zgsMidterminspectionService;
    @Autowired
    private IZgsPlannedprojectchangeService zgsPlannedprojectchangeService;
    @Autowired
    private IZgsPlannedprojectchangedetailService zgsPlannedprojectchangedetailService;
    @Autowired
    private IZgsScientificbaseService zgsScientificbaseService;
    @Autowired
    private IZgsDemoprojectacceptanceService zgsDemoprojectacceptanceService;
    @Autowired
    private IZgsSacceptcertificatebaseService zgsSacceptcertificatebaseService;
    @Autowired
    private IZgsExamplecertificatebaseService zgsExamplecertificatebaseService;
    @Autowired
    private IZgsSpostcertificatebaseService zgsSpostcertificatebaseService;
    @Autowired
    private IZgsPlanresultbaseService zgsPlanresultbaseService;
    @Autowired
    private IZgsProjectunitmembersendService zgsProjectunitmembersendService;
    @Autowired
    private IZgsPtempTjService zgsPtempTjService;
    @Autowired
    private ISysAnnouncementService sysAnnouncementService;
    @Autowired
    private ISysAnnouncementSendService sysAnnouncementSendService;
    @Resource
    private WebSocket webSocket;
    @Autowired
    private IZgsReportEnergyExistinfoTotalService zgsReportEnergyExistinfoTotalService;
    @Autowired
    private IZgsReportMonthfabrAreaCityNewconstructionService zgsReportMonthfabrAreaCityNewconstructionService;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 定时任务数据同步区县及市州
     */
    @Scheduled(cron = "0 00 05 ? * *")
    public void dataInitByAreaCode() {
        log.info("定时任务数据同步区县及市州----------start");
        zgsReportMonthfabrAreaCityNewconstructionService.dataInitByAreaCode(null, null);
        log.info("定时任务数据同步区县及市州----------end");
    }

    /**
     * 批量初始化zgs_projectunitmember用户加密密码、角色
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 48 22 * * *")
//    @Scheduled(cron = "0 20 19 * * *")
//    @Scheduled(cron = "0 40 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void initUserAdd() {
        log.info("账号初始化工作----------start");
        QueryWrapper<ZgsProjectunitmember> queryWrapper = new QueryWrapper();
        queryWrapper.eq("is_flag", '6');
//        queryWrapper.eq("id", "012545b5-ft1f-4rt5-bqr5-avcwq1a1001");
//        queryWrapper.ne("isdelete", 1);
//        queryWrapper.isNull("is_flag");
        List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(queryWrapper);
        if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
            for (int i = 0; i < zgsProjectunitmemberList.size(); i++) {
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberList.get(i);
                String accounttype = zgsProjectunitmember.getAccounttype();
                String usertype = zgsProjectunitmember.getUsertype();
                String id = zgsProjectunitmember.getId();
                String salt = oConvertUtils.randomGen(8);
                SysUser user = new SysUser();
                user.setSalt(salt);
                //
//                String pasW = zgsProjectunitmember.getLoginpassword();
                String pasW = "123456";
                if (StringUtils.isEmpty(pasW)) {
                    pasW = PinyinHelperUtils.getPsw(12);
                }
                String passwordEncode = PasswordUtil.encrypt(zgsProjectunitmember.getLoginname(), pasW, salt);
                user.setUsername(zgsProjectunitmember.getLoginname());
                user.setRealname(zgsProjectunitmember.getEnterprisename());
                user.setOrgCode("A01");
                user.setCreateBy("admin");
                user.setCreateTime(new Date());
                user.setPassword(passwordEncode);
                user.setStatus(1);
                user.setDelFlag(0);
                user.setEnterpriseguid(zgsProjectunitmember.getEnterpriseguid());
                user.setAccounttype(zgsProjectunitmember.getAccounttype());
                user.setUsertype(zgsProjectunitmember.getUsertype());
                user.setUnitmemberid(zgsProjectunitmember.getId());
                user.setAreaname(zgsProjectunitmember.getAreaname());
                user.setAreacode(zgsProjectunitmember.getAreacode());
                if ("2".equals(usertype)) {
                    //专家
                    user.setLoginUserType(2);
                    sysUserService.addUserWithRole(user, "1494972462588784641");
                } else {
                    if ("0".equals(accounttype)) {
                        //单位
                        user.setLoginUserType(0);
                        sysUserService.addUserWithRole(user, "1494973276225040385");
                    } else if ("4".equals(accounttype)) {
                        //推荐单位
                        user.setLoginUserType(4);
                        sysUserService.addUserWithRole(user, "1503640381931552769");
                    } else if ("6".equals(accounttype)) {
                        //统计报表县区账号
                        user.setLoginUserType(6);
                        sysUserService.addUserWithRole(user, "1507319687570640898");
                    } else if ("7".equals(accounttype)) {
                        //统计报表市区账号
                        user.setLoginUserType(7);
                        sysUserService.addUserWithRole(user, "1507319783372738561");
                    } else {
                        //个人
                        user.setLoginUserType(1);
                        sysUserService.addUserWithRole(user, "1494972736309063682");
                    }
                }
                ZgsProjectunitmember projectunitmember = new ZgsProjectunitmember();
                projectunitmember.setId(id);
//                if (StringUtils.isEmpty(zgsProjectunitmember.getLoginpassword())) {
                projectunitmember.setLoginpassword(pasW);
                projectunitmember.setIsFlag(7);
//                }
                zgsProjectunitmemberService.updateById(projectunitmember);
            }
        }
        log.info("账号初始化工作----------end");
    }

    /**
     * 批量初始化统计报表用户修改或添加操作
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 05 19 * * *")
//    @Scheduled(cron = "0 01 19 * * *")
//    @Scheduled(cron = "0 14 01 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void initReportUserAddOrUpdate() {
        log.info("批量初始化统计报表用户修改或添加操作----------start");
        QueryWrapper<ZgsPtempTj> queryWrapper = new QueryWrapper();
        queryWrapper.isNull("is_flag");
        queryWrapper.orderByAsc("areacode");
        List<ZgsPtempTj> zgsPtempTjList = zgsPtempTjService.list(queryWrapper);
        if (zgsPtempTjList.size() > 0) {
            //AT002000
            int count = 2000;
            for (int i = 0; i < zgsPtempTjList.size(); i++) {
                ZgsPtempTj zgsPtempTj = zgsPtempTjList.get(i);
                String areacode = zgsPtempTj.getAreacode();
                String areaname = zgsPtempTj.getAreaname();
                log.info(areaname + "----------" + areacode);
                ZgsPtempTj tj = new ZgsPtempTj();
                //县区
                //先从用户表查询，是否有区县账号，条件是没有被执行的is_flag为空
                QueryWrapper<SysUser> userWrapper = new QueryWrapper();
                userWrapper.isNull("is_flag");
                userWrapper.eq("areacode", areacode);
                List<SysUser> userList = sysUserService.list(userWrapper);
                SysUser sysUser = null;
                if (userList.size() > 0) {
                    //如果有修改用户表和用户单位关联表
                    sysUser = userList.get(0);
                    SysUser user = new SysUser();
                    user.setId(sysUser.getId());
                    user.setIsFlag("1");
                    user.setRealname(zgsPtempTj.getName());
                    user.setAreaname(areaname);
                    ZgsProjectunitmember zgsProjectunitmember = new ZgsProjectunitmember();
                    if ("0".equals(zgsPtempTj.getAreatype())) {
                        user.setAccounttype("6");
                        zgsProjectunitmember.setAccounttype("6");
                        user.setLoginUserType(6);
                    } else {
                        user.setAccounttype("7");
                        zgsProjectunitmember.setAccounttype("7");
                        user.setLoginUserType(7);
                    }
                    sysUserService.updateById(user);
                    //
                    zgsProjectunitmember.setId(sysUser.getUnitmemberid());
                    zgsProjectunitmember.setEnterprisename(zgsPtempTj.getName());
                    zgsProjectunitmember.setMobilephone(zgsPtempTj.getPhone());
                    zgsProjectunitmember.setEmail(zgsPtempTj.getEmail());
                    zgsProjectunitmember.setAreaname(areaname);
                    zgsProjectunitmember.setAreacode(areacode);
                    zgsProjectunitmember.setEmployunit(zgsPtempTj.getBm());
                    zgsProjectunitmember.setDepartment(zgsPtempTj.getRy() + "-" + zgsPtempTj.getZw());
                    zgsProjectunitmemberService.updateById(zgsProjectunitmember);
                } else {
                    //否则新建用户和用户单位关联表
                    ++count;
                    sysUser = new SysUser();
                    String id = UUID.randomUUID().toString();
                    String uid = UUID.randomUUID().toString();
                    String eid = UUID.randomUUID().toString();
                    sysUser.setId(id);
                    String salt = oConvertUtils.randomGen(8);
                    String pasW = "123456";
                    String uname = "AT00" + count;
                    String passwordEncode = PasswordUtil.encrypt(uname, pasW, salt);
                    sysUser.setSalt(salt);
                    sysUser.setUsername(uname);
                    sysUser.setRealname(zgsPtempTj.getName());
                    sysUser.setOrgCode("A01");
                    sysUser.setCreateBy("admin");
                    sysUser.setCreateTime(new Date());
                    sysUser.setPassword(passwordEncode);
                    sysUser.setStatus(1);
                    sysUser.setDelFlag(0);
                    sysUser.setUsertype("1");
                    sysUser.setUnitmemberid(uid);
                    sysUser.setEnterpriseguid(eid);
                    sysUser.setAreaname(areaname);
                    sysUser.setAreacode(areacode);
                    sysUser.setIsFlag("1");
                    String roles = null;
                    ZgsProjectunitmember member = new ZgsProjectunitmember();
                    if ("0".equals(zgsPtempTj.getAreatype())) {
                        sysUser.setLoginUserType(6);
                        sysUser.setAccounttype("6");
                        member.setAccounttype("6");
                        roles = "1507319687570640898";
                    } else {
                        sysUser.setLoginUserType(7);
                        sysUser.setAccounttype("7");
                        member.setAccounttype("7");
                        roles = "1507319783372738561";
                    }
                    sysUserService.addUserWithRole(sysUser, roles);
                    member.setId(uid);
                    member.setLoginname(uname);
                    member.setLoginpassword(pasW);
                    member.setEnterpriseguid(eid);
                    member.setEnterprisename(zgsPtempTj.getName());
                    member.setMobilephone(zgsPtempTj.getPhone());
                    member.setEmail(zgsPtempTj.getEmail());
                    member.setAreaname(areaname);
                    member.setAreacode(areacode);
                    member.setEmployunit(zgsPtempTj.getBm());
                    member.setDepartment(zgsPtempTj.getRy() + "-" + zgsPtempTj.getZw());
                    //
                    member.setStatus("0");
                    member.setCreatedate(new Date());
                    member.setCreateaccountname("管理员");
                    member.setCreateusername("管理员");
                    member.setUsertype("1");
                    zgsProjectunitmemberService.save(member);
                }
                tj.setId(zgsPtempTj.getId());
                tj.setIsFlag("1");
                tj.setUserid(sysUser.getId());
                tj.setUsername(sysUser.getUsername());
                zgsPtempTjService.updateById(tj);
            }
        }
        log.info("批量初始化统计报表用户修改或添加操作----------end");
    }

    /**
     * 批量初始化zgs_projectunitmember用户密码
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 32 21 * * *")
//    @Scheduled(cron = "0 20 18 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void initUserPwd() {
        log.info("账号初始化工作----------start");
        QueryWrapper<ZgsProjectunitmember> queryWrapper = new QueryWrapper();
        queryWrapper.eq("is_flag", '5');
//        queryWrapper.ne("isdelete", 1);
//        queryWrapper.ne("is_flag", '3');
        List<ZgsProjectunitmember> zgsProjectunitmemberList = zgsProjectunitmemberService.list(queryWrapper);
        if (zgsProjectunitmemberList != null && zgsProjectunitmemberList.size() > 0) {
            for (int i = 0; i < zgsProjectunitmemberList.size(); i++) {
                ZgsProjectunitmember zgsProjectunitmember = zgsProjectunitmemberList.get(i);
                String id = zgsProjectunitmember.getId();
                log.info("Projectunitmember主键=" + id);
                String salt = oConvertUtils.randomGen(8);
                String pasW = PinyinHelperUtils.getPswInitAppend();
                String passwordEncode = PasswordUtil.encrypt(zgsProjectunitmember.getLoginname(), pasW, salt);
                ZgsProjectunitmember projectunitmember = new ZgsProjectunitmember();
                projectunitmember.setId(id);
                projectunitmember.setLoginpassword(pasW);
                zgsProjectunitmemberService.updateById(projectunitmember);
                UpdateWrapper<SysUser> updateWrapper = new UpdateWrapper();
                updateWrapper.eq("unitmemberid", id);
                SysUser user = new SysUser();
                user.setSalt(salt);
                user.setPassword(passwordEncode);
                sysUserService.update(user, updateWrapper);
            }
        }
        log.info("账号初始化工作----------end");
    }

    /**
     * 批量初始化统一认证账号用户加密密码、角色
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 05 00 * * *")
//    @Scheduled(cron = "0 56 17 * * *")
//    @Scheduled(cron = "0 57 01 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    //login_user_type=3,密码Greensjk@835290
    //admin管理员权限,密码Greensjk!386490
    public void initAuthCodeUserAdd() {
        log.info("统一认证账号初始化工作----------start");
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
//        queryWrapper.ne("login_user_type", 2);
//        queryWrapper.ne("login_user_type", 3);
//        queryWrapper.isNull("password");
//        queryWrapper.eq("login_user_type", 3);
//        queryWrapper.eq("id", "e9ca23d68d884d4ebb19d07889727dae");
        List<SysUser> userList = sysUserService.list(queryWrapper);
        if (userList != null && userList.size() > 0) {
            for (int i = 0; i < userList.size(); i++) {
                SysUser sysUser = userList.get(i);
                log.info("sysUser=" + sysUser.getId());
                String salt = oConvertUtils.randomGen(8);
                SysUser user = new SysUser();
                user.setId(sysUser.getId());
                user.setSalt(salt);
                //Greensjk@1359、123456@Aa
                String passwordEncode = PasswordUtil.encrypt(Sm4Util.decryptEcb(sysUser.getUsername()), "Greensjk@1359", salt);
//                String passwordEncode = PasswordUtil.encrypt(sysUser.getUsername(), "Greensjk@835290", salt);
//                String passwordEncode = PasswordUtil.encrypt(sysUser.getUsername(), "Greensjk!386490", salt);
//                user.setUpdateBy("admin");
//                user.setUpdateTime(new Date());
                user.setPassword(passwordEncode);
                sysUserService.updateById(user);
                //住建厅角色
//                SysUserRole userRole = new SysUserRole(sysUser.getId(), "1494974004406546434");
                //个人角色
//                SysUserRole userRole = new SysUserRole(sysUser.getId(), "1494972736309063682");
//                sysUserRoleService.save(userRole);
            }
        }
        log.info("统一认证账号初始化工作----------end");
    }

    /**
     * 批量下载文件
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 04 22 * * *")
//    @Scheduled(cron = "0 20 19 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void iniDownLoadFile() {
        log.info("批量下载文件----------start");
        QueryWrapper<ZgsAttachappendix> queryWrapper = new QueryWrapper();
        queryWrapper.isNotNull("appendixpath");
        queryWrapper.isNull("isflag");
//        queryWrapper.apply("date_format(uploaddate,'%Y-%m-%d')>{0}", "2022-01-11");
        List<ZgsAttachappendix> zgsAttachappendixList = zgsAttachappendixService.list(queryWrapper);
        if (zgsAttachappendixList != null && zgsAttachappendixList.size() > 0) {
            ZgsAttachappendix zgsAttachappendix = null;
            ZgsAttachappendix attachappendix = null;
            for (int i = 0; i < zgsAttachappendixList.size(); i++) {
                zgsAttachappendix = zgsAttachappendixList.get(i);
//                if (DownloadFile.downLoadFileList(zgsAttachappendix.getAppendixpath())) {
//                    log.info(zgsAttachappendixList.size() + "，已下载" + i);
//                    attachappendix = new ZgsAttachappendix();
//                    attachappendix.setId(zgsAttachappendix.getId());
//                    attachappendix.setIsflag(1);
//                    zgsAttachappendixService.updateById(attachappendix);
//                } else {
//                    log.info("批量下载文件报错----------id=" + zgsAttachappendix.getId());
//                }
                attachappendix = new ZgsAttachappendix();
                attachappendix.setId(zgsAttachappendix.getId());
                //转换url附件地址
                if (StringUtils.isNotEmpty(zgsAttachappendix.getAppendixpath())) {
                    int position = zgsAttachappendix.getAppendixpath().lastIndexOf("\\");
                    if (position < 0) {
                        position = zgsAttachappendix.getAppendixpath().lastIndexOf("/");
                    }
                    log.info("替换文件路径----------fileUrl开始=" + position + "-" + zgsAttachappendix.getAppendixpath());
                    String fileUrl = "green_file\\2022-06-09" + zgsAttachappendix.getAppendixpath().substring(position);
                    attachappendix.setAppendixpath(fileUrl);
                    attachappendix.setIsflag(1);
                    zgsAttachappendixService.updateById(attachappendix);
                    log.info("替换文件路径----------fileUrl结束=" + fileUrl);
                }
            }
        }
        log.info("批量下载文件----------end");
    }

    /**
     * 批量导出word文件
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 18 01 * * *")
//    @Scheduled(cron = "0 42 13 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void exportWordFile() {
        log.info("批量导出word文件----------start");
        QueryWrapper<ZgsSciencetechtask> queryWrapper0 = new QueryWrapper<ZgsSciencetechtask>();
        queryWrapper0.eq("status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper0.ne("isdelete", 1);
        queryWrapper0.isNotNull("projectname");
        List<ZgsSciencetechtask> list0 = zgsSciencetechtaskService.list(queryWrapper0);
        if (list0 != null && list0.size() > 0) {
            for (ZgsSciencetechtask zgsSciencetechtask : list0) {
                zgsSciencetechtaskService.initProjectSelectById(zgsSciencetechtask);
                String fileName = zgsSciencetechtask.getProjectname();
                if (StringUtils.isNotEmpty(fileName)) {
                    String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "9科技项目任务书.docx", BeanUtil.beanToMap(zgsSciencetechtask));
                    ZgsSciencetechtask sciencetechtask = new ZgsSciencetechtask();
                    sciencetechtask.setId(zgsSciencetechtask.getId());
                    sciencetechtask.setPdfUrl(filePath);
                    zgsSciencetechtaskService.updateById(sciencetechtask);
                }
            }
        }
        QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<ZgsProjecttask>();
        queryWrapper1.eq("status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper1.ne("isdelete", 1);
        List<ZgsProjecttask> list1 = zgsProjecttaskService.list(queryWrapper1);
        if (list1 != null && list1.size() > 0) {
            for (ZgsProjecttask zgsProjecttask : list1) {
                zgsProjecttaskService.initProjectSelectById(zgsProjecttask);
                ZgsProjectlibrary zgsProjectlibrary = zgsProjecttask.getZgsProjectlibrary();
                if (zgsProjectlibrary != null) {
                    String fileName = zgsProjectlibrary.getProjectname();
                    if (StringUtils.isNotEmpty(fileName)) {
                        String filePath = new JeecgTemplateWordView().exportWordOut(fileName + ".doc", wordTemplate, upload, "8建设厅任务书-示范工程.docx", BeanUtil.beanToMap(zgsProjecttask));
                        ZgsProjecttask projecttask = new ZgsProjecttask();
                        projecttask.setId(zgsProjecttask.getId());
                        projecttask.setPdfUrl(filePath);
                        zgsProjecttaskService.updateById(projecttask);
                    }
                }
            }
        }
        log.info("批量导出word文件----------end");
    }

    /**
     * 定时备份mysql数据库
     */
    @Scheduled(cron = "0 00 05 * * *")
    public void mysqlDataBackTask() {
        log.info("定时备份mysql数据库----------start");
//        String result = LibreOfficeUtil.executeCommand("/usr/bin/mysqldump -h172.17.34.31 -P15307 -uqidi -p'bT+ej.@y$7ap' --set-gtid-purged=off --triggers --routines  --databases greensjk > /mnt/upload/data/greensjk_$(date +%Y%m%d_%H%M%S).sql");
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", "mysqldump -h172.17.34.31 -P15307 -uqidi -p'bT+ej.@y$7ap' --set-gtid-purged=off --triggers --routines  --databases greensjk > /mnt/upload/data/greensjk_$(date +%Y%m%d_%H%M%S).sql"});
            log.info("process.waitFor()=" + process.waitFor());
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("定时备份mysql数据库----------end");
    }

    /**
     * 定时扫描：超过项目截止时间6个月的项目进行强制终止执行操作
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 01 06 * * *")
//    @Scheduled(cron = "0 49 16 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void dealProjectDateInit() {
        log.info("超过项目截止时间6个月的项目进行强制终止执行操作----------start");
        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -6);
        //1、科研
        QueryWrapper<ZgsSciencetechfeasible> queryWrapper1 = new QueryWrapper();
        queryWrapper1.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper1.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsSciencetechfeasible> sciencetechfeasibleList = zgsSciencetechfeasibleService.list(queryWrapper1);
        if (sciencetechfeasibleList != null && sciencetechfeasibleList.size() > 0) {
            for (ZgsSciencetechfeasible zgsSciencetechfeasible : sciencetechfeasibleList) {
                if (dealProjectDateCompare(zgsSciencetechfeasible.getProjectname(), zgsSciencetechfeasible.getId())) {
                    ZgsSciencetechfeasible upInfo = new ZgsSciencetechfeasible();
                    upInfo.setId(zgsSciencetechfeasible.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("1、科研超时项目ID--------" + zgsSciencetechfeasible.getId());
                    zgsSciencetechfeasibleService.updateById(upInfo);
                    //8.1
                    QueryWrapper<ZgsSacceptcertificatebase> updateWrapper1 = new QueryWrapper();
                    ZgsSacceptcertificatebase upInfo1 = new ZgsSacceptcertificatebase();
                    upInfo1.setProjectlibraryguid(zgsSciencetechfeasible.getId());
                    upInfo1.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("8.1、科研项目验收证书--------" + zgsSciencetechfeasible.getId());
                    zgsSacceptcertificatebaseService.update(upInfo1, updateWrapper1);
                    //9
                    QueryWrapper<ZgsSpostcertificatebase> updateWrapper2 = new QueryWrapper();
                    ZgsSpostcertificatebase upInfo2 = new ZgsSpostcertificatebase();
                    upInfo2.setProjectlibraryguid(zgsSciencetechfeasible.getId());
                    upInfo2.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("9、科研项目结题证书--------" + zgsSciencetechfeasible.getId());
                    zgsSpostcertificatebaseService.update(upInfo2, updateWrapper2);
                }
            }
        }
        //2、示范
        QueryWrapper<ZgsProjectlibrary> queryWrapper2 = new QueryWrapper();
        queryWrapper2.apply("date_format(completedate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsProjectlibrary> zgsProjectlibraryList = zgsProjectlibraryService.list(queryWrapper2);
        if (zgsProjectlibraryList != null && zgsProjectlibraryList.size() > 0) {
            for (ZgsProjectlibrary zgsProjectlibrary : zgsProjectlibraryList) {
                if (dealProjectDateCompare(zgsProjectlibrary.getProjectname(), zgsProjectlibrary.getId())) {
                    if (StringUtils.isNotEmpty(zgsProjectlibrary.getProjecttypenum())) {
                        if (GlobalConstants.GreenBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsGreenbuildproject> wrapper1 = new QueryWrapper();
                            wrapper1.eq("projectlibraryguid", zgsProjectlibrary.getId());
                            ZgsGreenbuildproject zgsGreenbuildproject = zgsGreenbuildprojectService.getOne(wrapper1);
                            ZgsGreenbuildproject upInfo = new ZgsGreenbuildproject();
                            upInfo.setId(zgsGreenbuildproject.getId());
                            upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                            log.info("2、绿色建筑示范ID--------" + zgsGreenbuildproject.getId());
                            zgsGreenbuildprojectService.updateById(upInfo);
                        } else if (GlobalConstants.ConstructBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsBuildproject> wrapper2 = new QueryWrapper();
                            wrapper2.eq("projectlibraryguid", zgsProjectlibrary.getId());
                            ZgsBuildproject zgsBuildproject = zgsBuildprojectService.getOne(wrapper2);
                            ZgsBuildproject upInfo = new ZgsBuildproject();
                            upInfo.setId(zgsBuildproject.getId());
                            upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                            log.info("2、建筑工程示范--------" + zgsBuildproject.getId());
                            zgsBuildprojectService.updateById(upInfo);
                        } else if (GlobalConstants.AssemBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsAssembleproject> wrapper3 = new QueryWrapper();
                            wrapper3.eq("projectlibraryguid", zgsProjectlibrary.getId());
                            ZgsAssembleproject zgsAssembleproject = zgsAssembleprojectService.getOne(wrapper3);
                            ZgsAssembleproject upInfo = new ZgsAssembleproject();
                            upInfo.setId(zgsAssembleproject.getId());
                            upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                            log.info("2、装配式建筑示范--------" + zgsAssembleproject.getId());
                            zgsAssembleprojectService.updateById(upInfo);
                        } else if (GlobalConstants.EnergyBuildType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsEnergybuildproject> wrapper4 = new QueryWrapper();
                            wrapper4.eq("projectlibraryguid", zgsProjectlibrary.getId());
                            ZgsEnergybuildproject zgsEnergybuildproject = zgsEnergybuildprojectService.getOne(wrapper4);
                            ZgsEnergybuildproject upInfo = new ZgsEnergybuildproject();
                            upInfo.setId(zgsEnergybuildproject.getId());
                            upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                            log.info("2、建筑节能示范--------" + zgsEnergybuildproject.getId());
                            zgsEnergybuildprojectService.updateById(upInfo);
                        } else if (GlobalConstants.SampleBuidType.equals(zgsProjectlibrary.getProjecttypenum())) {
                            QueryWrapper<ZgsSamplebuildproject> wrapper5 = new QueryWrapper();
                            wrapper5.eq("projectlibraryguid", zgsProjectlibrary.getId());
                            ZgsSamplebuildproject zgsSamplebuildproject = zgsSamplebuildprojectService.getOne(wrapper5);
                            ZgsSamplebuildproject upInfo = new ZgsSamplebuildproject();
                            upInfo.setId(zgsSamplebuildproject.getId());
                            upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                            log.info("2、市政工程示范--------" + zgsSamplebuildproject.getId());
                            zgsSamplebuildprojectService.updateById(upInfo);
                        }
                    }
                }
            }
        }
        //3.1、科技项目任务书
        QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
        queryWrapper3.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper3.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
        if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
            for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                if (dealProjectDateCompare(zgsSciencetechtask.getProjectname(), zgsSciencetechtask.getSciencetechguid())) {
                    ZgsSciencetechtask upInfo = new ZgsSciencetechtask();
                    upInfo.setId(zgsSciencetechtask.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("3.1、科技项目任务书--------" + zgsSciencetechtask.getId());
                    zgsSciencetechtaskService.updateById(upInfo);
                }
            }
        }
        //3.2、示范项目任务书
        QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
        queryWrapper32.ne("t.status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper32.apply("date_format(l.completedate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
        if (listInfoList != null && listInfoList.size() > 0) {
            for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : listInfoList) {
                if (dealProjectDateCompare(zgsProjectlibraryListInfo.getProjectname(), zgsProjectlibraryListInfo.getBid())) {
                    ZgsProjecttask upInfo = new ZgsProjecttask();
                    upInfo.setId(zgsProjectlibraryListInfo.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("3.2、示范项目任务书--------" + zgsProjectlibraryListInfo.getId());
                    zgsProjecttaskService.updateById(upInfo);
                }
            }
        }
        //4、科研项目结题
        QueryWrapper<ZgsScientificpostbase> queryWrapper4 = new QueryWrapper();
        queryWrapper4.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper4.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsScientificpostbase> zgsScientificpostbaseList = zgsScientificpostbaseService.list(queryWrapper4);
        if (zgsScientificpostbaseList != null && zgsScientificpostbaseList.size() > 0) {
            for (ZgsScientificpostbase zgsScientificpostbase : zgsScientificpostbaseList) {
                if (dealProjectDateCompare(zgsScientificpostbase.getProjectname(), zgsScientificpostbase.getScientificbaseguid())) {
                    ZgsScientificpostbase upInfo = new ZgsScientificpostbase();
                    upInfo.setId(zgsScientificpostbase.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("4、科研项目结题--------" + zgsScientificpostbase.getId());
                    zgsScientificpostbaseService.updateById(upInfo);
                }
            }
        }
        //5、项目中期查验
        QueryWrapper<ZgsMidterminspection> queryWrapper5 = new QueryWrapper();
        queryWrapper5.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper5.apply("date_format(projectenddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsMidterminspection> zgsMidterminspectionList = zgsMidterminspectionService.list(queryWrapper5);
        if (zgsMidterminspectionList != null && zgsMidterminspectionList.size() > 0) {
            for (ZgsMidterminspection zgsMidterminspection : zgsMidterminspectionList) {
                if (dealProjectDateCompare(zgsMidterminspection.getProjectname(), zgsMidterminspection.getProjectlibraryguid())) {
                    ZgsMidterminspection upInfo = new ZgsMidterminspection();
                    upInfo.setId(zgsMidterminspection.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("5、项目中期查验--------" + zgsMidterminspection.getId());
                    zgsMidterminspectionService.updateById(upInfo);
                }
            }
        }
        //6、计划项目变更
        QueryWrapper<ZgsPlannedprojectchange> queryWrapper6 = new QueryWrapper();
        queryWrapper6.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper6.apply("date_format(researchenddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsPlannedprojectchange> zgsPlannedprojectchangeList = zgsPlannedprojectchangeService.list(queryWrapper6);
        if (zgsPlannedprojectchangeList != null && zgsPlannedprojectchangeList.size() > 0) {
            for (ZgsPlannedprojectchange zgsMidterminspection : zgsPlannedprojectchangeList) {
                if (dealProjectDateCompare(zgsMidterminspection.getProjectname(), zgsMidterminspection.getProjectlibraryguid())) {
                    ZgsPlannedprojectchange upInfo = new ZgsPlannedprojectchange();
                    upInfo.setId(zgsMidterminspection.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("6、计划项目变更--------" + zgsMidterminspection.getId());
                    zgsPlannedprojectchangeService.updateById(upInfo);
                }
            }
        }
        //7.1、科研项目验收
        QueryWrapper<ZgsScientificbase> queryWrapper7 = new QueryWrapper();
        queryWrapper7.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper7.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsScientificbase> zgsScientificbaseList = zgsScientificbaseService.list(queryWrapper7);
        if (zgsScientificbaseList != null && zgsScientificbaseList.size() > 0) {
            for (ZgsScientificbase zgsScientificbase : zgsScientificbaseList) {
                if (dealProjectDateCompare(zgsScientificbase.getProjectname(), zgsScientificbase.getProjectlibraryguid())) {
                    ZgsScientificbase upInfo = new ZgsScientificbase();
                    upInfo.setId(zgsScientificbase.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("7.1、科研项目验收--------" + zgsScientificbase.getId());
                    zgsScientificbaseService.updateById(upInfo);
                }
            }
        }
        //7.2、示范项目验收
        QueryWrapper<ZgsDemoprojectacceptance> queryWrapper72 = new QueryWrapper();
        queryWrapper72.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper72.apply("date_format(projectenddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper72);
        if (zgsDemoprojectacceptanceList != null && zgsDemoprojectacceptanceList.size() > 0) {
            for (ZgsDemoprojectacceptance zgsDemoprojectacceptance : zgsDemoprojectacceptanceList) {
                if (dealProjectDateCompare(zgsDemoprojectacceptance.getProjectname(), zgsDemoprojectacceptance.getProjectlibraryguid())) {
                    ZgsDemoprojectacceptance upInfo = new ZgsDemoprojectacceptance();
                    upInfo.setId(zgsDemoprojectacceptance.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("7.2、示范项目验收--------" + zgsDemoprojectacceptance.getId());
                    zgsDemoprojectacceptanceService.updateById(upInfo);
                }
            }
        }
        //8.1、科研项目验收证书，提取到科研申报项目中去了
        //8.2、示范项目验收证书
        QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
        queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper82.apply("date_format(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
        if (zgsExamplecertificatebaseList != null && zgsExamplecertificatebaseList.size() > 0) {
            for (ZgsExamplecertificatebase zgsExamplecertificatebase : zgsExamplecertificatebaseList) {
                if (dealProjectDateCompare(zgsExamplecertificatebase.getProjectname(), zgsExamplecertificatebase.getProjectlibraryguid())) {
                    ZgsExamplecertificatebase upInfo = new ZgsExamplecertificatebase();
                    upInfo.setId(zgsExamplecertificatebase.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("8.2、示范项目验收证书--------" + zgsExamplecertificatebase.getId());
                    zgsExamplecertificatebaseService.updateById(upInfo);
                }
            }
        }
        //9、科研项目结题证书，提取到科研申报项目中去了
        //10、示范项目验收证书
        QueryWrapper<ZgsPlanresultbase> queryWrapper10 = new QueryWrapper();
        queryWrapper82.ne("status", GlobalConstants.SHENHE_STATUS7);
        queryWrapper82.apply("enddate(enddate,'%Y-%m-%d')<{0}", DateUtils.formatDate(calendar.getTime()));
        List<ZgsPlanresultbase> zgsPlanresultbaseList = zgsPlanresultbaseService.list(queryWrapper10);
        if (zgsPlanresultbaseList != null && zgsPlanresultbaseList.size() > 0) {
            for (ZgsPlanresultbase zgsPlanresultbase : zgsPlanresultbaseList) {
                if (dealProjectDateCompare(zgsPlanresultbase.getResultname(), zgsPlanresultbase.getProjectlibraryguid())) {
                    ZgsPlanresultbase upInfo = new ZgsPlanresultbase();
                    upInfo.setId(zgsPlanresultbase.getId());
                    upInfo.setStatus(GlobalConstants.SHENHE_STATUS7);
                    log.info("10、示范项目验收证书--------" + zgsPlanresultbase.getId());
                    zgsPlanresultbaseService.updateById(upInfo);
                }
            }
        }
        log.info("超过项目截止时间6个月的项目进行强制终止执行操作----------end");
    }

    /**
     * 所有超时项目与绩效自评做比较
     *
     * @return
     */
    private boolean dealProjectDateCompare(String projectname, String baseguid) {
        if (StringUtils.isNotEmpty(projectname)) {
            QueryWrapper<ZgsPerformancebase> queryWrapper = new QueryWrapper();
            queryWrapper.and(qwp1 -> {
                qwp1.or(qwp2 -> {
                    qwp2.eq("projectname", projectname.trim());
                });
                qwp1.or(qwp2 -> {
                    qwp2.eq("baseguid", baseguid);
                });
            });
            queryWrapper.ne("status", GlobalConstants.SHENHE_STATUS8);
            List<ZgsPerformancebase> list = zgsPerformancebaseService.list(queryWrapper);
            if (list != null && list.size() > 0) {
                return true;
            }
        }
//        return false;
        return true;
    }

    /**
     * 每日定时更新项目当前阶段状态
     */
//    @Scheduled(cron = "0/60 * * * * ?")
    @Scheduled(cron = "0 45 00 * * *")
//    @Scheduled(cron = "0 30 00 * * *")
//    @Scheduled(cron = "0 10 04 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void updateProjectStepStatusValue() {
        zgsProjectlibraryService.initStageAndStatus(null);
    }

    /**
     * 定时扫描：临近截止日期1个月做预警提醒
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 01 06 * * *")
//    @Scheduled(cron = "0 01 19 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void dealProjectDateInitOne() {
        log.info("临近截止日期1个月做预警提醒----------start");
        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        //3.1、科技项目任务书
        QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
        queryWrapper3.between("enddate", DateUtils.formatDate(date), DateUtils.formatDate(calendar.getTime()));
        List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
        if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
            for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                //3天再次发送
                if (!redisUtil.hasKey(RedisUtil.DELAY_PREFIX + zgsSciencetechtask.getId())) {
                    redisUtil.set(RedisUtil.DELAY_PREFIX + zgsSciencetechtask.getId(), zgsSciencetechtask.getProjectname(), 259200);
                    String enterpriseguid = zgsSciencetechtask.getEnterpriseguid();
                    QueryWrapper<ZgsSacceptcertificatebase> updateWrapper81 = new QueryWrapper();
                    updateWrapper81.and(w81 -> {
                        w81.ne("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid());
                    ZgsSacceptcertificatebase zgsSacceptcertificatebase = zgsSacceptcertificatebaseService.getOne(updateWrapper81);
                    QueryWrapper<ZgsSpostcertificatebase> updateWrapper83 = new QueryWrapper();
                    updateWrapper83.and(w83 -> {
                        w83.ne("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper83.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid());
                    ZgsSpostcertificatebase zgsSpostcertificatebase = zgsSpostcertificatebaseService.getOne(updateWrapper83);
                    if (zgsSacceptcertificatebase != null || zgsSpostcertificatebase != null) {
                        warnOneMonth(enterpriseguid, zgsSciencetechtask.getProjectname());
                        if (zgsSacceptcertificatebase != null) {
                            log.info("科研验收证书=" + zgsSacceptcertificatebase.getId());
                        }
                        if (zgsSpostcertificatebase != null) {
                            log.info("科研结题证书=" + zgsSpostcertificatebase.getId());
                        }
                    }
                }
            }
        }
        //3.2、示范项目任务书
        QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
        queryWrapper32.between("l.completedate", DateUtils.formatDate(date), DateUtils.formatDate(calendar.getTime()));
        List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
        if (listInfoList != null && listInfoList.size() > 0) {
            for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : listInfoList) {
                //3天再次发送
                if (!redisUtil.hasKey(RedisUtil.DELAY_PREFIX + zgsProjectlibraryListInfo.getId())) {
                    redisUtil.set(RedisUtil.DELAY_PREFIX + zgsProjectlibraryListInfo.getId(), zgsProjectlibraryListInfo.getProjectname(), 259200);
                    String enterpriseguid = zgsProjectlibraryListInfo.getEnterpriseguid();
                    QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
                    queryWrapper82.and(w82 -> {
                        w82.ne("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    queryWrapper82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid());
                    ZgsExamplecertificatebase zgsExamplecertificatebase = zgsExamplecertificatebaseService.getOne(queryWrapper82);
                    if (zgsExamplecertificatebase != null) {
//                        warnOneMonth(enterpriseguid, zgsProjectlibraryListInfo.getProjectname());
                    }
                }
            }
        }
        log.info("临近截止日期1个月做预警提醒----------end");
    }

    private void warnOneMonth(String enterpriseguid, String projectname) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
        queryWrapper.eq("enterpriseguid", enterpriseguid);
        SysUser user = sysUserService.getOne(queryWrapper);
        if (user != null) {
            String id = UUID.randomUUID().toString();
            SysAnnouncement sysAnnouncement = new SysAnnouncement();
            sysAnnouncement.setId(id);
            sysAnnouncement.setTitile("预警提醒");
            sysAnnouncement.setMsgContent(projectname + "，临近截止日期1个月做预警提醒！");
            sysAnnouncement.setStartTime(new Date());
            sysAnnouncement.setEndTime(new Date());
            sysAnnouncement.setSender("系统管理员");
            sysAnnouncement.setPriority("L");
            sysAnnouncement.setMsgCategory("2");
            sysAnnouncement.setMsgType("USER");
            sysAnnouncement.setDelFlag("0");
            sysAnnouncement.setSendStatus(CommonSendStatus.PUBLISHED_STATUS_1);
            sysAnnouncement.setSendTime(new Date());
            sysAnnouncement.setUserIds(user.getId());
            sysAnnouncement.setMsgAbstract("");
            sysAnnouncementService.save(sysAnnouncement);
            //
            // 2.插入用户通告阅读标记表记录
            String userId = sysAnnouncement.getUserIds();
            String anntId = sysAnnouncement.getId();
            Date refDate = new Date();
            SysAnnouncementSend announcementSend = new SysAnnouncementSend();
            announcementSend.setAnntId(anntId);
            announcementSend.setUserId(userId);
            announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
            announcementSend.setReadTime(refDate);
            sysAnnouncementSendService.save(announcementSend);
            JSONObject obj = new JSONObject();
            obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
            obj.put(WebsocketConst.MSG_ID, sysAnnouncement.getId());
            obj.put(WebsocketConst.MSG_TXT, sysAnnouncement.getTitile());
            webSocket.sendMessage(userId, obj.toJSONString());
            log.info("临近截止日期1个月做预警提醒----------发送消息userId=" + user.getId());
        }
    }

    /**
     * 定时扫描：临近一个月打标签
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 55 10 * * *")
//    @Scheduled(cron = "0 00 04 * * *")
    @Scheduled(cron = "0 30 04 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void dealProjectDateInitZero() {
        log.info("临近一个月打标签----------start");
        //全部清空逾期状态
        zgsSciencetechtaskService.updateDealTypeIsNull();
        //
        Date date = new Date();//获取当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 1);
        //3.1、科技项目任务书
        QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
        queryWrapper3.eq("status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper3.between("enddate", DateUtils.formatDate(date), DateUtils.formatDate(calendar.getTime()));
        queryWrapper3.and(qwr -> {
            qwr.ne("ishistory", 1).or().isNull("ishistory");
        });
//            queryWrapper3.isNull("isdealoverdue");
        List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper3);
        if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
            ZgsSciencetechtask sciencetechtask = null;
            for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                sciencetechtask = new ZgsSciencetechtask();
                QueryWrapper<ZgsSacceptcertificatebase> updateWrapper81 = new QueryWrapper();
                updateWrapper81.and(w81 -> {
                    w81.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
                });
                updateWrapper81.and(w81 -> {
                    w81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                });
                List<ZgsSacceptcertificatebase> zgsSacceptcertificatebaseList = zgsSacceptcertificatebaseService.list(updateWrapper81);
                QueryWrapper<ZgsSpostcertificatebase> updateWrapper83 = new QueryWrapper();
                updateWrapper83.and(w83 -> {
                    w83.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
                });
                updateWrapper83.and(w83 -> {
                    w83.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                });
                List<ZgsSpostcertificatebase> zgsSpostcertificatebaseList = zgsSpostcertificatebaseService.list(updateWrapper83);
                if (zgsSacceptcertificatebaseList.size() == 0 || zgsSpostcertificatebaseList.size() == 0) {
                    sciencetechtask.setId(zgsSciencetechtask.getId());
                    sciencetechtask.setDealtype("2");
                    zgsSciencetechtaskService.updateById(sciencetechtask);
                }
            }
        }
        //3.2、示范项目任务书
        QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
        queryWrapper32.eq("t.status", GlobalConstants.SHENHE_STATUS8);
        queryWrapper32.between("l.completedate", DateUtils.formatDate(date), DateUtils.formatDate(calendar.getTime()));
        queryWrapper32.and(qwr -> {
            qwr.ne("t.ishistory", 1).or().isNull("t.ishistory");
        });
//            queryWrapper32.isNull("l.isdealoverdue");
        List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
        if (listInfoList != null && listInfoList.size() > 0) {
            ZgsProjecttask zgsProjecttask = null;
            for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : listInfoList) {
                zgsProjecttask = new ZgsProjecttask();
                QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
                queryWrapper82.and(w82 -> {
                    w82.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
                });
                queryWrapper82.and(w82 -> {
                    w82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                });
                List<ZgsExamplecertificatebase> zgsExamplecertificatebaseList = zgsExamplecertificatebaseService.list(queryWrapper82);
                if (zgsExamplecertificatebaseList.size() == 0) {
                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
                    zgsProjecttask.setDealtype("2");
                    zgsProjecttaskService.updateById(zgsProjecttask);
                }
            }
        }
        log.info("临近一个月打标签----------end");
        //逾期3-6月打标签
        dealProjectDateInitSixMonth();
        //逾期半年以上打标签
        dealProjectDateInitOneYear();
    }

    /**
     * 定时扫描：逾期3-6月打标签
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 31 21 * * *")
//    @Scheduled(cron = "0 27 00 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void dealProjectDateInitSixMonth() {
        try {
            log.info("逾期3-6月打标签----------start");
            Date date = new Date();//获取当前时间
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, -3);
            //3.1、科技项目任务书
            QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
            queryWrapper3.eq("t.status", GlobalConstants.SHENHE_STATUS8);
            queryWrapper3.lt("t.enddate", DateUtils.formatDate(calendar.getTime()));
            queryWrapper3.ne("t.isdelete", 1);
            queryWrapper3.and(qwr -> {
                qwr.ne("t.ishistory", 1).or().isNull("t.ishistory");
            });
            queryWrapper3.and(qwr -> {
                qwr.ne("l.isdealoverdue", 2).or().isNull("l.isdealoverdue");
            });
            List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.listZgsScienceTechTaskListInfoTask(queryWrapper3);
            if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
                ZgsSciencetechtask sciencetechtask = null;
                for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                    sciencetechtask = new ZgsSciencetechtask();
//                QueryWrapper<ZgsSacceptcertificatebase> updateWrapper81 = new QueryWrapper();
//                updateWrapper81.and(w81 -> {
//                    w81.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                updateWrapper81.and(w81 -> {
//                    w81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().eq("projectname", zgsSciencetechtask.getProjectname().trim());
//                });
//                ZgsSacceptcertificatebase zgsSacceptcertificatebase = zgsSacceptcertificatebaseService.getOne(updateWrapper81);
//                QueryWrapper<ZgsSpostcertificatebase> updateWrapper83 = new QueryWrapper();
//                updateWrapper83.and(w83 -> {
//                    w83.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                updateWrapper83.and(w83 -> {
//                    w83.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().eq("projectname", zgsSciencetechtask.getProjectname().trim());
//                });
//                ZgsSpostcertificatebase zgsSpostcertificatebase = zgsSpostcertificatebaseService.getOne(updateWrapper83);
//                if (zgsSacceptcertificatebase == null || zgsSpostcertificatebase == null) {
//                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                    sciencetechtask.setDealtype("0");
//                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                }
                    //按照验收初审通过日期算，如果验收初审通过日期小于（结束日期+3个月），则不算逾期，可以进行验收管理提交申请
                    //验收申请
                    QueryWrapper<ZgsScientificbase> updateWrapper81 = new QueryWrapper();
                    updateWrapper81.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper81.and(w81 -> {
                        w81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsScientificbase> scientificbaseList = zgsScientificbaseService.list(updateWrapper81);
                    //结题申请
                    QueryWrapper<ZgsScientificpostbase> updateWrapper83 = new QueryWrapper();
                    updateWrapper83.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper83.and(w83 -> {
                        w83.eq("scientificbaseguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsScientificpostbase> scientificpostbaseList = zgsScientificpostbaseService.list(updateWrapper83);
                    //验收证书
                    QueryWrapper<ZgsSacceptcertificatebase> yszs1 = new QueryWrapper();
                    yszs1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs1.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsSacceptcertificatebase> listNew10 = zgsSacceptcertificatebaseService.list(yszs1);
                    //结题证书
                    QueryWrapper<ZgsSpostcertificatebase> yszs11 = new QueryWrapper();
                    yszs11.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs11.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsSpostcertificatebase> listNew11 = zgsSpostcertificatebaseService.list(yszs11);
                    //绩效自评-需要加一个判断是否为结题
                    QueryWrapper<ZgsPerformancebase> jxzp1 = new QueryWrapper();
                    jxzp1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    jxzp1.and(newQ -> {
                        newQ.eq("baseguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
//                    List<ZgsPerformancebase> listNew12 = zgsPerformancebaseService.list(jxzp1);
                    //成果简介
                    QueryWrapper<ZgsPlanresultbase> cgjj1 = new QueryWrapper();
                    cgjj1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    cgjj1.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("resultname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsPlanresultbase> listNew13 = zgsPlanresultbaseService.list(cgjj1);
                    //查询变更是否有延期记录、如果变更截止日期
                    QueryWrapper<ZgsPlannedprojectchange> bglog1 = new QueryWrapper();
                    bglog1.and(bg -> {
                        bg.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    bglog1.eq("status", GlobalConstants.SHENHE_STATUS8);
                    bglog1.ne("isdelete", 1);
                    bglog1.orderByDesc("firstdate");
                    List<ZgsPlannedprojectchange> bgLogList1 = zgsPlannedprojectchangeService.list(bglog1);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail1 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList1 = null;
                    if (bgLogList1.size() > 0) {
                        for (int m = 0; m < bgLogList1.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog11 = new QueryWrapper();
                            bglog11.eq("baseguid", bgLogList1.get(m).getId());
                            bglog11.eq("item_key", 7);
                            bglog11.ne("isdelete", 1);
                            bglog11.orderByDesc("createdate");
                            plannedprojectchangedetailList1 = zgsPlannedprojectchangedetailService.list(bglog11);
                            if (plannedprojectchangedetailList1 != null && plannedprojectchangedetailList1.size() > 0) {
                                zgsPlannedprojectchangedetail1 = plannedprojectchangedetailList1.get(0);
                            }
                        }
                    }
                    Date bgDate1 = null;
                    if (zgsPlannedprojectchangedetail1 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail1.getChangecontent())) {
                        //获取到该项目变更日期
                        bgDate1 = dateFormat.parse(zgsPlannedprojectchangedetail1.getChangecontent());
                    }
                    //
                    if (scientificbaseList.size() == 0 && scientificpostbaseList.size() == 0 && listNew10.size() == 0 && listNew11.size() == 0 && listNew13.size() == 0) {
                        if (bgDate1 == null) {
                            sciencetechtask.setId(zgsSciencetechtask.getId());
                            sciencetechtask.setDealtype("0");
                            zgsSciencetechtaskService.updateById(sciencetechtask);
                        } else {
                            if (bgDate1.compareTo(calendar.getTime()) < 0) {
                                sciencetechtask.setId(zgsSciencetechtask.getId());
                                sciencetechtask.setDealtype("0");
                                zgsSciencetechtaskService.updateById(sciencetechtask);
                            }
                        }
                    } else {
//                        if (bgDate1 == null) {
//                            if (zgsScientificbase != null && zgsScientificbase.getApplydate() != null && zgsSciencetechtask.getEnddate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(zgsSciencetechtask.getEnddate());
//                                instance.add(Calendar.MONTH, 3);
//                                Date endDate = instance.getTime();
//                                if (zgsScientificbase.getApplydate().compareTo(endDate) > 0) {
//                                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                                    sciencetechtask.setDealtype("0");
//                                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                                }
//                            }
//                        } else {
//                            if (zgsScientificbase != null && zgsScientificbase.getApplydate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(bgDate1);
//                                instance.add(Calendar.MONTH, 3);
//                                Date endDate = instance.getTime();
//                                if (zgsScientificbase.getApplydate().compareTo(endDate) > 0) {
//                                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                                    sciencetechtask.setDealtype("0");
//                                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                                }
//                            }
//                        }
                    }
                }
            }
            //3.2、示范项目任务书
            QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
            queryWrapper32.eq("t.status", GlobalConstants.SHENHE_STATUS8);
            queryWrapper32.lt("l.completedate", DateUtils.formatDate(calendar.getTime()));
            queryWrapper32.and(qwr -> {
                qwr.ne("t.ishistory", 1).or().isNull("t.ishistory");
            });
            queryWrapper32.and(qwr -> {
                qwr.ne("l.isdealoverdue", 2).or().isNull("l.isdealoverdue");
            });
            List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
            if (listInfoList != null && listInfoList.size() > 0) {
                ZgsProjecttask zgsProjecttask = null;
                for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : listInfoList) {
                    zgsProjecttask = new ZgsProjecttask();
//                QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
//                queryWrapper82.and(w82 -> {
//                    w82.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                queryWrapper82.and(w82 -> {
//                    w82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().eq("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
//                });
//                ZgsExamplecertificatebase zgsExamplecertificatebase = zgsExamplecertificatebaseService.getOne(queryWrapper82);
//                if (zgsExamplecertificatebase == null) {
//                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                    zgsProjecttask.setDealtype("0");
//                    zgsProjecttaskService.updateById(zgsProjecttask);
//                }
                    //按照验收申请提交日期算，如果验收申请提交日期小于（结束日期+3个月），则不算逾期，可以进行验收管理提交申请
                    //验收申请
                    QueryWrapper<ZgsDemoprojectacceptance> queryWrapper82 = new QueryWrapper();
                    queryWrapper82.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    queryWrapper82.and(w82 -> {
                        w82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper82);
                    //验收证书
                    QueryWrapper<ZgsExamplecertificatebase> yszs2 = new QueryWrapper();
                    yszs2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs2.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsExamplecertificatebase> listNew20 = zgsExamplecertificatebaseService.list(yszs2);
                    //绩效自评
                    QueryWrapper<ZgsPerformancebase> jxzp2 = new QueryWrapper();
                    jxzp2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    jxzp2.and(newQ -> {
                        newQ.eq("baseguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
//                    List<ZgsPerformancebase> listNew21 = zgsPerformancebaseService.list(jxzp2);
                    //成果简介
                    QueryWrapper<ZgsPlanresultbase> cgjj2 = new QueryWrapper();
                    cgjj2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    cgjj2.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("resultname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsPlanresultbase> listNew22 = zgsPlanresultbaseService.list(cgjj2);
                    //查询变更是否有延期记录、如果变更截止日期
                    QueryWrapper<ZgsPlannedprojectchange> bglog2 = new QueryWrapper();
                    bglog2.and(bg -> {
                        bg.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().eq("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    bglog2.ne("isdelete", 1);
                    bglog2.eq("status", GlobalConstants.SHENHE_STATUS8);
                    bglog2.orderByDesc("firstdate");
                    List<ZgsPlannedprojectchange> bgLogList2 = zgsPlannedprojectchangeService.list(bglog2);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail2 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList2 = null;
                    if (bgLogList2.size() > 0) {
                        for (int m = 0; m < bgLogList2.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog22 = new QueryWrapper();
                            bglog22.eq("baseguid", bgLogList2.get(m).getId());
                            bglog22.eq("item_key", 7);
                            bglog22.ne("isdelete", 1);
                            bglog22.orderByDesc("createdate");
                            plannedprojectchangedetailList2 = zgsPlannedprojectchangedetailService.list(bglog22);
                            if (plannedprojectchangedetailList2 != null && plannedprojectchangedetailList2.size() > 0) {
                                zgsPlannedprojectchangedetail2 = plannedprojectchangedetailList2.get(0);
                            }
                        }
                    }
                    Date bgDate2 = null;
                    if (zgsPlannedprojectchangedetail2 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail2.getChangecontent())) {
                        //获取到该项目变更日期
                        bgDate2 = dateFormat.parse(zgsPlannedprojectchangedetail2.getChangecontent());
                    }
                    //
                    if (zgsDemoprojectacceptanceList.size() == 0 && listNew20.size() == 0 && listNew22.size() == 0) {
                        if (bgDate2 == null) {
                            zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
                            zgsProjecttask.setDealtype("0");
                            zgsProjecttaskService.updateById(zgsProjecttask);
                        } else {
                            if (bgDate2.compareTo(calendar.getTime()) < 0) {
                                zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
                                zgsProjecttask.setDealtype("0");
                                zgsProjecttaskService.updateById(zgsProjecttask);
                            }
                        }
                    } else {
//                        if (bgDate2 == null) {
//                            if (zgsDemoprojectacceptance.getApplydate() != null && zgsProjectlibraryListInfo.getCompletedate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(zgsProjectlibraryListInfo.getCompletedate());
//                                instance.add(Calendar.MONTH, 3);
//                                Date endDate = instance.getTime();
//                                if (zgsDemoprojectacceptance.getApplydate().compareTo(endDate) > 0) {
//                                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                                    zgsProjecttask.setDealtype("0");
//                                    zgsProjecttaskService.updateById(zgsProjecttask);
//                                }
//                            }
//                        } else {
//                            if (zgsDemoprojectacceptance.getApplydate() != null && zgsProjectlibraryListInfo.getCompletedate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(bgDate2);
//                                instance.add(Calendar.MONTH, 3);
//                                Date endDate = instance.getTime();
//                                if (zgsDemoprojectacceptance.getApplydate().compareTo(endDate) > 0) {
//                                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                                    zgsProjecttask.setDealtype("0");
//                                    zgsProjecttaskService.updateById(zgsProjecttask);
//                                }
//                            }
//                        }
                    }
                }
            }
            log.info("逾期3-6月打标签----------end");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 定时扫描：逾期半年以上打标签
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 01 06 * * *")
//    @Scheduled(cron = "0 01 19 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void dealProjectDateInitOneYear() {
        try {
            log.info("逾期半年以上打标签----------start");
            Date date = new Date();//获取当前时间
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH, -6);
            //3.1、科技项目任务书
            QueryWrapper<ZgsSciencetechtask> queryWrapper3 = new QueryWrapper();
            queryWrapper3.eq("t.status", GlobalConstants.SHENHE_STATUS8);
            queryWrapper3.lt("t.enddate", DateUtils.formatDate(calendar.getTime()));
            queryWrapper3.ne("t.isdelete", 1);
            queryWrapper3.and(qwr -> {
                qwr.ne("t.ishistory", 1).or().isNull("t.ishistory");
            });
            queryWrapper3.and(qwr -> {
                qwr.ne("l.isdealoverdue", 2).or().isNull("l.isdealoverdue");
            });
            List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.listZgsScienceTechTaskListInfoTask(queryWrapper3);
            if (zgsSciencetechtaskList != null && zgsSciencetechtaskList.size() > 0) {
                ZgsSciencetechtask sciencetechtask = null;
                for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                    sciencetechtask = new ZgsSciencetechtask();
                    //以下内容是修改后的第一版，现在注释掉，改成第二版
//                QueryWrapper<ZgsSacceptcertificatebase> updateWrapper81 = new QueryWrapper();
//                updateWrapper81.and(w81 -> {
//                    w81.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                updateWrapper81.and(w81 -> {
//                    w81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().eq("projectname", zgsSciencetechtask.getProjectname().trim());
//                });
//                ZgsSacceptcertificatebase zgsSacceptcertificatebase = zgsSacceptcertificatebaseService.getOne(updateWrapper81);
//                QueryWrapper<ZgsSpostcertificatebase> updateWrapper83 = new QueryWrapper();
//                updateWrapper83.and(w83 -> {
//                    w83.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                updateWrapper83.and(w83 -> {
//                    w83.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().eq("projectname", zgsSciencetechtask.getProjectname().trim());
//                });
//                ZgsSpostcertificatebase zgsSpostcertificatebase = zgsSpostcertificatebaseService.getOne(updateWrapper83);
//                if (zgsSacceptcertificatebase == null || zgsSpostcertificatebase == null) {
//                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                    sciencetechtask.setDealtype("1");
//                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                }
                    //按照验收申请提交日期算，如果验收申请提交日期小于（结束日期+3个月），则不算逾期，可以进行验收管理提交申请
                    //验收申请
                    QueryWrapper<ZgsScientificbase> updateWrapper81 = new QueryWrapper();
                    updateWrapper81.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper81.and(w81 -> {
                        w81.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsScientificbase> scientificbaseList = zgsScientificbaseService.list(updateWrapper81);
                    //结题申请
                    QueryWrapper<ZgsScientificpostbase> updateWrapper83 = new QueryWrapper();
                    updateWrapper83.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    updateWrapper83.and(w83 -> {
                        w83.eq("scientificbaseguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsScientificpostbase> scientificpostbaseList = zgsScientificpostbaseService.list(updateWrapper83);
                    //验收证书
                    QueryWrapper<ZgsSacceptcertificatebase> yszs1 = new QueryWrapper();
                    yszs1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs1.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsSacceptcertificatebase> listNew10 = zgsSacceptcertificatebaseService.list(yszs1);
                    //结题证书
                    QueryWrapper<ZgsSpostcertificatebase> yszs11 = new QueryWrapper();
                    yszs11.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs11.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsSpostcertificatebase> listNew11 = zgsSpostcertificatebaseService.list(yszs11);
                    //绩效自评
                    QueryWrapper<ZgsPerformancebase> jxzp1 = new QueryWrapper();
                    jxzp1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    jxzp1.and(newQ -> {
                        newQ.eq("baseguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
//                    List<ZgsPerformancebase> listNew12 = zgsPerformancebaseService.list(jxzp1);
                    //成果简介
                    QueryWrapper<ZgsPlanresultbase> cgjj1 = new QueryWrapper();
                    cgjj1.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    cgjj1.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("resultname", zgsSciencetechtask.getProjectname().trim());
                    });
                    List<ZgsPlanresultbase> listNew13 = zgsPlanresultbaseService.list(cgjj1);
                    //查询变更是否有延期记录、如果变更截止日期
                    QueryWrapper<ZgsPlannedprojectchange> bglog1 = new QueryWrapper();
                    bglog1.and(bg -> {
                        bg.eq("projectlibraryguid", zgsSciencetechtask.getSciencetechguid()).or().like("projectname", zgsSciencetechtask.getProjectname().trim());
                    });
                    bglog1.ne("isdelete", 1);
                    bglog1.eq("status", GlobalConstants.SHENHE_STATUS8);
                    List<ZgsPlannedprojectchange> bgLogList1 = zgsPlannedprojectchangeService.list(bglog1);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail1 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList1 = null;
                    if (bgLogList1.size() > 0) {
                        for (int m = 0; m < bgLogList1.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog11 = new QueryWrapper();
                            bglog11.eq("baseguid", bgLogList1.get(m).getId());
                            bglog11.eq("item_key", 7);
                            bglog11.ne("isdelete", 1);
                            bglog11.orderByDesc("createdate");
                            plannedprojectchangedetailList1 = zgsPlannedprojectchangedetailService.list(bglog11);
                            if (plannedprojectchangedetailList1 != null && plannedprojectchangedetailList1.size() > 0) {
                                zgsPlannedprojectchangedetail1 = plannedprojectchangedetailList1.get(0);
                            }
                        }
                    }
                    Date bgDate1 = null;
                    if (zgsPlannedprojectchangedetail1 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail1.getChangecontent())) {
                        //获取到该项目变更日期
                        bgDate1 = dateFormat.parse(zgsPlannedprojectchangedetail1.getChangecontent());
                    }
                    //
                    if (scientificbaseList.size() == 0 && scientificpostbaseList.size() == 0 && listNew10.size() == 0 && listNew11.size() == 0 && listNew13.size() == 0) {
                        if (bgDate1 == null) {
                            sciencetechtask.setId(zgsSciencetechtask.getId());
                            sciencetechtask.setDealtype("1");
                            zgsSciencetechtaskService.updateById(sciencetechtask);
                        } else {
                            if (bgDate1.compareTo(calendar.getTime()) < 0) {
                                sciencetechtask.setId(zgsSciencetechtask.getId());
                                sciencetechtask.setDealtype("1");
                                zgsSciencetechtaskService.updateById(sciencetechtask);
                            }
                        }
                    } else {
//                        if (bgDate1 == null) {
//                            if (zgsScientificbase != null && zgsScientificbase.getApplydate() != null && zgsSciencetechtask.getEnddate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(zgsSciencetechtask.getEnddate());
//                                instance.add(Calendar.MONTH, 6);
//                                Date endDate = instance.getTime();
//                                if (zgsScientificbase.getApplydate().compareTo(endDate) > 0) {
//                                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                                    sciencetechtask.setDealtype("1");
//                                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                                }
//                            }
//                        } else {
//                            if (zgsScientificbase != null && zgsScientificbase.getApplydate() != null && zgsSciencetechtask.getEnddate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(bgDate1);
//                                instance.add(Calendar.MONTH, 6);
//                                Date endDate = instance.getTime();
//                                if (zgsScientificbase.getApplydate().compareTo(endDate) > 0) {
//                                    sciencetechtask.setId(zgsSciencetechtask.getId());
//                                    sciencetechtask.setDealtype("1");
//                                    zgsSciencetechtaskService.updateById(sciencetechtask);
//                                }
//                            }
//                        }
                    }
                }
            }
            //3.2、示范项目任务书
            QueryWrapper<ZgsProjecttask> queryWrapper32 = new QueryWrapper();
            queryWrapper32.eq("t.status", GlobalConstants.SHENHE_STATUS8);
            queryWrapper32.lt("l.completedate", DateUtils.formatDate(calendar.getTime()));
            queryWrapper32.and(qwr -> {
                qwr.ne("t.ishistory", 1).or().isNull("t.ishistory");
            });
            queryWrapper32.and(qwr -> {
                qwr.ne("l.isdealoverdue", 2).or().isNull("l.isdealoverdue");
            });
            List<ZgsProjectlibraryListInfo> listInfoList = zgsProjecttaskService.listZgsProjecttaskListInfoTask(queryWrapper32);
            if (listInfoList != null && listInfoList.size() > 0) {
                ZgsProjecttask zgsProjecttask = null;
                for (ZgsProjectlibraryListInfo zgsProjectlibraryListInfo : listInfoList) {
                    zgsProjecttask = new ZgsProjecttask();
                    //以下内容是修改后的第一版，现在注释掉，改成第二版
//                QueryWrapper<ZgsExamplecertificatebase> queryWrapper82 = new QueryWrapper();
//                queryWrapper82.and(w82 -> {
//                    w82.eq("status", GlobalConstants.SHENHE_STATUS8).or().isNotNull("isclose");
//                });
//                queryWrapper82.and(w82 -> {
//                    w82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().eq("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
//                });
//                ZgsExamplecertificatebase zgsExamplecertificatebase = zgsExamplecertificatebaseService.getOne(queryWrapper82);
//                if (zgsExamplecertificatebase == null) {
//                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                    zgsProjecttask.setDealtype("1");
//                    zgsProjecttaskService.updateById(zgsProjecttask);
//                }
                    //按照验收申请提交日期算，如果验收申请提交日期小于（结束日期+3个月），则不算逾期，可以进行验收管理提交申请
                    //验收申请
                    QueryWrapper<ZgsDemoprojectacceptance> queryWrapper82 = new QueryWrapper();
                    queryWrapper82.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    queryWrapper82.and(w82 -> {
                        w82.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsDemoprojectacceptance> zgsDemoprojectacceptanceList = zgsDemoprojectacceptanceService.list(queryWrapper82);
                    //验收证书
                    QueryWrapper<ZgsExamplecertificatebase> yszs2 = new QueryWrapper();
                    yszs2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS2);
                    });
                    yszs2.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsExamplecertificatebase> listNew20 = zgsExamplecertificatebaseService.list(yszs2);
                    //绩效自评
                    QueryWrapper<ZgsPerformancebase> jxzp2 = new QueryWrapper();
                    jxzp2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    jxzp2.and(newQ -> {
                        newQ.eq("baseguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
//                    List<ZgsPerformancebase> listNew21 = zgsPerformancebaseService.list(jxzp2);
                    //成果简介
                    QueryWrapper<ZgsPlanresultbase> cgjj2 = new QueryWrapper();
                    cgjj2.and(qwd -> {
                        qwd.isNotNull("firstdate").or().eq("status", GlobalConstants.SHENHE_STATUS8);
                    });
                    cgjj2.and(newQ -> {
                        newQ.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("resultname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    List<ZgsPlanresultbase> listNew22 = zgsPlanresultbaseService.list(cgjj2);
                    //查询变更是否有延期记录、如果变更截止日期
                    QueryWrapper<ZgsPlannedprojectchange> bglog2 = new QueryWrapper();
                    bglog2.and(bg -> {
                        bg.eq("projectlibraryguid", zgsProjectlibraryListInfo.getBid()).or().like("projectname", zgsProjectlibraryListInfo.getProjectname().trim());
                    });
                    bglog2.ne("isdelete", 1);
                    bglog2.eq("status", GlobalConstants.SHENHE_STATUS8);
                    List<ZgsPlannedprojectchange> bgLogList2 = zgsPlannedprojectchangeService.list(bglog2);
                    ZgsPlannedprojectchangedetail zgsPlannedprojectchangedetail2 = null;
                    List<ZgsPlannedprojectchangedetail> plannedprojectchangedetailList2 = null;
                    if (bgLogList2.size() > 0) {
                        for (int m = 0; m < bgLogList2.size(); m++) {
                            QueryWrapper<ZgsPlannedprojectchangedetail> bglog22 = new QueryWrapper();
                            bglog22.eq("baseguid", bgLogList2.get(m).getId());
                            bglog22.eq("item_key", 7);
                            bglog22.ne("isdelete", 1);
                            bglog22.orderByDesc("createdate");
                            plannedprojectchangedetailList2 = zgsPlannedprojectchangedetailService.list(bglog22);
                            if (plannedprojectchangedetailList2 != null && plannedprojectchangedetailList2.size() > 0) {
                                zgsPlannedprojectchangedetail2 = plannedprojectchangedetailList2.get(0);
                            }
                        }
                    }
                    Date bgDate2 = null;
                    if (zgsPlannedprojectchangedetail2 != null && StringUtils.isNotEmpty(zgsPlannedprojectchangedetail2.getChangecontent())) {
                        //获取到该项目变更日期
                        bgDate2 = dateFormat.parse(zgsPlannedprojectchangedetail2.getChangecontent());
                    }
                    //
                    if (zgsDemoprojectacceptanceList.size() == 0 && listNew20.size() == 0 && listNew22.size() == 0) {
                        if (bgDate2 == null) {
                            zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
                            zgsProjecttask.setDealtype("1");
                            zgsProjecttaskService.updateById(zgsProjecttask);
                        } else {
                            if (bgDate2.compareTo(calendar.getTime()) < 0) {
                                zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
                                zgsProjecttask.setDealtype("1");
                                zgsProjecttaskService.updateById(zgsProjecttask);
                            }
                        }
                    } else {
//                        if (bgDate2 == null) {
//                            if (zgsDemoprojectacceptance.getApplydate() != null && zgsProjectlibraryListInfo.getCompletedate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(zgsProjectlibraryListInfo.getCompletedate());
//                                instance.add(Calendar.MONTH, 6);
//                                Date endDate = instance.getTime();
//                                if (zgsDemoprojectacceptance.getApplydate().compareTo(endDate) > 0) {
//                                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                                    zgsProjecttask.setDealtype("1");
//                                    zgsProjecttaskService.updateById(zgsProjecttask);
//                                }
//                            }
//                        } else {
//                            if (zgsDemoprojectacceptance.getApplydate() != null && zgsProjectlibraryListInfo.getCompletedate() != null) {
//                                Calendar instance = Calendar.getInstance();
//                                instance.setTime(bgDate2);
//                                instance.add(Calendar.MONTH, 6);
//                                Date endDate = instance.getTime();
//                                if (zgsDemoprojectacceptance.getApplydate().compareTo(endDate) > 0) {
//                                    zgsProjecttask.setId(zgsProjectlibraryListInfo.getId());
//                                    zgsProjecttask.setDealtype("1");
//                                    zgsProjecttaskService.updateById(zgsProjecttask);
//                                }
//                            }
//                        }
                    }
                }
            }
            log.info("逾期半年以上打标签----------end");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 账号短信下发
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 42 23 * * *")
//    @Scheduled(cron = "0 38 21 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/10 * * * ?")
    public void initUserAccountSend() {
        log.info("账号短信下发----------start");
        try {
            QueryWrapper<ZgsProjectunitmembersend> queryWrapper = new QueryWrapper();
            List<ZgsProjectunitmembersend> userList = zgsProjectunitmembersendService.list(queryWrapper);
            if (userList != null && userList.size() > 0) {
                for (int i = 0; i < userList.size(); i++) {
                    ZgsProjectunitmembersend sysUser = userList.get(i);
                    String loginname = sysUser.getLoginname();
                    String loginpassword = sysUser.getLoginpassword();
                    String enterprisename = sysUser.getEnterprisename();
                    String applyunitphone = sysUser.getApplyunitphone();
//                    log.info("----------" + loginname + "--" + enterprisename);
                    HashMap<String, Object> params2 = new HashMap<>(10);
                    params2.put("location", "http://36.142.16.227:12065/green_sjk/");
                    params2.put("accountname", loginname);
                    params2.put("username", "-");
                    params2.put("month", "5");
                    params2.put("day", "18");
                    params2.put("initPassword", "123456");
                    params2.put("QQNum", "686895470");
                    HashMap<String, Object> headers = new HashMap<>(10);
                    HashMap<String, Object> params = new HashMap<>(10);
                    headers.put("Authorization", "Bearer 86b491e9-f28e-47c5-a9fd-a9d8452b17f7");
                    params.put("phoneNumber", applyunitphone);
                    params.put("smsType", 3);
                    params.put("templateId", 19);
                    params.put("fillContent", params2);
                    String resultUser = HttpClientUtilJK.httpPostRequest(smsBaseUrl + GlobalConstants.usl, headers, JSONObject.toJSONString(params));
                    log.info(applyunitphone + "----------result" + resultUser);
                    //发短信成功修改状态
                    if (StringUtils.isNotEmpty(resultUser) && resultUser.contains("00000")) {
                        ZgsProjectunitmembersend projectunitmembersend = new ZgsProjectunitmembersend();
                        projectunitmembersend.setId(sysUser.getId());
                        projectunitmembersend.setIsflag("1");
                        zgsProjectunitmembersendService.updateById(projectunitmembersend);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("账号短信下发----------end");
    }


    /**
     * 0省级科技资金未支持项目,1省级科技资金支持项目
     * 打标签
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 42 23 * * *")
//    @Scheduled(cron = "0 20 21 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/3 * * * ?")
    public void initPerformanceFundType() {
        log.info("绩效自评项目-省级科技资金是否支持-打标签----------start");
        try {
            if (redisUtil.get("initPerformanceFundType") == null) {
                redisUtil.set("initPerformanceFundType", "test");
                QueryWrapper<ZgsPerformancebase> queryWrapper = new QueryWrapper<>();
                queryWrapper.ne("isdelete", 1);
                List<ZgsPerformancebase> zgsPerformancebaseList = zgsPerformancebaseService.list(queryWrapper);
                for (ZgsPerformancebase zgsPerformancebase : zgsPerformancebaseList) {
                    ZgsPerformancebase performancebase = new ZgsPerformancebase();
                    performancebase.setId(zgsPerformancebase.getId());
                    performancebase.setFundType(zgsProjecttaskService.initFundType(zgsPerformancebase.getBaseguid()));
                    zgsPerformancebaseService.updateById(performancebase);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("绩效自评项目-省级科技资金是否支持-打标签----------end");
    }

    /**
     * 0省级科技资金未支持项目,1省级科技资金支持项目
     * 任务书打标签
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 05 18 * * *")
//    @Scheduled(cron = "0 35 13 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/3 * * * ?")
    public void initZgsTaskFundType() {
        log.info("任务书-省级科技资金是否支持-打标签----------start");
        try {
            if (redisUtil.get("initZgsTaskFundType_N") == null) {
                redisUtil.set("initZgsTaskFundType_N", "test");
                QueryWrapper<ZgsProjecttask> queryWrapper1 = new QueryWrapper<>();
                queryWrapper1.ne("isdelete", 1);
                List<ZgsProjecttask> zgsProjecttaskList = zgsProjecttaskService.list(queryWrapper1);
                for (ZgsProjecttask zgsProjecttask : zgsProjecttaskList) {
                    ZgsProjecttask projecttask = new ZgsProjecttask();
                    projecttask.setId(zgsProjecttask.getId());
                    projecttask.setFundType(zgsProjecttaskService.initFundType(zgsProjecttask.getProjectguid()));
                    zgsProjecttaskService.updateById(projecttask);
                }
                //
                QueryWrapper<ZgsSciencetechtask> queryWrapper2 = new QueryWrapper<>();
                queryWrapper2.ne("isdelete", 1);
                List<ZgsSciencetechtask> zgsSciencetechtaskList = zgsSciencetechtaskService.list(queryWrapper2);
                for (ZgsSciencetechtask zgsSciencetechtask : zgsSciencetechtaskList) {
                    ZgsSciencetechtask sciencetechtask = new ZgsSciencetechtask();
                    sciencetechtask.setId(zgsSciencetechtask.getId());
                    sciencetechtask.setFundType(zgsProjecttaskService.initFundType(zgsSciencetechtask.getSciencetechguid()));
                    zgsSciencetechtaskService.updateById(sciencetechtask);
                }
//                redisUtil.set("d359c134-8455-4897-9db9-a8f843160cb1", "中期查验阶段");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("任务书-省级科技资金是否支持-打标签----------end");
    }

    /**
     * 初始化所有既有汇总存在，且既有清单不存在的既有汇总数据
     */
//    @Scheduled(cron = "0/60 * * * * ?")
//    @Scheduled(cron = "0 04 10 * * *")
//    @Scheduled(cron = "0 20 21 * * *")
//    @Scheduled(cron = "0 10 00 * * *")
//    @Scheduled(cron = "0 0/3 * * * ?")
    public void initAddZeroExistinfoNotInExistinfoTotal() {
        log.info("初始化所有既有汇总存在，且既有清单不存在的既有汇总数据----------start");
        zgsReportEnergyExistinfoTotalService.initAddZeroExistinfoNotInExistinfoTotal();
        log.info("初始化所有既有汇总存在，且既有清单不存在的既有汇总数据----------end");
    }

    public static void main(String[] args) {
        //Greensjk@835290
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.zhangw", "Greensjk@835290", "JwciStMa"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.wangw", "Greensjk@835290", "KZjXgeaC"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.wangfeng", "Greensjk@835290", "yjX4t3I4"));
//        System.out.println(PasswordUtil.encrypt("gsszjt401", "Greensjk@835290", "yjX4t3I4"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.cxt", "Greensjk@835290", "bMnjR7vT"));
//        System.out.println(PasswordUtil.encrypt("gsjst0039", "Greensjk@835290", "JDqYgetO"));
//        System.out.println(PasswordUtil.encrypt("gov_admin", "Greensjk@782490", "FXj6PLrY"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.myg", "Greensjk@835290", "tvN4pIGc"));
//        System.out.println(PasswordUtil.encrypt("gsjst0057.jst.szf.gs", "Greensjk@835290", "inVoev3S"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.shm", "Greensjk@835290", "5Q6APZKV"));
//        System.out.println(PasswordUtil.encrypt("gsjst0004", "Greensjk@835290", "oDsLSXYV"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.yfb", "Greensjk@835290", "slCJKGHi"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.llh", "Greensjk@835290", "p8SVrBMb"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.cwx", "Greensjk@835290", "gPMMDOY9"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.jinck", "Greensjk@835290", "ru7HBrjT"));
//        System.out.println(PasswordUtil.encrypt("gss.zfhcxjst.dzx", "Greensjk@835290", "2ngfSRzx"));
//        System.out.println(PasswordUtil.encrypt("longdingyly", "yangly1977", "Qya3NJdd"));
        try {
//            String passwordEncode = PasswordUtil.encrypt("刘彬", "J5J2V4N5_0501", "Qya3NJdd");
//            System.out.println(passwordEncode);
            String password = "Greensjk@835123";//密码符合要求!
            //判断是否弱密码
            if (GreenUtilSelf.matchesPassWord(password)) {
                //是弱密码
                System.out.println("您输入的密码不符合要求，须同时包含数字、大小写字母，且至少六位");
            } else {
                System.out.println("密码符合要求！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
