package org.jeecg.modules.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhyd.oauth.log.Log;

import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.util.encryption.ValidateEncryptEntityUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/data/encrypt")
@Api(tags="DataEncryptController")
public class DataEncryptControllerManual {

    @Autowired
    private Map<String, IService<?>> serviceMap;

   
    // 判断是否加密工具类
    private <T> QueryWrapper<T> getQueryByEncrypt(boolean flag) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        // 根据is_encrypt 字段判断加密状态
        if (!flag) {
            // 已加密的结果集
            queryWrapper.eq("is_encrypt", 1);
        }else{
            // 未加密的结果集
            queryWrapper.isNull("is_encrypt");
        }
        return queryWrapper;
    }

    private <T> List<T> setInfoEncrypt(boolean flag, List<T> infoList) {

        for (T info : infoList) {
            if (flag) {
                info = ValidateEncryptEntityUtil.validateEncryptObject(info, true);
            }else{
                info = ValidateEncryptEntityUtil.validateDecryptObject(info, true);
            }
            
            try {
                info.getClass().getMethod("setIsEncrypt", Integer.class).invoke(info, flag ? 1 : null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            }

        }
        return infoList;
    }

    @AutoLog(value = "manualPathEncrypt")
	@ApiOperation(value="manualPathEncrypt", notes="manualPathEncrypt")
    @GetMapping("/manual/{path}")
    public String manualPathEncrypt(@RequestParam("flag") boolean flag, @PathVariable String path) {

        if (StrUtil.isBlank(path)) {
            return "no service.......................................";
        }

        String serviceName = path + "ServiceImpl";
        IService<?> service = serviceMap.get(serviceName);
        if (service == null) {
            return "no service........................................";
        }

        QueryWrapper queryWrapper = getQueryByEncrypt(flag);
        List dataList = service.list(queryWrapper);
        dataList = setInfoEncrypt(flag, dataList);
        
        service.updateBatchById(dataList);

        return "OK";
    }

    @AutoLog(value = "manualPathEncryptAll")
	@ApiOperation(value="printService")
    @GetMapping("/manual/pring")
    public String manualPathEncryptPrintl() {

        JSONArray arr = new JSONArray();
        for (Map.Entry<String, IService<?>> serviceEntry : serviceMap.entrySet()) {
            arr.add(serviceEntry.getKey());
        }

        return JSON.toJSONString(arr);
    }

    @AutoLog(value = "manualPathEncryptAll")
	@ApiOperation(value="无脑全干")
    @GetMapping("/manual/all")
    public String manualPathEncryptAll(@RequestParam("flag") boolean flag) {

        for (Map.Entry<String, IService<?>> serviceEntry : serviceMap.entrySet()) {
            try {

                if (!serviceEntry.getKey().startsWith("zgs")) {
                    continue;
                }

                IService<?> service = serviceEntry.getValue();
                QueryWrapper queryWrapper = getQueryByEncrypt(flag);
                List dataList = service.list(queryWrapper);
                dataList = setInfoEncrypt(flag, dataList);
                
                service.updateBatchById(dataList);
            } catch (Exception e) {
                // 无脑继续。
                Log.debug("出错了， 没事儿，继续。 无脑干。。。");
            }

        }



        return "OK";
    }

}
