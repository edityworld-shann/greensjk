package org.jeecg.common.constant;

public class GlobalConstants {

    //*************** 短信发送 Begin**************************************************=======================================================================
    public static final String token =
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NTM5Nzk0OTYsInVzZXJuYW1lIjoiQVQwMDAxMjcifQ.G8No4WPzqMjqYO04N-HZjYegoBDEiGGsBJXTK6_1UDI";
    public static final String PRIVATE_KEY = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCmaTZy73n4xP/FsG+WWcK3zs/iPNBZAlNTQm6jE3olAlYrUI9GWWltEg3X4+rFeuLwXwz6xtHTmNLj7/NnBK22wGaFkFWGYKa531K075SRjxLvTUK45Hcj/9dY6fAMoHP7zY7INOKxjax5DnTZlb/+yd4ClNtTALFZ7vu6kThj3WJ4Ob+SVBc0gRAGtSLvTZFpKCEVmZYM3Jh0KRrWO5bBcP2ndNTk1yBNjxu+tA0hn8MTy5XzSfW25OF85Jgc7Kc/uXDM+tSzq39eqDGJRWI+JZdOyKX0GTrFMj6RNekbBsCCMVQQlORtS/ooPlKXz7KW9dhOVuAvudeTkTHK9LB7AgMBAAECggEAdmhWveF4QHKsK9YnECw+SNt3P9tVxH/LGmsinha1yq+m8JsZ10y3n0xBXWRnIK0X3UFE0wu1MYKETHvoDV1e3bJ5QIA62qra91bYWrY+WHQdXPj0iOrHe24HpNSQYeupm3ngJsS6qOvN1TVcH5ZWTs0Ek4GnrZ/Ikd1icJX0MdmmjCAjslGMPhiyxTQfWi1nXHMfyGw0x8JaTe088ANkv8GlHFm6u7hXT3P8G8oGXot9qN5GIyPrOTX3K/FmGQiA+rTWmgPfKBNGQsS2KRfLDbDiOtmTv7nm+l5X7rvLDXlBgc3XTMtE4CeF+UacyGWyhTO5iSUFLoqDrkprq43PAQKBgQDaby8831MR+F8KQ1PhI4fhxyAybpXgMpq1kuS+hx7KXC7UzcLGLc9xKxDxWenxqfhP9u30mAereV9Ejk4Qh4yhTYW5mB/coqvPD235yrEKmXbB9APlgu0XfZRp6lgT3sVknsdlY28Pk7gBcab+X5pVXdvKpMzsFOdEa1uNZmyIqwKBgQDDB6STud7Q4t0cIRBVnMwiNKkiGu/f55HDK1wazEcnSgmyohnt7AXHbDaw33axy/eT2haZDFOPE3xM275wsE4BcuUM/DskzCpXsWmZvQJ3f3ZmFsMXLHP+zdh+MVITMntWNMEPbMsWRPpmPb8hMR+9hSJRI1UVxUVz+HsQ+E8XcQKBgCLlfGB3NSg7y8Mx+maPbJfvXf9wl1UYAItdilm98HdXvsuUi7dpfiiQPsawHKXVB6yOwHlBCIimfUfQjDwry5XBcsmxufVfr58G0gUEEsCkv8pAl5bFEeec0mcRXzzdbB+lFinmdsnASbdBVvJwh5F3yzNWnL5ioIfXtuqiIFi9AoGAXfsn3pwZ+DTqceyo72iAohcYTbwHTbFMd21ElBFOVGuEItWkgxdSbZgC/tagXus4IcJdU2EyPucX5f2aVrlOzbntEIXXhlLwMJPonFINh0If7vAXEOq19tqA8caYk3GnJ5KCWgmGD/QraetlV/xRHEW0lL7e+H+4iTr7KuFNU7ECgYEAqSdjPRAOZVG1e25Z5Voi66KfEn6BpghpMFDX/3eT3sc5YsSgztm+R1jH/KTM8ZR++lWhyXXYn9ThL0NJqIwfE7UWxloBPxhz0U56Ngo93JMC/Cdr7aYYa3j1d97deYDaJRWnYCMapZLXlml8lJkvRzhPbHGZRPIEfGZ2cTbkkds=";
    //    public static final String client_id = "lsjnjz";
    public static final String usl = "SMSService/api/sms/sendSMSMessage/v1.1";
    //互联网测试环境
//    public static final String baseUrlPrefix_Test = "http://139.9.130.166:9999/";
    //政务网生产环境
//    public static final String baseUrlPrefix_HuLianWang = "http://59.219.204.31:18600/";
    //云内网生产环境
//    public static final String baseUrlPrefix_YunNeiWang = "http://172.17.34.45:9999/";
//    public static final String client_secret = "b6f24077-7727-4020-bdc9-ac68b8dc1c4f";
    //*************** 短信发送 End***********************P***************************=======================================================================


    //*************** 统计报表 Begin**************************************************=======================================================================
    public static final String greenBuild_Qx = "zgs_report_monthgreenbuild_area";                    //  绿色建筑 - 区县
    //  绿色建筑 - 市州 - 明细表   （0:施工图审查完成未开工 1:开工建设 2:已竣工验收 3：竣工后运行）
    public static final String greenBuild_areaDetail_Qx = "zgs_report_monthgreen_areadetail";

    public static final String greenBuild_Sz = "zgs_report_monthgreenbuild";                          //  绿色建筑 - 市州
    //  绿色建筑 - 市州 - 明细表   （0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行）
    public static final String greenBuild_detail_Sz = "zgs_report_monthgreenbuild_detail";


    public static final String fabricate_area_Qx = "zgs_report_monthfabricate_area";                  //  装配式建筑 - 区县  （1:新开工2:已竣工验收）
    public static final String fabricate_areadetail_Qx = "zgs_report_monthfabr_areadetail";           //  绿色建筑 - 市州 - 明细表
    public static final String fabricate_areaScDetail_Qx = "zgs_report_monthfabr_areascdetail";       //  绿色建筑 - 市州 - 生产产能

    public static final String fabricate_Sz = "zgs_report_monthfabricate";                            //  装配式建筑 - 市州
    public static final String fabricate_detail_Sz = "zgs_report_monthfabricate_detail";              //  绿色建筑 - 市州 - 明细表  （1:新开工2:已竣工验收）
    public static final String fabricate_scDetail_Sz = "zgs_report_monthfabricate_scdetail";          //  绿色建筑 - 市州 - 生产产能


    public static final String buildingenergy_Qx = "zgs_report_buildingenergyinfonew";                //  建筑节能 - 区县
    public static final String buildingenergy_Sz = "zgs_report_buildingenergyinfocity";               //  建筑节能 - 市州
    public static final String buildingenergy_detail_Qx = "zgs_report_buildingenergyinfonewdetail";   //  建筑节能 - 明细 （没有分类）


    public static final String build_type = "住宅";                       //  选项1-父项：1、住宅 2、公共建筑
    public static final String build_type2 = "公共建筑";                  //  选项1-父项：1、住宅 2、公共建筑
    public static final String build_child_type = "装配式混凝土结构 ";        //  选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构
    public static final String build_child_type2 = "装配式钢结构";    //  选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构
    public static final String build_child_type3 = "装配式木结构";      //  选项1-子项：1、装配式混凝土结构 2、装配式钢结构 3、装配式木结构
    //*************** 统计报表 End**************************************************=======================================================================


    /**
     * 业务处理器beanName传递参数
     */
    public static final String HANDLER_NAME = "handlerName";
    public static final String LOGIN_GET_TOKEN_URL = "auth/oauth/token?";
    public static final String GETUSERINFO_URL = "admin/api/user/getUserInfo/v1.0?";
    public static final String AUTHORIZATION_CODE = "authorization_code";
    /**
     * redis消息通道名称
     */
    public static final String REDIS_TOPIC_NAME = "jeecg_redis_topic";
    //绿色建筑示范 GreenBuildType;建筑工程示范 ConstructBuidType;装配式建筑示范 AssemBuidType;建筑节能示范	EnergyBuildType;市政工程示范 SampleBuidType
    public static final String GreenBuildType = "GreenBuildType";
    public static final String GreenBuildType_VALUE = "绿色建筑示范";
    public static final String ConstructBuidType = "ConstructBuidType";
    public static final String ConstructBuidType_VALUE = "建筑工程示范";
    public static final String AssemBuidType = "AssemBuidType";
    public static final String AssemBuidType_VALUE = "装配式建筑示范";
    public static final String EnergyBuildType = "EnergyBuildType";
    public static final String EnergyBuildType_VALUE = "建筑节能示范";
    public static final String SampleBuidType = "SampleBuidType";
    public static final String SampleBuidType_VALUE = "市政工程示范";
    //"审核状态：0未上报,1待审核,4初审通过,3初审退回,2终审通过,5终审退回,6专家驳回,7已超时,8形审通过,9形审退回,10专家通过,11建设单位自行评审,12待立项,13初审驳回,14形审驳回,15终审驳回,16省厅终止,17历史数据,18金融审批通过,19金融审批退回"
    public static final String SHENHE_STATUS0 = "0";
    public static final String SHENHE_STATUS1 = "1";
    public static final String SHENHE_STATUS2 = "2";
    public static final String SHENHE_STATUS3 = "3";
    public static final String SHENHE_STATUS4 = "4";
    public static final String SHENHE_STATUS5 = "5";
    public static final String SHENHE_STATUS6 = "6";
    public static final String SHENHE_STATUS7 = "7";
    public static final String SHENHE_STATUS8 = "8";
    public static final String SHENHE_STATUS9 = "9";
    public static final String SHENHE_STATUS10 = "10";
    public static final String SHENHE_STATUS11 = "11";
    public static final String SHENHE_STATUS12 = "12";
    public static final String SHENHE_STATUS13 = "13";
    public static final String SHENHE_STATUS14 = "14";
    public static final String SHENHE_STATUS15 = "15";
    public static final String SHENHE_STATUS16 = "16";
    public static final String SHENHE_STATUS17 = "17";
    public static final String SHENHE_STATUS18 = "18";
    public static final String SHENHE_STATUS19 = "19";

    //个人申报未审核
    public static final String PROJECT_LIST_SB_W_1_4 = "1,4";
    //个人申报已审核
    public static final String PROJECT_LIST_SB_Y_2_4_8 = "2,4,8";
    //个人申报已退回
    public static final String PROJECT_LIST_SB_T_3_5_9_13_14_15 = "3,5,9,13,14,15";
    //个人工作台资料待补充
    public static final String PROJECT_LIST_WORKTABLE_0_3 = "0,3";
    //初审申报未审核
    public static final String PROJECT_LIST_SB_W_1_5_9_13_14_15 = "1,5,9,13,14,15";
    //初审申报已审核
    public static final String PROJECT_LIST_SB_YC_2_4_8 = "2,4,8";
    //初审申报已退回
    public static final String PROJECT_LIST_SB_TC_5_9_14_15 = "5,9,14,15";
    //省申报未审核
    public static final String PROJECT_LIST_SB_W_4_8_12 = "4,8,12";
    //省申报已审核
    public static final String PROJECT_LIST_SB_Y_2_8_12 = "2,8,12";
    //省申报已退回
    public static final String PROJECT_LIST_SB_T_5_15 = "5,15";
    //
    public static final String PROJECT_LIST1439 = "1,4,3,9";
    public static final String PROJECT_LIST1439_12 = "1,4,3,9,12";
    //2终审通过,5终审退回,15终审驳回,8形审通过,9形审退回,14形审驳回(可撤回标识)
    public static final String ROLL_BACK_SP_2_5_8_9_14_15 = "2,5,8,9,14,15";
    //二次变更验收和证书阶段可强制变更状态不包含的
    public static final String BG_STATUS_2_8_14_15_16 = "2,8,15,14,16";

    //专家评审结果 1 ：不同意立项 ，2：同意立项
    public static final String AGREEPROJECT1 = "1";
    public static final String AGREEPROJECT2 = "2";

    public static final String ScienceTech = "ScienceTech";
    public static final String ScienceSoft = "ScienceSoft";
    public static final String ScientificAcceptance = "ScientificAcceptance";
    public static final String TechAcceptance = "TechAcceptance";
    public static final String ScientificPost = "ScientificPost";
    public static final String MidtermInspection = "MidtermInspection";
    public static final String PlannedProjectChange = "PlannedProjectChange";
    public static final String SAcceptCertificate = "SAcceptCertificate";
    public static final String SPostCertificate = "SPostCertificate";
    public static final String ExampleCertificate = "ExampleCertificate";
    public static final String ZgsReportProject = "ZgsReportProject";
    public static final String ExpertLibray = "ExpertLibray";
    public static final String ScienceTech_VALUE = "科技攻关";
    public static final String ScienceSoft_VALUE = "软科学";
    public static final String ScientificAcceptance_VALUE = "科研项目验收";
    public static final String TechAcceptance_VALUE = "示范项目验收";
    public static final String ScientificPost_VALUE = "科研项目结题";
    public static final String MidtermInspection_VALUE = "示范项目中期查验";
    public static final String PlannedProjectChange_VALUE = "计划项目变更";
    public static final String SAcceptCertificate_VALUE = "科研项目验收证书";
    public static final String ExampleCertificate_VALUE = "示范项目验收证书";

    public static final String handleSave = "0";//保存
    public static final String handleSubmit = "1";//保存并上报

    public static final String AGREE_0 = "0";//同意
    public static final String AGREE_1 = "1";//不同意
    //loginUserType登录账号类型：0单位，1个人，2专家库，3住建厅，4推荐单位，6县区，7市区，8金融审批单位
    public static final Integer loginUserType_0 = 0;
    public static final Integer loginUserType_1 = 1;
    public static final Integer loginUserType_2 = 2;
    public static final Integer loginUserType_3 = 3;
    public static final Integer loginUserType_4 = 4;
    public static final Integer loginUserType_6 = 6;
    public static final Integer loginUserType_7 = 7;
    public static final Integer loginUserType_8 = 8;
    //{label: '申报单位名称', value: '1'},
    //        {label: '负责人姓名', value: '2'},
    //        {label: '联系电话', value: '3'},
    //        {label: '法人姓名', value: '4'}
    public static final String expert_set_close1 = "1";
    public static final String expert_set_close2 = "2";
    public static final String expert_set_close3 = "3";
    public static final String expert_set_close4 = "4";
    //承诺书下载地址路径
    public static final String INDEX_CNS = "green_template/2022-03-05/推荐单位承诺书.docx";

    public static final String expert_set_default = "******";

    //绿色建筑，装配式建筑，节能建筑状态。  `prosstype` decimal(65,0) DEFAULT NULL COMMENT '0:施工图审查完成未开工1:开工建设2:已竣工验收3竣工后运行',；
    public static final Integer prosstype0 = 0;
    public static final Integer prosstype1 = 1;
    public static final Integer prosstype2 = 2;
    public static final Integer prosstype3 = 3;
    public static final Integer channeng = 9;
    //  applystate  申报状态  0未上报 1 待审核 2上报通过 3 上报退回
    //  绿色建筑，装配式建筑，节能建筑  `applystate` decimal(65,0) DEFAULT NULL COMMENT '申报状态  0未上报 1 已上报 3 退回',
    public static final Integer apply_state0 = 0;
    public static final Integer apply_state1 = 1;
    public static final Integer apply_state2 = 2;
    public static final Integer apply_state3 = 3;
    public static final Integer apply_state5 = 5;     //  市州 未上报：5、上报
    public static final Integer apply_state6 = 6;

    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE0 = "绿色建筑";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE1 = "建筑工程";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE2 = "市政工程";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE3 = "建筑节能";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE4 = "装配式工程";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE5 = "科技攻关项目";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE6 = "软科学项目";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE7 = "示范项目任务书";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE8 = "科研项目任务书";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE9 = "示范项目中期查验";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE10 = "项目变更";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE11 = "示范项目验收";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE12 = "科研项目验收";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE13 = "科研项目结项";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE14 = "科研项目验收证书";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE15 = "科研项目结题证书";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE16 = "示范项目验收证书";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE17 = "绩效自评";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE18 = "计划项目成果简介";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE19 = "专家库信息";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE20 = "初审";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE21 = "终审";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE22 = "专家审核";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE23 = "科技教育处";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE24 = "形审";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE25 = "示范项目库";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE26 = "立项";
    public static final String TASK_RETURN_BUSINESS_TYPE_VALUE27 = "注册";
    //项目阶段1-7（申报阶段、任务书阶段、中期查验阶段、验收管理阶段、验收结题证书阶段、结题验收绩效自评阶段、成果简介阶段）
    public static final String PROJECT_STAGE_1 = "1";
    public static final String PROJECT_STAGE_2 = "2";
    public static final String PROJECT_STAGE_3 = "3";
    public static final String PROJECT_STAGE_41 = "41";
    public static final String PROJECT_STAGE_42 = "42";
    public static final String PROJECT_STAGE_43 = "43";
    public static final String PROJECT_STAGE_51 = "51";
    public static final String PROJECT_STAGE_52 = "52";
    public static final String PROJECT_STAGE_53 = "53";
    public static final String PROJECT_STAGE_6 = "6";
    public static final String PROJECT_STAGE_6_1 = "6-1";
    public static final String PROJECT_STAGE_7 = "7";

    //项目库sfxmtype1项目库2示范项目
    public static final String PROJECT_KU_TYPE_1 = "1";
    public static final String PROJECT_KU_TYPE_2 = "2";
    //项目库sfxmstatus项目库状态：1未入库2已入库3已推荐
    public static final String PROJECT_KU_STATUS_1 = "1";
    public static final String PROJECT_KU_STATUS_2 = "2";
    public static final String PROJECT_KU_STATUS_3 = "3";
    //项目编号写死，因规则不确定
//    public static final String PROJECT_NUM_YEAR = "2023";

    //终止类型：1申请终止，2省厅终止
    public static final String CLOSE_TYPE_1 = "1";
    public static final String CLOSE_TYPE_2 = "2";
    //1延期、2终止
    public static final String IS_DEAL_OVERDUE_1 = "1";
    public static final String IS_DEAL_OVERDUE_2 = "2";
    //1项目终止申请,2信息变更申请
    public static final String IN_TYPE_1 = "1";
    public static final String IN_TYPE_2 = "2";
    //1-正常,2-冻结,3-新注册
    public static final String USER_STATUS_1 = "1";
    public static final String USER_STATUS_2 = "2";
    public static final String USER_STATUS_3 = "3";
    //最后进行阶段状态标识
    public static final String STAGE_STATUS = "STAGE_STATUS";
}
