package org.jeecg.common.util;

import org.springframework.util.StringUtils;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

/**
 * @description: 根据文件链接地址下载文件
 * @author: zyb
 * @date: 2020/10/13 15:47
 */
public class DownloadFile {
    public static String URL = "http://61.178.32.163:84/JSJGWebDataToG/DesktopModules/AttachMore/";

    public static void main(String[] args) {
        String url = "http://61.178.32.163:84/JSJGWebDataToG/DesktopModules/AttachMore/";
        //http://61.178.32.163:84/JSJGWebDataToG/DesktopModules/AttachMore\2c83ab43-00d7-4045-9fdc-536c10664c58\ScienceTech\9fd8484d-534f-4c10-9617-cf3fc49b2c70\8d18930d-edb0-4a8c-ad10-b0c7740f3409.jpg
        String testFile1 = "18cae2dc-e1dd-46ca-9bc2-8e1b06a34a\\ExpertLibray\\18cae2dc-e1dd-46ca-9bc2-8e1b06a34acd\\003e4dbf-4093-4e2b-84c0-d1c22fc16a79.pdf";
        int lastP = testFile1.lastIndexOf("\\");
        String lastName = testFile1.substring(lastP);
        String firstP = testFile1.substring(0, lastP);
        System.out.println("lastName=" + lastName);
        System.out.println("firstP=" + firstP);
        downloadB(url + testFile1, lastName, "E:\\greenFile\\" + firstP);
    }

    public static boolean downLoadFileList(String filePath) {
        if (!StringUtils.isEmpty(filePath)) {
            filePath = filePath.replace("green_file\\old\\", "");
        }
        int lastP = filePath.lastIndexOf("\\");
        if (lastP <= 0) {
            return false;
        } else {
            String lastName = filePath.substring(lastP);
            String firstP = filePath.substring(0, lastP);
            if (StringUtils.isEmpty(lastName) || StringUtils.isEmpty(firstP)) {
                return false;
            }
//            downloadB(URL + filePath, lastName, "F:\\greenFile\\" + firstP);
            downloadB(URL + filePath, lastName, "D:\\opt\\upFiles\\green_file\\old\\" + firstP);
        }
        return true;
    }

    /**
     * 根据链接地址下载文件
     *
     * @param downloadUrl 文件链接地址
     * @param path        下载存放文件地址 + 文件名
     */
    private static void downloadA(String downloadUrl, String path) {
        URL url = null;
        DataInputStream dataInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            url = new URL(downloadUrl);
            dataInputStream = new DataInputStream(url.openStream());
            fileOutputStream = new FileOutputStream(new File(path));
            ByteArrayOutputStream output = new ByteArrayOutputStream();

            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            fileOutputStream.write(output.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (dataInputStream != null) {
                try {
                    dataInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @param downloadUrl 文件链接地址
     * @param filename    保存文件名
     * @param filePath    保存文件路径
     */
    private static void downloadB(String downloadUrl, String filename, String filePath) {
        URL url = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            url = new URL(downloadUrl);
            // 打开连接
            URLConnection con = url.openConnection();
            // 请求超时:5s
            con.setConnectTimeout(5 * 1000);
            inputStream = con.getInputStream();

            byte[] bytes = new byte[1024];
            // 读取到的数据长度
            int length;
            File savePath = new File(filePath);
            if (!savePath.exists()) {
                // 如果不存在当前文件夹，则创建该文件夹
                boolean mkdir = savePath.mkdirs();
                if (!mkdir) {
                    System.out.println("创建文件夹失败");
                    return;
                }
            }
            outputStream = new FileOutputStream(savePath.getPath() + "\\" + filename);
            // 读取
            while ((length = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
