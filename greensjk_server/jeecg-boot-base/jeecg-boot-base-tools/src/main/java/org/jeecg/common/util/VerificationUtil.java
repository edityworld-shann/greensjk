package org.jeecg.common.util;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 12356
 * @Description: 字段校验
 * @date 2022/4/1310:20
 */
public class VerificationUtil {

    /**
     * 项目负责人校验
     *
     * @param projectLeader
     * @return
     */
    public static boolean projectLeaderVerification(String projectLeader) {
        if (StringUtils.isEmpty(projectLeader)) {
            return false;
        } else {
            if (patternEnName(projectLeader)) {
                return true;
            }
            if (!checkName(projectLeader)) {
                return false;
            }
        }
        return true;
    }

    //判断整个字符串都由汉字组成
    public static boolean checkName(String name) {
        int n = 0;
        for (int i = 0; i < name.length(); i++) {
            n = (int) name.charAt(i);
            if (!(19968 <= n && n < 40869)) {
                return false;
            }
        }
        return true;
    }

    //正则是否全是英文
    public static boolean patternEnName(String enName) {
        Pattern p = Pattern.compile("[a-zA-Z]+");
        Matcher m = p.matcher(enName);
        boolean flag = m.matches();
        return flag;
    }

    public static void main(String[] args) {
        System.out.println(patternEnName("Laa"));
    }
}
