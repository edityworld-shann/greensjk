package org.jeecg.common.util;

import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/10/2221:37
 */
public class GreenUtilSelf {
    ////默认为-1
    //        //0、负责人不符合此次申报条件：每年限申报1项建设科技计划项目
    //        //1、负责人不符合此次申报条件：有两项及以上建设科技计划项目未验收（结题）
    //        //2、负责人不符合此次申报条件：两年内有终止执行的项目
    //        //3、负责人不符合此次申报条件：有计划项目逾期但没提交终止执行申请及相关材料
    public static String getMesByNum(int mesNum) {
        String mes = null;
        switch (mesNum) {
            case 0:
                mes = "项目责任人不符合此次申报条件：每年只可以申报1项建筑科技计划项目！";
                break;
            case 1:
                mes = "项目责任人不符合此次申报条件：其名下有2项（含）以上建设科技计划项目未完结，不可再次申请项目。";
                break;
            case 2:
                mes = "项目责任人不符合此次申报条件：两年内，其名下有终止执行项目，不可再次申请项目。";
                break;
            case 3:
                mes = "项目责任人不符合此次申报条件：其名下有项目逾期且未终止，不可再次申请项目。";
                break;
        }
        return mes;
    }

    /**
     * 密码弱口令豹子号或3位连号
     *
     * @param pwd
     * @return
     */
    public static boolean matchesPassWord(String pwd) {
        boolean flag = false;
        //匹配3位以上豹子号
        String pattern1 = "([\\d])\\1{2,}";
        //匹配3位或以上顺增或顺降
        String pattern2 = "(?:(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){2,}|(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){2,})\\d";
        Pattern pa = Pattern.compile(pattern1);
        Matcher ma = pa.matcher(pwd);
        if (ma.find()) {
            flag = true;
        }
        Pattern pa1 = Pattern.compile(pattern2);
        Matcher ma1 = pa1.matcher(pwd);
        if (ma1.find()) {
            flag = true;
        }
        return flag;
    }

    /**
     * 申报项目创建日期跟2022-10-01作比较,大于返回true
     *
     * @param creteDate
     * @return
     */
    public static boolean compareToCreateDate(Date creteDate) {
        int result = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sdf.parse("2022-10-01"));
            result = creteDate.compareTo(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result >= 0) {
            return true;
        }
        return false;
    }
}
