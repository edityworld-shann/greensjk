package org.jeecg.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 使用libreoffice来进行转pdf
 */
@Slf4j
public class LibreOfficeUtil {
    /**
     * word转图片
     *
     * @param docxPath
     * @return
     * @throws IOException
     */
    public static boolean wordConverterToPdf(String docxPath) throws IOException {
        File file = new File(docxPath);
        String path = file.getParent();
        try {
            String osName = System.getProperty("os.name");
            String command = "";
            //图片转PDF
            //soffice --convert-to jpg ./123.pdf
            if (!osName.contains("Windows")) {
                command = "soffice --convert-to pdf  -outdir " + path + " " + docxPath;
//                command = "soffice --convert-to pdf  -outdir " + "D:\\opt\\upFiles\\green_task\\2022-06-07" + " " + docxPath;
            } else {
                command = "doc2pdf --output=" + path + File.separator + file.getName().replaceAll(".(?i)docx", ".pdf") + " " + docxPath;
            }
            log.info("pdf转换命令" + command);
            String result = executeCommand(command);
            log.info("pdf转换" + result);
            if (result.equals("") || result.contains("writer_pdf_Export")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return false;
    }

    /**
     * PDF转图片
     *
     * @param pdfPath
     * @return
     * @throws IOException
     */
    public static String wordConverterToJpg(String pdfPath) {
        try {
            if (!StringUtils.isEmpty(pdfPath)) {
                File file = new File(pdfPath);
                if (file.exists() && pdfPath.contains(".pdf")) {
                    String path = file.getParent();
                    String osName = System.getProperty("os.name");
                    String command = "";
                    //图片转PDF
                    //soffice --convert-to jpg ./123.pdf
                    if (!osName.contains("Windows")) {
                        command = "soffice --convert-to jpg  -outdir " + path + " " + pdfPath;
                    } else {
                        command = "doc2pdf --output=" + path + File.separator + file.getName().replaceAll(".(?i)docx", ".pdf") + " " + pdfPath;
                    }
                    log.info("PDF转图片命令" + command);
                    String result = executeCommand(command);
                    log.info("PDF转图片转换结果" + result);
                    if (result.equals("") || result.contains("draw_jpg_Export")) {
                        pdfPath = pdfPath.replace(".pdf", ".jpg");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        return pdfPath;
    }

    public static String executeCommand(String command) {
        StringBuffer output = new StringBuffer();
        Process p;
        InputStreamReader inputStreamReader = null;
        BufferedReader reader = null;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            inputStreamReader = new InputStreamReader(p.getInputStream(), "UTF-8");
            reader = new BufferedReader(inputStreamReader);
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(reader);
            IOUtils.closeQuietly(inputStreamReader);
        }
        return output.toString();

    }

    public static void main(String[] args) {
        try {
            String filepath = "D:\\opt\\upFiles\\green_task\\2022-06-07_";
            File file = new File(filepath);
            if (file.isDirectory()) {
                System.out.println("文件夹");
                String[] filelist = file.list();
                for (int i = 0; i < filelist.length; i++) {
                    File readfile = new File(filepath + "\\" + filelist[i]);
                    if (!readfile.isDirectory()) {
                        System.out.println("path=" + readfile.getPath());
                        wordConverterToPdf(readfile.getPath());
                    }
                }
                System.out.println("pdf转换完成" + filelist.length);
            }
//            wordConverterToPdf("D:\\opt\\nocreate.doc");
//            wordConverterToPdf("D:\\opt\\upFiles\\green_task\\temp\\长城·嘉峪苑住宅小区二期剩余楼位.doc");
//            wordConverterToPdf("D:\\opt\\upFiles\\green_task\\2022-05-15\\5.13陈鹏测试.doc");
//            wordConverterToJpg("E:\\专家验收意见+1页_1657006161658.pdf");
//            wordConverterToJpg("E:\\专家签字扫描件+1页_1657006253019.pdf");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
