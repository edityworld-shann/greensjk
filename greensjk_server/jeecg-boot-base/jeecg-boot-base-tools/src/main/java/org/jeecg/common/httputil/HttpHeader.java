package org.jeecg.common.httputil;

import java.util.HashMap;
import java.util.Map;

public class HttpHeader {
    private Map<String, String> params = new HashMap<String, String>();

    public HttpHeader addParam(String name, String value) {
        this.params.put(name, value);
        return this;
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public HttpHeader(Map<String, String> params) {
        this.params = params;
    }
}
