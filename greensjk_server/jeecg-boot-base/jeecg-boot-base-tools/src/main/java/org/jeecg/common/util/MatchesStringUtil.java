package org.jeecg.common.util;

import java.util.Random;


/**
 * ：随机产生字符串，字符串中的字符只能由特殊字符(&@#$%*),大写字母(A-Z)，小写字母(a-z)以及数字(0-9)组成，
 * 且每种字符至少出现一次。这样产生字符串的方式可以应用到如下场景，
 * 比如，我们有一个应用就是添加用户完毕之后，发邮件给指定用户包括一个长度为11位的初始化密码。
 *
 * @author lizhi
 */
public class MatchesStringUtil {
    private static char[] symbols;

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        for (char ch = 'a'; ch <= 'z'; ++ch)
            tmp.append(ch);
        for (char ch = 'A'; ch <= 'Z'; ++ch)
            tmp.append(ch);

        // 添加一些特殊字符
        tmp.append("@#$%&*");
        symbols = tmp.toString().toCharArray();
    }

    private static Random random = new Random();

    private static char[] buf;

    public MatchesStringUtil(int length) {
        if (length < 1)
            throw new IllegalArgumentException("length < 1: " + length);
        buf = new char[length];
    }

    public static String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    /**
     * 校验密码规则
     *
     * @param pwd
     * @return
     */
    public static boolean matchesPassword(String pwd) {
        return pwd.matches(".*(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&*]).*");
    }

    /**
     * 校验邮箱规则
     *
     * @param email
     * @return
     */
    public static boolean matchesEmail(String email) {
        return email.matches("\\w+@\\w+(\\.\\w{2,3})*\\.\\w{2,3}");
    }

    /**
     * 校验手机号规则
     *
     * @param phone
     * @return
     */
    public static boolean matchesPhone(String phone) {
        return phone.matches("[\\d]{11}");
    }

    /**
     * 校验身份证规则
     *
     * @param Card
     * @return
     */
    public static boolean matchesCard(String Card) {
        return Card.matches("^[1-9]\\d{5}(18|19|20|(3\\d))\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$");
    }

    public static void main(String[] args) {
//        MatchesStringUtil randomTest = new MatchesStringUtil(11);
//        for (int i = 0; i < 10; i++) {
//            String result = null;
//            do {
//                result = randomTest.nextString();
//            } while (!result.matches(".*(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&*]).*"));
//            System.out.println(result);
//        }
        System.out.println(matchesCard(""));
    }

}
