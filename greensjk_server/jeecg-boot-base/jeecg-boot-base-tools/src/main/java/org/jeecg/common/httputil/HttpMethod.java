package org.jeecg.common.httputil;

public enum HttpMethod {
    GET, POST;
}
