package org.jeecg.common.util.encryption;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.jeecg.common.aspect.annotation.ValidateEncryptEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/11/2512:06
 */
@Slf4j
public class ValidateEncryptEntityUtil {
    public static boolean isEncrypt = true;
    public static boolean isDecrypt = true;

    /**
     * Object加密
     *
     * @param object
     * @return
     */
    public static <T> T validateEncryptObject(T object, boolean isEncrypt) {
        if (object != null && isEncrypt) {
            Field[] declaredFields = object.getClass().getDeclaredFields();
            for (Field field : declaredFields) {
                ValidateEncryptEntity annotation = field.getAnnotation(ValidateEncryptEntity.class);
                if (annotation != null) {
                    if (annotation.isEncrypt()) {
                        //表示该字段是必填字段
                        field.setAccessible(true);
                        try {
                            Object o = field.get(object);
                            if (o != null && o instanceof String) {
                                String name = getMethodName(field.getName());
                                Method m = object.getClass().getMethod("get" + name);
                                //调用getter方法获取属性值
                                String value = (String) m.invoke(object);
                                String encryptValue = null;
                                //加密
                                encryptValue = Sm4Util.encryptEcb(value);
                                m = object.getClass().getMethod("set" + name, String.class);
                                //赋值
                                m.invoke(object, encryptValue);
//                                log.info(value + "--加密-->" + encryptValue);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return object;
    }

    /**
     * Object解密
     *
     * @param object
     * @return
     */
    public static <T> T validateDecryptObject(T object, boolean isDecrypt) {

        if (object != null && isDecrypt) {
            Field[] declaredFields = object.getClass().getDeclaredFields();
            for (Field field : declaredFields) {

                ValidateEncryptEntity annotation = field.getAnnotation(ValidateEncryptEntity.class);
                if (annotation != null) {
                    if (annotation.isEncrypt()) {
                        //表示该字段是必填字段
                        field.setAccessible(true);
                        try {
                            Object o = field.get(object);
                            if (o != null && o instanceof String) {
                                String name = getMethodName(field.getName());
                                Method m = object.getClass().getMethod("get" + name);
                                //调用getter方法获取属性值
                                String value = (String) m.invoke(object);
                                String encryptValue = null;

                                // add by liur for sm4 check
                                String regex="^[A-Fa-f0-9]+$";
                                if(!value.matches(regex) || value.length()==0 || value.length()%32 != 0){
                                    continue;
                                }

                                //解密
                                encryptValue = Sm4Util.decryptEcb(value);
                                m = object.getClass().getMethod("set" + name, String.class);
                                //赋值
                                m.invoke(object, encryptValue);
//                                log.info(value + "--解密-->" + encryptValue);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return object;
    }

    /**
     * List解密
     *
     * @param object
     * @return
     */
    public static <T> List<T> validateDecryptList(List<T> object, boolean isDecrypt) {
        if (object != null && isDecrypt) {
            for (int i = 0; i < object.size(); i++) {
                T t = object.get(i);
                //处理过程
                Field[] declaredFields = t.getClass().getDeclaredFields();
                for (Field field : declaredFields) {
                    ValidateEncryptEntity annotation = field.getAnnotation(ValidateEncryptEntity.class);
                    if (annotation != null) {
                        if (annotation.isEncrypt()) {
                            //表示该字段是必填字段
                            field.setAccessible(true);
                            try {
                                Object o = field.get(t);
                                if (o != null && o instanceof String) {
                                    String name = getMethodName(field.getName());
                                    Method m = t.getClass().getMethod("get" + name);
                                    //调用getter方法获取属性值
                                    String value = (String) m.invoke(t);
                                    String encryptValue = null;

                                    // add by liur for sm4 check
                                    String regex="^[A-Fa-f0-9]+$";
                                    if(!value.matches(regex) || value.length()==0 || value.length()%32 != 0){
                                        continue;
                                    }

                                    //解密
                                    encryptValue = Sm4Util.decryptEcb(value);
                                    m = t.getClass().getMethod("set" + name, String.class);
                                    //赋值
                                    m.invoke(t, encryptValue);
//                                    log.info(value + "--解密-->" + encryptValue);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                //
                object.set(i, t);
            }
        }
        return object;
    }

    /**
     * 把一个字符串的第一个字母大写、效率是最高的
     */
    public static String getMethodName(String fieldName) {
        byte[] items = fieldName.getBytes();
        items[0] = (byte) ((char) items[0] - 'a' + 'A');
        return new String(items);
    }
}
