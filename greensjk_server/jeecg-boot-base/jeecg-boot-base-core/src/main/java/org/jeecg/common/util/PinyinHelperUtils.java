package org.jeecg.common.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;

/**
 * @author 12356
 * @Description: TODO
 * @date 2022/3/1515:06
 */
public class PinyinHelperUtils {
    public static String getPinYinHeadChar(String name) {
        StringBuilder pinyin = new StringBuilder();
        try {
            char[] charArray = name.toCharArray();
            HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
            // 设置大小写格式
            defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
            // 设置声调格式：
            defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            for (int i = 0; i < charArray.length; i++) {
                //匹配中文,非中文转换会转换成null
                if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                    if (hanyuPinyinStringArray != null) {
                        pinyin.append(hanyuPinyinStringArray[0].charAt(0));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pinyin.toString();
    }

    public static String getPsw(int len) {
        String baseStr = "123456789abcdefghjkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ!@#$%&*";
        return getPswInit(len, baseStr);
    }

    public static String getPswInitAppend() {
        return getPswInit(1, "ABCDEFGHJKLMNPQRSTUVWXYZ") + getPswInit(4, "abcdefghjklmnpqrstuvwxyz") + getPswInit(1, "!@#$%&*") + getPswInit(6, "23456789");
    }

    public static String getPswInit(int len, String baseStr) {
        // 1、定义基本字符串baseStr和出参password
//        String baseStr = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()_+{}|<>?";
        StringBuilder password = null;
        // 2、使用循环来判断是否是正确的密码
        boolean flag = false;
//        while (!flag) {
        // 密码重置
        password = new StringBuilder();
        // 个数计数
        int a = 0, b = 0, c = 0, d = 0;
        for (int i = 0; i < len; i++) {
            int rand = (int) (Math.random() * baseStr.length());
            password.append(baseStr.charAt(rand));
            if (rand < 10) {
                a++;
            }
            if (10 <= rand && rand < 36) {
                b++;
            }
            if (36 <= rand && rand < 62) {
                c++;
            }
            if (62 <= rand) {
                d++;
            }
            // 若4类都不为0，则此次为无效密码，break掉，重新进入while
            if (a * b * c * d != 0) {
//                    break;
            }
        }
        // 3、判断是否是正确的密码（4类中仅一类为0，其他不为0）
        flag = (a * b * c != 0 && d == 0) || (a * b * d != 0 && c == 0)
                || (a * c * d != 0 && b == 0) || (b * c * d != 0 && a == 0);
//        }
        return password.toString();
    }

    /**
     * 根据名称生成小写首字母加随机数字密码
     *
     * @param name
     * @return
     */
    public static String createAutoPassWord(String name) {
        String head = getPinYinHeadChar(name);
        String footer = getPsw(5);
        return head + footer;
    }

    public static void main(String[] args) {
//        System.out.println(createAutoPassWord("天水市住房和城乡建设局"));
        for (int i = 0; i < 30; i++) {
//            System.out.println("11111111");
            System.out.println(getPswInitAppend());
        }
    }
}
